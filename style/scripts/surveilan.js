//load select2
function kembali_afp() {
    $.ajax({
        //dataType:'json',
        //data: 'sub_district_id='+$('#sub_district_id').val(),
        url:'{{URL::to("daftar_pe_afp")}}',
        success:function(data) {
            //alert(data);
            $('#data_afp').html('');
            $('#data_afp').html(data);
        }
    });
}

function entriafp() {
    $.ajax({
        //dataType:'json',
        //data: 'sub_district_id='+$('#sub_district_id').val(),
        url:'{{URL::to("afp/entriafp")}}',
        success:function(data) {
            //alert(data);
            $('#data_afp').html('');
            $('#data_afp').html(data);
            $('.id_entri').remove();
        }
    });
}


function showKabupaten(){
  var provinsi = $("#id_provinsi").val();
  // //kirim data ke server
  //$('.loading').html("<img src='../public/images/loading.gif' width='35' height='35'>");
  $.post('/provinsi/getkab', {provinsi:provinsi}, function(response)
  {
      $("#id_kabupaten").html(response);
     // $('.loading').html('');
  });

}

function showKecamatan(){
  var kabupaten = $("#id_kabupaten").val();
  // //kirim data ke server
  //$('.loading').html("<img src='../public/images/loading.gif' width='35' height='35'>");
  $.post('/provinsi/getkec', {kabupaten:kabupaten}, function(response)
  {
      $("#id_kecamatan").html(response);
     // $('.loading').html('');
  });

}

function showKelurahan(){
  var kecamatan= $("#id_kecamatan").val();
  // //kirim data ke server
  //$('.loading').html("<img src='../public/images/loading.gif' width='35' height='35'>");
  $.post('/provinsi/getkel', {kecamatan:kecamatan}, function(response)
  {
      $("#id_kelurahan").html(response);
     // $('.loading').html('');
  });

}

