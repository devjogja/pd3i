//load select2
$(document).ready(function() { $("#id_kabupaten").select2(); });
$(document).ready(function() { $("#id_provinsi").select2(); });
$(document).ready(function() { $("#id_kecamatan").select2(); });
$(document).ready(function() { $("#id_kelurahan").select2(); });
// Function to clear select2 option and disable it
function clearAndDisable(elem) {
  $(elem).select2("val", ""); // set element to null
  $(elem).select2('enable',false); // disable element
  $(elem).find('option').remove(); // clear element options
}

// Function to clear select2 option and enable it
function clearAndEnable(elem) {
  $(elem).select2('enable',true); // enable element
  $(elem).select2("val", ""); // set element to null
}

//load kabupaten select provinsi
$('#id_provinsi').on('change', function() {
  
  var provinsi = this.value;
  //alert(provinsi);
  // //kirim data ke server
  //$('#spinner').html("&nbsp;<img src='../asset/select2/select2-spinner.gif')}}'>");
  
  $.post('provinsi/getKab', {provinsi:provinsi}, function(response)
  {
      $("#id_kabupaten").html(response);
      //$('#spinner').html('');
  });
})

function showKabupaten(){
  var provinsi = $("#id_provinsi").val();
  // //kirim data ke server
  //$('.loading').html("<img src='../public/images/loading.gif' width='35' height='35'>");
  $.post('/provinsi/getkab', {provinsi:provinsi}, function(response)
  {
      $("#id_kabupaten").html(response);
     // $('.loading').html('');
  });

}

function showKecamatan(){
  var kabupaten = $("#id_kabupaten").val();
  // //kirim data ke server
  //$('.loading').html("<img src='../public/images/loading.gif' width='35' height='35'>");
  $.post('/provinsi/getkec', {kabupaten:kabupaten}, function(response)
  {
      $("#id_kecamatan").html(response);
     // $('.loading').html('');
  });

}

function showKelurahan(){
  var kecamatan= $("#id_kecamatan").val();
  // //kirim data ke server
  //$('.loading').html("<img src='../public/images/loading.gif' width='35' height='35'>");
  $.post('/provinsi/getkel', {kecamatan:kecamatan}, function(response)
  {
      $("#id_kelurahan").html(response);
     // $('.loading').html('');
  });

}
