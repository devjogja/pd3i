SELECT
	a.id_pe_crs AS id,
	b.no_epid AS no_epid,
	c.nama_anak AS nama_anak,
	c.nama_ortu AS nama_ortu,
	c.tanggal_lahir AS tanggal_lahir,
	c.umur AS umur,
	c.jenis_kelamin AS jenis_kelamin,
	b.id_crs AS id_crs,
	concat_ws(
		', ',
		c.alamat,
		d.kelurahan,
		d.kecamatan,
		d.kabupaten,
		d.provinsi
	) AS alamat,
	b.id_tempat_periksa AS id_tempat_periksa
FROM
pe_crs a
JOIN crs b ON a.id_pe_crs = b.id_pe_crs
JOIN pasien c ON b.id_pasien = c.id_pasien
JOIN located d ON c.id_kelurahan = d.id_kelurahan
WHERE
	(a.created_by IS NOT NULL)
GROUP BY
	a.id_pe_crs