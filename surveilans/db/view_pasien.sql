SELECT
	a.id_pasien AS pasien_id,
	a.nik AS pasien_nik,
	a.nama_anak AS pasien_nama,
	a.nama_ortu AS pasien_nama_ortu,
	a.alamat AS pasien_alamat,
	b.id_provinsi AS pasien_id_provinsi,
	b.id_kabupaten AS pasien_id_kabupaten,
	b.id_kecamatan AS pasien_id_kecamatan,
	b.id_kelurahan AS pasien_id_kelurahan,
	b.provinsi AS pasien_provinsi,
	b.kabupaten AS pasien_kabupaten,
	b.kecamatan AS pasien_kecamatan,
	b.kelurahan AS pasien_kelurahan,
	a.tanggal_lahir AS pasien_tgl_lahir,
	a.umur AS pasien_umur,
	a.umur_bln AS pasien_umur_bln,
	a.umur_hr AS pasien_umur_hr,
		CASE
		WHEN a.jenis_kelamin = '1' THEN 'Laki-laki'
		WHEN a.jenis_kelamin = '2' THEN 'Perempuan'
		WHEN a.jenis_kelamin = '3' THEN 'Tidak Jelas'
		ELSE '-'
		END AS pasien_jenis_kelamin,
	a.jenis_kelamin AS pasien_id_jenis_kelamin,
	a.no_telp AS pasien_no_telp,
	a.no_rm AS pasien_no_rm,
	a.tempat_lahir_bayi AS pasien_tempat_lahir_bayi,
	a.umur_kehamilan_bayi AS pasien_umur_kehamilan_bayi,
	a.berat_badan_bayi AS pasien_berat_badan_bayi
FROM
pasien a
LEFT JOIN located b ON a.id_kelurahan = b.id_kelurahan