SELECT
a.id AS id_profil_rs,
a.alamat,
a.penanggungjawab_1,
a.penanggungjawab_2,
a.created_dttm,
a.is_admin,
b.id,
b.kode_faskes,
b.kode_faskes_bpjs,
b.nama_faskes,
b.telp,
b.sms,
b.konfirmasi,
b.tipe_faskes,
c.id_kelurahan,
c.kelurahan,
d.id_kecamatan,
d.kecamatan,
e.id_kabupaten,
e.kabupaten,
f.id_provinsi,
f.provinsi,
g.id AS id_user,
g.email,
g.first_name,
g.last_name,
g.jabatan,
g.instansi,
g.jk,
g.no_telp
FROM
profil_rs AS a
JOIN rumahsakit2 AS b ON a.kode_faskes = b.kode_faskes
LEFT JOIN kelurahan AS c ON b.kode_desa = c.id_kelurahan
LEFT JOIN kecamatan AS d ON b.kode_kec = c.id_kecamatan
LEFT JOIN kabupaten AS e ON b.kode_kab = e.id_kabupaten
LEFT JOIN provinsi AS f ON b.kode_prop = f.id_provinsi
JOIN users AS g ON a.created_by=g.id

SELECT
c.rs_nama_faskes, c.rs_kabupaten
FROM
profil_rs AS a
JOIN users AS b ON a.created_by=b.id
JOIN view_rumahsakit AS c ON a.kode_faskes=c.rs_kode_faskes
WHERE b.email NOT IN('dev.jogja@gmail.com','root@gmail.com')
GROUP BY c.rs_id
ORDER BY c.rs_kabupaten

SELECT
c.provinsi
FROM
profil_dinkes_provinsi AS a
JOIN users AS b ON a.created_by=b.id
JOIN provinsi AS c ON a.kode_provinsi=c.id_provinsi
WHERE b.email NOT IN('dev.jogja@gmail.com','root@gmail.com')
GROUP BY c.id_provinsi
ORDER BY c.provinsi