SELECT
	a.jumlah_total_kasus_tambahan AS jumlah_total_kasus_tambahan,
	a.id AS id,
	a.no_epid AS no_epid,
	c.nama_anak AS nama_anak,
	c.nama_ortu AS nama_ortu,
	concat_ws(
		',',
		c.alamat,
		d.kelurahan,
		d.kecamatan,
		d.kabupaten,
		d.provinsi
	) AS alamat,
	c.tanggal_lahir AS tanggal_lahir,
	c.umur AS umur,
	c.jenis_kelamin AS jenis_kelamin,
	b.id_tempat_periksa AS id_tempat_periksa,
	e.puskesmas_code_faskes AS puskesmas_code_faskes
FROM
pe_campak a
JOIN campak b ON a.no_epid = b.no_epid
JOIN pasien c ON a.id_pasien = c.id_pasien
JOIN located d ON c.id_kelurahan = d.id_kelurahan
JOIN puskesmas e ON b.id_tempat_periksa = e.puskesmas_id