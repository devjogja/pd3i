SELECT
	concat_ws(
		', ',
		CONVERT (`e`.`alamat` USING utf8mb4),
		CONVERT (
			`h`.`kelurahan` USING utf8mb4
		),
		CONVERT (
			`h`.`kecamatan` USING utf8mb4
		),
		CONVERT (
			`h`.`kabupaten` USING utf8mb4
		),
		CONVERT (`h`.`provinsi` USING utf8mb4)
	) AS `alamat_new`,
	`a`.`id_campak` AS `campak_id_campak`,
	`a`.`no_epid` AS `campak_no_epid`,
	`a`.`no_epid_lama` AS `campak_no_epid_lama`,
	`a`.`id_tempat_periksa` AS `campak_id_tempat_periksa`,
	`a`.`nama_puskesmas` AS `campak_nama_puskesmas`,
	`a`.`created_by` AS `campak_created_by`,
	`a`.`vaksin_campak_sebelum_sakit` AS `campak_vaksin_campak_sebelum_sakit`,
	`a`.`tanggal_timbul_demam` AS `campak_tanggal_timbul_demam`,
	`a`.`tanggal_timbul_rash` AS `campak_tanggal_timbul_rash`,
	`a`.`tanggal_imunisasi_terakhir` AS `campak_tanggal_imunisasi_terakhir`,
	`a`.`keadaan_akhir` AS `campak_keadaan_akhir`,
	`a`.`vitamin_A` AS `campak_vitamin_A`,
	`a`.`hasil_serologi_igm_campak` AS `campak_hasil_serologi_igm_campak`,
	`a`.`hasil_igm_campak` AS `campak_hasil_igm_campak`,
	`a`.`hasil_igm_rubella` AS `campak_hasil_igm_rubella`,
	`a`.`hasil_isolasi_virus` AS `campak_hasil_isolasi_virus`,
	`a`.`hasil_serologi_igm_rubella` AS `campak_hasil_serologi_igm_rubella`,
	`a`.`hasil_virologi_igm_campak` AS `campak_hasil_virologi_igm_campak`,
	`a`.`status_kasus` AS `campak_status_kasus`,
	`a`.`hasil_virologi_igm_rubella` AS `campak_hasil_virologi_igm_rubella`,
	`a`.`klasifikasi_final` AS `campak_klasifikasi_final`,
	`a`.`jenis_spesimen_serologi` AS `campak_jenis_spesimen_serologi`,
	`a`.`tanggal_ambil_sampel_serologi` AS `campak_tanggal_ambil_sampel_serologi`,
	`a`.`jenis_spesimen_virologi` AS `campak_jenis_spesimen_virologi`,
	`a`.`tanggal_ambil_sampel_virologi` AS `campak_tanggal_ambil_sampel_virologi`,
	`a`.`tanggal_laporan_diterima` AS `campak_tanggal_laporan_diterima`,
	`a`.`tanggal_pelacakan` AS `campak_tanggal_pelacakan`,
	`a`.`tanggal_periksa` AS `campak_tanggal_periksa`,
	`a`.`kode_faskes` AS `campak_kode_faskes`,
	`b`.`id` AS `rs_id`,
	`b`.`kode_faskes` AS `rs_kode_faskes`,
	`b`.`kode_faskes_bpjs` AS `rs_kode_faskes_bpjs`,
	`b`.`nama_faskes` AS `rs_nama_faskes`,
	`b`.`tipe_faskes` AS `rs_tipe_faskes`,
	`b`.`kepemilikan` AS `rs_kepemilikan`,
	`b`.`alamat` AS `rs_alamat`,
	`b`.`provinsi` AS `rs_provinsi`,
	`b`.`kabupaten` AS `rs_kabupaten`,
	`b`.`kecamatan` AS `rs_kecamatan`,
	`b`.`desa` AS `rs_desa`,
	`b`.`kontrak_bpjs` AS `rs_kontrak_bpjs`,
	`b`.`regiona_bpjs` AS `rs_regiona_bpjs`,
	`b`.`nama_kc` AS `rs_nama_kc`,
	`b`.`regional_lokal` AS `rs_regional_lokal`,
	`b`.`telp` AS `rs_telp`,
	`b`.`sms` AS `rs_sms`,
	`b`.`wilayah_koordinasi` AS `rs_wilayah_koordinasi`,
	`b`.`konfirmasi` AS `rs_konfirmasi`,
	`b`.`kode_desa` AS `rs_kode_desa`,
	`b`.`kode_kec` AS `rs_kode_kec`,
	`b`.`kode_kab` AS `rs_kode_kab`,
	`b`.`kode_prop` AS `rs_kode_prop`,
	`c`.`puskesmas_id` AS `puskesmas_id`,
	`c`.`puskesmas_name` AS `puskesmas_name`,
	`c`.`puskesmas_code_faskes` AS `puskesmas_code_faskes`,
	`c`.`alamat` AS `puskesmas_alamat`,
	`c`.`lokasi_puskesmas` AS `puskesmas_lokasi_puskesmas`,
	`c`.`kabupaten_name` AS `puskesmas_kabupaten_name`,
	`c`.`konfirmasi` AS `puskesmas_konfirmasi`,
	`c`.`kode_desa` AS `puskesmas_kode_desa`,
	`c`.`kode_kec` AS `puskesmas_kode_kec`,
	`c`.`kode_kab` AS `puskesmas_kode_kab`,
	`c`.`kode_prop` AS `puskesmas_kode_prop`,
	`d`.`id_hasil_uji_lab_campak` AS `lab_id_hasil_uji_lab_campak`,
	`d`.`created_by` AS `lab_created_by`,
	`d`.`no_spesimen` AS `lab_no_spesimen`,
	`d`.`hasil_spesimen_darah` AS `lab_hasil_spesimen_darah`,
	`d`.`hasil_spesimen_urin` AS `lab_hasil_spesimen_urin`,
	`d`.`kondisi_spesimen` AS `lab_kondisi_spesimen`,
	`d`.`sampel_lainnya` AS `lab_sampel_lainnya`,
	`d`.`tanggal_pengiriman_spesimen_propinsi` AS `lab_tanggal_pengiriman_spesimen_propinsi`,
	`d`.`tanggal_pengiriman_spesimen_laboratorium` AS `lab_tanggal_pengiriman_spesimen_laboratorium`,
	`d`.`tanggal_terima_spesimen_laboratorium` AS `lab_tanggal_terima_spesimen_laboratorium`,
	`d`.`tanggal_pemeriksaan_sampel` AS `lab_tanggal_pemeriksaan_sampel`,
	`d`.`tanggal_hasil_tersedia` AS `lab_tanggal_hasil_tersedia`,
	`d`.`tanggal_hasil_dilaporkan` AS `lab_tanggal_hasil_dilaporkan`,
	`d`.`tanggal_RRL` AS `lab_tanggal_RRL`,
	`d`.`tanggal_terima_hasil_RRL` AS `lab_tanggal_terima_hasil_RRL`,
	`d`.`permintaan_tes_serologi` AS `lab_permintaan_tes_serologi`,
	`d`.`hasil_RRL` AS `lab_hasil_RRL`,
	`d`.`kondisi_spesimen_dipropinsi` AS `lab_kondisi_spesimen_dipropinsi`,
	`d`.`kit_elisa` AS `lab_kit_elisa`,
	`d`.`lainnya` AS `lab_lainnya`,
	`d`.`campak_antigen_OD` AS `lab_campak_antigen_OD`,
	`d`.`rubella_antigen_OD` AS `lab_rubella_antigen_OD`,
	`d`.`control_antigen_OD` AS `lab_control_antigen_OD`,
	`d`.`hasil_pemeriksaan_akhir` AS `lab_hasil_pemeriksaan_akhir`,
	`d`.`regional_reference_lab` AS `lab_regional_reference_lab`,
	`d`.`kondisi_spesimen_dilab` AS `lab_kondisi_spesimen_dilab`,
	`d`.`hasil_pemeriksaan_campak` AS `lab_hasil_pemeriksaan_campak`,
	`d`.`hasil_igm_campak` AS `lab_hasil_igm_campak`,
	`d`.`hasil_igm_rubella` AS `lab_hasil_igm_rubella`,
	`d`.`hasil_isolasi_virus` AS `lab_hasil_isolasi_virus`,
	`d`.`hasil_pemeriksaan_rubella` AS `lab_hasil_pemeriksaan_rubella`,
	`d`.`hasil_pemeriksaan_bukan_campak_rubella` AS `lab_hasil_pemeriksaan_bukan_campak_rubella`,
	`d`.`jenis_pemeriksaan` AS `lab_jenis_pemeriksaan`,
	`d`.`tipe_spesimen` AS `lab_tipe_spesimen`,
	`d`.`klasifikasi_final` AS `lab_klasifikasi_final`,
	`d`.`klasifikasi_final_rubella` AS `lab_klasifikasi_final_rubella`,
	`d`.`hasil_pemeriksaan_discarded` AS `lab_hasil_pemeriksaan_discarded`,
	`d`.`klasifikasi_final_discarded` AS `lab_klasifikasi_final_discarded`,
	`d`.`keterangan` AS `lab_keterangan`,
	`d`.`tanggal_uji_laboratorium` AS `lab_tanggal_uji_laboratorium`,
	`d`.`tanggal_terima_spesimen` AS `lab_tanggal_terima_spesimen`,
	`d`.`tanggal_kirim_hasil` AS `lab_tanggal_kirim_hasil`,
	`d`.`status` AS `lab_status`,
	`e`.`id_pasien` AS `pasien_id_pasien`,
	`e`.`nik` AS `pasien_nik`,
	`e`.`nama_anak` AS `pasien_nama_anak`,
	`e`.`nama_ortu` AS `pasien_nama_ortu`,
	`e`.`alamat` AS `pasien_alamat`,
	`e`.`tanggal_lahir` AS `pasien_tanggal_lahir`,
	`e`.`umur` AS `pasien_umur`,
	`e`.`umur_bln` AS `pasien_umur_bln`,
	`e`.`umur_hr` AS `pasien_umur_hr`,
	`e`.`jenis_kelamin` AS `pasien_jenis_kelamin`,
	`e`.`no_telp` AS `pasien_no_telp`,
	`e`.`no_rm` AS `pasien_no_rm`,
	`e`.`tempat_lahir_bayi` AS `pasien_tempat_lahir_bayi`,
	`e`.`umur_kehamilan_bayi` AS `pasien_umur_kehamilan_bayi`,
	`e`.`berat_badan_bayi` AS `pasien_berat_badan_bayi`,
	`f`.`id` AS `pe_id`,
	`f`.`created_by` AS `pe_created_by`,
	`f`.`alamat_tinggal_sementara` AS `pe_alamat_tinggal_sementara`,
	`f`.`tgl_pe_dialamat_sementara` AS `pe_tgl_pe_dialamat_sementara`,
	`f`.`jumlah_kasus_dialamat_sementara` AS `pe_jumlah_kasus_dialamat_sementara`,
	`f`.`alamat_tempat_tinggal` AS `pe_alamat_tempat_tinggal`,
	`f`.`tgl_pe_alamat_tempat_tinggal` AS `pe_tgl_pe_alamat_tempat_tinggal`,
	`f`.`jumlah_kasus_dialamat_tempat_tinggal` AS `pe_jumlah_kasus_dialamat_tempat_tinggal`,
	`f`.`sekolah` AS `pe_sekolah`,
	`f`.`tgl_pe_sekolah` AS `pe_tgl_pe_sekolah`,
	`f`.`jumlah_kasus_disekolah` AS `pe_jumlah_kasus_disekolah`,
	`f`.`tempat_kerja` AS `pe_tempat_kerja`,
	`f`.`tgl_pe_tempat_kerja` AS `pe_tgl_pe_tempat_kerja`,
	`f`.`jumlah_kasus_tempat_kerja` AS `pe_jumlah_kasus_tempat_kerja`,
	`f`.`lain_lain` AS `pe_lain_lain`,
	`f`.`tgl_pe_lain_lain` AS `pe_tgl_pe_lain_lain`,
	`f`.`jumlah_kasus_lain_lain` AS `pe_jumlah_kasus_lain_lain`,
	`f`.`jumlah_total_kasus_tambahan` AS `pe_jumlah_total_kasus_tambahan`,
	`f`.`waktu_pengobatan` AS `pe_waktu_pengobatan`,
	`f`.`tempat_pengobatan` AS `pe_tempat_pengobatan`,
	`f`.`obat_yang_diberikan` AS `pe_obat_yang_diberikan`,
	`f`.`no_epid` AS `pe_no_epid`,
	`f`.`dirumah_orang_sakit_sama` AS `pe_dirumah_orang_sakit_sama`,
	`f`.`kapan_sakit_dirumah` AS `pe_kapan_sakit_dirumah`,
	`f`.`disekolah_orang_sakit_sama` AS `pe_disekolah_orang_sakit_sama`,
	`f`.`kapan_sakit_disekolah` AS `pe_kapan_sakit_disekolah`,
	`f`.`kekurangan_gizi` AS `pe_kekurangan_gizi`,
	`f`.`tanggal_penyelidikan` AS `pe_tanggal_penyelidikan`,
	`f`.`pelaksana` AS `pe_pelaksana`,
	`h`.`kelurahan` AS `pasien_kelurahan`,
	`h`.`kecamatan` AS `pasien_kecamatan`,
	`h`.`kabupaten` AS `pasien_kabupaten`,
	`h`.`provinsi` AS `pasien_provinsi`,
	`h`.`id_kelurahan` AS `pasien_id_kelurahan`,
	`h`.`id_kabupaten` AS `pasien_id_kabupaten`,
	`h`.`id_provinsi` AS `pasien_id_provinsi`,
	`h`.`id_kecamatan` AS `pasien_id_kecamatan`,
	`d`.`id_uji_lab` AS `lab_id_uji_lab`,
	`f`.`faskes_id` AS `pe_faskes_id`,
	`a`.`jenis_kasus` AS `campak_jenis_kasus`,
	`a`.`klb_ke` AS `campak_klb_ke`,
	`f`.`status_at` AS `pe_status_at`,
	`a`.`id_laboratorium` AS `campak_id_laboratorium`,
	`a`.`status_vaksinasi` AS `campak_status_vaksinasi`,
	`a`.`dosis` AS `campak_dosis`,
	`a`.`riwayat_pergi` AS `campak_riwayat_pergi`,
	`a`.`tujuan_riwayat_pergi` AS `campak_tujuan_riwayat_pergi`,
	`d`.`sumber_spesimen` AS `lab_sumber_spesimen`,
	`d`.`id_outbreak` AS `lab_id_outbreak`,
	`d`.`negara` AS `lab_negara`,
	`d`.`pelapor` AS `lab_pelapor`,
	`d`.`id_provinsi_outbreak` AS `lab_id_provinsi_outbreak`,
	`d`.`id_kabupaten_outbreak` AS `lab_id_kabupaten_outbreak`,
	`d`.`id_kecamatan_outbreak` AS `lab_id_kecamatan_outbreak`,
	`d`.`id_kelurahan_outbreak` AS `lab_id_kelurahan_outbreak`,
	`d`.`id_puskesmas_outbreak` AS `lab_id_puskesmas_outbreak`,
	`d`.`id_rs_outbreak` AS `lab_id_rs_outbreak`,
	`d`.`klb_minggu` AS `lab_klb_minggu`,
	`d`.`dokumentasi` AS `lab_dokumentasi`,
	`d`.`type_spesimen` AS `lab_type_spesimen`,
	`d`.`tgl_ambil_spesimen` AS `lab_tgl_ambil_spesimen`,
	`d`.`tgl_kirim_spesimen` AS `lab_tgl_kirim_spesimen`,
	`d`.`sample_ke` AS `lab_sampel_ke`,
	`d`.`tgl_terima_spesimen` AS `lab_tgl_terima_spesimen`,
	`d`.`kondisi_spesimen_waktu_dilab` AS `lab_kondisi_spesimen_waktu_dilab`,
	`d`.`specimen_submit` AS `lab_specimen_submit`,
	`d`.`id_lab` AS `lab_id_lab`,
	`i`.`id_laboratorium` AS `labs_id_laboratorium`,
	`i`.`nama_laboratorium` AS `labs_nama_laboratorium`,
	`i`.`lab_code` AS `labs_lab_code`,
	`i`.`alamat` AS `labs_alamat`,
	`d`.`status_lab` AS `lab_status_lab`,
	`a`.`id_outbreak` AS `campak_id_outbreak`,
	`d`.`dilaporkan_oleh` AS `lab_dilaporkan_oleh`,
	`a`.`no_epid_klb` AS `campak_no_epid_klb`,
	`a`.`deleted_at` AS `campak_deleted_at`,
	`a`.`status_at` AS `status_at`
FROM
	(
		(
			(
				(
					(
						(
							(
								`campak` `a`
								LEFT JOIN `rumahsakit2` `b` ON (
									(
										`a`.`id_tempat_periksa` = `b`.`id`
									)
								)
							)
							LEFT JOIN `puskesmas` `c` ON (
								(
									`a`.`id_tempat_periksa` = `c`.`puskesmas_id`
								)
							)
						)
						JOIN `hasil_uji_lab_campak` `d` ON (
							(
								`a`.`id_campak` = `d`.`id_campak`
							)
						)
					)
					JOIN `pasien` `e` ON (
						(
							`d`.`id_pasien` = `e`.`id_pasien`
						)
					)
				)
				LEFT JOIN `pe_campak` `f` ON (
					(
						`a`.`id_pe_campak` = `f`.`id`
					)
				)
			)
			LEFT JOIN `located` `h` ON (
				(
					CONVERT (
						`e`.`id_kelurahan` USING utf8mb4
					) = CONVERT (
						`h`.`id_kelurahan` USING utf8mb4
					)
				)
			)
		)
		LEFT JOIN `laboratorium` `i` ON (
			(
				`a`.`id_laboratorium` = `i`.`id_laboratorium`
			)
		)
	)
WHERE
	isnull(`a`.`deleted_at`)
GROUP BY
	`a`.`id_campak`