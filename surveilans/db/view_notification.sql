SELECT
a.type_id,
a.submit_dttm,
b.id_kecamatan,
b.kecamatan,
c.id_kabupaten,
c.kabupaten,
d.id_provinsi,
d.provinsi,
e.campak_nama_puskesmas AS nama_faskes,
e.code_faskes
FROM
notification AS a
LEFT JOIN kecamatan AS b ON a.to_faskes_kec_id=b.id_kecamatan
LEFT JOIN kabupaten AS c ON b.id_kabupaten=c.id_kabupaten
LEFT JOIN provinsi AS d ON c.id_provinsi=d.id_provinsi
LEFT JOIN view_case_campak AS e ON a.global_id=e.id_campak AND a.type_id='campak'
WHERE a.deleted_at IS NULL