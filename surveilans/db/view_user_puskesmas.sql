SELECT
a.id AS id_profile_puskesmas,
a.alamat AS alamat_profile,
a.penanggungjawab_1 AS penanggungjawab_1,
a.penanggungjawab_2 AS penanggungjawab_2,
a.created_dttm AS created_dttm,
a.created_by AS created_by,
a.is_admin AS is_admin,
b.puskesmas_id AS puskesmas_id,
b.puskesmas_name AS puskesmas_name,
b.puskesmas_code_faskes AS puskesmas_code_faskes,
b.alamat AS alamat_puskesmas,
b.lokasi_puskesmas AS lokasi_puskesmas,
b.longitude AS longitude,
b.latitude AS latitude,
b.konfirmasi AS konfirmasi,
c.kelurahan AS kelurahan,
c.id_kelurahan AS id_kelurahan,
d.id_kecamatan AS id_kecamatan,
d.kecamatan AS kecamatan,
e.id_kabupaten AS id_kabupaten,
e.kabupaten AS kabupaten,
f.id_provinsi AS id_provinsi,
f.provinsi AS provinsi,
g.id AS id,
g.email AS email,
g.last_login AS last_login,
g.first_name AS first_name,
g.last_name AS last_name,
g.no_telp AS no_telp,
g.instansi AS instansi,
g.jabatan AS jabatan
FROM
profil_puskesmas AS a
JOIN puskesmas AS b ON a.kode_faskes = b.puskesmas_code_faskes
LEFT JOIN kelurahan AS c ON b.kode_desa = c.id_kelurahan
LEFT JOIN kecamatan AS d ON b.kode_kec = c.id_kecamatan
LEFT JOIN kabupaten AS e ON b.kode_kab = e.id_kabupaten
LEFT JOIN provinsi AS f ON b.kode_prop = f.id_provinsi
JOIN users AS g ON a.created_by=g.id
WHERE g.email NOT IN('dev.jogja@gmail.com','root@gmail.com')


SELECT
c.puskesmas_name, c.puskesmas_kecamatan, c.puskesmas_kabupaten
FROM
profil_puskesmas AS a
JOIN users AS b ON a.created_by=b.id
JOIN view_puskesmas AS c ON a.kode_faskes=c.puskesmas_code_faskes
WHERE b.email NOT IN('dev.jogja@gmail.com','root@gmail.com')
GROUP BY c.puskesmas_id
ORDER BY c.puskesmas_kabupaten