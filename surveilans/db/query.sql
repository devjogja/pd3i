SELECT a.id_campak, a.no_epid, c.* FROM
campak AS a
JOIN hasil_uji_lab_campak AS b ON a.id_campak=b.id_campak
JOIN pasien AS c ON b.id_pasien=c.id_pasien
LEFT JOIN notification AS d ON a.id_campak=d.global_id
LEFT JOIN pe_campak AS e ON a.no_epid=e.no_epid
WHERE c.nama_anak LIKE '%coba%'

-- delete campak
UPDATE campak AS a
JOIN hasil_uji_lab_campak AS b ON a.id_campak=b.id_campak
JOIN pasien AS c ON b.id_pasien=c.id_pasien
LEFT JOIN notification AS d ON a.id_campak=d.global_id
LEFT JOIN pe_campak AS e ON a.no_epid=e.no_epid
SET
a.deleted_at='2016-08-10 10:28:20', a.deleted_by='0',
b.deleted_at='2016-08-10 10:28:20', b.deleted_by='0',
c.deleted_at='2016-08-10 10:28:20', c.deleted_by='0',
d.deleted_at='2016-08-10 10:28:20', d.deleted_by='0',
e.deleted_at='2016-08-10 10:28:20', e.deleted_by='0'
WHERE c.nama_anak LIKE '%coba%'

-- delete afp
UPDATE afp AS a
JOIN hasil_uji_lab_afp AS b ON a.id_afp=b.id_afp
JOIN pasien AS c ON b.id_pasien=c.id_pasien
LEFT JOIN notification AS d ON a.id_afp=d.global_id
LEFT JOIN pe_afp AS e ON a.no_epid=e.no_epid
SET
a.deleted_at='2016-08-10 10:28:20', a.deleted_by='0',
b.deleted_at='2016-08-10 10:28:20', b.deleted_by='0',
c.deleted_at='2016-08-10 10:28:20', c.deleted_by='0',
d.deleted_at='2016-08-10 10:28:20', d.deleted_by='0',
e.deleted_at='2016-08-10 10:28:20', e.deleted_by='0'
WHERE c.nama_anak LIKE '%coba%'

-- delete difteri
UPDATE difteri AS a
JOIN hasil_uji_lab_difteri AS b ON a.id_difteri=b.id_difteri
JOIN pasien AS c ON b.id_pasien=c.id_pasien
LEFT JOIN notification AS d ON a.id_difteri=d.global_id
LEFT JOIN pe_difteri AS e ON a.no_epid=e.no_epid
SET
a.deleted_at='2016-08-10 10:28:20', a.deleted_by='0',
c.deleted_at='2016-08-10 10:28:20', c.deleted_by='0',
d.deleted_at='2016-08-10 10:28:20', d.deleted_by='0'
WHERE c.nama_anak LIKE '%coba%'

-- delete crs
UPDATE crs AS a
JOIN hasil_uji_lab_crs AS b ON a.id_hasil_uji_lab_crs=b.id_hasil_uji_lab_crs
JOIN pasien AS c ON a.id_pasien=c.id_pasien
LEFT JOIN notification AS d ON a.id_crs=d.global_id
LEFT JOIN pe_crs AS e ON a.id_pe_crs=e.id_pe_crs
SET
a.deleted_at='2016-08-10 10:28:20', a.deleted_by='0',
b.deleted_at='2016-08-10 10:28:20', b.deleted_by='0',
c.deleted_at='2016-08-10 10:28:20', c.deleted_by='0',
d.deleted_at='2016-08-10 10:28:20', d.deleted_by='0',
e.deleted_at='2016-08-10 10:28:20', e.deleted_by='0'
WHERE c.nama_anak LIKE '%coba%'

SELECT
view_detail_campak.pasien_nama AS `Nama Pasien`,
view_detail_campak.alamat_new AS `Alamat Pasien`,
CASE
WHEN kode_faskes='rs' THEN rs_nama_faskes
ELSE CONCAT('PUSKESMAS ',puskesmas_name)
END AS 'Nama Faskes',
CASE
WHEN kode_faskes='rs' THEN rs_kabupaten
ELSE puskesmas_kabupaten
END AS 'Kabupaten Faskes'
FROM
view_detail_campak
WHERE year(tanggal_timbul_rash)='2016'
AND
(rs_id_provinsi='34' OR puskesmas_id_provinsi='34')

-- query check user input
SELECT
CASE
WHEN kode_faskes='rs' THEN rs_nama_faskes
ELSE CONCAT('PUSKESMAS ',puskesmas_name)
END AS 'Nama Faskes',
CASE
WHEN kode_faskes='rs' THEN rs_kabupaten
ELSE puskesmas_kabupaten
END AS 'Kabupaten Faskes',
a.no_epid AS 'No Epid',
a.pasien_nama AS 'Nama pasien',
a.pasien_umur AS 'Tahun',
a.pasien_umur_bln AS 'Bulan',
a.pasien_umur_hr AS 'Hari',
a.alamat_new AS 'Alamat pasien',
a.pasien_kabupaten AS 'Kabupaten pasien',
CONCAT(b.first_name,' ',b.last_name) AS 'User penginput',
a.tanggal_timbul_rash AS 'Tanggal Rash',
b.no_telp AS 'NoTelp',
b.instansi AS 'Instansi',
a.tanggal_input AS 'Tanggal Input',
a.tanggal_update AS 'Tanggal Update'
FROM view_detail_campak AS a
JOIN users AS b ON a.created_by=b.id

SELECT
CASE
WHEN crs_kode_faskes='rs' THEN rs_nama_faskes
ELSE CONCAT('PUSKESMAS ',puskesmas_name)
END AS 'Nama Faskes',
CASE
WHEN crs_kode_faskes='rs' THEN rs_kabupaten
ELSE a.puskesmas_kabupaten
END AS 'Kabupaten Faskes',
a.crs_no_epid AS 'No Epid',
a.pasien_nama_anak AS 'Nama pasien',
a.pasien_umur AS 'Tahun',
a.pasien_umur_bln AS 'Bulan',
a.pasien_umur_hr AS 'Hari',
a.alamat_new AS 'Alamat pasien',
a.pasien_kabupaten AS 'Kabupaten pasien',
CONCAT(b.first_name,' ',b.last_name) AS 'User penginput',
a.crs_tanggal_periksa AS 'Tanggal periksa',
b.no_telp AS 'NoTelp',
b.instansi AS 'Instansi',
a.tanggal_input AS 'Tanggal Input',
a.tanggal_update AS 'Tanggal Update',
a.*
FROM getdetailcrs AS a
JOIN users AS b ON a.created_by=b.id
