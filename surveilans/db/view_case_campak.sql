SELECT
a.id_campak,
c.pasien_nama AS pasien_nama_anak,
c.pasien_nik,
c.pasien_nama_ortu,
c.pasien_jenis_kelamin,
c.pasien_tgl_lahir,
c.pasien_umur,
c.pasien_umur_bln,
c.pasien_umur_hr,
c.pasien_alamat,
c.pasien_provinsi,
c.pasien_kabupaten,
c.pasien_kecamatan,
c.pasien_kelurahan,
c.pasien_id_kabupaten,
c.pasien_id_provinsi,
a.no_epid AS campak_no_epid,
a.no_epid_lama AS campak_no_epid_lama,
a.no_epid_klb AS campak_no_epid_klb,
a.tanggal_imunisasi_terakhir AS campak_tanggal_imunisasi_terakhir,
CASE
WHEN a.vaksin_campak_sebelum_sakit = '7' THEN 'Tidak'
WHEN a.vaksin_campak_sebelum_sakit = '8' THEN 'Tidak tahu'
WHEN a.vaksin_campak_sebelum_sakit = '9' THEN 'Belum pernah'
WHEN a.vaksin_campak_sebelum_sakit = '' OR a.vaksin_campak_sebelum_sakit='0' THEN '-'
ELSE CONCAT(a.vaksin_campak_sebelum_sakit,' X')
END AS vaksin_campak_sebelum_sakit,
a.tanggal_timbul_demam AS campak_tanggal_timbul_demam,
a.tanggal_timbul_rash campak_tanggal_timbul_rash,
a.tanggal_laporan_diterima AS campak_tanggal_laporan_diterima,
a.tanggal_pelacakan,
CASE
WHEN a.vitamin_A = '1' THEN 'Ya'
WHEN a.vitamin_A = '2' THEN 'Tidak'
ELSE '-'
END AS vitamin_a_txt,
CASE
WHEN a.keadaan_akhir = '1' THEN 'Hidup'
WHEN a.keadaan_akhir = '2' THEN 'Meninggal'
ELSE '-'
END AS keadaan_akhir_txt,
CASE
WHEN a.jenis_kasus = '1' THEN 'KLB'
WHEN a.jenis_kasus = '2' THEN 'Bukan KLB'
ELSE '-'
END AS jenis_kasus_txt,
a.klb_ke,
CASE
WHEN a.status_kasus = '1' THEN 'Index'
WHEN a.status_kasus = '2' THEN 'Bukan Index'
ELSE '-'
END AS status_kasus_txt,
CASE
WHEN a.kode_faskes='puskesmas' THEN CONCAT('PUSKESMAS ',d.puskesmas_name)
WHEN a.kode_faskes='rs' THEN CONCAT('RUMAH SAKIT ',e.rs_nama_faskes)
ELSE a.nama_puskesmas
END AS campak_nama_puskesmas,
CASE
WHEN a.kode_faskes='puskesmas' THEN d.puskesmas_code_faskes
WHEN a.kode_faskes='rs' THEN e.rs_kode_faskes
ELSE ''
END AS code_faskes,
CASE
WHEN a.kode_faskes='puskesmas' THEN d.puskesmas_kecamatan
WHEN a.kode_faskes='rs' THEN e.rs_kecamatan
ELSE ''
END AS kecamatan_faskes,
CASE
WHEN a.kode_faskes='puskesmas' THEN d.puskesmas_kabupaten
WHEN a.kode_faskes='rs' THEN e.rs_kabupaten
ELSE ''
END AS kabupaten_faskes,
CASE
WHEN a.kode_faskes='puskesmas' THEN d.puskesmas_id_kabupaten
WHEN a.kode_faskes='rs' THEN e.rs_id_kabupaten
ELSE ''
END AS code_kabupaten_faskes,
CASE
WHEN a.kode_faskes='puskesmas' THEN d.puskesmas_provinsi
WHEN a.kode_faskes='rs' THEN e.rs_provinsi
ELSE ''
END AS provinsi_faskes,
CASE
WHEN a.kode_faskes='puskesmas' THEN d.puskesmas_id_provinsi
WHEN a.kode_faskes='rs' THEN e.rs_id_provinsi
ELSE ''
END AS code_provinsi_faskes,
CASE
WHEN a.klasifikasi_final='1' THEN 'Campak (Lab)'
WHEN a.klasifikasi_final='2' THEN 'Campak (Epid)'
WHEN a.klasifikasi_final='3' THEN 'Campak (Klinis)'
WHEN a.klasifikasi_final='4' THEN 'Rubella'
WHEN a.klasifikasi_final='5' THEN 'Bukan campak/rubella'
WHEN a.klasifikasi_final='6' THEN 'Pending'
ELSE ''
END AS campak_klasifikasi_final,
f.waktu_pengobatan AS pe_waktu_pengobatan,
f.tempat_pengobatan AS pe_tempat_pengobatan,
f.obat_yang_diberikan AS pe_obat_yang_diberikan,
f.dirumah_orang_sakit_sama AS pe_dirumah_orang_sakit_sama,
f.kapan_sakit_dirumah AS pe_kapan_sakit_dirumah,
f.disekolah_orang_sakit_sama AS pe_disekolah_orang_sakit_sama,
f.kapan_sakit_disekolah AS pe_kapan_sakit_disekolah,
CASE
WHEN f.kekurangan_gizi = '1' THEN 'Ya'
WHEN f.kekurangan_gizi = '2' THEN 'Tidak'
ELSE ''
END AS pe_kekurangan_gizi,
f.alamat_tinggal_sementara AS pe_alamat_tinggal_sementara,
f.tgl_pe_dialamat_sementara AS pe_tgl_pe_dialamat_sementara,
f.jumlah_kasus_dialamat_sementara AS pe_jumlah_kasus_dialamat_sementara,
f.alamat_tempat_tinggal AS pe_alamat_tempat_tinggal,
f.tgl_pe_alamat_tempat_tinggal AS pe_tgl_pe_alamat_tempat_tinggal,
f.jumlah_kasus_dialamat_tempat_tinggal AS pe_jumlah_kasus_dialamat_tempat_tinggal,
f.sekolah AS pe_sekolah,
f.tgl_pe_sekolah AS pe_tgl_pe_sekolah,
f.jumlah_kasus_disekolah AS pe_jumlah_kasus_disekolah,
f.tempat_kerja AS pe_tempat_kerja,
f.tgl_pe_tempat_kerja AS pe_tgl_pe_tempat_kerja,
f.jumlah_kasus_tempat_kerja AS pe_jumlah_kasus_tempat_kerja,
f.lain_lain AS pe_lain_lain,
f.tgl_pe_lain_lain AS pe_tgl_pe_lain_lain,
f.jumlah_kasus_lain_lain AS pe_jumlah_kasus_lain_lain,
f.jumlah_total_kasus_tambahan AS pe_jumlah_total_kasus_tambahan,
f.tanggal_penyelidikan AS pe_tanggal_penyelidikan,
f.pelaksana AS pe_pelaksana
FROM campak AS a
JOIN hasil_uji_lab_campak AS b ON a.id_campak=b.id_campak
JOIN view_pasien AS c ON b.id_pasien=c.pasien_id
LEFT JOIN view_puskesmas AS d ON a.id_tempat_periksa=d.puskesmas_id
LEFT JOIN view_rumahsakit AS e ON a.id_tempat_periksa=e.rs_id
LEFT JOIN pe_campak AS f ON f.id=a.id_pe_campak
WHERE a.deleted_at IS NULL