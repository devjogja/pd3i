SELECT
	pe_tetanus.id AS id,
	pe_tetanus.no_epid AS no_epid,
	pasien.nama_anak AS nama_anak,
	pasien.nama_ortu AS nama_ortu,
	concat_ws(
		',',
		pasien.alamat,
		located.kelurahan,
		located.kecamatan,
		located.kabupaten,
		located.provinsi
	) AS alamat,
	pasien.tanggal_lahir AS tanggal_lahir,
	pasien.umur AS umur,
	pasien.jenis_kelamin AS jenis_kelamin
FROM
located JOIN pasien JOIN pe_tetanus
WHERE
pasien.id_kelurahan = located.id_kelurahan
AND pasien.id_pasien = pe_tetanus.id_pasien
