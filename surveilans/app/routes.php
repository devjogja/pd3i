<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
 */
Route::get('pas', function () {

    return Hash::make('admin');
});

Route::get('getMaps', 'CobaController@getMaps');
Route::get('hitung_kasus', function () {

    return Input::get('id1') + Input::get('id2') + Input::get('id3') + Input::get('id4') + Input::get('id5');
});
Route::get('crs/sync', 'CrsController@sync');
Route::get('campak/sync', 'CampakController@sync');

Route::get('/', 'AdminController@getLogin');
// logout
Route::get('logout', 'AdminController@logout');
//route ke beranda
Route::get('dashboard', 'BerandaController@getIndex');
Route::get('removeDataLatihan', 'HomeController@removeDataLatihan');
Route::get('generateJenisKelaminCrs', 'CrsController@generateJenisKelaminCrs');
Route::get('generateJenisKelaminTetanus', 'CrsController@generateJenisKelaminTetanus');

//Master Modullaboratorium
Route::get('modullaboratorium', 'ModullaboratoriumController@getIndex');
Route::post('modullaboratorium_list', 'ModullaboratoriumController@getList');
Route::post('modullaboratorium/process_form', 'ModullaboratoriumController@ModullaboratoriumProcessForm');
Route::post('modullaboratorium/get_data_by_id', 'ModullaboratoriumController@ModullaboratoriumGetDataById');
Route::post('modullaboratorium/delete_list', 'ModullaboratoriumController@ModullaboratoriumDeleteList');
Route::get('modullaboratorium_list_get', 'ModullaboratoriumController@getListGet');

//Master Modulprovinsi
Route::get('modulprovinsi', 'ModulprovinsiController@getIndex');
Route::post('modulprovinsi_list', 'ModulprovinsiController@getList');
Route::post('modulprovinsi/process_form', 'ModulprovinsiController@ModulprovinsiProcessForm');
Route::post('modulprovinsi/get_data_by_id', 'ModulprovinsiController@ModulprovinsiGetDataById');
Route::post('modulprovinsi/delete_list', 'ModulprovinsiController@ModulprovinsiDeleteList');
Route::get('modulprovinsi_list_get', 'ModulprovinsiController@getListGet');

//Master Modulkabupaten
Route::get('modulkabupaten', 'ModulkabupatenController@getIndex');
Route::post('modulkabupaten_list', 'ModulkabupatenController@getList');
Route::post('modulkabupaten/process_form', 'ModulkabupatenController@ModulkabupatenProcessForm');
Route::post('modulkabupaten/get_data_by_id', 'ModulkabupatenController@ModulkabupatenGetDataById');
Route::post('modulkabupaten/delete_list', 'ModulkabupatenController@ModulkabupatenDeleteList');
Route::get('modulkabupaten_list_get', 'ModulkabupatenController@getListGet');

//Master Modulkecamatan
Route::get('modulkecamatan', 'ModulkecamatanController@getIndex');
Route::post('modulkecamatan_list', 'ModulkecamatanController@getList');
Route::post('modulkecamatan/process_form', 'ModulkecamatanController@ModulkecamatanProcessForm');
Route::post('modulkecamatan/get_data_by_id', 'ModulkecamatanController@ModulkecamatanGetDataById');
Route::post('modulkecamatan/delete_list', 'ModulkecamatanController@ModulkecamatanDeleteList');
Route::get('modulkecamatan_list_get', 'ModulkecamatanController@getListGet');

//Master Modulkelurahan
Route::get('modulkelurahan', 'ModulkelurahanController@getIndex');
Route::post('modulkelurahan_list', 'ModulkelurahanController@getList');
Route::post('modulkelurahan/process_form', 'ModulkelurahanController@ModulkelurahanProcessForm');
Route::post('modulkelurahan/get_data_by_id', 'ModulkelurahanController@ModulkelurahanGetDataById');
Route::post('modulkelurahan/delete_list', 'ModulkelurahanController@ModulkelurahanDeleteList');
Route::get('modulkelurahan_list_get', 'ModulkelurahanController@getListGet');

//Master Modulrumahsakit
Route::get('modulrumahsakit', 'ModulrumahsakitController@getIndex');
Route::post('modulrumahsakit_list', 'ModulrumahsakitController@getList');
Route::post('modulrumahsakit/process_form', 'ModulrumahsakitController@ModulrumahsakitProcessForm');
Route::post('modulrumahsakit/get_data_by_id', 'ModulrumahsakitController@ModulrumahsakitGetDataById');
Route::post('modulrumahsakit/delete_list', 'ModulrumahsakitController@ModulrumahsakitDeleteList');
Route::get('modulrumahsakit_list_get', 'ModulrumahsakitController@getListGet');
Route::get('modulrumahsakit_updatewilayah', 'RumahsakitController@updateWilayah');

//Master Modulpuskesmas
Route::get('modulpuskesmas', 'ModulpuskesmasController@getIndex');
Route::post('modulpuskesmas_list', 'ModulpuskesmasController@getList');
Route::post('modulpuskesmas/process_form', 'ModulpuskesmasController@ModulpuskesmasProcessForm');
Route::post('modulpuskesmas/get_data_by_id', 'ModulpuskesmasController@ModulpuskesmasGetDataById');
Route::post('modulpuskesmas/delete_list', 'ModulpuskesmasController@ModulpuskesmasDeleteList');
Route::get('modulpuskesmas_list_get', 'ModulpuskesmasController@getListGet');
Route::post('puskesmas/list', 'ModulpuskesmasController@getPuskesmas');

//Master Modulwilayahkerjapuskesmas
Route::get('modulwilayahkerjapuskesmas', 'ModulwilayahkerjapuskesmasController@getIndex');
Route::post('modulwilayahkerjapuskesmas_list', 'ModulwilayahkerjapuskesmasController@getList');
Route::post('modulwilayahkerjapuskesmas/process_form', 'ModulwilayahkerjapuskesmasController@ModulwilayahkerjapuskesmasProcessForm');
Route::post('modulwilayahkerjapuskesmas/get_data_by_id', 'ModulwilayahkerjapuskesmasController@ModulwilayahkerjapuskesmasGetDataById');
Route::post('modulwilayahkerjapuskesmas/delete_list', 'ModulwilayahkerjapuskesmasController@ModulwilayahkerjapuskesmasDeleteList');
Route::get('modulwilayahkerjapuskesmas_list_get', 'ModulwilayahkerjapuskesmasController@getListGet');

//route ke halaman manajemen admin
Route::get('admin', 'AdminController@getIndex');
Route::post('login', 'AdminController@login');
//signup
Route::get('signup', 'AdminController@signup');
Route::get('/excel', 'DatabaseController@Importexcel');
//route ke index laboratorium
Route::get('laboratorium', 'LaboratoriumController@getIndex');
Route::get('listlaboratorium', 'LaboratoriumController@getListlab');
Route::get('laboratorium/create', 'LaboratoriumController@getCreate');
Route::get('laboratorium/edit/{id}', 'LaboratoriumController@getEdit');
Route::post('laboratorium/simpan', 'LaboratoriumController@postSimpan');
Route::post('laboratorium/update/{id}', 'LaboratoriumController@postUpdate');
Route::get('laboratorium/hapus/{id}', 'LaboratoriumController@getDestroy');
Route::get('laboratorium/ujilab', 'LaboratoriumController@getUjilab');
//Route::post('laboratorium/simpanspesimencampak','LaboratoriumController@postSimpanHasilLabCampak');
Route::post('laboratorium/simpanspesimendifteri', 'LaboratoriumController@postSimpanHasilLabDifteri');
Route::post('laboratorium/simpanspesimenafp', array('as' => 'simpan_hasil_lab_afp', 'uses' => 'LaboratoriumController@postSimpanHasilLabAfp'));
Route::post('laboratorium/daftarakun', 'LaboratoriumController@postDaftarAkun');
Route::get('laboratorium/listakun', 'LaboratoriumController@getListAkun');
Route::get('laboratorium/createakun', 'LaboratoriumController@getForm');
Route::get('laboratorium/editspesimencampak/{id}', 'LaboratoriumController@editSpesimenCampak');
Route::get('laboratorium/editspesimendifteri/{id}', 'LaboratoriumController@editSpesimenDifteri');
Route::get('laboratorium/editspesimenafp/{id}', 'LaboratoriumController@editSpesimenAfp');
Route::post('laboratorium/simpanhasil', array('as' => 'simpan_hasil_lab_campak', 'uses' => 'LaboratoriumController@postSimpanHasilLabCampak'));
Route::get('laboratorium/cari_pasien_campak', array('as' => 'cari_pasien_campak', 'uses' => 'LaboratoriumController@getFindPasienCampak'));
Route::get('laboratorium/cari_pasien_afp', array('as' => 'cari_pasien_afp', 'uses' => 'LaboratoriumController@getFindPasienAfp'));
Route::get('input_hasil_lab/{id}', 'LaboratoriumController@getInputhasilLab');
Route::get('input_baru', 'LaboratoriumController@getInputbaru');
Route::post('input_baru', 'CampakController@postInputbaru');
Route::get('input_baru_afp', 'LaboratoriumController@getInputbaruafp');

Route::get('inputHasilLabCampak/{id}', 'LaboratoriumController@getInputhasilLabCampak');

Route::post('lab/storelab', 'LaboratoriumController@postStoreLab');
Route::get('labdetailCampak/{id}', 'LaboratoriumController@getLabDetail');
Route::get('labhapus/{id}', 'LaboratoriumController@getLabHapus');
Route::get('labsDelete/{penyakit}/{id}', 'LaboratoriumController@getDeleteLab');
Route::get('deleteLabCrs/{id}', 'LaboratoriumController@getLabCrsHapus');

Route::get('genExcel', 'CrsController@genExcel');

Route::post('storeLabCrs', 'LaboratoriumController@postStoreLabCrs');

//route ke pasien
Route::resource('pasien', 'PasienController');
Route::post('/pasien/cari', 'PasienController@postCari');

//find pasien
Route::post('search_pasien', 'LaboratoriumController@findPasien');

//konfirmasi
Route::get('kemenkescode', 'AdminController@konfirmkemenkes');
Route::get('konfirm/puskesmas/{id}', 'AdminController@konfirmpuskesmas');
Route::get('konfirm/rs/{id}', 'AdminController@konfirmrs');
Route::get('konfirm/kabupaten/{id}', 'AdminController@konfirmkab');
Route::get('konfirm/provinsi/{id}', 'AdminController@konfirmprov');

//route PE afp
Route::get('daftar_pe_afp', 'AfpController@getDaftarAfp');
Route::get('afp/entriafp/{id}', 'AfpController@getEntri_afp');
Route::post('afp/entri', array('as' => 'pe_afp', 'uses' => 'AfpController@postEpidAfp'));
Route::get('afp_detail/{id}', 'AfpController@detailafp');
Route::get('afp_edit/{id}', 'AfpController@getEditAfp');
Route::post('afp_update', 'AfpController@updateAfp');
Route::get('afp/delete/{id}', 'AfpController@delete');
Route::get('pe_afp_edit/{id}', 'AfpController@getEditPeAfp');
Route::get('pe_campak_edit/{id}', 'CampakController@getEditPeCampak');
Route::post('pe_afp_update', 'AfpController@postUpdatePeAfp');
Route::get('pe_afp_hapus/{id}', 'AfpController@getHapusPeAfp');
Route::get('afp_hapus/{id}', 'AfpController@getHapus');
Route::get('cetak_campak', 'CampakController@getCetakCampak');
Route::get('daftar_pe_campak', 'CampakController@getDaftarPeCampak');
Route::get('campak/entricampak/{id}', 'CampakController@getEntri_campak');
Route::post('campak/entri', array('as' => 'pe_campak', 'uses' => 'CampakController@postEpidCampak'));
Route::post('update_pe_campak', 'CampakController@postUpdatePeCampak');

Route::get('daftar_pe_difteri', 'DifteriController@getDaftarDifteri');
Route::get('difteri/PEdifteri/{id}', 'DifteriController@getPEdifteri');
Route::post('difteri/entri', array('as' => 'pe_difteri', 'uses' => 'DifteriController@postEpidDifteri'));
Route::get('difteri_detail/{id}', 'DifteriController@detaildifteri');
Route::get('difteri_edit/{id}', 'DifteriController@getEditDifteri');
Route::post('difteri_update', 'DifteriController@updateDifteri');
Route::get('difteri_hapus/{id}', 'DifteriController@getHapus');
Route::get('pe_difteri_edit/{id}', 'DifteriController@getEditPeDifteri');
Route::post('afp_store', 'AfpController@store');

Route::get('daftar_pe_tetanus', 'TetanusController@getDaftarTetanus');
Route::get('tetanus/entritetanus/{id}', 'TetanusController@getEntri_tetanus');
Route::post('tetanus/entri', array('as' => 'pe_tetanus', 'uses' => 'TetanusController@postEpidTetanus'));
Route::get('pe_tetanus_detail/{id}', 'TetanusController@getDetailPeTetanus');
Route::get('pe_afp_detail/{id}', 'AfpController@getDetailPeAfp');
Route::get('pe_campak_detail/{id}', 'CampakController@getDetailPeCampak');
Route::get('pe_difteri_detail/{id}', 'DifteriController@getDetailPeDifteri');
//route ke tetanus
Route::resource('tetanus', 'TetanusController');
Route::get('tetanus_detail/{id}', 'TetanusController@detailtetanus');
Route::get('tetanus_edit/{id}', 'TetanusController@getEditTetanus');
Route::post('tetanus_update', 'TetanusController@updateTetanus');
Route::get('tetanus_hapus/{id}/{id_lab}', 'TetanusController@getHapus');
Route::get('pe_tetanus_edit/{id}', 'TetanusController@getEditPeTetanus');
Route::post('pe_update_tetanus', 'TetanusController@postEditPeTetanus');
Route::get('pe_tetanus_hapus/{id}', 'TetanusController@getHapusPeTetanus');
Route::get('pe_campak_hapus/{id}', 'CampakController@getHapusPeCampak');
Route::get('pe_difteri_hapus/{id}', 'DifteriController@getHapusPeDifteri');
Route::post('pe_update_difteri', 'DifteriController@postUpdatePeDifteri');

// route to crs
Route::resource('crs', 'CrsController');
Route::get('daftar_pe_crs', 'CrsController@getDaftarCrs');
Route::get('pe_crs_detail/{id}', 'CrsController@getDetailPeCrs');
Route::get('pe_crs_edit/{id}', 'CrsController@getEditPeCrs');
Route::get('pe_crs_hapus/{id}', 'CrsController@getHapusPeCrs');
Route::get('crs/entricrs/{id}', 'CrsController@getEntri_crs');
Route::get('crs_hapus/{id}', 'CrsController@getHapus');
Route::post('crs/entri', array('as' => 'pe_crs', 'uses' => 'CrsController@postEpidCrs'));
Route::get('crs_detail/{id}', 'CrsController@detailcrs');
Route::get('crs_edit/{id}', 'CrsController@getEditCrs');
Route::post('crs_update', 'CrsController@updateCrs');
Route::post('crs_store', 'CrsController@store');

//route ke difteri
Route::resource('difteri', 'DifteriController');
Route::post('difteri_store', 'DifteriController@store');
//route ke afp
Route::resource('afp', 'AfpController');
//route ke rumahsakit
Route::resource('rumahsakit', 'RumahsakitController');
Route::get('rumahsakit/hapus/{id}', 'RumahsakitController@destroy');
//route ke puskemas
Route::resource('puskesmas', 'PuskesmasController');
Route::get('puskesmas/hapus/{id}', 'PuskesmasController@destroy');
Route::get('puskesmas/listakun', 'PuskesmasController@listakun');
Route::get('puskesmas/createakun', 'PuskesmasController@createakun');
//route ke propinsi
Route::resource('provinsi', 'ProvinsiController');
Route::get('provinsi/listakun', 'ProvinsiController@getListAkun');
Route::post('provinsi/daftarakun', 'ProvinsiController@DaftarAkun');
Route::get('provinsi/createakun', 'ProvinsiController@Form');

//route ke propinsi
Route::get('listprovinsi', 'WilayahController@getListProvinsi');
//route ke kabupaten
Route::get('listkabupaten/{id_provinsi}', 'WilayahController@getListKabupaten');
//route ke kecamatan
Route::get('listkecamatan/{id_kabupaten}/{id_provinsi}', 'WilayahController@getListKecamatan');
//route ke desa
Route::get('listkelurahan/{id_kecamatan}/{id_kabupaten}/{id_provinsi}', 'WilayahController@getListKelurahan');

Route::get('getArea', 'WilayahController@getArea');

//load kabupaten
Route::post('/provinsi/getkab', 'ProvinsiController@Kabupaten');
//load kecamatan
Route::post('/provinsi/getkec', 'ProvinsiController@Kecamatan');
//load kelurahan
Route::post('/provinsi/getkel', 'ProvinsiController@Kelurahan');
//load Puskesmas
Route::post('/provinsi/getpus', 'ProvinsiController@Puskesmas');
//load rumahsakit
Route::post('/provinsi/getrs', 'ProvinsiController@Rumahsakit');
//route campak
Route::resource('campak', 'CampakController');
//Route::get('campak/index','CampakController@getIndex');
Route::get('/cetak/campak', 'CampakController@laporancampak');
Route::get('campak_detail/{id}', 'CampakController@detailcampak');
Route::get('campak_edit/{id}', 'CampakController@getEdit');
Route::get('campak_hapus/{id}', 'CampakController@getHapus');
Route::get('get_pasien', array('as' => 'getPasien', 'uses' => 'CampakController@pasien'));
Route::post('campak/epid', 'CampakController@epidCampak');
Route::post('campak_update', 'CampakController@update');
Route::post('campak_store', 'CampakController@store');
Route::get('data_campak', 'CampakController@getDataCampak');
Route::get('tampil_data_campak', 'CampakController@getTampilDataCampak');

//getform
Route::post("/dianosa/getform", 'CampakController@postForm');
Route::post("getDataLabCampak", 'LaboratoriumController@postDataLabCampak');

Route::get("lab/campak", "LaboratoriumController@labCampak");
Route::get("lab/difteri", "LaboratoriumController@labDifteri");
Route::get("lab/afp", "LaboratoriumController@labAFP");

Route::post("getDetailSampel", "LaboratoriumController@getDetailSampel");
Route::post('labs/get_analisa', 'LaboratoriumController@getAnalisa');

//laboratorium
Route::get("labs/{case}", "LaboratoriumController@labs");
Route::get("inputHasilLab/{penyakit}/{id}", "LaboratoriumController@inputHasilLab");
Route::post("getDataLabCrs", "LaboratoriumController@getDataLabCrs");

//route sub kabupaten
Route::resource('kabupaten', 'KabupatenController');
Route::post('kabupaten/daftarakun', 'KabupatenController@postDaftarAkun');

//route backup and restore database
Route::get('backup', 'DatabaseController@backup');
Route::get('restore', 'DatabaseController@restore');
Route::post('database/restore', 'DatabaseController@postRestore');

Route::get('region/get_district', 'WilayahController@getDistrict');
Route::get('region/get_sub_district', 'WilayahController@getSubDistrict');
Route::get('region/get_village', 'WilayahController@getVillage');
Route::get('region/get_puskesmas', 'WilayahController@getPuskesmas');
Route::get('region/get_rs', 'WilayahController@getRS');
Route::get('region/get_puskesmas_detail', 'WilayahController@getPuskesmasDetail');
Route::get('region/get_rs_detail', 'WilayahController@getRSDetail');
Route::get('region/get_laboratorium_detail', 'WilayahController@getLaboratoriumDetail');

Route::post('lab/get_analisa', 'LaboratoriumController@getAnalisa');
Route::post('region/get_chart_campak_js', 'WilayahController@postChartJS');
Route::post('region/get_chart_campak_maps', 'WilayahController@postChartMaps');

Route::post('save_signup', 'AdminController@postSaveSignup');
Route::post('save_level', 'AdminController@postSaveLevel');
Route::get('setting/profil', 'AdminController@getSettingProfil');
Route::post('save_profil_puskesmas', 'AdminController@postSaveProfilPuskesmas');
Route::post('save_profil_rs', 'AdminController@postSaveProfilRS');
Route::post('save_profil_kabupaten', 'AdminController@postSaveProfilKabupaten');
Route::post('save_profil_provinsi', 'AdminController@postSaveProfilProvinsi');
Route::post('save_profil_pusat', 'AdminController@postSaveProfilPusat');
Route::post('save_profil_laboratorium', 'AdminController@postSaveProfilLaboratorium');

Route::get('generateKodeKonfirm', 'AdminController@generateKodeKonfirm');
Route::get('signup', 'AdminController@getSignup');
Route::get('setting/add_profil', 'AdminController@getSettingAddprofil');

Route::get('setting/level', 'AdminController@getSettingLevel');
Route::post('save_signup', 'AdminController@postSaveSignup');
Route::post('save_level', 'AdminController@postSaveLevel');
Route::post('save_profil_puskesmas', 'AdminController@postSaveProfilPuskesmas');
Route::post('save_profil_kabupaten', 'AdminController@postSaveProfilKabupaten');
Route::post('save_profil_provinsi', 'AdminController@postSaveProfilProvinsi');
Route::post('save_profil_pusat', 'AdminController@postSaveProfilPusat');
Route::post('save_profil_laboratorium', 'AdminController@postSaveProfilLaboratorium');

Route::get('register_admin', 'AdminController@getRegisterAdmin');
Route::get('delete_profile', 'AdminController@getDeleteProfile');

Route::get('pilih_profil', 'AdminController@getPilihProfil');
Route::get('profil_id', 'WilayahController@getProfilID');

Route::get('get_js', 'AdminController@getJs');
Route::get('get_function', 'AdminController@getFunction');

Route::get('get_js_grafik_maps_nasional', 'AdminController@getJsGrafikMaps');
Route::get('get_js_grafik_wilayah_nasional', 'AdminController@getJsGrafikWilayah');
Route::get('get_js_grafik_klasifikasi_final_nasional', 'AdminController@getJsGrafikKlasifikasiFinal');
Route::get('get_js_grafik_waktu_nasional', 'AdminController@getJsGrafikWaktu');

Route::get('get_js_init_maps_instansi', 'BerandaController@getJsInitMaps');
Route::get('get_js_grafik_maps_instansi', 'BerandaController@getJsGrafikMaps');
Route::get('get_js_grafik_maps_alert_instansi', 'BerandaController@getJsGrafikMapsAlert');
Route::get('get_js_grafik_klasifikasi_final_instansi', 'BerandaController@getJsGrafikKlasifikasiFinal');

Route::get('edit_profil_user', 'BerandaController@getEditProfilUser');
Route::get('edit_profil_instansi', 'BerandaController@getEditProfilInstansi');
Route::get('daftar_petugas_surveilans', 'BerandaController@getDaftarPetugasSurveilans');
Route::post('save_edit_profil_user', 'BerandaController@postSaveEditProfilUser');
Route::post('save_edit_profil_puskesmas', 'BerandaController@postSaveEditProfilPuskesmas');
Route::post('save_edit_profil_rs', 'BerandaController@postSaveEditProfilRS');
Route::post('save_edit_profil_kabupaten', 'BerandaController@postSaveEditProfilKabupaten');
Route::post('save_edit_profil_provinsi', 'BerandaController@postSaveEditProfilProvinsi');
Route::post('save_edit_profil_pusat', 'BerandaController@postSaveEditProfilPusat');
Route::post('save_edit_profil_laboratorium', 'BerandaController@postSaveEditProfilLaboratorium');
Route::get('reset_password/{id}', 'BerandaController@getResetPassword');
Route::get('delete_user/{id}', 'BerandaController@getDeleteUser');
Route::get('list_user', 'BerandaController@getListUser');

Route::get('listlaboratorium', 'LaboratoriumController@getListlab');

Route::get('download/jenis_kelamin/excel', 'WilayahController@getCampakJenisKelaminExcel');
Route::get('download/waktu/excel', 'WilayahController@getCampakWaktuExcel');
Route::get('download/umur/excel', 'WilayahController@getCampakUmurExcel');
Route::get('download/imunisasi/excel', 'WilayahController@getCampakImunisasiExcel');
Route::get('download/klasifikasi_final/excel', 'WilayahController@getCampakKlasifikasiFinalExcel');
Route::get('download/wilayah/excel', 'WilayahController@getCampakWilayahExcel');

Route::get('download/jenis_kelamin/print', 'WilayahController@getCampakJenisKelaminPrint');
Route::get('download/waktu/print', 'WilayahController@getCampakWaktuPrint');
Route::get('download/umur/print', 'WilayahController@getCampakUmurPrint');
Route::get('download/imunisasi/print', 'WilayahController@getCampakImunisasiPrint');
Route::get('download/klasifikasi_final/print', 'WilayahController@getCampakKlasifikasiFinalPrint');
Route::get('download/wilayah/print', 'WilayahController@getCampakWilayahPrint');

//route untk mengarahkn fungsi untuk menghitung umur
Route::get('hitung/umur', 'CampakController@getUmur');
Route::get('generateEpidCampak', 'CampakController@generateEpid');
Route::get('generateEpidCampakKLB', 'CampakController@generateEpidKLB');
//route to get date of birth
ROute::get('hitung/tgl_lahir', 'DifteriController@getTgl_lahir');
Route::get('tampil_faskes', 'CampakController@getTampilFaskes');
ROute::get('hitung/umur_pasien', 'PasienController@getUmur');
ROute::get('hitung/tanggal_lahir', 'PasienController@getTglLahir');

Route::post('getNoSpesimen', 'LaboratoriumController@getNoSpesimen');
Route::post('cekPasien', 'PasienController@cekPasien');

//route untuk mengarahkan fungsi untk menentukan no epid secara otomatis ketika suatu wilayah kelurahan dipilih
Route::post('getEpidCrs', 'CrsController@getEpid');
Route::get('ambil_epid_klb', 'CampakController@getEpidKLB');
Route::get('ambil_epid_campak', 'CampakController@getEpid');
Route::get('ambil_epid_crs', 'CrsController@getEpid');
Route::get('ambil_epid_afp', 'AfpController@getEpid');
Route::get('ambil_epid_difteri', 'DifteriController@getEpid');
Route::get('ambil_epid_tetanus', 'TetanusController@getEpid');
Route::get('tambah_petugas_persalinan', 'TetanusController@getTambahPetugasPersalinan');
Route::get('edit_petugas_persalinan', 'TetanusController@getUpdatePetugasPersalinan');
Route::get('edit_petugas_kehamilan', 'TetanusController@getUpdatePetugasKehamilan');
Route::get('edit_pelacak', 'TetanusController@getUpdatePetugasPelacak');
Route::get('hapus_petugas_persalinan', 'TetanusController@getHapusPetugasPersalinan');
Route::get('hapus_petugas_kehamilan', 'TetanusController@getHapusPetugasKehamilan');
Route::get('hapus_pelacak', 'TetanusController@getHapusPetugasPelacak');
Route::get('petugas_kehamilan/{id}', 'TetanusController@getPetugasKehamilan');
Route::get('petugas_pelacak/{id}', 'TetanusController@getPetugasPelacak');
Route::get('tampil_edit_petugas_persalinan', 'TetanusController@getEditPetugasPersalinan');
Route::get('tampil_edit_petugas_kehamilan', 'TetanusController@getEditPetugasKehamilan');
Route::get('tampil_edit_petugas_pelacak', 'TetanusController@getEditPetugasPelacak');
Route::get('tambah_petugas_kehamilan', 'TetanusController@getTambahPetugasKehamilan');
Route::get('tambah_pelacak', 'TetanusController@getTambahPetugasPelacak');

Route::get('execute', 'CobaController@execute');

//Route untuk memilih jenis sampel pemeriksaan
Route::get('pilih_jenis_sampel', 'LaboratoriumController@getPilihJenisSampel');
//route simpan hasil lab
Route::post('hasil_lab', 'LaboratoriumController@posthasillab');
Route::post('submitujisampel', 'LaboratoriumController@postUjiSampel');
//tampilkan hasil uji spesimen
Route::get('get_hasil_uji_spesimen/{id_campak}', 'LaboratoriumController@getHasilUjiSpesimen');
//input detail pemeriksan
Route::get('input_detail_pemeriksaan/{no_epid}', 'LaboratoriumController@getInputDetailPemeriksaan');

Route::get('hapusUjispesimen', 'LaboratoriumController@getHapusUjispesimen');

Route::get('serologi', 'LaboratoriumController@InputSerologi');
Route::get('verologi', 'LaboratoriumController@InputVerologi');
Route::get('usermanual', 'AdminController@usermanual');

Route::get('deletesampel/{id}', 'LaboratoriumController@deletesampel');

// Export Tetanus ----------------------------------------------------------- //
Route::get('export/tetanus', array(
    'as'   => 'tetanus.all',
    'uses' => 'TetanusController@getAll',
));

Route::get('exportall/tetanus/{filetype}/{dt}', array(
    'as'   => 'tetanus.export',
    'uses' => 'TetanusController@ExportExcel',
));

Route::get('export/tetanus/{filetype}/{dt}', array(
    'as'   => 'tetanus.export',
    'uses' => 'TetanusController@ExportExcel',
));

// Export Campak ------------------------------------------------------------ //

Route::get('export/campak', array(
    'as'   => 'campak.all',
    'uses' => 'CampakController@getAll',
));

Route::get('export/campak/{filetype}/{dt}', array(
    'as'   => 'campak.export',
    'uses' => 'CampakController@ExportExcel',
));
Route::get('exportall/campak/{filetype}/{dt}', array(
    'as'   => 'campak.exportall',
    'uses' => 'CampakController@ExportAllExcel',
));

// Export Difteri ----------------------------------------------------------- //

Route::get('exportall/difteri/{filetype}/{dt}', array(
    'as'   => 'difteri.all',
    'uses' => 'DifteriController@ExportExcel',
));

Route::get('export/difteri', array(
    'as'   => 'difteri.all',
    'uses' => 'DifteriController@getAll',
));

Route::get('export/difteri/{filetype}/{dt}', array(
    'as'   => 'difteri.export',
    'uses' => 'DifteriController@ExportExcel',
));

// Export AFP --------------------------------------------------------------- //

Route::get('export/afp', array(
    'as'   => 'afp.all',
    'uses' => 'AfpController@getAll',
));

Route::get('export/afp/{filetype}/{dt}', array(
    'as'   => 'afp.export',
    'uses' => 'AfpController@ExportExcel',
));
Route::get('exportall/afp/{filetype}/{dt}', array(
    'as'   => 'afp.exportall',
    'uses' => 'AfpController@ExportAllExcel',
));

// Export CRS --------------------------------------------------------------- //
Route::get('exportall/crs/{filetype}/{dt}', array(
    'as'   => 'crs.export',
    'uses' => 'CrsController@ExportExcelAll',
));
Route::get('export/crs/{filetype}/{dt}', array(
    'as'   => 'crs.export',
    'uses' => 'CrsController@ExportExcelAll',
));

// Coba Relasi Wilayah

Route::get('provinsi', array(
    'uses' => 'WilayahController@Provinsi',
));

Route::get('kabupaten', array(
    'uses' => 'WilayahController@Kabupaten',
));

Route::get('kecamatan', array(
    'uses' => 'WilayahController@Kecamatan',
));

Route::get('kelurahan', array(
    'uses' => 'WilayahController@Kelurahan',
));

Route::get('daftar-petugas-surveilans', array(
    'as'   => 'users.list',
    'uses' => 'PuskesmasController@SurveilansOfficer',
));

// Filter daftar kasus ------------------------------------------------------ //
Route::post('afp', array(
    'as'   => 'afp',
    'uses' => 'AfpController@index',
));

Route::post('campak', array(
    'as'   => 'campak',
    'uses' => 'CampakController@index',
));
Route::post('difteri', array(
    'as'   => 'difteri',
    'uses' => 'DifteriController@index',
));
Route::post('crs', array(
    'as'   => 'crs',
    'uses' => 'CrsController@index',
));

Route::get('filter-daftar-kasus/tetanus', array(
    'as'   => 'tetanus.filter-daftar-kasus',
    'uses' => 'TetanusController@FilterDaftarKasus',
));

// Import from excel -------------------------------------------------------- //

/*Route::get('import/campak', array(
'as'   => 'campak.import',
'uses' => 'CampakController@ImportFromExcel'
));

Route::get('import/campak/extract', array(
'as'   => 'campak.extract',
'uses' => 'CampakController@ExtractExcel'
));*/

Route::get('import/campak', array(
    'as'   => 'campak.import',
    'uses' => 'ImportController@ImportCampak',
));

Route::post('campak.extract', array(
    'as'   => 'campak.extract',
    'uses' => 'ImportController@ExtractCampak',
));
Route::post('afp.extract', array(
    'as'   => 'afp.extract',
    'uses' => 'ImportController@ExtractAfp',
));
