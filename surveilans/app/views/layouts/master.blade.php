<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Surveilans Administrator</title>
    <link type="text/css" href="{{URL::to('style/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link type="text/css" href="{{URL::to('style/bootstrap/css/bootstrap-responsive.min.css')}}" rel="stylesheet">
    <link type="text/css" href="{{URL::to('style/css/theme.css')}}" rel="stylesheet">
    <link type="text/css" href="{{URL::to('style/css/suveilans.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{URL::to('asset/css/app.css')}}" type="text/css" cache="false"/>
    <link rel="stylesheet" href="{{URL::to('style/css/jquery.validation.css')}}" type="text/css" cache="false"/>
    <link type="text/css" href="{{URL::to('style/images/icons/css/font-awesome.css')}}" rel="stylesheet">
    <link type="text/css" href="{{URL::to('asset/css/jquery-ui.css')}}" rel="stylesheet">
    <link type="text/css" href="{{URL::to('asset/css/ibnu.css')}}" rel="stylesheet">
    <script src="{{URL::to('style/scripts/surveilan.js')}}" type="text/javascript"></script>
    <link rel="stylesheet" href="{{ asset('packages/uikit/css/addons/uikit.addons.css')}}">
    <link rel="stylesheet" href="{{ asset('packages/uikit/css/uikit.css')}}">
    <link rel="stylesheet" href="{{ asset('asset/select2/select2.css')}}">
    <link rel="stylesheet" href="{{ URL::to('packages/datatables/css/jquery.dataTables.css')}}" />
    <script src="{{URL::to('style/scripts/jquery-1.10.0.js')}}" type="text/javascript"></script>
    <script src="{{URL::to('style/scripts/jquery-ui-1.11.0.js')}}" type="text/javascript"></script>
    
    <script src="{{URL::to('style/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::to('packages/datatables/js/jquery.dataTables.js')}}" type="text/javascript"></script>
    {{ HTML::script('packages/uikit/js/uikit.js') }}
    {{ HTML::script('packages/uikit/js/addons/datepicker.js') }}
    {{ HTML::script('packages/uikit/js/addons/form-select.js') }}
    <script src="{{URL::to('style/scripts/jquery.form.js')}}" type="text/javascript"></script>
    <script src="{{URL::to('style/scripts/jquery.validate.js')}}" type="text/javascript"></script>
    <script src="{{URL::to('style/js/jquery.validation.js')}}" type="text/javascript"></script>
    <script src="{{URL::to('highcharts/js/highcharts.js')}}"></script>
    <script src="{{URL::to('highcharts/js/highcharts-3d.js')}}"></script>
    <script src="{{URL::to('highcharts/js/exporting.js')}}"></script>
    <script src="{{URL::to('googlemaps/maps.js')}}"></script>
    <script src="{{URL::to('googlemaps/maplabel.js')}}"></script>
    <script src="{{URL::to('googlemaps/maplabel-compiled.js')}}"></script>   
    <script src="{{URL::to('asset/select2/select2.js')}}"></script>
    <script src="{{URL::to('asset/js/html2canvas.js')}}"></script>
    <script type="text/javascript">
        var BASE_URL = {{ json_encode(url('/')) }}+'/';
        $(function(){
            $('select').select2();
        });
    </script>
    <style type="text/css">
       
        img {
            vertical-align: middle;
        }
        .modal-content {
            max-width: 800px;
        }
        .modal-body{
            max-width: 800px;
        }

        .img-responsive {
            display: block;
            height: auto;
            max-width: 100%;
        }

        .img-rounded {
            border-radius: 3px;
        }

        .img-thumbnail {
            background-color: #fff;
            border: 1px solid #ededf0;
            border-radius: 3px;
            display: inline-block;
            height: auto;
            line-height: 1.428571429;
            max-width: 100%;
            moz-transition: all .2s ease-in-out;
            o-transition: all .2s ease-in-out;
            padding: 2px;
            transition: all .2s ease-in-out;
            webkit-transition: all .2s ease-in-out;
        }

        .img-circle {
            border-radius: 50%;
        }

        .timeline-centered {
            position: relative;
            margin-bottom: 30px;
            padding-top: 20px;
        }

        .timeline-centered:before, .timeline-centered:after {
            content: " ";
            display: table;
        }

        .timeline-centered:after {
            clear: both;
        }

        .timeline-centered:before, .timeline-centered:after {
            content: " ";
            display: table;
        }

        .timeline-centered:after {
            clear: both;
        }

        .timeline-centered:before {
            content: '';
            position: absolute;
            display: block;
            width: 4px;
            background: #f5f5f6;
            /*left: 50%;*/
            top: 20px;
            bottom: 20px;
            margin-left: 30px;
        }

        .timeline-centered .timeline-entry {
            position: relative;
        /*width: 50%;
        float: right;*/
        margin-top: 5px;
        margin-left: 30px;
        margin-bottom: 10px;
        clear: both;
    }

    .timeline-centered .timeline-entry:before, .timeline-centered .timeline-entry:after {
        content: " ";
        display: table;
    }

    .timeline-centered .timeline-entry:after {
        clear: both;
    }

    .timeline-centered .timeline-entry:before, .timeline-centered .timeline-entry:after {
        content: " ";
        display: table;
    }

    .timeline-centered .timeline-entry:after {
        clear: both;
    }

    .timeline-centered .timeline-entry.begin {
        margin-bottom: 0;
    }

    .timeline-centered .timeline-entry.left-aligned {
        float: left;
    }

    .timeline-centered .timeline-entry.left-aligned .timeline-entry-inner {
        margin-left: 0;
        margin-right: -18px;
    }

    .timeline-centered .timeline-entry.left-aligned .timeline-entry-inner .timeline-time {
        left: auto;
        right: -100px;
        text-align: left;
    }

    .timeline-centered .timeline-entry.left-aligned .timeline-entry-inner .timeline-icon {
        float: right;
    }

    .timeline-centered .timeline-entry.left-aligned .timeline-entry-inner .timeline-label {
        margin-left: 0;
        margin-right: 70px;
    }

    .timeline-centered .timeline-entry.left-aligned .timeline-entry-inner .timeline-label:after {
        left: auto;
        right: 0;
        margin-left: 0;
        margin-right: -9px;
        -moz-transform: rotate(180deg);
        -o-transform: rotate(180deg);
        -webkit-transform: rotate(180deg);
        -ms-transform: rotate(180deg);
        transform: rotate(180deg);
    }

    .timeline-centered .timeline-entry .timeline-entry-inner {
        position: relative;
        margin-left: -20px;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner:before, .timeline-centered .timeline-entry .timeline-entry-inner:after {
        content: " ";
        display: table;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner:after {
        clear: both;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner:before, .timeline-centered .timeline-entry .timeline-entry-inner:after {
        content: " ";
        display: table;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner:after {
        clear: both;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-time {
        position: absolute;
        left: -100px;
        text-align: right;
        padding: 10px;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-time > span {
        display: block;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-time > span:first-child {
        font-size: 15px;
        font-weight: bold;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-time > span:last-child {
        font-size: 12px;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-icon {
        background: #fff;
        color: #737881;
        display: block;
        width: 40px;
        height: 40px;
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding;
        background-clip: padding-box;
        -webkit-border-radius: 20px;
        -moz-border-radius: 20px;
        border-radius: 20px;
        text-align: center;
        -moz-box-shadow: 0 0 0 5px #f5f5f6;
        -webkit-box-shadow: 0 0 0 5px #f5f5f6;
        box-shadow: 0 0 0 5px #f5f5f6;
        line-height: 40px;
        font-size: 15px;
        float: left;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-icon.bg-primary {
        background-color: #303641;
        color: #fff;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-icon.bg-secondary {
        background-color: #ee4749;
        color: #fff;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-icon.bg-success {
        background-color: #00a651;
        color: #fff;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-icon.bg-info {
        background-color: #21a9e1;
        color: #fff;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-icon.bg-warning {
        background-color: #fad839;
        color: #fff;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-icon.bg-danger {
        background-color: #cc2424;
        color: #fff;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-label {
        position: relative;
        background: #f5f5f6;
        padding: 1em;
        margin-left: 60px;
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding;
        background-clip: padding-box;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-label:after {
        content: '';
        display: block;
        position: absolute;
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 9px 9px 9px 0;
        border-color: transparent #f5f5f6 transparent transparent;
        left: 0;
        top: 10px;
        margin-left: -9px;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-label h2, .timeline-centered .timeline-entry .timeline-entry-inner .timeline-label p {
        color: #737881;
        font-family: "Noto Sans",sans-serif;
        font-size: 12px;
        margin: 0;
        line-height: 1.428571429;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-label p + p {
        margin-top: 15px;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-label h2 {
        font-size: 16px;
        margin-bottom: 10px;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-label h2 a {
        color: #303641;
    }

    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-label h2 span {
        -webkit-opacity: .6;
        -moz-opacity: .6;
        opacity: .6;
        -ms-filter: alpha(opacity=60);
        filter: alpha(opacity=60);
    }

    /* Tabs panel */
    .tabbable-panel {
      border:1px solid #eee;
      padding: 10px;
  }

  /* Default mode */
  .tabbable-line > .nav-tabs {
      border: none;
      margin: 0px;
  }
  .tabbable-line > .nav-tabs > li {
      margin-right: 2px;
  }
  .tabbable-line > .nav-tabs > li > a {
      border: 0;
      margin-right: 0;
      color: #737373;
  }
  .tabbable-line > .nav-tabs > li > a > i {
      color: #a6a6a6;
  }
  .tabbable-line > .nav-tabs > li.open, .tabbable-line > .nav-tabs > li:hover {
      border-bottom: 4px solid #fbcdcf;
  }
  .tabbable-line > .nav-tabs > li.open > a, .tabbable-line > .nav-tabs > li:hover > a {
      border: 0;
      background: none !important;
      color: #333333;
  }
  .tabbable-line > .nav-tabs > li.open > a > i, .tabbable-line > .nav-tabs > li:hover > a > i {
      color: #a6a6a6;
  }
  .tabbable-line > .nav-tabs > li.open .dropdown-menu, .tabbable-line > .nav-tabs > li:hover .dropdown-menu {
      margin-top: 0px;
  }
  .tabbable-line > .nav-tabs > li.active {
      border-bottom: 4px solid #f3565d;
      position: relative;
  }
  .tabbable-line > .nav-tabs > li.active > a {
      border: 0;
      color: #333333;
  }
  .tabbable-line > .nav-tabs > li.active > a > i {
      color: #404040;
  }
  .tabbable-line > .tab-content {
      
      background-color: #fff;
      border: 0;
      border-top: 1px solid #eee;
      padding: 15px 0;
  }
  .portlet .tabbable-line > .tab-content {
      padding-bottom: 0;
  }


</style>
</head>
<body>
    @include('admin.heading')
    <!-- /navbar -->
    <div class="wrapper">
        <div class="container">
            <div class="row">
                @yield('content')
                <!--/.span9-->
            </div>
        </div>
        <!--/.container-->
    </div>
    <!--/.wrapper-->
    <div class="footer navbar-fixed-bottom">
        <div class="container">   </div>
    </div>
    @include('global.globaljs')
</body>
</html>
