@extends('layouts.master')
@section('content')	
	<script language="JavaScript" type="text/javascript">
		function disableForm() {
			$('input, select, button, textarea').attr('disabled','disabled');
		}
		function enableForm() {
			$('input, select, button, textarea').removeAttr('disabled');
		}
	</script>

	<script language="JavaScript" type="text/javascript">
		var allIsChecked = false;
		
		$(document).ready(function() {
			

		$('#closeSmallSearch').click(function(){
			$("#div_search").slideUp("fast", function(){$('#id').focus()});return false;
		});

		//get List Data
		//ajaxing search form
		$('#frmSearch').submit(function() {
			$('#result').html("<img src='{{URL::to('asset/images/ajax-loader-green.gif')}}' height='15px' /> <font color='red'>Loading...</font>");
			$.ajax({
				url: "modulkecamatan_list_get",
				data: "search_name="+$('#search_name').val()+"&page=1",
				success: function(jsonData) {
					$('#divSearchResult').html(jsonData);
					$('#result').html("Success !");
					prepareList();
				}
			});
			return false;
		});

		$('#frmInput').submit(function() {		
			$('#frmInput').ajaxSubmit({
				beforeSubmit:function() {
					disableForm();
					$('#result').html("<img src='{{URL::to('asset/images/ajax-loader-green.gif')}}' height='15px' /> <font color='red'>Loading...</font>");
				},
				dataType: 'json',
				success:function(data) {
					getList();
					reset();
					enableForm();
					$('#result').html(data.msg);
				}				
			});
			return false;
		});	

		$('#reset').click( function() {
			reset();
			$("#frmInput").slideUp("fast");
			return false;
		});
	
		$('#frmSearch').resetForm();
		$('#frmSearch').submit();
		$('#id').focus();
	});


	function get_data_by_id(idx) {
		getList();
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "modulkecamatan/get_data_by_id",
			data: "id="+idx,
			success: function(jsonData) {
				$('#saved_id').val(jsonData[0].kecamatan);
	$('#id_kecamatan').val(jsonData[0].id_kecamatan);
$('#id_kabupaten').val(jsonData[0].id_kabupaten);
$('#kecamatan').val(jsonData[0].kecamatan);

	$('#frmInput').slideDown('fast', function() {
		$('#name').focus();
	});
	
			}
		});
	} 

	function reloadingData(){
		$('#result').html("<img src='{{URL::to('asset/images/ajax-loader-green.gif')}}' height='15px' /> <font color='red'>Loading...</font>");
		$.ajax({
			url: "modulkecamatan_list_get",
			data: "search_name="+$('#search_name').val()+"&page="+$('#current_page').val(),
			success: function(jsonData) {
				$('#divSearchResult').html(jsonData);
				$('#result').html("Success !");
				prepareList();
			}
		});
	}

	function prepareList() {
		$('#pagingLinks a').click(function() {
			$('#result').html("<img src='{{URL::to('asset/images/ajax-loader-green.gif')}}' height='15px' /> <font color='red'>Loading...</font>");
			$(this).removeClass().addClass('buttonReloadIsLoad');
			
			$.ajax({
				url: this.href,
				data: "search_name="+$('#current_search').val(),
				success: function(jsonData) {
					$('#divSearchResult').html(jsonData);
					prepareList();
					$('#result').html("Success !");
				}
			});
		   
		   return false;
        });

		// select or unselect
		$('#tbodySearchResult tr').click(function(){
			var checkbox = $(this).children().eq(0).children();
			if(checkbox.prop('checked') == false) {
				checkbox.prop('checked', true);
				//$(this).removeClass('notSelected').addClass('bg_type4');
				$(this).css( "background", "#ffa" );
			} else {
				checkbox.prop('checked', false);
				//$(this).removeClass().addClass('notSelected');
				$(this).css( "background", "" );
			}
		});

		$('#reloadData').click(function(){
			getList();
			prepareList();
			return false;
		});
		
		$('#addData').click(function() {
			$('#frmInput').slideDown('fast');
			reset();
			
		});
		$('#findData').click(function() {
			var ofs = $("#findData").offset();
			$("#div_search")
			.css('left', ofs.left+20)
			.css('position', 'absolute')
			.slideDown('fast');
			$('#search_name').focus();
		});
		
	}
		
	function deleteList() {
		$('#reloadData').ajaxStart(function(){
			$('#result').html("Loading...");
		});
		
		$('#reloadData').ajaxStop(function(){
		});
		
		$('#frmList').ajaxSubmit({
			beforeSubmit:confirmDelete,
			type: 'POST',
			dataType: 'json',
			success:function(data) {
				$('#result').html(data.msg);
				if(data.status == 'success') {
					getList();
				} else $("#name").focus();
			}
		});
		return false;
	}
	
	function confirmDelete(formData, jqForm, opt) {
		var v = $('.checkbox_delete').fieldValue();
		if(v == 0 || v == 'undefined') {alert('Choose min 1 data!');return false;}
		return confirm('Delete '+ v.length +' data?');
	}
	
	function getList() {
		reloadingData();
		return false;
	}
	
	function selectAll() {
		if (allIsChecked == false) {
			$('#selectAll').removeClass('buttonSelectAll').addClass('buttonDeselectAll').html('<i class="icon-check"></i> Deselect All');
			$("#tbodySearchResult .checkbox_delete").prop("checked", true);
			//$('#tbodySearchResult tr').removeClass().addClass('selected');
			//$('#tbodySearchResult tr').removeClass("notSelected").addClass('bg_type4');
			$('#tbodySearchResult tr').css("background", "#ffa");
			allIsChecked = true;
		} else {
			$('#selectAll').removeClass('buttonDeselectAll').addClass('buttonSelectAll').html('<i class="icon-check"></i> Select All');
			$("#tbodySearchResult .checkbox_delete").prop("checked", false);
			//$('#tbodySearchResult tr').removeClass("bg_type4").addClass('notSelected');
			//$( this ).css( "color", "red" );
			$('#tbodySearchResult tr').css("background","");
			allIsChecked = false;
		}
	}
	
	function reset(){
		$('#frmInput').clearForm();
		$('#saved_id').val('');
		$('#name').focus();
	}
</script>

<div class="span12">
<div class="content">
	<div class="module">
		<div id=""class="module-body" style="min-height:700px;">
			<div class="profile-head media">
				<h4>
					Data Modulkecamatan
				</h4>
			</div>
			<br>
			<form method="POST" name="frmInput" style="display:none;" id="frmInput" action="{{URL::to('modulkecamatan/process_form')}}">
				<input type="hidden" name="saved_id" id="saved_id" />
				<table cellpadding="0" cellspacing="0" border="0" class="uk-table uk-table-hover uk-table-striped uk-table-condensed table-bordered">
					
<tr>
	<td>id_kecamatan</td>
	<td>
		<input type="text" name="id_kecamatan" id="id_kecamatan" size="30" class="" />
	</td>
</tr>
	
<tr>
	<td>id_kabupaten</td>
	<td>
		<input type="text" name="id_kabupaten" id="id_kabupaten" size="30" class="" />
	</td>
</tr>
	
<tr>
	<td>kecamatan</td>
	<td>
		<input type="text" name="kecamatan" id="kecamatan" size="30" class="" />
	</td>
</tr>
	
					<tr>
						<td></td>
						<td>
							<div class="">
								<button type="submit" name="Simpan" id="simpan" class="btn btn-success"><i class="icon-ok"></i> Simpan</button>
								<button type="reset" name="Reset" id="reset" class="btn btn-success"><i class="icon-remove"></i> Batal</button>
							</div>
						</td>
					</tr>
				</table>
				
			</form>
			<div id="result" class="result"></div>
			<div class="smallSearchContainer bg_type2" id="div_search" style="display:none;">
				Search
				<button type="button" id="closeSmallSearch" class="btn btn-danger" style="float:right;"><i class="icon-remove"></i> </button>
				<div class="smallSearch">
					<form method="POST" name="frmSearch" id="frmSearch" action="{{URL::to('modulkecamatan_list')}}">
						<input type="text" name="search_name" id="search_name" value="" size="25" style="color:#000;;" />
					</form>
				</div>
			</div>

			<div id="divFormInputContainer"></div>

			<form method="POST" name="frmList" id="frmList" action="{{URL::to('modulkecamatan/delete_list')}}">
				<div id="divSearchResult"></div>
			</form>
			
<a href="{{URL::to('dashboard')}}" class="btn btn-warning">Kembali</a>
	</div>
</div>
</div>
</div>
@stop
