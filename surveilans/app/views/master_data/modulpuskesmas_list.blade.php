<input type="hidden" name="current_page" id="current_page" value="<?php echo $current_page; ?>" />
<input type="hidden" name="current_search" id="current_search" value="<?php echo $current_search; ?>" />
<div>
	<div class="btn btn-primary" id="addData"><i class="icon-plus"></i> Add</div>
	<div class="btn btn-primary" id="deleteList" onClick="deleteList();"><i class="icon-trash"></i> Delete</div>
	<div class="btn btn-primary" id="selectAll" onClick="selectAll();"><i class="icon-check"></i> Select All</div>
	<div class="btn btn-primary" id="reloadingData" onClick="reloadingData();"><i class="icon-refresh"></i> Reload</div>
	<div class="btn btn-primary" id="findData"><i class="icon-search"></i> Find</div>
	<div class="pagination pagination-sm" id="pagingLinks" style="margin:0px;float:right;height:60px;">
		{{$modulpuskesmas->links()}}
	</div>
</div>
<div class="clear"></div>
<div class="table-responsive">
	<table cellpadding="0" cellspacing="0" border="0" class="uk-table uk-table-hover uk-table-striped uk-table-condensed table-bordered"><!--class="tblListData"-->
		<thead>
			
<tr class="">
	<th style="display:none;"></th>
		
<th>puskesmas_id</th>
<th>puskesmas_name</th>
<th>puskesmas_code_faskes</th>
<th>alamat</th>
<th>kabupaten_name</th>
<th>konfirmasi</th>
<th>kode_desa</th>
<th>kode_kec</th>
<th>kode_kab</th>
<th>kode_prop</th>	
	<th class="span1">Aksi</th>
</tr>
		</thead>
		<tbody id="tbodySearchResult">
			@if($modulpuskesmas)
			@foreach($modulpuskesmas as $data)	
			
<tr class="">
	<td style="display:none;">
		<input type="checkbox" class="checkbox_delete" id="{{$data->puskesmas_id}}" name="delete_id[]" value="{{$data->puskesmas_id}}" />
	</td>
	
<td ondblclick="get_data_by_id('{{$data->puskesmas_id}}')" id="">{{$data->puskesmas_id}}</td>
<td ondblclick="get_data_by_id('{{$data->puskesmas_name}}')" id="">{{$data->puskesmas_name}}</td>
<td ondblclick="get_data_by_id('{{$data->puskesmas_code_faskes}}')" id="">{{$data->puskesmas_code_faskes}}</td>
<td ondblclick="get_data_by_id('{{$data->alamat}}')" id="">{{$data->alamat}}</td>
<td ondblclick="get_data_by_id('{{$data->kabupaten_name}}')" id="">{{$data->kabupaten_name}}</td>
<td ondblclick="get_data_by_id('{{$data->konfirmasi}}')" id="">{{$data->konfirmasi}}</td>
<td ondblclick="get_data_by_id('{{$data->kode_desa}}')" id="">{{$data->kode_desa}}</td>
<td ondblclick="get_data_by_id('{{$data->kode_kec}}')" id="">{{$data->kode_kec}}</td>
<td ondblclick="get_data_by_id('{{$data->kode_kab}}')" id="">{{$data->kode_kab}}</td>
<td ondblclick="get_data_by_id('{{$data->kode_prop}}')" id="">{{$data->kode_prop}}</td>
	<td onclick="get_data_by_id('{{$data->puskesmas_id}}')" id="">
		<button type="button" class="btn btn-success"><i class="icon-ok"></i>Edit</input>
	</td>
</tr>

			@endforeach
			@endif
		</tbody>
	</table>
</table>
