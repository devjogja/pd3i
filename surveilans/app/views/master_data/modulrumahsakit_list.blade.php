<input id="current_page" name="current_page" type="hidden" value="<?php echo $current_page; ?>"/>
<input id="current_search" name="current_search" type="hidden" value="<?php echo $current_search; ?>"/>
<div>
    <div class="btn btn-primary" id="addData">
        <i class="icon-plus">
        </i>
        Add
    </div>
    <div class="btn btn-primary" id="deleteList" onclick="deleteList();">
        <i class="icon-trash">
        </i>
        Delete
    </div>
    <div class="btn btn-primary" id="selectAll" onclick="selectAll();">
        <i class="icon-check">
        </i>
        Select All
    </div>
    <div class="btn btn-primary" id="reloadingData" onclick="reloadingData();">
        <i class="icon-refresh">
        </i>
        Reload
    </div>
    <div class="btn btn-primary" id="findData">
        <i class="icon-search">
        </i>
        Find
    </div>
    <div class="pagination pagination-sm" id="pagingLinks" style="margin:0px;float:right;height:60px;">
        {{$modulrumahsakit->links()}}
    </div>
</div>
<div class="clear">
</div>
<div class="table-responsive">
    <table border="0" cellpadding="0" cellspacing="0" class="uk-table uk-table-hover uk-table-striped uk-table-condensed table-bordered">
        <!--class="tblListData"-->
        <thead>
            <tr class="">
                <th style="display:none;">
                </th>
                <th>
                    id_rumah_sakit
                </th>
                <th>
                    nama_rumah_sakit
                </th>
                <th>
                    alamat
                </th>
                <th>
                    longitude
                </th>
                <th>
                    latitude
                </th>
                <th>
                    jenis_rumah_sakit
                </th>
                <th>
                    kabupaten
                </th>
                <th>
                    kelas_rumah_sakit
                </th>
                <th>
                    telepon
                </th>
                <th class="span1">
                    Aksi
                </th>
            </tr>
        </thead>
        <tbody id="tbodySearchResult">
        @if($modulrumahsakit)
	@foreach($modulrumahsakit as $data)
		    <tr class="">
		        <td style="display:none;">
		            <input class="checkbox_delete" id="{{$data->id}}" name="delete_id[]" type="checkbox" value="{{$data->id}}"/>
		        </td>
		        <td id="" ondblclick="get_data_by_id('{{$data->id}}')">
		            {{$data->kode_faskes}}
		        </td>
		        <td id="" ondblclick="get_data_by_id('{{$data->id}}')">
		            {{$data->nama_faskes}}
		        </td>
		        <td id="" ondblclick="get_data_by_id('{{$data->id}}')">
		            {{$data->alamat}}
		        </td>
		        <td id="" ondblclick="get_data_by_id('{{$data->id}}')">
		            {{$data->longitude}}
		        </td>
		        <td id="" ondblclick="get_data_by_id('{{$data->id}}')">
		            {{$data->latitude}}
		        </td>
		        <td id="" ondblclick="get_data_by_id('{{$data->id}}')">
		            {{$data->kepemilikan}}
		        </td>
		        <td id="" ondblclick="get_data_by_id('{{$data->id}}')">
		            {{$data->kabupaten}}
		        </td>
		        <td id="" ondblclick="get_data_by_id('{{$data->id}}')">
		            {{$data->tipe_faskes}}
		        </td>
		        <td id="" ondblclick="get_data_by_id('{{$data->id}}')">
		            {{$data->telp}}
		        </td>
		        <td id="" onclick="get_data_by_id('{{$data->id}}')">
		            <button class="btn btn-success" type="button">
		                <i class="icon-ok">
		                </i>
		                Edit
		            </button>
		        </td>
		    </tr>
	@endforeach
	@endif
        </tbody>
    </table>
</div>
