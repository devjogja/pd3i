@extends('layouts.master')
@section('content')
<script language="JavaScript" type="text/javascript">
    function disableForm() {
			$('input, select, button, textarea').attr('disabled','disabled');
		}
		function enableForm() {
			$('input, select, button, textarea').removeAttr('disabled');
		}
</script>
<script language="JavaScript" type="text/javascript">
    var allIsChecked = false;
		
		$(document).ready(function() {
			

		$('#closeSmallSearch').click(function(){
			$("#div_search").slideUp("fast", function(){$('#id').focus()});return false;
		});

		//get List Data
		//ajaxing search form
		$('#frmSearch').submit(function() {
			$('#result').html("<img src='{{URL::to('asset/images/ajax-loader-green.gif')}}' height='15px' /> <font color='red'>Loading...</font>");
			$.ajax({
				url: "modulrumahsakit_list_get",
				data: "search_name="+$('#search_name').val()+"&page=1",
				success: function(jsonData) {
					$('#divSearchResult').html(jsonData);
					$('#result').html("Success !");
					prepareList();
				}
			});
			return false;
		});

		$('#frmInput').submit(function() {		
			$('#frmInput').ajaxSubmit({
				beforeSubmit:function() {
					disableForm();
					$('#result').html("<img src='{{URL::to('asset/images/ajax-loader-green.gif')}}' height='15px' /> <font color='red'>Loading...</font>");
				},
				dataType: 'json',
				success:function(data) {
					getList();
					reset();
					enableForm();
					$('#result').html(data.msg);
				}				
			});
			return false;
		});	

		$('#reset').click( function() {
			reset();
			$("#frmInput").slideUp("fast");
			return false;
		});
	
		$('#frmSearch').resetForm();
		$('#frmSearch').submit();
		$('#id').focus();
	});


	function get_data_by_id(idx) {
		getList();
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "modulrumahsakit/get_data_by_id",
			data: "id="+idx,
			success: function(jsonData) {
				$('#saved_id').val(jsonData[0].id);
				$('#id_rumah_sakit').val(jsonData[0].kode_faskes);
				$('#nama_rumah_sakit').val(jsonData[0].nama_faskes);
				$('#alamat').val(jsonData[0].alamat);
				$('#longitude').val(jsonData[0].longitude);
				$('#latitude').val(jsonData[0].latitude);
				$('#jenis_rumah_sakit').val(jsonData[0].kepemilikan);
				$('#kabupaten').val(jsonData[0].kabupaten);
				$('#kelas_rumah_sakit').val(jsonData[0].tipe_faskes);
				$('#telepon').val(jsonData[0].telp);

				$('#frmInput').slideDown('fast', function() {
					$('#name').focus();
				});
			}
		});
	} 

	function reloadingData(){
		$('#result').html("<img src='{{URL::to('asset/images/ajax-loader-green.gif')}}' height='15px' /> <font color='red'>Loading...</font>");
		$.ajax({
			url: "modulrumahsakit_list_get",
			data: "search_name="+$('#search_name').val()+"&page="+$('#current_page').val(),
			success: function(jsonData) {
				$('#divSearchResult').html(jsonData);
				$('#result').html("Success !");
				prepareList();
			}
		});
	}

	function prepareList() {
		$('#pagingLinks a').click(function() {
			$('#result').html("<img src='{{URL::to('asset/images/ajax-loader-green.gif')}}' height='15px' /> <font color='red'>Loading...</font>");
			$(this).removeClass().addClass('buttonReloadIsLoad');
			
			$.ajax({
				url: this.href,
				data: "search_name="+$('#current_search').val(),
				success: function(jsonData) {
					$('#divSearchResult').html(jsonData);
					prepareList();
					$('#result').html("Success !");
				}
			});
		   
		   return false;
        });

		// select or unselect
		$('#tbodySearchResult tr').click(function(){
			var checkbox = $(this).children().eq(0).children();
			if(checkbox.prop('checked') == false) {
				checkbox.prop('checked', true);
				//$(this).removeClass('notSelected').addClass('bg_type4');
				$(this).css( "background", "#ffa" );
			} else {
				checkbox.prop('checked', false);
				//$(this).removeClass().addClass('notSelected');
				$(this).css( "background", "" );
			}
		});

		$('#reloadData').click(function(){
			getList();
			prepareList();
			return false;
		});
		
		$('#addData').click(function() {
			$('#frmInput').slideDown('fast');
			reset();
			
		});
		$('#findData').click(function() {
			var ofs = $("#findData").offset();
			$("#div_search")
			.css('left', ofs.left+20)
			.css('position', 'absolute')
			.slideDown('fast');
			$('#search_name').focus();
		});
		
	}
		
	function deleteList() {
		$('#reloadData').ajaxStart(function(){
			$('#result').html("Loading...");
		});
		
		$('#reloadData').ajaxStop(function(){
		});
		
		$('#frmList').ajaxSubmit({
			beforeSubmit:confirmDelete,
			type: 'POST',
			dataType: 'json',
			success:function(data) {
				$('#result').html(data.msg);
				if(data.status == 'success') {
					getList();
				} else $("#name").focus();
			}
		});
		return false;
	}
	
	function confirmDelete(formData, jqForm, opt) {
		var v = $('.checkbox_delete').fieldValue();
		if(v == 0 || v == 'undefined') {alert('Choose min 1 data!');return false;}
		return confirm('Delete '+ v.length +' data?');
	}
	
	function getList() {
		reloadingData();
		return false;
	}
	
	function selectAll() {
		if (allIsChecked == false) {
			$('#selectAll').removeClass('buttonSelectAll').addClass('buttonDeselectAll').html('<i class="icon-check"></i> Deselect All');
			$("#tbodySearchResult .checkbox_delete").prop("checked", true);
			//$('#tbodySearchResult tr').removeClass().addClass('selected');
			//$('#tbodySearchResult tr').removeClass("notSelected").addClass('bg_type4');
			$('#tbodySearchResult tr').css("background", "#ffa");
			allIsChecked = true;
		} else {
			$('#selectAll').removeClass('buttonDeselectAll').addClass('buttonSelectAll').html('<i class="icon-check"></i> Select All');
			$("#tbodySearchResult .checkbox_delete").prop("checked", false);
			//$('#tbodySearchResult tr').removeClass("bg_type4").addClass('notSelected');
			//$( this ).css( "color", "red" );
			$('#tbodySearchResult tr').css("background","");
			allIsChecked = false;
		}
	}
	
	function reset(){
		$('#frmInput').clearForm();
		$('#saved_id').val('');
		$('#name').focus();
	}
</script>
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body" id="" style="min-height:700px;">
                <div class="profile-head media">
                    <h4>
                        Data Modulrumahsakit
                    </h4>
                </div>
                <br>
                    <form action="{{URL::to('modulrumahsakit/process_form')}}" id="frmInput" method="POST" name="frmInput" style="display:none;">
                        <input id="saved_id" name="saved_id" type="hidden"/>
                        <table border="0" cellpadding="0" cellspacing="0" class="uk-table uk-table-hover uk-table-striped uk-table-condensed table-bordered">
                            <tr>
                                <td>
                                    Kode Faskes
                                </td>
                                <td>
                                    <input class="" id="id_rumah_sakit" name="id_rumah_sakit" size="30" type="text"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Nama Faskes
                                </td>
                                <td>
                                    <input class="" id="nama_rumah_sakit" name="nama_rumah_sakit" size="30" type="text"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Alamat
                                </td>
                                <td>
                                    <input class="" id="alamat" name="alamat" size="30" type="text"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    longitude
                                </td>
                                <td>
                                    <input class="" id="longitude" name="longitude" size="30" type="text"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    latitude
                                </td>
                                <td>
                                    <input class="" id="latitude" name="latitude" size="30" type="text"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Kepimilikan
                                </td>
                                <td>
                                    <input class="" id="jenis_rumah_sakit" name="jenis_rumah_sakit" size="30" type="text"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Provinsi
                                </td>
                                <td>
                                    {{ Form::select('kode_prop',array('0'=>'Pilih provinsi')+Provinsi::lists('provinsi','id_provinsi'),null,array('id'=>'id_provinsi_rs','placeholder' => 'Pilih Provinsi',		'onchange'=>"getKabupaten('_rs')",'class'=>'input-large id_combobox'))}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    kabupaten
                                </td>
                                <td>
                                    {{Form::select('kode_kab',array('0'=>'Pilih Kabupaten'),null,array('placeholder'=>'Pilih kabupaten','class' => 'input-large id_combobox','id'=>'id_kabupaten_rs'))}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Tipe faskes
                                </td>
                                <td>
                                    <input class="" id="kelas_rumah_sakit" name="kelas_rumah_sakit" size="30" type="text"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Telp
                                </td>
                                <td>
                                    <input class="" id="telepon" name="telepon" size="30" type="text"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <div class="">
                                        <button class="btn btn-success" id="simpan" name="Simpan" type="submit">
                                            <i class="icon-ok">
                                            </i>
                                            Simpan
                                        </button>
                                        <button class="btn btn-success" id="reset" name="Reset" type="reset">
                                            <i class="icon-remove">
                                            </i>
                                            Batal
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                    <div class="result" id="result">
                    </div>
                    <div class="smallSearchContainer bg_type2" id="div_search" style="display:none;">
                        Search
                        <button class="btn btn-danger" id="closeSmallSearch" style="float:right;" type="button">
                            <i class="icon-remove">
                            </i>
                        </button>
                        <div class="smallSearch">
                            <form action="{{URL::to('modulrumahsakit_list')}}" id="frmSearch" method="POST" name="frmSearch">
                                <input id="search_name" name="search_name" size="25" style="color:#000;;" type="text" value=""/>
                            </form>
                        </div>
                    </div>
                    <div id="divFormInputContainer">
                    </div>
                    <form action="{{URL::to('modulrumahsakit/delete_list')}}" id="frmList" method="POST" name="frmList">
                        <div id="divSearchResult">
                        </div>
                    </form>
                    <a class="btn btn-warning" href="{{URL::to('dashboard')}}">
                        Kembali
                    </a>
                </br>
            </div>
        </div>
    </div>
</div>
@stop
