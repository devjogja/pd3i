<input type="hidden" name="current_page" id="current_page" value="<?php echo $current_page; ?>" />
<input type="hidden" name="current_search" id="current_search" value="<?php echo $current_search; ?>" />
<div>
	<div class="btn btn-primary" id="addData"><i class="icon-plus"></i> Add</div>
	<div class="btn btn-primary" id="deleteList" onClick="deleteList();"><i class="icon-trash"></i> Delete</div>
	<div class="btn btn-primary" id="selectAll" onClick="selectAll();"><i class="icon-check"></i> Select All</div>
	<div class="btn btn-primary" id="reloadingData" onClick="reloadingData();"><i class="icon-refresh"></i> Reload</div>
	<div class="btn btn-primary" id="findData"><i class="icon-search"></i> Find</div>
	<div class="pagination pagination-sm" id="pagingLinks" style="margin:0px;float:right;height:60px;">
		{{$modulwilayahkerjapuskesmas->links()}}
	</div>
</div>
<div class="clear"></div>
<div class="table-responsive">
	<table cellpadding="0" cellspacing="0" border="0" class="uk-table uk-table-hover uk-table-striped uk-table-condensed table-bordered"><!--class="tblListData"-->
		<thead>			
			<tr class="">
				<th style="display:none;"></th>		
				<th>Kode Kelurahan</th>
				<th>Kelurahan</th>
				<th>Kode Puskesmas</th>
				<th>Puskesmas</th>
				<th class="span1">Aksi</th>
			</tr>
		</thead>
		<tbody id="tbodySearchResult">
			@if($modulwilayahkerjapuskesmas)
			@foreach($modulwilayahkerjapuskesmas as $data)	
			<tr class="">
				<td style="display:none;">
					<input type="checkbox" class="checkbox_delete" id="{{$data->id}}" name="delete_id[]" value="{{$data->id}}" />
				</td>	
				<td ondblclick="get_data_by_id('{{$data->id}}')" id="">{{$data->id_kelurahan}}</td>
				<td ondblclick="get_data_by_id('{{$data->id}}')" id="">{{$data->kelurahan}}</td>
				<td ondblclick="get_data_by_id('{{$data->id}}')" id="">{{$data->puskesmas_code_faskes}}</td>
				<td ondblclick="get_data_by_id('{{$data->id}}')" id="">{{$data->puskesmas_name}}</td>
				<td onclick="get_data_by_id('{{$data->id}}')" id="">
					<button type="button" class="btn btn-success"><i class="icon-ok"></i>Edit</input>
				</td>
			</tr>
			@endforeach
			@endif
		</tbody>
	</table>
</table>
