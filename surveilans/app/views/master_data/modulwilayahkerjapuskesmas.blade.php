@extends('layouts.master')
@section('content')	
	<script language="JavaScript" type="text/javascript">
		function disableForm() {
			$('input, select, button, textarea').attr('disabled','disabled');
		}
		function enableForm() {
			$('input, select, button, textarea').removeAttr('disabled');
		}
	</script>

	<script language="JavaScript" type="text/javascript">
		var allIsChecked = false;
		
		$(document).ready(function() {
			

		$('#closeSmallSearch').click(function(){
			$("#div_search").slideUp("fast", function(){$('#id').focus()});return false;
		});

		//get List Data
		//ajaxing search form
		$('#frmSearch').submit(function() {
			$('#result').html("<img src='{{URL::to('asset/images/ajax-loader-green.gif')}}' height='15px' /> <font color='red'>Loading...</font>");
			$.ajax({
				url: "modulwilayahkerjapuskesmas_list_get",
				data: "search_name="+$('#search_name').val()+"&page=1",
				success: function(jsonData) {
					$('#divSearchResult').html(jsonData);
					$('#result').html("Success !");
					prepareList();
				}
			});
			return false;
		});

		$('#frmInput').submit(function() {		
			$('#frmInput').ajaxSubmit({
				beforeSubmit:function() {
					disableForm();
					$('#result').html("<img src='{{URL::to('asset/images/ajax-loader-green.gif')}}' height='15px' /> <font color='red'>Loading...</font>");
				},
				dataType: 'json',
				success:function(data) {
					getList();
					reset();
					enableForm();
					$('#result').html(data.msg);
				}				
			});
			return false;
		});	

		$('#reset').click( function() {
			reset();
			$("#frmInput").slideUp("fast");
			return false;
		});
	
		$('#frmSearch').resetForm();
		$('#frmSearch').submit();
		$('#id').focus();
	});


	function get_data_by_id(idx) {
		getList();
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "modulwilayahkerjapuskesmas/get_data_by_id",
			data: "id="+idx,
			success: function(jsonData) {
				$('#saved_id').val(idx);
				$('#id').val(jsonData[0].id);
				$('#id_kelurahan').val(jsonData[0].id_kelurahan);
				$('#puskesmas_code_faskes').val(jsonData[0].puskesmas_code_faskes);
				$('#frmInput').slideDown('fast', function() {
					$('#name').focus();
				});
			}
		});
	} 

	function reloadingData(){
		$('#result').html("<img src='{{URL::to('asset/images/ajax-loader-green.gif')}}' height='15px' /> <font color='red'>Loading...</font>");
		$.ajax({
			url: "modulwilayahkerjapuskesmas_list_get",
			data: "search_name="+$('#search_name').val()+"&page="+$('#current_page').val(),
			success: function(jsonData) {
				$('#divSearchResult').html(jsonData);
				$('#result').html("Success !");
				prepareList();
			}
		});
	}

	function prepareList() {
		$('#pagingLinks a').click(function() {
			$('#result').html("<img src='{{URL::to('asset/images/ajax-loader-green.gif')}}' height='15px' /> <font color='red'>Loading...</font>");
			$(this).removeClass().addClass('buttonReloadIsLoad');
			
			$.ajax({
				url: this.href,
				data: "search_name="+$('#current_search').val(),
				success: function(jsonData) {
					$('#divSearchResult').html(jsonData);
					prepareList();
					$('#result').html("Success !");
				}
			});
		   
		   return false;
        });

		// select or unselect
		$('#tbodySearchResult tr').click(function(){
			var checkbox = $(this).children().eq(0).children();
			if(checkbox.prop('checked') == false) {
				checkbox.prop('checked', true);
				$(this).css( "background", "#ffa" );
			} else {
				checkbox.prop('checked', false);
				$(this).css( "background", "" );
			}
		});

		$('#reloadData').click(function(){
			getList();
			prepareList();
			return false;
		});
		
		$('#addData').click(function() {
			$('#frmInput').slideDown('fast');
			reset();
			
		});
		$('#findData').click(function() {
			var ofs = $("#findData").offset();
			$("#div_search")
			.css('left', ofs.left+20)
			.css('position', 'absolute')
			.slideDown('fast');
			$('#search_name').focus();
		});
		
	}
		
	function deleteList() {
		$('#reloadData').ajaxStart(function(){
			$('#result').html("Loading...");
		});
		
		$('#reloadData').ajaxStop(function(){
		});
		
		$('#frmList').ajaxSubmit({
			beforeSubmit:confirmDelete,
			type: 'POST',
			dataType: 'json',
			success:function(data) {
				$('#result').html(data.msg);
				if(data.status == 'success') {
					getList();
				} else $("#name").focus();
			}
		});
		return false;
	}
	
	function confirmDelete(formData, jqForm, opt) {
		var v = $('.checkbox_delete').fieldValue();
		if(v == 0 || v == 'undefined') {alert('Choose min 1 data!');return false;}
		return confirm('Delete '+ v.length +' data?');
	}
	
	function getList() {
		reloadingData();
		return false;
	}
	
	function selectAll() {
		if (allIsChecked == false) {
			$('#selectAll').removeClass('buttonSelectAll').addClass('buttonDeselectAll').html('<i class="icon-check"></i> Deselect All');
			$("#tbodySearchResult .checkbox_delete").prop("checked", true);
			$('#tbodySearchResult tr').css("background", "#ffa");
			allIsChecked = true;
		} else {
			$('#selectAll').removeClass('buttonDeselectAll').addClass('buttonSelectAll').html('<i class="icon-check"></i> Select All');
			$("#tbodySearchResult .checkbox_delete").prop("checked", false);
			$('#tbodySearchResult tr').css("background","");
			allIsChecked = false;
		}
	}
	
	function reset(){
		$('#frmInput').clearForm();
		$('#saved_id').val('');
		$('#name').focus();
		<?php
			$type = Session::get('type');
			$kd_faskes = Session::get('kd_faskes');
			if($type == 'puskesmas') echo "$('#puskesmas_code_faskes').val('".$kd_faskes."');";
		?>
	}
	
	function get_district(){
		$.ajax({
			//dataType:'json',
			data: 'province_id='+$('#province_id').val(),
			url:'{{URL::to("region/get_district")}}',
			success:function(data) {
				$('#district_id').html(data);
				if($('#province_id').val()=='') {
					$('#district_id').attr('disabled',true);
					$('#sub_district_id').attr('disabled',true);
					$('#village_id').attr('disabled',true);
				} else {
					$('#district_id').removeAttr('disabled');
				}   
				get_sub_district();
				get_village();
			}
		});
	}   
	
	function get_sub_district(){
		$.ajax({
			//dataType:'json',
			data: 'district_id='+$('#district_id').val(),
			url:'{{URL::to("region/get_sub_district")}}',
			success:function(data) {
				//alert(data);
				$('#sub_district_id').html(data);
				if($('#district_id').val()=='') {
					$('#sub_district_id').attr('disabled',true);
					$('#village_id').attr('disabled',true);
				} else {
					$('#sub_district_id').removeAttr('disabled');
				}
				get_village();
			}
		});
	}

	function getPuskesmas() {
		var code_kecamatan = $('#sub_district_id').val();
		if(code_kecamatan){
			var url = "{{url('puskesmas/list')}}";
			$.post(url,{code_kecamatan:code_kecamatan},function(data, status){
				$('#id_puskesmas').html('<option value="">Pilih Puskesmas</option>');
				if(data){
					$.each(JSON.parse(data), function(key, value) {
						$('#id_puskesmas')
						.append($("<option></option>")
							.attr("value",value.puskesmas_code_faskes)
							.text(value.puskesmas_name));
					});
				}
			});
		}
		$('#id_puskesmas').select2('val','');
		return false;
	}

	function get_village(){
		$.ajax({
			//dataType:'json',
			data: 'sub_district_id='+$('#sub_district_id').val(),
			url:'{{URL::to("region/get_village")}}',
			success:function(data) {
				//alert(data);
				$('#village_id').html(data);
				if($('#sub_district_id').val()==''){
					$('#village_id').attr('disabled',true);
				} else {
					$('#village_id').removeAttr('disabled');
					$('#id_puskesmas').removeAttr('disabled');
				}
			}
		});
	}   
	
</script>

<div class="span12">
<div class="content">
	<div class="module">
		<div id=""class="module-body" style="min-height:700px;">
			<div class="profile-head media">
				<h4>
					Wilayah Kerja Puskesmas
				</h4>
			</div>
			<br>
			<form method="POST" name="frmInput" style="display:none;" id="frmInput" action="{{URL::to('modulwilayahkerjapuskesmas/process_form')}}">
				<input type="hidden" name="saved_id" id="saved_id" />
				<table cellpadding="0" cellspacing="0" border="0" class="uk-table uk-table-hover uk-table-striped uk-table-condensed table-bordered">
						
					<tr>
						<td style="text-align:right;width:15%;">
							Provinsi
						</td>
						<td>    
							<select name="province_id" id="province_id" style="width:200px" onchange="get_district();" >
								<option value="">--- Pilih ---</option>
								<?php
								$q = "
									SELECT 
										id_provinsi,
										provinsi
									FROM provinsi
									order by provinsi ASC
								";
								$combo_district=DB::select($q);
								?>
								<?php for($i=0;$i<count($combo_district);$i++) :?>
								<?php //if($combo_district[$i]->id_provinsi == "34") $sel='selected'; else $sel='';?>
								<option value="<?php echo $combo_district[$i]->id_provinsi?>" <?php //echo $sel; ?>><?php echo $combo_district[$i]->provinsi; ?></option>
								<?php endfor;?>
							</select>
						</td>
					</tr>
					<tr>
						<td style="text-align:right;">
							Kabupaten
						</td>
						<td>
							<select name="district_id" id="district_id" style="width: 190px;" disabled="disabled"  onchange="get_sub_district();">
							</select>
						</td>
					</tr>
					<tr>
						<td style="text-align:right;">
							Kecamatan
						</td>
						<td>
							<select name="sub_district_id" id="sub_district_id" style="width: 190px;" disabled="disabled"  onchange="get_village();getPuskesmas();">
							</select>
						</td>
					</tr>
					<tr>
						<td style="text-align:right;">
							Desa / Kelurahan
						</td>
						<td><select name="id_kelurahan" id="village_id" style="width: 190px;" disabled="disabled" ></select>
						</td>
					</tr>
					<tr>
						<td>Kode Puskesmas</td>
						<td>
							<select name="puskesmas_code_faskes" id="id_puskesmas" style="width: 190px;" disabled="disabled" ></select>
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<div class="">
								<button type="submit" name="Simpan" id="simpan" class="btn btn-success"><i class="icon-ok"></i> Simpan</button>
								<button type="reset" name="Reset" id="reset" class="btn btn-success"><i class="icon-remove"></i> Batal</button>
							</div>
						</td>
					</tr>
				</table>
			</form>
			<div id="result" class="result"></div>
			<div class="smallSearchContainer bg_type2" id="div_search" style="display:none;">
				Search
				<button type="button" id="closeSmallSearch" class="btn btn-danger" style="float:right;"><i class="icon-remove"></i> </button>
				<div class="smallSearch">
					<form method="POST" name="frmSearch" id="frmSearch" action="{{URL::to('modulwilayahkerjapuskesmas_list')}}">
						<input type="text" name="search_name" id="search_name" value="" size="25" style="color:#000;;" />
					</form>
				</div>
			</div>

			<div id="divFormInputContainer"></div>

			<form method="POST" name="frmList" id="frmList" action="{{URL::to('modulwilayahkerjapuskesmas/delete_list')}}">
				<div id="divSearchResult"></div>
			</form>
	
<a href="{{URL::to('dashboard')}}" class="btn btn-warning">Kembali</a>		
	</div>
</div>
</div>
</div>
@stop
