<input type="hidden" name="current_page" id="current_page" value="<?php echo $current_page; ?>" />
<input type="hidden" name="current_search" id="current_search" value="<?php echo $current_search; ?>" />
<div>
	<div class="btn btn-primary" id="addData"><i class="icon-plus"></i> Add</div>
	<div class="btn btn-primary" id="deleteList" onClick="deleteList();"><i class="icon-trash"></i> Delete</div>
	<div class="btn btn-primary" id="selectAll" onClick="selectAll();"><i class="icon-check"></i> Select All</div>
	<div class="btn btn-primary" id="reloadingData" onClick="reloadingData();"><i class="icon-refresh"></i> Reload</div>
	<div class="btn btn-primary" id="findData"><i class="icon-search"></i> Find</div>
	<div class="pagination pagination-sm" id="pagingLinks" style="margin:0px;float:right;height:60px;">
		{{$modullaboratorium->links()}}
	</div>
</div>
<div class="clear"></div>
<div class="table-responsive">
	<table cellpadding="0" cellspacing="0" border="0" class="uk-table uk-table-hover uk-table-striped uk-table-condensed table-bordered"><!--class="tblListData"-->
		<thead>
			
<tr class="">
	<th style="display:none;"></th>
		
<th>id_laboratorium</th>
<th>lab_code</th>
<th>nama_laboratorium</th>
<th>alamat</th>
<th>telepon</th>
<th>fax</th>
<th>email</th>
<th>konfirmasi</th>	
	<th class="span1">Aksi</th>
</tr>
		</thead>
		<tbody id="tbodySearchResult">
			@if($modullaboratorium)
			@foreach($modullaboratorium as $data)	
			
<tr class="">
	<td style="display:none;">
		<input type="checkbox" class="checkbox_delete" id="{{$data->id_laboratorium}}" name="delete_id[]" value="{{$data->id_laboratorium}}" />
	</td>
	
<td ondblclick="get_data_by_id('{{$data->id_laboratorium}}')" id="">{{$data->id_laboratorium}}</td>
<td ondblclick="get_data_by_id('{{$data->lab_code}}')" id="">{{$data->lab_code}}</td>
<td ondblclick="get_data_by_id('{{$data->nama_laboratorium}}')" id="">{{$data->nama_laboratorium}}</td>
<td ondblclick="get_data_by_id('{{$data->alamat}}')" id="">{{$data->alamat}}</td>
<td ondblclick="get_data_by_id('{{$data->telepon}}')" id="">{{$data->telepon}}</td>
<td ondblclick="get_data_by_id('{{$data->fax}}')" id="">{{$data->fax}}</td>
<td ondblclick="get_data_by_id('{{$data->email}}')" id="">{{$data->email}}</td>
<td ondblclick="get_data_by_id('{{$data->konfirmasi}}')" id="">{{$data->konfirmasi}}</td>
	<td onclick="get_data_by_id('{{$data->id_laboratorium}}')" id="">
		<button type="button" class="btn btn-success"><i class="icon-ok"></i>Edit</input>
	</td>
</tr>

			@endforeach
			@endif
		</tbody>
	</table>
</table>
