<input type="hidden" name="current_page" id="current_page" value="<?php echo $current_page; ?>" />
<input type="hidden" name="current_search" id="current_search" value="<?php echo $current_search; ?>" />
<div>
	<div class="btn btn-primary" id="addData"><i class="icon-plus"></i> Add</div>
	<div class="btn btn-primary" id="deleteList" onClick="deleteList();"><i class="icon-trash"></i> Delete</div>
	<div class="btn btn-primary" id="selectAll" onClick="selectAll();"><i class="icon-check"></i> Select All</div>
	<div class="btn btn-primary" id="reloadingData" onClick="reloadingData();"><i class="icon-refresh"></i> Reload</div>
	<div class="btn btn-primary" id="findData"><i class="icon-search"></i> Find</div>
	<div class="pagination pagination-sm" id="pagingLinks" style="margin:0px;float:right;height:60px;">
		{{$modultipespesimen->links()}}
	</div>
</div>
<div class="clear"></div>
<div class="table-responsive">
	<table cellpadding="0" cellspacing="0" border="0" class="uk-table uk-table-hover uk-table-striped uk-table-condensed table-bordered"><!--class="tblListData"-->
		<thead>
			
<tr class="">
	<th style="display:none;"></th>
		
<th>id</th>
<th>id_campak</th>
<th>tipe_spesimen</th>
<th>jml_tipe_spesimen</th>
<th>created_at</th>
<th>updated_at</th>	
	<th class="span1">Aksi</th>
</tr>
		</thead>
		<tbody id="tbodySearchResult">
			@if($modultipespesimen)
			@foreach($modultipespesimen as $data)	
			
<tr class="">
	<td style="display:none;">
		<input type="checkbox" class="checkbox_delete" id="{{$data->id}}" name="delete_id[]" value="{{$data->id}}" />
	</td>
	
<td ondblclick="get_data_by_id('{{$data->id}}')" id="">{{$data->id}}</td>
<td ondblclick="get_data_by_id('{{$data->id_campak}}')" id="">{{$data->id_campak}}</td>
<td ondblclick="get_data_by_id('{{$data->tipe_spesimen}}')" id="">{{$data->tipe_spesimen}}</td>
<td ondblclick="get_data_by_id('{{$data->jml_tipe_spesimen}}')" id="">{{$data->jml_tipe_spesimen}}</td>
<td ondblclick="get_data_by_id('{{$data->created_at}}')" id="">{{$data->created_at}}</td>
<td ondblclick="get_data_by_id('{{$data->updated_at}}')" id="">{{$data->updated_at}}</td>
	<td onclick="get_data_by_id('{{$data->id}}')" id="">
		<button type="button" class="btn btn-success"><i class="icon-ok"></i>Edit</button>
	</td>
</tr>

			@endforeach
			@endif
		</tbody>
	</table>
</table>
</div>
</div>
