@extends('layouts.master')
@section('content')	
<div id="container"></div>
<textarea id="geojson" placeholder="Paste your GeoJSON here to test your map" style="height:100px;width:100%"></textarea>
<button id="run">Run</button>

<script type="text/javascript">
	$(function(){
		$('#run').click(function () {     
			var geojson = $.parseJSON($('#geojson').val());
			$('#container').slideDown().highcharts('Map', {
				series: [{
					mapData: geojson
				}]
			});    
		});

	});
</script>

<style type="text/css">
	.loading {
		margin-top: 10em;
		text-align: center;
		color: gray;
	}
</style>
@stop