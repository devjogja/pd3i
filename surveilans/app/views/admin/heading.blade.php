<div class="navbar navbar-fixed-top form-login-heading">
    <div class="navbar-inner form-login-heading" style="background: none repeat scroll 0 0 #7ab317;">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                <i class="icon-reorder shaded"></i></a><a class="brand" href="{{URL::to('/')}}" style="color:#fff;">Surveilans PD3I </a>
            <div class="nav-collapse collapse navbar-inverse-collapse">
                @if(Sentry::check())
                    @if(Request::segment(1))
                        <ul class="nav pull-right">
                    @elseif(Sentry::getUser()->hak_akses==2)
                    <ul class="nav pull-left">
                        <?php if(Session::get('type')): ?>
    					<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:#fff;">Master Data
                            <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{URL::to('modulwilayahkerjapuskesmas')}}">Wilayah Kerja Puskesmas</a></li>
                            </ul>
                        </li>
    					<?php endif; ?>
    				</ul>	
    				@endif
    				@if(Sentry::getUser()->hak_akses==4)
                    <ul class="nav pull-left">
                        <?php if(Session::get('type')): ?>
    					<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:#fff;">Master Data
                            <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{URL::to('modulwilayahkerjapuskesmas')}}">Wilayah Kerja Puskesmas</a></li>
    							<li><a href="{{URL::to('modulpuskesmas')}}">Puskesmas</a></li>
    							<li><a href="{{URL::to('modulkelurahan')}}">Desa</a></li>
                            </ul>
                        </li>
    					<?php endif; ?>
    				</ul>	
    				@endif
    				@if(Sentry::getUser()->hak_akses==6)
                    <ul class="nav pull-left">
                        <?php if(Session::get('type')): ?>
    					<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:#fff;">Master Data
                            <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{URL::to('modulwilayahkerjapuskesmas')}}">Wilayah Kerja Puskesmas</a></li>
    							<li><a href="{{URL::to('modulkabupaten')}}">Kabupaten</a></li>
    							<li><a href="{{URL::to('modulpuskesmas')}}">Puskesmas</a></li>
    							<li><a href="{{URL::to('modulkelurahan')}}">Desa</a></li>
                            </ul>
                        </li>
    					<?php endif; ?>
    				</ul>	
    				@endif
    				@if(Sentry::getUser()->hak_akses==1)
                    <ul class="nav pull-left">
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:#fff;">Master Data
                            <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li class="nav-header">Wilayah</li>
                                <li><a href="{{URL::to('modulprovinsi')}}">Provinsi</a></li>
                                <li><a href="{{URL::to('modulkabupaten')}}">Kabupaten</a></li>
                                <li><a href="{{URL::to('modulkecamatan')}}">Kecamatan</a></li>
    							<li><a href="{{URL::to('modulkelurahan')}}">Desa</a></li>
                                <li class="divider"></li>
                                <li class="nav-header">Fasilitas kesehatan</li>
                                <li><a href="{{URL::to('modullaboratorium')}}">Laboratorium</a></li>
                                <li><a href="{{URL::to('modulrumahsakit')}}">Rumah Sakit</a></li>
                                <li><a href="{{URL::to('modulpuskesmas')}}">Puskesmas</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav pull-right">
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:#fff;">Backup Data
                            <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{URL::to('backup')}}">Export</a></li>
                                <li><a href="{{URL::to('restore')}}">Import</a></li>
                            </ul>
                        </li>
                    @elseif(Sentry::getUser()->hak_akses==2 || Sentry::getUser()->hak_akses==7 || Sentry::getUser()->hak_akses==3 || Sentry::getUser()->hak_akses==4 || Sentry::getUser()->hak_akses==6)
                    <!-- -->
                    <ul class="nav pull-right">
    					<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:#fff;">Pilih Kasus lain
                            <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{URL::to('campak')}}">Campak</a></li>
                                <li><a href="{{URL::to('difteri')}}">Difteri</a></li>
                                <li><a href="{{URL::to('afp')}}">AFP</a></li>
                                <li><a href="{{URL::to('tetanus')}}">Tetanus neonatorum</a></li>
                                <li><a href="{{URL::to('crs')}}">Crs</a></li>
                            </ul>
                        </li>
    				@elseif(Sentry::getUser()->hak_akses==5)
                    <ul class="nav pull-right">
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:#fff;">Pilih tersangka lain
                            <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{URL::to('labs/campak')}}">Tersangka Campak</a></li>
                                <li><a href="{{URL::to('labs/afp')}}">Tersangka AFP</a></li>
                                <li><a href="{{URL::to('labs/crs')}}">Tersangka CRS</a></li>
                            </ul>
                        </li>
                    @else
                    <ul class="nav pull-right">
                    @endif
                @endif
				
				<?php if(Session::get('type')): ?>
                    <li class="nav-user dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php
							$nama_instansi="";
							$type_profil = Session::get('type_profil');
							$kd_faskes = Session::get('kd_faskes');
							$id_profil = Session::get('id_profil');
							if($type_profil == "puskesmas"){
								$q = "
									SELECT b.puskesmas_name
									FROM profil_puskesmas a
									JOIN puskesmas b ON b.puskesmas_code_faskes=a.kode_faskes
									WHERE a.id='".$id_profil."'
								";
								$data=DB::select($q);
								if(count($data) > 0) $nama_instansi = $data[0]->puskesmas_name;
							} else if($type_profil == "rs"){
								$q = "
									SELECT b.nama_faskes
									FROM profil_rs a
									JOIN rumahsakit2 b ON b.kode_faskes=a.kode_faskes
									WHERE a.id='".$id_profil."'
								";
								$data=DB::select($q);
								if(count($data) > 0) $nama_instansi = $data[0]->nama_faskes;
							} else if($type_profil == "kabupaten"){
								$q = "
									SELECT b.kabupaten
									FROM profil_dinkes_kabupaten a
									JOIN kabupaten b ON b.id_kabupaten=a.kode_kabupaten
									WHERE a.id='".$id_profil."'
								";
								$data=DB::select($q);
								if(count($data) > 0) $nama_instansi = $data[0]->kabupaten;
								
							} else if($type_profil == "provinsi"){
								$q = "
									SELECT b.provinsi
									FROM profil_dinkes_provinsi a
									JOIN provinsi b ON b.id_provinsi=a.kode_provinsi
									WHERE a.id='".$id_profil."'
								";
								$data=DB::select($q);
								if(count($data) > 0) $nama_instansi = $data[0]->provinsi;
							} else if($type_profil == "laboratorium"){
								$q = "
									SELECT b.nama_laboratorium
									FROM profil_laboratorium a
									JOIN laboratorium b ON b.lab_code=a.kode_lab
									WHERE a.id='".$id_profil."'
								";
								$data=DB::select($q);
								if(count($data) > 0) $nama_instansi = $data[0]->nama_laboratorium;
							} else if($type_profil == "kemenkes"){
								$nama_instansi = " KEMENTERIAN KESEHATAN";
							}
						?>
                        <img src="{{URL::to('style/images/user1.png')}}" class="nav-avatar" />
                        <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{URL::to('edit_profil_user')}}">@if(Sentry::check()){{Sentry::getUser()->first_name}}@endif</a></li>
                            <li><a href="{{URL::to('edit_profil_instansi')}}"><?php echo $type_profil." - ".$nama_instansi; ?></a></li>
                            <!-- <li><a href="#">Account Settings</a></li> -->
                            <li class="divider"></li>
                            <li><a href="{{URL::to('logout')}}">Logout</a></li>
                        </ul>
                    </li>
				<?php endif; ?>	
                </ul>
				<ul class="nav pull-right">				
					@if(isset(Sentry::getUser()->id))
					@else
					<li>
						<a href="{{URL::to('signup')}}" style="color:#fff;">
							Sign Up
						</a>
					</li>
					@endif
				</ul>
            </div>
        </div>
    </div>
</div>