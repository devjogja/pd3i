<?php 

		$penyakit = Input::get('penyakit');
		$arr_penyakit['campak'] = "Campak";
		$arr_penyakit['afp'] = "Polio";
		$arr_penyakit['difteri'] = "Difteri";
		$arr_penyakit['tetanus'] = "Tetanus Neonatorum";
		
		$arr_nama_bulan[1] = "Januari";
		$arr_nama_bulan[2] = "Februari";
		$arr_nama_bulan[3] = "Maret";
		$arr_nama_bulan[4] = "April";
		$arr_nama_bulan[5] = "Mei";
		$arr_nama_bulan[6] = "Juni";
		$arr_nama_bulan[7] = "Juli";
		$arr_nama_bulan[8] = "Agustus";
		$arr_nama_bulan[9] = "September";
		$arr_nama_bulan[10] = "Oktober";
		$arr_nama_bulan[11] = "November";
		$arr_nama_bulan[12] = "Desember";
		
		if($penyakit == "campak") {
			$data=DB::select("
				SELECT 
					IF(c.klasifikasi_final=NULL,'Pending',
					IF(c.klasifikasi_final='6','Pending',
					IF(c.klasifikasi_final='1','(Lab)',
					IF(c.klasifikasi_final='2','(Epid)',
					IF(c.klasifikasi_final='3','(Klinis)',
					IF(c.klasifikasi_final='4','Rubella',
					IF(c.klasifikasi_final='5','Bkn Campak',''
					))))))) AS `name`,	
					COUNT(*) AS jml 
				FROM hasil_uji_lab_campak a 
				JOIN pasien b ON b.id_pasien=a.id_pasien
				JOIN campak c ON c.id_campak=a.id_campak
				JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
				LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
				LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
				LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
				LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
				WHERE 
				c.deleted_at IS NULL
				AND c.klasifikasi_final IN ('1','2','3')
				AND CASE WHEN c.tanggal_timbul_rash='1970-01-01' OR c.tanggal_timbul_rash='0000-00-00' THEN YEAR(c.tanggal_timbul_demam) ELSE YEAR(c.tanggal_timbul_rash) END BETWEEN '2014' AND '2015'
				GROUP BY c.klasifikasi_final
			");
		} else if($penyakit == "difteri") {
			$data=DB::select("
				SELECT 
					IFNULL(c.klasifikasi_final,'Pending') AS `name`,	
					COUNT(*) AS `jml` 
				FROM hasil_uji_lab_difteri a 
				JOIN pasien b ON b.id_pasien=a.id_pasien
				JOIN difteri c ON c.id_difteri=a.id_difteri
				JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
				LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
				LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
				LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
				LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
				WHERE 
				1=1
				AND c.klasifikasi_final IN ('konfirm')
				AND DATE_FORMAT(c.tanggal_timbul_demam,'%Y') >= '2014'
				AND DATE_FORMAT(c.tanggal_timbul_demam,'%Y') <= '2015'
				GROUP BY c.klasifikasi_final
			");
		} else if($penyakit == "tetanus") {	
			$data=DB::select("
				SELECT 
					IFNULL(c.klasifikasi_akhir,'Pending') AS `name`,	
					COUNT(*) AS `jml` 
				FROM pasien_terserang_tetanus a 
				JOIN pasien b ON b.id_pasien=a.id_pasien
				JOIN tetanus c ON c.id_tetanus=a.id_tetanus
				JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
				LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
				LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
				LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
				LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
				WHERE 
				1=1
				AND c.klasifikasi_akhir IN ('Konfirm TN')
				AND DATE_FORMAT(c.tanggal_mulai_sakit,'%Y') >= '2014'
				AND DATE_FORMAT(c.tanggal_mulai_sakit,'%Y') <= '2015'
				GROUP BY c.klasifikasi_akhir
			");
		}
		
		$x_data = "";
		$comma = "";
		for($i=0;$i<count($data);$i++){
			$x_data .= 	$comma."['".$data[$i]->name."',".$data[$i]->jml."]";
			$comma = ",";
		}
	?>
	
	
$('#container_klasifikasi_final').highcharts({
	chart: {
		type: 'pie',
		options3d: {
			enabled: true,
			alpha: 45
		}
	},
	title: {
		text: '<?php echo $arr_penyakit[$penyakit]; ?>'
	},
	subtitle: {
		text: 'Tahun <?php echo date('Y')-1;?> - <?php echo date('Y');?>'
	},
	plotOptions: {
		pie: {
			innerSize: 70,
			depth: 45
		}
	},
	series: [{
		name: 'Jumlah Penderita',
		data: [
			<?php echo $x_data; ?>
		],
		dataLabels: {
			formatter: function () {
				// display only if larger than 1
				return this.y >= 1 ? '<br>' + this.point.name + ' : ' + this.y + ' jiwa'  : null;
			}
		}
		
	}]
});
		
		