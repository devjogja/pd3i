<!DOCTYPE html>
<html lang="en">
<head>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Surveilans PD3I</title>
<link type="text/css" href="{{URL::to('style/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
<link type="text/css" href="{{URL::to('style/bootstrap/css/bootstrap-responsive.min.css')}}" rel="stylesheet">
<link type="text/css" href="{{URL::to('style/css/theme.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{URL::to('asset/css/app.css')}}" type="text/css" cache="false"/>
<link type="text/css" href="{{URL::to('style/images/icons/css/font-awesome.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::to('packages/datatables/css/jquery.dataTables.css')}}" />

<script src="{{URL::to('style/scripts/jquery-1.9.1.min.js')}}" type="text/javascript"></script>

<script src="{{URL::to('style/scripts/jquery-ui-1.10.1.custom.min.js')}}" type="text/javascript"></script>
<script src="{{URL::to('style/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{URL::to('style/scripts/flot/jquery.flot.js')}}" type="text/javascript"></script>
<script src="{{URL::to('style/scripts/flot/jquery.flot.resize.js')}}" type="text/javascript"></script>
<script src="{{URL::to('style/scripts/datatables/jquery.dataTables.js')}}" type="text/javascript"></script>

<script src="{{URL::to('style/scripts/jquery.form.js')}}" type="text/javascript"></script>
<script src="{{URL::to('style/scripts/jquery.validate.js')}}" type="text/javascript"></script>
<!--<script src="{{URL::to('style/scripts/common.js')}}" type="text/javascript"></script>-->
<!--{{ HTML::script('asset/js/app1.js') }}-->
<script src="{{URL::to('highcharts/js/highcharts.js')}}"></script>
<script src="{{URL::to('highcharts/js/highcharts-3d.js')}}"></script>
<script src="{{URL::to('highcharts/js/exporting.js')}}"></script>

<script src="{{URL::to('googlemaps/maps.js')}}"></script>
<script src="{{URL::to('googlemaps/maplabel.js')}}"></script>
<script src="{{URL::to('googlemaps/maplabel-compiled.js')}}"></script>

<script>
$(document).ready(function(){
	$('#login').submit(function() {
		loading_state();
		$('#login').ajaxSubmit({
			type: 'POST',
			dataType:'json',
			success:function(jsonData) {
				$('#status').html(jsonData.message);
				if(jsonData.status == 'success')location.href  = jsonData.address;
			}
		});
		return false;
	});
	
	combo_grafik_waktu();
	combo_grafik_wilayah();
	combo_grafik_klasifikasi_final();
	init_map();
	
	
	$('.carousel').carousel({
		interval: 6000
	})
});

function loading_state(){
	$('#status').html('Loading...');
}	

</script>

</head>
<body>
@include('admin.heading')
<style>
.gmnoprint img {
    max-width: none; 
}

#map_dashboard img {
    max-width: none;
}
</style>




<script>

function init_map(){
	$.ajax({
		dataType:'script',
		//type: 'POST',
		//data: 'penyakit='+$('#combo_grafik_maps').val()+'&num='+num,
		url:'{{URL::to("get_js_init_maps_instansi")}}',
		success:function(data) {
			combo_grafik_maps();
		}
	});
}

function combo_grafik_maps(){
	//for(var i=0;i<=50;i++){
		get_map();
	//}
	
}

function get_map(){
	$.ajax({
		dataType:'script',
		//type: 'POST',
		data: 'penyakit='+$('#combo_grafik_maps').val()+'&num='+$('#last_maps_id').val(),
		url:'{{URL::to("get_js_grafik_maps_nasional")}}',
		success:function(data) {
			//var a = parseInt("10")
			var num = (parseInt ($('#last_maps_id').val()))+1;
			$('#last_maps_id').val(num);
			if(parseInt($('#last_maps_id').val()) < parseInt($('#max_maps_id').val())) get_map();
		}
	});
}

function combo_grafik_wilayah(){
	$.ajax({
		dataType:'script',
		//type: 'POST',
		data: 'penyakit='+$('#combo_grafik_wilayah').val(),
		url:'{{URL::to("get_js_grafik_wilayah_nasional")}}',
		success:function(data) {
					
		}
	});
}

function combo_grafik_klasifikasi_final(){
	$.ajax({
		dataType:'script',
		//type: 'POST',
		data: 'penyakit='+$('#combo_grafik_klasifikasi_final').val(),
		url:'{{URL::to("get_js_grafik_klasifikasi_final_nasional")}}',
		success:function(data) {
					
		}
	});
}

function combo_grafik_waktu(){
	$.ajax({
		dataType:'script',
		//type: 'POST',
		data: 'penyakit='+$('#combo_grafik_waktu').val(),
		url:'{{URL::to("get_js_grafik_waktu_nasional")}}',
		success:function(data) {
					
		}
	});
}
</script>
<div class="wrapper">
    <div class="container">
        <div class="row">
            <!--/.span3-->
            <div class="span12">
                <div class="content">
                    <div class="btn-controls">
                        
                        <div class="btn-box-row row-fluid">
                            <ul class="widget widget-usage unstyled span8">
                                <div class="module-head">
                                    <h3>Info</h3>
                                </div>
                                <div class="module-body" id="">
									<div id="myCarousel" class="carousel slide">
										<div class="carousel-inner">
										  <div class="item active">
											<img src="{{URL::to('asset/images/slide-campak.jpg')}}" alt="">
											<div class="carousel-caption">
											  <h4>Campak</h4>
											  <p>Penyakit Campak (Rubeola, Campak 9 hari, measles) adalah suatu infeksi virus yang sangat menular, yang ditandai dengan demam, batuk, konjungtivitis (peradangan selaput ikat mata/konjungtiva) dan ruam kulit. </p>
											</div>
										  </div>
										  <div class="item">
											<img src="{{URL::to('asset/images/slide-difteri.jpg')}}" alt="">
											<div class="carousel-caption">
											  <h4>Difteri</h4>
											  <p>Difteri ialah penyakit yang mengerikan di mana masa lalu telah menyebabkan ribuan kematian, dan masih mewabah di daerah-daerah dunia yang belum berkembang</p>
											</div>
										  </div>
										  <div class="item">
											<img src="{{URL::to('asset/images/slide-tetanus.jpg')}}" alt="">
											<div class="carousel-caption">
											  <h4>Tetanus Neonatorum</h4>
											  <p>Tetanus Neonatorum yang juga dikenal dengan lockjaw [1], merupakan penyakit yang disebakan oleh tetanospasmin, yaitu sejenis neurotoksin yang diproduksi oleh Clostridium tetani yang menginfeksi sistem urat saraf dan otot sehingga saraf dan otot menjadi kaku (rigid)</p>
											</div>
										  </div>
										  <div class="item">
											<img src="{{URL::to('asset/images/slide-afp.jpg')}}" alt="">
											<div class="carousel-caption">
											  <h4>AFP</h4>
											  <p>Poliomielitis atau polio, adalah penyakit paralisis atau lumpuh yang disebabkan oleh virus. Agen pembawa penyakit ini, sebuah virus yang dinamakan poliovirus (PV), masuk ke tubuh melalui mulut, mengifeksi saluran usus</p>
											</div>
										  </div>
										</div>
										<a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
										<a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
									  </div>
								</div>
								
                            </ul>
                            
                            <ul class="widget widget-usage unstyled span4">
                                {{Form::open(array('url'=>'login','class'=>'form-vertical','id'=>'login'))}}
                                    <div class="module-head">
                                        <h3>Sign In</h3>
                                    </div>
                                    <div class="module-body" style="height:155px;">
                                        <div class="control-group">
                                            <div class="controls row-fluid">
                                                <input name="username" class="span12" type="text" id="inputEmail" placeholder="Email">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <div class="controls row-fluid">
												<input name="password" class="span12" type="password" id="inputPassword" placeholder="Password">
                                            </div>
                                        </div>
										<div class="control-group">
                                            <div class="controls row-fluid" id="status">
												
                                            </div>
                                        </div>
										
                                    </div>
                                    <div class="module-foot">
                                        <div class="control-group">
                                            <div class="controls clearfix">
                                                <button type="submit" class="btn btn-primary pull-right">Login</button>
                                            </div>
                                        </div>
                                    </div>
                                {{Form::close()}}
                            </ul>
							
							<ul class="widget widget-usage unstyled span4">
								<div class="module-body">
									<div class="control-group">
										<div class="controls row-fluid">
											<center>
												<a href="http://202.70.136.52/rsonline/report/" target="_blank">
													<img src="{{URL::to('asset/images/link-data-puskesmas.jpg')}}">
												</a>
												<a href="http://www.bankdata.depkes.go.id/puskesmas/" target="_blank">
													<img src="{{URL::to('asset/images/link-data-rs.jpg')}}">
												</a>
												<a href="http://www.infopenyakit.org/" target="_blank">
													<img src="{{URL::to('asset/images/link-info-penyakit.jpg')}}">
												</a>
											<br>
												<a href="http://www.depkes.go.id/" target="_blank">
													<img src="{{URL::to('asset/images/link-kemenkes.jpg')}}">
												</a>
												<a href="http://www.pppl.kemkes.go.id/" target="_blank">	
													<img src="{{URL::to('asset/images/link-p2pl.jpg')}}">
												</a>
												<a href="http://www.who.int" target="_blank">
													<img src="{{URL::to('asset/images/link-who.jpg')}}">
												</a>
											</center>
										</div>
									</div>
								</div>
                            </ul>
							
                        </div>
                    </div>
					
					<div class="btn-controls">
						<div class="btn-box-row row-fluid">
							<ul class="widget widget-usage unstyled span8">
								<div class="module-head">
									<h3 style="float:left;">Grafik</h3>
									<div style="float:right;">
										<select id="combo_grafik_waktu" onChange="combo_grafik_waktu();">
											<option value="campak">Campak</option>
											<option value="tetanus">Tetanus Neonatorum</option>
											<option value="difteri">Difteri</option>
										</select>
									</div>
									<div style="clear:both;"></div>
								</div>
								<div class="module-body" id="container_waktu" style="width:98%;height:350px;">
									
								</div>
								
							</ul>
							<ul class="widget widget-usage unstyled span4">
								<div class="module-head">
									<h3 style="float:left;">Grafik</h3>
									<div style="float:right;">
										<select id="combo_grafik_klasifikasi_final" onChange="combo_grafik_klasifikasi_final();">
											<option value="campak">Campak</option>
											<option value="tetanus">Tetanus Neonatorum</option>
											<option value="difteri">Difteri</option>
										</select>
									</div>
									<div style="clear:both;"></div>
								</div>
								<div class="module-body" id="container_klasifikasi_final" style="width:90%;height:350px;">
									
								</div>
								
							</ul>
						</div>
					</div>		
					
                    <!--/#btn-controls-->
                    <div class="module">
						<div class="module-head">
							<h3 style="float:left;">Grafik</h3>
							<div style="float:right;">
								<select id="combo_grafik_wilayah" onChange="combo_grafik_wilayah();">
									<option value="campak">Campak</option>
									<option value="tetanus">Tetanus Neonatorum</option>
									<option value="difteri">Difteri</option>
								</select>
							</div>
							<div style="clear:both;"></div>
						</div>
                        <div id="grafik_per_wilayah" class="module-body" style="height:500px;">
                            
                        </div>
                    </div>
                    
					<div class="module">
						<div class="module-head">
							<h3 style="float:left;">Maps</h3>
							<div style="float:right;">
								<input type="hidden" id="last_maps_id" name="last_maps_id" value="0">
								<input type="hidden" id="max_maps_id" name="max_maps_id" value="50">
								<select id="combo_grafik_maps" onChange="combo_grafik_maps();">
									<option value="campak">Campak</option>
									<option value="tetanus">Tetanus Neonatorum</option>
									<option value="difteri">Difteri</option>
								</select>
							</div>
							<div style="clear:both;"></div>
						</div>
                        <div id="map_dashboard" class="module-body" style="height:500px;">
                            
                        </div>
                    </div>
                    
                    <!--/.module-->
                </div>
                <!--/.content-->
            </div>
            <!--/.span9-->
        </div>
    </div>
    <!--/.container-->
</div>
<!--/.wrapper-->
<div class="footer">
    <div class="container">
        <b class="copyright">&copy; 2014 surveilan PD3I </b>All rights reserved.
    </div>
</div>

</body>
</html>
