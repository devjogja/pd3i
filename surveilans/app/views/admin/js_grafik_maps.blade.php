<?php
	$nama_instansi = "";
	$type = 'provinsi';
	$kd_faskes = Session::get('kd_faskes');
	//$latitude = "-7.8936254";
	//$longitude = "110.4029268";
	$latitude = "0";
	$longitude = "0";
	$default_zoom = "0";
	$min_zoom = "0";
	//$last_id = Session::get('sess_id');
	//Session::put('sess_id', ((int) $last_id+1));
	//$now_id = Session::get('sess_id');
	$now_id = "";
	$last_id = "";
		
		$parent_level = "provinsi";
		$level = "kabupaten";
		$q = "
		SELECT a.id,e.id_kabupaten,e.kabupaten as kabupaten,b.center_lat,b.center_lon 
		FROM polygon a 
		LEFT JOIN peta_dati b ON b.polygon_id=a.id
		LEFT JOIN kabupaten e ON e.id_kabupaten=b.desa_id
		LEFT JOIN provinsi f ON f.id_provinsi=e.id_provinsi
		WHERE 
		1=1
		AND
		f.id_provinsi IN ('33','34')
		AND b.level='".$level."'
		LIMIT ".Input::get('num').",1
		";
	
		$latitude = '-7.4024589';
		$longitude = '110.1254052';
		$default_zoom  = '9';
		$min_zoom  = '9';
	
	
	
	$penyakit = Input::get('penyakit');
	$arr_penyakit['campak'] = "Campak";
	$arr_penyakit['afp'] = "Polio";
	$arr_penyakit['difteri'] = "Difteri";
	$arr_penyakit['tetanus'] = "Tetanus";
	
	
	$polygon_delete=DB::select($q);
	$poly_map = "";
	for($i=0;$i<count($polygon_delete);$i++){
		
		//if($last_id) $poly_map .= "Triangle".$last_id.$polygon_delete[$i]->id.".setMap(null);";
		
		//$poly_map .= "
		//	var Triangle".$now_id.$polygon_delete[$i]->id.";		
		//";
	}
	
?>


var myLatlng = new google.maps.LatLng(<?php  echo $latitude; ?>,<?php  echo $longitude; ?>);
map.setCenter(myLatlng);
map.setZoom(<?php echo $default_zoom; ?>);


<?php
	$polygon2=DB::select($q);
	for($i=0;$i<count($polygon2);$i++){
		
		////////////////////////////////////////////////////////////////////////////
		//Campak
		$q_campak = "
			SELECT 
				COUNT(*) AS jml 
			FROM hasil_uji_lab_campak a 
			JOIN pasien b ON b.id_pasien=a.id_pasien
			JOIN campak c ON c.id_campak=a.id_campak
			JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
			LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
			LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
			LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
			LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
			WHERE 
			c.status_at='1'
			AND h.kode_kab = '".$polygon2[$i]->id_kabupaten."'
			AND c.klasifikasi_final IN ('1','2','3')
			AND CASE WHEN c.tanggal_timbul_rash='1970-01-01' OR c.tanggal_timbul_rash='0000-00-00' THEN YEAR(c.tanggal_timbul_demam) ELSE YEAR(c.tanggal_timbul_rash) END BETWEEN '2014' AND '2015'
		";
		$data_campak = DB::select($q_campak);
		//Difteri
		$q_difteri = "
			SELECT 
				COUNT(*) AS jml 
			FROM hasil_uji_lab_difteri a 
			JOIN pasien b ON b.id_pasien=a.id_pasien
			JOIN difteri c ON c.id_difteri=a.id_difteri
			JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
			LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
			LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
			LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
			LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
			WHERE 
			1=1
			AND h.kode_kab = '".$polygon2[$i]->id_kabupaten."'
			AND c.klasifikasi_final IN ('konfirm')
			AND DATE_FORMAT(c.tanggal_timbul_demam,'%Y') >= '2014'
			AND DATE_FORMAT(c.tanggal_timbul_demam,'%Y') <= '2015'
		";
		$data_difteri = DB::select($q_difteri);
		//Tetanus
		$q_tetanus = "
			SELECT 
				COUNT(*) AS jml 
			FROM pasien_terserang_tetanus a 
			JOIN pasien b ON b.id_pasien=a.id_pasien
			JOIN tetanus c ON c.id_tetanus=a.id_tetanus
			JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
			LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
			LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
			LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
			LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
			WHERE 
			1=1
			AND h.kode_kab = '".$polygon2[$i]->id_kabupaten."'
			AND c.klasifikasi_akhir IN ('Konfirm TN')
			AND DATE_FORMAT(c.tanggal_mulai_sakit,'%Y') >= '2014'
			AND DATE_FORMAT(c.tanggal_mulai_sakit,'%Y') <= '2015'
		";
		$data_tetanus = DB::select($q_tetanus);
		$total_penderita_pd3i = $data_campak[0]->jml + $data_difteri[0]->jml + $data_tetanus[0]->jml;
		////////////////////////////////////////////////////////////////////////////
		
		
		$q2 = "SELECT * FROM peta WHERE polygon_id='".$polygon2[$i]->id."' AND MOD(id,1)='0' ORDER BY id ASC";
		$peta=DB::select($q2);	
		$polygon = "";
		$koma = "";
		
		$last_lat = "0";
		$last_lon = "0";
		
		//echo 
		$center_lat = $polygon2[$i]->center_lat;
		//echo "|";
		$center_lon = $polygon2[$i]->center_lon;
		
		
		
		for($j=0;$j<count($peta);$j++){
			$polygon .= $koma;
			$polygon .= "new google.maps.LatLng(".$peta[$j]->latitude.",".$peta[$j]->longitude.")";
			$koma = ",";
			$last_lat = $peta[$j]->latitude;
			$last_lon = $peta[$j]->longitude;
		}
		
		if($center_lat == "0" || $center_lat == "" || $center_lon == "0" || $center_lon == ""){
			
		} else {
			$last_lat = $center_lon;
			//echo "|";
			$last_lon = $center_lat;
		}
		
		$warna_poly = "#afa";
		if($total_penderita_pd3i > 30) $warna_poly = "#f00";
		else if($total_penderita_pd3i > 10) $warna_poly = "#ff0";
		
		$poly_map .= "
			var triangleCoords".$polygon2[$i]->id." = [
				".$polygon."
			];
			Triangle".$now_id.$polygon2[$i]->id." = new google.maps.Polygon({
				paths: triangleCoords".$polygon2[$i]->id.",
				strokeColor: '#000',
				strokeOpacity: 0.5,
				strokeWeight: 1,
				fillColor: '".$warna_poly."',
				fillOpacity: 0.4
			});
			
			Triangle".$now_id.$polygon2[$i]->id.".setMap(map);
		";
		
		$poly_map .= "	
			var opt1_".$polygon2[$i]->id." = 
			{
				fillColor:'#aaf'
			};
			var opt2_".$polygon2[$i]->id." = {
				fillColor:'".$warna_poly."'
			};
			google.maps.event.addListener(Triangle".$now_id.$polygon2[$i]->id.", 'mouseover', function() {
				var currentPolygon = this;
				currentPolygon.setOptions(opt1_".$polygon2[$i]->id.");
				//alert('".$polygon2[$i]->id."');
				//this.setMap(null);
			});
			google.maps.event.addListener(Triangle".$now_id.$polygon2[$i]->id.", 'mouseout', function() {
				var currentPolygon = this;
				currentPolygon.setOptions(opt2_".$polygon2[$i]->id.");
			});
			var lokasi_db  = new google.maps.LatLng(".$last_lat.",".$last_lon.");
			var mapLabel = new MapLabel({
			  text: '".$polygon2[$i]->kabupaten."',
			  position: lokasi_db,
			  map: map,
			  fontSize: 12,
			  align: 'center',
			  minZoom : ".$min_zoom."
			});
		";
		
				
		
		$poly_map .= "
				var content = 
					'<table style=\'width:220px;height:120px;\'><tr><td>'+
					'Kabupaten ".$polygon2[$i]->kabupaten."<br>'+
					'Jumlah Total Pasien : <br>'+
					'<b> '+
					'Campak : ".$data_campak[0]->jml." penderita <br>'+
					'Difteri : ".$data_difteri[0]->jml." penderita <br>'+
					'Tetanus : ".$data_tetanus[0]->jml." penderita <br>'+
					'</b>' +
					'</td></tr></table>'
					;
				var infowindow_Triangle".$now_id.$polygon2[$i]->id." = new google.maps.InfoWindow({
					content: content
				});
				var myLatlng = new google.maps.LatLng(-7.9622172, 110.6031254);
				var marker_".$polygon2[$i]->id." = new google.maps.Marker({
					position: lokasi_db
				});
				";
		$poly_map .= "
				google.maps.event.addListener(Triangle".$now_id.$polygon2[$i]->id.", 'click', function() {
					infowindow_Triangle".$now_id.$polygon2[$i]->id.".open(map,marker_".$polygon2[$i]->id.");
				});
				
				";
		
	}
	
	echo $poly_map; 
	
?>