	
	//###################################################################
	//#BERDASAR BULAN & TAHUN
	//###################################################################
	
	<?php 
		$penyakit = Input::get('penyakit');
		$arr_penyakit['campak'] = "Campak";
		$arr_penyakit['afp'] = "Polio";
		$arr_penyakit['difteri'] = "Difteri";
		$arr_penyakit['tetanus'] = "Tetanus Neonatorum";
		
		$arr_nama_bulan[1] = "Januari";
		$arr_nama_bulan[2] = "Februari";
		$arr_nama_bulan[3] = "Maret";
		$arr_nama_bulan[4] = "April";
		$arr_nama_bulan[5] = "Mei";
		$arr_nama_bulan[6] = "Juni";
		$arr_nama_bulan[7] = "Juli";
		$arr_nama_bulan[8] = "Agustus";
		$arr_nama_bulan[9] = "September";
		$arr_nama_bulan[10] = "Oktober";
		$arr_nama_bulan[11] = "November";
		$arr_nama_bulan[12] = "Desember";
		
		
		$x_data = "";
		$x_categories = "";
		$comma = "";
		
		for($i=date('Y')-1;$i<=date('Y');$i++){
			for($j=1;$j<=12;$j++){
				if($j < 10) $bln_num = "0".$j;
				else $bln_num = $j;
				$bln_name = $arr_nama_bulan[$j];
				
				if($penyakit == "campak") {
					$q = "
						SELECT 
							DATE_FORMAT(c.tanggal_timbul_rash,'%m') AS bln,	
							DATE_FORMAT(c.tanggal_timbul_rash,'%Y') AS tahun,	
							DATE_FORMAT(c.tanggal_timbul_rash,'%m %Y') AS bln_num,	
							IFNULL(COUNT(*),0) AS jml 
						FROM hasil_uji_lab_campak a 
						JOIN pasien b ON b.id_pasien=a.id_pasien
						JOIN campak c ON c.id_campak=a.id_campak
						JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
						LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
						LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
						LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
						LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
						WHERE 
						c.status_at='1'
						AND c.klasifikasi_final IN ('1','2','3')
						AND DATE_FORMAT(c.tanggal_timbul_rash,'%m') = '".$bln_num."'
						AND DATE_FORMAT(c.tanggal_timbul_rash,'%Y') = '".$i."'
						GROUP BY DATE_FORMAT(c.tanggal_timbul_rash,'%m %y')
					";
				} else if($penyakit == "difteri") {
					$q = "
						SELECT 
							DATE_FORMAT(c.tanggal_timbul_demam,'%m') AS bln,	
							DATE_FORMAT(c.tanggal_timbul_demam,'%Y') AS tahun,	
							DATE_FORMAT(c.tanggal_timbul_demam,'%m %Y') AS bln_num,	
							IFNULL(COUNT(*),0) AS jml 
						FROM hasil_uji_lab_difteri a 
						JOIN pasien b ON b.id_pasien=a.id_pasien
						JOIN difteri c ON c.id_difteri=a.id_difteri
						JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
						LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
						LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
						LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
						LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
						WHERE 
						1=1
						AND c.klasifikasi_final IN ('konfirm')
						AND DATE_FORMAT(c.tanggal_timbul_demam,'%m') = '".$bln_num."'
						AND DATE_FORMAT(c.tanggal_timbul_demam,'%Y') = '".$i."'
						GROUP BY DATE_FORMAT(c.tanggal_timbul_demam,'%m %y')
					";
				} else if($penyakit == "tetanus") {
					$q = "
						SELECT 
							DATE_FORMAT(c.tanggal_mulai_sakit,'%m') AS bln,	
							DATE_FORMAT(c.tanggal_mulai_sakit,'%Y') AS tahun,	
							DATE_FORMAT(c.tanggal_mulai_sakit,'%m %Y') AS bln_num,	
							IFNULL(COUNT(*),0) AS jml 
						FROM pasien_terserang_tetanus a 
						JOIN pasien b ON b.id_pasien=a.id_pasien
						JOIN tetanus c ON c.id_tetanus=a.id_tetanus
						JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
						LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
						LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
						LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
						LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
						WHERE 
						1=1
						AND c.klasifikasi_akhir IN ('Konfirm TN')
						AND DATE_FORMAT(c.tanggal_mulai_sakit,'%m') = '".$bln_num."'
						AND DATE_FORMAT(c.tanggal_mulai_sakit,'%Y') = '".$i."'
						GROUP BY DATE_FORMAT(c.tanggal_mulai_sakit,'%m %y')
					";
				}
				
				
				//echo "<br>";
				$data=DB::select($q);
				
				//AND DATE_FORMAT(c.tanggal_periksa,'%m') = '".$j."'
				//AND DATE_FORMAT(c.tanggal_periksa,'%Y') = '".$i."'
					
				$val_jml = 0;
				$val_bln = 0;
				$val_tahun = 0;
				if(count($data)>0) {
					$val_jml = $data[0]->jml;
					$val_bln = $data[0]->bln;
					$val_tahun = $data[0]->tahun;
				}
				$x_data .= 	$comma.$val_jml;
				$x_categories .= $comma."'".$bln_name." ".$i."'";
				$comma = ",";
				
			}
		}
	?>
	
	$('#container_waktu').highcharts({
        title: {
            text: 'Grafik Penderita <?php echo $arr_penyakit[$penyakit] ?> Bulanan',
            x: -20 //center
        },
        subtitle: {
            text: 'Tahun <?php echo date('Y')-1;?> - <?php echo date('Y');?>',
            x: -20
        },
        xAxis: {
            categories: [
			<?php echo $x_categories; ?>
			],
			labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        yAxis: {
            title: {
                text: 'Jumlah Penderita'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' penderita'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 1
        },
        series: [{
            name: 'Jml Penderita',
            data: [
				<?php echo $x_data; ?>
			]
        }
		]
    });
	