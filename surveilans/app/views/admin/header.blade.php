<!DOCTYPE html>
<html lang="en">
<head>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Surveilans PD3I</title>
<link type="text/css" href="{{URL::to('style/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
<link type="text/css" href="{{URL::to('style/bootstrap/css/bootstrap-responsive.min.css')}}" rel="stylesheet">
<link type="text/css" href="{{URL::to('style/css/theme.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{URL::to('asset/css/app.css')}}" type="text/css" cache="false"/>
<link type="text/css" href="{{URL::to('style/images/icons/css/font-awesome.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::to('packages/datatables/css/jquery.dataTables.css')}}" />
<link rel="stylesheet" href="{{ URL::to('asset/css/jquery-ui.css')}}" />
<script src="{{URL::to('style/scripts/jquery-1.9.1.min.js')}}" type="text/javascript"></script>
<script src="{{URL::to('style/scripts/jquery-ui-1.10.1.custom.min.js')}}" type="text/javascript"></script>
<script src="{{URL::to('style/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{URL::to('style/scripts/flot/jquery.flot.js')}}" type="text/javascript"></script>
<script src="{{URL::to('style/scripts/flot/jquery.flot.resize.js')}}" type="text/javascript"></script>
<script src="{{URL::to('style/scripts/datatables/jquery.dataTables.js')}}" type="text/javascript"></script>

<script src="{{URL::to('style/scripts/jquery.form.js')}}" type="text/javascript"></script>
<script src="{{URL::to('style/scripts/jquery.validate.js')}}" type="text/javascript"></script>
<!--<script src="{{URL::to('style/scripts/common.js')}}" type="text/javascript"></script>-->
<!--{{ HTML::script('asset/js/app1.js') }}-->
</head>
<body>