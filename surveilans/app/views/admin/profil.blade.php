@extends('layouts.master')
@section('content')
<div class="wrapper">
    <div class="container">
        <div class="row">
            <!--/.span3-->
            <div class="span12">
                <div class="content">
                    <div class="btn-controls">
                        
                        <div class="btn-box-row row-fluid">
                            <ul class="widget widget-usage unstyled span12">
                                <div class="module-head">
                                    <h3>Daftar</h3>
                                </div>
                                <div class="module-body">
									{{Form::open(array('url'=>'save_profil','class'=>'form-horizontal row-fluid'))}}
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Provinsi</label>
											<div class="controls span6">
												<select name="province_id" id="province_id" style="width:200px" onchange="get_district();" >
													<option value="">--- Semua ---</option>
													<?php
													$q = "
														SELECT 
															id_provinsi,
															provinsi
														FROM provinsi
														order by provinsi ASC
													";
													$combo_district=DB::select($q);
													?>
													<?php for($i=0;$i<count($combo_district);$i++) :?>
													<?php //if($combo_district[$i]->id_provinsi == "34") $sel='selected'; else $sel='';?>
													<option value="<?php echo $combo_district[$i]->id_provinsi?>" <?php //echo $sel; ?>><?php echo $combo_district[$i]->provinsi; ?></option>
													<?php endfor;?>
												</select>
												
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Kabupaten</label>
											<div class="controls span6">
												<select name="district_id" id="district_id" style="width: 190px;" disabled="disabled"  onchange="get_sub_district();">
												</select>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Kecamatan</label>
											<div class="controls span6">
												<select name="sub_district_id" id="sub_district_id" style="width: 190px;" disabled="disabled"  onchange="get_village();">
												</select>
												
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Puskesmas</label>
											<div class="controls span6">
												<input name="nama" type="text" class="form-control span8"/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Kode Puskesmas</label>
											<div class="controls span6">
												<input name="nama" type="text" class="form-control span8"/>
											</div>
										</div>
										
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Kode Konfirmasi</label>
											<div class="controls span6">
												<input name="nama" type="text" class="form-control span8"/>
											</div>
										</div>

										
										<div class="control-group">
											<div class="controls">
												<button type="submit" class="btn btn-success">Simpan</button>
												<button type="reset" class="btn">Batal</button>
												<a href="{{URL::to('')}}" class="btn btn-warning">Kembali</a>
											</div>
										</div>
									{{Form::close()}}
							</div>
                            </ul>
                        </div>
                    </div>
                    
                    <!--/.module-->
                </div>
                <!--/.content-->
            </div>
            <!--/.span9-->
        </div>
    </div>
    <!--/.container-->
</div>

<script>
	
function get_district(){
	$.ajax({
		//dataType:'json',
		data: 'province_id='+$('#province_id').val(),
		url:'{{URL::to("region/get_district")}}',
		success:function(data) {
			$('#district_id').html(data);
			if($('#province_id').val()=='') {
				$('#district_id').attr('disabled',true);
				$('#sub_district_id').attr('disabled',true);
				$('#village_id').attr('disabled',true);
			} else {
				$('#district_id').removeAttr('disabled');
			}	
			get_sub_district();
			get_village();
		}
	});
}	

function get_sub_district(){
	$.ajax({
		//dataType:'json',
		data: 'district_id='+$('#district_id').val(),
		url:'{{URL::to("region/get_sub_district")}}',
		success:function(data) {
			//alert(data);
			$('#sub_district_id').html(data);
			if($('#district_id').val()=='') {
				$('#sub_district_id').attr('disabled',true);
				$('#village_id').attr('disabled',true);
			} else {
				$('#sub_district_id').removeAttr('disabled');
			}
			get_village();
		}
	});
}	

function get_village(){
	$.ajax({
		//dataType:'json',
		data: 'sub_district_id='+$('#sub_district_id').val(),
		url:'{{URL::to("region/get_village")}}',
		success:function(data) {
			//alert(data);
			$('#village_id').html(data);
			if($('#sub_district_id').val()=='') $('#village_id').attr('disabled',true);
			else $('#village_id').removeAttr('disabled');
		}
	});
}
	
</script>

<!--/.wrapper-->
@stop