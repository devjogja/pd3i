<!-- DINAS KESEHATAN KABUPATEN -->
<?php
$q = "
	SELECT a.id,b.kabupaten,b.id_kabupaten,a.alamat
	FROM profil_dinkes_kabupaten a
	JOIN kabupaten b ON b.id_kabupaten=a.kode_kabupaten
	WHERE a.created_by='".Sentry::getUser()->id."'
";
$data=DB::select($q);
for($is=0;$is<count($data);$is++):
?>
<div class="control-group">
	<label class="control-label span6" for="basicinput">
		<a href="profil_id?type=kabupaten&kd_faskes=<?php echo $data[$is]->id_kabupaten; ?>&id_profil=<?php echo $data[$is]->id; ?>&id_profil=<?php echo $data[$is]->id; ?>&type_profil=kabupaten">
			KABUPATEN <?php echo $data[$is]->kabupaten; ?><br>
			<?php echo $data[$is]->alamat; ?>
		</a>
	</label>
	<label class="control-label span4" for="basicinput">
		<?php
			$q = "
				SELECT COUNT(*) as jml
				FROM profil_dinkes_kabupaten a
				WHERE a.is_admin='y' AND a.kode_kabupaten ='".$data[$is]->id_kabupaten."'
			";
			$data_jml=DB::select($q);
			echo $data_jml[0]->jml;
			$btn_type = '';
			$btn_disabled = 'disabled="disabled"';
			if($data_jml[0]->jml < 2){
				$btn_type = 'btn-success';
				$btn_disabled = '';
			}
			 
		?>
		Admin
		<a href="#" onClick="konfirmasiAdmin('kabupaten','<?php echo $data[$is]->id; ?>');" class="btn <?php echo $btn_type; ?>" <?php echo $btn_disabled; ?>>Jadikan Saya Admin</a>
		<a href="#" onClick="konfirmasiDelete('kabupaten','<?php echo $data[$is]->id; ?>');" class="btn btn-danger">Hapus Profil</a>
	
		<script>
			$(document).ready(function(){
					
					$( "#accordion_kabupaten_<?php echo $data[$is]->id; ?>" ).accordion({
							collapsible: true,
							active:false,
							heightStyle: "content",
							activate: function( event, ui ) {
								
							}
						});
						
			
					<?php
						$q_kab = "
							SELECT * 
							FROM kabupaten
							WHERE id_kabupaten = '".$data[$is]->id_kabupaten."'
						";
						$data_kab=DB::select($q_kab);
						for($j=0;$j<count($data_kab);$j++):													
						?>
						$( "#accordion_kabupaten_<?php echo $data[$is]->id; ?>_<?php echo $data_kab[$j]->id_kabupaten; ?>" ).accordion({
							collapsible: true,
							active:false,
							heightStyle: "content",
							activate: function( event, ui ) {
								
							}
						});
						
					<?php endfor; ?>
			});
		</script>
	</label>
	<label class="control-label span10" for="basicinput">
		<div id="accordion_kabupaten_<?php echo $data[$is]->id; ?>" class="accordion">
			<?php
				$q_kab = "
					SELECT * 
					FROM kabupaten
					WHERE  id_kabupaten = '".$data[$is]->id_kabupaten."'
				";
				$data_kab=DB::select($q_kab);
				for($j=0;$j<count($data_kab);$j++):													
			?>
				
			<h3>KABUPATEN <?php echo $data_kab[$j]->kabupaten; ?></h3>
			<div>
				<a href="profil_id?type=kabupaten&kd_faskes=<?php echo $data_kab[$j]->id_kabupaten; ?>&id_profil=<?php echo $data[$is]->id; ?>&type_profil=kabupaten" class="btn btn-success span12">
					Masuk sebagai petugas surveilan DKK <?php echo $data_kab[$j]->kabupaten; ?>
				</a>
				<br><br>
				<?php
					$q_rs = "
						SELECT * 
						FROM rumahsakit2
						WHERE kode_kab IN ('".$data_kab[$j]->id_kabupaten."')
					";
					$data_rs=DB::select($q_rs);
					for($k=0;$k<count($data_rs);$k++):?>
					<a href="profil_id?type=rs&kd_faskes=<?php echo $data_rs[$k]->kode_faskes; ?>&id_profil=<?php echo $data[$is]->id; ?>&type_profil=kabupaten" class="btn btn-success span12">
						Masuk sebagai petugas surveilan Rumahsakit <?php echo $data_rs[$k]->nama_faskes; ?>
					</a>
				<br><br>
				<?php endfor;?>
				<div id="accordion_kabupaten_<?php echo $data[$is]->id; ?>_<?php echo $data_kab[$j]->id_kabupaten; ?>">
					<?php
					$q_kec = "
						SELECT * 
						FROM kecamatan
						WHERE id_kabupaten IN ('".$data_kab[$j]->id_kabupaten."')
					";
					$data_kec=DB::select($q_kec);
					for($k=0;$k<count($data_kec);$k++):													
					?>
					
					<h3>KECAMATAN <?php echo $data_kec[$k]->kecamatan; ?></h3>
					<div>
						<?php
						$q_pus = "
							SELECT * 
							FROM puskesmas
							WHERE kode_kec IN ('".$data_kec[$k]->id_kecamatan."')
						";
						$data_pus=DB::select($q_pus);
						for($l=0;$l<count($data_pus);$l++):													
						?>
							<a href="profil_id?type=puskesmas&kd_faskes=<?php echo $data_pus[$l]->puskesmas_code_faskes; ?>&id_profil=<?php echo $data[$is]->id; ?>&type_profil=kabupaten" class="btn btn-success span12">
								Masuk sebagai petugas surveilan PUSKESMAS <?php echo $data_pus[$l]->puskesmas_name; ?>
							</a>
							<br><br>
						<?php endfor; ?>
					</div>
					<?php endfor; ?>
				</div>
			</div>
			<?php endfor; ?>
		</div>	
	</label>
</div>
<?php endfor; ?>
