<!-- PUSKESMAS -->
<?php
if(!Sentry::check()) {

	return Redirect::to('/');
}else {
	$id = Sentry::getUser()->id;
}
$q = "
	SELECT a.id,b.puskesmas_name,a.kode_faskes,b.alamat
	FROM profil_puskesmas a
	JOIN puskesmas b ON b.puskesmas_code_faskes=a.kode_faskes
	WHERE a.created_by='".$id."'
";
$data=DB::select($q);
for($is=0;$is<count($data);$is++):
?>
<div class="control-group">
	<label class="control-label span6" for="basicinput">
		<a href="profil_id?type=puskesmas&kd_faskes=<?php echo $data[$is]->kode_faskes; ?>&id_profil=<?php echo $data[$is]->id; ?>&type_profil=puskesmas">
			PUSKESMAS <?php echo $data[$is]->puskesmas_name; ?><br>
			<i><?php echo $data[$is]->alamat; ?></i>
		</a>
	</label>
	<label class="control-label span4" for="basicinput">
		<?php
			$q = "
				SELECT COUNT(*) as jml
				FROM profil_puskesmas a
				WHERE a.is_admin='y' AND a.kode_faskes ='".$data[$is]->kode_faskes."'
			";
			$data_jml=DB::select($q);
			echo $data_jml[0]->jml;
			$btn_type = '';
			$btn_disabled = 'disabled="disabled"';
			if($data_jml[0]->jml < 2){
				$btn_type = 'btn-success';
				$btn_disabled = '';
			}
			 
		?>
		Admin
		<a href="#" onClick="konfirmasiAdmin('puskesmas','<?php echo $data[$is]->id; ?>');" class="btn <?php echo $btn_type; ?>" <?php echo $btn_disabled; ?>>Jadikan Saya Admin</a>
		<a href="#" onClick="konfirmasiDelete('puskesmas','<?php echo $data[$is]->id; ?>');" class="btn btn-danger">Hapus Profil</a>
	</label>
</div>
<?php endfor; ?>