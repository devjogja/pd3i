@include('admin.header')
<script>
$(document).ready(function(){
	$('#inputForm').submit(function() {
		loading_state();
		$('#inputForm').ajaxSubmit({
			type: 'POST',
			dataType:'json',
			success:function(jsonData) {
				$('#status').html(jsonData.message);
				if(jsonData.status == 'success')location.href  = jsonData.address;
				//alert(jsonData.address);
			}
		});
		return false;
	});
});

function loading_state(){
	$('#status').html('Loading...');
}	
</script>
@include('admin.heading')
<div class="wrapper">
    <div class="container">
        <div class="row">
            <!--/.span3-->
            <div class="span12">
                <div class="content">
                    <div class="btn-controls">
                        
                        <div class="btn-box-row row-fluid">
                            <ul class="widget widget-usage unstyled span12">
                                <div class="module-head">
                                    <h3>Profil Puskesmas</h3>
                                </div>
                                <div class="module-body">
									{{Form::open(array('url'=>'save_profil_puskesmas','class'=>'form-horizontal row-fluid','id'=>'inputForm'))}}
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Provinsi</label>
											<div class="controls span6">
												<select name="province_id" id="province_id" style="width:200px" onchange="get_district();" >
													<option value="">--- Semua ---</option>
													<?php
													$q = "
														SELECT 
															id_provinsi,
															provinsi
														FROM provinsi
														order by provinsi ASC
													";
													$combo_district=DB::select($q);
													?>
													<?php for($i=0;$i<count($combo_district);$i++) :?>
													<?php //if($combo_district[$i]->id_provinsi == "34") $sel='selected'; else $sel='';?>
													<option value="<?php echo $combo_district[$i]->id_provinsi?>" <?php //echo $sel; ?>><?php echo $combo_district[$i]->provinsi; ?></option>
													<?php endfor;?>
												</select>
												
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Kabupaten</label>
											<div class="controls span6">
												<select name="district_id" id="district_id" style="width: 190px;" disabled="disabled"  onchange="get_sub_district();">
												</select>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Kecamatan</label>
											<div class="controls span6">
												<select name="sub_district_id" id="sub_district_id" style="width: 190px;" disabled="disabled"  onchange="get_puskesmas();">
												</select>
												
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Puskesmas</label>
											<div class="controls span6">
												<select name="puskesmas_id" id="puskesmas_id" style="width: 190px;" disabled="disabled"  onchange="get_code();">
												</select>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Kode Puskesmas</label>
											<div class="controls span6">
												<input name="puskesmas_code" id="puskesmas_code" type="text" class="form-control span8"/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Kode Konfirmasi</label>
											<div class="controls span6">
												<input name="kode_konfirmasi" id="kode_konfirmasi" type="text" class="form-control span8"/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Alamat</label>
											<div class="controls span6">
												<input name="alamat" id="alamat" type="text" class="form-control span8"/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Penanggungjawab 1</label>
											<div class="controls span6">
												<input name="penanggungjawab_1" type="text" class="form-control span8"/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Penanggungjawab 2</label>
											<div class="controls span6">
												<input name="penanggungjawab_2" type="text" class="form-control span8"/>
											</div>
										</div>										
										
										<div class="control-group">
											<div class="controls">
												<div class="span3">
													<button type="submit" class="btn btn-success">Simpan</button>
													<button type="reset" class="btn">Batal</button>
													<a href="{{URL::to('setting/level')}}" class="btn btn-warning">Kembali</a>
												</div>
												<div class="span5" id="status">&nbsp;</div>
											</div>
										</div>
									{{Form::close()}}
							</div>
                            </ul>
                        </div>
                    </div>
                    
                    <!--/.module-->
                </div>
                <!--/.content-->
            </div>
            <!--/.span9-->
        </div>
    </div>
    <!--/.container-->
</div>

<script>
	
function get_district(){
	$.ajax({
		//dataType:'json',
		data: 'province_id='+$('#province_id').val(),
		url:'{{URL::to("region/get_district")}}',
		success:function(data) {
			$('#district_id').html(data);
			if($('#province_id').val()=='') {
				$('#district_id').attr('disabled',true);
				$('#sub_district_id').attr('disabled',true);
				$('#puskesmas_id').attr('disabled',true);
			} else {
				$('#district_id').removeAttr('disabled');
			}	
			get_sub_district();
			get_puskesmas();
		}
	});
}	

function get_sub_district(){
	$.ajax({
		//dataType:'json',
		data: 'district_id='+$('#district_id').val(),
		url:'{{URL::to("region/get_sub_district")}}',
		success:function(data) {
			//alert(data);
			$('#sub_district_id').html(data);
			if($('#district_id').val()=='') {
				$('#sub_district_id').attr('disabled',true);
				$('#puskesmas_id').attr('disabled',true);
			} else {
				$('#sub_district_id').removeAttr('disabled');
			}
			get_puskesmas();
		}
	});
}	

function get_puskesmas(){
	$.ajax({
		//dataType:'json',
		data: 'sub_district_id='+$('#sub_district_id').val(),
		url:'{{URL::to("region/get_puskesmas")}}',
		success:function(data) {
			//alert(data);
			$('#puskesmas_id').html(data);
			if($('#sub_district_id').val()=='') $('#puskesmas_id').attr('disabled',true);
			else $('#puskesmas_id').removeAttr('disabled');
		}
	});
}

function get_code(){
	$('#puskesmas_code').val($('#puskesmas_id').val());
	$.ajax({
		//dataType:'json',
		data: 'puskesmas_code='+$('#puskesmas_code').val(),
		url:'{{URL::to("region/get_puskesmas_detail")}}',
		success:function(data) {
			//alert(data);
			$('#alamat').val(data);
			if($('#sub_district_id').val()=='') $('#puskesmas_code').attr('disabled',true);
			else $('#puskesmas_code').removeAttr('disabled');
		}
	});
}
</script>

@include('admin.footer')
