@extends('layouts.master')
@section('content')
<div class="wrapper">
    <div class="container">
        <div class="row">
            <!--/.span3-->
            <div class="span12">
                <div class="content">
                    <div class="btn-controls">
                        
                        <div class="btn-box-row row-fluid">
                            <ul class="widget widget-usage unstyled span12">
                                <div class="module-head">
                                    <h3>Tambah profil</h3>
                                </div>
                                <div class="module-body">
									{{Form::open(array('url'=>'','class'=>'form-horizontal row-fluid'))}}

										<div class="control-group">
											<label class="control-label span3" for="basicinput">Nama xx</label>
											<div class="controls span8">
												{{Form::text('nama_puskesmas',null,array('class'=>'input-large'))}}
											</div>
										</div>
                                        <div class="control-group">
                                            <label class="control-label span3" for="basicinput">Nama xx</label>
                                            <div class="controls span8">
                                                {{Form::text('nama_puskesmas',null,array('class'=>'input-large'))}}
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label span3" for="basicinput">Nama xx</label>
                                            <div class="controls span8">
                                                {{Form::text('nama_puskesmas',null,array('class'=>'input-large'))}}
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label span3" for="basicinput">Nama xx</label>
                                            <div class="controls span8">
                                                {{Form::text('nama_puskesmas',null,array('class'=>'input-large'))}}
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label span3" for="basicinput">Nama xx</label>
                                            <div class="controls span8">
                                                {{Form::text('nama_puskesmas',null,array('class'=>'input-large'))}}
                                            </div>
                                        </div>

										<div class="control-group">
											<div class="controls">
												<button type="submit" class="btn btn-success">Simpan</button>
												<button type="reset" class="btn">Batal</button>
												<a href="{{URL::to('')}}" class="btn btn-warning">Kembali</a>
											</div>
										</div>
									{{Form::close()}}
							    </div>
                            </ul>
                        </div>
                    </div>
                    
                    <!--/.module-->
                </div>
                <!--/.content-->
            </div>
            <!--/.span9-->
        </div>
    </div>
    <!--/.container-->
</div>
<!--/.wrapper-->
@stop
