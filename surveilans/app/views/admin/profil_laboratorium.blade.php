@include('admin.header')
<script>
$(document).ready(function(){
	$('#inputForm').submit(function() {
		loading_state();
		$('#inputForm').ajaxSubmit({
			type: 'POST',
			dataType:'json',
			success:function(jsonData) {
				$('#status').html(jsonData.message);
				if(jsonData.status == 'success')location.href  = jsonData.address;
				//alert(jsonData.address);
			}
		});
		return false;
	});
});

function loading_state(){
	$('#status').html('Loading...');
}	
</script>
@include('admin.heading')

<div class="wrapper">
    <div class="container">
        <div class="row">
            <!--/.span3-->
            <div class="span12">
                <div class="content">
                    <div class="btn-controls">
                        
                        <div class="btn-box-row row-fluid">
                            <ul class="widget widget-usage unstyled span12">
                                <div class="module-head">
                                    <h3>Profil Laboratorium</h3>
                                </div>
                                <div class="module-body">
									{{Form::open(array('url'=>'save_profil_laboratorium','class'=>'form-horizontal row-fluid','id'=>'inputForm'))}}
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Laboratorium</label>
											<div class="controls span6">
												<select name="laboratorium_id" id="laboratorium_id" style="width:200px" onchange="get_code();" >
													<option value="">--- Semua ---</option>
													<?php
													$q = "
														SELECT 
															id_laboratorium,
															lab_code,
															nama_laboratorium,
															alamat
														FROM laboratorium
														order by nama_laboratorium ASC
													";
													$combo_lab=DB::select($q);
													?>
													<?php for($i=0;$i<count($combo_lab);$i++) :?>
													<?php //if($combo_district[$i]->id_provinsi == "34") $sel='selected'; else $sel='';?>
													<option value="<?php echo $combo_lab[$i]->lab_code?>" <?php //echo $sel; ?>><?php echo $combo_lab[$i]->nama_laboratorium; ?></option>
													<?php endfor;?>
												</select>
												
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Kode Laboratorium</label>
											<div class="controls span6">
												<input name="laboratorium_code" id="laboratorium_code" type="text" class="form-control span8"/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Kode Konfirmasi</label>
											<div class="controls span6">
												<input name="kode_konfirmasi" id="kode_konfirmasi" type="text" class="form-control span8"/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Alamat</label>
											<div class="controls span6">
												<input name="alamat" id="alamat" type="text" class="form-control span8"/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Penanggungjawab 1</label>
											<div class="controls span6">
												<input name="penanggungjawab_1" type="text" class="form-control span8"/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Penanggungjawab 2</label>
											<div class="controls span6">
												<input name="penanggungjawab_2" type="text" class="form-control span8"/>
											</div>
										</div>										
										
										<div class="control-group">
											<div class="controls">
												<div class="span3">
													<button type="submit" class="btn btn-success">Simpan</button>
													<button type="reset" class="btn">Batal</button>
													<a href="{{URL::to('setting/level')}}" class="btn btn-warning">Kembali</a>
												</div>
												<div class="span5" id="status">&nbsp;</div>
											</div>
										</div>
									{{Form::close()}}
							</div>
                            </ul>
                        </div>
                    </div>
                    
                    <!--/.module-->
                </div>
                <!--/.content-->
            </div>
            <!--/.span9-->
        </div>
    </div>
    <!--/.container-->
</div>

<script>
function get_code(){
	$('#laboratorium_code').val($('#laboratorium_id').val());
	$.ajax({
		//dataType:'json',
		data: 'lab_code='+$('#laboratorium_code').val(),
		url:'{{URL::to("region/get_laboratorium_detail")}}',
		success:function(data) {
			//alert(data);
			$('#alamat').val(data);
			if($('#laboratorium_id').val()=='') $('#laboratorium_code').attr('disabled',true);
			else $('#laboratorium_code').removeAttr('disabled');
		}
	});
}
</script>

@include('admin.footer')
