<?php
		$penyakit = Input::get('penyakit');
		$arr_penyakit['campak'] = "Campak";
		$arr_penyakit['afp'] = "Polio";
		$arr_penyakit['difteri'] = "Difteri";
		$arr_penyakit['tetanus'] = "Tetanus Neonatorum";
		
		$data=DB::select("
			SELECT * 
			FROM provinsi	
		");

		$arr_penyakit_x = array();
		$arr_penyakit_x[0] = $arr_penyakit[$penyakit];
		
		$arr_penyakit_table[0] = "campak";
		
		$comma= "";
		$series = "";
		for($i=0;$i<count($arr_penyakit_x);$i++){			
			$comma2= "";
			$x_data = "";
			for($j=0;$j<count($data);$j++){
				if($penyakit == "campak") {
					$q2 = "
					SELECT 
						COUNT(*) AS jml 
					FROM hasil_uji_lab_campak a 
					JOIN pasien b ON b.id_pasien=a.id_pasien
					JOIN campak c ON c.id_campak=a.id_campak
					JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
					LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
					LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
					LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
					LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
					WHERE 
					c.status_at='1'
					AND c.klasifikasi_final IN ('1','2','3')
					AND h.kode_prop = '".$data[$j]->id_provinsi."'
					AND CASE WHEN c.tanggal_timbul_rash='1970-01-01' OR c.tanggal_timbul_rash='0000-00-00' THEN YEAR(c.tanggal_timbul_demam) ELSE YEAR(c.tanggal_timbul_rash) END BETWEEN '2014' AND '2015'
					";
				} else if($penyakit == "difteri") {
					$q2 = "
					SELECT 
						COUNT(*) AS jml 
					FROM hasil_uji_lab_difteri a 
					JOIN pasien b ON b.id_pasien=a.id_pasien
					JOIN difteri c ON c.id_difteri=a.id_difteri
					JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
					LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
					LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
					LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
					LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
					WHERE 
					1=1
					AND c.klasifikasi_final IN ('konfirm')
					AND h.kode_prop = '".$data[$j]->id_provinsi."'
					AND DATE_FORMAT(c.tanggal_timbul_demam,'%Y') >= '2014'
					AND DATE_FORMAT(c.tanggal_timbul_demam,'%Y') <= '2015'
					";
				} else if($penyakit == "tetanus") {
					$q2 = "
					SELECT 
						COUNT(*) AS jml 
					FROM pasien_terserang_tetanus a 
					JOIN pasien b ON b.id_pasien=a.id_pasien
					JOIN tetanus c ON c.id_tetanus=a.id_tetanus
					JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
					LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
					LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
					LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
					LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
					WHERE 
					1=1
					AND c.klasifikasi_akhir IN ('Konfirm TN')
					AND h.kode_prop = '".$data[$j]->id_provinsi."'
					AND DATE_FORMAT(c.tanggal_mulai_sakit,'%Y') >= '2014'
					AND DATE_FORMAT(c.tanggal_mulai_sakit,'%Y') <= '2015'
					";
				}
				
				$data2=DB::select($q2);
				
				$jml = "0";
				if(count($data2) > 0){
					$jml = $data2[0]->jml;
				} 
				
				$x_data .= $comma2.$jml;	
				$comma2 = ",";	
			}
			
			$series .= $comma."
			{
                name: '".$arr_penyakit_x[$i]."',
                data: [".$x_data."]
            }";
			$comma = ",";
		}
		
		$categories = "";
		$comma= "";
		for($i=0;$i<count($data);$i++){
			$categories .= $comma."'".$data[$i]->provinsi."'";	
			$comma = ",";	
		}
		
	?>
	
	$('#grafik_per_wilayah').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Grafik Total Penderita <?php echo $arr_penyakit[$penyakit]; ?> Per Wilayah'
            },
			subtitle: {
				text: 'Tahun  <?php echo date('Y')-1;?> - <?php echo date('Y');?>'
			},
            xAxis: {
                categories: [<?php echo $categories; ?>],
				labels: {
					rotation: -90,
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				}
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total Penderita'
                },
                stackLabels: {
                    //enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: -70,
                verticalAlign: 'top',
                y: 20,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b><br/>'+
                        this.series.name +': '+ this.y +'<br/>'+
                        'Total: '+ this.point.stackTotal;
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
						rotation: -90,
						color: '#FFFFFF',
						align: 'right',
						format: '{point.y:.1f}', // one decimal
						y: -10, // 10 pixels down from the top
						x:2,
						style: {
							fontSize: '10px',
							fontFamily: 'Verdana, sans-serif'
						}
                    }
                }
            },
            series: [
			<?php
				echo $series;
			?>
			]
        });