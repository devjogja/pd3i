@include('admin.header')
<script>
$(document).ready(function(){
	$('#signup').submit(function() {
		loading_state();
		$('#signup').ajaxSubmit({
			type: 'POST',
			dataType:'json',
			success:function(jsonData) {
				$('#status').html(jsonData.message);
				if(jsonData.status == 'success')location.href  = jsonData.address;
			}
		});
		return false;
	});
});

function loading_state(){
	$('#status').html('Loading...');
}	

</script>
@include('admin.heading')
<div class="wrapper">
    <div class="container">
        <div class="row">
            <!--/.span3-->
            <div class="span12">
                <div class="content">
                    <div class="btn-controls">
                        
                        <div class="btn-box-row row-fluid">
                            <ul class="widget widget-usage unstyled span12">
                                <div class="module-head">
                                    <h3>Daftar</h3>
                                </div>
                                <div class="module-body">
									{{Form::open(array('url'=>'save_signup','class'=>'form-horizontal row-fluid','id'=>'signup'))}}
										<div class="control-group">
											<label class="control-label span3" for="basicinput">First Name</label>
											<div class="controls span6">
												<input name="first_name" type="text" class="form-control span8"/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Last Name</label>
											<div class="controls span6">
												<input name="last_name" type="text" class="form-control span8"/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Instansi</label>
											<div class="controls span6">
												<input name="instansi" type="text" class="form-control span8" id="date">
											</div>
										</div>

										<div class="control-group">
											<label class="control-label span3" for="basicinput">Jabatan</label>
											<div class="controls span6">
												<input name="jabatan" type="text" class="form-control span8" id="date">
											</div>
										</div>

										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">E-mail</label>
											<div class="controls span6">
												<input name="email" type="email" class="form-control span8" id="date">
											</div>
										</div>

										<div class="control-group">
											<label class="control-label span3" for="basicinput">Password</label>
											<div class="controls span6">
												<input name="password" type="password" class="form-control span8"/>  
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Ulangi Password</label>
											<div class="controls span6">
												<input name="password2" type="password" class="form-control span8"/>  
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Nomor Telp / HP</label>
											<div class="controls span6">
												<input name="no_telp" type="text" class="form-control span8"/>  
											</div>
										</div>

										<div class="control-group">
											<label class="control-label span3" for="basicinput">Jenis kelamin</label>
											<div class="controls span6">
												<select name="jk">
													<option value="l">Laki-laki</option>
													<option value="p">Perempuan</option>
												</select>
											</div>
										</div>

										<div class="control-group">
											<div class="controls">
												<div class="span3">
													<button type="submit" class="btn btn-success">Simpan</button>
													<button type="reset" class="btn">Batal</button>
													<a href="{{URL::to('')}}" class="btn btn-warning">Kembali</a>
												</div>
												<div class="span3" id="status">&nbsp;</div>
											</div>
										</div>
									{{Form::close()}}
							</div>
                            </ul>
                        </div>
                    </div>
                    
                    <!--/.module-->
                </div>
                <!--/.content-->
            </div>
            <!--/.span9-->
        </div>
    </div>
    <!--/.container-->
</div>
@include('admin.footer')