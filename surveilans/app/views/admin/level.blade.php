@include('admin.header')
@include('admin.heading')
<div class="wrapper">
    <div class="container">
        <div class="row">
            <!--/.span3-->
            <div class="span12">
                <div class="content">
                    <div class="btn-controls">
                        
                        <div class="btn-box-row row-fluid">
                            <ul class="widget widget-usage unstyled span12">
                                <div class="module-head">
                                    <h3>Pilih Wilayah Administrasi</h3>
                                </div>
                                <div class="module-body">
									{{Form::open(array('url'=>'save_level','class'=>'form-horizontal row-fluid'))}}

										<div class="control-group">
											<label class="control-label span3" for="basicinput">Level</label>
											<div class="controls span6">
												<select placeholder="Pilih Kelurahan" name="level">
													<option value="puskesmas">Puskesmas</option>
													<option value="rs">Rumah Sakit</option>
													<option value="laboratorium">Laboratorium</option>
													<option value="pusat">Pusat</option>
													<option value="provinsi">Provinsi</option>
													<option value="kabupaten">Kabupaten</option>
												</select>
											</div>
										</div>

										<div class="control-group">
											<div class="controls">
												<button type="submit" class="btn btn-success">Simpan</button>
												<button type="reset" class="btn">Batal</button>
												<a href="{{URL::to('')}}" class="btn btn-warning">Kembali</a>
											</div>
										</div>
									{{Form::close()}}
							</div>
                            </ul>
                        </div>
                    </div>
                    
                    <!--/.module-->
                </div>
                <!--/.content-->
            </div>
            <!--/.span9-->
        </div>
    </div>
    <!--/.container-->
</div>
@include('admin.footer')
