@include('admin.header')
<script>
$(document).ready(function(){
	$('#inputForm').submit(function() {
		loading_state();
		$('#inputForm').ajaxSubmit({
			type: 'POST',
			dataType:'json',
			success:function(jsonData) {
				$('#status').html(jsonData.message);
				if(jsonData.status == 'success')location.href  = jsonData.address;
				//alert(jsonData.address);
			}
		});
		return false;
	});
});

function loading_state(){
	$('#status').html('Loading...');
}	
</script>
@include('admin.heading')
<div class="wrapper">
    <div class="container">
        <div class="row">
            <!--/.span3-->
            <div class="span12">
                <div class="content">
                    <div class="btn-controls">
                        
                        <div class="btn-box-row row-fluid">
                            <ul class="widget widget-usage unstyled span12">
                                <div class="module-head">
                                    <h3>Profil Rumah Sakit</h3>
                                </div>
                                <div class="module-body">
									{{Form::open(array('url'=>'save_profil_rs','class'=>'form-horizontal row-fluid','id'=>'inputForm'))}}
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Provinsi</label>
											<div class="controls span6">
												<select name="province_id" id="province_id" style="width:200px" onchange="get_district();" >
													<option value="">--- Semua ---</option>
													<?php
													$q = "
														SELECT 
															id_provinsi,
															provinsi
														FROM provinsi
														order by provinsi ASC
													";
													$combo_district=DB::select($q);
													?>
													<?php for($i=0;$i<count($combo_district);$i++) :?>
													<?php //if($combo_district[$i]->id_provinsi == "34") $sel='selected'; else $sel='';?>
													<option value="<?php echo $combo_district[$i]->id_provinsi?>" <?php //echo $sel; ?>><?php echo $combo_district[$i]->provinsi; ?></option>
													<?php endfor;?>
												</select>
												
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Kabupaten</label>
											<div class="controls span6">
												<select name="district_id" id="district_id" style="width: 190px;" disabled="disabled"  onchange="get_rs();">
												</select>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Rumah Sakit</label>
											<div class="controls span6">
												<select name="rs_id" id="rs_id" style="width: 190px;" disabled="disabled"  onchange="get_code();">
												</select>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Kode Rumah Sakit</label>
											<div class="controls span6">
												<input name="rs_code" id="rs_code" type="text" class="form-control span8"/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Kode Konfirmasi</label>
											<div class="controls span6">
												<input name="kode_konfirmasi" id="kode_konfirmasi" type="text" class="form-control span8"/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Alamat</label>
											<div class="controls span6">
												<input name="alamat" id="alamat" type="text" class="form-control span8"/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Penanggungjawab 1</label>
											<div class="controls span6">
												<input name="penanggungjawab_1" type="text" class="form-control span8"/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Penanggungjawab 2</label>
											<div class="controls span6">
												<input name="penanggungjawab_2" type="text" class="form-control span8"/>
											</div>
										</div>										
										
										<div class="control-group">
											<div class="controls">
												<div class="span3">
													<button type="submit" class="btn btn-success">Simpan</button>
													<button type="reset" class="btn">Batal</button>
													<a href="{{URL::to('setting/level')}}" class="btn btn-warning">Kembali</a>
												</div>
												<div class="span5" id="status">&nbsp;</div>
											</div>
										</div>
									{{Form::close()}}
							</div>
                            </ul>
                        </div>
                    </div>
                    
                    <!--/.module-->
                </div>
                <!--/.content-->
            </div>
            <!--/.span9-->
        </div>
    </div>
    <!--/.container-->
</div>

<script>
	
function get_district(){
	$.ajax({
		//dataType:'json',
		data: 'province_id='+$('#province_id').val(),
		url:'{{URL::to("region/get_district")}}',
		success:function(data) {
			$('#district_id').html(data);
			if($('#province_id').val()=='') {
				$('#district_id').attr('disabled',true);
				$('#sub_district_id').attr('disabled',true);
				$('#rs_id').attr('disabled',true);
			} else {
				$('#district_id').removeAttr('disabled');
			}	
			get_rs();
		}
	});
}	


function get_rs(){
	$.ajax({
		//dataType:'json',
		data: 'district_id='+$('#district_id').val(),
		url:'{{URL::to("region/get_rs")}}',
		success:function(data) {
			//alert(data);
			$('#rs_id').html(data);
			if($('#district_id').val()=='') $('#rs_id').attr('disabled',true);
			else $('#rs_id').removeAttr('disabled');
		}
	});
}

function get_code(){
	$('#rs_code').val($('#rs_id').val());
	$.ajax({
		//dataType:'json',
		data: 'rs_code='+$('#rs_code').val(),
		url:'{{URL::to("region/get_rs_detail")}}',
		success:function(data) {
			//alert(data);
			$('#alamat').val(data);
			if($('#sub_district_id').val()=='') $('#rs_code').attr('disabled',true);
			else $('#rs_code').removeAttr('disabled');
		}
	});
}
</script>

@include('admin.footer')
