<!-- Laboratorium -->
<?php
$q = "
	SELECT a.id,b.nama_laboratorium,a.kode_lab,b.alamat
	FROM profil_laboratorium a
	JOIN laboratorium b ON b.lab_code=a.kode_lab
	WHERE a.created_by='".Sentry::getUser()->id."'
";
$data=DB::select($q);
for($is=0;$is<count($data);$is++):
?>
<div class="control-group">
	<label class="control-label span6" for="basicinput">
		<a href="profil_id?type=laboratorium&kd_faskes=<?php echo $data[$is]->kode_lab; ?>&id_profil=<?php echo $data[$is]->id; ?>&type_profil=laboratorium">
			Laboratorium <?php echo $data[$is]->nama_laboratorium; ?><br>
			<i><?php echo $data[$is]->alamat; ?></i>
		</a>
	</label>
	<label class="control-label span4" for="basicinput">
		<?php
			$q = "
				SELECT COUNT(*) as jml
				FROM profil_laboratorium a
				WHERE a.is_admin='y' AND a.kode_lab ='".$data[$is]->kode_lab."'
			";
			$data_jml=DB::select($q);
			echo $data_jml[0]->jml;
			$btn_type = '';
			$btn_disabled = 'disabled="disabled"';
			if($data_jml[0]->jml < 2){
				$btn_type = 'btn-success';
				$btn_disabled = '';
			}			 
		?>
		Admin
		<a href="#" onClick="konfirmasiAdmin('laboratorium','<?php echo $data[$is]->id; ?>');" class="btn <?php echo $btn_type; ?>" <?php echo $btn_disabled; ?>>Jadikan Saya Admin</a>
		<a href="#" onClick="konfirmasiDelete('laboratorium','<?php echo $data[$is]->id; ?>');" class="btn btn-danger">Hapus Profil</a>
	</label>
</div>
<?php endfor; ?>
