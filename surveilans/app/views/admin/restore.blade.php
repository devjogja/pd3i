@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
        <!--/#btn-controls-->
        <div class="module">
            <div class="module-head">
                <h3>Modul restore/import database</h3>
            </div>
            <div class="module-body">
                <div class="chart inline-legend grid">
                    <div id="placeholder2" class="graph">
                        {{Form::open(array('url'=>'database/restore','class'=>'form-horizontal row-fluid','files'=>true))}}
                            <div class="control-group">
                                <label class="control-label span3" for="basicinput">Pilih File</label>
                                <div class="controls span6">
                                    {{Form::file('sql',null,array('class'=>'form-control span8'))}}
                                </div>
                                <?php if(Session::get('sql')) echo Session::get('sql'); ?>
                            </div>
                             <div class="control-group">
                                <label class="control-label span3" for="basicinput"></label>
                                <div class="controls span6">
                                    <input type="submit" class="btn btn-md btn-success" value="restore"></div>
                                </div>
                            </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/.content-->
</div>
@stop