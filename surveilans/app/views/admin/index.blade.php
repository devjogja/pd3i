@extend('layouts.master')
@section('content')
<div class="row mt">
    <div class="col-md-12">
        <div class="content-panel">
            <table class="table table-striped table-advance table-hover">
      	  	  <h4><i class="fa fa-angle-right"></i> Daftar Anggota Surveilans</h4>
      	  	  <hr>
                <thead>
                <tr>
                    <th><i class="fa fa-bullhorn"></i> Nama</th>
                    <th class="hidden-phone"><i class="fa fa-question-circle"></i> Alamat</th>
                    <th><i class="fa fa-bookmark"></i> Jenis Kelamin</th>
                    <th><i class=" fa fa-edit"></i> Status</th>
                    <th>actions</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Hendri</td>
                    <td class="hidden-phone">Sleman, Jogjakarta</td>
                    <td>Laki-laki</td>
                    <td><span class="label label-info label-mini">Aktif</span></td>
                    <td>
                        <button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                        <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                    </td>
                </tr>
                <tr>
                    <td>Joko Purnomo</td>
                    <td class="hidden-phone">Sleman, Jogjakarta</td>
                    <td>Laki-laki</td>
                    <td><span class="label label-warning label-mini">Tidak Aktif</span></td>
                    <td>
                        <button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                        <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                    </td>
                </tr>
                <tr>
                    <td>Santi</td>
                    <td class="hidden-phone">Kota Jogja, Jogjakarta</td>
                    <td>Perempuan </td>
                    <td><span class="label label-info label-mini">Aktif</span></td>
                    <td>
                        <button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                        <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div><!-- /content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- /row -->
@stop