@include('admin.header')
<script>
	function konfirmasiDelete(instansi,profile_id){
		if (confirm('Hapus Profil ?')) {
			$.ajax({
				dataType: "json",
				url: "{{URL::to('delete_profile')}}",
				data: "profile_id="+profile_id+"&instansi="+instansi,
				success: function(jsonData) {
					alert(jsonData.message);
					location.href = jsonData.address;
				}
			});
		} else {
		}
	}

	function konfirmasiAdmin(instansi,profile_id){
		if (confirm('Jadi Admin ?')) {
			$.ajax({
				dataType: "json",
				url: "{{URL::to('register_admin')}}",
				data: "profile_id="+profile_id+"&instansi="+instansi,
				success: function(jsonData) {
					alert(jsonData.message);
					location.href = jsonData.address;
				}
			});
		} else {
		}
	}
</script>

<style>
	.accordion h3{
		text-align:center;
	}
	
	.accordion div{
		text-align:center;
	}
</style>
@include('admin.heading')
<div class="wrapper">
	<div class="container">
		<div class="row">
			<!--/.span3-->
			<div class="span12">
				<div class="content">
					<div class="btn-controls">
						
						<div class="btn-box-row row-fluid">
							<ul class="widget widget-usage unstyled span12">
								<div class="module-head">
									<h3>Tambah profil</h3>
								</div>
								<div class="module-body span12">
									<div class="form-horizontal">
										@include('admin.pilih_profil_puskesmas')
										@include('admin.pilih_profil_rs')
										@include('admin.pilih_profil_laboratorium')
										@include('admin.pilih_profil_kabupaten')
										@include('admin.pilih_profil_provinsi')
										@include('admin.pilih_profil_pusat')
										<div class="control-group">
											<label class="control-label span6" for="basicinput">
												<a href="setting/level" class="btn btn-success">
													TAMBAH PROFIL
												</a>
											</label>
										</div>
									</div>
								</div>
							</ul>
						</div>
					</div>
					<!--/.module-->
				</div>
				<!--/.content-->
			</div>
			<!--/.span9-->
		</div>
	</div>
	<!--/.container-->
</div>
@include('admin.footer')


