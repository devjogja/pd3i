@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <h4>
						<a class="btn btn-warning btn-xs" href="{{URL::to('listprovinsi')}}">
							<i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus">
								<< Kembali	
							</i>
						</a>
                        Data Kabupaten
                    </h4>
                </div>
                <br>
                <div class="table-responsive">
                  <table class="uk-table uk-table-hover uk-table-striped uk-table-condensed table-bordered">
                    <thead>
                      <tr>
                        <th class="span1">No.</th>
                        <th class="span2">Kode kabupaten</th>
                        <th class="span8">Nama kabupaten</th>
                        <th class="span1">Aksi</th>
                      </tr>
                    </thead>
                  <tbody>
                      <?php $no=1; ?>
                      @if($kabupaten)
                      @foreach($kabupaten as $data)
                      <tr>
                        <td>{{$no}}</td>
                        <td>{{$data->id_kabupaten}}</td>
                        <td>{{$data->kabupaten}}</td>
                        <td>
							<!--
							<a class="btn btn-primary btn-xs" href="{{URL::to('kabupaten/edit/'.$data->id_kabupaten)}}">
								<i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ubah">
										
								</i>
							</a>
							<a class="btn btn-warning btn-xs" href="{{URL::to('kabupaten/hapus/'.$data->id_kabupaten)}}" onclick="alert('yakin ini menghapus data ini?')">
								<i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus">
										
								</i>
							</a>
							-->
							<a class="btn btn-warning btn-xs" href="{{URL::to('listkecamatan/'.$data->id_kabupaten.'/'.$data->id_provinsi)}}">
								<i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus">
									Daftar Kecamatan	
								</i>
							</a>
                        </td>
                      </tr>
                      <?php $no++?>
                      @endforeach
                      @endif
                  </tbody>
                  </table>
                  <ul class="pagination pagination-sm">
                    {{$kabupaten->links()}}
                  </ul>
                </div>
            </div>
            <!--/.module-body-->
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<!--/.span9-->
@stop