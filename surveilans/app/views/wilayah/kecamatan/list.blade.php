@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <h4>
						<a class="btn btn-warning btn-xs" href="{{URL::to('listkabupaten/'.$id_provinsi)}}">
							<i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus">
								<< Kembali	
							</i>
						</a>
                        Data Kecamatan
                    </h4>
                </div>
                <br>
                <div class="table-responsive">
                  <table class="uk-table uk-table-hover uk-table-striped uk-table-condensed table-bordered">
                    <thead>
                      <tr>
                        <th class="span1">No.</th>
                        <th class="span2">Kode Kecamatan</th>
                        <th class="span8">Nama Kecamatan</th>
                        <th class="span1">Aksi</th>
                      </tr>
                    </thead>
                  <tbody>
                      <?php $no=1; ?>
                      @if($kecamatan)
                      @foreach($kecamatan as $data)
                      <tr>
                        <td>{{$no}}</td>
                        <td>{{$data->id_kecamatan}}</td>
                        <td>{{$data->kecamatan}}</td>
                        <td>
							<!--
							<a class="btn btn-primary btn-xs" href="{{URL::to('kecamatan/edit/'.$data->id_kecamatan)}}">
								<i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ubah">
										
								</i>
							</a>
							<a class="btn btn-warning btn-xs" href="{{URL::to('kecamatan/hapus/'.$data->id_kecamatan)}}" onclick="alert('yakin ini menghapus data ini?')">
								<i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus">
										
								</i>
							</a>
							-->
							<a class="btn btn-warning btn-xs" href="{{URL::to('listkelurahan/'.$data->id_kecamatan.'/'.$data->id_kabupaten.'/'.$id_provinsi)}}">
								<i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus">
									Daftar Kelurahan	
								</i>
							</a>
                        </td>
                      </tr>
                      <?php $no++?>
                      @endforeach
                      @endif
                  </tbody>
                  </table>
                  <ul class="pagination pagination-sm">
                    {{$kecamatan->links()}}
                  </ul>
                </div>
            </div>
            <!--/.module-body-->
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<!--/.span9-->
@stop