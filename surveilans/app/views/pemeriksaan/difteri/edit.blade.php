@extends('layouts.master')
@section('content')
<!-- awal class span12 -->
<div class="span12">
    <!-- awal class content -->
    <div class="content">
        <!-- awal class module -->
        <div class="module">
            <!-- awal class module-body -->
            <div class="module-body">
                <div class="profile-head media">
                    <h4>
                        Form Edit Kasus Penyakit Difteri
                    </h4>
                    <hr>
                </div>

                <!-- awal form -->
                @foreach($difteri as $row)
                {{
                    Form::open
                    (
                        array
                        (
                            'url'=>'difteri_update',
                            'class'=>'form-horizontal'
                        )
                    )
                }}

                <!-- awal input kolom identitas pasien -->
                <fieldset>
                    <legend>Identitas pasien</legend>
                    

                    <div class="control-group">
                        <label class="control-label">NIK</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'nik',
                                    $row->nik, 
                                    array
                                    (
                                        'class' => 'input-medium',
                                        'placeholder'=>'Nomer induk kependudukan'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Nama penderita</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'nama_anak',
                                    $row->nama_anak, 
                                    array
                                    (
                                        'class' => 'input-xlarge',
                                        'placeholder'=>'Nama anak'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Nama orang tua</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'nama_ortu',
                                    $row->nama_ortu, 
                                    array
                                    (
                                        'class' => 'input-xlarge',
                                        'placeholder'=>'Nama orang tua'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Jenis kelamin</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'jenis_kelamin',
                                    [
                                        '0'=>'Pilih',
                                        '1'=>'Laki-laki',
                                        '2'=>'Perempuan',
                                        '3'=>'Tidak jelas'
                                    ],
                                    [$row->jenis_kelamin], 
                                    array
                                    (
                                        'class' => 'input-medium id_combobox'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Tanggal lahir</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'tanggal_lahir',
                                    Helper::getDate($row->tanggal_lahir), 
                                    array
                                    (
                                        'class' => 'input-medium tgl_lahir',
                                        'placeholder'=>'Tanggal lahir',
                                        'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
                                        'onchange'=>'usia()'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <div class="controls">
                            <input type="text" class="input-mini" value="{{$row->umur}}" name="tgl_tahun" id="tgl_tahun">
                            Thn 
                            <input type="text" class="input-mini" value="{{$row->umur_bln}}" name="tgl_bulan" id="tgl_bulan">
                            Bln 
                            <input type="text" class="input-mini" value="{{$row->umur_hr}}" name="tgl_hari" id="tgl_hari">
                            Hr
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Provinsi</label>
                        <div class="controls">
                            <select name="" class="input-medium" id="id_provinsi" onchange="showKabupaten()">
                                <?php $pro=Provinsi::lists('provinsi','id_provinsi'); 
                                    foreach ($pro as $id_provinsi => $provinsi) 
                                    {
                                        if($provinsi==$row->provinsi) 
                                        {
                                    ?>
                                            <option value="{{$id_provinsi}}" selected="selected">{{$provinsi}}</option>
                                    <?php
                                        } 
                                        else 
                                        {
                                    ?>
                                            <option value="{{$id_provinsi}}">{{$provinsi}}</option>
                                    <?php
                                        }
                                    } 
                                ?>
                            </select>
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Kabupaten/Kota</label>
                        <div class="controls">
                            <select name="" class="input-medium" id="id_kabupaten" onchange="showKecamatan()">
                                <option value="{{$row->id_kabupaten}}">{{$row->kabupaten}}</option>
                            </select>
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Kecamatan</label>
                        <div class="controls">
                            <select name="" class="input-medium" id="id_kecamatan" onchange="showKelurahan()">
                                <option value="{{$row->id_kecamatan}}">{{$row->kecamatan}}</option>
                            </select>
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Kelurahan/Desa</label>
                        <div class="controls">
                            <select name="id_kelurahan" class="input-medium" id="id_kelurahan">
                                <option value="{{$row->id_kelurahan}}">{{$row->kelurahan}}</option>
                            </select>
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Alamat</label>
                        <div class="controls">
                            {{
                                Form::textarea
                                (
                                    'alamat',
                                    $row->alamat,
                                    array
                                    (
                                        'rows'=>'3'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">No Epidemologi</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'no_epid',
                                    $row->no_epid, 
                                    array
                                    (
                                        'class' => 'input-medium',
                                        'placeholder'=>'Nomer epidemologi',
                                        'id'=>'epid'
                                    )
                                )
                            }}
                            {{
                                Form::hidden
                                (
                                    'id_hasil_uji_lab_difteri',
                                    $row->id_hasil_uji_lab_difteri
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">No Epidemologi lama</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'no_epid_lama',
                                    $row->no_epid_lama, 
                                    array
                                    (
                                        'class' => 'input-medium',
                                        'placeholder'=>'Nomer epidemologi lama',
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                </fieldset>
                <!-- akhir input kolom identitas pasien-->
                
                <!-- awal input kolom data surveilans difteri -->
                <fieldset>
                    <legend>Data surveilans difteri</legend>
                    <div class="control-group">
                        <label class="control-label">Tanggal mulai demam</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'tanggal_timbul_demam',
                                    Helper::getDate($row->tanggal_timbul_demam),
                                    array
                                    (
                                        'class'=>'input-medium',
                                        'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
                                        'id'    =>'tgl_mulai_sakit',
                                        'onchange' => 'showEpidDifteri(),usia()',
                                    )
                                )
                            }}
                        <span class="help-inline"></span>
                        </div>    
                    </div>


                    <div class="control-group">
                        <label class="control-label">Vaksin DPT sebelum sakit berapa kali?</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'vaksin_DPT_sebelum_sakit',
                                    [
                                        '0'=>'Pilih',
                                        '1' => '1X',
                                        '2'=>'2X',
                                        '3'=>'3X',
                                        '4'=>'4X',
                                        '5'=>'5X',
                                        '6'=>'6X',
                                        '7'=>'Tidak',
                                        '8'=>'Tidak tahu'
                                    ],
                                    [$row->vaksin_DPT_sebelum_sakit],
                                    array
                                    (
                                        'class'=>'input-medium id_combobox'
                                    )
                                )
                            }}
                        <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Tanggal vaksinasi difteri terakhir</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'tanggal_vaksinasi_difteri_terakhir',
                                    Helper::getDate($row->tanggal_vaksinasi_difteri_terakhir),
                                    array
                                    (
                                        'class'=>'input-medium',
                                        'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>
                    

                    <div class="control-group">
                        <label class="control-label">Tanggal pelacakan</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'tanggal_pelacakan',
                                    Helper::getDate($row->tanggal_pelacakan),
                                    array
                                    (
                                        'class'=>'input-medium',
                                        'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Gejalan lain</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'gejala_lain',
                                    $row->gejala_lain,
                                    array
                                    (
                                        'class'=>'input-medium'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                          <label class="control-label">Kontak</label>
                          <div class="controls">
                          <table>
                                <tr>
                                    <td>Jumlah</td>
                                    <td> : 
                                        {{
                                            Form::text
                                            (
                                                'jumlah_kontak',
                                                $row->jumlah_kontak,
                                                array
                                                (
                                                    'class'=>'input-mini'
                                                )
                                            )
                                        }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Diambil spec (swab hidung)</td>
                                    <td> : 
                                        {{
                                            Form::text
                                            (
                                                'jumlah_swab_hidung',
                                                $row->jumlah_swab_hidung,
                                                array
                                                (
                                                    'class'=>'input-mini'
                                                )
                                            )
                                        }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Positif</td>
                                    <td> : 
                                        {{
                                            Form::text
                                            (
                                                'jumlah_positif',
                                                $row->jumlah_positif,
                                                array
                                                (
                                                    'class'=>'input-mini'
                                                )
                                            )
                                        }}
                                    </td>
                                </tr>
                            </table>
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Keadaan akhir</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'keadaan_akhir',
                                    [
                                        '0'=>'Pilih',
                                        '1'=>'Hidup/Sehat',
                                        '2'=>'Mati'
                                    ],
                                    [$row->keadaan_akhir],
                                    array
                                    (
                                        'class'=>'input-medium id_combobox'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>
                </fieldset>
                <!-- akhir input kolom data surveilans difteri-->           
                
                <!-- awal input kolom data spesimen dan laboratorium -->
                <fieldset>
                    <legend>Data spesimen dan laboratorium</legend>
                    <div class="control-group">
                        <label class="control-label">Tanggal pengambilan spesimen tenggorokan</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'tanggal_diambil_spesimen_tenggorokan',
                                    Helper::getDate($row->tanggal_diambil_spesimen_tenggorokan),
                                    array
                                    (                                    
                                        'class'=>'input-medium',
                                        'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Tanggal pengambilan spesimen hidung</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'tanggal_diambil_spesimen_hidung',
                                    Helper::getDate($row->tanggal_diambil_spesimen_hidung),
                                    array
                                    (
                                        'class'=>'input-medium',
                                        'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Jenis pemeriksaan laboratorium</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'jenis_pemeriksaan',
                                    [
                                        '0'=>'Pilih',
                                        '1'=>'Kultur',
                                        '2'=>'Mikroskop'
                                    ],
                                    [$row->jenis_pemeriksaan],
                                    array
                                    (
                                        'class'=>'input-medium id_combobox'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <!-- <div class="control-group">
                        <label class="control-label">Tipe spesimen (swab)</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'tipe_spesimen',
                                    [
                                        ''=>'Pilih',
                                        '0'=>'Tenggorokan',
                                        '1'=>'Hidung'
                                    ],
                                    [$row->tipe_spesimen],
                                    array
                                    (
                                        'class'=>'input-medium id_combobox'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div> -->


                    <div class="control-group">
                        <label class="control-label">Hasil lab kultur tenggorokan</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'hasil_lab_kultur_tenggorokan',
                                    [
                                        '0'=>'Pilih',
                                        '1'=>'Positif',
                                        '2'=>'Negatif'
                                    ],
                                    [$row->hasil_lab_kultur_tenggorokan],
                                    array
                                    (
                                        'class'=>'input-medium id_combobox'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Hasil lab kultur hidung</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'hasil_lab_kultur_hidung',
                                    [
                                        '0'=>'Pilih',
                                        '1'=>'Positif',
                                        '2'=>'Negatif'
                                    ],
                                    [$row->hasil_lab_kultur_hidung],
                                    array
                                    (
                                        'class'=>'input-medium id_combobox'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Hasil lab mikroskop tenggorokan</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'hasil_lab_mikroskop_tenggorokan',
                                    [
                                        '0'=>'Pilih',
                                        '1'=>'Positif',
                                        '2'=>'Negatif'
                                    ],
                                    [$row->hasil_lab_mikroskop_tenggorokan],
                                    array
                                    (
                                        'class'=>'input-medium id_combobox'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Hasil lab mikroskop hidung</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'hasil_lab_mikroskop_hidung',
                                    [
                                        '0'=>'Pilih',
                                        '1'=>'Positif',
                                        '2'=>'Negatif'
                                    ],
                                    [$row->hasil_lab_mikroskop_hidung],
                                    array
                                    (
                                        'class'=>'input-medium id_combobox'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>
                </fieldset>
                <!-- akhir input kolom data spesimen dan laboratorium -->

                <!-- awal input kolom klasifikasi final -->
                <fieldset>
                    <legend>Klasifikasi final</legend>
                    <div class="control-group">
                        <label class="control-label">Klasifikasi final</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'klasifikasi_final',
                                    [
                                        '0'=>'Pilih',
                                        '1' => 'Probable',
                                        '2'=>'Konfirm',
                                        '3'=>'Negatif'
                                    ], 
                                    [$row->klasifikasi_final], 
                                    array
                                    (
                                        'class' => 'input-medium id_combobox'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>
                </fieldset>
                <!-- akhir input kolom klasifikasi final -->

                <!-- btn -->
                <div class="form-actions">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <a href="{{URL::to('difteri')}}" class="btn btn-primary">kembali</a>
                </div>
                <br/>
                <br/>
                <!-- btn -->
                {{Form::close()}}
                @endforeach()
                <!-- akhir form -->
            </div>
            <!-- akhir class module-body -->
        </div>
        <!-- akhir class module -->
    </div>
    <!-- akhir class content -->
    
    <!-- js -->
    <script>
        $(document).ready(function() 
        {
            $("select").select2();
            showKabupaten();
            showKecamatan();
            showKelurahan();
        });

        function showKabupaten(){
          var provinsi = $("#id_provinsi").val();
          var kabupaten = $("#id_kabupaten").val();
          // //kirim data ke server
          $('.lodingKab').html('<img src="{{URL::to("style/images/loding.gif")}}">');
          $.post('{{URL::to("provinsi/getkab")}}', {provinsi:provinsi,kabupaten:kabupaten}, function(response)
          {
              $('#id_kabupaten').removeAttr('disabled','');
              $("#id_kabupaten").html(response);
              $('.lodingKab').html('');
          });

        }

        function showKecamatan(){
          var kabupaten = $("#id_kabupaten").val();
          var kecamatan = $("#id_kecamatan").val();
          // //kirim data ke server
          $('.lodingKec').html('<img src="{{URL::to("style/images/loding.gif")}}">');
          $.post('{{URL::to("provinsi/getkec")}}', {kabupaten:kabupaten,kecamatan:kecamatan}, function(response)
          {
              $('#id_kecamatan').removeAttr('disabled','');
              $("#id_kecamatan").html(response);
              $('.lodingKec').html('');
          });

        }

        function showKelurahan(){
          var kecamatan= $("#id_kecamatan").val();
          var kelurahan = $("#id_kelurahan").val();
          // //kirim data ke server
          $('.lodingKel').html('<img src="{{URL::to("style/images/loding.gif")}}">');
          $.post('{{URL::to("provinsi/getkel")}}', {kecamatan:kecamatan,kelurahan:kelurahan}, function(response)
          { 
              $('#id_kelurahan').removeAttr('disabled','');
              $("#id_kelurahan").html(response);
              $('.lodingKel').html('');
          });

        }

        $( "#id_kelurahan" ).change(function() {
        $.ajax({
            data:'id_kelurahan='+$('#id_kelurahan').val()+'&date='+$('#tgl_mulai_sakit').val(),
            url:'{{ URL::to("ambil_epid_difteri") }}',
            success:function(data) {
                $('#epid').val(data);
            }
        });
        });



        // Generate no. epid
        $( "#tgl_mulai_sakit" ).change(function() {
            $.ajax({
                data:'id_kelurahan='+$('#id_kelurahan').val()+'&date='+$('#tgl_mulai_sakit').val(),
                url:'{{ URL::to("ambil_epid_difteri") }}',
                success:function(data) {
                    $('#epid').val(data);
                }
            });
        });
        function umur()
        {

          $.ajax({

            data:'tanggal_lahir='+$('.tgl_lahir').val(),
            url:'{{URL::to("hitung/umur")}}',
            success:function(data) {

                //alert(data.tgl_tahun);
                
                $('#tgl_tahun').val(data.tgl_tahun);
                $('#tgl_bulan').val(data.tgl_bulan);
                $('#tgl_hari').val(data.tgl_hari);
            }
          });
        }
    </script>
    <!-- js -->

</div>
<!-- akhir class span12 -->
@stop