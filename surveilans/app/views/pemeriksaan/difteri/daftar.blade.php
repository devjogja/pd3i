<div class="module-body uk-overflow-container">
    <table class="display table table-bordered" id="pe_difteri">
    <thead>
        <tr>
          <th>No. Epid</th>
          <th>Nama pasien</th>
          <th>Nama Orang Tua</th>
          <th>Jenis kelamin</th>
          <th>Umur</th>
          <th>Alamat</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
      @if($pe_difteri)
      @foreach($pe_difteri as $row)
                <tr>
                    <td>{{$row->no_epid}}</td>
                    <td>{{$row->nama_anak}}</td>
                    <td>{{$row->nama_ortu}}</td>
                    <?php
                      $jeniskelamin = 'Tidak diisi';
                      if ($row->jenis_kelamin==1) {
                        $jeniskelamin = 'Laki-laki';
                      } elseif($row->jenis_kelamin==2) {
                        $jeniskelamin = 'Perempuan';
                      } elseif($row->jenis_kelamin==3){
                        $jeniskelamin = 'Tidak Jelas';
                      }
                    ?>
                    <td>{{$jeniskelamin}}</td>
                    <td>{{$row->umur}} Th</td>
                    <td>{{$row->alamat}}</td>
                    @if(Sentry::getUser()->hak_akses==2 || Sentry::getUser()->hak_akses==3 || Sentry::getUser()->hak_akses==7)
                    <td>
                    <div class="btn-group" role="group">
                      <a href="{{URL::to('pe_difteri_detail/'.$row->id)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
                      <a href="{{URL::to('pe_difteri_edit/'.$row->id)}}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>
                      <a href="{{URL::to('pe_difteri_hapus/'.$row->id)}}" onclick="return confirm('apakah yakin ingin menghapus data ini?');return false;" class="btn btn-xs btn-danger" data-uk-tooltip title="hapus data"><i class="icon icon-remove"></i></a>
                    </div>
                    </td>
                    @elseif(Sentry::getUser()->hak_akses==4)
                    <td>
                    <div class="btn-group" role="group">
                      <a href="{{URL::to('pe_difteri_detail/'.$row->id)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
                      <a href="{{URL::to('pe_difteri_edit/'.$row->id)}}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>
                    </div>
                    </td>
                    @elseif(Sentry::getUser()->hak_akses==1 || Sentry::getUser()->hak_akses==6)
                    <td>
                    <div class="btn-group" role="group">
                      <a href="{{URL::to('pe_difteri_detail/'.$row->id)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
                    </div>
                    </td>
                    @elseif(Sentry::getUser()->hak_akses==5)

                    @endif
                    
                </tr>
            @endforeach
        @endif
      </tbody>
    </table>
    <script>
        $(document).ready(function() 
        {
            $('#pe_difteri').dataTable();
        });
    </script>
</div>