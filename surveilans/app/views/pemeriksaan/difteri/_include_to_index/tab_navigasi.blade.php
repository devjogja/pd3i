<ul class="profile-tab nav nav-tabs">
    @if(Sentry::getUser()->hak_akses==2 || Sentry::getUser()->hak_akses==3 || Sentry::getUser()->hak_akses==7)

	    <li class="active"><a href="#daftar" data-toggle="tab">Daftar kasus</a></li>
	    <li><a href="#input-data" data-toggle="tab">Input data individual kasus</a></li>
	    <li><a href="#analisis" data-toggle="tab">Analisis kasus</a></li>
	    <li><a href="#PE" data-toggle="tab">Daftar kasus penyelidikan epidemologi kasus difteri</a></li>

    @elseif(Sentry::getUser()->hak_akses==4 || Sentry::getUser()->hak_akses==6 || Sentry::getUser()->hak_akses==1)

	    <li class="active"><a href="#daftar" data-toggle="tab">Daftar kasus</a></li>
	    <li><a href="#analisis" data-toggle="tab">Analisis kasus</a></li>
	    <li><a href="#PE" data-toggle="tab">Daftar kasus penyelidikan epidemologi kasus difteri</a></li>

    @endif
	<li><a href="#importdifteri" data-toggle="tab">Import</a></li>
</ul>
