<div class="alert alert-error">
   <p style="font-size:12px">Text input yang bertanda bintang <strong>(*)</strong> wajib di isi
</div>

{{
	Form::open(array(
		'url' => 'difteri_store',
		'class' => 'form-horizontal',
		'id'    => 'form-save_difteri'
	))
}}
<fieldset>
   <legend>Identitas pasien</legend>

<!-- awal input kolom nama penderita -->
	<div class="control-group">
		<label class="control-label">Nama penderita</label>
		<div class="controls">
			{{
				Form::text(
					'nama_anak',
					Input::old('nama_anak'),
					array(
						'data-validation'         => '[MIXED]',
						'data'                    => '$ wajib di isi!',
						'class'                   => 'input-xlarge',
						'placeholder'             => 'Nama penderita',
						'data-validation-message' => 'nama penderita wajib di isi!'
					 )
				)
			}}
			<span class="help-inline" id="error-nama_anak" style="color:red">(*)</span>
		</div>
	</div>


<!-- awal input kolom nik -->
	<div class="control-group">
		<label class="control-label">NIK</label>
		<div class="controls">
			{{
				Form::text(
					'nik',
					Input::old('nik'),
					array(
						'class'       => 'input-medium',
						'placeholder' => 'Nomer induk kependudukan'
					)
				)
			}}
			<span class="help-inline"></span>
		</div>
	</div>


<!-- awal input kolom ortu -->
	<div class="control-group">
		<label class="control-label">Nama orang tua</label>
		<div class="controls">
			{{
				Form::text(
					'nama_ortu',
					Input::old('nama_ortu'),
					array(
						'data-validation'         => '[MIXED]',
						'data'                    => '$ wajib di isi!',
						'class'                   => 'input-xlarge',
						'placeholder'             => 'Nama orang tua',
						'data-validation-message' => 'nama orang tua wajib di isi!'
					)
				)
			}}
			<span class="help-inline" id="error-nama_ortu" style="color:red">(*)</span>
		</div>
	</div>


<!-- awal input kolom jenis kelamin -->
	<div class="control-group">
		<label class="control-label">Jenis kelamin</label>
		<div class="controls">
			{{
				Form::select(
					'jenis_kelamin',
					array(
						'0'  => 'Pilih',
						'1' => 'Laki-laki',
						'2' => 'Perempuan',
						'3' => 'Tidak jelas'
					),
					null,
					array(
						'data-validation'         => '[MIXED]',
						'data'                    => '$ wajib di isi!',
						'data-validation-message' => 'jenis kelamin wajib di isi!',
						'class'                   => 'input-medium id_combobox',
						'id'                      => 'jenis_kelamin'
					)
				)
			}}
			<span class="help-inline" style="color:red">(*)</span>
		</div>
	</div>


<!-- awal input kolom tanggal lahir -->
	<div class="control-group">
		<label class="control-label"><input type="radio" name="option" onclick="pilihOptionTglLahir()">Tanggal lahir</label>
		<div class="controls">
			{{
				Form::text(
					'tanggal_lahir',
					Input::old('tanggal_lahir'),
					array(
						'class'                   => 'input-medium tgl_lahir',
						'placeholder'             => 'Tanggal lahir',
						'data-uk-datepicker'      => '{format:"DD-MM-YYYY"}',
						'onchange'                => 'usia()',
						'readonly'                => 'readonly'
					)
				)
			}}
			<span class="help-inline"></span>
		</div>
	</div>


<!-- awal input kolom umur -->
	<div class="control-group">
		<label class="control-label"><input type="radio" name="option" onclick="pilihOptionUmur()">Umur</label>
		<div class="controls">
			<input type="text" class="input-mini"  name="tgl_tahun" id="tgl_tahun" placeholder="Tahun" onchange="tgl_lahir()" readonly="readonly">
			Thn
			<input type="text" class="input-mini" name="tgl_bulan" id="tgl_bulan" placeholder="Bulan" onchange="tgl _lahir()" readonly="readonly">
			Bln
			<input type="text" class="input-mini" name="tgl_hari" id="tgl_hari" placeholder="Hari" onchange="tgl _lahir()" readonly="readonly">
			Hr
		</div>
	</div>


<!-- awal input kolom nama puskesmas -->
	<div class="control-group">
			<label class="control-label">Nama faskes saat periksa</label>
			<div class="controls">
				<?php
					$type = Session::get('type');
					$kd_faskes = Session::get('kd_faskes');
					$namainstansi = '';
					if($type == "puskesmas")
					{
					  $nama_instansi = 'PUSKESMAS '.DB::table('puskesmas')
								  ->select('puskesmas_name','puskesmas_code_faskes')
								  ->WHERE('puskesmas_code_faskes',$kd_faskes)->pluck('puskesmas_name');
					  echo Form::text('nama_puskesmas',$nama_instansi, array('class' => 'input-large','readonly'=>'readonly','placeholder'=>'Masukan nama puskesmas'));
					}
					elseif ($type == "rs")
					{
					  $nama_instansi = DB::table('rumahsakit2')
								  ->select('nama_faskes')
								  ->WHERE('kode_faskes',$kd_faskes)->pluck('nama_faskes');
					  echo Form::text('nama_puskesmas',$nama_instansi, array('class' => 'input-large','readonly'=>'readonly','placeholder'=>'Masukan nama rumahsakit'));
					}
				?>
				  <span class="help-inline"></span>
			  </div>
		  </div>


<!-- awal input kolom provinsi -->
	<div class="control-group">
		<label class="control-label">Provinsi</label>
		<div class="controls">
			{{
				Form::select(
					'id_provinsi',
					array('' => 'Pilih provinsi')+ Provinsi::lists('provinsi','id_provinsi'),
					null,
					array(
						'id'       => 'id_provinsi',
						'onchange' => 'showKabupaten()',
						'class'    => 'input-large id_combobox'
					)
				)
			}}
			<span class="help-inline" id="error-id_propinsi"></span>
		</div>
	</div>


<!-- awal input kolom kabupaten -->
	<div class="control-group">
		<label class="control-label">Kabupaten</label>
		<div class="controls">
			{{
				Form::select(
					'id_kabupaten',
					array('' => 'Pilih kabupaten'),
					null,
					array(
						'class'    => 'input-large id_combobox',
						'id'       => 'id_kabupaten',
						'onchange' => 'showKecamatan()'
					)
				)
			}}
			<span class="lodingKab"></span>
			<span class="help-inline" id="error-id_kabupaten"></span>
		</div>
	</div>


<!-- awal input kolom kecamatan -->
	<div class="control-group">
		<label class="control-label">Kecamatan</label>
		<div class="controls">
			{{
				Form::select(
					'id_kecamatan',
					array(
						'' => 'Pilih kecamatan'
					),
					null,
					array(
						'class'    => 'input-large id_combobox',
						'id'       => 'id_kecamatan',
						'onchange' => 'showKelurahan()'
					)
				)
			}}
			<span class="lodingKec"></span>
			<span class="help-inline" id="error-id_kecamatan"></span>
		</div>
	</div>


<!-- awal input kolom kelurahan -->
	<div class="control-group">
		<label class="control-label">Kelurahan/Desa</label>
		<div class="controls">
			{{
				Form::select(
					'id_kelurahan',
					array(
						'' => 'Pilih kelurahan/desa'
					),
					null,
					array(
						'class' => 'input-large id_combobox',
						'id'    => 'id_kelurahan',
						'onchange' => 'showEpidDifteri()'
					)
				)
			}}
			<span class="lodingKel"></span>
			<span class="help-inline" id="error-id_kelurahan" style="color:red">(*)</span>
		</div>
	</div>


<!-- awal input kolom alamat -->
	<div class="control-group">
		<label class="control-label">Alamat</label>
		<div class="controls">
			{{
				Form::textarea(
					'alamat',
					null,
					array(
						'rows'        => '3',
						'style'       => 'width:300px',
						'placeholder' => 'Hanya diisi nama jalan, no. rumah dan no. RT - RW'
					)
				)
			}}
			<span class="help-inline" style="color:red"></span>
		</div>
	</div>


<!-- awal input kolom no epidemologi -->
	<div class="control-group">
		<label class="control-label">No Epidemologi</label>
		<div class="controls">
			{{
				Form::text(
					'no_epid',
					Input::old('no_epid'),
					array(
						'class'                   => 'input-medium',
						'placeholder'             => 'Nomer epidemologi',
						'id'                      => 'epid',
						'readonly'                => 'readonly'
					)
				)
			}}
			<span class="help-inline" id="alert alert-error"></span>
		</div>
	</div>


<!-- awal input kolom no epid lama -->
	<div class="control-group">
		<label class="control-label">No Epidemologi lama</label>
		<div class="controls">
			{{
				Form::text(
					'no_epid_lama',
					Input::old('no_epid_lama'),
					array(
						'class'       => 'input-medium',
						'placeholder' =>'Nomer epidemologi lama'
					)
				)
			}}
			<span class="help-inline"></span>
		</div>
	</div>

</fieldset>


<!-- awal fieldset -->
<fieldset>
<legend>Data surveilans difteri</legend>

<!-- awal input kolom tanggal mulai sakit -->
	<div class="control-group">
		<label class="control-label">Tanggal mulai sakit (demam)</label>
		<div class="controls">
			{{
				Form::text(
					'tanggal_timbul_demam',
					Input::old('tanggal_timbul_demam'),
					array(
						'data-validation'         => '[MIXED]',
						'data'                    => '$ wajib di isi!',
						'data-validation-message' => 'tanggal mulai lumpuh wajib di isi!',
						'class'                   => 'input-medium tgl_mulai_sakit',
						'data-uk-datepicker'      => '{format:"DD-MM-YYYY"}',
						'onchange'                => 'showEpidDifteri(),usia()',
						'id'                      => 'tgl_mulai_sakit'
					)
				)
			}}
			<span class="help-inline" id="error-tanggal_timbul_demam" style="color:red">(*)</span>
		</div>
	</div>


<!-- awal input kolom imunisasi DPT -->
	<div class="control-group">
		<label class="control-label">Imunisasi DPT sebelum sakit berapa kali?</label>
		<div class="controls">
			{{
				Form::select(
					'vaksin_DPT_sebelum_sakit',
					array(
						'0'  => 'Pilih',
						'1' => '1X',
						'2' => '2X',
						'3' => '3X',
						'4' => '4X',
						'5' => '5X',
						'6' => '6X',
						'7' => 'Tidak',
						'8' => 'Tidak tahu'
					),
					Input::old('vaksin_DPT_sebelum_sakit'),
					array(
						'data-validation'         => '[MIXED]',
						'data'                    => '$ wajib di isi!',
						'data-validation-message' => 'imunisasi DPT sebelum sakit wajib di isi!',
						'class'                   => 'input-medium id_combobox'
					)
				)
			}}
			<span class="help-inline" id="error-vaksin_DPT_sebelum_sakit" style="color:red">(*)</span>
		</div>
	</div>


<!-- awal input kolom tanggal imunisasi difteri -->
	<div class="control-group">
		<label class="control-label">Tanggal imunisasi difteri terakhir</label>
		<div class="controls">
			{{
				Form::text(
					'tanggal_vaksinasi_difteri_terakhir',
					Input::old('tanggal_vaksinasi_difteri_terakhir'),
					array(
						'class'                   => 'input-medium',
						'data-uk-datepicker'      => '{format:"DD-MM-YYYY"}'
					)
				)
			}}
			<span class="help-inline" id="error-tanggal_vaksinasi_difteri_terakhir"></span>
		</div>
	</div>


<!-- awal input kolom tanggal pelacakan -->
	<div class="control-group">
		<label class="control-label">Tanggal pelacakan</label>
		<div class="controls">
			{{
				Form::text(
					'tanggal_pelacakan',
					Input::old('tanggal_pelacakan'),
					array(
						'class'                   => 'input-medium',
						'data-uk-datepicker'      => '{format:"DD-MM-YYYY"}'
					)
				)
			}}
			<span class="help-inline" id="error-tanggal_pelacakan"></span>
		</div>
	</div>


<!-- awal input kolom gejala lain -->
	<div class="control-group">
		<label class="control-label">Gejala lain</label>
		<div class="controls">
			{{
				Form::text(
					'gejala_lain',
					Input::old('gejala_lain'),
					array('class' => 'input-medium')
				)
			}}
			<span class="help-inline"></span>
		</div>
	</div>


<!-- awal input kolom kontak -->
	<div class="control-group">
		<label class="control-label">Kontak</label>
		<div class="controls">
			<table>
				<tr>
					<td>Jumlah</td>
					<td> :
						{{
							Form::text(
								'jumlah_kontak',
								Input::old('jumlah_kontak'),
								array('class' => 'input-mini')
							)
						}}
					</td>
				</tr>
				<tr>
					<td>Diambil spec (swab hidung)</td>
					<td> :
						{{
							Form::text(
								'jumlah_swab_hidung',
								Input::old('jumlah_swab_hidung'),
								array('class' => 'input-mini')
							)
						}}
					</td>
				</tr>
				<tr>
					<td>Positif</td>
					<td> :
						{{
							Form::text(
								'jumlah_positif',
								Input::old('jumlah_positif'),
								array('class' => 'input-mini')
							)
						}}
					</td>
				</tr>
			</table>
			<span class="help-inline"></span>
		</div>
	</div>


<!-- awal input keadaan akhir -->
	<div class="control-group">
		<label class="control-label">Keadaan akhir</label>
		<div class="controls">
			{{
				Form::select(
					'keadaan_akhir',
					array(
						'0'  => 'Pilih',
						'1' => 'Hidup/Sehat',
						'2' => 'Meninggal'
					),
					Input::old('keadaan_akhir'),
					array(
						'class'                   => 'input-medium id_combobox'
					)
				)
			}}
			<span class="help-inline" id="error-keadaan_akhir"></span>
		</div>
	</div>
</fieldset>


<!-- awal fieldset -->
<fieldset>
	<legend>Data spesimen dan laboratorium</legend>
	<div style="margin-bottom:14px">
		<table class="table table-bordered" id="data_spesimen">
			<thead>
				<tr>
					<th>Jenis Spesimen</th>
					<th>Tanggal ambil spesimen</th>
					<th>Jenis Pemeriksaan</th>
					<th>Hasil Lab</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table> 
	</div>
	<div style="margin-bottom:14px" class="buttonadd">
		<a class="btn btn-success" onclick="tambah_sampel()" id="tambah_sampel">Tambah sampel</a>
	</div>
	<div class="addspesimen">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Jenis Spesimen</th>
					<th>Tanggal ambil spesimen</th>
					<th>Jenis Pemeriksaan</th>
					<th>Hasil Lab</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<select id="jenis_spesimen" class='input-medium'>
							<option value="">Pilih</option>
							<option value="1">Tenggorokan</option>
							<option value="2">Hidung</option>
						</select>
					</td>
					<td>
						{{Form::text('',null,array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','id'=>'tanggal_ambil_spesimen'))}}
					</td>
					<td>
						<select id="jenis_pemeriksaan" class='input-medium'>
							<option value="">Pilih</option>
							<option value='1'>Kultur</option>
							<option value='2'>Mikroskop</option>
						</select>
					</td>
					<td>
						<select id="hasil_lab" class='input-medium'>
							<option value="">Pilih</option>
							<option value='1'>Positif</option>
							<option value='2'>Negatif</option>
						</select>
					</td>
				</tr>
			</tbody>
		</table>
		<div style="padding-left:40%;margin-top:14px">
			<button class="btn btn-primary" onclick="add_sampel()">Tambah</button>
			<button class="btn btn-warning" onclick="tutup_sampel()">Tutup</button>
		</div>
	</div>
</fieldset>
<!-- akhir fieldset -->

<!-- awal fieldset -->
<fieldset>
<!-- awal input kolom klasifikasi final -->
	<legend>Klasifikasi final</legend>
	<div class="control-group">
		<label class="control-label">Klasifikasi final</label>
		<div class="controls">
			{{
				Form::select(
					'klasifikasi_final',
					array(
						'0'  => 'Pilih',
						'1' => 'Probable',
						'2' => 'Konfirm',
						'3' => 'Negatif'
					),
					Input::old('klasifikasi_final'),
					array(
						'data-validation'         => '[MIXED]',
						'data'                    => '$ wajib di isi!',
						'data-validation-message' => 'klasifikasi final wajib di isi!',
						'class' => 'input-medium id_combobox'
					)
				)
			}}
			<span class="help-inline" style="color:red">*</span>
		</div>
		<!-- akhir input kolom klasifikasi final -->
	</div>
</fieldset>
<!-- akhir fieldset -->

<!-- tombol -->
	<div class="form-actions">
	   <button type="submit" class="btn btn-success">Simpan</button>
	   <button type="reset" class="btn btn-warning">Batal</button>
	</div>
	<br/>
	<br/>

{{Form::close()}}
<!-- akhir form -->

<script type="text/javascript">
$(function(){
	$('.addspesimen').hide();
	// Validasi form input data individual
	$( '#form-save_difteri' ).validate({
		submit: {
			settings: {
				scrollToError: {
					offset: -100,
					duration: 500
				}
			}
		}
	});

	$('#form-save_difteri input').keydown(function(e){
		if(e.keyCode==13){
			// check for submit button and submit form on enter press
			if($(':input:eq(' + ($(':input').index(this) + 1) + ')').attr('type')=='submit'){
				return true;
			}
			$(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();
			return false;
		}
	});

	$("#data_spesimen").on('click','.rmSpesimen',function(){
        $(this).parent().parent().remove();
    });
});

	function tambah_sampel(){
		$('.addspesimen').fadeIn();
		$('.buttonadd').fadeOut();
	}

	function tutup_sampel() {
		$('.addspesimen').fadeOut();
		$('.buttonadd').fadeIn();
	}

	function add_sampel() {
		if ($('#jenis_spesimen').val() !== '') {
			var jenis_spesimen = $('#jenis_spesimen').val();
			if (jenis_spesimen==='1'){
				jenis_spesimen = 'Tenggorokan';
			}else if(jenis_spesimen==='2'){
				jenis_spesimen = 'Hidung';
			};
			var tanggal_ambil_spesimen = $('#tanggal_ambil_spesimen').val();
			var jenis_pemeriksaan = $('#jenis_pemeriksaan').val();
			if (jenis_pemeriksaan==='1'){
				jenis_pemeriksaan = 'Kultur';
			}else if(jenis_pemeriksaan==='2'){
				jenis_pemeriksaan = 'Mikroskop';
			};
			var hasil_lab = $('#hasil_lab').val();
			if (hasil_lab==='1'){
				hasil_lab = 'Positif';
			}else if(hasil_lab==='2'){
				hasil_lab = 'Negatif';
			};
			var push = '<tr>'+
							'<td>'+jenis_spesimen+'</td>'+
							'<td>'+tanggal_ambil_spesimen+'</td>'+
							'<td>'+jenis_pemeriksaan+'</td>'+
							'<td>'+hasil_lab+'</td>'+
							'<td><a href="javascript:void(0);" class="rmSpesimen">Remove</a></td>'+
							'<input type="hidden" name="spesimen[jenis_spesimen][]" value='+jenis_spesimen+'>'+
							'<input type="hidden" name="spesimen[tgl_ambil_spesimen][]" value='+tanggal_ambil_spesimen+'>'+
							'<input type="hidden" name="spesimen[jenis_pemeriksaan][]" value='+jenis_pemeriksaan+'>'+
							'<input type="hidden" name="spesimen[hasil_lab][]" value='+hasil_lab+'>'+
						'</tr>';
			$('#data_spesimen tbody').append(push);
			clearspesimen();
			return false;
		};
	}

	function clearspesimen() {
		$('#tanggal_ambil_spesimen').val(null);
		$("#jenis_spesimen").select2().val(null).trigger('change');
		$("#jenis_pemeriksaan").select2().val(null).trigger('change');
		$("#hasil_lab").select2().val(null).trigger('change');
	}

</script>