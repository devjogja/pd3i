@extends('layouts.master')
@section('content')
<!-- awal sapn12 -->
<div class="span12">
    <!-- awal class content -->
    <div class="content">
        <!-- awal class module -->
        <div class="module">
            <!-- awal class module-body -->
            <div class="module-body">
                <div class="profile-head media">
                    <h4>
                        Form Edit Penyelidikan Epidemologi Kasus Penyakit Difteri
                    </h4>
                    <hr>
                </div>
                <!-- awal form -->
                {{
                    Form::open
                    (
                        array
                        (
                            'url'=>'pe_update_difteri',
                            'class'=>'form-horizontal'
                        )
                    )
                }}
                @foreach($difteri as $row)
                <!-- awal input kolom identitas penderita -->
                <fieldset>
                    <legend>Identitas penderita</legend>
                    <div class="control-group">
                        <label class="control-label">No. epidemologi</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'no_epid',
                                    $row->no_epid, 
                                    array
                                    (
                                        'class' => 'input-medium',
                                        'placeholder'=>'No. epidemologi',
                                        'onkeyup'=>'getPasien()',
                                        'id'=>'epid_pe'
                                    )
                                )
                            }}

                            {{
                                Form::hidden
                                (
                                    'id',
                                    $row->id,
                                    array
                                    (
                                        'class'=>'input-medium'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Nama penderita</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'nama_anak',
                                    $row->nama_anak, 
                                    array
                                    (
                                        'class' => 'input-medium nama_anak',
                                        'placeholder'=>'Nama penderita'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Nama orang tua/KK</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'nama_ortu',
                                    $row->nama_ortu, 
                                    array
                                    (
                                        'class' => 'input-medium nama_ortu',
                                        'placeholder'=>'Nama orang tua'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Jenis kelamin</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'jenis_kelamin',
                                    ['0'=>'Pilih',
                                    '1'=>'Laki-laki',
                                    '2'=>'Perempuan',
                                    '3'=>'Tidak jelas'],
                                    [$row->jenis_kelamin], 
                                    array
                                    (
                                        'class' => 'input-medium id_combobox jenis_kelamin'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Tanggal lahir</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'tanggal_lahir',
                                    Helper::getDate($row->tanggal_lahir),
                                    array
                                    (
                                        'class' => 'input-medium tanggal_lahir',
                                        'placeholder'=>'Tanggal lahir',
                                        'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
                                        'onchange'=>'umur_pe()'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <div class="controls">
                        <input type="text" class="input-mini" value="{{$row->umur}}"  name="tgl_tahun" id="tanggal_tahun">
                        Thn 
                        <input type="text" class="input-mini" name="tgl_bln" value="{{$row->umur_bln}}" id="tanggal_bulan">
                        Bln 
                        <input type="text" class="input-mini" name="tgl_hr" value="{{$row->umur_hr}}" id="tanggal_hari">
                        Hr
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Nomer telepon orang tua</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'telepon',
                                    $row->telepon,
                                    array
                                    (
                                        'class'=>'input-medium',
                                        'placeholder'=>'Telepon'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Pekerjaan orang tua</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'pekerjaan',
                                    $row->pekerjaan,
                                    array
                                    (
                                        'class'=>'input-medium',
                                        'placeholder'=>'Pekerjaan'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Alamat tempat kerja</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'alamat_tempat_kerja',
                                    $row->alamat_tempat_kerja,
                                    array
                                    (
                                        'class'=>'input-medium',
                                        'placeholder'=>''
                                        )
                                    )
                                }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Orang tua/saudara dekat yang dapat dihubungi</label>
                        <div class="controls">
                            <table>
                                <tr>
                                    <td>Nama</td>
                                    <td>:</td>
                                    <td> 
                                        {{
                                            Form::text
                                            (
                                                'nama_saudara_yang_bisa_dihubungi',
                                                $row->nama_saudara_yang_bisa_dihubungi,
                                                array
                                                (
                                                    'class'=>'input-medium',
                                                    'placeholder'=>''
                                                )
                                            )
                                        }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Alamat</td>
                                    <td>:</td>
                                    <td>
                                        {{
                                            Form::text
                                            (
                                                'alamat_saudara_yang_bisa_dihubungi',
                                                $row->alamat_saudara_yang_bisa_dihubungi,
                                                array
                                                (
                                                    'class'=>'input-xlarge',
                                                    'placeholder'=>''
                                                )
                                            )
                                        }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Telepon</td>
                                    <td>:</td>
                                    <td>
                                        {{
                                            Form::text
                                            (
                                                'tlp_saudara_yang_bisa_dihubungi',
                                                $row->tlp_saudara_yang_bisa_dihubungi,
                                                array
                                                (
                                                    'class'=>'input-medium',
                                                    'placeholder'=>''
                                                )
                                            )
                                        }}
                                    </td>
                                </tr>
                            </table>
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Tempat tinggal saat ini</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'tempat_tinggal',
                                    $row->tempat_tinggal, 
                                    array
                                    (
                                        'class' => 'input-xlarge',
                                        'placeholder'=>'tempat tinggal saat ini'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                              <label class="control-label">Provinsi</label>
                              <div class="controls">
                              <select name="" class="input-medium" id="id_provinsi" onchange="showKabupaten()">
                              <?php $pro=Provinsi::lists('provinsi','id_provinsi'); 
                                foreach ($pro as $id_provinsi => $provinsi) 
                                {
                                    if($provinsi==$row->provinsi) 
                                    {
                                        ?>
                                        <option value="{{$id_provinsi}}" selected="selected">{{$provinsi}}</option>
                                        <?php
                                    } 
                                    else 
                                    {
                                        ?>
                                        <option value="{{$id_provinsi}}">{{$provinsi}}</option>
                                        <?php
                                    }
                                } 
                                ?>
                            </select>
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Kabupaten/Kota</label>
                        <div class="controls">
                            <select name="" class="input-medium" id="id_kabupaten" onchange="showKecamatan()">
                                <option value="">{{$row->kabupaten}}</option>
                            </select>
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Kecamatan</label>
                        <div class="controls">
                            <select name="" class="input-medium" id="id_kecamatan" onchange="showKelurahan()">
                                <option value="">{{$row->kecamatan}}</option>
                            </select>
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Kelurahan/Desa</label>
                        <div class="controls">
                            <select name="id_kelurahan" class="input-medium" id="id_kelurahan" onchange="showEpid()">
                                <option value="{{$row->id_kelurahan}}">{{$row->kelurahan}}</option>
                            </select>
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Alamat</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'alamat',
                                    $row->alamat,
                                    array
                                    (
                                        'class'=>'input-xxlarge alamat',
                                        'placeholder'=>'Jalan, RT/RW, Blok, Pemukiman'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Puskesmas</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'puskesmas',
                                    $row->puskesmas,
                                    array
                                    (
                                        'class'=>'input-medium',
                                        'placeholder'=>'Puskesmas'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>
                </fieldset>
                <!-- akhir input kolom identitas penderita -->

                <!-- awal input kolom riwayat sakit -->
                <fieldset>
                    <legend>Riwayat sakit</legend>
                    <div class="control-group">
                        <label class="control-label">Tanggal mulai sakit</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'tanggal_mulai_sakit',
                                    Helper::getDate($row->tanggal_mulai_sakit),
                                    array
                                    (
                                        'class'=>'input-medium',
                                        'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Keluhan pertama yang mendorong untuk berobat?</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'keluhan',
                                    $row->keluhan,
                                    array
                                    (
                                        'class'=>'input-medium'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Gejala dan tanda sakit</label>
                        <div class="controls">
                            <table>
                                <tr>
                                    <td>
                                        @if($row->gejala_sakit_demam!='')
                                        <input type="checkbox" value="demam" name="gejala_sakit_demam" onclick="cek_edit_demam()" checked>
                                        Demam
                                        @else
                                        <input type="checkbox" value="demam" name="gejala_sakit_demam" onclick="cek_edit_demam()">
                                        Demam
                                        @endif
                                    </td>
                                    <td>
                                        {{
                                            Form::text
                                            (
                                                'tanggal_gejala_sakit_demam',
                                                Helper::getDate($row->tanggal_gejala_sakit_demam),
                                                array
                                                (
                                                    'class'=>'input-medium cek_sakit_demam',
                                                    'placeholder'=>'Tanggal demam',
                                                    'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
                                                )
                                            )
                                        }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        @if($row->gejala_sakit_kerongkongan!='')
                                        <input type="checkbox" value="sakit kerongkongan" name="gejala_sakit_kerongkongan" onclick="edit_kerongkongan()" checked>
                                        Sakit Kerongkongan
                                        @else
                                        <input type="checkbox" value="sakit kerongkongan" name="gejala_sakit_kerongkongan" onclick="edit_kerongkongan()">
                                        Sakit Kerongkongan
                                        @endif
                                    </td>
                                    <td>
                                        {{
                                            Form::text
                                            (
                                                'tanggal_gejala_sakit_kerongkongan',
                                                Helper::getDate($row->tanggal_gejala_sakit_kerongkongan),
                                                array
                                                (
                                                    'class'=>'input-medium cek_sakit_kerongkongan',
                                                    'placeholder'=>'Tanggal sakit kerongkongan',
                                                    'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
                                                )
                                            )
                                        }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        @if($row->gejala_leher_bengkak!='')
                                        <input type="checkbox" value="leher bengkak" name="gejala_leher_bengkak" onclick="edit_bengkak()" checked>
                                        Leher bengkak
                                        @else
                                        <input type="checkbox" value="leher bengkak" name="gejala_leher_bengkak" onclick="edit_bengkak()">
                                        Leher bengkak
                                        @endif
                                    </td>
                                    <td>
                                        {{
                                            Form::text
                                            (
                                                'tanggal_gejala_leher_bengkak',
                                                Helper::getDate($row->tanggal_gejala_leher_bengkak),
                                                array
                                                (
                                                    'placeholder'=>'tanggal leher bengkak',
                                                    'class'=>'input-medium cek_sakit_leher',
                                                    'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
                                                )
                                            )
                                        }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        @if($row->gejala_sesak_nafas!='')
                                        <input type="checkbox" value="sesak nafas" name="gejala_sesak_nafas" onclick="edit_sesak()" checked>
                                        Sesak nafas
                                        @else
                                        <input type="checkbox" value="sesak nafas" name="gejala_sesak_nafas" onclick="edit_sesak()">
                                        Sesak nafas
                                        @endif
                                    </td>
                                    <td>
                                        {{
                                            Form::text
                                            (
                                                'tanggal_gejala_sesak_nafas',
                                                Helper::getDate($row->tanggal_gejala_sesak_nafas),
                                                array
                                                (
                                                    'placeholder'=>'tanggal sesak nafas',
                                                    'class'=>'input-medium cek_sesak',
                                                    'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
                                                )
                                            )
                                        }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        @if($row->gejala_pseudomembran!='')
                                        <input type="checkbox" value="pseudomembran" name="gejala_pseudomembran" onclick="edit_membran()" checked>
                                        Pseudemomembran
                                        @else
                                        <input type="checkbox" value="pseudomembran" name="gejala_pseudomembran" onclick="edit_membran()">
                                        Pseudemomembran
                                        @endif
                                    </td>
                                    <td>
                                        {{
                                            Form::text
                                            (
                                                'tanggal_gejala_pseudomembran',
                                                Helper::getDate($row->tanggal_gejala_pseudomembran),
                                                array
                                                (
                                                    'placeholder'=>'tanggal pseudomembran',
                                                    'class'=>'input-medium cek_membran',
                                                    'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
                                                )
                                            )
                                        }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        @if($row->gejala_lain!='')
                                        <input type="checkbox" value="gejala lain" name="gejala" onclick="edit_lain()" checked>
                                        Gejala lain, sebutkan
                                        @else
                                        <input type="checkbox" value="gejala lain" name="gejala" onclick="edit_lain()">
                                        Gejala lain, sebutkan
                                        @endif
                                    </td>
                                    <td> 
                                        {{
                                            Form::text
                                            (
                                                'gejala_lain',
                                                $row->gejala_lain,
                                                array
                                                (
                                                    'placeholder'=>'contoh : batuk,pilek, dll',
                                                    'class'=>'input-large cek_gejala'
                                                )
                                            )
                                        }}
                                    </td>
                                </tr>
                            </table>
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Status imunisasi difteri?</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'status_imunisasi_difteri',
                                    [
                                    '0'=>'Pilih',
                                    '1' => '1X',
                                    '2'=>'2X',
                                    '3'=>'3X',
                                    '4'=>'4X',
                                    '5'=>'5X',
                                    '6'=>'6X',
                                    '7'=>'Tidak',
                                    '8'=>'Tidak tahu'],
                                    [$row->status_imunisasi_difteri], 
                                    array
                                    (
                                        'class' => 'id_combobox'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Jenis spesiemen yang diambil</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'jenis_spesimen',
                                    [
                                    '0'=>'Pilih',
                                    '1' => 'tenggorokan',
                                    '2'=>'Hidung',
                                    '3'=>'Keduanya'],
                                    [$row->jenis_spesimen], 
                                    array
                                    (
                                        'class' => 'id_combobox'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Tanggal pengambilan spesimen</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'tanggal_ambil_spesimen',
                                    Helper::getDate($row->tanggal_ambil_spesimen),
                                    array
                                    (
                                        'class' => 'input-medium',
                                        'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Kode spesimen</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'kode_spesimen',
                                    $row->kode_spesimen,
                                    array
                                    (
                                        'class' => 'input-medium'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>
                </fieldset>
                <!-- akhir input kolom riwayat sakit -->

                <!-- awal input kolom riwayat pengobatan -->
                <fieldset>
                    <legend>Riwayat pengobatan</legend>


                    <div class="control-group">
                        <label class="control-label">Penderita berobat ke</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'tempat_berobat',
                                    [
                                        '0'=>'Pilih',
                                        '1' => 'Rumah sakit',
                                        '2'=>'Puskesmas',
                                        '3'=>'Dokter praktek swasta',
                                        '4'=>'Perawat/mantri/bidan',
                                        '5'=>'Tidak berobat'
                                    ],
                                    [$row->tempat_berobat], 
                                    array
                                    (
                                        'class' => 'id_combobox '
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Dirawat</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'dirawat',
                                    [
                                        '0'=>'Pilih',
                                        '1' => 'Ya',
                                        '2'=>'Tidak'
                                    ],
                                    [$row->dirawat], 
                                    array
                                    (
                                        'class' => 'id_combobox'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Trakeostomi</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'trakeostomi',
                                    [
                                        '0'=>'Pilih',
                                        '1' => 'Ya',
                                        '2'=>'Tidak'
                                    ],
                                    [$row->trakeostomi], 
                                    array
                                    (
                                        'class' => 'id_combobox'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>
                      

                    <div class="control-group">
                        <label class="control-label">Antibiotik</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'antibiotik',
                                    $row->antibiotik,
                                    array
                                    (
                                        'class'=>'input-medium'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Obat lain</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'obat_lain',
                                    $row->obat_lain,
                                    array
                                    (
                                        'class'=>'input-medium'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">ADS (Anti Difteri Serum)</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'ads',
                                    $row->ads,
                                    array
                                    (
                                        'class'=>'input-medium'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Kondisi kasus saat ini</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'kondisi_kasus',
                                    [
                                        '0'=>'Pilih',
                                        '1' => 'Masih sakit',
                                        '2'=>'Sembuh',
                                        '3'=>'Meninggal'
                                    ],
                                    [$row->kondisi_kasus], 
                                    array
                                    (
                                        'class' => 'id_combobox'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>
                </fieldset>
                <!-- akhir input kolom riwayat pengobatan -->

                <!-- awal input kolom riwayat kontak -->
                <fieldset>
                    <legend>Riwayat kontak</legend>
                    <div class="control-group">
                        <label class="control-label">Dalam 2 minggu terakhir sebelum sakit apakah penderita bepergian</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'penderita_berpergian', 
                                    [
                                        '0'=>'Pilih',
                                        '1' => 'Pernah',
                                        '2'=>'Tidak pernah',
                                        '3'=>'Tidak jelas'], 
                                        [$row->penderita_berpergian], 
                                        array
                                    (
                                        'class' => 'id_combobox',
                                        'onchange'=>'cekpergi()',
                                        'id'=>'id_pergi'
                                    )
                                )
                            }}
                            Jika pernah, kemana? : 
                            {{
                                Form::text
                                (
                                    'tempat_berpergian',
                                    $row->tempat_berpergian,
                                    array
                                    (
                                        'class'=>'input-medium cek_pergi'
                                    )
                                )
                            }}
                        <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Dalam 2 minggu terakhir sebelum sakit apakah penderita pernah berkunjung ke rumah teman/saudara yang sakit/meninggal dengan gejala yang sama</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'penderita_berkunjung', 
                                    [
                                        '0'=>'Pilih',
                                        '1' => 'Pernah',
                                        '2'=>'Tidak pernah',
                                        '3'=>'Tidak jelas'
                                    ], 
                                    [$row->penderita_berkunjung], 
                                    array
                                    (
                                        'class' => 'id_combobox ',
                                        'onchange'=>'cekmeninggal()',
                                        'id'=>'id_meninggal'
                                    )
                                )
                            }}
                            Jika pernah, kemana? : {{Form::text('tempat_berkunjung',$row->tempat_berkunjung,array('class'=>'input-medium cek_meninggal'))}}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Dalam 2 minggu terakhir, apakah pernah menerima tamu dengan sakit dengan gejala yang sama</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'penderita_menerima_tamu',
                                    [
                                        '0'=>'Pilih',
                                        '1' => 'Pernah',
                                        '2'=>'Tidak pernah',
                                        '3'=>'Tidak jelas'
                                    ], 
                                    [$row->penderita_menerima_tamu], 
                                    array
                                    (
                                        'class' => 'id_combobox ',
                                        'onchange'=>'ceksama()',
                                        'id'=>'id_sama'
                                    )
                                )
                            }}
                            Jika pernah, dari mana? : 
                            {{
                                Form::text
                                (
                                    'asal_tamu',
                                    $row->asal_tamu,
                                    array
                                    (
                                        'class'=>'input-medium cek_sama'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>
                </fieldset>
                <!-- akhir input kolom riwayat kontak -->

                <!-- awal input kolom kontak kasus -->
                <fieldset>
                <legend>Kontak kasus</legend>
                    <div class="control-group">
                        <label class="control-label">Nama</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'nama',
                                    $row->nama, 
                                    array
                                    (
                                        'class' => 'input-medium'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Umur</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'umur',
                                    $row->umur_kontak, 
                                    array
                                    (
                                        'class' => 'input-mini'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Hubungan dengan kasus</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'hubungan_kasus',
                                    $row->hubungan_kasus, 
                                    array
                                    (
                                        'class' => 'input-medium'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Status imunisasi</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'status_imunisasi',
                                    $row->status_imunisasi, 
                                    array
                                    (
                                        'class' => 'input-medium'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Hasil lab</label>
                        <div class="controls">
                            {{
                                Form::select(
                                    'hasil_lab',
                                    array(
                                        '0'  => 'Pilih',
                                        '1' => 'Positif',
                                        '2' => 'Negatif',
                                    ),
                                    $row->hasil_lab,
                                    array(
                                        'data-validation'         => '[MIXED]',
                                        'data'                    => '$ wajib di isi!',
                                        'data-validation-message' => 'jenis kelamin wajib di isi!',
                                        'class'                   => 'input-medium id_combobox',
                                        'id'                      => 'jenis_kelamin'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Profilaksis</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'profilaksis',
                                    $row->profilaksis,
                                    array
                                    (
                                        'class'=>'input-medium'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>
                </fieldset>
                <!-- akhir input kolom kontak kasus -->


                <div class="form-actions">
                    {{
                        Form::submit
                        (
                            'simpan',
                            array
                            (
                                'class'=>'btn btn-primary'
                            )
                        )
                    }}
                    <a href="{{ URL::to('difteri') }}" class="btn btn-warning">Kembali</a>
                </div>
                @endforeach
                {{Form::close()}}
                <!-- akhir form -->

            </div>
            <!-- akhir class module-body -->
        </div>
        <!-- akhir class module -->
        @include('pemeriksaan.campak.analisis-js')
    </div>
    <!-- akhir class content -->

    <!-- js -->
    <script type="text/javascript">
        $(document).ready(function() 
            {
                $("select").select2();
                showKabupaten();
                showKecamatan();
                showKelurahan(); 
            });
    
            function showKabupaten(){
              var provinsi = $("#id_provinsi").val();
              var kabupaten = $("#id_kabupaten").val();
              // //kirim data ke server
              $('.lodingKab').html('<img src="{{URL::to("style/images/loding.gif")}}">');
              $.post('{{URL::to("provinsi/getkab")}}', {provinsi:provinsi,kabupaten:kabupaten}, function(response)
              {
                  $('#id_kabupaten').removeAttr('disabled','');
                  $("#id_kabupaten").html(response);
                  $('.lodingKab').html('');
              });

            }

            function showKecamatan(){
              var kabupaten = $("#id_kabupaten").val();
              var kecamatan = $("#id_kecamatan").val();
              // //kirim data ke server
              $('.lodingKec').html('<img src="{{URL::to("style/images/loding.gif")}}">');
              $.post('{{URL::to("provinsi/getkec")}}', {kabupaten:kabupaten,kecamatan:kecamatan}, function(response)
              {
                  $('#id_kecamatan').removeAttr('disabled','');
                  $("#id_kecamatan").html(response);
                  $('.lodingKec').html('');
              });

            }

            function showKelurahan(){
              var kecamatan= $("#id_kecamatan").val();
              var kelurahan = $("#id_kelurahan").val();
              // //kirim data ke server
              $('.lodingKel').html('<img src="{{URL::to("style/images/loding.gif")}}">');
              $.post('{{URL::to("provinsi/getkel")}}', {kecamatan:kecamatan,kelurahan:kelurahan}, function(response)
              { 
                  $('#id_kelurahan').removeAttr('disabled','');
                  $("#id_kelurahan").html(response);
                  $('.lodingKel').html('');
              });

            }
    </script>
    <!-- js -->
</div>
<!-- akhir class span12 -->
@stop

