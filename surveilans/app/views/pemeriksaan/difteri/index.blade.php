@extends('layouts.master')
@section('content')
<div class="span12">
	<div class="content">
		<div class="module">
			<div class="module-body">
				<div class="profile-head media">
					<h4>
						Penyakit Difteri
					</h4>
					<hr>
				</div>
				{{-- Tab Navigasi --}}
				@include('pemeriksaan.difteri._include_to_index.tab_navigasi')
				<div class="profile-tab-content tab-content">
					{{-- Daftar Kasus --}}
					<div class="tab-pane fade active in" id="daftar">
						@if(Session::get('type')=='rs')
						@include('pemeriksaan._include.filter_daftar_kasus_rs')
						@elseif(Session::get('type')=='puskesmas')
						@include('pemeriksaan._include.filter_daftar_kasus_puskesmas')
						@else
						@include('pemeriksaan._include.filter_daftar_kasus')
						@endif
						@include('pemeriksaan.difteri._include_to_index.daftar_kasus')
					</div>
					{{-- Input Data Individual --}}
					<div class="tab-pane fade" id="input-data">
						@include('pemeriksaan.difteri._include_to_index.input_data_individual_kasus')
					</div>
					{{-- Analisis Kasus --}}
					<div class="tab-pane fade" id="analisis">
						@include('pemeriksaan.campak.analisis')
					</div>
					{{-- PE --}}
					<div class="tab-pane fade" id="PE">
						<div id="data_difteri"></div>
					</div>
					<div class="tab-pane fade in" id="importdifteri">
						@include('pemeriksaan.difteri.import')
					</div>
				</div>{{-- /.tab-content --}}
			</div>{{--/.module-body--}}
		</div>{{-- /.module --}}
		@include('pemeriksaan.campak.analisis-js')
	</div>{{-- /.content --}}
</div>{{-- /.span12 --}}
@stop