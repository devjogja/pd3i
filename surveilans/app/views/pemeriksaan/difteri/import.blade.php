<div class="module-body uk-overflow-container">
	{{Form::open(array('url'=>'afp.extract','class'=>'form-horizontal','files'=> true,'method'=>'POST'))}}
	<fieldset>
		<legend>Import Difteri</legend>
		<div class="control-group">
			<label class="control-label">Filename</label>
			<div class="controls">
				{{ Form::file('excel') }}
			</div>
		</div>
	</fieldset>
	<div class="form-actions">
		{{ Form::submit('Upload', array('class' => 'btn btn-default')) }}
	</div>
	{{ Form::close() }}
</div>