@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <h4>
                        Detail penyelidikan epidemologi kasus penyakit difteri
                    </h4>
                    <hr>
                </div>
<div class="tabbable-panel">
	<div class="tabbable-line">
		<ul class="nav nav-tabs ">
			<li class="active">
				<a href="#tab_default_1" data-toggle="tab">
				Identitas penderita</a>
			</li>
			<li>
				<a href="#tab_default_2" data-toggle="tab">
				Riwayat sakit</a>
			</li>
			<li>
				<a href="#tab_default_3" data-toggle="tab">
				Riwayat pengobatan
				</a>
			</li>
			<li>
				<a href="#tab_default_4" data-toggle="tab">
				Riwayat kontak
				</a>
			</li>
			<li>
				<a href="#tab_default_5" data-toggle="tab">
				Riwayat kasus
				</a>
			</li>
		</ul>
		@if($data)
		@foreach($data as $row)
		<div class="tab-content">
			<div class="tab-pane active" id="tab_default_1">
				<table class="table table-striped">
				<tr>
			          <td>Nomer Epidemologi</td>
			          <td>{{$row->no_epid}}</td>
			      </tr>
			      <tr>
			          <td>Nama Pasien</td>
			          <td>{{$row->nama_anak}}</td>
			      </tr>
			      <tr>
			          <td>Nama Orang tua</td>
			          <td>{{$row->nama_ortu}}</td>
			      </tr>
			      <tr>
			          <td>Nomer telepon orang tua</td>
			          <td>{{$row->telepon}}</td>
			      </tr>
			      <tr>
			          <td>Pekerjaan orang tua</td>
			          <td>{{$row->pekerjaan}}</td>
			      </tr>
			      <tr>
			          <td>Tempat tinggal saat ini</td>
			          <td>{{$row->tempat_tinggal}}</td>
			      </tr>
			      <tr>
			          <td>Puskesmas</td>
			          <td>{{$row->puskesmas}}</td>
			      </tr>
			      <tr>
			          	<td>Jenis Kelamin</td>
				        @if($row->jenis_kelamin==1)
				        <td>Laki-laki</td>
				        @elseif($row->jenis_kelamin==2)
				        <td>Perempuan</td>
				        @elseif($row->jenis_kelamin==3)
				        <td>Tidak jelas</td>
				        @endif
			      </tr>
			      <tr>
			          <td>Tanggal Lahir</td>
			          <td>{{Helper::getDate($row->tanggal_lahir)}}</td>
			      </tr>
			      <tr>
			          <td>Umur</td>
			          <td>{{$row->umur}} tahun {{$row->umur_bln}} bulan {{$row->umur_hr}} hari</td>
			      </tr>
			      <tr>
			          <td>Alamat</td>
			          <td>{{$row->alamat}}</td>
			      </tr>
				</table>
			</div>
			<div class="tab-pane" id="tab_default_2">
				<table class="table table-striped">
				<tr>
					<td>Tanggal mulai sakit</td>
					<td>{{Helper::getDate($row->tanggal_mulai_sakit)}}</td>
				</tr>
				<tr>
					<td>Keluhan pertama yang mendorong untuk berobat?</td>
					<td>{{$row->keluhan}}</td>
				</tr>
				<tr>
					<td>Status imunisasi difteri?</td>
					@if($row->status_imunisasi_difteri==0)
					<td></td>
					@else
					<td>{{$row->status_imunisasi_difteri}}</td>
					@endif
				</tr>
				<tr>
					<td>Jenis spesiemen yang diambil</td>
					<td>
					<?php
						if ($row->jenis_spesimen==1) {
						 	echo "Tenggorokan";
						 } 
						elseif ($row->jenis_spesimen==2) {
							echo "Hidung";
						}
						elseif ($row->jenis_spesimen==3) {
							echo "Keduanya";
						}else{
							echo "";
						}
					?>
					</td>
				</tr>
				<tr>
					<td>Kode spesimen</td>
					<td>{{$row->kode_spesimen}}</td>
				</tr>
				<br/>
				<tr>
					<table class="table table-striped">
						<tr>
							<td>Gejala dan tanda sakit</td>
							<td>Tanggal sakit</td>
						</tr>
						<?php if (!empty($row->gejala_sakit_demam)) {?>
						<tr>
							<td>{{$row->gejala_sakit_demam}}</td>
							<td>{{Helper::getDate($row->tanggal_gejala_sakit_demam)}}</td>
						</tr>
						<?php }
						if(!empty($row->gejala_sakit_kerongkongan)) {?>
						<tr>
							<td>{{$row->gejala_sakit_kerongkongan}}</td>
							<td>{{Helper::getDate($row->tanggal_gejala_sakit_kerongkongan)}}</td>
						</tr>
						<?php }
						if(!empty($row->gejala_leher_bengkak)) { ?>
						<tr>
							<td>{{$row->gejala_leher_bengkak}}</td>
							<td>{{Helper::getDate($row->tanggal_gejala_leher_bengkak)}}</td>
						</tr>
						<?php }
						if(!empty($row->gejala_sesak_nafas)) { ?>
						<tr>
							<td>{{$row->gejala_sesak_nafas}}</td>
							<td>{{Helper::getDate($row->tanggal_gejala_sesak_nafas)}}</td>
						</tr>
						<?php }
						if(!empty($row->gejala_pseudomembran)) {?>
						<tr>
							<td>{{$row->gejala_pseudomembran}}</td>
							<td>{{Helper::getDate($row->tanggal_gejala_pseudomembran)}}</td>
						</tr>
						<?php }?>
						<tr>
							<td>{{$row->gejala_lain}}</td>
							<td></td>
						</tr>
					</table>
				</tr> 
				</table>
			</div>
			<div class="tab-pane" id="tab_default_3">
				<table class="table table-striped">
				<tr>
					<td>Penderita berobat ke</td>
					<td>
					<?php 
						if($row->tempat_berobat==1) {
							echo "Rumah sakit"; 
						}
						elseif ($row->tempat_berobat==2) {
							echo "Puskesmas";
						}
						elseif ($row->tempat_berpergian==3) {
							echo "Dokter praktek swasta";
						}
						elseif ($row->tempat_berobat==4) {
							echo "Perawat/mantri/bidan";
						}
						elseif ($row->tempat_berobat==5) {
							echo "Tidak berobat";
						}else{
							echo "";
						}
					?>
					</td>
				</tr>
				<tr>
					<td>Dirawat</td>
					<td>
						<?php
							if($row->dirawat==1) {
							echo "Ya";
							} 
							elseif ($row->dirawat==2) {
								echo "Tidak";
							}
							else {
								echo "";
							}
						?>
				</tr>
				<tr>
					<td>Trakeostomi</td>
					<td>
						<?php
							if($row->trakeostomi==1) {
							echo "Ya";
							} 
							elseif ($row->trakeostomi==2) {
								echo "Tidak";
							}
							else {
								echo "";
							}
						?>
				</tr>
				<tr>
					<td>Antibiotik</td>
					<td>{{$row->antibiotik}}</td>
				</tr>
				<tr>
					<td>Obat lain</td>
					<td>{{$row->obat_lain}}</td>
				</tr>
				<tr>
					<td>ADS (Anti Difteri Serum)</td>
					<td>{{$row->ads}}</td>
				</tr>
				<tr>
					<td>Kondisi kasus saat ini</td>
					<td>
					<?php 
						if($row->kondisi_kasus==1) {
							echo "Masih sakit";
						} 
						elseif ($row->kondisi_kasus==2) {
							echo "Sembuh";
						}
						elseif($row->kondisi_kasus==3) {
							echo "Meninggal";
						}else {
							echo "";
						}
					?>
					</td>
				</tr>
				</table>
			</div>
			<div class="tab-pane" id="tab_default_4">
				<table class="table table-striped">
				<tr>
					<td>Dalam 2 minggu terakhir sebelum sakit apakah penderita bepergian</td>
					<td>
					@if($row->penderita_berpergian==1)
						Pernah
					@elseif ($row->penderita_berpergian==2)
						Tidak pernah
					@elseif ($row->penderita_berpergian==3)
						Tidak jelas
					@else
					@endif
					</td>
				</tr>
				@if($row->penderita_berpergian==1)
				<tr>
					<td>tempat berpergian</td>
					<td>{{$row->tempat_berpergian}}</td>
				</tr>
				@endif
				<tr>
					<td>Dalam 2 minggu terakhir sebelum sakit apakah penderita pernah berkunjung ke rumah teman/saudara yang sakit/meninggal dengan gejala yang sama</td>
					<td>
					@if($row->penderita_berkunjung==1)
						Pernah
					@elseif ($row->penderita_berkunjung==2)
						Tidak pernah
					@elseif ($row->penderita_berkunjung==3)
						Tidak jelas
					@else
					@endif
					</td>
				</tr>
				<tr>
					@if($row->penderita_berkunjung==1)
					<tr>
						<td>tempat berkunjung</td>
						<td>{{$row->tempat_berkunjung}}</td>
					</tr>
					@endif
				</tr>
				<tr>
					<td>Dalam 2 minggu terakhir, apakah pernah menerima tamu dengan sakit dengan gejala yang sama</td>
					<td>
					@if($row->penderita_menerima_tamu==1)
						Pernah
					@elseif ($row->penderita_menerima_tamu==2)
						Tidak pernah
					@elseif ($row->penderita_menerima_tamu==3)
						Tidak jelas
					@else
					@endif
					</td>
				</tr>
				<tr>
					@if($row->penderita_menerima_tamu==1)
					<tr>
						<td>Asal tamu/siapa yang berkunjung/tempat tinggal tamu</td>
						<td>{{$row->asal_tamu}}</td>
					</tr>
					@endif
				</tr>
				</table>
			</div>
			<div class="tab-pane" id="tab_default_5">
				<table class="table table-striped">
				<tr>
					<td>Nama</td>
					<td>{{$row->nama}}</td>
				</tr>
				<tr>
					<td>Umur</td>
					<td>{{$row->umur_kontak}} tahun</td>
				</tr>
				<tr>
					<td>Hubungan dengan kasus</td>
					<td>{{$row->hubungan_kasus}}</td>
				</tr>
				<tr>
					<td>Status imunisasi</td>
					<td>{{$row->status_imunisasi}}</td>
				</tr>
				<tr>
					<td>Hasil lab</td>
					@if($row->hasil_lab==1)
					<td>Positif</td>
					@elseif($row->hasil_lab==2)
					<td>Negatif</td>
					@else
					<td></td>
					@endif
				</tr>
				<tr>
					<td>Profilaksis</td>
					<td>{{$row->profilaksis}}</td>
				</tr>
				</table>
			</div>
		</div>
		@endforeach
		@endif
	</div>
</div>
</div>
</div>
<div class="form-actions" style="text-align:center">
  	<a href="{{URL::to('difteri')}}" class="btn btn-warning">Kembali</a>
</div>
</div>
</div>
@stop