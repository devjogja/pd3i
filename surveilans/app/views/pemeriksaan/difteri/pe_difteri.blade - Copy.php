@extends('layouts.master')
@section('content')
<style type="text/css">
	.saudara_dihubungi td{
		padding: 4px;
	}
</style>

<!-- awal class span12 -->
<div class="span12">
	<!-- awal class content -->
	<div class="content">
		<!-- awal class module -->
		<div class="module">
			<!-- awal class module-body -->
			<div class="module-body">
				<div class="profile-head media">
					<h4>
						Input data penyelidikan epidemologi kasus difteri
					</h4>
					<hr>
				</div>
				@if(!empty($pe_difteri))
				<!-- awal form -->
				{{Form::open(
					array(
						'route'=>'pe_difteri',
						'class'=>'form-horizontal'
						)
					)
				}}
				@foreach($pe_difteri as $row)
				<input type="hidden" name="id_difteri" value="<?php echo $row->id_difteri?>">
				<fieldset>
					<legend>Identitas pelapor</legend>
					<!-- awal input kolom no epidemologi -->
					<div class="control-group">
						<label class="control-label">Nama</label>
						<div class="controls">
							{{Form::text('dp[nama_pelapor]',null,array('class' => 'input-medium','placeholder'=>'Nama Pelapor'))}}
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Nama Kantor</label>
						<div class="controls">
							{{Form::text('dp[nama_kantor]',null,array('class' => 'input-medium','placeholder'=>'Nama kantor'))}}
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Jabatan</label>
						<div class="controls">
							{{Form::text('dp[jabatan]',null,array('class' => 'input-medium','placeholder'=>'Jabatan'))}}
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Provinsi</label>
						<div class="controls">
							{{Form::select('dp[id_provinsi]',array(null => 'Pilih provinsi')+ Provinsi::lists('provinsi','id_provinsi'),null,array('id'=>'id_provinsi_pelapor','onchange' => "showKabupaten('_pelapor')",'class' => 'input-large id_combobox'))}}
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Kabupaten/Kota</label>
						<div class="controls">
							{{Form::select('dp[id_kabupaten]',array(null => 'Pilih kabupaten'),null,array('class' => 'input-large id_combobox','id' => 'id_kabupaten_pelapor','onchange' => "showKecamatan('_pelapor')"))}}
							<span class="lodingKab"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Tanggal laporan</label>
						<div class="controls">
							{{Form::text('dp[tgl_laporan]',null,array('class' => 'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
						</div>
					</div>
				</fieldset>

				<!-- awal fieldset -->
				<fieldset>
					<legend>Identitas penderita</legend>
					<!-- awal input kolom no epidemologi -->
					<div class="control-group">
						<label class="control-label">No. epidemologi</label>
						<div class="controls">
							{{Form::text(
								'no_epid',
								$row->no_epid, 
								array(
									'class' => 'input-medium',
									'placeholder'=>'No. epidemologi'
									)
								)
							}}
							{{Form::hidden(
								'id_pasien',
								$row->id_pasien,
								array(
									'class'=>'input-medium id_pasien'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Nama penderita</label>
						<div class="controls">
							{{Form::text(
								'nama_anak',
								$row->nama_anak, 
								array(
									'class' => 'input-medium nama_anak',
									'placeholder'=>'Nama penderita'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">NIK</label>
						<div class="controls">
							{{Form::text('nik',$row->nik, array('class' => 'input-medium','placeholder'=>'NIK'))}}
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Nama orang tua/KK</label>
						<div class="controls">
							{{Form::text(
								'nama_ortu',
								$row->nama_ortu, 
								array(
									'class' => 'input-medium nama_ortu',
									'placeholder'=>'Nama orang tua'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Jenis kelamin</label>
						<div class="controls">
							{{Form::select(
								'jenis_kelamin',
								[
								null => 'Pilih',
								'1'=>'Laki-laki',
								'2'=>'Perempuan',
								'3' =>'Tidak diketahui',
								],
								[$row->jenis_kelamin], 
								array(
									'class' => 'input-medium id_combobox jenis_kelamin'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Tanggal lahir</label>
						<div class="controls">
							{{Form::text(
								'tanggal_lahir',
								Helper::getDate($row->tanggal_lahir), 
								array(
									'class' => 'input-medium tanggal_lahir',
									'placeholder'=>'Tanggal lahir',
									'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
									'onchange'=>'umur_pe()'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Umur</label>
						<div class="controls">
							<input type="text" class="input-mini" value="{{$row->umur}}" name="tgl_tahun" id="tanggal_tahun">
							Thn 
							<input type="text" class="input-mini" value="{{$row->umur_bln}}" name="tgl_bln" id="tanggal_bulan">
							Bln 
							<input type="text" class="input-mini" value="{{$row->umur_hr}}" name="tgl_hr" id="tanggal_hari">
							Hr
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Nomer telepon</label>
						<div class="controls">
							{{Form::text(
								'telepon',
								Input::old('telepon'),
								array(
									'class'=>'input-medium',
									'placeholder'=>'Telepon'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Tempat tinggal saat ini</label>
						<div class="controls">
							{{Form::textarea('tempat_tinggal',null, array('class' => 'input-xlarge','placeholder'=>'tempat tinggal saat ini','rows'=>'3'))}}
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Tempat tinggal sesuai kartu identitas/alamat asal</label>
						<div class="controls">
							{{Form::textarea('tempat_tinggal_berdasar_alamat',null, array('class' => 'input-xlarge','placeholder'=>'tempat tinggal sesuai kartu identitas/alamat','rows'=>'3'))}}
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Pekerjaan</label>
						<div class="controls">
							{{Form::text('pekerjaan',Input::old('pekerjaan'),array('class'=>'input-xlarge','placeholder'=>'Pekerjaan'))}}
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Alamat tempat kerja</label>
						<div class="controls">
							{{Form::textarea('alamat_tempat_kerja',Input::old('alamat_tempat_kerja'),array('class'=>'input-xlarge','placeholder'=>'Alamat tempat kerja','rows'=>'3'))}}
							<span class="help-inline"></span>
						</div>
					</div>

					<!-- <div class="control-group">
						<label class="control-label">Provinsi</label>
						<div class="controls">
						<select name="" class="input-medium id_combobox" id="id_provinsi" onchange="showKabupaten()">
							<?php $pro=Provinsi::lists('provinsi','id_provinsi'); 
							 foreach ($pro as $id_provinsi => $provinsi) {
								if($provinsi==$row->provinsi) {
								  ?>
								  <option value="{{$id_provinsi}}" selected="selected">{{$provinsi}}</option>
								  <?php
								} else {
								?>
								  <option value="{{$id_provinsi}}">{{$provinsi}}</option>
								<?php
								}
							  } 
							?>
							</select>
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Kabupaten/Kota</label>
						<div class="controls">
							<select name="" class="input-medium id_combobox" id="id_kabupaten" onchange="showKecamatan()">
								<option value="">{{$row->kabupaten}}</option>
							</select>
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Kecamatan</label>
						<div class="controls">
							<select name="" class="input-medium id_combobox" id="id_kecamatan" onchange="showKelurahan()">
								<option value="">{{$row->kecamatan}}</option>
							</select>
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Kelurahan/Desa</label>
						<div class="controls">
							<select name="id_kelurahan" class="input-medium id_combobox" id="id_kelurahan" onchange="showEpid()">
								<option value="{{$row->id_kelurahan}}">{{$row->kelurahan}}</option>
							</select>
							<span class="help-inline"></span>
						</div>
					</div> 

					<div class="control-group">
						<label class="control-label">Alamat</label>
						<div class="controls">
							{{Form::text(
								'alamat',
								$row->alamat, 
								array(
									'class' => 'input-xlarge alamat',
									'placeholder'=>'tulisan alamat seperti nama dusun/gang/blok/no jalan/rt/rw'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div> -->
					<!-- akhir input kolom alamat -->

					<!-- awal input kolom puskesmas -->
					@endforeach
					<div class="control-group">
						<label class="control-label">Faskes tempat periksa (puskesmas)</label>
						<div class="controls">
							<?php
							//ambil nama puskesmas scara otomatis sesuia user yg login
							$type = Session::get('type');
							$kd_faskes = Session::get('kd_faskes');
							if($type == "puskesmas"){
								$q = "
								SELECT b.puskesmas_name,b.alamat
								FROM puskesmas b
								WHERE b.puskesmas_code_faskes='".$kd_faskes."'
								";
								$data=DB::select($q);
								for($i=0;$i<count($data);$i++){
									$nama_instansi = "PUSKESMAS ". $data[0]->puskesmas_name;
									echo Form::text('nama_puskesmas',$nama_instansi, array('class' => 'input-large','placeholder'=>'Masukan nama puskesmas')) ;
									
								}
							}
							?>
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Orang tua/saudara dekat yang dapat dihubungi</label>
						<div class="controls">
							<table class="saudara_dihubungi">
								<tr>
									<td>Nama</td>
									<td>:</td>
									<td> 
										{{Form::text(
											'nama_saudara_yang_bisa_dihubungi',
											Input::old('nama_saudara_yang_bisa_dihubungi'),
											array(
												'class'=>'input-medium',
												'placeholder'=>''
												)
											)
										}}
									</td>
								</tr>
								<tr>
									<td>Alamat</td>
									<td>:</td>
									<td>
										<table>
											<tr>
												<td>Provinsi</td>
												<td>
													{{Form::select('ds[id_provinsi_saudara]',array(null => 'Pilih provinsi')+ Provinsi::lists('provinsi','id_provinsi'),null,array('id'=>'id_provinsi_saudara','onchange' => "showKabupaten('_saudara')",'class' => 'input-large id_combobox'))}}
												</td>
											</tr>
											<tr>
												<td>Kabupaten</td>
												<td>
													{{Form::select('dp[id_kabupaten_saudara]',array(null => 'Pilih kabupaten'),null,array('class' => 'input-large id_combobox','id' => 'id_kabupaten_saudara','onchange' => "showKecamatan('_saudara')"))}}
												</td>
											</tr>
											<tr>
												<td>Kecamatan</td>
												<td>
													{{Form::select('dp[id_kecamatan_saudara]',array(null => 'Pilih Kecamatan'),null,array('class' => 'input-large id_combobox','id' => 'id_kecamatan_saudara','onchange' => "showKelurahan('_saudara')"))}}
												</td>
											</tr>
											<tr>
												<td>Kelurahan</td>
												<td>
													{{Form::select('dp[id_kelurahan_saudara]',array(null => 'Pilih Kelurahan'),null,array('class' => 'input-large id_combobox','id' => 'id_kelurahan_saudara'))}}
												</td>
											</tr>
											<tr>
												<td>Alamat</td>
												<td>
													{{Form::textarea('alamat_saudara_yang_bisa_dihubungi',null,array('rows'=>'3','class'=>'input-xlarge','placeholder'=>''))}}
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>Telepon</td>
									<td>:</td>
									<td>
										{{Form::text(
											'tlp_saudara_yang_bisa_dihubungi',
											Input::old('tlp_saudara_yang_bisa_dihubungi'),
											array(
												'class'=>'input-medium',
												'placeholder'=>''
												)
											)
										}}
									</td>
								</tr>
							</table>
							<span class="help-inline"></span>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>Riwayat sakit</legend>
					<div class="control-group">
						<label class="control-label">Tanggal mulai sakit</label>
						<div class="controls">
							{{Form::text(
								'tanggal_mulai_sakit',
								Crs::getDate($row->tanggal_timbul_demam),
								array(
									'data-validation'         => '[MIXED]',
									'data'                    => '$ wajib di isi!',
									'data-validation-message' => 'Tanggal wajib di isi!',
									'class'=>'input-medium',
									'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
									)
								)
							}}
							<span class="help-inline" id="error-nama_anak" style="color:red">(*)</span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Keluhan utama yang mendorong untuk berobat?</label>
						<div class="controls">
							{{Form::text(
								'keluhan',
								Input::old('keluhan'),
								array(
									'class'=>'input-medium'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Gejala dan tanda sakit</label>
						<div class="controls">
							<table>
								<tr>
									<td>
										<input type="checkbox" value="demam" name="gejala_sakit_demam" onclick="cek_demam()">
										Demam
									</td>
									<td>
										{{Form::text(
											'tanggal_gejala_sakit_demam',
											Input::old('tanggal_gejala_sakit_demam'),
											array(
												'class'=>'input-medium cek2',
												'placeholder'=>'Tanggal demam',
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												'disabled'=>'disabled'
												)
											)
										}}
									</td>
								</tr>
								<tr>
									<td>
										<input type="checkbox" value="sakit kerongkongan" name="gejala_sakit_kerongkongan" onclick="kerongkongan()">
										Sakit Kerongkongan
									</td>
									<td>
										{{Form::text(
											'tanggal_gejala_sakit_kerongkongan',
											Input::old('tanggal_gejala_sakit_kerongkongan'),
											array(
												'class'=>'input-medium cek4',
												'placeholder'=>'Tanggal sakit kerongkongan',
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												'disabled'=>'disabled'
												)
											)
										}}
									</td>
								</tr>
								<tr>
									<td>
										<input type="checkbox" value="leher bengkak" name="gejala_leher_bengkak" onclick="bengkak()">
										Leher bengkak
									</td>
									<td>
										{{Form::text(
											'tanggal_gejala_leher_bengkak',
											Input::old('tanggal_leher_bengkak'),
											array(
												'placeholder'=>'tanggal leher bengkak',
												'class'=>'input-medium cek5',
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												'disabled'=>'disabled'
												)
											)
										}}
									</td>
								</tr>
								<tr>
									<td>
										<input type="checkbox" value="sesak nafas" name="gejala_sesak_nafas" onclick="sesak()">
										Sesak nafas
									</td>
									<td>
										{{Form::text(
											'tanggal_gejala_sesak_nafas',
											Input::old('tanggal_gejala_sesak_nafas'),
											array(
												'placeholder'=>'tanggal sesak nafas',
												'class'=>'input-medium cek7',
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												'disabled'=>'disabled'
												)
											)
										}}
									</td>
								</tr>
								<tr>
									<td>
										<input type="checkbox" value="pseudomembran" name="gejala_pseudomembran" onclick="membran()">
										Pseudemomembran
									</td>
									<td>
										{{Form::text(
											'tanggal_gejala_pseudomembran',
											Input::old('tanggal_gejala_pseudomembran'),
											array(
												'placeholder'=>'tanggal pseudomembran',
												'class'=>'input-medium cek9',
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												'disabled'=>'disabled'
												)
											)
										}}
									</td>
								</tr>
								<tr>
									<td>
										<input type="checkbox" value="gejala lain" name="gejala" onclick="lain()">
										Gejala lain, sebutkan
									</td>
									<td> 
										{{Form::text(
											'gejala_lain',
											Input::old('gejala_lain'),
											array(
												'placeholder'=>'contoh : batuk,pilek, dll',
												'class'=>'input-large cek10',
												'disabled'=>'disabled'
												)
											)
										}}
									</td>
								</tr>
							</table>
							<span class="help-inline"></span>
						</div>
					</div>
					<!-- akhir input kolom gejala -->

					<!-- awal input kolom status imunisasi difteri -->
					<!-- <div class="control-group">
						<label class="control-label">Status imunisasi difteri?</label>
						<div class="controls">
							{{Form::select
							(
								'status_imunisasi_difteri', 
								array
								(
									'0'=>'Pilih',
									'1' => '1X',
									'2'=>'2X',
									'3'=>'3X',
									'4'=>'4X',
									'5'=>'5X',
									'6'=>'6X',
									'7'=>'Tidak',
									'8'=>'Tidak tahu'
								),
								Input::old('status_imunisasi_difteri'), 
								array
								(
									'class' => 'input-medium'
								)
							)
							}}
							<span class="help-inline"></span>
						</div>
					</div> -->

					<div class="control-group">
						<label class="control-label">Status imunisasi difteri</label>
						<div class="controls">
							<table>
								<tr>
									<td style="width: 30%">
										{{Form::select('status_imunisasi_difteri', array(null=>'Pilih','1'=>'Belum pernah','2'=>'Sudah pernah','3'=>'Tidak tahu'),null, array('class' => 'input-medium status_imunisasi_difteri'))}}
									</td>
									<td>
										<table class="imun">
											<thead>
												<tr>
													<th>Berapa kali imunisasi difteri</th>
													<th>Tahun imunisasi difteri</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>{{Form::text('berapa_kali_imunisasi_difteri[]',NULL,array('placeholder'=>'Berapa kali','class'=>'input-large '))}}</td>
													<td>{{Form::text('tahun_imunisasi_difteri[]',NULL,array('placeholder'=>'Tahun','class'=>'input-large'))}}</td>
													<td><a href="javascript:void(0)" class="btn btn-xs btn-success add_imunisasi" data-uk-tooltip title="kImunisasi" ><i class="icon icon-plus"></i></a></td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<!-- <div class="control-group">
						<label class="control-label">Jenis spesimen yang diambil</label>
						<div class="controls">
							{{Form::select
							(
								'jenis_spesimen', 
								array
								(
									'0'=>'Pilih',
									'1' => 'tenggorokan',
									'2'=>'Hidung',
									'3'=>'Keduanya'
								),
								Input::old('jenis_spesimen'), 
								array
								(
									'class' => 'input-medium'
								)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Tanggal pengambilan spesimen</label>
						<div class="controls">
							{{Form::text
							(
								'tanggal_ambil_spesimen',
								Input::old('tanggal_ambil_spesimen'), 
								array
								(
									'class' => 'input-medium',
									'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
								)
							)
							}}
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Kode spesimen</label>
						<div class="controls">
							{{Form::text
							(
								'kode_spesimen',
								Input::old('kode_spesimen'),
								array
								(
									'class' => 'input-medium'
								)
							)
							}}
							<span class="help-inline"></span>
						</div>
					</div> -->

				</fieldset>

				<fieldset>
					<legend>Data Spesimen</legend>
					<table class="table table-bordered" id="data_spesimen">
						<thead>
							<tr>
								<th>Jenis Spesimen</th>
								<th>Tanggal ambil spesimen</th>
								<th>Kode spesimen</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{{Form::select('jenis_spesimen[]', array(null=>'Pilih','1' => 'Tenggorokan','2'=>'Hidung','3'=>'Keduanya'),null, array('class' => 'input-xlarge'))}}</td>
								<td>{{Form::text('tgl_ambil_spesimen[]',null, array('class' => 'input-xlarge','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}</td>
								<td>{{Form::text('kode_spesimen[]',null,array('class' => 'input-xlarge'))}}</td>
								<td><a href="javascript:void(0)" class="btn btn-xs btn-success add_spesimen" data-uk-tooltip title="Tambah Spesimen" ><i class="icon icon-plus"></i></a></td>
							</tr>
						</tbody>
					</table>
				</fieldset>
				
				<fieldset>
					<legend>Riwayat pengobatan</legend>
					<!-- awal input kolom penderita berobat -->
					<div class="control-group">
						<label class="control-label">Penderita berobat ke</label>
						<div class="controls">
							<div class="checkbox">
								{{ Form::checkbox('tempat_berobat[]', '1',  null, array('class'=>'tempat_berobat1')) }} Rumah Sakit
								{{ Form::select('dirawat', array(null=>'Pilih','1' => 'Ya','2'=>'Tidak'),null, array('class' => 'input-small dirawat','disabled'))}}
							</div>
							<div class="checkbox">
								{{ Form::checkbox('tempat_berobat[]', '2',  null, array('class'=>'tempat_berobat2')) }} Puskesmas
							</div>
							<div class="checkbox">
								{{ Form::checkbox('tempat_berobat[]', '3',  null, array('class'=>'tempat_berobat3')) }} Dokter praktek swasta
							</div>
							<div class="checkbox">
								{{ Form::checkbox('tempat_berobat[]', '4',  null, array('class'=>'tempat_berobat4')) }} Perawat/mantri/bidan
							</div>
							<span class="help-inline"></span>
						</div>
					</div>
					<!-- akhir input kolom penderita berobat -->

					<!-- awal input kolom dirawat -->
					<!-- <div class="control-group">
						<label class="control-label">Dirawat</label>
						<div class="controls">
							{{Form::select
							(
								'dirawat', 
								array
								(
									'0'=>'Pilih',
									'1' => 'Ya',
									'3'=>'Tidak'
								),
								Input::old('dirawat'), 
								array
								(
									'class' => 'input-small'
								)
							)
							}}
							<span class="help-inline"></span>
						</div>
					</div> -->
					<!-- akhir input kolom dirawat -->

					<!-- awal input kolom trakeostomi -->
					<div class="control-group">
						<label class="control-label">Trakeostomi</label>
						<div class="controls">
							{{Form::select
								(
									'trakeostomi', 
									array
									(
										'0'=>'Pilih',
										'1' => 'Ya',
										'2'=>'Tidak'
										),
									Input::old('trakeostomi'), 
									array
									(
										'class' => 'input-small'
										)
									)
								}}
								<span class="help-inline"></span>
							</div>
						</div>
						<!-- akhir input kolom trakeostomi -->

						<!-- awal input kolom antibiotik -->
						<div class="control-group">
							<label class="control-label">Antibiotik</label>
							<div class="controls">
								{{Form::text
									(
										'antibiotik',
										Input::old('antibiotik'),
										array
										(
											'class'=>'input-medium'
											)
										)
									}}
									<span class="help-inline"></span>
								</div>
							</div>
							<!-- akhir input kolom antibiotik -->

							<!-- awal input kolom obat lain -->
							<div class="control-group">
								<label class="control-label">Obat lain</label>
								<div class="controls">
									{{Form::text
										(
											'obat_lain',
											Input::old('obat_lain'),
											array
											(
												'class'=>'input-medium'
												)
											)
										}}
										<span class="help-inline"></span>
									</div>
								</div>
								<!-- akhir input kolom obat lain -->

								<!-- awal input kolom ADS -->
								<div class="control-group">
									<label class="control-label">ADS (Anti Difteri Serum)</label>
									<div class="controls">
										{{Form::text
											(
												'ads',
												Input::old('ads'),
												array
												(
													'class'=>'input-medium'
													)
												)
											}}
											<span class="help-inline"></span>
										</div>
									</div>
									<!-- akhir input kolom ADS -->

									<!-- awal input kolom kondisi kasus -->
									<div class="control-group">
										<label class="control-label">Kondisi kasus saat ini</label>
										<div class="controls">
											{{Form::select
												(
													'kondisi_kasus', 
													array
													(
														'0'=>'Pilih',
														'1' => 'Masih sakit',
														'2'=>'Sembuh',
														'3'=>'Meninggal'
														),
													Input::old('kondisi_kasus'), 
													array
													(
														'class' => 'input-medium'
														)
													)
												}}
												<span class="help-inline"></span>
											</div>
										</div>
										<!-- akhir input kolom kondisi kasus -->
									</fieldset>
									<!-- akhir fieldset -->

									<!-- awal fieldset -->
									<fieldset>
										<legend>Riwayat kontak</legend>
										<!-- awal input kolom konfirmasi berpergian -->
										<div class="control-group">
											<label class="control-label">Dalam 2 minggu terakhir sebelum sakit apakah penderita bepergian</label>
											<div class="controls">
												{{Form::select
													(
														'penderita_berpergian', 
														array
														(
															'0'=>'Pilih',
															'1' => 'Pernah',
															'2'=>'Tidak pernah',
															'3'=>'Tidak jelas'
															), 
														Input::old('penderita_berpergian'), 
														array
														(
															'class' => 'input-medium',
															'onchange'=>'cekpergi()',
															'id'=>'id_pergi'
															)
														)
													}}
													Jika pernah, kemana? : 
													{{Form::text
														(
															'tempat_berpergian',
															Input::old('tempat_berpergian'),
															array
															(
																'class'=>'input-medium cek_pergi',
																'disabled'=>'disabled'
																)
															)
														}}
														<span class="help-inline"></span>
													</div>
												</div>
												<!-- akhir input kolom konfirmasi berpergian -->

												<!-- awal input kolom konfirmasi berkunjung -->
												<div class="control-group">
													<label class="control-label">Dalam 2 minggu terakhir sebelum sakit apakah penderita pernah berkunjung ke rumah teman/saudara yang sakit/meninggal dengan gejala yang sama</label>
													<div class="controls">
														{{Form::select
															(
																'penderita_berkunjung', 
																array
																(
																	'0'=>'Pilih',
																	'1' => 'Pernah',
																	'2'=>'Tidak pernah',
																	'3'=>'Tidak jelas'
																	), 
																Input::old('penderita_berkunjung'), 
																array
																(
																	'class' => 'input-medium',
																	'onchange'=>'cekmeninggal()',
																	'id'=>'id_meninggal'
																	)
																)
															}}
															Jika pernah, kemana? : 
															{{Form::text
																(
																	'tempat_berkunjung',
																	Input::old('tempat_berkunjung'),
																	array
																	(
																		'class'=>'input-medium cek_meninggal',
																		'disabled'=>'disabled'
																		)
																	)
																}}
																<span class="help-inline"></span>
															</div>
														</div>
														<!-- akhir input kolom konfirmasi berkunjung -->

														<!-- awal input kolom konfirmasi menerima tamu -->
														<div class="control-group">
															<label class="control-label">Dalam 2 minggu terakhir, apakah pernah menerima tamu dengan sakit dengan gejala yang sama</label>
															<div class="controls">
																{{Form::select
																	(
																		'penderita_menerima_tamu', 
																		array
																		(
																			'0'=>'Pilih',
																			'1' => 'Pernah',
																			'2'=>'Tidak pernah',
																			'3'=>'Tidak jelas'
																			), 
																		Input::old('penderita_menerima_tamu'), 
																		array
																		(
																			'class' => 'input-medium',
																			'onchange'=>'ceksama()',
																			'id'=>'id_sama'
																			)
																		)
																	}}
																	Jika pernah, dari mana? : 
																	{{Form::text
																		(
																			'asal_tamu',
																			Input::old('asal_tamu'),
																			array
																			(
																				'class'=>'input-medium cek_sama',
																				'disabled'=>'disabled'
																				)
																			)
																		}}
																		<span class="help-inline"></span>
																	</div>
																</div>
																<!-- akhir input kolom konfirmasi menerima tamu -->              
															</fieldset>
															<!-- akhir fieldset -->

															<!-- awal fieldset -->
															<fieldset>
																<legend>Kontak kasus</legend>
				<!-- <div class="control-group">
					<label class="control-label">Nama</label>
					<div class="controls">
						{{
							Form::text
							(
								'nama',
								Input::old('nama'), 
								array
								(
									'class' => 'input-medium'
								)
							)
						}}
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Umur</label>
					<div class="controls">
						{{
							Form::text
							(
								'umur',
								Input::old('umur'), 
								array
								(
									'class' => 'input-mini'
								)
							)
						}}
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Hubungan dengan kasus</label>
					<div class="controls">
					{{
						Form::text
						(
							'hubungan_kasus',
							Input::old('hubungan_kasus'), 
							array
							(
								'class' => 'input-medium'
							)
						)
					}}
					<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Status imunisasi</label>
					<div class="controls">
						{{
							Form::text
							(
								'status_imunisasi',
								Input::old('status_imunisasi'), 
								array
								(
									'class' => 'input-medium'
								)
							)
						}}
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Hasil lab</label>
					<div class="controls">
						{{
							Form::select
							(
								'hasil_lab', 
								array
								(
									'0'=>'Pilih',
									'1' => 'Positif',
									'2'=>'Negatif'
								), 
								Input::old('hasil_lab'), 
								array
								(
									'class' => 'input-small'
									)
								)
							}}
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Profilaksis</label>
					<div class="controls">
						{{
							Form::text
							(
								'profilaksis',
								Input::old('profilaksis'),
								array
								(
									'class'=>'input-medium'
								)
							)
						}}
						<span class="help-inline"></span>
					</div>
				</div> -->
				<table class="table table-bordered" id="data_kontak_kasus">
					<thead>
						<tr>
							<th>Nama</th>
							<th>Umur</th>
							<th>Hubungan dengan kasus</th>
							<th>Status imunisasi</th>
							<th>Hasil Lab</th>
							<th>Profilaksis</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>{{Form::text('nama_kontak_kasus[]',null,array('class' => 'input-medium'))}}</td>
							<td>{{Form::text('umur_kontak_kasus[]',null,array('class' => 'input-small'))}}</td>
							<td>{{Form::text('hub_dgn_kontak_kasus[]',null,array('class' => 'input-medium'))}}</td>
							<td>{{Form::select('status_imunisasi_kontak_kasus[]',array(null=>'Pilih','1'=>'Sudah Pernah','2'=>'Belum pernah','3'=>'Tidak tahu'),null,array('class' => 'input-medium'))}}</td>
							<td>{{Form::select('hasil_lab_kontak_kasus[]',array(null=>'Pilih','1'=>'Positif','2'=>'Negatif'),null,array('class' => 'input-medium'))}}</td>
							<td>{{Form::text('Profilaksis_kontak_kasus[]',null,array('class' => 'input-medium'))}}</td>
							<td><a href="javascript:void(0)" class="btn btn-xs btn-success add_kontak_kasus" data-uk-tooltip title="Tambah Kontak kasus" ><i class="icon icon-plus"></i></a></td>
						</tr>
					</tbody>
				</table>
			</fieldset>
			<!-- akhir fieldset -->
			<div class="form-actions">
				{{
					Form::submit
					(
						'simpan',
						array
						(
							'class'=>'btn btn-success'
							)
						)
					}}
				</div>
				{{Form::close()}}
				@endif
				<a href="{{URL::to('difteri')}}" class="btn btn-primary">batal</a>
				<!-- akhir form -->
			</div>
		</div>
	</div>
	@include('pemeriksaan.campak.analisis-js')
</div>

<script type="text/javascript">
	$(function(){
		$('.imun').hide();
		$('.status_imunisasi_difteri').on('change',function(){
			var val = $(this).val();
			if(val=='2'){
				$('.imun').show(200);
			}else{
				$('.imun').hide(200);
			}
			return false;
		});
		$('.add_imunisasi').on('click',function(){
			$('.imun').append('<tr><td>{{Form::text('berapa_kali_imunisasi_difteri[]',NULL,array('placeholder'=>'Berapa kali','class'=>'input-large'))}}</td>'+
				'<td>{{Form::text('tahun_imunisasi_difteri[]',NULL,array('placeholder'=>'Tahun','class'=>'input-large'))}}</td>'+
				'<td><a href="javascript:void(0)" class="btn btn-xs btn-warning remove" ><i class="icon icon-remove"></i></a></td>'+
				'</tr>'
				);
			$('.remove').on('click',function(){
				$(this).parent().parent().remove();
				return false;
			});
			return false;
		});
		$('.add_spesimen').on('click',function(){
			$('#data_spesimen tbody').append('<tr>'+
				'<td>{{Form::select('jenis_spesimen[]', array(null=>'Pilih','1' => 'Tenggorokan','2'=>'Hidung','3'=>'Keduanya'),null, array('class' => 'input-xlarge'))}}</td>'+
				'<td>{{Form::text('tgl_ambil_spesimen',null, array('class' => 'input-xlarge','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}</td>'+
				'<td>{{Form::text('kode_spesimen[]',null,array('class' => 'input-xlarge'))}}</td>'+
				'<td><a href="javascript:void(0)" class="btn btn-xs btn-warning rmSpesimen" ><i class="icon icon-remove"></i></a></td>'+
				'</tr>');
			$('.rmSpesimen').on('click',function(){
				$(this).parent().parent().remove();
				return false;
			});
			$('select').select2();
			return false;
		});
		$('.add_kontak_kasus').on('click',function(){
			$('#data_kontak_kasus tbody').append('<tr>'+
				'<td>{{Form::text('nama_kontak_kasus[]',null,array('class' => 'input-medium'))}}</td>'+
				'<td>{{Form::text('umur_kontak_kasus[]',null,array('class' => 'input-small'))}}</td>'+
				'<td>{{Form::text('hub_dgn_kontak_kasus[]',null,array('class' => 'input-medium'))}}</td>'+
				'<td>{{Form::select('status_imunisasi_kontak_kasus[]',array(null=>'Pilih','1'=>'Sudah Pernah','2'=>'Belum pernah','3'=>'Tidak tahu'),null,array('class' => 'input-medium'))}}</td>'+
				'<td>{{Form::select('hasil_lab_kontak_kasus[]',array(null=>'Pilih','1'=>'Positif','2'=>'Negatif'),null,array('class' => 'input-medium'))}}</td>'+
				'<td>{{Form::text('Profilaksis_kontak_kasus[]',null,array('class' => 'input-medium'))}}</td>'+
				'<td><a href="javascript:void(0)" class="btn btn-xs btn-warning rmKontakKasus" ><i class="icon icon-remove"></i></a></td>'+
				'</tr>');
			$('.rmKontakKasus').on('click',function(){
				$(this).parent().parent().remove();
				return false;
			});
			$('select').select2();
			return false;
		});

		$('.tempat_berobat1').on('click',function(){
			if($('.tempat_berobat1').is(':checked')){
				$('.dirawat').removeAttr('disabled');
			}else{
				$('.dirawat').select2('val',null);
				$('.dirawat').attr('disabled','disabled');
			}
		});
	})
</script>
@stop
