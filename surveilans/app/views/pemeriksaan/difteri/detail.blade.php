@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <h4>
                        Detail Kasus Penyakit Difteri
                    </h4>
                    <hr>
                </div>
<table class="table table-striped">
@if($difteri)
@foreach($difteri as $row)
    <tr>
        <td>No. Epidemologi</td>
        <td>{{$row->no_epid}}</td>
    </tr>
    <tr>
        <td>No. Epidemologi lama</td>
        <td>{{$row->no_epid_lama}}</td>
    </tr>
    <tr>
        <td>No induk kependudukan</td>
        <td>{{$row->nik}}</td>
    </tr>
    <tr>
        <td>Nama Pasien</td>
        <td>{{$row->nama_anak}}</td>
    </tr>
    <tr>
        <td>Nama Orang tua</td>
        <td>{{$row->nama_ortu}}</td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        @if($row->jenis_kelamin==1)
        <td>Laki-laki</td>
        @elseif($row->jenis_kelamin==2)
        <td>Perempuan</td>
        @elseif($row->jenis_kelamin==3)
        <td>Tidak jelas</td>
        @endif
    </tr>
    <tr>
        <td>Tanggal Lahir</td>
        <td>{{Helper::getDate($row->tanggal_lahir)}}</td>
    </tr>
    <tr>
        <td>Umur</td>
        <td>{{$row->umur}} tahun {{$row->umur_bln}} Bulan {{$row->umur_hr}} Hari</td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>{{$row->alamat_pasien}}</td>
    </tr>
</table>
<hr>
@endforeach
@endif
<br/>
<div class="module-body uk-overflow-container">
<div class="table-responsive">
<table class="table table-bordered" style="font-size:10px">
  <tr style="background-color:#eee;">
    <td rowspan="2">Tanggal periksa</td>
    <td rowspan="2">Tanggal timbul/mulai demam</td>
    <td rowspan="2">Vaksin Difteri sebelum sakit</td>
    <td rowspan="2">Tanggal vaksinasi difteri terakhir</td>
    <td rowspan="2">Tanggal pelacakan</td>
    <td colspan="2">Tanggal pengambilan spesimen</td>
  </tr>
  <tr style="background-color:#eee;">
    <td>Tenggorokan</td>
    <td>Hidung</td>
  </tr>
   @if($difteri)
   @foreach($difteri as $result)
  <tr>
    <td>{{Helper::getDate($result->tanggal_periksa);}}</td>
    <td>{{Helper::getDate($result->tanggal_timbul_demam);}}</td>
    @if($result->vaksin_DPT_sebelum_sakit==1)
    <td>1X</td>
    @elseif($result->vaksin_DPT_sebelum_sakit==2)
    <td>2X</td>
    @elseif($result->vaksin_DPT_sebelum_sakit==3)
    <td>3X</td>
    @elseif($result->vaksin_DPT_sebelum_sakit==4)
    <td>4X</td>
    @elseif($result->vaksin_DPT_sebelum_sakit==5)
    <td>5X</td>
    @elseif($result->vaksin_DPT_sebelum_sakit==6)
    <td>6X</td>
    @elseif($result->vaksin_DPT_sebelum_sakit==7)
    <td>Tidak</td>
    @elseif($result->vaksin_DPT_sebelum_sakit==8)
    <td>Tidak tahu</td>
    @endif
    <td>{{Helper::getDate($result->tanggal_vaksinasi_difteri_terakhir);}}</td>
    <td>{{Helper::getDate($result->tanggal_pelacakan)}}</td>
    <td>{{Helper::getDate($result->tanggal_diambil_spesimen_tenggorokan)}}</td>
    <td>{{Helper::getDate($result->tanggal_diambil_spesimen_hidung)}}</td>
  </tr>
  @endforeach
  @endif
</table>
</div>
</div>
<div class="module-body uk-overflow-container">
<div class="table-responsive">
<table class="table table-bordered" style="font-size:10px">
  <tr style="background-color:#eee;">
    <td colspan="4" style="text-align:center">Hasil lab</td>
    <td rowspan="3" style="text-align:center">Keadaan akhir</td>
    <td colspan="3" style="text-align:center">Kontak</td>
    <td rowspan="3" style="text-align:center">Klasifikasi final</td>
  </tr>
  <tr style="background-color:#eee;">
    <td colspan="2" style="text-align:center">Kultur</td>
    <td colspan="2" style="text-align:center">Mikroskop</td>
    <td rowspan="2" style="text-align:center">Jumlah</td>
    <td rowspan="2" style="text-align:center">Diambil spec (swab hidung)</td>
    <td rowspan="2" style="text-align:center">Positif</td>
  </tr>
  <tr style="background-color:#eee;">
    <td style="text-align:center">Tenggorokan</td>
    <td style="text-align:center">Hidung</td>
    <td style="text-align:center">Tenggorokan</td>
    <td style="text-align:center">Hidung</td>
  </tr>
   @if($difteri)
   @foreach($difteri as $rslt)
  <tr>
    @if($rslt->hasil_lab_kultur_tenggorokan==1)
    <td style="text-align:center">Positif</td>
    @elseif($rslt->hasil_lab_kultur_tenggorokan==2)
    <td style="text-align:center">Negatif</td>
    @else
    <td></td>
    @endif
    @if($rslt->hasil_lab_kultur_hidung==1)
    <td style="text-align:center">Positif</td>
    @elseif($rslt->hasil_lab_kultur_hidung==2)
    <td style="text-align:center">Negatif</td>
    @else
    <td></td>
    @endif
    @if($rslt->hasil_lab_mikroskop_tenggorokan==1)
    <td style="text-align:center">Positif</td>
    @elseif($rslt->hasil_lab_mikroskop_tenggorokan==2)
    <td style="text-align:center">Negatif</td>
    @else
    <td></td>
    @endif
    @if($rslt->hasil_lab_mikroskop_hidung==1)
    <td style="text-align:center">Positif</td>
    @elseif($rslt->hasil_lab_mikroskop_hidung==2)
    <td style="text-align:center">Negatif</td>
    @else
    <td></td>
    @endif
    @if($rslt->keadaan_akhir==1)
    <td style="text-align:center">Hidup/sehat</td>
    @elseif($rslt->keadaan_akhir==2)
    <td style="text-align:center">Meninggal</td>
    @else
    <td></td>
    @endif
    <td style="text-align:center">{{($rslt->jumlah_kontak==0)?'':$rslt->jumlah_kontak;}}</td>
    <td style="text-align:center">{{($rslt->jumlah_swab_hidung==0)?'':$rslt->jumlah_swab_hidung;}}</td>
    <td style="text-align:center">{{($rslt->jumlah_positif==0)?'':$rslt->jumlah_positif;}}</td>
    @if($rslt->jumlah_positif==1)
    <td style="text-align:center">Probable</td>
    @elseif($rslt->jumlah_positif==2)
    <td style="text-align:center">Konfirm</td>
    @elseif($rslt->jumlah_positif==3)
    <td style="text-align:center">Negatif</td>
    @else
    <td></td>
    @endif
  </tr>
  @endforeach
  @endif
</table>
</div>
</div>
<div class="form-actions" style="text-align:center">
    <a href="{{URL::to('difteri')}}" class="btn btn-warning">Kembali</a>
</div>
</div>
</div>
@stop
