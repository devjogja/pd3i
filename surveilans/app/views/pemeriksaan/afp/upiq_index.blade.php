@extends('layouts.master')
@section('content')
<div class="span12">
	<div class="content">
		<div class="module">
			<div class="module-body" >
				<div class="profile-head media">
					<h4>Penyakit AFP</h4>
					<hr>
				</div>

			{{-- Tab Navigasi --}}
				@include('pemeriksaan.afp._include_to_index.tab_navigasi')

			{{-- Content Dari Tab --}}
				<div class="profile-tab-content tab-content">

				{{-- Daftar Kasus --}}
					<div class="tab-pane fade active in" id="activity">
						@include('pemeriksaan._include.filter_daftar_kasus')
						@include('pemeriksaan.afp._include_to_index.daftar_kasus')
					</div>

					<div id="results"></div>

				{{-- Input Data Individual Kasus --}}
					<div class="tab-pane fade" id="friends">
						@include('pemeriksaan.afp._include_to_index.input_data_individual_kasus')
					</div>

				{{-- Analisis Kasus AFP --}}
					@include('pemeriksaan.campak.analisis')

			    @include('pemeriksaan.campak.analisis-js')

			    @include('pemeriksaan.afp.afp-js')

			  {{-- PE AFP --}}
					<div class="tab-pane fade in" id="PE">
						<!-- penyelidikan epidemologi AFP -->
						<div id="data_afp"></div>
					</div>

				</div> <!-- /.tab-content-->
			</div> <!--/.module-body-->
		</div> <!--/.module-->
	</div> <!--/.content-->
</div> <!--/.span9-->

<script>
	$(document).ready(function() {
  	$( '#form_save_afp' ).validate({
		  submit: {
				settings: {
				  scrollToError: {
					  offset: -100,
				  	duration: 500
			  	}
				}
		  }
		});

	  $( '#form_save_afp input' ).keydown(function(e) {
	  	if(e.keyCode==13) {
				// check for submit button and submit form on enter press
			 	if($(':input:eq(' + ($(':input').index(this) + 1) + ')').attr('type')=='submit'){
			  	return true;
			  }

			  $(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();
			 	return false;
	  	}
	  });

	  $( '.btn-add-row' ).click(function(){
	  	$( '#example' ).append('<tr><td>Lalalala</td><td>#</td><td>#</td><td>#</td><td>#</td><td>#</td><td>#</td></tr>');
	  });

	  // $('#example2').dataTable();

	});
</script>

@stop