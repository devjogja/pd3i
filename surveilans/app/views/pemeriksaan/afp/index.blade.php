@extends('layouts.master')
@section('content')
<div class="span12">
	<div class="content">
		<div class="module">
			<div class="module-body" >
				<div class="profile-head media">
					<h4>Penyakit AFP</h4>
					<hr>
				</div>
				@include('pemeriksaan.afp._include_to_index.tab_navigasi')
				<div class="profile-tab-content tab-content">
					<div class="tab-pane fade active in" id="daftar">
						@if(Session::get('type')=='rs')
						@include('pemeriksaan._include.filter_daftar_kasus_rs')
						@elseif(Session::get('type')=='puskesmas')
						@include('pemeriksaan._include.filter_daftar_kasus_puskesmas')
						@else
						@include('pemeriksaan._include.filter_daftar_kasus')
						@endif
						@include('pemeriksaan.afp._include_to_index.daftar_kasus')
					</div>
					<div id="results"></div>
					<div class="tab-pane fade" id="input">
						@include('pemeriksaan.afp._include_to_index.input_data_individual_kasus')
					</div>
					@include('pemeriksaan.campak.analisis')
					@include('pemeriksaan.campak.analisis-js')
					@include('pemeriksaan.afp.afp-js')
					<div class="tab-pane fade in" id="PE">
						<div id="data_afp"></div>
					</div>
					<div class="tab-pane fade in" id="importafp">
						@include('pemeriksaan.afp.import')
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$("#example_afp_length select").select2();
		$("select").select2();  
		$('#example_afp').dataTable();
		$('#example_pe_afp').dataTable();
		$('#data_afp').load('{{URL::to("daftar_pe_afp")}}')
		
		$('#form_save_afp').validate({
			submit: {
				settings: {
					scrollToError: {
						offset: -100,
						duration: 500
					}
				}
			}
		});

		$('#form_save_afp input').keydown(function(e){
			if(e.keyCode==13){       

								if($(':input:eq(' + ($(':input').index(this) + 1) + ')').attr('type')=='submit'){// check for submit button and submit form on enter press
									return true;
								}

								$(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();

								return false;
							}

						});
	});
	function pilihOptionTglLahir()
	{
		$('.tgl_lahir').removeAttr('readonly');
		$('#tgl_mulai_sakit').removeAttr('class');
		$('#tgl_tahun').attr('readonly','readonly');
		$('#tgl_bulan').attr('readonly','readonly');
		$('#tgl_hari').attr('readonly','readonly');
		$('.tgl_lahir').val('');
		$('#tgl_tahun').val('');
		$('#tgl_bulan').val('');
		$('#tgl_hari').val('');
	}

	function pilihOptionUmur()
	{
		$('#tgl_tahun').removeAttr('readonly');
		$('#tgl_bulan').removeAttr('readonly');
		$('#tgl_hari').removeAttr('readonly');
		$('#tgl_mulai_sakit').attr('class','input-medium tgl_mulai_sakit');
		$('.tgl_lahir').attr('readonly','readonly');
		$('.tgl_lahir').val('');
		$('#tgl_tahun').val('');
		$('#tgl_bulan').val('');
		$('#tgl_hari').val('');           
	}
</script>

@stop