@extends('layouts.master')
@section('content')

<style type="text/css">
	.gejala_tanda {
		border-collapse: collapse;
		width: 100%;
	}
	.gejala_tanda th, td {
		text-align: left;
		padding: 6px;
	}
	.gejala_tanda tr:nth-child(even){background-color: #f2f2f2}
	.gejala_tanda th {
		background-color: #4CAF50;
		color: white;
		text-align: left;
	}
	.pengumpulan_spesimen {
		border-collapse: collapse;
		width: 100%;
	}
	.pengumpulan_spesimen th, td {
		text-align: left;
		padding: 6px;
	}
	.pengumpulan_spesimen tr:nth-child(even){background-color: #f2f2f2}
	.pengumpulan_spesimen th {
		background-color: #4CAF50;
		color: white;
		text-align: left;
	}
</style>

<div class="span12">
	<div class="content">
		<div class="module">
			<div class="module-body">
				<div class="profile-head media">
					<h4>
						Penyelidikan Epidemologi Pasien Kasus Penyakit AFP
					</h4>
					<hr>
				</div>
				{{Form::open(array('route'=>'pe_afp','class'=>'form-horizontal'))}}
				<div class="module-body">
					<div class="row-fluid">
						<div class="span12">
							<div class="media">
							<?php $row = $pe_afp[0];?>
								<input type="hidden" name="id_afp" value="{{$row->afp_id_afp}}">
								<fieldset>
									<legend>Identitas penderita</legend>
									<div class="control-group">
										<label class="control-label">No. epidemologi</label>
										<div class="controls">
											{{Form::text('dp[no_epid]',$row->afp_no_epid,array('class'=>'input-large','placeholder'=>'Masukan nomer epidemologi'))}}
											{{Form::hidden('dp[id_pasien]',$row->pasien_id_pasien,array('class'=>'input-medium id_pasien'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Nama penderita</label>
										<div class="controls">
											{{Form::text('dp[nama_anak]',$row->pasien_nama_anak,array('class'=>'input-medium nama_anak'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Nama orang tua</label>
										<div class="controls">
											{{Form::text('dp[nama_ortu]',$row->pasien_nama_ortu,array('class'=>'input-medium nama_ortu'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Jenis kelamin</label>
										<div class="controls">
											{{Form::select('dp[jenis_kelamin]',[
											'0' => 'Pilih',
											'1'=>'Laki-laki',
											'2'=>'Perempuan',
											'3' => 'Tidak jelas'],[$row->pasien_jenis_kelamin], array('class' => 'input-large id_combobox jenis_kelamin'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Tanggal lahir</label>
										<div class="controls">
											{{Form::text('dp[tanggal_lahir]',$row->pasien_tanggal_lahir, array('class' => 'input-medium tanggal_lahir','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','onchange'=>'umur_pe()'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Umur</label>
										<div class="controls">
											<input type="text" class="input-mini" value="{{$row->pasien_umur}}" name="dp[tgl_tahun]" id="tanggal_tahun">
											Thn 
											<input type="text" class="input-mini" value="{{$row->pasien_umur_bln}}" name="dp[tgl_bln]" id="tanggal_bulan">
											Bln 
											<input type="text" class="input-mini" value="{{$row->pasien_umur_hr}}" name="dp[tgl_hr]" id="tanggal_hari">
											Hr
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Provinsi</label>
										<div class="controls">
											{{ Form::select('dp[id_provinsi]',
											array(''=>'Pilih provinsi')+Provinsi::lists('provinsi','id_provinsi'),
											$row->pasien_id_provinsi,
											array('id'=>'id_provinsi_pasien',
											'placeholder' => 'Pilih Provinsi',
											'onchange'=>'getKabupaten("_pasien")',
											'class'=>'input-large')) }}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Kabupaten/Kota</label>
										<div class="controls">
											<select name="" class="input-large id_combobox" id="id_kabupaten_pasien" onchange="getKecamatan('_pasien')">
												<option value="">{{$row->pasien_kabupaten}}</option>
											</select>
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Kecamatan</label>
										<div class="controls">
											<select name="" class="input-large id_combobox" id="id_kecamatan_pasien" onchange="getKelurahan('_pasien')">
												<option value="">{{$row->pasien_kecamatan}}</option>
											</select>
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Kelurahan/Desa</label>
										<div class="controls">
											<select name="dp[id_kelurahan]" class="input-large id_combobox" id="id_kelurahan_pasien">
												<option value="{{$row->pasien_id_kelurahan}}">{{$row->pasien_kelurahan}}</option>
											</select>
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Alamat</label>
										<div class="controls">
											{{Form::textarea('dp[alamat]',$row->pasien_alamat, array('class' => 'input-large alamat','rows'=>'4','placeholder'=>'tulisan alamat seperti nama dusun/gang/blok/no jalan/rt/rw'))}}
											<span class="help-inline"></span>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<div class="media">
								<fieldset>
									<legend>Sumber informasi</legend>
									<div class="control-group">
										<label class="control-label">Provinsi</label>
										<div class="controls">
											{{ Form::select('propinsi',
											array(''=>'Pilih provinsi')+Provinsi::lists('provinsi','id_provinsi'),
											null,
											array('id'=>'id_provinsi_sumber',
											'placeholder' => 'Pilih Provinsi',
											'onchange'=>'getKabupaten("_sumber")',
											'class'=>'input-large')) }}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Kabupaten</label>
										<div class="controls">
											{{Form::select('kabupaten',array(null=>'Pilih Kabupaten'),Input::old('kabupaten'),array('class'=>'input-large','id'=>'id_kabupaten_sumber'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Laporan dari</label>
										<div class="controls">
											{{Form::select('laporan_dari',array(''=>'Pilih','Rumah sakit' => 'Rumah sakit','Puskesmas'=>'Puskesmas','Dokter praktek'=>'Dokter praktek','Lainnya'=>'Lainnya'),Input::old('kelumpuhan'),array('class'=>'input-large'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" >Keterangan sumber laporan</label>
										<div class="controls">
											{{Form::textarea('ket_sumber_laporan',Input::old('ket_sumber_laporan'),array('cols'=>'55','rows'=>'3'))}}
											<span class="help-inline"></span>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<div class="media">
								<fieldset>
									<legend>Riwayat sakit</legend>
									<div class="control-group">
										<label class="control-label" >Tanggal mulai sakit</label>
										<div class="controls">
											{{Form::text('tanggal_mulai_sakit',Input::old('tanggal_mulai_sakit'),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
											<span class="help-inline"></span>
										</div> 
									</div>
									<div class="control-group">
										<label class="control-label" >Tanggal mulai lumpuh</label>
										<div class="controls">
											{{Form::text('tanggal_mulai_lumpuh',Input::old('tanggal_mulai_lumpuh'),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
											<span class="help-inline"></span>
										</div> 
									</div>
									<div class="control-group">
										<label class="control-label" >Tanggal meninggal (bila penderita meninggal) </label>
										<div class="controls">
											{{Form::text('tanggal_meninggal',Input::old('tanggal_meninggal'),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
											<span class="help-inline"></span>
										</div> 
									</div>
									<div class="control-group">
										<label class="control-label" >Sebelum dilaporkan, apakah penderita berobat ke unit pelayanan lain?</label>
										<div class="controls">
											{{Form::select('berobat_unit_pelayanan', array(
											'0'=>'Pilih',
											'1' => 'Ya',
											'2'=>'Tidak'),Input::old('berobat_unit_pelayanan'), array('class' => 'input-small','id'=>'id_unit','onchange'=>'cekBerobat()'))}}
										</div>
									</div>
									<div class="control-group">
										<div class="control-label">
											<table>
												<tr>
													<td>Nama unit pelayanan</td>
												</tr>
												<tr>
													<td></td>
												</tr>
												<tr>
													<td>Tanggal berobat</td>
												</tr>
												<tr>
													<td>&nbsp;</td>
												</tr>
												<tr>
													<td>Diagnosis</td>
												</tr>
												<tr>
													<td></td>
												</tr>
												<tr>
													<td>NO. Rekam medis</td>
												</tr>
											</table>
										</div>
										<div class="controls">
											<table>
												<tr>
													<td> : {{Form::text('nama_unit_pelayanan',Input::old('nama_unit_pelayanan'),array('id'=>'unit_1','disabled'=>'disabled','class'=>'input-medium id_unit_pelayanan'))}}</td>
												</tr>
												<tr>
													<td> : {{Form::text('tanggal_berobat',Input::old('tanggal_berobat'),array('id'=>'unit_2','disabled'=>'disabled','class'=>'input-small id_berobat','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}</td>
												</tr>
												<tr>
													<td> : {{Form::text('diagnosis',Input::old('diagnosis'),array('id'=>'unit_3','disabled'=>'disabled','class'=>'input-small id_diagnosis'))}}</td>
												</tr>
												<tr>
													<td> : {{Form::text('no_rekam_medis',Input::old('no_rekam_medis'),array('id'=>'unit_4','disabled'=>'disabled','class'=>'input-medium id_rekam_medis'))}}</td>
												</tr>
											</table>
											<span class="help-inline"></span>
										</div> 
									</div>
									<div class="control-group">
										<label class="control-label">Apakah kelumpuhan sifatnya akut (1-14 hari)?</label>
										<div class="controls">
											{{Form::select('kelumpuhan_akut',array(
											'0'=>'Pilih',
											'1' => 'Ya',
											'2'=>'Tidak',
											'3'=>'Tidak jelas'),Input::old('kelumpuhan_akut'),array('class'=>'input-large','id'=>'id_akut','onchange'=>'cekPelacakan()'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Apakah kelumpuhan sifatnya layuh (flaccid)?</label>
										<div class="controls">
											{{Form::select('kelumpuhan_layuh',array(
											'0'=>'Pilih',
											'1' => 'Ya',
											'2'=>'Tidak',
											'3'=>'Tidak jelas'),Input::old('kelumpuhan_layuh'),array('class'=>'input-large','id'=>'id_layuh','onchange'=>'cekPelacakan()'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Apakah kelumpuhan disebabkan ruda?</label>
										<div class="controls">
											{{Form::select('kelumpuhan_ruda',array(
											'0'=>'Pilih',
											'1' => 'Ya',
											'2'=>'Tidak',
											'3'=>'Tidak jelas'),Input::old('kelumpuhan_ruda'),array('class'=>'input-large','id'=>'id_lumpuh','onchange'=>'cekPelacakan()'))}}
											<span class="help-inline"></span>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
					<br>
				<div class="stop">
					<div class="row-fluid">
						<div class="span12">
							<div class="media">
								<fieldset>
									<legend>Gejala tanda</legend>
									<div class="control-group">
										<label class="control-label">Apakah penderita demam sebelum lumpuh ?</label>
										<div class="controls">
											{{ Form::select('demam_sebelum_lumpuh',array(null=>'Pilih','1' => 'Ya','2'=>'Tidak'),null,array('class'=>'input-large'))}}
										</div>
									</div>
									</br>
									<table class="gejala_tanda">
										<tr>
											<th style="width:40%"></th>
											<th>Kelumpuhan</th>
											<th>Gangguan Raba</th>
										</tr>
										<tr>
											<td>Tungkai Kanan</td>
											<td>{{Form::select('kelumpuhan_tungkai_kanan',array(''=>'Pilih','ya'=>'Ya','tidak'=>'Tidak'),null,array('class'=>'input-large'))}}</td>
											<td>{{Form::select('gangguan_raba_tungkai_kanan',array(''=>'Pilih','ya'=>'Ya','tidak'=>'Tidak'),null,array('class'=>'input-large'))}}</td>
										</tr>
										<tr>
											<td>Tungkai Kiri</td>
											<td>{{Form::select('kelumpuhan_tungkai_kiri',array(''=>'Pilih','ya'=>'Ya','tidak'=>'Tidak'),null,array('class'=>'input-large'))}}</td>
											<td>{{Form::select('gangguan_raba_tungkai_kiri',array(''=>'Pilih','ya'=>'Ya','tidak'=>'Tidak'),null,array('class'=>'input-large'))}}</td>
										</tr>
										<tr>
											<td>Lengan Kanan</td>
											<td>{{Form::select('kelumpuhan_lengan_kanan',array(''=>'Pilih','ya'=>'Ya','tidak'=>'Tidak'),null,array('class'=>'input-large'))}}</td>
											<td>{{Form::select('gangguan_raba_lengan_kanan',array(''=>'Pilih','ya'=>'Ya','tidak'=>'Tidak'),null,array('class'=>'input-large'))}}</td>
										</tr>
										<tr>
											<td>Lengan Kiri</td>
											<td>{{Form::select('kelumpuhan_lengan_kiri',array(''=>'Pilih','ya'=>'Ya','tidak'=>'Tidak'),null,array('class'=>'input-large'))}}</td>
											<td>{{Form::select('gangguan_raba_lengan_kiri',array(''=>'Pilih','ya'=>'Ya','tidak'=>'Tidak'),null,array('class'=>'input-large'))}}</td>
											<td></td>
										</tr>
										<tr>
											<td>{{Form::text('bagian_kelumpuhan[]',null,array('class'=>'input-xlarge','placeholder'=>'Lain-lain'))}}</td>
											<td>{{Form::select('kelumpuhan[]',array(''=>'Pilih','ya'=>'Ya','tidak'=>'Tidak'),null,array('class'=>'input-large'))}}</td>
											<td>{{Form::select('gangguan_raba[]',array(''=>'Pilih','ya'=>'Ya','tidak'=>'Tidak'),null,array('class'=>'input-large'))}}</td>
											<td>
												<a href="javascript:void(0)" class="btn btn-xs btn-success add_kelumpuhan" data-uk-tooltip title="Tambah Kelumpuhan" ><i class="icon icon-plus"></i></a>
											</td>
										</tr>
									</table>
								</fieldset>
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<div class="span6">
								<div class="media">
									<fieldset>
										<legend>Riwayat kontak</legend>
										<div class="control-group">
											<label class="control-label">Dalam satu bulan terakhir sebelum sakit, apakah penderita pernah bepergian ?</label>
											<div class="controls">
												{{Form::select('sebelum_sakit_berpergian',array(''=>'Pilih','ya'=>'Ya','tidak'=>'Tidak'),Input::old('sebelum_sakit_berpergian'),array('class'=>'input-small berpergian'))}}
												<table>
													<tr>
														<td>Lokasi</td>
														<td>:</td>
														<td>{{Form::text('lokasi',Input::old('lokasi'),array('class'=>'input-medium lokasi'))}}</td>
													</tr>
													<tr>
														<td>Tanggal pergi</td>
														<td>:</td>
														<td>{{Form::text('tanggal_pergi',Input::old('tanggal_pergi'),array('class'=>'input-small tanggal_pergi','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}</td>
													</tr>
												</table>             
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Dalam satu bulan terakhir sebelum sakit, apakah penderita pernah berkunjung ke rumah anak yang baru mendapat imunisasi polio ?</label>
											<div class="controls">
												{{Form::select('berkunjung',array(''=>'Pilih','ya'=>'Ya','tidak'=>'Tidak'),Input::old('berkunjung'),array('class'=>'input-small'))}}
												<span class="help-inline"></span>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
							<div class="span6">
								<div class="media">
									<fieldset>
										<legend>Status imunisasi polio</legend>
										<table>
											<tr>
												<td>Imunisasi rutin</td>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td>Jumlah dosis</td>
												<td>
													{{Form::select(
														'jumlah_dosis',
														array(
														'0' => 'Pilih',
														'1' => '1X',
														'2'=>'2X',
														'3'=>'3X',
														'4'=>'4X',
														'5'=>'5X',
														'6'=>'6X',
														'7'=>'Tidak [belum pernah]',
														'8'=>'Tidak tahu'
														),
														Input::old('jumlah_dosis'),
														array(
														'class' => 'input-medium id_combobox'
														)
														)
													}}
												</td>
											</tr>
											<tr>
												<td>Sumber informasi</td>
												<td>
													{{Form::select(
														'sumber_informasi',
														array(
														'0' => 'Pilih',
														'1' => 'KMS/Jurim',
														'2'=>'Ingatan responden'
														),
														Input::old('sumber_informasi'),
														array(
														'class' => 'input-medium id_combobox'
														)
														)
													}}
												</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td>PIN, Mop-up, ORI, BIAS Polio</td>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td>Jumlah dosis</td>
												<td>
													{{Form::select(
														'jumlah_dosis_bias_polio',
														array(
														'0' => 'Pilih',
														'1' => '1X',
														'2'=>'2X',
														'3'=>'3X',
														'4'=>'4X',
														'5'=>'5X',
														'6'=>'6X',
														'7'=>'Tidak [belum pernah]',
														'8'=>'Tidak tahu'
														),
														Input::old('jumlah_dosis_bias_polio'),
														array(
														'class' => 'input-medium id_combobox'
														)
														)
													}}
												</td>
											</tr>
											<tr>
												<td>Sumber informasi</td>
												<td>
													{{Form::select(
														'sumber_informasi_bias_polio',
														array(
														'0' => 'Pilih',
														'1' => 'Catatan',
														'2'=>'Ingatan responden'
														),
														Input::old('sumber_informasi_bias_polio'),
														array(
														'class' => 'input-medium id_combobox'
														)
														)
													}}
												</td>
											</tr>
										</table>
										<br/>
										<div class="control-group">
											<label class="control-label">Tanggal imunisasi polio yang paling akhir</label>
											<div class="controls">
												{{Form::text('tanggal_imunisasi_polio',Input::old('tanggal_imunisasi_polio'),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
												<span class="help-inline"></span>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<div class="media">
								<fieldset>
									<legend>Pengumpulan spesimen</legend>
									<div class="control-group">
										<label class="control-label">Apakah spesimen diambil</label>
										<div class="controls">
											{{ Form::select('spesimen_diambil',array(null=>'Pilih','1' => 'Ya','2'=>'Tidak'),null,array('class'=>'input-large spesimen'))}}
										</div>
									</div>
									</br>
									<table class="pengumpulan_spesimen">
										<tr>
											<th style="width: 25%;"></th>
											<th style="width: 25%;">Tanggal Ambil</th>
											<th style="width: 25%;">Tanggal Kirim ke kabupaten/kota</th>
											<th style="width: 25%;">Tanggal Kirim ke provinsi</th>
										</tr>
										<tr>
											<td style="text-align: center;font-weight: bold">Spesimen I</td>
											<td>{{Form::text('tanggal_ambil_spesimen_I',null,array('class'=>'input-medium tgl_spesimen','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','disabled'))}}</td>
											<td>{{Form::text('tanggal_kirim_spesimen_I_kabupaten',null,array('class'=>'input-medium tgl_spesimen','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','disabled'))}}</td>
											<td>{{Form::text('tanggal_kirim_spesimen_I_provinsi',null,array('class'=>'input-medium tgl_spesimen','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','disabled'))}}</td>
										</tr>
										<tr>
											<td style="text-align: center;font-weight: bold">Spesimen II</td>
											<td>{{Form::text('tanggal_ambil_spesimen_II',null,array('class'=>'input-medium tgl_spesimen','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','disabled'))}}</td>
											<td>{{Form::text('tanggal_kirim_spesimen_II_kabupaten',null,array('class'=>'input-medium tgl_spesimen','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','disabled'))}}</td>
											<td>{{Form::text('tanggal_kirim_spesimen_II_provinsi',null,array('class'=>'input-medium tgl_spesimen','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','disabled'))}}</td>
										</tr>
									</table>
									<br>
									<div class="control-group">
										<label class="control-label">Tidak diambil spesimen, berikan alasannya</label>
										<div class="controls">
											{{Form::textarea('catatan_tidak_diambil_spesimen',Input::old('catatan_tidak_diambil_spesimen'),array('rows'=>'4','class'=>'input-xlarge tidak_ambil_spesimen','disabled'))}}
											<span class="help-inline"></span>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<div class="media">
								<fieldset>
									<legend>Petugas pelacak</legend>
									<div class="control-group">
										<label class="control-label">Nama petugas</label>
										<div class="controls">
											{{Form::text('nama_petugas',Input::old('nama_petugas'),array('class'=>'input-large'))}}
											<span class="help-inline"></span>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="12">
							<div class="media">
								<fieldset>
									<legend>Hasil pemeriksaan</legend>
									<div class="control-group">
										<label class="control-label">Diagnosis</label>
										<div class="controls">
											{{Form::text('hasil_diagnosis',Input::old('hasil_diagnosis'),array('class'=>'input-medium'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Nama DSA/DSS/DRM/Dr/pemeriksa lain</label>
										<div class="controls">
											{{Form::text('nama_DSA',Input::old('nama_DSA'),array('class'=>'input-large'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Telepon/Hp</label>
										<div class="controls">
											{{Form::text('telepon_hp',Input::old('telepon_hp'),array('class'=>'input-large'))}}
											<span class="help-inline"></span>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
				</div>
		</div>
	<div class="alert alert-arror" id="id_stop"><center>Stop pelacakan</center></div>
		<div class="form-actions ">
			{{Form::submit('simpan',array('class'=>'btn btn-primary'))}}
			<a href="{{URL::to('afp')}}" class="btn btn-default">Batal</a>
		</div>
	</div>
</div>
{{Form::close()}}
@include('pemeriksaan.campak.analisis-js')
<script type="text/javascript">
	$(function(){
		$('#id_stop').hide();
		$('.add_kelumpuhan').on('click',function(){
			$('.gejala_tanda').append('<tr>'+
				'<td>{{Form::text('bagian_kelumpuhan[]',null,array('class'=>'input-xlarge','placeholder'=>'Lain-lain'))}}</td>'+
				'<td>{{Form::select('kelumpuhan[]',array(''=>'Pilih','ya'=>'Ya','tidak'=>'Tidak'),null,array('class'=>'input-large'))}}</td>'+
				'<td>{{Form::select('gangguan_raba[]',array(''=>'Pilih','ya'=>'Ya','tidak'=>'Tidak'),null,array('class'=>'input-large'))}}</td>'+
				'<td><a href="javascript:void(0)" class="btn btn-xs btn-warning remove" ><i class="icon icon-remove"></i></a></td>'+
				'</tr>');
			$('.remove').on('click',function(){
				$(this).parent().parent().remove();
				return false;
			});
			$('select').select2();
			return false;
		});

		$('.spesimen').on('change',function(){
			var val = $(this).val();
			if(val=='1'){
				$('.tgl_spesimen').removeAttr('disabled');
				$('.tidak_ambil_spesimen').attr('disabled','disabled');
				$('.tidak_ambil_spesimen').val(null);
			}else{
				$('.tidak_ambil_spesimen').removeAttr('disabled');
				$('.tgl_spesimen').attr('disabled','disabled');
				$('.tgl_spesimen').val(null);
			}
			return false;
		});
	});

	function cekPelacakan() 
	{
		var akut = $('#id_akut').val();
		var layuh = $('#id_layuh').val();
		var lumpuh = $('#id_lumpuh').val();
		if(akut=='2' || layuh=='2' || lumpuh=='2')
		{
			$('.stop').hide(2000);
			$('#id_stop').show();
		}
		else 
		{
			$('.stop').show(2000);
			$('#id_stop').hide();
		}
	}

	function cekBerobat()
	{
		var cek = $('#id_unit').val();
		if(cek=='1'){
			$('#unit_1').removeAttr('disabled');
			$('#unit_2').removeAttr('disabled');
			$('#unit_3').removeAttr('disabled');
			$('#unit_4').removeAttr('disabled');
		} else {
			$('#unit_1').attr('disabled','disabled');
			$('#unit_2').attr('disabled','disabled');
			$('#unit_3').attr('disabled','disabled');
			$('#unit_4').attr('disabled','disabled');
		}
	}
</script>
@stop