@extends('layouts.master')
@section('content')
<div class="span12">
	<div class="content">
		<div class="module">
			<div class="module-body">
				<div class="profile-head media">
					<h4>
						Form Edit Pasien Kasus Penyakit AFP
					</h4>
					<hr>
				</div>
				{{Form::open(array('url'=>'afp_update','class'=>'form-horizontal'))}}
				<div class="module-body">
					<div class="row-fluid">
						<div class="span6">
							<div class="media">
							<?php $row = $afp[0];?>
								<fieldset>
									<legend>Identitas pasien</legend>
									<div class="control-group">
										<label class="control-label">Nama penderita</label>
										<div class="controls">
											{{Form::text('nama_anak',$row->pasien_nama_anak, array('class' => 'input-xlarge','placeholder'=>'Nama anak'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">NIK</label>
										<div class="controls">
											{{Form::text('nik',$row->pasien_nik, array('class' => 'input-medium','placeholder'=>'Nomer induk kependudukan'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Nama orang tua</label>
										<div class="controls">
											{{Form::text('nama_ortu',$row->pasien_nama_ortu, array('class' => 'input-xlarge','placeholder'=>'Nama orang tua'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Jenis kelamin</label>
										<div class="controls">
											{{Form::select('jenis_kelamin',[
											'0' => 'Pilih',
											'1'=>'Laki-laki',
											'3'=>'Perempuan',
											'4' => 'Tidak diketahui',
											'5'=>'Lainnya'],[$row->pasien_jenis_kelamin], array('class' => 'input-medium'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Tanggal lahir</label>
										<div class="controls">
											{{Form::text('tanggal_lahir',Helper::getDate($row->pasien_tanggal_lahir), array('class' => 'input-medium tgl_lahir','placeholder'=>'Tanggal lahir','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','onchange'=>'usia()'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<input type="text" class="input-mini"  value="{{$row->pasien_umur}}" name="umur" id="tgl_tahun">
											Thn 
											<input type="text" class="input-mini" value="{{$row->pasien_umur_bln}}" name="umur_bln" id="tgl_bulan">
											Bln 
											<input type="text" class="input-mini" value="{{$row->pasien_umur_hr}}" name="umur_hr" id="tgl_hari">
											Hr
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Nama faskes saat periksa</label>
										<div class="controls">
											{{Form::text('nama_puskesmas',$row->afp_nama_puskesmas, array('class' => 'input-xlarge','placeholder'=>'Nama faskes saat periksa'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
				                        <label class="control-label">Provinsi</label>
				                        <div class="controls">
				                            {{Form::select(
				                                'id_provinsi',
				                                ['0'=>'Pilih provinsi']+Provinsi::lists('provinsi','id_provinsi'),
				                                $row->pasien_id_provinsi,
				                                ['id'       => 'id_provinsi_pasien',
				                                'onchange' => "show_kabupaten('_pasien')",
				                                'class'    => 'input-large id_combobox']);
				                            }}
				                            <span class="help-inline" id="error-id_provinsi_pasien"></span>
				                        </div>
				                    </div>
				                    <!-- awal input kolom kabupaten -->
				                    <div class="control-group">
				                        <label class="control-label">Kabupaten</label>
				                        <div class="controls">
				                            {{Form::select(
				                                'id_kabupaten',
				                                [$row->pasien_id_kabupaten=>$row->pasien_kabupaten],
				                                $row->pasien_id_kabupaten,
				                                ['id'       => 'id_kabupaten_pasien',
				                                'onchange' => "show_kecamatan('_pasien')",
				                                'class'    => 'input-large id_combobox']);
				                            }}
				                            <span class="lodingKab"></span>
				                            <span class="help-inline" id="error-id_kabupaten_pasien"></span>
				                        </div>
				                    </div>
				                    <!-- awal input kolom kecamatan -->
				                    <div class="control-group">
				                        <label class="control-label">Kecamatan</label>
				                        <div class="controls">
				                            {{Form::select(
				                                'id_kecamatan',
				                                [$row->pasien_id_kecamatan=>$row->pasien_kecamatan],
				                                $row->pasien_id_kecamatan,
				                                ['id'       => 'id_kecamatan_pasien',
				                                'onchange' => "show_kelurahan('_pasien')",
				                                'class'    => 'input-large id_combobox']);
				                            }}
				                            <span class="lodingKec"></span>
				                            <span class="help-inline" id="error-id_kecamatan_pasien"></span>
				                        </div>
				                    </div>
				                    <!-- awal input kolom kelurahan -->
				                    <div class="control-group">
				                        <label class="control-label">Kelurahan/Desa</label>
				                        <div class="controls">
				                            {{Form::select(
				                                'id_kelurahan',
				                                [$row->pasien_id_kelurahan=>$row->pasien_kelurahan],
				                                $row->pasien_id_kelurahan,
				                                ['id'       => 'id_kelurahan_pasien',
				                                'class'    => 'input-large id_combobox']);
				                            }}
				                            <span class="lodingKel"></span>
				                            <span class="help-inline" style="color:red">(*)</span>
				                        </div>
				                    </div>
									<div class="control-group">
										<label class="control-label">Alamat</label>
										<div class="controls">
											{{Form::textarea('alamat',$row->pasien_alamat,array('rows'=>'7'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">No Epidemologi</label>
										<div class="controls">
											{{Form::text('no_epid',$row->afp_no_epid, array('class' => 'input-medium','placeholder'=>'Nomer epidemologi','id'=>'epid'))}}
											{{Form::hidden('id_hasil_uji_lab_afp',$row->lab_id_hasil_uji_lab_afp)}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">No Epidemologi lama</label>
										<div class="controls">
											{{Form::text('no_epid_lama',$row->afp_no_epid_lama, array('class' => 'input-medium','placeholder'=>'Nomer epidemologi lama'))}}
											<span class="help-inline"></span>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
						<div class="span6">
							<div class="media">
								<fieldset>
									<legend>Data surveilans AFP</legend>
									<div class="control-group">
										<label class="control-label" >Tanggal mulai lumpuh</label>
										<div class="controls">
											{{Form::text('tanggal_mulai_lumpuh',Helper::getDate($row->afp_tanggal_mulai_lumpuh),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','id'=>'tgl_mulai_sakit','onchange' => 'showEpidAfp(),usia()'))}}
											<span class="help-inline"></span>
										</div> 
									</div>
									<div class="control-group">
										<label class="control-label" >Demam sebelum lumpuh</label>
										<div class="controls">
											{{Form::select('demam_sebelum_lumpuh',[
											'0'=>'Pilih',
											'1' => 'Ya',
											'2'=>'Tidak'],[$row->afp_demam_sebelum_lumpuh], array('class' => 'input-small'))}}
											<span class="help-inline"></span>
										</div>    
									</div>

									<div class="control-group">
										<label class="control-label">Kelumpuhan anggota gerak kanan</label>
										<div class="controls">
											{{ Form::select('kelumpuhan_anggota_gerak_kanan',[
											'0' => 'Pilih',
											'1' => 'lengan (1)',
											'2' => 'tungkai (1)',
											'3' => 'lengan dan tungkai (2)',
											'4' => 'Tidak'], [$row->afp_kelumpuhan_anggota_gerak_kanan], array('class' => 'input-medium id_combobox')) }}
											<span class="help-inline"></span>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" >Kelumpuhan anggota gerak kiri</label>
										<div class="controls">
											{{ Form::select('kelumpuhan_anggota_gerak_kiri', [
											'0' => 'Pilih',
											'1' => 'lengan (1)',
											'2' => 'tungkai (1)',
											'3' => 'lengan dan tungkai (2)',
											'4' => 'Tidak'], [$row->afp_kelumpuhan_anggota_gerak_kiri], array('class' => 'input-medium id_combobox')) }}
											<span class="help-inline"></span>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" >Gangguan raba anggota gerak kanan</label>
										<div class="controls">
											{{ Form::select('gangguan_raba_gerak_kanan', [
											'0' => 'Pilih',
											'1' => 'lengan (1)',
											'2'=>'tungkai (1)',
											'3' => 'lengan dan tungkai (2)',
											'4' => 'Tidak'], [$row->afp_gangguan_raba_anggota_gerak_kanan], array('class' => 'input-medium id_combobox')) }}
											<span class="help-inline"></span>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" >Gangguan raba anggota gerak kiri</label>
										<div class="controls">
											{{ Form::select('gangguan_raba_gerak_kiri',[
											'0' => 'Pilih',
											'1' => 'lengan (1)',
											'2' => 'tungkai (1)',
											'3' => 'lengan dan tungkai (2)',
											'4' => 'Tidak'], [$row->afp_gangguan_raba_anggota_gerak_kiri], array('class' => 'input-medium id_combobox')) }}
											<span class="help-inline"></span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label" >Imunisasi rutin polio sebelum sakit</label>
										<div class="controls">
											<table>
												<tbody>
													<tr>
														<td>
														{{ Form::select('imunisasi_polio_sebelum_sakit', array(
															null => 'Jumlah dosis',
															'1' => '1X',
															'2' => '2X',
															'3' => '3X',
															'4' => '4X',
															'5' => '5X',
															'6' => '6X',
															'7' => 'Tidak',
															'8' => 'Tidak tahu',
															'9' => 'Belum pernah'
															), $row->afp_imunisasi_polio_sebelum_sakit, array(
															'data-validation' => '[MIXED]',
															'data' => '$ wajib di isi!', 
															'data-validation-message' => 'wajib di isi!',
															'class' => 'input-medium id_combobox')) }}
														</td>
														<td>
															{{ Form::select('sumber_informasi_imunisasi', array(
															null => 'Sumber Informasi',
															'1' => 'KMS/Catatan Jurim',
															'2' => 'Ingatan responden',
															), $row->afp_sumber_informasi_imunisasi, array(
															'class' => 'input-medium id_combobox')) }}
														</td>
													</tr>
												</tbody>
											</table>
											<span class="help-inline" id="error-imunisasi_polio_sebelum_sakit" style="color:red">(*)</span>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" >PIN, Mop-up, ORI, BIAS Polio</label>
										<div class="controls">
											<table>
												<tbody>
													<tr>
														<td>
														{{ Form::select('imunisasi_polio_lain', array(
															null => 'Jumlah dosis',
															'1' => '1X',
															'2' => '2X',
															'3' => '3X',
															'4' => '4X',
															'5' => '5X',
															'6' => '6X',
															'7' => 'Tidak',
															'8' => 'Tidak tahu',
															'9' => 'Belum pernah'
															), $row->afp_imunisasi_polio_lain, array(
															'data-validation' => '[MIXED]',
															'data' => '$ wajib di isi!', 
															'data-validation-message' => 'wajib di isi!',
															'class' => 'input-medium id_combobox')) }}
														</td>
														<td>
															{{ Form::select('sumber_informasi_imunisasi_polio_lain', array(
															null => 'Sumber Informasi',
															'1' => 'Catatan',
															'2' => 'Ingatan responden',
															), $row->afp_sumber_informasi_imunisasi_polio_lain, array(
															'class' => 'input-medium id_combobox')) }}
														</td>
													</tr>
												</tbody>
											</table>
											<span class="help-inline" id="error-imunisasi_polio_sebelum_sakit" style="color:red">(*)</span>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" >Tanggal vaksinasi polio terakhir</label>
										<div class="controls">
											{{Form::text('tanggal_vaksinasi_polio',Helper::getDate($row->afp_tanggal_vaksinasi_polio),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Tanggal laporan diterima</label>
										<div class="controls">
											{{Form::text('tanggal_laporan_diterima',Helper::getDate($row->afp_tanggal_laporan_diterima),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Tanggal pelacakan</label>
										<div class="controls">
											{{Form::text('tanggal_pelacakan',Helper::getDate($row->afp_tanggal_pelacakan),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" >Kontak dengan penyakit lain</label>
										<div class="controls">
											{{Form::select('kontak',[''=>'Pilih','Y' => 'Ya','T'=>'Tidak'],[$row->afp_kontak], array('class' => 'input-small'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" >Keadaan akhir</label>
										<div class="controls">
											{{Form::select('keadaan_akhir',[
											'0'=>'Pilih',
											'1' => 'Hidup',
											'2'=>'Mati'],[$row->afp_keadaan_akhir], array('class' => 'input-small'))}}
											<span class="help-inline"></span>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
						<div class="span11">
							<div class="media">
								<fieldset>
									<legend>Data spesimen dan laboratorium</legend>
									<div class="control-group">
										<label class="control-label" >Tanggal pengambilan spesimen I</label>
										<div class="controls">
											{{Form::text('tanggal_pengambilan_spesimen1',Helper::getDate($row->afp_tanggal_pengambilan_spesimen1),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" >Tanggal pengambilan spesimen II</label>
										<div class="controls">
											{{Form::text('tanggal_pengambilan_spesimen2',Helper::getDate($row->afp_tanggal_pengambilan_spesimen2),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
					                  <label class="control-label" >Jenis pemeriksaan laboratorium spesimen I
					                  	{{ Form::checkbox('jp1',1,$row->lab_jp1, array('onclick'=>'pilih_periksa1(this)')) }}	
					                  </label>
					                  <div class="controls">
					                    <table>
					                      <tr>
					                        <td>
					                        	<?php $civjp1 = (($row->lab_isolasi_virus_jp1==1)?null:'disabled');?>
							                  	{{ Form::checkbox('isolasi_virus_jp1',1,$row->lab_isolasi_virus_jp1, array('id'=>'isolasi_virus1',$civjp1=>$civjp1, 'onchange'=>'change_virus1(this)')) }}
					                          Isolasi virus
					                        </td>
					                        <td>
					                          {{ Form::text('ket_isolasi_virus_jp1', $row->lab_ket_isolasi_virus_jp1, array('class' => 'input-medium cek_periksa_virus1' ,$civjp1=>$civjp1)) }}
					                        </td>
					                      </tr>
					                      <tr>
					                        <td>
					                        	<?php $cijp1 = (($row->lab_itd_jp1==1)?null:'disabled');?>
							                  	{{ Form::checkbox('itd_jp1',1,$row->lab_itd_jp1, array('id'=>'ITD1',$cijp1=>$cijp1, 'onchange'=>'change_itd1(this)')) }}
					                          ITD
					                        </td>
					                        <td>
					                          {{ Form::text('ket_itd_jp1', $row->lab_ket_itd_jp1, array('class' => 'input-medium cek_periksa_itd1', $cijp1=>$cijp1, 'placeholder' => '')) }}
					                        </td>
					                      </tr>
					                      <tr>
					                        <td>
					                        	<?php $csjp1 = (($row->lab_sequencing_jp1==1)?null:'disabled');?>
							                  	{{ Form::checkbox('sequencing_jp1',1,$row->lab_sequencing_jp1, array('id'=>'Sequencing1',$csjp1=>$csjp1, 'onchange'=>'change_sq1(this)')) }}
					                          Sequencing
					                        </td>
					                        <td>
					                          {{ Form::text('ket_sequencing_jp1', $row->lab_ket_sequencing_jp1, array('placeholder' => '',$csjp1=>$csjp1, 'class' => 'cek_periksa_sq1')) }}
					                        </td>
					                      </tr>
					                    </table>
					                  </div>
					                </div>

					                <div class="control-group">
					                  <label class="control-label" >Jenis pemeriksaan laboratorium spesimen II
					                  	{{ Form::checkbox('jp2',1,$row->lab_jp2, array('onclick'=>'pilih_periksa2(this)')) }}	
					                  </label>
					                  <div class="controls">
					                    <table>
					                      <tr>
					                        <td>
					                        	<?php $civjp2 = (($row->lab_isolasi_virus_jp2==1)?null:'disabled');?>
							                  	{{ Form::checkbox('isolasi_virus_jp2',1,$row->lab_isolasi_virus_jp2, array('id'=>'isolasi_virus2',$civjp2=>$civjp2, 'onchange'=>'change_virus2(this)')) }}
					                          Isolasi virus
					                        </td>
					                        <td>
					                          {{ Form::text('ket_isolasi_virus_jp2', $row->lab_ket_isolasi_virus_jp2, array('class' => 'input-medium cek_periksa_virus2',$civjp2=>$civjp2, 'placeholder' => '')) }}
					                        </td>
					                      </tr>
					                      <tr>
					                        <td>
					                        	<?php $cijp2 = (($row->lab_itd_jp2==1)?null:'disabled');?>
							                  	{{ Form::checkbox('itd_jp2',1,$row->lab_itd_jp2, array('id'=>'ITD2',$cijp2=>$cijp2, 'onchange'=>'change_itd2(this)')) }}
					                          ITD
					                        </td>
					                        <td>
					                          {{ Form::text('ket_itd_jp2', $row->lab_ket_itd_jp2, array('class' => 'input-medium cek_periksa_itd2',$cijp2=>$cijp2, 'placeholder' => '')) }}
					                        </td>
					                      </tr>
					                      <tr>
					                        <td>
					                        	<?php $csjp2 = (($row->lab_sequencing_jp2==1)?null:'disabled');?>
					                        {{ Form::checkbox('sequencing_jp2',1,$row->lab_sequencing_jp2, array('id'=>'Sequencing2',$csjp2=>$csjp2, 'onchange'=>'change_sq2(this)')) }}
					                          Sequencing
					                        </td>
					                        <td>
					                          {{ Form::text('ket_sequencing_jp2', $row->lab_ket_sequencing_jp2, array('placeholder' => '',$csjp2=>$csjp2, 'class' => 'input-medium cek_periksa_sq2')) }}
					                        </td>
					                      </tr>
					                    </table>
					                  </div>
					                </div>
									<div class="control-group">
							<label class="control-label">Klasifikasi final</label>
							<div class="controls">
								{{Form::select('klasifikasi_final',[
								'0'=>'Pilih',
								'1' => 'Polio',
								'2'=>'Bukan polio',
								'3'=>'compatible'],[$row->afp_klasifikasi_final], array('class' => 'input-medium id_combobox'))}}
								<span class="help-inline"></span>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
		<script>
			$(document).ready(function() 
			{
				$("select").select2();
				showKabupaten();
				showKecamatan();
				showKelurahan();
			});
			function showKabupaten(){
				var provinsi = $("#id_provinsi").val();
				var kabupaten = $("#id_kabupaten").val();
					// //kirim data ke server
					$('.lodingKab').html('<img src="{{URL::to("style/images/loding.gif")}}">');
					$.post('{{URL::to("provinsi/getkab")}}', {provinsi:provinsi,kabupaten:kabupaten}, function(response)
					{
						$('#id_kabupaten').removeAttr('disabled','');
						$("#id_kabupaten").html(response);
						$('.lodingKab').html('');
					});

				}

				function showKecamatan(){
					var kabupaten = $("#id_kabupaten").val();
					var kecamatan = $("#id_kecamatan").val();
					// //kirim data ke server
					$('.lodingKec').html('<img src="{{URL::to("style/images/loding.gif")}}">');
					$.post('{{URL::to("provinsi/getkec")}}', {kabupaten:kabupaten,kecamatan:kecamatan}, function(response)
					{
						$('#id_kecamatan').removeAttr('disabled','');
						$("#id_kecamatan").html(response);
						$('.lodingKec').html('');
					});

				}

				function showKelurahan(){
					var kecamatan= $("#id_kecamatan").val();
					var kelurahan = $("#id_kelurahan").val();
					// //kirim data ke server
					$('.lodingKel').html('<img src="{{URL::to("style/images/loding.gif")}}">');
					$.post('{{URL::to("provinsi/getkel")}}', {kecamatan:kecamatan,kelurahan:kelurahan}, function(response)
					{ 
						$('#id_kelurahan').removeAttr('disabled','');
						$("#id_kelurahan").html(response);
						$('.lodingKel').html('');
					});

				}

			</script>
		</div>
		<div class="form-actions">
			<button type="submit" class="btn btn-success">Simpan</button>
			<a href="{{URL::to('afp')}}" class="btn btn-warning">Kembali</a>
		</div>
		{{Form::close()}}
	</div>
</div>
</div>
</div>
@include('pemeriksaan.afp.afp-js')
@stop