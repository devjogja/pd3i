<script type="text/javascript">

function pilih_periksa1(e) {
	
	if (e.checked) 
  {
		$('#isolasi_virus1').removeAttr('disabled','');
		$('#ITD1').removeAttr('disabled','');
		$('#Sequencing1').removeAttr('disabled','');
	} 
  else
  {
    $('#isolasi_virus1').attr('checked', false);
    $('#ITD1').attr('checked', false);
    $('#Sequencing1').attr('checked', false);
	$('#isolasi_virus1').attr('disabled','disabled');
	$('#ITD1').attr('disabled','disabled');
	$('#Sequencing1').attr('disabled','disabled');
    $('.cek_periksa_virus1').attr('disabled','disabled');
    $('.cek_periksa_itd1').attr('disabled','disabled');
    $('.cek_periksa_sq1').attr('disabled','disabled');		
	};
	
}

function showEpidAfp() {
	var id_kelurahan = $('#id_kelurahan_pasien').val() != 'undefined' ? $('#id_kelurahan_pasien').val() : '';
	var tgl_mulai_sakit = $('#tgl_mulai_sakit_pasien').val() != 'undefined' ? $('#tgl_mulai_sakit').val() : '';
  $.ajax({
    data:'id_kelurahan='+id_kelurahan+'&date='+tgl_mulai_sakit,
    url:'{{URL::to("ambil_epid_afp")}}',
    success:function(data) {

       $('#epid').val(data);
   }
  });

}

function pilih_periksa2(e) {
	
	if (e.checked) {
		$('#isolasi_virus2').removeAttr('disabled','');
		$('#ITD2').removeAttr('disabled','');
		$('#Sequencing2').removeAttr('disabled','');
	} 
  else
  {
    $('#isolasi_virus2').attr('checked', false);
    $('#ITD2').attr('checked', false);
    $('#Sequencing2').attr('checked', false);
		$('#isolasi_virus2').attr('disabled','disabled');
		$('#ITD2').attr('disabled','disabled');
		$('#Sequencing2').attr('disabled','disabled');
    $('.cek_periksa_virus2').attr('disabled','disabled');
    $('.cek_periksa_itd2').attr('disabled','disabled');
    $('.cek_periksa_sq2').attr('disabled','disabled');     
	};
		
}

function change_virus2(e) {
	if (e.checked) {
		$('.cek_periksa_virus2').removeAttr('disabled','');
	} else{
		$('.cek_periksa_virus2').attr('disabled','disabled');
	};
}
function change_itd2(e) {
	if (e.checked) {
		$('.cek_periksa_itd2').removeAttr('disabled','');
	} else{
		$('.cek_periksa_itd2').attr('disabled','disabled');
	};
}
function change_sq2(e) {
	if (e.checked) {
		$('.cek_periksa_sq2').removeAttr('disabled','');
	} else{
		$('.cek_periksa_sq2').attr('disabled','disabled');
	};
}

function change_virus1(e) {
	if (e.checked) {
		$('.cek_periksa_virus1').removeAttr('disabled','');
	} else{
		$('.cek_periksa_virus1').attr('disabled','disabled');
	};
}
function change_itd1(e) {
	if (e.checked) {
		$('.cek_periksa_itd1').removeAttr('disabled','');
	} else{
		$('.cek_periksa_itd1').attr('disabled','disabled');
	};
}
function change_sq1(e) {
	if (e.checked) {
		$('.cek_periksa_sq1').removeAttr('disabled','');
	} else{
		$('.cek_periksa_sq1').attr('disabled','disabled');
	};
}

</script>