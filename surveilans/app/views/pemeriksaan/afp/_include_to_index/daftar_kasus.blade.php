<!-- <fieldset style="padding-bottom:0px;">
	<p>
	DARI {{Form::text('tgl_mulai',Input::old('tgl_mulai'),array('class'=>'input-medium tgl_mulai','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
	SAMPAI {{Form::text('tgl_sampai',Input::old('tgl_sampai'),array('class'=>'input-medium tgl_sampai','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
	<a onclick="tampil_campak()" class="btn btn-warning" style="margin-bottom:11px;">Tampilkan</a>
	<a onclick="cetak_campak()" class="btn btn-info" style="margin-bottom:11px;">cetak</a>
	</p>
</fieldset> -->
<div class="module-body uk-overflow-container">
	<div class="table-responsive">
		<table class="table table-bordered display" id="example_afp">
		  <thead>
				<tr>
				  <th>No. Epid</th>
				  <th>Nama pasien</th>
				  <th>Usia</th>
				  <th>Alamat</th>
				  <th>Keadaan Akhir</th>
				  <th>Klasifikasi Final</th>
				  @if(Sentry::getUser()->hak_akses==2 || Sentry::getUser()->hak_akses==7)
                        <th>PE</th>
                      @else
                        <th>Nama Faskes Penginput Data</th>
                      @endif
				  <th style="text-align:center">Aksi</th>
				</tr>
		  </thead>
		  <tbody>
			@foreach($data as $row)
				<tr>
					<td>{{ $row->no_epid }}</td>
					<td>{{ $row->nama_anak }}</td>
					@if($row->umur || $row->umur_bln || $row->umur_hr)
                      <td>{{ $row->umur.' Th '.$row->umur_bln.' bln '.$row->umur_hr.' hr' }}</td>
                    @else
                      <td>-</td>
                    @endif
                    <td>{{ $row->alamat }}</td>
					<td>{{ $row->keadaan_akhir_nm }}</td>
					<td>{{ $row->klasifikasi_final_nm }}</td>
					@if(Sentry::getUser()->hak_akses==2 || Sentry::getUser()->hak_akses==7)
						<td>
						<?php
                            if(Afp::checkPE($row->no_epid, $row->id_pe_afp))
                            {
                                echo '<a class="btn btn-danger">Sudah PE</a>';
                            }
                            else
                            {
                          ?>
                          <a href="{{URL::to('afp/entriafp/'.$row->id_afp)}}" data-uk-tooltip title="Penyelidikan epidemologi" class="btn btn-primary">PE</a>
							<?php
                            }
                          ?>
						</td>
					@else
                      <td>{{$row->nama_faskes}}</td>
                    @endif
					@if(Sentry::getUser()->hak_akses==2 || Sentry::getUser()->hak_akses==3 || Sentry::getUser()->hak_akses==7)
						<td>
						  <div class="btn-group" role="group" >
						  <a href="{{URL::to('afp_detail/'.$row->id_afp)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
						  <a href="{{URL::to('afp_edit/'.$row->id_afp)}}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>
						  <a href="{{URL::to('afp_hapus/'.$row->id_afp)}}" onclick="return confirm('apakah yakin ingin menghapus data ini?');return false;" class="btn btn-xs btn-danger" data-uk-tooltip title="hapus data"><i class="icon icon-remove"></i></a>
						  </div>
						</td>
					@elseif(Sentry::getUser()->hak_akses==4)
						<td>
						  <div class="btn-group" role="group" >
						  <a href="{{URL::to('afp_detail/'.$row->id_afp)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
						  <a href="{{URL::to('afp_edit/'.$row->id_afp)}}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>
						  </div>
						</td>
					@elseif(Sentry::getUser()->hak_akses==1 || Sentry::getUser()->hak_akses==6)
						<td>
						  <div class="btn-group" role="group" >
						  <a href="{{URL::to('afp_detail/'.$row->id_afp)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
						  <a href="{{URL::to('afp_hapus/'.$row->id_afp)}}" onclick="return confirm('apakah yakin ingin menghapus data ini?');return false;" class="btn btn-xs btn-danger" data-uk-tooltip title="hapus data"><i class="icon icon-remove"></i></a>
						  </div>
						</td>
					@elseif(Sentry::getUser()->hak_akses==5)
					<td></td>
					@endif
				</tr>
			@endforeach
		  </tbody>
		</table>
	</div>
</div>