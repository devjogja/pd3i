<div class="alert alert-error">
	<p style="font-size:12px">Text input yang bertanda bintang <strong>(*)</strong> wajib di isi
	</div>
	{{ Form::open(array(
	'url' =>'afp_store',
	'class' =>'form-horizontal',
	'id'    =>'form_save_afp'
	)) }}
	<div class="module-body">
		<div class="row-fluid">
			<div class="span6">
				<div class="media">
					<fieldset>
						<legend>Identitas pasien</legend>
						<div class="control-group">
							<label class="control-label">Nama penderita</label>
							<div class="controls">
								{{ 
								Form::text
								(
								'nama_anak', 
								Input::old('nama_anak'), 
								array
								(
								'data-validation' => '[MIXED]',
								'data' => '$ wajib di isi!', 
								'data-validation-message' => 'wajib di isi!',
								'class' => 'input-xlarge',
								'placeholder'=>'Nama penderita'
								)
								) 
							}}
							<span class="help-inline" id="error-nama_anak" style="color:red">(*)</span>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">NIK</label>
						<div class="controls">
							{{ Form::text('nik', Input::old('nik'), array('class' => 'input-medium', 'placeholder' => 'Nomer induk kependudukan')) }}
							<span class="help-inline"></span>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">Nama orang tua</label>
						<div class="controls">
							{{ Form::text('nama_ortu', Input::old('nama_ortu'), array(
							'data-validation' => '[MIXED]',
							'data' => '$ wajib di isi!', 
							'data-validation-message' => 'wajib di isi!',
							'class' => 'input-xlarge',
							'placeholder' => 'Nama orang tua')) }}
							<span class="help-inline" id="error-nama_ortu" style="color:red">(*)</span>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">Jenis kelamin</label>
						<div class="controls">
							{{Form::select
							(
							'jenis_kelamin', 
							array
							(
							'0' => 'Pilih',
							'1'=>'Laki-laki',
							'2'=>'Perempuan',
							'3' => 'Tidak jelas'
							),
							null,
							array
							(
							'data-validation'=>'[MIXED]',
							'data'=>'$ wajib di isi!',
							'data-validation-message'=>'jenis kelamin wajib di isi!',
							'class' => 'input-medium id_combobox'
							)
							)
						}}
						<span class="help-inline" style="color:red">(*)</span>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label"><input type="radio" name="option" onclick="pilihOptionTglLahir()">Tanggal lahir</label>
					<div class="controls">
						{{Form::text('tanggal_lahir', Input::old('tanggal_lahir'), array(
						'class' => 'input-medium tgl_lahir', 
						'placeholder' => 'Tanggal lahir', 
						'data-uk-datepicker' => '{format:"DD-MM-YYYY"}', 
						'onchange' => 'usia()',
						'readonly' => 'readonly'
						))
					}}
					<span class="help-inline"></span>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label"><input type="radio" name="option" onclick="pilihOptionUmur()">Umur</label>
				<div class="controls">
					<input type="text" class="input-mini"  name="tgl_tahun" id="tgl_tahun" onchange="tgl_lahir()" readonly="readonly">Thn
					<input type="text" class="input-mini" name="tgl_bulan" id="tgl_bulan" onchange="tgl_lahir()" readonly="readonly">Bln
					<input type="text" class="input-mini" name="tgl_hari" id="tgl_hari" onchange="tgl_lahir()" readonly="readonly">Hari
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Nama faskes saat periksa</label>
				<div class="controls">
					<?php
					$type = Session::get('type');
					$kd_faskes = Session::get('kd_faskes');
					$namainstansi = '';
					if($type == "puskesmas")
					{
						$nama_instansi = 'PUSKESMAS '.DB::table('puskesmas')
						->select('puskesmas_name','puskesmas_code_faskes')
						->WHERE('puskesmas_code_faskes',$kd_faskes)->pluck('puskesmas_name');
						echo Form::text('nama_puskesmas',$nama_instansi, array('class' => 'input-large','placeholder'=>'Masukan nama puskesmas'));
					}
					elseif ($type == "rs")
					{
						$nama_instansi = DB::table('rumahsakit2')
						->select('nama_faskes')
						->WHERE('kode_faskes',$kd_faskes)->pluck('nama_faskes');
						echo Form::text('nama_puskesmas',$nama_instansi, array('class' => 'input-large','placeholder'=>'Masukan nama rumahsakit'));
					}
					?>
					<span class="help-inline"></span>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Provinsi</label>
				<div class="controls">
					{{ Form::select(
					'id_provinsi', 
					array(
					''=>'Pilih provinsi')+Provinsi::lists('provinsi','id_provinsi'),
					null,
					array(
					'id'=>'id_provinsi',
					'placeholder' => 'Pilih Provinsi',
					'onchange'=>'showKabupaten()',
					'class'=>'input-large id_combobox'
					)
					)
				}}
				<span class="help-inline" id="error-id_propinsi"></span>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Kabupaten</label>
			<div class="controls">
				{{ Form::select('id_kabupaten',
				array('' => 'Pilih Kabupaten'),
				null,
				array('class' => 'input-medium id_combobox',
				'id' => 'id_kabupaten',
				'placeholder' => 'Pilih Kabupaten',
				'onchange' => 'showKecamatan()')) }}
				<span class="lodingKab"></span>
				<span class="help-inline" id="error-id_kabupaten"></span>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Kecamatan</label>
			<div class="controls">
				{{ Form::select('id_kecamatan', array('' => 'Pilih Kecamatan'), null, array('class' => 'input-medium id_combobox','id' => 'id_kecamatan', 'onchange' => 'showKelurahan()')) }}
				<span class="lodingKec"></span>
				<span class="help-inline" id="error-id_kecamatan"></span>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Kelurahan/Desa</label>
			<div class="controls">
				{{ Form::select('id_kelurahan', array('' => 'Pilih Kelurahan'), null, array(
				'data-validation' => '[MIXED]',
				'data' => '$ wajib di isi!', 
				'data-validation-message' => 'wajib di isi!',
				'class' => 'input-medium id_combobox', 
				'id' => 'id_kelurahan', 
				'onchange' => 'showEpidAfp()')) }}
				<span class="lodingKel"></span>
				<span class="help-inline" id="error-id_kelurahan" style="color:red">*)</span>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Alamat</label>
			<div class="controls">
				{{ Form::textarea('alamat', null, array('rows' => '7', 'placeholder' => 'Hanya diisi nama jalan, no. rumah dan no. RT - RW')) }}
				<span class="help-inline"></span>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">No Epidemologi</label>
			<div class="controls">
			 {{ 
			 Form::text
			 (
			 'no_epid', 
			 Input::old('no_epid'), 
			 array
			 ( 
			 'class' => 'input-medium', 
			 'placeholder' => 'Nomer epidemologi', 
			 'id' => 'epid'
			 )
			 ) 
		 }}
		 <span class="help-inline" id="error-no_epid"></span>
	 </div>
 </div>

 <div class="control-group">
	<label class="control-label">No Epidemologi lama</label>
	<div class="controls">
	 {{ Form::text('no_epid_lama', Input::old('no_epid_lama'), array('class' => 'input-medium', 'placeholder' => 'Nomer epidemologi lama')) }}
	 <span class="help-inline" style="color:red"></span>
 </div>
</div>
</fieldset>
</div> <!-- /.media -->
</div> <!-- /.span6 -->

<div class="span6">
	<div class="media">
		<fieldset>
			<legend>Data surveilans AFP</legend>
			<div class="control-group">
				<label class="control-label">Tanggal mulai lumpuh (bukan karena ruda paksa)</label>
				<div class="controls">
					{{ Form::text('tanggal_mulai_lumpuh', Input::old('tanggal_mulai_lumpuh'), array(
					'data-validation' => '[MIXED]',
					'data' => '$ wajib di isi!', 
					'data-validation-message' => 'wajib di isi!',
					'class' => 'input-medium tgl_mulai_sakit',  
					'id' => 'tgl_mulai_sakit',
					'data-uk-datepicker' => '{format:"DD-MM-YYYY"}', 
					'onchange' => 'showEpidAfp(),usia()')) }}
					<span class="help-inline" id="error-tanggal_mulai_lumpuh" style="color:red">(*)</span>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Demam sebelum lumpuh (bukan karena ruda paksa)</label>
				<div class="controls">
					{{ Form::select('demam_sebelum_lumpuh', array(
					'0' => 'Pilih',
					'1' => 'Ya',
					'2' => 'Tidak'), Input::old('demam_sebelum_lumpuh'), array(
					'class' => 'input-small id_combobox')) }}
					<span class="help-inline" id="error-demam_sebelum_lumpuh"></span>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Kelumpuhan anggota gerak kanan</label>
				<div class="controls">
					{{ Form::select('kelumpuhan_anggota_gerak_kanan', array(
					'0' => 'Pilih',
					'1' => 'lengan (1)',
					'2' => 'tungkai (1)',
					'3' => 'lengan dan tungkai (2)',
					'4' => 'Tidak'), Input::old('kelumpuhan_anggota_gerak_kanan'), array('class' => 'input-medium id_combobox')) }}
					<span class="help-inline"></span>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" >Kelumpuhan anggota gerak kiri</label>
				<div class="controls">
					{{ Form::select('kelumpuhan_anggota_gerak_kiri', array(
					'0' => 'Pilih',
					'1' => 'lengan (1)',
					'2' => 'tungkai (1)',
					'3' => 'lengan dan tungkai (2)',
					'4' => 'Tidak'), Input::old('kelumpuhan_anggota_gerak_kiri'), array('class' => 'input-medium id_combobox')) }}
					<span class="help-inline"></span>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" >Gangguan raba anggota gerak kanan</label>
				<div class="controls">
					{{ Form::select('gangguan_raba_gerak_kanan', array(
					'0' => 'Pilih',
					'1' => 'lengan (1)',
					'2'=>'tungkai (1)',
					'3' => 'lengan dan tungkai (2)',
					'4' => 'Tidak'), Input::old('gangguan_raba_gerak_kanan'), array('class' => 'input-medium id_combobox')) }}
					<span class="help-inline"></span>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" >Gangguan raba anggota gerak kiri</label>
				<div class="controls">
					{{ Form::select('gangguan_raba_gerak_kiri', array(
					'0' => 'Pilih',
					'1' => 'lengan (1)',
					'2' => 'tungkai (1)',
					'3' => 'lengan dan tungkai (2)',
					'4' => 'Tidak'), Input::old('gangguan_raba_gerak_kiri'), array('class' => 'input-medium id_combobox')) }}
					<span class="help-inline"></span>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" >Imunisasi rutin polio sebelum sakit</label>
				<div class="controls">
					<table>
						<tbody>
							<tr>
								<td>
								{{ Form::select('imunisasi_polio_sebelum_sakit', array(
									null => 'Jumlah dosis',
									'1' => '1X',
									'2' => '2X',
									'3' => '3X',
									'4' => '4X',
									'5' => '5X',
									'6' => '6X',
									'7' => 'Tidak',
									'8' => 'Tidak tahu',
									'9' => 'Belum pernah'
									), Input::old('imunisasi_polio_sebelum_sakit'), array(
									'data-validation' => '[MIXED]',
									'data' => '$ wajib di isi!', 
									'data-validation-message' => 'wajib di isi!',
									'class' => 'input-medium id_combobox')) }}
								</td>
								<td>
									{{ Form::select('sumber_informasi_imunisasi', array(
									null => 'Sumber Informasi',
									'1' => 'KMS/Catatan Jurim',
									'2' => 'Ingatan responden',
									), Input::old('sumber_informasi_imunisasi'), array(
									'class' => 'input-medium id_combobox')) }}
								</td>
							</tr>
						</tbody>
					</table>
					<span class="help-inline" id="error-imunisasi_polio_sebelum_sakit" style="color:red">(*)</span>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" >PIN, Mop-up, ORI, BIAS Polio</label>
				<div class="controls">
					<table>
						<tbody>
							<tr>
								<td>
								{{ Form::select('imunisasi_polio_lain', array(
									null => 'Jumlah dosis',
									'1' => '1X',
									'2' => '2X',
									'3' => '3X',
									'4' => '4X',
									'5' => '5X',
									'6' => '6X',
									'7' => 'Tidak',
									'8' => 'Tidak tahu',
									'9' => 'Belum pernah'
									), Input::old('imunisasi_polio_lain'), array(
									'class' => 'input-medium id_combobox')) }}
								</td>
								<td>
									{{ Form::select('sumber_informasi_imunisasi_polio_lain', array(
									null => 'Sumber Informasi',
									'1' => 'Catatan',
									'2' => 'Ingatan responden',
									), Input::old('sumber_informasi_imunisasi_polio_lain'), array(
									'class' => 'input-medium id_combobox')) }}
								</td>
							</tr>
						</tbody>
					</table>
					<span class="help-inline" id="error-imunisasi_polio_sebelum_sakit" style="color:red">(*)</span>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" >Tanggal imunisasi polio terakhir</label>
				<div class="controls">
					{{ Form::text('tanggal_vaksinasi_polio', Input::old('tanggal_vaksinasi_polio'), array(
					'class' => 'input-medium', 
					'data-uk-datepicker' => '{format:"DD-MM-YYYY"}')) }}
					<span class="help-inline"></span>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Tanggal laporan diterima</label>
				<div class="controls">
					{{ Form::text('tanggal_laporan_diterima', Input::old('tanggal_laporan_diterima'), array(
					'class' => 'input-medium', 
					'data-uk-datepicker' => '{format:"DD-MM-YYYY"}')) }}
					<span class="help-inline" ></span>
				</div>

			</div>

			<div class="control-group">
				<label class="control-label">Tanggal pelacakan</label>
				<div class="controls">
					{{ Form::text('tanggal_pelacakan', Input::old('tanggal_pelacakan'), array(
					'class' => 'input-medium', 
					'data-uk-datepicker' => '{format:"DD-MM-YYYY"}')) }}
					<span class="help-inline" id="error-tanggal_pelacakan"></span>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" >Kontak</label>
				<div class="controls">
					{{ Form::select('kontak', array(
					'0' => 'Pilih',
					'1' => 'Ya',
					'2' => 'Tidak'), Input::old('kontak'), array(
					'class' => 'input-small id_combobox')) }}
					<span class="help-inline" id="error-kontak" style="color:red"></span>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" >Keadaan akhir</label>
				<div class="controls">
					{{ Form::select('keadaan_akhir', array(
					'0' => 'Pilih',
					'1' => 'Hidup',
					'2' => 'Meninggal'), Input::old('keadaan_akhir'), array(
					'class' => 'input-medium id_combobox')) }}
					<span class="help-inline" id="error-keadaan_akhir"></span>
				</div>
			</div>

		</fieldset>
	</div> <!-- /.media -->
</div><!-- /.span6 -->

<div class="span12">
	<div class="media">
		<fieldset>
			<legend>Data spesimen dan laboratorium</legend>

			<div class="control-group">
				<label class="control-label" >Tanggal pengambilan spesimen I</label>
				<div class="controls">
					{{ Form::text('tanggal_pengambilan_spesimen1', Input::old('tanggal_pengambilan_spesimen1'), array(
					'class' => 'input-medium', 
					'data-uk-datepicker' => '{format:"DD-MM-YYYY"}')) }}
					<span class="help-inline"></span>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" >Tanggal pengambilan spesimen II</label>
				<div class="controls">
					{{ Form::text('tanggal_pengambilan_spesimen2', Input::old('tanggal_pengambilan_spesimen2'), array(
					'class' => 'input-medium', 
					'data-uk-datepicker' => '{format:"DD-MM-YYYY"}')) }}
					<span class="help-inline"></span>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" >Jenis pemeriksaan laboratorium spesimen I
					<input type="checkbox" name="jp1" value="1" onclick="pilih_periksa1(this)">
				</label>
				<div class="controls">
					<table>
						<tr>
							<td>
								<input type="checkbox" id="isolasi_virus1" value="1" name="isolasi_virus_jp1" disabled onchange="change_virus1(this)">
								Isolasi virus
							</td>
							<td>
								{{ Form::text('ket_isolasi_virus_jp1', null, array('class' => 'input-medium cek_periksa_virus1', 'placeholder' => '','disabled' => 'disabled')) }}
							</td>
						</tr>
						<tr>
							<td>
								<input type="checkbox" id="ITD1" value="1" name="itd_jp1" disabled onchange="change_itd1(this)">
								ITD
							</td>
							<td>
								{{ Form::text('ket_itd_jp1', null, array('class' => 'input-medium cek_periksa_itd1', 'placeholder' => '', 'disabled' => 'disabled')) }}
							</td>
						</tr>
						<tr>
							<td>
								<input type="checkbox" id="Sequencing1" value="1" name="sequencing_jp1" disabled onchange="change_sq1(this)">
								Sequencing
							</td>
							<td>
								{{ Form::text('ket_sequencing_jp1', null, array('placeholder' => '','disabled' => 'disabled', 'class' => 'cek_periksa_sq1')) }}
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" >Jenis pemeriksaan laboratorium spesimen II
					<input type="checkbox" name="jp2" value="1" onclick="pilih_periksa2(this)">
				</label>
				<div class="controls">
					<table>
						<tr>
							<td>
								<input type="checkbox" id="isolasi_virus2" value="1" name="isolasi_virus_jp2" disabled onchange="change_virus2(this)">
								Isolasi virus
							</td>
							<td>
								{{ Form::text('ket_isolasi_virus_jp2', null, array('class' => 'input-medium cek_periksa_virus2', 'placeholder' => '', 'disabled' => 'disabled')) }}
							</td>
						</tr>
						<tr>
							<td>
								<input type="checkbox" id="ITD2" value="1" name="itd_jp2" disabled onchange="change_itd2(this)">
								ITD
							</td>
							<td>
								{{ Form::text('ket_itd_jp2', null, array('class' => 'input-medium cek_periksa_itd2', 'placeholder' => '', 'disabled' => 'disabled')) }}
							</td>
						</tr>
						<tr>
							<td>
								<input type="checkbox" id="Sequencing2" value="1" name="sequencing_jp2" disabled onchange="change_sq2(this)">
								Sequencing
							</td>
							<td>
								{{ Form::text('ket_sequencing_jp2', null, array('placeholder' => '', 'class' => 'input-medium cek_periksa_sq2', 'disabled' => 'disabled')) }}
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Klasifikasi final</label>
				<div class="controls">
					{{ Form::select('klasifikasi_final', array(
					'0' => 'Pilih',
					'1' => 'Polio',
					'2' => 'Bukan polio',
					'3'=>'compatible'), Input::old('klasifikasi_final'), array('data-validation'         => '[MIXED]',
					'data'                    => '$ wajib di isi!',
					'data-validation-message' => 'klasifikasi final wajib di isi!','class' => 'input-medium id_combobox')) }}
					<span class="help-inline" id="error-klasifikasi_final" style="color:red">*</span>
				</div>
			</div>

		</fieldset>
	</div> <!-- /.media -->
</div> <!-- /.span11 -->

</div> <!-- /.row-fluid -->
</div> <!-- /.module-body -->

<!-- awal class form-actions -->
<div class="form-actions">
	{{Form::submit('simpan',array('class'=>'btn btn-primary'))}}
	{{Form::reset('batal',array('class'=>'btn btn-warning'))}}
</div>
<!-- akhir class form-actions -->
{{Form::close()}}