@extends('layouts.master')
@section('content')
<div class="span12">
	<div class="content">
		<div class="module">
			<div class="module-body">
				<div class="profile-head media">
					<h4>
						Detail penyelidikan epidemologi kasus penyakit AFP
					</h4>
					<hr>
				</div>
				<div class="tabbable-panel">
					<div class="tabbable-line">
						<ul class="nav nav-tabs ">
							<li class="active">
								<a href="#tab_default_1" data-toggle="tab">
								Identitas penderita </a>
							</li>
							<li>
								<a href="#tab_default_2" data-toggle="tab">
								Sumber informasi </a>
							</li>
							<li>
								<a href="#tab_default_3" data-toggle="tab">
								Riwayat sakit
								</a>
							</li>
							<li>
								<a href="#tab_default_4" data-toggle="tab">
								Gejala / Tanda sakit
								</a>
							</li>
							<li>
								<a href="#tab_default_5" data-toggle="tab">
								Riwayat kontak
								</a>
							</li>
							<li>
								<a href="#tab_default_6" data-toggle="tab">
								Status imunisasi polio
								</a>
							</li>
							<li>
								<a href="#tab_default_7" data-toggle="tab">
								Pengumpulan spesimen
								</a>
							</li>
							<li>
								<a href="#tab_default_8" data-toggle="tab">
								Hasil pemeriksaan
								</a>
							</li>
						</ul>
						@foreach($data as $row)
							<div class="tab-content">
								<div class="tab-pane active" id="tab_default_1">
									<table class="table table-striped">
								      <tr>
								          <td>No. Epidemologi</td>
								          <td>{{$row->no_epid}}</td>
								      </tr>
								      <tr>
								          <td>No induk kependudukan</td>
								          <td>{{$row->nik}}</td>
								      </tr>
								      <tr>
								          <td>Nama Pasien</td>
								          <td>{{$row->nama_anak}}</td>
								      </tr>
								      <tr>
								          <td>Nama Orang tua</td>
								          <td>{{$row->nama_ortu}}</td>
								      </tr>
								      <tr>
								          <td>Jenis Kelamin</td>
								          @if($row->jenis_kelamin==1)
								          <td>Laki-laki</td>
								          @elseif($row->jenis_kelamin==2)
								          <td>Perempuan</td>
								          @elseif($row->jenis_kelamin==3)
								          <td>Tidak diketahui</td>
								          @else
								          <td></td>
								          @endif
								      </tr>
								      <tr>
								          <td>Tanggal Lahir</td>
								          <td>{{Helper::getDate($row->tanggal_lahir)}}</td>
								      </tr>
								      <tr>
								          <td>Umur</td>
								          <td>{{$row->umur}} tahun {{$row->umur_bln}} bulan {{$row->umur_hr}} hari</td>
								      </tr>
								      <tr>
								          <td>Alamat</td>
								          <td>{{$row->alamat}}</td>
								      </tr>
									</table>
								</div>
								<div class="tab-pane" id="tab_default_2">
									<table class="table table-striped">
									<tr>
										<td>Provinsi</td>
										<td>{{$row->propinsi}}</td>
									</tr>
									<tr>
										<td>Kabupaten</td>
										<td>{{$row->kabupaten}}</td>
									</tr>
									<tr>
										<td>Laporan dari</td>
										<td>{{$row->laporan_dari}}</td>
									</tr>
									<tr>
										<td>Keterangan sumber laporan</td>
										<td>{{$row->ket_sumber_laporan}}</td>
									</tr>
									</table>
								</div>
								<div class="tab-pane" id="tab_default_3">
									<table class="table table-striped">
									<tr>
										<td>Tanggal mulai sakit</td>
										<td>{{Helper::getDate($row->tanggal_mulai_sakit)}}</td>
									</tr>
									<tr>
										<td>Tanggal mulai lumpuh</td>
										<td>{{Helper::getDate($row->tanggal_mulai_lumpuh)}}</td>
									</tr>
									<tr>
										<td>Tanggal meninggal (jika pasien meninggal)</td>
										<td>{{Helper::getDate($row->tanggal_meninggal)}}</td>
									</tr>
									<tr>
										<td>Sifat kelumpuhan</td>
										<td>{{$row->kelumpuhan}}</td>
									</tr>
									<tr>
										<td>Pernah berobat ke unit pelayanan lain</td>
										<td>{{$row->berobat_unit_pelayanan}}</td>
									</tr>
									@if($row->berobat_unit_pelayanan=='ya')
									<tr>
										<td>Nama unit pelayanan</td>
										<td>{{$row->berobat_unit_pelayanan}}</td>
									</tr>
									<tr>
										<td>Tanggal berobat</td>
										<td>{{Helper::getDate($row->berobat_unit_pelayanan)}}</td>
									</tr>
									<tr>
										<td>Diagnosis</td>
										<td>{{$row->berobat_unit_pelayanan}}</td>
									</tr>
									<tr>
										<td>No. Rekam medis</td>
										<td>{{$row->berobat_unit_pelayanan}}</td>
									</tr>
									@endif
									</table>
								</div>
								<div class="tab-pane" id="tab_default_4">
									<table class="table table-striped">
									<tr>
										<td>Demam sebelum lumpuh</td>
										<td>
										<?php
											if($row->demam_sebelum_lumpuh==1){
												echo "Ya";
											} else if($row->demam_sebelum_lumpuh==2){
												echo "Tidak";
											} else {
												echo "";
											}
										?>
									</tr>
									<tr>
										<td>Kelumpuhan tungkai kanan</td>
										<td>{{$row->lumpuh_tungkai_kanan}}</td>
									</tr>
									<tr>
										<td>Kelumpuhan tungkai kiri</td>
										<td>{{$row->lumpuh_tungkai_kiri}}</td>
									</tr>
									<tr>
										<td>Kelumpuhan lengan kanan</td>
										<td>{{$row->lumpuh_lengan_kanan}}</td>
									</tr>
									<tr>
										<td>Kelumpuhan lengan kiri</td>
										<td>{{$row->lumpuh_lengan_kiri}}</td>
									</tr>
									<tr>
										<td>Gangguan rasa raba tungkai kanan</td>
										<td>{{$row->raba_tungkai_kanan}}</td>
									</tr>
									<tr>
										<td>Gangguan rasa raba tungkai kiri</td>
										<td>{{$row->raba_tungkai_kiri}}</td>
									</tr>
									<tr>
										<td>Gangguan rasa raba lengan kanan</td>
										<td>{{$row->raba_lengan_kanan}}</td>
									</tr>
									<tr>
										<td>Gangguan rasa raba lengan kiri</td>
										<td>{{$row->raba_lengan_kiri}}</td>
									</tr>
									<tr>
										<td>Gejala lain-lain</td>
										<td>{{$row->catatan_lain}}</td>
									</tr>
									</table>
								</div>
								<div class="tab-pane" id="tab_default_5">
									<table class="table table-striped">
									<tr>
										<td>Dalam satu bulan terakhir sebelum sakit, apakah penderita pernah bepergian ?</td>
										<td>{{$row->sebelum_sakit_berpergian}}</td>
									</tr>
									@if($row->sebelum_sakit_berpergian=='ya')
									<tr>
										<td>Lokasi berpergian</td>
										<td>{{$row->lokasi}}</td>
									</tr>
									<tr>
										<td>Tanggal pergi</td>
										<td>{{Helper::getDate($row->tanggal_pergi)}}</td>
									</tr>
									@endif
									<tr>
										<td>Dalam satu bulan terakhir sebelum sakit, apakah penderita pernah berkunjung ke rumah anak yang baru mendapat imunisasi polio ?</td>
										<td>{{$row->berkunjung}}</td>
									</tr>
									</table>
								</div>
								<div class="tab-pane" id="tab_default_6">
									<table class="table table-striped">
									<tr>
										<td>Imunisasi rutin</td>
										<td>{{$row->imunisasi_rutin}}</td>
									</tr>
									<tr>
										<td>Jumlah dosis</td>
										<td>{{$row->lokasi}}</td>
									</tr>
									<tr>
										<td>Sumber informasi</td>
										<td>{{$row->lokasi}}</td>
									</tr>
									<tr>
										<td>PIN, Mop-up, ORI, BIAS Polio</td>
										<td>{{$row->bias_polio}}</td>
									</tr>
									<tr>
										<td>Jumlah dosis</td>
										@if($row->jumlah_dosis_bias_polio=='0')
										<td></td>
										@else
										<td>{{$row->jumlah_dosis_bias_polio}}</td>
										@endif
									</tr>
									<tr>
										<td>Sumber informasi</td>
										<td>{{$row->sumber_informasi_bias_polio}}</td>
									</tr>
									<tr>
										<td>Tanggal imunisasi polio yang paling akhir</td>
										<td>{{Helper::getDate($row->tanggal_imunisasi_polio)}}</td>
									</tr>
									</table>
								</div>
								<div class="tab-pane" id="tab_default_7">
									<table class="table table-striped">
									<tr>
										<td>Tanggal ambil spesimen I</td>
										<td>{{Helper::getDate($row->tanggal_ambil_spesimen_I)}}</td>
									</tr>
									<tr>
										<td>Tanggal ambil spesimen II</td>
										<td>{{Helper::getDate($row->tanggal_ambil_spesimen_II)}}</td>
									</tr>
									<tr>
										<td>Tanggal kirim spesimen 1 ke kabupaten/kota</td>
										<td>{{Helper::getDate($row->tanggal_kirim_spesimen_I)}}</td>
									</tr>
									<tr>
										<td>Tanggal kirim spesimen II ke kabupaten/kota</td>
										<td>{{Helper::getDate($row->tanggal_kirim_spesimen_II)}}</td>
									</tr>
									<tr>
										<td>Tanggal kirim spesimen 1 ke propinsi</td>
										<td>{{Helper::getDate($row->tanggal_kirim_spesimen_I_propinsi)}}</td>
									</tr>
									<tr>
										<td>Tanggal kirim spesimen II ke propinsi</td>
										<td>{{Helper::getDate($row->tanggal_kirim_spesimen_II_propinsi)}}</td>
									</tr>
									<tr>
										<td>Tidak diambil spesimen, berikan alasannya</td>
										<td>{{$row->catatan_tidak_diambil_spesimen}}</td>
									</tr>
									</table>
								</div>
								<div class="tab-pane" id="tab_default_8">
									<table class="table table-striped">
									<tr>
										<td>Diagnosis</td>
										<td>{{$row->hasil_diagnosis}}</td>
									</tr>
									<tr>
										<td>Nama DSA/DSS/DRM/Dr/pemeriksa lain</td>
										<td>{{$row->nama_DSA}}</td>
									</tr>
									</table>
								</div>
							</div>
						@endforeach
					</div>
				</div>
				<div class="form-actions" style="text-align:center">
				  	<a href="{{URL::to('afp')}}" class="btn btn-warning">kembali</a>
				</div>
			</div>
		</div>
	</div>
</div>
@stop