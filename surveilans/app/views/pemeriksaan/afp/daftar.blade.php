<div class="module-body uk-overflow-container">
  <table class="table table-bordered display" id="example_pe_afp">
  <thead>
      <tr>
        <th>No. Epid</th>
        <th>Nama pasien</th>
        <th>Nama Orang Tua</th>
        <th>Jenis kelamin</th>
        <th>Umur</th>
        <th>Alamat</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
    @if($pe_afp)
    @foreach($pe_afp as $row)
      <tr>
          <td>{{$row->afp_no_epid}}</td>
          <td>{{$row->pasien_nama_anak}}</td>
          <td>{{$row->pasien_nama_ortu}}</td>
          @if($row->pasien_jenis_kelamin=='1')
          <td>Laki-laki</td>
          @elseif($row->pasien_jenis_kelamin=='2')
          <td>Perempuan</td>
          @else
          <td></td>
          @endif
          <td>{{$row->pasien_umur or '-'}} Th {{$row->pasien_umur_bln or '-'}} Bln {{$row->pasien_umur_hr or '-'}} Hari</td>
          <td>{{$row->pasien_alamat}}</td>
          @if(Sentry::getUser()->hak_akses==2 || Sentry::getUser()->hak_akses==3 || Sentry::getUser()->hak_akses==7)
          <td>
          <div class="btn-group" role="group">
          <a href="{{URL::to('pe_afp_detail/'.$row->pe_id)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
          <a href="{{URL::to('pe_afp_edit/'.$row->afp_id_afp)}}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>
          <a href="{{URL::to('pe_afp_hapus/'.$row->afp_id_afp)}}" onclick="return confirm('apakah yakin ingin menghapus data ini?');return false;" class="btn btn-xs btn-danger" data-uk-tooltip title="hapus data"><i class="icon icon-remove"></i></a>
          </div>
          </td>
          @elseif(Sentry::getUser()->hak_akses==4)
          <td>
          <div class="btn-group" role="group">
          <a  href="{{URL::to('pe_afp_detail/'.$row->pe_id)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
          <a  href="{{URL::to('pe_afp_edit/'.$row->afp_id_afp)}}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>
          </div>
          </td>
          @elseif(Sentry::getUser()->hak_akses==1 || Sentry::getUser()->hak_akses==6)
          <td>
          <div class="btn-group" role="group">
          <a href="{{URL::to('pe_afp_detail/'.$row->pe_id)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
          </div>
          </td>
          @elseif(Sentry::getUser()->hak_akses==5)

          @endif          
          
      </tr>
          @endforeach
      @endif
    </tbody>
  </table>
  <script>
  $(document).ready(function() {
    $("#example_length select").select2();
    $('#example_pe_afp').dataTable();
  });
  </script>
</div>