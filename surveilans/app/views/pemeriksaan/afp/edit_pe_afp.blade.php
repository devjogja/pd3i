@extends('layouts.master')
@section('content')
<div class="span12">
	<div class="content">
		<div class="module">
			<div class="module-body">
				<div class="profile-head media">
					<h4>
						Penyelidikan Epidemologi Pasien Kasus Penyakit AFP
					</h4>
					<hr>
				</div>
				{{Form::open(array('url'=>'pe_afp_update','class'=>'form-horizontal'))}}
				<div class="module-body">
					<div class="row-fluid">
						<div class="span12">
							<div class="media">
								<input type="hidden" name="id_afp" value="{{$row->afp_id_afp}}">
								<fieldset>
									<legend>Identitas penderita</legend>
									<div class="control-group">
										<label class="control-label">No. epidemologi</label>
										<div class="controls">
											{{Form::text('dp[no_epid]',$row->afp_no_epid,array('class'=>'input-large','placeholder'=>'Masukan nomer epidemologi'))}}
											{{Form::hidden('id_pasien',$row->pasien_id_pasien,array('class'=>'input-medium id_pasien'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Nama penderita</label>
										<div class="controls">
											{{Form::text('dp[nama_anak]',$row->pasien_nama_anak,array('class'=>'input-medium nama_anak'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Nama orang tua</label>
										<div class="controls">
											{{Form::text('dp[nama_ortu]',$row->pasien_nama_ortu,array('class'=>'input-medium nama_ortu'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Jenis kelamin</label>
										<div class="controls">
											{{Form::select('dp[jenis_kelamin]',[
											'0' => 'Pilih',
											'1'=>'Laki-laki',
											'2'=>'Perempuan',
											'3' => 'Tidak diketahui',
											'4'=>'Lainnya'],[$row->pasien_jenis_kelamin], array('class' => 'input-large id_combobox jenis_kelamin'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Tanggal lahir</label>
										<div class="controls">
											{{Form::text('dp[tanggal_lahir]',$row->pasien_tanggal_lahir, array('class' => 'input-medium tanggal_lahir','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','onchange'=>'umur_pe()'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<input type="text" class="input-mini" value="{{$row->pasien_umur}}" name="dp[tgl_tahun]" id="tanggal_tahun">
											Thn 
											<input type="text" class="input-mini" value="{{$row->pasien_umur_bln}}" name="dp[tgl_bln]" id="tanggal_bulan">
											Bln 
											<input type="text" class="input-mini" value="{{$row->pasien_umur_hr}}" name="dp[tgl_hr]" id="tanggal_hari">
											Hr
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Provinsi</label>
										<div class="controls">
											{{ Form::select('dp[id_provinsi]',
											array(''=>'Pilih provinsi')+Provinsi::lists('provinsi','id_provinsi'),
											$row->pasien_id_provinsi,
											array('id'=>'id_provinsi_pasien',
											'placeholder' => 'Pilih Provinsi',
											'onchange'=>'getKabupaten("_pasien")',
											'class'=>'input-large')) }}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Kabupaten/Kota</label>
										<div class="controls">
											<select name="" class="input-large id_combobox" id="id_kabupaten_pasien" onchange="getKecamatan('_pasien')">
												<option value="">{{$row->pasien_kabupaten}}</option>
											</select>
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Kecamatan</label>
										<div class="controls">
											<select name="" class="input-large id_combobox" id="id_kecamatan_pasien" onchange="getKelurahan('_pasien')">
												<option value="">{{$row->pasien_kecamatan}}</option>
											</select>
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Kelurahan/Desa</label>
										<div class="controls">
											<select name="dp[id_kelurahan]" class="input-large id_combobox" id="id_kelurahan_pasien">
												<option value="{{$row->pasien_id_kelurahan}}">{{$row->pasien_kelurahan}}</option>
											</select>
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Alamat</label>
										<div class="controls">
											{{Form::textarea('dp[alamat]',$row->pasien_alamat, array('class' => 'input-large alamat','rows'=>'4','placeholder'=>'tulisan alamat seperti nama dusun/gang/blok/no jalan/rt/rw'))}}
											<span class="help-inline"></span>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<div class="media">
								<fieldset>
									<legend>Sumber informasi</legend>
									<div class="control-group">
										<label class="control-label">Provinsi</label>
										<div class="controls">
											{{ Form::select('propinsi',
											array(''=>'Pilih provinsi')+Provinsi::lists('provinsi','id_provinsi'),
											$row->pe_propinsi,
											array('id'=>'id_provinsi_sumber',
											'placeholder' => 'Pilih Provinsi',
											'onchange'=>'getKabupaten("_sumber")',
											'class'=>'input-large')) }}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Kabupaten</label>
										<div class="controls">
											{{Form::select('kabupaten',array($row->sumberinformasi_id_kabupaten=>$row->sumberinformasi_kabupaten),Input::old('kabupaten'),array('class'=>'input-large','id'=>'id_kabupaten_sumber'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Laporan dari</label>
										<div class="controls">
											{{Form::select('laporan_dari',array(''=>'Pilih','Rumah sakit' => 'Rumah sakit','Puskesmas'=>'Puskesmas','Dokter praktek'=>'Dokter praktek','Lainnya'=>'Lainnya'),$row->pe_laporan_dari,array('class'=>'input-large'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" >Keterangan sumber laporan</label>
										<div class="controls">
											{{Form::textarea('ket_sumber_laporan',$row->pe_ket_sumber_laporan,array('rows'=>'3'))}}
											<span class="help-inline"></span>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<div class="media">
								<fieldset>
									<legend>Riwayat sakit</legend>
									<div class="control-group">
										<label class="control-label" >Tanggal mulai sakit</label>
										<div class="controls">
											{{Form::text('tanggal_mulai_sakit',$row->pe_tanggal_mulai_sakit,array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
											<span class="help-inline"></span>
										</div> 
									</div>
									<div class="control-group">
										<label class="control-label" >Tanggal mulai lumpuh</label>
										<div class="controls">
											{{Form::text('tanggal_mulai_lumpuh',$row->pe_tanggal_mulai_lumpuh,array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
											<span class="help-inline"></span>
										</div> 
									</div>
									<div class="control-group">
										<label class="control-label" >Tanggal meninggal (bila penderita meninggal) </label>
										<div class="controls">
											{{Form::text('tanggal_meninggal',$row->pe_tanggal_meninggal,array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
											<span class="help-inline"></span>
										</div> 
									</div>
									<div class="control-group">
										<label class="control-label" >Sebelum dilaporkan, apakah penderita berobat ke unit pelayanan lain?</label>
										<div class="controls">
											{{Form::select('berobat_unit_pelayanan', array(
											'0'=>'Pilih',
											'1' => 'Ya',
											'2'=>'Tidak'),$row->pe_berobat_unit_pelayanan, array('class' => 'input-small','id'=>'id_unit','onchange'=>'cekBerobat()'))}}
										</div>
									</div>
									<div class="control-group">
										<div class="control-label">
											<table>
												<tr>
													<td>Nama unit pelayanan</td>
												</tr>
												<tr>
													<td></td>
												</tr>
												<tr>
													<td>Tanggal berobat</td>
												</tr>
												<tr>
													<td>&nbsp;</td>
												</tr>
												<tr>
													<td>Diagnosis</td>
												</tr>
												<tr>
													<td></td>
												</tr>
												<tr>
													<td>NO. Rekam medis</td>
												</tr>
											</table>
										</div>
										<div class="controls">
											<table>
												<tr>
													<td> : {{Form::text('nama_unit_pelayanan',$row->pe_nama_unit_pelayanan,array('id'=>'unit_1','disabled'=>'disabled','class'=>'input-medium id_unit_pelayanan'))}}</td>
												</tr>
												<tr>
													<td> : {{Form::text('tanggal_berobat',$row->pe_tanggal_berobat,array('id'=>'unit_2','disabled'=>'disabled','class'=>'input-small id_berobat','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}</td>
												</tr>
												<tr>
													<td> : {{Form::text('diagnosis',$row->pe_diagnosis,array('id'=>'unit_3','disabled'=>'disabled','class'=>'input-small id_diagnosis'))}}</td>
												</tr>
												<tr>
													<td> : {{Form::text('no_rekam_medis',$row->pe_no_rekam_medis,array('id'=>'unit_4','disabled'=>'disabled','class'=>'input-medium id_rekam_medis'))}}</td>
												</tr>
											</table>
											<span class="help-inline"></span>
										</div> 
									</div>
									<div class="control-group">
										<label class="control-label">Apakah kelumpuhan sifatnya akut (1-14 hari)?</label>
										<div class="controls">
											{{Form::select('kelumpuhan_akut',array(
											'0'=>'Pilih',
											'1' => 'Ya',
											'2'=>'Tidak',
											'3'=>'Tidak jelas'),$row->pe_kelumpuhan_akut,array('class'=>'input-large','id'=>'id_akut','onchange'=>'cekPelacakan()'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Apakah kelumpuhan sifatnya layuh (flaccid)?</label>
										<div class="controls">
											{{Form::select('kelumpuhan_layuh',array(
											'0'=>'Pilih',
											'1' => 'Ya',
											'2'=>'Tidak',
											'3'=>'Tidak jelas'),$row->pe_kelumpuhan_layuh,array('class'=>'input-large','id'=>'id_layuh','onchange'=>'cekPelacakan()'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Apakah kelumpuhan disebabkan ruda?</label>
										<div class="controls">
											{{Form::select('kelumpuhan_ruda',array(
											'0'=>'Pilih',
											'1' => 'Ya',
											'2'=>'Tidak',
											'3'=>'Tidak jelas'),$row->pe_kelumpuhan_ruda,array('class'=>'input-large','id'=>'id_lumpuh','onchange'=>'cekPelacakan()'))}}
											<span class="help-inline"></span>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
					<br>
					<div class="row-fluid">
						<div id="stop">
							<div class="span6">
								<div class="media">
									<fieldset>
										<legend>Gejalan/tanda</legend>
										<div class="control-group">
											<label class="control-label">Apakah penderita demam sebelum lumpuh ?</label>
											<div class="controls">
												{{Form::select('demam_sebelum_lumpuh', array(
												'0'=>'Pilih',
												'1' => 'Ya',
												'2'=>'Tidak'),$row->pe_demam_sebelum_lumpuh, array('class' => 'input-small'))}}
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Kelumpuhan tungkai kanan</label>
											<div class="controls">
												{{Form::select('lumpuh_tungkai_kanan', array(
												'0'=>'Pilih',
												'1' => 'Ya',
												'2'=>'Tidak'),$row->pe_lumpuh_tungkai_kanan, array('class' => 'input-small'))}}
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Kelumpuhan tungkai kiri</label>
											<div class="controls">
												{{Form::select('lumpuh_tungkai_kiri', array(
												'0'=>'Pilih',
												'1' => 'Ya',
												'2'=>'Tidak'),$row->pe_lumpuh_tungkai_kiri, array('class' => 'input-small'))}}
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Kelumpuhan lengan kanan</label>
											<div class="controls">
												{{Form::select('lumpuh_lengan_kanan', array(
												'0'=>'Pilih',
												'1' => 'Ya',
												'2'=>'Tidak'),$row->pe_lumpuh_lengan_kanan, array('class' => 'input-small'))}}
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Kelumpuhan lengan kiri</label>
											<div class="controls">
												{{Form::select('lumpuh_lengan_kiri', array(
												'0'=>'Pilih',
												'1' => 'Ya',
												'2'=>'Tidak'),$row->pe_lumpuh_lengan_kiri, array('class' => 'input-small'))}}
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Gangguan rasa raba tungkai kanan</label>
											<div class="controls">
												{{Form::select('raba_tungkai_kanan', array(
												'0'=>'Pilih',
												'1' => 'Ya',
												'2'=>'Tidak'),$row->pe_raba_tungkai_kanan, array('class' => 'input-small'))}}
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Gangguan rasa raba tungkai kiri</label>
											<div class="controls">
												{{Form::select('raba_tungkai_kiri', array(
												'0'=>'Pilih',
												'1' => 'Ya',
												'2'=>'Tidak'),$row->pe_raba_tungkai_kiri, array('class' => 'input-small'))}}
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Gangguan rasa raba lengan kanan</label>
											<div class="controls">
												{{Form::select('raba_lengan_kanan', array(
												'0'=>'Pilih',
												'1' => 'Ya',
												'2'=>'Tidak'),$row->pe_raba_lengan_kanan, array('class' => 'input-small'))}}
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Gangguan rasa raba lengan kiri</label>
											<div class="controls">
												{{Form::select('raba_lengan_kiri', array(
												'0'=>'Pilih',
												'1' => 'Ya',
												'2'=>'Tidak'),$row->pe_raba_lengan_kiri, array('class' => 'input-small'))}}
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Lain-lain, sebutkan</label>
											<div class="controls">
												{{Form::textarea('catatan_lain',$row->pe_catatan_lain,array('rows'=>'3'))}}
												<span class="help-inline"></span>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
							<div class="span6">
								<div class="media">
									<fieldset>
										<legend>Riwayat kontak</legend>
										<div class="control-group">
											<label class="control-label">Dalam satu bulan terakhir sebelum sakit, apakah penderita pernah bepergian ?</label>
											<div class="controls">
												{{Form::select('sebelum_sakit_berpergian',array(''=>'Pilih','ya'=>'Ya','tidak'=>'Tidak'),$row->pe_sebelum_sakit_berpergian,array('class'=>'input-small berpergian'))}}
												<table>
													<tr>
														<td>Lokasi</td>
														<td>:</td>
														<td>{{Form::text('lokasi',$row->pe_demam_sebelum_lumpuh,array('class'=>'input-medium lokasi'))}}</td>
													</tr>
													<tr>
														<td>Tanggal pergi</td>
														<td>:</td>
														<td>{{Form::text('tanggal_pergi',$row->pe_demam_sebelum_lumpuh,array('class'=>'input-small tanggal_pergi','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}</td>
													</tr>
												</table>             
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Dalam satu bulan terakhir sebelum sakit, apakah penderita pernah berkunjung ke rumah anak yang baru mendapat imunisasi polio ?</label>
											<div class="controls">
												{{Form::select('berkunjung',array(''=>'Pilih','ya'=>'Ya','tidak'=>'Tidak'),$row->pe_berkunjung,array('class'=>'input-small'))}}
												<span class="help-inline"></span>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
							<div class="span6">
								<div class="media">
									<fieldset>
										<legend>Status imunisasi polio</legend>
										<table>
											<tr>
												<td>Imunisasi rutin</td>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td>Jumlah dosis</td>
												<td>
													{{Form::select(
													'jumlah_dosis',
													array(
													'0' => 'Pilih',
													'1' => '1X',
													'2'=>'2X',
													'3'=>'3X',
													'4'=>'4X',
													'5'=>'5X',
													'6'=>'6X',
													'7'=>'Tidak [belum pernah]',
													'8'=>'Tidak tahu'
													),
													$row->pe_jumlah_dosis,
													array(
													'class' => 'input-medium id_combobox'
													)
													)
												}}
											</td>
										</tr>
										<tr>
											<td>Sumber informasi</td>
											<td>
												{{Form::select(
												'sumber_informasi',
												array(
												'0' => 'Pilih',
												'1' => 'KMS/Jurim',
												'2'=>'Ingatan responden'
												),
												$row->pe_sumber_informasi,
												array(
												'class' => 'input-medium id_combobox'
												)
												)
											}}
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>PIN, Mop-up, ORI, BIAS Polio</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>Jumlah dosis</td>
										<td>
											{{Form::select(
											'jumlah_dosis_bias_polio',
											array(
											'0' => 'Pilih',
											'1' => '1X',
											'2'=>'2X',
											'3'=>'3X',
											'4'=>'4X',
											'5'=>'5X',
											'6'=>'6X',
											'7'=>'Tidak [belum pernah]',
											'8'=>'Tidak tahu'
											),
											$row->pe_jumlah_dosis_bias_polio,
											array(
											'class' => 'input-medium id_combobox'
											)
											)
										}}
									</td>
								</tr>
								<tr>
									<td>Sumber informasi</td>
									<td>
										{{Form::select(
										'sumber_informasi_bias_polio',
										array(
										'0' => 'Pilih',
										'1' => 'Catatan',
										'2'=>'Ingatan responden'
										),
										$row->pe_sumber_informasi_bias_polio,
										array(
										'class' => 'input-medium id_combobox'
										)
										)
									}}
								</td>
							</tr>
						</table>
						<br/>
						<div class="control-group">
							<label class="control-label">Tanggal imunisasi polio yang paling akhir</label>
							<div class="controls">
								{{Form::text('tanggal_imunisasi_polio',$row->pe_tanggal_imunisasi_polio,array('class'=>'input-small','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
								<span class="help-inline"></span>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span12">
				<div class="media">
					<fieldset>
						<legend>Pengumpulan spesimen</legend>
						<div class="control-group">
							<label class="control-label">Tanggal ambil spesimen I</label>
							<div class="controls">
								{{Form::text('tanggal_ambil_spesimen_I',$row->pe_tanggal_ambil_spesimen_I,array('class'=>'input-small','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
								<span class="help-inline"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Tanggal ambil spesimen II</label>
							<div class="controls">
								{{Form::text('tanggal_ambil_spesimen_II',$row->pe_tanggal_ambil_spesimen_II,array('class'=>'input-small','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
								<span class="help-inline"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Tanggal kirim spesimen 1 ke kabupaten/kota</label>
							<div class="controls">
								{{Form::text('tanggal_kirim_spesimen_I',$row->pe_tanggal_kirim_spesimen_I,array('class'=>'input-small','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
								<span class="help-inline"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Tanggal kirim spesimen II ke kabupaten/kota</label>
							<div class="controls">
								{{Form::text('tanggal_kirim_spesimen_II',$row->pe_tanggal_kirim_spesimen_II,array('class'=>'input-small','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
								<span class="help-inline"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Tanggal kirim spesimen 1 ke propinsi</label>
							<div class="controls">
								{{Form::text('tanggal_kirim_spesimen_I_propinsi',$row->pe_tanggal_kirim_spesimen_I_propinsi,array('class'=>'input-small','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
								<span class="help-inline"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Tanggal kirim spesimen II ke propinsi</label>
							<div class="controls">
								{{Form::text('tanggal_kirim_spesimen_II_propinsi',$row->pe_tanggal_kirim_spesimen_II_propinsi,array('class'=>'input-small','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
								<span class="help-inline"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Tidak diambil spesimen, berikan alasannya</label>
							<div class="controls">
								{{Form::textarea('catatan_tidak_diambil_spesimen',$row->pe_catatan_tidak_diambil_spesimen,array('rows'=>'3','cols'=>'25'))}}
								<span class="help-inline"></span>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span12">
				<div class="media">
					<fieldset>
						<legend>Petugas pelacak</legend>
						<div class="control-group">
							<label class="control-label">Nama petugas</label>
							<div class="controls">
								{{Form::text('nama_petugas',$row->pe_nama_petugas,array('class'=>'input-large'))}}
								<span class="help-inline"></span>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="12">
				<div class="media">
					<fieldset>
						<legend>Hasil pemeriksaan</legend>
						<div class="control-group">
							<label class="control-label">Diagnosis</label>
							<div class="controls">
								{{Form::text('hasil_diagnosis',$row->pe_hasil_diagnosis,array('class'=>'input-medium'))}}
								<span class="help-inline"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Nama DSA/DSS/DRM/Dr/pemeriksa lain</label>
							<div class="controls">
								{{Form::text('nama_DSA',$row->pe_nama_DSA,array('class'=>'input-large'))}}
								<span class="help-inline"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Telepon/Hp</label>
							<div class="controls">
								{{Form::text('telepon_hp',$row->pe_telepon_hp,array('class'=>'input-large'))}}
								<span class="help-inline"></span>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="alert alert-arror" id="id_stop"><center>Stop pelacakan</center></div>
<div class="form-actions">
	{{Form::submit('simpan',array('class'=>'btn btn-primary'))}}
	<a href="{{URL::to('afp')}}" class="btn btn-default">Batal</a>
</div>
</div>
{{Form::close()}}
@include('pemeriksaan.campak.analisis-js')
<script type="text/javascript">
	$(document).ready(function(){
		$('#id_stop').hide();
	});

	function cekPelacakan() 
	{
		var akut = $('#id_akut').val();
		var layuh = $('#id_layuh').val();
		var lumpuh = $('#id_lumpuh').val();
		if(akut=='1' && layuh=='1' && lumpuh=='0')
		{
			$('#stop').hide(2000);
			$('#id_stop').show();
		}
		else 
		{
			$('#stop').show(2000);
			$('#id_stop').hide();
		}
	}

	function cekBerobat()
	{
		var cek = $('#id_unit').val();
		if(cek=='1'){
			$('#unit_1').removeAttr('disabled');
			$('#unit_2').removeAttr('disabled');
			$('#unit_3').removeAttr('disabled');
			$('#unit_4').removeAttr('disabled');
		}
		else {
			$('#unit_1').attr('disabled','disabled');
			$('#unit_2').attr('disabled','disabled');
			$('#unit_3').attr('disabled','disabled');
			$('#unit_4').attr('disabled','disabled');
		}
	}
</script>
@stop