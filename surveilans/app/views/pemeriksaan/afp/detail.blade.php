<!-- detail kasus difteri -->
@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <h4>
                        Detail Pasien Kasus Penyakit AFP
                    </h4>
                    <hr>
                </div>
<table class="table table-striped">
  @if($afp)
  @foreach($afp as $row)
      <tr>
          <td>No. Epidemologi</td>
          <td>{{$row->no_epid}}</td>
      </tr>
      <tr>
          <td>No. Epidemologi lama</td>
          <td>{{$row->no_epid_lama}}</td>
      </tr>
      <tr>
          <td>No induk kependudukan</td>
          <td>{{$row->nik}}</td>
      </tr>
      <tr>
          <td>Nama Pasien</td>
          <td>{{$row->nama_anak}}</td>
      </tr>
      <tr>
          <td>Nama Orang tua</td>
          <td>{{$row->nama_ortu}}</td>
      </tr>
      <tr>
          <td>Jenis Kelamin</td>
          @if($row->jenis_kelamin=='1')
          <td>Laki-laki</td>
          @elseif($row->jenis_kelamin=='2')
          <td>Perempuan</td>
          @elseif($row->jenis_kelamin=='3')
          <td>Tidak jelas</td>
          @else
          <td></td>
          @endif
      </tr>
      <tr>
          <td>Tanggal Lahir</td>
          <td>{{Helper::getDate($row->tanggal_lahir)}}</td>
      </tr>
      <tr>
          <td>Umur</td>
          <td>{{$row->umur}} tahun</td>
      </tr>
      <tr>
          <td>Alamat</td>
          <td>{{$row->alamat}}</td>
      </tr>
  </table>
  <hr>
  @endforeach
  @endif
  <br/>
  <div class="module-body uk-overflow-container">
  <div class="table-responsive">
  <table class="table table-bordered" style="font-size:10px">
  <caption style="border:1px solid #eee;background-color:#666; color:#fff">Data surveilans AFP</caption>
    <tr style="background-color:#eee;">
      {{-- <td>Tanggal periksa</td>  --}}
      <td>Tanggal mulai lumpuh</td>
      <td>Demam sebelum lumpuh</td>
      <td>Kelumpuhan anggota gerak kanan</td>
      <td>Kelumpuhan anggota gerak kiri</td>
      <td>Gangguan raba anggota gerak kanan</td>
      <td>Gangguan raba anggota gerak kiri</td>
      <td>Imunisasi polio sebelum sakit</td>
      <td>Tanggal imunisasi polio terakhir</td>
      <td>Tanggal laporan diterima</td>
      <td>Tanggal pelacakan</td>
      <td>Riwayat kontak</td>
      <td>Keadaan akhir</td>
    </tr>
     @if($afp)
     @foreach($afp as $result)
    <tr>
      {{-- <td>{{Helper::getDate($result->tanggal_periksa)}}</td> --}}
      <td>{{Helper::getDate($result->tanggal_mulai_lumpuh)}}</td>

      @if($result->demam_sebelum_lumpuh==1)
      <td>Ya</td>
      @elseif($result->demam_sebelum_lumpuh==2)
      <td>Tidak</td>
      @else
      <td>&nbsp;</td>
      @endif

      @if($result->kelumpuhan_anggota_gerak_kanan==1)
      <td>Lengan (1)</td>
      @elseif($result->kelumpuhan_anggota_gerak_kanan==2)
      <td>Tungkai (1)</td>
      @elseif($result->kelumpuhan_anggota_gerak_kanan==3)
      <td>Lengan dan Tungkai (2)</td>
      @elseif($result->kelumpuhan_anggota_gerak_kanan==4)
      <td>Tidak</td>
      @else
      <td>&nbsp;</td>
      @endif

      @if($result->kelumpuhan_anggota_gerak_kiri==1)
      <td>Lengan (1)</td>
      @elseif($result->kelumpuhan_anggota_gerak_kiri==2)
      <td>Tungkai (1)</td>
      @elseif($result->kelumpuhan_anggota_gerak_kiri==3)
      <td>Lengan dan Tungkai (2)</td>
      @elseif($result->kelumpuhan_anggota_gerak_kiri==4)
      <td>Tidak</td>
      @else
      <td>&nbsp;</td>
      @endif

      @if($result->gangguan_raba_anggota_gerak_kanan==1)
      <td>Lengan (1)</td>
      @elseif($result->gangguan_raba_anggota_gerak_kanan==2)
      <td>Tungkai (1)</td>
      @elseif($result->gangguan_raba_anggota_gerak_kanan==3)
      <td>Lengan dan Tungkai (2)</td>
      @elseif($result->gangguan_raba_anggota_gerak_kanan==4)
      <td>Tidak</td>
      @else
      <td>&nbsp;</td>
      @endif

      @if($result->gangguan_raba_anggota_gerak_kiri==1)
      <td>Lengan (1)</td>
      @elseif($result->gangguan_raba_anggota_gerak_kiri==2)
      <td>Tungkai (1)</td>
      @elseif($result->gangguan_raba_anggota_gerak_kiri==3)
      <td>Lengan dan Tungkai (2)</td>
      @elseif($result->gangguan_raba_anggota_gerak_kiri==4)
      <td>Tidak</td>
      @else
      <td>&nbsp;</td>
      @endif      

      @if($result->imunisasi_polio_sebelum_sakit==1)
      <td>1X</td>
      @elseif($result->imunisasi_polio_sebelum_sakit==2)
      <td>2X</td>
      @elseif($result->imunisasi_polio_sebelum_sakit==3)
      <td>3X</td>
      @elseif($result->imunisasi_polio_sebelum_sakit==4)
      <td>4X</td>
      @elseif($result->imunisasi_polio_sebelum_sakit==5)
      <td>5X</td>
      @elseif($result->imunisasi_polio_sebelum_sakit==6)
      <td>6X</td>
      @elseif($result->imunisasi_polio_sebelum_sakit==7)
      <td>Tidak</td>
      @elseif($result->imunisasi_polio_sebelum_sakit==8)
      <td>Tidak tahu</td>
      @else
      <td>&nbsp;</td>
      @endif
      
      <td>{{Helper::getDate($result->tanggal_vaksinasi_polio)}}</td>
      <td>{{Helper::getDate($result->tanggal_laporan_diterima)}}</td>
      <td>{{Helper::getDate($result->tanggal_pelacakan)}}</td>
      
      @if($result->kontak==1)
      <td>Ya</td>
      @elseif($result->kontak==2)
      <td>Tidak</td>
      @else
      <td>&nbsp;</td>
      @endif

      @if($result->keadaan_akhir==1)
      <td>Hidup</td>
      @elseif($result->keadaan_akhir==2)
      <td>Meninggal</td>
      @else
      <td>&nbsp;</td>
      @endif

    </tr>
    @endforeach
    @endif
  </table>
  </div>
  </div>
  <div class="module-body uk-overflow-container">
  <div class="table-responsive">

    <table class="table table-bordered" style="font-size:10px">
    <caption style="border:1px solid #eee;background-color:#666; color:#fff">Data spesimen dan laboratorium</caption>
      <tr style="background-color:#eee;">
        <td>No spesimen</td>
        <td>Tempat uji laboratorim</td>
        <td>Tanggal uji laboratorium</td>
        <td colspan="6" style="text-align:center">Klasifikasi Final</td>
      </tr>
            @foreach($afp as $rslt)
                <?php $hasil= Laboratorium::getLaboratorium($rslt->id_uji_lab)?>
                @foreach($hasil as $hsl)
                    <tr>
                        <td>{{$hsl->no_spesimen}}</td>
                        <td>fsafhasfasa</td>
                        <td>2492492</td>
                        <td colspan="6" style="text-align:center">positih</td>
                    </tr>
                @endforeach
                @if(empty($hasil))
                <tr>
                    <td>no data</td>
                    <td>no data</td>
                    <td>no data</td>
                    <td colspan="6" style="text-align:center">no data</td>
                </tr>
                @endif
            @endforeach
      <tr style="background-color:#eee;">
        <td colspan="2" rowspan="2" valign="center" style="text-align:center">Tanggal pengambilan spesimen (stool)</td>
        <td colspan="6" style="text-align:center">Hasil laboratorium (spesimen)</td>
      </tr>
      <tr>
        <td colspan="3" style="text-align:center">Spesimen I</td>
        <td colspan="3" style="text-align:center">Spesimen II</td>
      </tr>
      <tr>
        <td>Spesimen I</td>
        <td>Spesimen II</td>
        <td>solasi  virus</td>
        <td>ITD</td>
        <td>Sequencing</td>
        <td>solasi  virus</td>
        <td>ITD</td>
        <td>Sequencing</td>
      </tr>
      @foreach($afp as $rslt)
      <tr>
        <td>{{$rslt->tanggal_pengambilan_spesimen1}}</td>
        <td>{{$rslt->tanggal_pengambilan_spesimen2}}</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      @endforeach
    </table>
  </div>
  </div>
  <div class="form-actions" style="text-align:center">
    <a href="{{URL::to('afp')}}" class="btn btn-warning">Batal</a>
</div>
  </div>

  </div>
  @stop
                  