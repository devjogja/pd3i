<table class="table" style="width:100%;">
	<tr>
		<td style="text-align:right;width:10%;">
			Rentang Waktu
		</td>
		<td style="width:23%;">
			<select name="unit" class="unit" style="width: 90px;" onkeypress="focusNext('day_start', 'clinic_id', this, event)" onchange="setDisable(this, event)">
				<option value="all">All</option>
				<option value="day">Hari</option>
				<option value="month">Bulan</option>
				<option value="year">Tahun</option>
			</select>
		</td>
		<td style="text-align:right;width:10%;">
			Provinsi
		</td>
		<td style="width:23%;">
			{{ Form::select(
				'province_id',
				array(
					''=>'Pilih provinsi')+Provinsi::lists('provinsi','id_provinsi'),
					null,
					array(
						'id'=>'id_provinsi_analisis',
						'placeholder' => 'Pilih Provinsi',
						'onchange'=>"getKabupaten('_analisis')",
					)
				)
			}}
		</td>
		<td style="text-align:right;width:10%;">
			Rumah Sakit
		</td>
		<td style="width:23%;">
			<select name="rs_id" id="rs_id_analisis" style="width:80%" disabled="disabled">
			<option value="">--- Pilih ---</option>
		</td>
	</tr>
</table>