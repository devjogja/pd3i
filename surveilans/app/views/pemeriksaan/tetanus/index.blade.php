@extends('layouts.master')
@section('content')
<div class="span12">
	<div class="content">
		<div class="module">
			<div class="module-body">
				<div class="profile-head media">
					<h4>
						Penyakit Tetanus neonatorum
					</h4>
					<hr>
				</div>
				@include('pemeriksaan.tetanus._include_to_index.tab_navigasi')
				<div class="profile-tab-content tab-content">
					<div class="tab-pane fade active in" id="daftar">
						@if(Session::get('type')=='rs')
						@include('pemeriksaan._include.filter_daftar_kasus_rs')
						@elseif(Session::get('type')=='puskesmas')
						@include('pemeriksaan._include.filter_daftar_kasus_puskesmas')
						@else
						@include('pemeriksaan._include.filter_daftar_kasus')
						@endif
						@include('pemeriksaan.tetanus._include_to_index.daftar_kasus')
					</div>
					<div class="tab-pane fade" id="input">
						@include('pemeriksaan.tetanus._include_to_index.input_data_individual_kasus')
					</div>
					@include('pemeriksaan.campak.analisis')
					<div class="tab-pane fade" id="PE">
						<div id="data_tetanus"></div>
					</div>
					<div class="tab-pane fade in" id="importtetanus">
						@include('pemeriksaan.tetanus.import')
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('pemeriksaan.campak.analisis-js')

<!-- JS -->
<script>
	$(document).ready(function() {
		$("select").select2();

		$("#example-tetanus").dataTable();
		$("#example-pe_tetanus").dataTable();
		$('#form_save_tetanus').validate({
			submit: {
				settings: {
					scrollToError: {
						offset: -100,
						duration: 500
					}
				}
			}
		});

		$('#form_save_tetanus input').keydown(function(e) {
			if(e.keyCode == 13) {

			// check for submit button and submit form on enter press
			if($(':input:eq(' + ($(':input').index(this) + 1) + ')').attr('type') == 'submit') {
				return true;
			}

			$(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();

			return false;
		}

	});

		$( "#id_kelurahan" ).change(function() {
			$.ajax({
				data: 'id_kelurahan='+$('#id_kelurahan').val()+'&date='+$('#tgl_mulai_sakit').val(),
				url: '{{ URL::to("ambil_epid_tetanus") }}',
				success:function(data) {
					$('#epid').val(data);
				}
			});
		});

		$( "#tgl_mulai_sakit" ).change(function() {
			$.ajax({
				data: 'id_kelurahan='+$('#id_kelurahan').val()+'&date='+$('#tgl_mulai_sakit').val(),
				url: '{{ URL::to("ambil_epid_tetanus") }}',
				success:function(data) {
					$('#epid').val(data);
				}
			});
		});
	});
	
	function pilihOptionTglLahir()
	{
		$('.tgl_lahir').removeAttr('readonly');
		$('#tgl_mulai_sakit').removeAttr('class');
		$('#tgl_tahun').attr('readonly','readonly');
		$('#tgl_bulan').attr('readonly','readonly');
		$('#tgl_hari').attr('readonly','readonly');
		$('.tgl_lahir').val('');
		$('#tgl_tahun').val('');
		$('#tgl_bulan').val('');
		$('#tgl_hari').val('');
	}

	function pilihOptionUmur()
	{
		$('#tgl_tahun').removeAttr('readonly');
		$('#tgl_bulan').removeAttr('readonly');
		$('#tgl_hari').removeAttr('readonly');
		$('#tgl_mulai_sakit').attr('class','input-medium tgl_mulai_sakit');
		$('.tgl_lahir').attr('readonly','readonly');
		$('.tgl_lahir').val('');
		$('#tgl_tahun').val('');
		$('#tgl_bulan').val('');
		$('#tgl_hari').val('');           
	}
</script>

@stop