<div class="module-body uk-overflow-container">
  <table class="table table-bordered display" id="example-pe_tetanus">
  <thead>
      <tr>
        <th>No. Epid</th>
        <th>Nama pasien</th>
        <th>Nama Orang Tua</th>
        <th>Jenis kelamin</th>
        <th>Umur</th>
        <th>Alamat</th>
        <th>action</th>
      </tr>
    </thead>
    <tbody>
    @if($pe_tetanus)
    @foreach($pe_tetanus as $row)
              <tr>
                  <td>{{$row->no_epid}}</td>
                  <td>{{$row->nama_anak}}</td>
                  <td>{{$row->nama_ortu}}</td>
                  <td>
                  @if($row->jenis_kelamin=='1')
                  Laki-laki
                  @elseif($row->jenis_kelamin=='2')
                  Perempuan
                  @elseif($row->jenis_kelamin=='3')
                  Tidak jelas
                  @else
                  -
                  @endif   
                  </td>      
                  <td>{{$row->umur}} Th</td>
                  <td>{{$row->alamat}}</td>
                  @if(Sentry::getUser()->hak_akses==2 || Sentry::getUser()->hak_akses==3|| Sentry::getUser()->hak_akses==7)
                  <td>
                  <div class="btn-group" role="group">
                    <a href="{{URL::to('pe_tetanus_detail/'.$row->id)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
                    <a href="{{URL::to('pe_tetanus_edit/'.$row->id)}}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>
                    <a href="{{URL::to('pe_tetanus_hapus/'.$row->id)}}" onclick="return confirm('apakah yakin ingin menghapus data ini?');return false;" class="btn btn-xs btn-danger" data-uk-tooltip title="hapus data"><i class="icon icon-remove"></i></a>
                  </div>
                  </td>
                  @elseif(Sentry::getUser()->hak_akses==4)
                  <td>
                  <div class="btn-group" role="group">
                    <a href="{{URL::to('pe_tetanus_detail/'.$row->id)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
                    <a href="{{URL::to('pe_tetanus_edit/'.$row->id)}}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>
                   </div>
                  </td>
                  @elseif(Sentry::getUser()->hak_akses==1 || Sentry::getUser()->hak_akses==6)
                  <td>
                  <div class="btn-group" role="group">
                    <a href="{{URL::to('pe_tetanus_detail/'.$row->id)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
                  </div>
                  </td>
                  @elseif(Sentry::getUser()->hak_akses==5)

                  @endif
              </tr>
          @endforeach
      @endif
    </tbody>
  </table>
</div>
<script>
  $(document).ready(function() {
      $("select").select2();
      $("#example-pe_tetanus").dataTable();
    });
</script>