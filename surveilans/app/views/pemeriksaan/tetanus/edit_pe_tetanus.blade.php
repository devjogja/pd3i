@extends('layouts.master')
@section('content')
<!-- awal class span12 -->
<div class="span12">
    <!-- awal class content -->
    <div class="content">
        <!-- awal class module -->
        <div class="module">
            <!-- awal class module-body -->
            <div class="module-body">
                <div class="profile-head media">
                    <h4>
                        Penyelidikan Penyakit Tetanus Neonatorum
                    </h4>
                    <hr>
                </div>
                <!-- awal form -->
                {{
                    Form::open
                    (
                        array
                        (
                            'url'=>'pe_update_tetanus',
                            'class'=>'form-horizontal'
                        )
                    )
                }}

                <!-- awal input kolom identitas pelapor -->
                @foreach($tetanus as $row)
                <fieldset>
                    <legend>Identitas pelapor</legend>
                    
                    <div class="control-group">
                        <label class="control-label">Sumber laporan</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'sumber_laporan',
                                    [
                                        ''=>'Pilih',
                                        '0'=>'Rumah sakit',
                                        '1'=>'Puskesmas',
                                        '2'=>'Praktek swasta',
                                        '3'=>'lainnya (sebutkan)'
                                    ],
                                    [$row->sumber_laporan], 
                                    array
                                    (
                                        'class' => 'input-medium',
                                        'id'=> 'id_sumber',
                                        'onchange'=>'sumberLaporan()'
                                    )
                                )
                            }}
                            Sebutkan : <input type="text" value="{{$row->nama_sumber_laporan}}" name="nama_sumber_laporan" id="nama_sumber_laporan">
                            
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Tanggal laporan diterima</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'tanggal_laporan_diterima',
                                    Helper::getDate($row->tanggal_laporan_diterima), 
                                    array
                                    (
                                        'class' => 'input-medium',
                                        'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Tanggal pelacakan</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'tanggal_pelacakan',
                                    Helper::getDate($row->tanggal_pelacakan), 
                                    array
                                    (
                                        'class' => 'input-medium',
                                        'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>
                </fieldset>
                <!-- akhir input kolom identitas pelapor -->
                <fieldset>
                <legend>Identitas bayi</legend>
                    <div class="control-group">
                        <label class="control-label">No. epidemologi</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'no_epid',
                                    $row->no_epid,
                                    array
                                    (
                                        'class'=>'input-medium no_epid'
                                    )
                                )
                            }}
                            {{
                                Form::hidden
                                (
                                    'id',
                                    $row->id
                                )
                            }}
                        </div>
                    </div>

                  <div class="control-group">
                    <label class="control-label">Nama bayi</label>
                    <div class="controls">
                      {{Form::text('nama_anak',$row->nama_anak, array('class' => 'input-medium'))}}
                      <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Jenis kelamin</label>
                    <div class="controls">
                      {{Form::select('jenis_kelamin',[null => 'Tidak diisi','1'=>'Laki-laki','2'=>'Perempuan','3'=>'Tidak Jelas'],[$row->jenis_kelamin], array('class' => 'input-medium'))}}
                      <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Tanggal lahir</label>
                    <div class="controls">
                      {{Form::text('tanggal_lahir',Helper::getDate($row->tanggal_lahir), array('class' => 'input-medium tanggal_lahir','placeholder'=>'Tanggal lahir','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','onchange'=>'umur_pe()'))}}
                      <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <div class="controls">
                    <input type="text" class="input-mini" value="{{$row->umur_hr}}" name="tgl_hari" id="tanggal_hari">
                    Hr
                    </div>
                  </div>                        
                  <div class="control-group">
                    <label class="control-label">Anak ke</label>
                    <div class="controls">
                      {{Form::text('anak_ke',$row->anak_ke, array('class' => 'input-medium'))}}
                      <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Nama ayah</label>
                    <div class="controls">
                      {{Form::text('nama_ayah',$row->nama_ayah, array('class' => 'input-medium'))}}
                      <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Umur ayah</label>
                    <div class="controls">
                      {{Form::text('umur_ayah',$row->umur_ayah, array('class' => 'input-mini'))}}
                      <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Tahun pendidikan ayah</label>
                    <div class="controls">
                      {{Form::text('th_pendidikan_ayah',$row->th_pendidikan_ayah, array('class' => 'input-mini'))}}
                      <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">pekerjaan ayah</label>
                    <div class="controls">
                      {{Form::text('pekerjaan_ayah',$row->pekerjaan_ayah, array('class' => 'input-medium'))}}
                      <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Nama ibu</label>
                    <div class="controls">
                      {{Form::text('nama_ibu',$row->nama_ibu, array('class' => 'input-medium'))}}
                      <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Tahun pendidikan ibu</label>
                    <div class="controls">
                      {{Form::text('th_pendidikan_ibu',$row->th_pendidikan_ibu, array('class' => 'input-mini'))}}
                      <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">pekerjaan ibu</label>
                    <div class="controls">
                      {{Form::text('pekerjaan_ibu',$row->pekerjaan_ibu, array('class' => 'input-medium'))}}
                      <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Provinsi</label>
                    <div class="controls">
                    <select name=""  id="id_provinsi" onchange="showKabupaten()">
                    <?php $pro=Provinsi::lists('provinsi','id_provinsi'); 
                     foreach ($pro as $id_provinsi => $provinsi) {
                        if($provinsi==$row->provinsi) {
                          ?>
                          <option value="{{$id_provinsi}}" selected="selected">{{$provinsi}}</option>
                          <?php
                        } else {
                        ?>
                          <option value="{{$id_provinsi}}">{{$provinsi}}</option>
                        <?php
                        }
                      } 
                    ?>
                    
                      </select>
                    <span class="help-inline"></span>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Kabupaten/Kota</label>
                      <div class="controls">
                        <select  id="id_kabupaten" onchange="showKecamatan()">
                          <option value="">{{$row->kabupaten}}</option>
                        </select>
                        <span class="help-inline"></span>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Kecamatan</label>
                      <div class="controls">
                        <select name="" id="id_kecamatan" onchange="showKelurahan()">
                          <option value="">{{$row->kecamatan}}</option>
                        </select>
                        <span class="help-inline"></span>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Kelurahan/Desa</label>
                      <div class="controls">
                        <select name="id_kelurahan"  id="id_kelurahan" onchange="showEpid()">
                          <option value="{{$row->id_kelurahan}}">{{$row->kelurahan}}</option>
                        </select>
                        <span class="help-inline"></span>
                      </div>
                    </div>
                  <div class="control-group">
                    <label class="control-label">Alamat</label>
                    <div class="controls">
                      {{Form::text('alamat',$row->alamat,array('class'=>'input-xxlarge','placeholder'=>'Jalan, RT/RW, Blok, Pemukiman, Dusun'))}}
                      <span class="help-inline"></span>
                    </div>
                  </div>
                  </fieldset>
                  <fieldset>
                  <legend>Informasi Riwayat Kesakitan/Kematian Bayi 3-28 Hari</legend>
                  <div class="control-group">
                    <label class="control-label">Nama yang diwawancarai</label>
                    <div class="controls">
                      {{Form::text('nama_yang_diwawancarai',$row->nama_yang_diwawancarai,array('class'=>'input-medium'))}}
                      <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Hubungan dengan keluarga bayi</label>
                    <div class="controls">
                      {{Form::text('hubungan_keluarga',$row->hubungan_keluarga,array('class'=>'input-medium'))}}
                      <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Bayi lahir hidup</label>
                    <div class="controls">
                      {{Form::select('bayi_hidup',[''=>'Pilih','0'=>'Ya','1'=>'Tidak'],[$row->bayi_hidup],array('class'=>'id_combobox input-medium'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Tanggal lahir bayi</label>
                    <div class="controls">
                    {{Form::text('tgl_lahir',Helper::getDate($row->tgl_lahir), array('class' => 'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Tanggal mulai sakit</label>
                    <div class="controls">
                    {{Form::text('tanggal_mulai_sakit',Helper::getDate($row->tanggal_mulai_sakit), array('class' => 'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Apakah bayi meninggal</label>
                    <div class="controls">
                    {{Form::select('bayi_meninggal',['' => 'Pilih','1'=>'Ya','2'=>'Tidak'],[$row->bayi_meninggal], array('class' => 'input-medium'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                  <label class="control-label">Tanggal meninggal</label>
                    <div class="controls">
                    {{Form::text('tanggal_meninggal',Helper::getDate($row->tanggal_meninggal), array('class' => 'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
                    {{Form::text('umur',$row->umur, array('class' => 'input-mini'))}} umur(hari)
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Waktu lahir apakah bayi menangis</label>
                    <div class="controls">
                    {{Form::select('bayi_menangis',[''=>'Pilih','0'=>'Ya','1'=>'Tidak','2'=>'Tidak tahu'],[$row->bayi_menangis],array('class' => 'input-medium'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Apakah terlihat tanda-tanda kehidupan lain dari bayi (mis. Sedikit gerakan)</label>
                    <div class="controls">
                    {{Form::select('tanda_kehidupan_lain',[''=>'Pilih','0'=>'Ya','1'=>'Tidak','2'=>'Tidak tahu'],[$row->tanda_kehidupan_lain],array('class' => 'input-medium'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Setelah lahir apakah bayi bisa menetek atau menghisap susu botol dengan baik</label>
                    <div class="controls">
                    {{Form::select('bayi_menetek',[''=>'Pilih','0'=>'Ya','1'=>'Tidak','2'=>'Tidak tahu'],[$row->bayi_menetek],array('class' => 'input-medium'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Apakah 3 hari kemudian tiba-tiba mulut bayi mencucu dan tidak bisa menetek?</label>
                    <div class="controls">
                    {{Form::select('bayi_mencucu',[''=>'Pilih','0'=>'Ya','1'=>'Tidak','2'=>'Tidak tahu'],[$row->bayi_mencucu],array('class' => 'input-medium'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Apakah bayi mudah kejang jika disentuh/terkena sinar atau bunyi?</label>
                    <div class="controls">
                    {{Form::select('bayi_kejang',[''=>'Pilih','0'=>'Ya','1'=>'Tidak','2'=>'Tidak tahu'],[$row->bayi_kejang],array('class' => 'input-medium'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Bayi dirawat</label>
                    <div class="controls">
                    {{Form::select('dirawat',[''=>'Pilih','0' => 'Ya','1'=>'Tidak'],[$row->dirawat], array('class' => 'input-small'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Dimana RS/Puskesmas</label>
                    <div class="controls">
                    {{Form::select('tempat_berobat',[''=>'Pilih','0' => 'Rumah sakit','1'=>'Puskesmas','2'=>'Dokter praktek swasta','3'=>'Perawat/mantri/bidan','4'=>'Tidak berobat'],[$row->tempat_berobat], array('class' => 'input-medium'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Tanggal mulai dirawat</label>
                    <div class="controls">
                    {{Form::text('tanggal_dirawat',Helper::getDate($row->tanggal_dirawat), array('class' => 'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Keadaan bayi setelah dirawat</label>
                    <div class="controls">
                    {{Form::select('keadaan_setelah_dirawat',[''=>'Pilih','0' => 'Sembuh','1'=>'Tidak sembuh','2'=>'Meninggal'],[$row->keadaan_setelah_dirawat], array('class' => 'input-medium'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  </fieldset>
                  <fieldset>
                  <legend>Riwayat kehamilan</legend>
                  <div class="control-group">
                    <label class="control-label">Petugas pemeriksa</label>
                    <div class="controls">
                      <table class="table table-bordered petugas_kehamilan">
                        
                      </table>
                      <br/>
                      <div id="R_kehamilan">
                      <table class="table">
                        <tr>
                          <td>Nama</td>
                          <td><input type="text" class="nama_petugas_kehamilan"></td>
                        </tr>
                        <tr>
                            <td>Profesi</td>
                            <td>
                            <select id="profesi">
                                <option value="Dokter">Dokter</option>
                                <option value="Bidan/perawat">Bidan/perawat</option>
                                <option value="Dukun">Dukun</option>
                                <option value="Lainnya">Lainnya</option>
                            </select>
                            </td>
                        </tr>
                        <tr>
                          <td>Alamat</td>
                          <td><input type="text" class="input-xxlarge alamat_petugas_kehamilan"></td>
                        </tr>
                        <tr>
                          <td>Frekuensi</td>
                          <td><input type="hidden" class="id_petugas_kehamilan"><input type="text" class="frekuensi"></td>
                        </tr>
                      </table>
                      <br/>
                      <a class="btn tutup_petugas_kehamilan">Tutup</a>&nbsp;
                      <button class="btn btn-success simpan_petugas_kehamilan">Tambah petugas</button>
                      <button class="btn btn-success edit_petugas_kehamilan">edit petugas</button>
                      </div>
                      <a class="btn show_petugas_kehamilan">Tampilkan untuk tambah petugas</a>
                    </div>
                  </div>  
                  <div class="control-group">
                    <label class="control-label">Apakah ibu pernah mendapat imunisasi TT pada waktu hamil bayi ini?</label>
                    <div class="controls">
                    {{Form::select('mendapat_imunisasi', [''=>'Pilih','0' => 'Ya','1'=>'Tidak'],[$row->mendapat_imunisasi], array('class' => 'input-small'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Sumber informasi imunisasi TT </label>
                    <div class="controls">
                    {{Form::select('info_imunisasi', [''=>'Pilih','0' => 'Ingatan','1'=>'Buku catatan'],[$row->info_imunisasi], array('class' => 'input-small'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Berapa kali mendapat imunisasi TT pada saat kehamilan bayi tersebut?</label>
                    <div class="controls">
                    {{Form::select('jumlah_imunisasi',[''=>'Pilih','0' => 'Pertama kali','1'=>'Kedua kali'],[$row->jumlah_imunisasi], array('class' => 'input-small'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Umur kehamilan saat mendapat imunisasi TT pertama (bulan)</label>
                    <div class="controls">
                      {{Form::text('umur_kehamilan_pertama',$row->umur_kehamilan_pertama,array('class'=>'input-mini'))}}
                      <span class="help-inline"></span>
                    </div>
                  </div>
                  
                  <div class="control-group">
                    <label class="control-label">Umur kehamilan saat mendapat imunisasi TT kedua (bulan)</label>
                    <div class="controls">
                      {{Form::text('umur_kehamilan_kedua',$row->umur_kehamilan_kedua,array('class'=>'input-mini'))}}
                      <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Tanggal imunisasi kedua saat kehamilan bayi tersebut?</label>
                    <div class="controls">
                      {{Form::text('tanggal_imunisasi_kedua',Helper::getDate($row->tanggal_imunisasi_kedua),array('class'=>'input-small','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
                      <span class="help-inline"></span>
                    </div>
                  </div>  
                  
                  
                  <div class="control-group">
                    <label class="control-label">Pernahkah ibu mendapat suntikan TT calon pengantin</label>
                    <div class="controls">
                    {{Form::select('ibu_mendapat_suntikan_pengantin', [''=>'Pilih','0' => 'Ya','1'=>'Tidak'],[$row->ibu_mendapat_suntikan_pengantin], array('class' => 'input-small'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  
                  <div class="control-group">
                    <label class="control-label">Status TT ibu pada saat kehamilan bayi tersebut</label>
                    <div class="controls">
                      {{Form::select('status_ibu_saat_hamil',[''=>'Pilih','0' => 'TT1','1'=>'TT2','2'=>'TT3','3'=>'TT4','4'=>'TT5'],[$row->status_ibu_saat_hamil], array('class' => 'input-medium'))}}
                      <span class="help-inline"></span>
                    </div>
                  </div>                        
                  </fieldset>
                  <fieldset>
                  <legend>Riwayat persalinan</legend>
                  <!-- <div class="control-group">
                    <label class="control-label">Petugas persalinan</label>
                    <div class="controls">
                  <table class="table table-bordered petugas_persalinan">
                    

                  </table>
                  <br/>
                  <div id="R_persalinan">
                  <table class="table">
                    <tr>
                      <td>Nama</td>
                      <td><input type="text" class="nama"></td>
                    </tr>
                    <tr>
                      <td>Profesi</td>
                      <td><input type="text" class="profesi"></td>
                    </tr>
                    <tr>
                      <td>Alamat</td>
                      <td><input type="text" class="input-xxlarge alamat_petugas_persalinan"></td>
                    </tr>
                    <tr>
                      <td>Tempat persalinan</td>
                      <td><input type="hidden" class="id"><input type="text" class="tempat_persalinan"></td>
                    </tr>
                  </table>
                  <br/>
                  <a class="btn tutup">Tutup</a>&nbsp;
                  <button class="btn btn-success simpan_petugas">Tambah petugas</button>
                  <button class="btn btn-success edit_petugas">edit petugas</button>
                  </div>
                  <a class="btn show">Tampilkan untuk tambah petugas</a>
                  </div>
                  </div>
                   -->
                  
                  <div class="control-group">
                    <label class="control-label">Obat yang dibubuhkan setelah tali pusat di potong</label>
                    <div class="controls">
                    {{Form::select('obat_penyembuh', [''=>'pilih','0' => 'Alkohol/lod','1'=>'Ramuan tradisional','2'=>'Lainnya'], [$row->obat_penyembuh], array('class' => 'input-medium'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                  <label class="control-label">Jika jawaban ramuan tradisional atau lainnya, sebutkan : </label>
                    <div class="controls">
                     {{Form::text('ramuan_lainnya',$row->ramuan_lainnya,array('class'=>'input-xlarge'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Yang melakukan perawatan tali pusat awal-akhir</label>
                    <div class="controls">
                    {{Form::select('perawatan_tali_pusat',[''=>'pilih','0' => 'Tenaga kerja','1'=>'Bukan tenaga kerja'], [$row->perawatan_tali_pusat], array('class' => 'input-medium'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                  <label class="control-label">Obat atau ramuan apa saja yang dibubuhkan selama merawat tali pusat, sebutkan</label>
                    <div class="controls">
                        {{
                            Form::select
                            (
                                'obat_merawat_tali_pusat',
                                [
                                    ''=>'pilih',
                                    '0' => 'Alkohol/lod',
                                    '1'=>'Ramuan tradisional',
                                    '2'=>'Kasa kering',
                                    '3'=>'Lainnya'
                                ], 
                                [$row->obat_merawat_tali_pusat], 
                                array
                                (
                                    'class' => 'input-medium'
                                )
                            )
                        }}
                        <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Kesimpulan diagnosis</label>
                    <div class="controls">
                    {{Form::select('kesimpulan_diagnosis', [''=>'pilih','0' => 'Konfirm TN','1'=>'Tersangka TN','2'=>'Bukan TN'], [$row->kesimpulan_diagnosis], array('class' => 'input-medium'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Cakupan imunisasi TT di desa kasus TN</label>
                    <div class="controls">
                    <table>
                        <tr>
                          <td>
                          TT1 (%)
                          </td>
                          <td>
                          {{Form::text('TT1',$row->TT1,array('class'=>'input-mini'))}}
                          </td>
                        </tr>
                        <tr>
                          <td>
                          TT2 (%)
                          </td>
                          <td>
                          {{Form::text('TT2',$row->TT2,array('class'=>'input-mini'))}}
                          </td>
                        </tr>
                        <tr>
                          <td>
                          TT3 (%)
                          </td>
                          <td>
                          {{Form::text('TT3',$row->TT3,array('class'=>'input-mini'))}}
                          </td>
                        </tr>
                        <tr>
                          <td>
                          TT4 (%)
                          </td>
                          <td>
                          {{Form::text('TT4',$row->TT4,array('class'=>'input-mini'))}}
                          </td>
                        </tr>
                        <tr>
                          <td>
                          TT5 (%)
                          </td>
                          <td> 
                          {{Form::text('TT5',$row->TT5,array('class'=>'input-mini'))}}
                          </td>
                        </tr>
                      </table>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Cakupan persalinan tenaga kesehatan (%)</label>
                    <div class="controls">
                    {{Form::text('cakupan_tenaga_kesehatan',$row->cakupan_tenaga_kesehatan, array('class' => 'input-mini'))}}
                    <span class="help-inline"></span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Cakupan kunjungan neonatus</label>
                    <div class="controls">
                    <table>
                        <tr>
                          <td>
                          KN1 (%)
                          </td>
                          <td>
                          {{Form::text('KN1',$row->KN1,array('class'=>'input-mini'))}}
                          </td>
                        </tr>
                        <tr>
                          <td>
                          KN2 (%)
                          </td>
                          <td>
                          {{Form::text('KN2',$row->KN2,array('class'=>'input-mini'))}}
                          </td>
                        </tr>
                      </table>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Tim pelacak</label>
                    <div class="controls">
                    <table class="table table-bordered petugas_pelacak">
                    <tr>
                      <td>No.</td>
                      <td>Nama</td>
                      <td>Jabatan</td>
                      <td>aksi</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>doktr</td>
                      <td>oaks</td>
                      <td><a href="" class="btn btn-warning">edit</a>
                      <a href="" class="btn btn-danger">hapus</a></td>
                    </tr>
                  </table>
                  </br>
                  <div id="R_pelacak">
                  <table class="table">
                    <tr>
                      <td>Nama</td>
                      <td><input type="text" class="nama_pelacak"></td>
                    </tr>
                    <tr>
                      <td>Jabatan</td>
                      <td><input type="hidden" class="id"><input type="text" class="profesi_pelacak"></td>
                    </tr>
                  </table>
                  <br/>
                  <a class="btn tutup_pelacak">Tutup</a>&nbsp;
                  <button class="btn btn-success simpan_pelacak">Tambah petugas</button>
                  <button class="btn btn-success edit_pelacak">edit petugas</button>
                  </div>
                  <a class="btn show_pelacak">Tampilkan untuk tambah petugas</a>
                  </div>
                  </div>
                  </fieldset>
                  <div class="form-actions">
                  {{Form::submit('simpan',array('class'=>'btn btn-primary'))}}
                  <a href="{{URL::to('tetanus')}}" class="btn btn-warning">Batal</a>
                  </div>
                  
                {{Form::close()}}
              <script>
              $(document).ready(function(){
                showKabupaten();
                showKecamatan();
                showKelurahan();
                if($('#id_sumber').val()!=3)
                {
                    $('#nama_sumber_laporan').attr('disabled','disabled');
                    $('#nama_sumber_laporan').val('');
                }
                $("select").select2();
                $('.edit_petugas').hide();
                $('.edit_petugas_kehamilan').hide();
                $('.edit_pelacak').hide();
                $('.petugas_persalinan').load('{{URL::to("petugas_persalinan/".$row->no_epid)}}');
                $('.petugas_kehamilan').load('{{URL::to("petugas_kehamilan/".$row->no_epid)}}');
                $('.petugas_pelacak').load('{{URL::to("petugas_pelacak/".$row->no_epid)}}');
                $("#R_persalinan").hide();
                $("#R_kehamilan").hide();
                $("#R_pelacak").hide();

                $(".show").click(function(e){
                  e.preventDefault();
                  $("#R_persalinan").show(1000);
                  $('.show').hide();
                });

                $(".show_pelacak").click(function(e){
                  e.preventDefault();
                  $("#R_pelacak").show(1000);
                  $('.show_pelacak').hide();
                });

                $(".show_petugas_kehamilan").click(function(e){
                  e.preventDefault();
                  $("#R_kehamilan").show(1000);
                  $('.show_petugas_kehamilan').hide();
                });

                $(".tutup").click(function(e){
                  e.preventDefault();
                  $("#R_persalinan").hide(2000);
                  $('.nama').val('');
                      $('.profesi').val('');
                      $('.alamat_petugas_persalinan').val('');
                      $('.tempat_persalinan').val('');
                  $('.show').show();
                  $('.edit_petugas').hide();
                  $('.simpan_petugas').show();
                });

                $(".tutup_petugas_kehamilan").click(function(e){
                  e.preventDefault();
                  $("#R_kehamilan").hide(2000);
                  $('.nama_petugas_kehamilan').val('');
                      $('.profesi_petugas_kehamilan').val('');
                      $('.alamat_petugas_kehamilan').val('');
                      $('.frekuensi').val('');
                  $('.show_petugas_kehamilan').show();
                  $('.edit_petugas_kehamilan').hide();
                  $('.simpan_petugas_kehamilan').show();
                });

                $(".tutup_pelacak").click(function(e){
                  e.preventDefault();
                  $("#R_pelacak").hide(2000);
                  $('.nama_pelacak').val('');
                      $('.profesi_pelacak').val('');;
                  $('.show_pelacak').show();
                  $('.edit_pelacak').hide();
                  $('.simpan_pelacak').show();
                });

                $(".simpan_petugas").click(function(e){
                  e.preventDefault();
                  $.ajax({
                    data:'nama='+$('.nama').val()+'&profesi='+$('.profesi').val()+'&alamat='+$('.alamat_petugas_persalinan').val()+'&t_persalinan='+$('.tempat_persalinan').val()+'&no_epid='+$('.no_epid').val(),
                      url:'{{URL::to("tambah_petugas_persalinan")}}',
                      success:function(data) {
                      $('.nama').val('');
                      $('.profesi').val('');
                      $('.alamat_petugas_persalinan').val('');
                      $('.tempat_persalinan').val('');
                      $("#R_persalinan").hide(2000);
                  $('.show').show(1000)
                  if(data=='sukses') 
                  {
                    $('.petugas_persalinan').load('{{URL::to("petugas_persalinan/".$row->no_epid)}}');
                    
                  }
                    }
                  });
                });

                $(".simpan_pelacak").click(function(e){
                  e.preventDefault();
                  $.ajax({
                    data:'nama='+$('.nama_pelacak').val()+'&profesi='+$('.profesi_pelacak').val()+'&no_epid='+$('.no_epid').val(),
                      url:'{{URL::to("tambah_pelacak")}}',
                      success:function(data) {
                      $('.nama_pelacak').val('');
                      $('.profesi_pelacak').val('');
                      $("#R_pelacak").hide(2000);
                  $('.show_pelacak').show(1000)
                  if(data=='sukses') 
                  {
                    $('.petugas_pelacak').load('{{URL::to("petugas_pelacak/".$row->no_epid)}}');
                    
                  }
                    }
                  });
                });

                $(".edit_petugas").click(function(e){
                  e.preventDefault();
                  $.ajax({
                    data:'id='+$('.id').val()+'&nama='+$('.nama').val()+'&profesi='+$('.profesi').val()+'&alamat='+$('.alamat_petugas_persalinan').val()+'&t_persalinan='+$('.tempat_persalinan').val()+'&id_pasien='+$('.id_pasien').val(),
                      url:'{{URL::to("edit_petugas_persalinan")}}',
                      success:function(data) {
                      $('.id').val('');
                      $('.nama').val('');
                      $('.profesi').val('');
                      $('.alamat_petugas_persalinan').val('');
                      $('.tempat_persalinan').val('');
                      $("#R_persalinan").hide(2000);
                  $('.show').show(1000)
                  if(data=='sukses') 
                  {
                    $('.petugas_persalinan').load('{{URL::to("petugas_persalinan/".$row->no_epid)}}');
                    $('.edit_petugas').hide();
                    $('.simpan_petugas').show();
                  }
                    }
                  });
                });

                $(".edit_pelacak").click(function(e){
                  e.preventDefault();
                  $.ajax({
                    data:'id='+$('.id').val()+'&nama='+$('.nama_pelacak').val()+'&profesi='+$('.profesi_pelacak').val(),
                      url:'{{URL::to("edit_pelacak")}}',
                      success:function(data) {
                      $('.no_epidlacak').val('');
                      $('.nama_pelacak').val('');
                      $('.profesi_pelacak').val('');
                      $("#R_pelacak").hide(2000);
                  $('.show_pelacak').show(1000)
                  if(data=='sukses') 
                  {
                    $('.petugas_pelacak').load('{{URL::to("petugas_pelacak/".$row->no_epid)}}');
                    $('.edit_pelacak').hide();
                    $('.simpan_pelacak').show();
                  }
                    }
                  });
                });

                $(".simpan_petugas_kehamilan").click(function(e){
                  e.preventDefault();
                  $.ajax({
                    data:'nama_petugas_kehamilan='+$('.nama_petugas_kehamilan').val()+'&profesi_petugas_kehamilan='+$('#profesi').val()+'&alamat_petugas_kehamilan='+$('.alamat_petugas_kehamilan').val()+'&frekuensi='+$('.frekuensi').val()+'&no_epid='+$('.no_epid').val(),
                      url:'{{URL::to("tambah_petugas_kehamilan")}}',
                      success:function(data) {
                      $('.nama_petugas_kehamilan').val('');
                      $('.profesi_petugas_kehamilan').val('');
                      $('.alamat_petugas_kehamilan').val('');
                      $('.frekuensi').val('');
                      $("#R_kehamilan").hide(2000);
                  $('.show_petugas_kehamilan').show(1000)
                  if(data=='sukses') 
                  {
                    $('.petugas_kehamilan').load('{{URL::to("petugas_kehamilan/".$row->no_epid)}}');
                    
                  }
                    }
                  });
                });

                $(".edit_petugas_kehamilan").click(function(e){
                  e.preventDefault();
                  $.ajax({
                    data:'id='+$('.id_petugas_kehamilan').val()+'&nama_petugas_kehamilan='+$('.nama_petugas_kehamilan').val()+'&profesi_petugas_kehamilan='+$('#profesi').val()+'&alamat_petugas_kehamilan='+$('.alamat_petugas_kehamilan').val()+'&frekuensi='+$('.frekuensi').val(),
                      url:'{{URL::to("edit_petugas_kehamilan")}}',
                      success:function(data) {
                      $('.id').val('');
                      $('.nama_petugas_kehamilan').val('');
                      $('#profesi').val('');
                      $('.alamat_petugas_kehamilan').val('');
                      $('.frekuensi').val('');
                      $("#R_kehamilan").hide(2000);
                  $('.show_petugas_kehamilan').show(1000)
                  if(data=='sukses') 
                  {
                    $('.petugas_kehamilan').load('{{URL::to("petugas_kehamilan/".$row->no_epid)}}');
                    $('.edit_petugas_kehamilan').hide();
                    $('.simpan_petugas_kehamilan').show();
                  }
                    }
                  });
                });

              });
                function showKabupaten(){
	              var provinsi = $("#id_provinsi").val();
	              var kabupaten = $("#id_kabupaten").val();
	              // //kirim data ke server
	              $('.lodingKab').html('<img src="{{URL::to("style/images/loding.gif")}}">');
	              $.post('{{URL::to("provinsi/getkab")}}', {provinsi:provinsi,kabupaten:kabupaten}, function(response)
	              {
	                  $('#id_kabupaten').removeAttr('disabled','');
	                  $("#id_kabupaten").html(response);
	                  $('.lodingKab').html('');
	              });

	            }

	            function showKecamatan(){
	              var kabupaten = $("#id_kabupaten").val();
	              var kecamatan = $("#id_kecamatan").val();
	              // //kirim data ke server
	              $('.lodingKec').html('<img src="{{URL::to("style/images/loding.gif")}}">');
	              $.post('{{URL::to("provinsi/getkec")}}', {kabupaten:kabupaten,kecamatan:kecamatan}, function(response)
	              {
	                  $('#id_kecamatan').removeAttr('disabled','');
	                  $("#id_kecamatan").html(response);
	                  $('.lodingKec').html('');
	              });

	            }

	            function showKelurahan(){
	              var kecamatan= $("#id_kecamatan").val();
	              var kelurahan = $("#id_kelurahan").val();
	              // //kirim data ke server
	              $('.lodingKel').html('<img src="{{URL::to("style/images/loding.gif")}}">');
	              $.post('{{URL::to("provinsi/getkel")}}', {kecamatan:kecamatan,kelurahan:kelurahan}, function(response)
	              { 
	                  $('#id_kelurahan').removeAttr('disabled','');
	                  $("#id_kelurahan").html(response);
	                  $('.lodingKel').html('');
	              });

	            }
            //fungsi edit petugas kehamilan pada modul PE tetanus
                function edit_petugas_kehamilan(id)
                {
                    $.ajax({
                        //dataType:'json',
                        data: 'id='+id,
                        url:'{{URL::to("tampil_edit_petugas_kehamilan")}}',
                        success:function(data) {
                            $('.id_petugas_kehamilan').val(data.id);
                            $('.nama_petugas_kehamilan').val(data.nama_pemeriksa);
                            $('#profesi').val(data.profesi);
                            $('.alamat_petugas_kehamilan').val(data.alamat);
                            $('.frekuensi').val(data.frekuensi);
                            $("#R_kehamilan").show(2000);
                            $('.show_petugas_kehamilan').hide()
                            $('.edit_petugas_kehamilan').show();
                            $('.simpan_petugas_kehamilan').hide();
                        }
                    });
                    return false;
                }

                //fungsi hapus petugas kehamilan pada modul PE tetanus
                function hapus_petugas_kehamilan(id)
                {
                    $.ajax({
                        //dataType:'json',
                        data: 'id='+id,
                        url:'{{URL::to("hapus_petugas_kehamilan")}}',
                        success:function(data) {
                            if(data=='sukses') 
                            {
                                $('.petugas_kehamilan').load('{{URL::to("petugas_kehamilan/".$row->no_epid)}}');
                                
                            }
                        }
                    });
                    return false;
                }

                //fungsi edit petugas pemeriksa pada modul PE tetanus
                function edit_petugas_persalinan(id)
                {
                    $.ajax({
                        //dataType:'json',
                        data: 'id_petugas_persalinan='+id,
                        url:'{{URL::to("tampil_edit_petugas_persalinan")}}',
                        success:function(data) {
                            $('.id').val(data.id);
                            $('.nama').val(data.nama_pemeriksa);
                            $('.profesi').val(data.profesi);
                            $('.alamat_petugas_persalinan').val(data.alamat);
                            $('.tempat_persalinan').val(data.tempat_persalinan);
                            $("#R_persalinan").show(2000);
                            $('.show').hide()
                            $('.edit_petugas').show();
                            $('.simpan_petugas').hide();
                        }
                    });
                    return false;
                }

                //fungsi edit petugas pelacak pada modul PE tetanus
                function edit_pelacak(id)
                {
                    $.ajax({
                        //dataType:'json',
                        data: 'id='+id,
                        url:'{{URL::to("tampil_edit_petugas_pelacak")}}',
                        success:function(data) {
                            $('.id').val(data.id);
                            $('.nama_pelacak').val(data.nama_pemeriksa);
                            $('.profesi_pelacak').val(data.profesi);
                            $("#R_pelacak").show(2000);
                            $('.show_pelacak').hide()
                            $('.edit_pelacak').show();
                            $('.simpan_pelacak').hide();
                        }
                    });
                    return false;
                }

                //fungsi hapus petugas pemeriksa pada modul PE tetanus
                function hapus_petugas_persalinan(id)
                {
                    $.ajax({
                        //dataType:'json',
                        data: 'id='+id,
                        url:'{{URL::to("hapus_petugas_persalinan")}}',
                        success:function(data) {
                            if(data=='sukses') 
                            {
                                $('.petugas_persalinan').load('{{URL::to("petugas_persalinan/".$row->no_epid)}}');
                                
                            }
                        }
                    });
                    return false;
                }

                //fungsi hapus petugas pemeriksa pada modul PE tetanus
                function hapus_pelacak(id)
                {
                    $.ajax({
                        //dataType:'json',
                        data: 'id='+id,
                        url:'{{URL::to("hapus_pelacak")}}',
                        success:function(data) {
                            if(data=='sukses') 
                            {
                                $('.petugas_pelacak').load('{{URL::to("petugas_pelacak/".$row->no_epid)}}');
                                
                            }
                        }
                    });
                    return false;
                }

                //fungsi untuk memilih sumber laporan
                function sumberLaporan()
                {
                    
                    var id = $('#id_sumber').val();
                    if (id==3) {
                        $('#nama_sumber_laporan').removeAttr('disabled','');
                    }else{
                        $('#nama_sumber_laporan').attr('disabled','disabled');
                    }
                }
            </script>
            @endforeach
          </div>
      </div>
  </div>
</div>
</div>
</div>
</div>
</div>
@stop