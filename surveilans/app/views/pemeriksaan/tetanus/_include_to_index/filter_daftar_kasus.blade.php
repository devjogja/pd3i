<div class="module">
<!--
<div class="module-head">
	<h3>Filter</h3>
</div>
-->
<div class="module-body">
	<form id="form-filter-daftar-kasus" method="get" action="{{ route('tetanus.filter-daftar-kasus') }}">
		<div class="chart inline-legend grid">
			<table class="table" style="width:100%;">

		{{-- Row 1 --}}
				<tr>
					<td style="text-align:right;width:10%;">
						Rentang Waktu
					</td>
					<td style="width:25%;">
						<select name="unit" class="unit" style="width: 90px;" onkeypress="focusNext('day_start', 'clinic_id', this, event)" onchange="setDisable(this, event)">
							<option value="all">All</option>
							<option value="day">Hari</option>
							<option value="month">Bulan</option>
							<option value="year">Tahun</option>
						</select>
					</td>

			{{-- Provinsi --}}
					<td style="text-align:right;width:15%;">
						Provinsi
					</td>
					<td>
						<select name="province_id" id="province_id" style="width:200px" onchange="get_district();" >
						<option value="">--- Pilih ---</option>
						<?php
							$q = "
								SELECT
									id_provinsi,
									provinsi
								FROM provinsi
								ORDER BY provinsi ASC
							";
							$combo_district=DB::select($q);
						?>
						@for($i=0;$i<count($combo_district);$i++)
							<?php //if($combo_district[$i]->id_provinsi == "34") $sel='selected'; else $sel='';?>
							<option value="{{ $combo_district[$i]->id_provinsi }}" <?php //echo $sel; ?> >
								{{ $combo_district[$i]->provinsi; }}
							</option>
						@endfor
						</select>
					</td>
				</tr>

		{{-- Row 2 --}}
				<tr>

			{{-- Tanggal Awal --}}
					<td style="text-align:right;">
						Sejak
					</td>
					<td>
						<?php
							$start['mktime'] = strtotime("-1 day");
							$start['day']    = date("j", $start['mktime']);
							$start['month']  = date("n", $start['mktime']);
							$start['year']   = date("Y", $start['mktime']);

							$now['day']        = date("j");
							$now['month']      = date("n");
							$now['year']       = date("Y");
							$now['year_start'] = 1971;

							$arr_nama_bulan[1]  = "Januari";
							$arr_nama_bulan[2]  = "Februari";
							$arr_nama_bulan[3]  = "Maret";
							$arr_nama_bulan[4]  = "April";
							$arr_nama_bulan[5]  = "Mei";
							$arr_nama_bulan[6]  = "Juni";
							$arr_nama_bulan[7]  = "Juli";
							$arr_nama_bulan[8]  = "Agustus";
							$arr_nama_bulan[9]  = "September";
							$arr_nama_bulan[10] = "Oktober";
							$arr_nama_bulan[11] = "November";
							$arr_nama_bulan[12] = "Desember";
						?>

					{{-- Tanggal --}}
						<select name="day_start" class="day_start" style="width: 50px;" onkeypress="focusNext( 'month_start', 'unit', this, event)">
							@for($i=1; $i<32; $i++) :
								@if($i == $start['day'])
									<?php $sel = "selected"; ?>
								@else
									<?php $sel = "" ?>
								@endif

								<option value="{{ $i }}" {{ $sel }} >
									{{ $i }}
								</option>
							@endfor
						</select>

					{{-- Bulan --}}
						<select name="month_start" class="month_start" style="width: 100px;" onkeypress="focusNext( 'year_start', 'day_start', this, event)">
							@for($i=1; $i<13; $i++) :

								//$bln = tambahNol($i, 2);
								@if($i == $start['month'])
									<?php $sel = "selected"; ?>
								@else
									<?php $sel = ""; ?>
								@endif

								<option value="{{ $i }}" {{ $sel }} >
									{{ $arr_nama_bulan[$i] }}
								</option>
							@endfor
						</select>

					{{-- Tahun --}}
						<select name="year_start" class="year_start inputan" style="width: 70px;" onkeypress="focusNext( 'day_end', 'month_start', this, event)">
							@for($i=$now['year_start']; $i<=$now['year']; $i++)
								@if($i==$start['year'])
									<?php $sel = "selected" ?>
								@else
									<?php $sel = ""; ?>
								@endif

								<option value="{{ $i }}" {{ $sel }}>
									{{ $i }}
								</option>
							@endfor;
						</select>
					</td>

			{{-- Kabupaten --}}
					<td style="text-align:right;">
						Kabupaten
					</td>
					<td>
						<select name="district_id" id="district_id" style="width: 190px;" disabled="disabled"  onchange="get_sub_district();">

						</select>
					</td>
				</tr>

		{{-- Row 3 --}}
				<tr>

			{{-- Tanggal Akhir --}}
					<td style="text-align:right;">
						Sampai
					</td>

					<td>
				{{-- Tanggal --}}
						<select name="day_end" class="day_end" style="width: 50px;" >
							@for($i=1; $i<32; $i++)
								@if($i == $now['day'])
									<?php $sel = "selected"; ?>
								@else
									<?php $sel = "" ?>
								@endif

								<option value="{{ $i }}" {{ $sel }} >
									{{ $i }}
								</option>
							@endfor
						</select>

				{{-- Bulan --}}
						<select name="month_end" class="month_end" style="width: 100px;" >
							@for($i=1;$i<13;$i++)
								@if($i==$now['month'])
									<?php $sel = "selected" ?>
								@else
									<?php $sel = ""; ?>
								@endif

								<option value="{{ $i }}" {{ $sel }} >
									{{ $arr_nama_bulan[$i] }}
								</option>
							@endfor
						</select>

				{{-- Tahun --}}
						<select name="year_end" class="year_end inputan" style="width: 70px;">
							@for($i=$now['year_start']; $i<=$now['year']; $i++)
								@if($i==$now['year'])
									<?php $sel = "selected"; ?>
								@else
									<?php $sel = ""; ?>
								@endif

								<option value="{{ $i }}" {{ $sel }} >
									{{ $i }}
								</option>
							@endfor
						</select>
					</td>

			{{-- Kecamatan --}}
					<td style="text-align:right;">
						Kecamatan
					</td>
					<td>
						<select name="sub_district_id" id="sub_district_id" style="width: 190px;" disabled="disabled"  onchange="get_village();">

						</select>
					</td>
				</tr>

	{{-- Row 4 --}}
				<tr>
					<td style="text-align:right;">

					</td>
					<td>

					</td>
					<td style="text-align:right;">
						Puskesmas
					</td>
					<td>
						<select name="puskesmas_id" id="puskesmas_id" style="width: 190px;" disabled="disabled" >

						</select>
					</td>
				</tr>
				<tr>
					<td style="text-align:right;">

					</td>
					<td>

					</td>
					<td style="text-align:right;">
						<!--Desa-->
					</td>
					<td><!--
						<select name="village_id" id="village_id" style="width: 190px;" disabled="disabled" >

						</select>
						-->
					</td>
				</tr>
			</table>
			<center>
				<input id="filter_daftar_kasus" type="submit" value="Tampilkan" class="btn btn-success">
				<a href="{{-- URL::to('export/'.strtolower(Session::get('penyakit')).'/xls') --}}" class="btn btn-success">Export Excel</a>
			</center>
		</div>
	</form>
</div>
</div>