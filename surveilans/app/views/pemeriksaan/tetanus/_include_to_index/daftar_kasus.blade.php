<div class="module-body uk-overflow-container">
  <div class="table-responsive">
    <table class="table table-bordered display" id="example-tetanus">
       <thead>
        <tr>
          <th>No. Epid</th>
          <th>Nama pasien</th>
          <th>Usia</th>
          <th>Alamat</th>
          <th>Keadaan Akhir</th>
          <th>Klasifikasi akhir</th>
          @if(Sentry::getUser()->hak_akses==2 || Sentry::getUser()->hak_akses==7)
            <th>PE</th>
          @else
            <th>Nama Faskes</th>
          @endif
          <th style="text-align:center">action</th>
        </tr>
      </thead>

      <tbody>

      <?php $data = Tetanus::pasientetanus(); ?>

        @if($data)
          @foreach($data as $row)
            <tr>
              <td>{{ $row->no_epid }}</td>
              <td>{{ $row->nama_anak }}</td>
              @if($row->umur || $row->umur_bln || $row->umur_hr)
                <td>{{ $row->umur.' Th '.$row->umur_bln.' bln '.$row->umur_hr.' hr' }}</td>
              @else
                <td>-</td>
              @endif
              <td>{{ $row->alamat }}</td>
              <td>{{ $row->keadaan_akhir_nm }}</td>
              <td>{{ $row->klasifikasi_akhir_nm }}</td>
                @if(Sentry::getUser()->hak_akses == 2 || Sentry::getUser()->hak_akses == 7)
                <td>
                <?php
                  if(Tetanus::checkPE($row->no_epid))
                  {
                      echo '<a class="btn btn-danger">Sudah PE</a>';
                  }
                  else
                  {
                ?>
                  <a href="{{ URL::to('tetanus/entritetanus/'.$row->id_tetanus) }}" data-uk-tooltip title="Penyelidikan epidemologi" class="btn btn-primary">PE</a>
                  <?php
                  }
                ?>
                </td>
                @else
                  <td>{{$row->nama_faskes}}</td>
                @endif
                <td>
                @if(Sentry::getUser()->hak_akses == 2 || Sentry::getUser()->hak_akses == 3 || Sentry::getUser()->hak_akses==7)
                  <div class="btn-group" role="group">
                    <a href="{{ URL::to('tetanus_detail/'.$row->id_tetanus) }}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
                    <a href="{{URL::to('tetanus_edit/'.$row->id_tetanus)}}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>
                    <a href="{{ URL::to('tetanus_hapus/'.$row->id_tetanus.'/'.$row->id_pasien_tetanus) }}" onclick="return confirm('apakah yakin ingin menghapus data ini?');return false;" class="btn btn-xs btn-danger" data-uk-tooltip title="hapus data"><i class="icon icon-remove"></i></a>
                  </div>
                @elseif(Sentry::getUser()->hak_akses == 4)
                  <div class="btn-group" role="group">
                    <a href="{{ URL::to('tetanus_detail/'.$row->id_tetanus) }}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
                    <a href="{{ URL::to('tetanus_edit/'.$row->id_tetanus) }}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>
                  </div>
                @elseif(Sentry::getUser()->hak_akses == 1 || Sentry::getUser()->hak_akses == 6)
                  <div class="btn-group" role="group">
                    <a href="{{ URL::to('tetanus_detail/'.$row->id_tetanus) }}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
                    <a href="{{ URL::to('tetanus_hapus/'.$row->id_tetanus.'/'.$row->id_pasien_tetanus) }}" onclick="return confirm('apakah yakin ingin menghapus data ini?');return false;" class="btn btn-xs btn-danger" data-uk-tooltip title="hapus data"><i class="icon icon-remove"></i></a>
                  </div>
                @elseif(Sentry::getUser()->hak_akses == 5)
                  {{-- Kosong ? --}}
                @endif
              </td>

            </tr>
          @endforeach
        @endif
      </tbody>
    </table>
  </div>
</div>