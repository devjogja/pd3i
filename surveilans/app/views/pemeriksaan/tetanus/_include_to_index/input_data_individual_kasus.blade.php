<div class="alert alert-error">
 <p style="font-size:12px">Text input yang bertanda bintang <strong>(*)</strong> wajib di isi
 </div>

 {{
 Form::open(
 array(
 'route' =>'tetanus.store',
 'class' =>'form-horizontal',
 'id'    =>'form_save_tetanus'
 )
 )
}}
<fieldset>
	<legend>Identitas pasien</legend>
	<div class="control-group">
		<label class="control-label">Nama bayi</label>
		<div class="controls">
		 {{
		 Form::text
		 (
		 'nama_anak',
		 Input::old('nama_anak'),
		 array(
		 'data-validation'         =>'[MIXED]',
		 'data'                    =>'$ wajib di isi!',
		 'data-validation-message' =>'nama bayi wajib di isi!',
		 'class'                   => 'input-xlarge',
		 'placeholder'             =>'Nama bayi'
		 )
		 )
	 }}
	 <span class="help-inline" style="color:red">(*)</span>
 </div>
</div>

<div class="control-group">
	<label class="control-label">NIK</label>
	<div class="controls">
	 {{
	 Form::text
	 (
	 'nik',
	 Input::old('nik'),
	 array(
	 'class'       => 'input-medium',
	 'placeholder' =>'Nomer induk kependudukan'
	 )
	 )
 }}
 <span class="help-inline"></span>
</div>
</div>


<div class="control-group">
	<label class="control-label">Nama orang tua</label>
	<div class="controls">
	 {{
	 Form::text
	 (
	 'nama_ortu',
	 Input::old('nama_ortu'),
	 array(
	 'data-validation'         =>'[MIXED]',
	 'data'                    =>'$ wajib di isi!',
	 'data-validation-message' =>'nama orang tua wajib di isi!',
	 'class'                   => 'input-xlarge',
	 'placeholder'             =>'Nama orang tua'
	 )
	 )
 }}
 <span class="help-inline" style="color:red">(*)</span>
</div>
</div>


<div class="control-group">
	<label class="control-label">Jenis kelamin</label>
	<div class="controls">
	 {{
	 Form::select
	 (
	 'jenis_kelamin',
	 array(
	 null  => 'Pilih',
	 '1' =>'Laki-laki',
	 '2' =>'Perempuan',
	 '3' => 'Tidak jelas'
	 ),
	 null,
	 array(
	 'data-validation'         =>'[MIXED]',
	 'data'                    =>'$ wajib di isi!',
	 'data-validation-message' =>'jenis kelamin wajib di isi!',
	 'class'                   => 'input-medium id_combobox',
	 'id'                      =>'jenis_kelamin'
	 )
	 )
 }}
 <span class="help-inline" style="color:red">(*)</span>
</div>
</div>


<div class="control-group">
	<label class="control-label"><input type="radio" name="option" onclick="pilihOptionTglLahir()">Tanggal lahir</label>
	<div class="controls">
	 {{
	 Form::text
	 (
	 'tanggal_lahir',
	 Input::old('tanggal_lahir'),
	 array(
	 'data-validation'         =>'[MIXED]',
	 'data'                    =>'$ wajib di isi!',
	 'data-validation-message' =>'tanggal lahir wajib di isi!',
	 'class'                   => 'input-medium tgl_lahir',
	 'placeholder'             =>'Tanggal lahir',
	 'data-uk-datepicker'      =>'{format:"DD-MM-YYYY"}',
	 'onchange'                =>'usia()',
	 'readonly'                =>'readonly'
	 )
	 )
 }}
 <span class="help-inline" style="color:red">(*)</span>
</div>
</div>


<div class="control-group">
 <label class="control-label"><input type="radio" name="option" onclick="pilihOptionUmur()">Umur</label>
 <div class="controls">
	 <input type="text" class="input-mini" name="tgl_tahun" id="tgl_tahun" onchange="tgl_lahir()" readonly="readonly">
	 Thn
	 <input type="text" class="input-mini" name="tgl_bulan" id="tgl_bulan" onchange="tgl_lahir()" readonly="readonly">
	 Bln
	 <input type="text" class="input-mini" name="tgl_hari" id="tgl_hari" onchange="tgl_lahir()" readonly="readonly">
	 Hr
 </div>
</div>


<div class="control-group">
	<label class="control-label">Nama faskes saat periksa</label>
	<div class="controls">
		<?php
		$type = Session::get('type');
		$kd_faskes = Session::get('kd_faskes');
		$namainstansi = '';
		if($type == "puskesmas")
		{
			$nama_instansi = 'PUSKESMAS '.DB::table('puskesmas')
			->select('puskesmas_name','puskesmas_code_faskes')
			->WHERE('puskesmas_code_faskes',$kd_faskes)->pluck('puskesmas_name');
			echo Form::text('nama_puskesmas',$nama_instansi, array('class' => 'input-large','placeholder'=>'Masukan nama puskesmas'));
		}
		elseif ($type == "rs")
		{
			$nama_instansi = DB::table('rumahsakit2')
			->select('nama_faskes')
			->WHERE('kode_faskes',$kd_faskes)->pluck('nama_faskes');
			echo Form::text('nama_puskesmas',$nama_instansi, array('class' => 'input-large','placeholder'=>'Masukan nama rumahsakit'));
		}
		?>
		<span class="help-inline"></span>
	</div>
</div>


<div class="control-group">
	<label class="control-label">Provinsi</label>
	<div class="controls">
	 {{
	 Form::select(
	 '',
	 array('placeholder'=>'Pilih propinsi')+Provinsi::lists('provinsi','id_provinsi'),
	 null,
	 array(
	 'id'          => 'id_provinsi',
	 'placeholder' => 'Pilih Provinsi',
	 'onchange'    => 'showKabupaten()',
	 'class'       => 'input-xlarge'
	 )
	 )
 }}
 <span class="help-inline"></span>
</div>
</div>


<div class="control-group">
	<label class="control-label">Kabupaten</label>
	<div class="controls">
	 {{
	 Form::select(
	 '',
	 array(''),
	 null,
	 array(
	 'class'    => 'input-medium',
	 'id'       => 'id_kabupaten',
	 'onchange' => 'showKecamatan()'
	 )
	 )
 }}
 <span class="help-inline"></span>
</div>
</div>


<div class="control-group">
	<label class="control-label">Kecamatan</label>
	<div class="controls">
	 {{
	 Form::select(
	 '',
	 array(''),
	 null,
	 array(
	 'class'    => 'input-medium',
	 'id'       => 'id_kecamatan',
	 'onchange' => 'showKelurahan()'
	 )
	 )
 }}
 <span class="help-inline"></span>
</div>
</div>


<div class="control-group">
	<label class="control-label">Kelurahan/Desa</label>
	<div class="controls">
	 {{
	 Form::select(
	 'id_kelurahan',
	 array(''),
	 null,
	 array(
	 'class'    => 'input-medium',
	 'id'       =>'id_kelurahan',
	 'onchange' =>'showEpidTetanus()'
	 )
	 )
 }}
 <span class="help-inline" style="color:red">(*)</span>
</div>
</div>


<div class="control-group">
	<label class="control-label">Alamat</label>
	<div class="controls">
	 {{
	 Form::textarea(
	 'alamat',
	 null,
	 array(
	 'rows'        => '7',
	 'placeholder' => 'Hanya diisi nama jalan, no. rumah dan no. RT - RW'
	 )
	 )
 }}
 <span class="help-inline"></span>
</div>
</div>


<div class="control-group">
	<label class="control-label">No Epidemologi</label>
	<div class="controls">
		{{
		Form::text(
		'no_epid',
		Input::old('no_epid'),
		array(
		'class'                   => 'input-medium',
		'placeholder'             => 'Nomer epidemologi',
		'id'                      => 'epid'
		)
		)
	}}
	<span class="help-inline"></span>
</div>
</div>


<!-- awal input kolom no epid lama -->
<div class="control-group">
	<label class="control-label">No Epidemologi lama</label>
	<div class="controls">
		{{
		Form::text(
		'no_epid_lama',
		Input::old('no_epid_lama'),
		array(
		'class'       => 'input-medium',
		'placeholder' => 'Nomer epidemologi'
		)
		)
	}}
	<span class="help-inline"></span>
</div>
</div>
</fieldset>



<!-- awal input kolom surveilans TN  -->
<fieldset>
	<legend>Data surveilans tetanus neonatorum</legend>
	<div class="control-group">
		<label class="control-label">Tanggal mulai sakit</label>
		<div class="controls">
			{{
			Form::text(
			'tanggal_mulai_sakit',
			Input::old('tanggal_mulai_sakit'),
			array(
			'data-validation'         => '[MIXED]',
			'data'                    => '$ wajib di isi!',
			'data-validation-message' => 'tanggal mulai sakit wajib di isi!',
			'class'                   => 'input-medium tgl_mulai_sakit',
			'id'                      => 'tgl_mulai_sakit',
			'data-uk-datepicker'      => '{format:"DD-MM-YYYY"}',
			'onchange'                => 'usia(),showEpidTetanus()'
			)
			)
		}}
		<span class="help-inline" style="color:red">(*)</span>
	</div>
</div>


<div class="control-group">
 <label class="control-label">Tanggal laporan diterima</label>
 <div class="controls">
	 {{
	 Form::text(
	 'tanggal_laporan_diterima',
	 Input::old('tanggal_laporan_diterima'),
	 array(
	 'class'                   => 'input-medium',
	 'data-uk-datepicker'      => '{format:"DD-MM-YYYY"}'
	 )
	 )
 }}
 <span class="help-inline"></span>
</div>
</div>


<div class="control-group">
	<label class="control-label">Tanggal pelacakan</label>
	<div class="controls">
	 {{
	 Form::text(
	 'tanggal_pelacakan',
	 Input::old('tanggal_pelacakan'),
	 array
	 (
	 'class'                   => 'input-medium',
	 'data-uk-datepicker'      => '{format:"DD-MM-YYYY"}'
	 )
	 )
 }}
 <span class="help-inline"></span>
</div>
</div>


<div class="control-group">
	<label class="control-label">Antenatal care (ANC)</label>
	<div class="controls">
	 {{
	 Form::select(
	 'ANC',
	 array(
	 ''  => 'Pilih',
	 '0' => 'Dokter',
	 '1' => 'Bidan/Perawat',
	 '2' => 'Dukun',
	 '3' => 'Tidak ANC',
	 '4' => 'Tidak jelas'
	 ),
	 Input::old('ANC'),
	 array(
	 'class' => 'input-medium'
	 )
	 )
 }}
 <span class="help-inline"></span>
</div>
</div>


<div class="control-group">
	<label class="control-label">Status imunisasi ibu</label>
	<div class="controls">
	 {{
	 Form::select(
	 'status_imunisasi',
	 array(
	 ''  => 'Pilih',
	 '0' => 'TT2+',
	 '1' => 'TT1',
	 '2' => 'Tidak imunisasi',
	 '3' => 'Tidak jelas'
	 ),
	 Input::old('status_imunisasi'),
	 array(
	 'data-validation'         => '[MIXED]',
	 'data'                    => '$ wajib di isi!',
	 'data-validation-message' => 'status imunisasi ibu wajib di isi!',
	 'class'                   => 'input-medium'
	 )
	 )
 }}
 <span class="help-inline" style="color:red">(*)</span>
</div>
</div>


<div class="control-group">
	<label class="control-label">Penolong persalinan</label>
	<div class="controls">
	 {{
	 Form::select(
	 'penolong_persalinan',
	 array(
	 ''  => 'Pilih',
	 '0' => 'Dokter',
	 '1' => 'Bidan/Perawat',
	 '2' => 'Dukun',
	 '3' => 'Tidak jelas'
	 ),
	 Input::old('penolong_persalinan'),
	 array(
	 'class' => 'input-medium'
	 )
	 )
 }}
 <span class="help-inline"></span>
</div>
</div>


<div class="control-group">
	<label class="control-label">Alat Pemotong tali pusat</label>
	<div class="controls">
	 {{
	 Form::select(
	 'pemotongan_tali_pusat',
	 array(
	 ''  => 'Pilih',
	 '0' => 'Gunting',
	 '1' => 'Bambu',
	 '2' => 'Silet',
	 '3' => 'pisau',
	 '4' => 'Lain-lain',
	 '5' => 'Tidak tahu'
	 ),
	 Input::old('pemotongan_tali_pusat'),
	 array(
	 'class' => 'input-medium'
	 )
	 )
 }}
 <span class="help-inline"></span>
</div>
</div>


<div class="control-group">
	<label class="control-label">Rawat rumah sakit</label>
	<div class="controls">
	 {{
	 Form::select(
	 'rawat_rumah_sakit',
	 array(
	 ''  => 'Pilih',
	 '0' => 'Ya',
	 '1' => 'Tidak',
	 '2' => 'Tidak jelas'
	 ),
	 Input::old('rawat_rumah_sakit'),
	 array(
	 'class' => 'input-medium'
	 )
	 )
 }}
 <span class="help-inline"></span>
</div>
</div>


<div class="control-group">
	<label class="control-label">Keadaan akhir</label>
	<div class="controls">
	 {{
	 Form::select(
	 'keadaan_akhir',
	 array(
	 ''  => 'Pilih',
	 '0' => 'Hidup',
	 '1' => 'Meninggal'
	 ),
	 Input::old('keadaan_akhir'),
	 array(
	 'class'                   =>  'input-small'
	 )
	 )
 }}
 <span class="help-inline" ></span>
</div>
</div>
</fieldset>



<!-- awal input kolom klasifikasi final -->
<fieldset>
	<legend>Klasifikasi final</legend>
	<div class="control-group">
		<label class="control-label">Klasifikasi Final</label>
		<div class="controls">
		 {{
		 Form::select(
		 'klasifikasi_akhir',
		 array(
		 ''  => 'Pilih',
		 '1' => 'Konfirm TN',
		 '2' => 'Tersangka TN',
		 '3' => 'Bukan TN'
		 ),
		 Input::old('klasifikasi_akhir'),
		 array(
		 'data-validation'         => '[MIXED]',
		 'data'                    => '$ wajib di isi!',
		 'data-validation-message' => 'klasifkasi final wajib di isi!',
		 'class'                   => 'input-medium'
		 )
		 )
	 }}
	 <span class="help-inline" style="color:red">(*)</span>
 </div>
</div>
</fieldset>

<!-- btn -->
<div class="form-actions">
 {{ Form::submit('simpan', array('class' => 'btn btn-primary')) }}
 {{ Form::reset('batal', array('class' => 'btn')) }}
</div>
<br/>
<br/>

{{ Form::close() }}