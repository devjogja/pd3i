<script>
$(document).ready(function(){
	$('#populasi_1,#populasi_2,#populasi_3,#populasi_4,#populasi_5').tooltip();
});	
</script>
<div class="module">
	<div class="module-head">
		<h3>Tetanus Neonatorum</h3>
	</div>
	<div class="module-body">
		<div class="chart inline-legend grid" id="" style="width:80%;">
			<table class="table" style="width:100%;">
				<tr>
					<td>
						<span id="populasi_1" title="(jumlah kasus TN meninggal / jumlah kasus TN ) x 100%">Case Fatality Rate (CFR)</span><br>
					</td>
					<td style="width:200px;">
						0
					</td>
				</tr>
				
			</table>
		</div>
	</div>
</div>
<!--/.module-->