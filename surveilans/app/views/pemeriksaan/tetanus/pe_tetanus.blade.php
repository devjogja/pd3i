@extends('layouts.master')
@section('content')
<!-- awal span12 -->
<div class="span12">
	<!-- awal class content -->
	<div class="content">
		<!-- awal class module -->
		<div class="module">
			<!-- awal class module-body -->
			<div class="module-body">
				<div class="profile-head media">
					<h4>
							Penyelidikan Epidemologi Penyakit Tetanus Neonatorum
					</h4>
					<hr>
				</div>

				<!-- awal form -->
				{{
					Form::open
					(
						array
						(
							'route'=>'pe_tetanus',
							'class'=>'form-horizontal'
						)
					)
				}}
				@foreach($pe_tetanus as $row)
				<!-- awal input kolom informasi pelapor -->
				<fieldset>
					<legend>Informasi pelapor</legend>
					<div class="control-group">
						<label class="control-label">Sumber laporan</label>
						<div class="controls">
							{{
								Form::select
								(
									'sumber_laporan',
									array
									(
										''=>'Pilih',
										'0'=>'Rumah sakit',
										'1'=>'Puskesmas',
										'2'=>'Praktek swasta',
										'3'=>'Masyarakat',
									),
									Input::old('sumber_laporan'), 
									array
									(
										'class' => 'input-medium',
										'id' => 'id_sumber',
									)
								)
								}}
							<span class="help-inline"></span>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">Tanggal laporan diterima</label>
						<div class="controls">
							{{
								Form::text
								(
									'tanggal_laporan_diterima',
									Helper::getDate($row->tanggal_laporan_diterima),
									array
									(
										'class' => 'input-medium',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">Tanggal pelacakan</label>
						<div class="controls">
							{{
								Form::text
								(
									'tanggal_pelacakan',
									Helper::getDate($row->tanggal_laporan_diterima),
									array
									(
										'class' => 'input-medium',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>
				</fieldset>
				<!-- akhir input kolom informasi pelapor -->
				
				<!-- awal input kolom identitas bayi -->
				<fieldset>
				<legend>Identitas bayi</legend>
					<div class="control-group">
						<label class="control-label">No. epidemologi</label>
						<div class="controls">
							{{
								Form::text
								(
									'no_epid',
									$row->no_epid,
									array
									(
										'class'=>'input-medium no_epid'
									)
								)
							}}
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Nama bayi</label>
						<div class="controls">
							{{
								Form::text
								(
									'nama_anak',
									$row->nama_anak, 
									array
									(
										'class' => 'input-medium'
									)
								)
							}}
							{{
								Form::hidden
								(
									'id_pasien',
									$row->id_pasien,
									array
									(
										'class'=>'id_pasien'
									)
								)
							}}
							{{
								Form::hidden
								(
									'id_tetanus',
									$row->id_tetanus,
									array
									(
										'class'=>'id_tetanus'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>


					<div class="control-group">
						<label class="control-label">Jenis kelamin</label>
						<div class="controls">
							{{
								Form::select
								(
									'jenis_kelamin',
									[
										null => 'Pilih',
										'1'=>'Laki-laki',
										'2'=>'Perempuan',
										'3' => 'Tidak Jelas'
									],
									[$row->jenis_kelamin], 
									array
									(
										'class' => 'input-medium id_combobox jenis_kelamin'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>


					<div class="control-group">
						<label class="control-label">Tanggal lahir</label>
						<div class="controls">
							{{
								Form::text
								(
									'tanggal_lahir',
									$row->tanggal_lahir, 
									array
									(
										'class' => 'input-medium tanggal_lahir',
										'placeholder'=>'Tanggal lahir',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
										'onchange'=>'umur_pe()'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>


					<div class="control-group">
						<div class="controls">
							<input type="text" class="input-mini" value="{{$row->umur_hr}}" name="tgl_hari" id="tanggal_hari">
							Hr
						</div>
					</div>


					<div class="control-group">
						<label class="control-label">Anak ke</label>
						<div class="controls">
							{{
								Form::text
								(
									'anak_ke',
									Input::old('anak_ke'), 
									array
									(
										'class' => 'input-medium'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>


					<div class="control-group">
						<label class="control-label">Nama ayah</label>
						<div class="controls">
							{{
								Form::text
								(
									'nama_ayah',
									Input::old('nama_ayah'), 
									array
									(
										'class' => 'input-medium'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>



					<div class="control-group">
						<label class="control-label">Umur ayah</label>
						<div class="controls">
							{{
								Form::text
								(
									'umur_ayah',
									Input::old('umur_ayah'), 
									array
									(
										'class' => 'input-mini'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>


					<!-- <div class="control-group">
						<label class="control-label">Tahun pendidikan ayah</label>
						<div class="controls">
							{{
								Form::text
								(
									'th_pendidikan_ayah',
									Input::old('th_pendidikan_ayah'), 
									array
									(
										'class' => 'input-mini'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div> -->
					<div class="control-group">
						<label class="control-label">Pendidikan terakhir ayah</label>
						<div class="controls">
							{{
								Form::select
								(
									'pendidikan_terakhir_ayah',
									array
									(
										null=>'Pilih',
										'1'=>'Tidak Sekolah',
										'2'=>'SD',
										'3'=>'SMP',
										'4'=>'SMA/SMK',
										'5'=>'D3',
										'6'=>'S1',
										'7'=>'S2',
										'8'=>'S3',
									),
									Input::old('pendidikan_terakhir_ayah'),
									array
									(
										'class'=>'input-medium',
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">pekerjaan ayah</label>
						<div class="controls">
							{{
								Form::text
								(
									'pekerjaan_ayah',
									Input::old('pekerjaan_ayah'), 
									array
									(
										'class' => 'input-medium'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>


					<div class="control-group">
						<label class="control-label">Nama ibu</label>
						<div class="controls">
							{{
								Form::text
								(
									'nama_ibu',
									Input::old('nama_ibu'), 
									array
									(
										'class' => 'input-medium'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>


					<!-- <div class="control-group">
						<label class="control-label">Tahun pendidikan ibu</label>
						<div class="controls">
							{{
								Form::text
								(
									'th_pendidikan_ibu',
									Input::old('th_pendidikan_ibu'), 
									array
									(
										'class' => 'input-mini'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div> -->
					<div class="control-group">
						<label class="control-label">Pendidikan terakhir ibu</label>
						<div class="controls">
							{{
								Form::select
								(
									'pendidikan_terakhir_ibu',
									array
									(
										null=>'Pilih',
										'1'=>'Tidak Sekolah',
										'2'=>'SD',
										'3'=>'SMP',
										'4'=>'SMA/SMK',
										'5'=>'D3',
										'6'=>'S1',
										'7'=>'S2',
										'8'=>'S3',
									),
									Input::old('pendidikan_terakhir_ibu'),
									array
									(
										'class'=>'input-medium',
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">pekerjaan ibu</label>
						<div class="controls">
							{{
								Form::text
								(
									'pekerjaan_ibu',
									Input::old('pekerjaan_ibu'), 
									array
									(
										'class' => 'input-medium'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>


					<div class="control-group">
						<label class="control-label">Provinsi</label>
						<div class="controls">
						<select name="" class="input-medium id_combobox" id="id_provinsi" onchange="showKabupaten()">
							<?php $pro=Provinsi::lists('provinsi','id_provinsi'); 
							 foreach ($pro as $id_provinsi => $provinsi) {
									if($provinsi==$row->provinsi) {
										?>
										<option value="{{$id_provinsi}}" selected="selected">{{$provinsi}}</option>
										<?php
									} else {
									?>
										<option value="{{$id_provinsi}}">{{$provinsi}}</option>
									<?php
									}
								} 
							?>
							</select>
							<span class="help-inline"></span>
						</div>
					</div>


					<div class="control-group">
						<label class="control-label">Kabupaten/Kota</label>
						<div class="controls">
							<select name="" class="input-medium id_combobox" id="id_kabupaten" onchange="showKecamatan()">
								<option value="">{{$row->kabupaten}}</option>
							</select>
							<span class="help-inline"></span>
						</div>
					</div>


					<div class="control-group">
						<label class="control-label">Kecamatan</label>
						<div class="controls">
							<select name="" class="input-medium id_combobox" id="id_kecamatan" onchange="showKelurahan()">
								<option value="">{{$row->kecamatan}}</option>
							</select>
							<span class="help-inline"></span>
						</div>
					</div>


					<div class="control-group">
						<label class="control-label">Kelurahan/Desa</label>
						<div class="controls">
							<select name="id_kelurahan" class="input-medium id_combobox" id="id_kelurahan" onchange="showEpid()">
								<option value="{{$row->id_kelurahan}}">{{$row->kelurahan}}</option>
							</select>
							<span class="help-inline"></span>
						</div>
					</div>


					<div class="control-group">
						<label class="control-label">Alamat</label>
						<div class="controls">
							{{Form::text('alamat',$row->alamat, array('class' => 'input-xlarge alamat','placeholder'=>'tulisan alamat seperti nama dusun/gang/blok/no jalan/rt/rw'))}}
							<span class="help-inline"></span>
						</div>
					</div>
					@endforeach
				</fieldset>
				<!-- akhir input kolom identitas bayi -->


				<!-- awal input kolom Informasi Riwayat Kesakitan  -->
				<fieldset>
					<legend>Informasi Riwayat Kesakitan/Kematian Bayi 3-28 Hari</legend>
					<div class="control-group">
						<label class="control-label">Nama yang diwawancarai</label>
						<div class="controls">
							{{
								Form::text
								(
									'nama_yang_diwawancarai',
									Input::old('nama_yang_diwawancarai'),
									array
									(
										'class'=>'input-medium'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>


					<div class="control-group">
						<label class="control-label">Hubungan dengan keluarga bayi</label>
						<div class="controls">
							{{
								Form::text
								(
									'hubungan_keluarga',
									Input::old('hubungan_keluarga'),
									array
									(
										'class'=>'input-medium'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>


					<div class="control-group">
						<label class="control-label">Bayi lahir hidup </label>
						<div class="controls">
							{{
								Form::select
								(
									'bayi_hidup',
									array
									(
										''=>'Pilih',
										'0'=>'Ya',
										'1'=>'Tidak'
									),
									Input::old('bayi_hidup'),
									array
									(
										'class'=>'input-medium',
										'id'=>'id_bayi_lahir',
										'onchange'=>'cekBayiLahir()'
									)
								)
							}}
						<span class="help-inline"></span>
						</div>
					</div>
					<br>


					<div id="bayi_lahir">
						<div class="control-group">
							<label class="control-label">Tanggal lahir bayi</label>
							<div class="controls">
								{{
									Form::text
									(
										'tgl_lahir',
										Input::old('tgl_lahir'), 
										array
										(
											'class' => 'input-medium',
											'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
										)
									)
								}}
								<span class="help-inline"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Tanggal tidak mau menetek/minum</label>
							<div class="controls">
								{{
									Form::text
									(
										'tanggal_mulai_sakit',
										Input::old('tanggal_mulai_sakit'), 
										array
										(
											'class' => 'input-medium',
											'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
										)
									)
								}}
								<span class="help-inline"></span>
							</div>
						</div>


						<div class="control-group">
							<label class="control-label">Tanggal mulai kejang (bila ada rangsang sentuh,suara,cahaya)</label>
							<div class="controls">
								{{
									Form::text
									(
										'tanggal_mulai_sakit',
										Input::old('tanggal_mulai_sakit'), 
										array
										(
											'class' => 'input-medium',
											'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
										)
									)
								}}
								<span class="help-inline"></span>
							</div>
						</div>


						<div class="control-group">
							<label class="control-label">Apakah bayi meninggal</label>
							<div class="controls">
								{{
									Form::select
									(
										'bayi_meninggal', 
										array
										(
											null => 'Pilih',
											'0'=>'Ya',
											'1'=>'Tidak'
										),
										Input::old('bayi_meinggal'), 
										array
										(
											'class' => 'input-medium bayi_meninggal'
										)
									)
								}}
								<span class="help-inline"></span>
							</div>
						</div>


						<div class="control-group">
						<label class="control-label">Tanggal meninggal</label>
							<div class="controls">
								{{
									Form::text
									(
										'tanggal_meninggal',
										Input::old('tanggal_meninggal'), 
										array
										(
											'class' => 'input-medium tanggal_meninggal',
											'disabled',
											'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
										)
									)
								}}
								{{
									Form::text
									(
										'umur',
										Input::old('umur'), 
										array
										(
											'class' => 'input-mini tanggal_meninggal',
											'disabled'
										)
									)
								}}
								umur(hari)
							</div>
						</div>


						<div class="control-group">
							<label class="control-label">Waktu lahir apakah bayi menangis</label>
							<div class="controls">
								{{
									Form::select
									(
										'bayi_menangis',
										Array
										(
											''=>'Pilih',
											'0'=>'Ya',
											'1'=>'Tidak',
											'2'=>'Tidak tahu'
										),
										Input::old('bayi_menangis'),
										array
										(
											'class' => 'input-medium'
										)
									)
								}}
								<span class="help-inline"></span>
							</div>
						</div>


						<div class="control-group">
							<label class="control-label">Apakah terlihat tanda-tanda kehidupan lain dari bayi (mis. Sedikit
							 gerakan)</label>
							<div class="controls">
								{{
									Form::select
									(
										'tanda_kehidupan_lain',
										Array
										(
											''=>'Pilih',
											'0'=>'Ya',
											'1'=>'Tidak',
											'2'=>'Tidak tahu'
										),
										Input::old('tanda_kehidupan_lain'),
										array
										(
											'class' => 'input-medium'
										)
									)
								}}
								<span class="help-inline"></span>
							</div>
						</div>


						<div class="control-group">
							<label class="control-label">Setelah lahir apakah bayi bisa menetek atau menghisap susu botol dengan baik </label>
							<div class="controls">
								{{
									Form::select
									(
										'bayi_menetek',
										Array
										(
											''=>'Pilih',
											'0'=>'Ya',
											'1'=>'Tidak',
											'2'=>'Tidak tahu'
										),
										Input::old('bayi_menetek'),
										array
										(
											'class' => 'input-medium',
											'id'=>'id_lahir',
											'onchange'=>'cekLahir()'
										)
									)
								}}
								<span class="help-inline"></span>
							</div>
						</div>
						<br>


					<div id="lahir">
						<div class="control-group">
							<label class="control-label">Apakah 3 hari kemudian tiba-tiba mulut bayi mencucu dan tidak bisa menetek?</label>
							<div class="controls">
								{{
									Form::select
									(
										'bayi_mencucu',
										Array
										(
											''=>'Pilih',
											'0'=>'Ya',
											'1'=>'Tidak',
											'2'=>'Tidak tahu'
										),
										Input::old('bayi_mencucu'),
										array
										(
											'class' => 'input-medium'
										)
									)
								}}
								<span class="help-inline"></span>
							</div>
						</div>


						<div class="control-group">
							<label class="control-label">Apakah bayi mudah kejang jika disentuh/terkena sinar atau bunyi?</label>
							<div class="controls">
								{{
									Form::select
									(
										'bayi_kejang',
										Array
										(
											''=>'Pilih',
											'0'=>'Ya',
											'1'=>'Tidak',
											'2'=>'Tidak tahu'
										),
										Input::old('bayi_kejang'),
										array
										(
											'class' => 'input-medium'
										)
									)
								}}
								<span class="help-inline"></span>
							</div>
						</div>


						<div class="control-group">
							<label class="control-label">Bayi dirawat</label>
							<div class="controls">
								{{
									Form::select
									(
										'dirawat', 
										array
										(
											''=>'Pilih',
											'0' => 'Ya',
											'1'=>'Tidak'
										),
										Input::old('dirawat'), 
										array
										(
											'class' => 'input-small',
											'id'=>'id_dirawat',
											'onchange'=>'diRawat()'
										)
									)
								}}
								<span class="help-inline"></span>
							</div>
						</div>
						</br>


					<div id="rawat">
						<div class="control-group">
							<label class="control-label">Dimana RS/Puskesmas</label>
							<div class="controls">
								{{
									Form::select
									(
										'tempat_berobat', 
										array
										(
											''=>'Pilih',
											'0' => 'Rumah sakit',
											'1'=>'Puskesmas',
											'2'=>'Dokter praktek swasta',
											'3'=>'Perawat/mantri/bidan',
											'4'=>'Tidak berobat'
										),
										Input::old('tempat_berobat'), 
										array
										(
											'class' => 'input-medium',
											'id'=>'id_faskes_bayi_dirawat',
											'onchange'=>'pilihFaskes()'
										)
									)
								}}
								<input type="text" name="nama_faskes_bayi_dirawat" id="nama_faskes_bayi_dirawat">
								<span class="help-inline"></span>
							</div>
						</div>


						<div class="control-group">
							<label class="control-label">Tanggal mulai dirawat</label>
							<div class="controls">
								{{
									Form::text
									(
										'tanggal_dirawat',
										Input::old('tanggal_dirawat'), 
										array
										(
											'class' => 'input-medium',
											'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
										)
									)
								}}
								<span class="help-inline"></span>
							</div>
						</div>


						<div class="control-group">
							<label class="control-label">Keadaan bayi setelah dirawat</label>
							<div class="controls">
								{{
									Form::select
									(
										'keadaan_setelah_dirawat', 
										array
										(
											''=>'Pilih',
											'0' => 'Sembuh',
											'1'=>'Tidak sembuh',
											'2'=>'Meninggal'
										),
										Input::old('keadaan_setelah_dirawat'), 
										array
										(
											'class' => 'input-medium'
										)
									)
								}}
								<span class="help-inline"></span>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
			<!-- akhir input kolom Informasi Riwayat Kesakitan  -->

			<!-- awal input kolom riwayat kehamilan -->
			<fieldset id='hide_kehamilan'>
				<legend>Riwayat kehamilan</legend>
				<div class="control-group">
					<label class="control-label">GPA</label>
					<div class="controls">
						<input type="text" name="gpa">
					</div>
				</div>


				<div class="control-group">
				    <label class="control-label">Berapa kali periksa kehamilan dengan</label>
				    <div class="controls">
					    <table>
					        <tr>
						        <td>
						          	<input type="checkbox" id="periksa_1" onclick="Periksa(1)">Dokter
						        </td>
						        <td>
						         	<input type="text" class="input-medium" name="jml_periksa_kehamilan_dengan_dokter" id="id_periksa_1">
						        </td>
					        </tr>
					          	<td>
					          		<input type="checkbox" id="periksa_2" onclick="Periksa(2)">Bidan / perawat
					          	</td>
					         	<td>
					          		<input type="text" class="input-medium" name="jml_periksa_kehamilan_dengan_bidan_perawat" id="id_periksa_2">
					          	</td>
					        </tr>
					        <tr>
					          	<td>
					          		<input type="checkbox" id="periksa_3" onclick="Periksa(3)">DUkun
					          	</td>
					          	<td>
					          		<input type="text" class="input-medium" name="jml_periksa_kehamilan_dengan_dukun" id="id_periksa_3">
					          	</td>
					        </tr>
					        <tr>
					          	<td>
					          		<input type="checkbox" id="periksa_4" onclick="Periksa(4)">Tidak ANC
					          	</td>
					          	<td>
					          		<input type="text" class="input-medium" name="jml_Tidak_ANC" id="id_periksa_4">
					          	</td>
					        </tr>
					        <tr>
					          	<td>
					          		<input type="checkbox" id="periksa_5" onclick="Periksa(5)">Tidak jelas
					          	</td>
					          	<td>
					          		<input type="text" class="input-medium" name="jml_tidak_jelas" id="id_periksa_5">
					          	</td>
					        </tr>
					    </table>
				      	<span class="help-inline"></span>
				    </div>
			    </div>


				<div class="control-group">
					<label class="control-label">Petugas pemeriksa</label>
					<div class="controls">
						<table class="table table-bordered petugas_kehamilan">
						</table>
						<br/>
						<div id="R_kehamilan">
							<table class="table">
								<tr>
									<td>Nama</td>
									<td><input type="text" class="nama_petugas_kehamilan"></td>
								</tr>
								<tr>
									<td>Profesi</td>
									<td>
									<select id="profesi">
										<option value="Dokter">Dokter</option>
										<option value="Bidan/perawat">Bidan/perawat</option>
										<option value="Dukun">Dukun</option>
										<option value="Lainnya">Lainnya</option>
									</select>
									</td>
								<tr>
									<td>Alamat</td>
									<td><input type="text" class="input-xxlarge alamat_petugas_kehamilan"></td>
								</tr>
								<tr>
									<td>Frekuensi</td>
									<td><input type="hidden" class="id_petugas_kehamilan"><input type="text" class="frekuensi"></td>
								</tr>
							</table>
							<br/>
							<a class="btn tutup_petugas_kehamilan">Tutup</a>&nbsp;
							<button class="btn btn-success simpan_petugas_kehamilan">Tambah petugas</button>
							<button class="btn btn-success edit_petugas_kehamilan">edit petugas</button>
						</div>
						<a class="btn show_petugas_kehamilan">Tampilkan untuk tambah petugas</a>
					</div>
				</div> 


				<div class="control-group">
					<label class="control-label">Apakah ibu pernah mendapat imunisasi TT pada waktu hamil bayi ini?</label>
					<div class="controls">
						{{
							Form::select
							(
								'mendapat_imunisasi', 
								array(''=>'Pilih','0' => 'Ya','1'=>'Tidak'),Input::old('mendapat_imunisasi'), array('class' => 'input-small'))}}
						<span class="help-inline"></span>
					</div>
				</div>


				<div class="control-group">
					<label class="control-label">Sumber informasi imunisasi TT </label>
					<div class="controls">
						{{
							Form::select
							(
								'info_imunisasi', 
								array
								(
									''=>'Pilih',
									'0' => 'Ingatan',
									'1'=>'Buku catatan'
								),
								Input::old('info_imunisasi'), 
								array
								(
									'class' => 'input-small'
								)
							)
						}}
						<span class="help-inline"></span>
					</div>
				</div>


				<div class="control-group">
					<label class="control-label">Berapa kali mendapat imunisasi TT pada saat kehamilan bayi tersebut?</label>
					<div class="controls">
						{{
							Form::select
							(
								'jumlah_imunisasi', 
								array
								(
									''=>'Pilih',
									'0' => 'Pertama kali',
									'1'=>'Kedua kali'
								),
								Input::old('jumlah_imunisasi'), 
								array
								(
									'class' => 'input-small'
								)
							)
						}}
						<span class="help-inline"></span>
					</div>
				</div>


				<div class="control-group">
					<label class="control-label">Umur kehamilan saat mendapat imunisasi TT pertama (bulan)</label>
					<div class="controls">
						{{
							Form::text
							(
								'umur_kehamilan_pertama',
								Input::old('umur_kehamilan_pertama'),
								array
								(
									'class'=>'input-mini'
								)
							)
						}}
						<span class="help-inline"></span>
					</div>
				</div>


				<div class="control-group">
					<label class="control-label">Umur kehamilan saat mendapat imunisasi TT kedua (bulan)</label>
					<div class="controls">
						{{
							Form::text
							(
								'umur_kehamilan_kedua',
								Input::old('umur_kehamilan_kedua'),
								array
								(
									'class'=>'input-mini'
								)
							)
						}}
						<span class="help-inline"></span>
					</div>
				</div>


				<div class="control-group">
					<label class="control-label">Tanggal imunisasi kedua saat kehamilan bayi tersebut?</label>
					<div class="controls">
						{{
							Form::text
							(
								'tanggal_imunisasi_kedua',
								Input::old('tanggal_imunisasi_kedua'),
								array
								(
									'class'=>'input-small',
									'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
								)
							)
						}}
						<span class="help-inline"></span>
					</div>
				</div>


				<!-- <div class="control-group">
					<label class="control-label">Tahun kehamilan sebelumnya -1</label>
					<div class="controls">
						{{
							Form::text
							(
								'tahun_kehamilan_1',
								Input::old('tanggal_kehamilan_1'),
								array
								(
									'class'=>'input-small'
								)
							)
						}}
						<span class="help-inline"></span>
					</div>
				</div>  


				<div class="control-group">
					<label class="control-label">Berapa kali ibu mendapat imunisasi TT pada kehamilan sebelumnya</label>
					<div class="controls">
						{{
							Form::select
							(
								'ibu_mendapat_imunisasi_1', 
								array
								(
									''=>'Pilih',
									'0' => '1X',
									'1'=>'2X',
									'2'=>'Tidak pernah'
								),
								Input::old('ibu_mendapat_imunisasi_1'), 
								array
								(
									'class' => 'input-small',
									'id'=>'ibu_mendapat_imunisasi_1'
								)
							)
						}}
						<span class="help-inline"></span>
					</div>
				</div>


				<div class="control-group">
					<label class="control-label">Tahun kehamilan sebelumnya -2</label>
					<div class="controls">
						{{
							Form::text
							(
								'tahun_kehamilan_2',
								Input::old('tanggal_imunisasi_kedua'),
								array
								(
									'class'=>'input-small'
								)
							)
						}}
						<span class="help-inline"></span>
					</div>
				</div>

-->
				<div class="control-group">
					<label class="control-label">Berapa kali ibu mendapat imunisasi TT pada kehamilan sebelumnya</label>
					<div class="controls">
						{{
							Form::select
							(
								'ibu_mendapat_imunisasi_2', 
								array
								(
									''=>'Pilih',
									'0' => '1X',
									'1'=>'2X',
									'2'=>'Tidak pernah'
								),
								Input::old('ibu_mendapat_imunisasi_2'), 
								array
								(
									'class' => 'input-small',
									'id'=>'ibu_mendapat_imunisasi_2'
								)
							)
						}}
						<table>
							<tr>
								<td>Tahun pertama</td>
								<td>:</td>
								<td><input type="text" name="tahun_Hpertama" id="tahun_Hpertama"></td>
							</tr>
							<tr>
								<td>Tahun kedua</td>
								<td>:</td>
								<td><input type="text" name="tahun_Hkedua" id="tahun_Hkedua"></td>
							</tr>
						</table>
						<span class="help-inline"></span>
					</div>
				</div>
 

				<div class="control-group">
					<label class="control-label">Berapa kali ibu mendapat suntikan TT calon pengantin</label>
					<div class="controls">
						{{
							Form::select
							(
								'ibu_mendapat_suntikan_pengantin', 
								array
								(
									''=>'Pilih',
									'0' => '1x',
									'1'=>'2X',
									'2'=>'Tidak pernah'
								),
								Input::old('ibu_mendapat_suntikan_pengantin'), 
								array
								(
									'class' => 'input-small',
									'id'=>'ibu_mendapat_suntikan_pengantin'
								)
							)
						}}
						<table>
							<tr>
								<td>Tahun pertama</td>
								<td>:</td>
								<td><input type="text" name="tahun_Ppertama" id="tahun_Ppertama"></td>
							</tr>
							<tr>
								<td>Tahun kedua</td>
								<td>:</td>
								<td><input type="text" name="tahun_Pkedua" id="tahun_Pkedua"></td>
							</tr>
						</table>
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Status TT ibu pada saat kehamilan bayi tersebut</label>
					<div class="controls">
						{{
							Form::select
							(
								'status_ibu_saat_hamil', 
								array
								(
									''=>'Pilih',
									'0' => 'TT1',
									'1'=>'TT2',
									'2'=>'TT3',
									'3'=>'TT4',
									'4'=>'TT5'
								),
								Input::old('status_ibu_saat_hamil'), 
								array
								(
									'class' => 'input-medium'
								)
							)
						}}
						<span class="help-inline"></span>
					</div>
				</div>                        
			</fieldset>
			<!-- akhir input kolom riwayat kehamilan -->

			<!-- awal input kolom riwayat persalinan -->
			<fieldset id="hide_persalinan">
				<legend>Riwayat persalinan</legend>
				<div class="control-group">
					<label class="control-label">Obat yang dibubuhkan setelah tali pusat di potong</label>
					<div class="controls">
						{{
							Form::select
							(
								'obat_penyembuh', 
								array
								(
									''=>'pilih',
									'0' => 'Alkohol/lod',
									'1'=>'Ramuan tradisional',
									'2'=>'Kasa kering',
									'3'=>'Lainnya'
								), 
								Input::old('obat_penyembuh'), 
								array
								(
									'class' => 'input-medium'
								)
							)
						}}
						<span class="help-inline"></span>
					</div>
				</div>


				<div class="control-group">
					<label class="control-label">Jika jawaban ramuan tradisional atau lainnya, sebutkan : </label>
					<div class="controls">
					 	{{
					 		Form::text
					 		(
					 			'ramuan_lainnya',
					 			Input::old('ramuan_lainnya'),
					 			array
					 			(
					 				'class'=>'input-xlarge'
					 			)
					 		)
					 	}}
						<span class="help-inline"></span>
					</div>
				</div>


				<div class="control-group">
					<label class="control-label">Yang melakukan perawatan tali pusat awal-akhir</label>
					<div class="controls">
						{{
							Form::select
							(
								'perawatan_tali_pusat', 
								array
								(
									''=>'pilih',
									'0' => 'Tenaga kesehatan',
									'1'=>'Bukan tenaga kesehatan'
								), 
								Input::old('perawatan_tali_pusat'), 
								array
								(
									'class' => 'input-medium'
								)
							)
						}}
						<span class="help-inline"></span>
					</div>
				</div>


				<div class="control-group">
				<label class="control-label">Obat atau ramuan apa saja yang dibubuhkan selama merawat tali pusat, sebutkan</label>
					<div class="controls">
						{{
							Form::select
							(
								'obat_merawat_tali_pusat', 
								array
								(
									''=>'pilih',
									'0' => 'Alkohol/lod',
									'1'=>'Ramuan tradisional',
									'2'=>'Kasa kering',
									'3'=>'Lainnya'
								), 
								Input::old('obat_merawat_tali_pusat'), 
								array
								(
									'class' => 'input-medium'
								)
							)
						}}
						<span class="help-inline"></span>
					</div>
				</div>


				<div class="control-group">
					<label class="control-label">Kesimpulan diagnosis</label>
					<div class="controls">
						{{
							Form::select
							(
								'kesimpulan_diagnosis', 
								array
								(
									''=>'pilih',
									'0' => 'Konfirm TN',
									'1'=>'Tersangka TN',
									'2'=>'Bukan TN'
								), 
								Input::old('kesimpulan_diagnosis'), 
								array
								(
									'class' => 'input-medium'
								)
							)
						}}
						<span class="help-inline"></span>
					</div>
				</div>


				<div class="control-group">
					<label class="control-label">Cakupan imunisasi TT di desa kasus TN</label>
					<div class="controls">
					<table>
							<tr>
								<td>
									TT1 (%)
								</td>
								<td>
									{{
										Form::text
										(
											'TT1',
											Input::old('TT1'),
											array
											(
												'class'=>'input-mini'
											)
										)
									}}
								</td>
							</tr>
							<tr>
								<td>
									TT2 (%)
								</td>
								<td>
									{{
										Form::text
										(
											'TT2',
											Input::old('TT2'),
											array
											(
												'class'=>'input-mini'
											)
										)
									}}
								</td>
							</tr>
							<tr>
								<td>
									TT3 (%)
								</td>
								<td>
									{{
										Form::text
										(
											'TT3',
											Input::old('TT3'),
											array
											(
												'class'=>'input-mini'
											)
										)
									}}
								</td>
							</tr>
							<tr>
								<td>
									TT4 (%)
								</td>
								<td>
									{{
										Form::text
										(
											'TT4',
											Input::old('TT4'),
											array
											(
												'class'=>'input-mini'
											)
										)
									}}
								</td>
							</tr>
							<tr>
								<td>
									TT5 (%)
								</td>
								<td> 
									{{
										Form::text
										(
											'TT5',
											Input::old('TT5'),
											array
											(
												'class'=>'input-mini'
											)
										)
									}}
								</td>
							</tr>
						</table>
					</div>
				</div>


				<div class="control-group">
					<label class="control-label">Cakupan persalinan tenaga kesehatan (%)</label>
					<div class="controls">
						{{
							Form::text
							(
								'cakupan_tenaga_kesehatan',
								Input::old('cakupan_tenaga_kesehatan'), 
								array
								(
									'class' => 'input-mini'
								)
							)
						}}
						<span class="help-inline"></span>
					</div>
				</div>


				<div class="control-group">
					<label class="control-label">Cakupan kunjungan neonatus</label>
					<div class="controls">
					<table>
							<tr>
								<td>
									KN1 (%)
								</td>
								<td>
									{{
										Form::text('KN1',Input::old('KN1'),array('class'=>'input-mini'))}}
								</td>
							</tr>
							<tr>
								<td>
									KN2 (%)
								</td>
								<td>
									{{
										Form::text
										(
											'KN2',
											Input::old('KN2'),
											array
											(
												'class'=>'input-mini'
											)
										)
									}}
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Tim pelacak</label>
					<div class="controls">
						<table class="table table-bordered petugas_pelacak">
							
						</table>
				</br>
				<div id="R_pelacak">
					<table class="table">
						<tr>
							<td>Nama</td>
							<td><input type="text" class="nama_pelacak"></td>
						</tr>
						<tr>
							<td>Jabatan</td>
							<td><input type="hidden" class="id"><input type="text" class="profesi_pelacak"></td>
						</tr>
					</table>
					<br/>
					<a class="btn tutup_pelacak">Tutup</a>&nbsp;
					<button class="btn btn-success simpan_pelacak">Tambah petugas</button>
					<button class="btn btn-success edit_pelacak">edit petugas</button>
				</div>
				<a class="btn show_pelacak">Tampilkan untuk tambah petugas</a>
			</div>
		</div>
	</div>
	</fieldset>
	<!-- akhir input kolom riwayat persalinan -->

	<div class="alert alert-error" id="stop" style="text-align:center">Stop pelacakan</div>
	
	<!-- btn -->
	<div class="form-actions" style="text-align:center">
		{{
			Form::submit
			(
				'simpan',
				array
				(
					'class'=>'btn btn-primary'
				)
			)
		}}
		<a href="{{URL::to('tetanus')}}" class="btn btn-defauld">Batal</a>
	@include('pemeriksaan.campak.analisis-js')
	</div>
	<!-- btn -->

	<!-- JS -->
	{{Form::close()}}
		<script>
			$(document).ready(function(){
				$('.bayi_meninggal').on('change',function(){
					var val = $(this).val();
					if(val == '0'){
						$('.tanggal_meninggal').removeAttr('disabled');
					}else{
						$('.tanggal_meninggal').val(null);
						$('.tanggal_meninggal').attr('disabled','disabled');
					}
					return false;
				});

				$('.edit_petugas').hide();
				$('#stop').hide();
				$('.edit_petugas_kehamilan').hide();
				$('.edit_pelacak').hide();
				$('.petugas_kehamilan').load('{{URL::to("petugas_kehamilan/'+$('.no_epid').val()+'")}}');
				$('.petugas_pelacak').load('{{URL::to("petugas_pelacak/'+$('.no_epid').val()+'")}}');
				$("#R_kehamilan").hide();
				$('#rawat').hide();
				$("#R_pelacak").hide();
				$('#nama_sumber_laporan').attr('disabled','disabled');
				$('#nama_faskes_bayi_dirawat').attr('disabled','disabled');
				$('#tahun_Ppertama').attr('disabled','disabled');
				$('#tahun_Pkedua').attr('disabled','disabled');
				$('#tahun_Hpertama').attr('disabled','disabled');
				$('#tahun_Hkedua').attr('disabled','disabled');
				$('#ibu_mendapat_imunisasi_2').on('change',function(){
					var id = $('#ibu_mendapat_imunisasi_2').val();
					if(id=='0' || id=='1'){
						$('#tahun_Hpertama').removeAttr('disabled');
						$('#tahun_Hkedua').removeAttr('disabled');
					}else{
						$('#tahun_Hpertama').attr('disabled','disabled');
						$('#tahun_Hkedua').attr('disabled','disabled');
					};
				});

				$('#ibu_mendapat_suntikan_pengantin').on('change',function(){
					var id = $('#ibu_mendapat_suntikan_pengantin').val();
					if(id=='0' || id=='1'){
						$('#tahun_Ppertama').removeAttr('disabled');
						$('#tahun_Pkedua').removeAttr('disabled');
					}else{
						$('#tahun_Ppertama').attr('disabled','disabled');
						$('#tahun_Pkedua').attr('disabled','disabled');
					};
				});

				for (var i = 1; i <= 5; i++) {
					$('#id_periksa_'+i).attr('disabled','disabled');
				};

				$(".show").click(function(e){
					e.preventDefault();
					$("#R_persalinan").show(1000);
					$('.show').hide();
				});

				$(".show_pelacak").click(function(e){
					e.preventDefault();
					$("#R_pelacak").show(1000);
					$('.show_pelacak').hide();
				});

				$(".show_petugas_kehamilan").click(function(e){
					e.preventDefault();
					$("#R_kehamilan").show(1000);
					$('.show_petugas_kehamilan').hide();
				});

				
				$(".tutup_petugas_kehamilan").click(function(e){
					e.preventDefault();
					$("#R_kehamilan").hide(2000);
					$('.nama_petugas_kehamilan').val('');
			        $('.profesi_petugas_kehamilan').val('');
			        $('.alamat_petugas_kehamilan').val('');
			        $('.frekuensi').val('');
					$('.show_petugas_kehamilan').show();
					$('.edit_petugas_kehamilan').hide();
					$('.simpan_petugas_kehamilan').show();
				});

				$(".tutup_pelacak").click(function(e){
					e.preventDefault();
					$("#R_pelacak").hide(2000);
					$('.nama_pelacak').val('');
			        $('.profesi_pelacak').val('');;
					$('.show_pelacak').show();
					$('.edit_pelacak').hide();
					$('.simpan_pelacak').show();
				});

				
				$(".simpan_pelacak").click(function(e){
					e.preventDefault();
					$.ajax({
			    	data:'no_epid='+$('.no_epid').val()+'&nama='+$('.nama_pelacak').val()+'&profesi='+$('.profesi_pelacak').val(),
			        url:'{{URL::to("tambah_pelacak")}}',
			        success:function(data) {
			        $('.nama_pelacak').val('');
			        $('.profesi_pelacak').val('');
			        $("#R_pelacak").hide(2000);
					$('.show_pelacak').show(1000)
					if(data=='sukses') 
					{
						$('.petugas_pelacak').load('{{URL::to("petugas_pelacak/'+$('.no_epid').val()+'")}}');
						
					}
			   		}
			  	});
				});

				$(".edit_pelacak").click(function(e){
					e.preventDefault();
					$.ajax({
			    	data:'id='+$('.id').val()+'&nama='+$('.nama_pelacak').val()+'&profesi='+$('.profesi_pelacak').val(),
			        url:'{{URL::to("edit_pelacak")}}',
			        success:function(data) {
			        $('.id_pelacak').val('');
			        $('.nama_pelacak').val('');
			        $('.profesi_pelacak').val('');
			        $("#R_pelacak").hide(2000);
					$('.show_pelacak').show(1000)
					if(data=='sukses') 
					{
						$('.petugas_pelacak').load('{{URL::to("petugas_pelacak/'+$('.no_epid').val()+'")}}');
						$('.edit_pelacak').hide();
						$('.simpan_pelacak').show();
					}
			   		}
			  	});
				});

				$(".simpan_petugas_kehamilan").click(function(e){
					e.preventDefault();
					$.ajax({
			    	data:'no_epid='+$('.no_epid').val()+'&nama_petugas_kehamilan='+$('.nama_petugas_kehamilan').val()+'&profesi_petugas_kehamilan='+$('#profesi').val()+'&alamat_petugas_kehamilan='+$('.alamat_petugas_kehamilan').val()+'&frekuensi='+$('.frekuensi').val(),
			        url:'{{URL::to("tambah_petugas_kehamilan")}}',
			        success:function(data) {
			        $('.nama_petugas_kehamilan').val('');
			        $('#profesi').val('');
			        $('.alamat_petugas_kehamilan').val('');
			        $('.frekuensi').val('');
			        $("#R_kehamilan").hide(2000);
					$('.show_petugas_kehamilan').show(1000)
					if(data=='sukses') 
					{
						$('.petugas_kehamilan').load('{{URL::to("petugas_kehamilan/'+$('.no_epid').val()+'")}}');
						
					}
			   		}
			  	});
				});

				$(".edit_petugas_kehamilan").click(function(e){
					e.preventDefault();
					$.ajax({
			    	data:'id='+$('.id_petugas_kehamilan').val()+'&nama_petugas_kehamilan='+$('.nama_petugas_kehamilan').val()+'&profesi_petugas_kehamilan='+$('#profesi').val()+'&alamat_petugas_kehamilan='+$('.alamat_petugas_kehamilan').val()+'&frekuensi='+$('.frekuensi').val(),
			        url:'{{URL::to("edit_petugas_kehamilan")}}',
			        success:function(data) {
			        $('.id').val('');
			        $('.nama_petugas_kehamilan').val('');
			        $('#profesi').val('');
			        $('.alamat_petugas_kehamilan').val('');
			        $('.frekuensi').val('');
			        $("#R_kehamilan").hide(2000);
					$('.show_petugas_kehamilan').show(1000)
					if(data=='sukses') 
					{
						$('.petugas_kehamilan').load('{{URL::to("petugas_kehamilan/'+$('.no_epid').val()+'")}}');
						$('.edit_petugas_kehamilan').hide();
						$('.simpan_petugas_kehamilan').show();
					}
			   		}
			  	});
				});

			});
		//fungsi edit petugas kehamilan pada modul PE tetanus
				function edit_petugas_kehamilan(id)
				{
				    $.ajax({
				        //dataType:'json',
				        data: 'id='+id,
				        url:'{{URL::to("tampil_edit_petugas_kehamilan")}}',
				        success:function(data) {
				            $('.id_petugas_kehamilan').val(data.id);
				            $('.nama_petugas_kehamilan').val(data.nama_pemeriksa);
				            $('#profesi').val(data.profesi);
				            $('.alamat_petugas_kehamilan').val(data.alamat);
				            $('.frekuensi').val(data.frekuensi);
				            $("#R_kehamilan").show(2000);
				            $('.show_petugas_kehamilan').hide()
				            $('.edit_petugas_kehamilan').show();
				            $('.simpan_petugas_kehamilan').hide();
				        }
				    });
				    return false;
				}

				//fungsi hapus petugas kehamilan pada modul PE tetanus
				function hapus_petugas_kehamilan(id)
				{
				    $.ajax({
				        //dataType:'json',
				        data: 'id='+id,
				        url:'{{URL::to("hapus_petugas_kehamilan")}}',
				        success:function(data) {
				            if(data=='sukses') 
				            {
				                $('.petugas_kehamilan').load('{{URL::to("petugas_kehamilan/'+$('.no_epid').val()+'")}}');
				                
				            }
				        }
				    });
				    return false;
				}

				
				//fungsi edit petugas pelacak pada modul PE tetanus
				function edit_pelacak(id)
				{
				    $.ajax({
				        //dataType:'json',
				        data: 'id='+id,
				        url:'{{URL::to("tampil_edit_petugas_pelacak")}}',
				        success:function(data) {
				            $('.id').val(data.id);
				            $('.nama_pelacak').val(data.nama_pemeriksa);
				            $('.profesi_pelacak').val(data.profesi);
				            $("#R_pelacak").show(2000);
				            $('.show_pelacak').hide()
				            $('.edit_pelacak').show();
				            $('.simpan_pelacak').hide();
				        }
				    });
				    return false;
				}

				
				//fungsi hapus petugas pemeriksa pada modul PE tetanus
				function hapus_pelacak(id)
				{
				    $.ajax({
				        //dataType:'json',
				        data: 'id='+id,
				        url:'{{URL::to("hapus_pelacak")}}',
				        success:function(data) {
				            if(data=='sukses') 
				            {
				                $('.petugas_pelacak').load('{{URL::to("petugas_pelacak/'+$('.no_epid').val()+'")}}');
				                
				            }
				        }
				    });
				    return false;
				}
				
				function Periksa(id)
				{

					var cek =$('#periksa_'+id).is(':checked');
				    if(cek){
				        $('#id_periksa_'+id).removeAttr('disabled');
				    }
				    else {
				        $('#id_periksa_'+id).attr('disabled','disabled');
				    }
				}

				function pilihFaskes()
				{
					
					var id = $('#id_faskes_bayi_dirawat').val();
					if (id!='') {
				        $('#nama_faskes_bayi_dirawat').removeAttr('disabled');
				    }
				    else {
				        $('#nama_faskes_bayi_dirawat').attr('disabled','disabled');
				    }
				}

				function diRawat()
				{
					
					var id = $('#id_dirawat').val();
					if (id=='0') {
				        $("#rawat").show(2000);
				    }
				    else if (id=='1') {
				        $("#rawat").hide(2000);
				    }
				    else{
				    	$("#rawat").hide(2000);
				    }
				}

				function cekBayiLahir() {

					var id = $('#id_bayi_lahir').val();
					if (id=='0') {
				        $("#bayi_lahir").show(2000);
				        $('#hide_kehamilan').show(2000);
				        $('#hide_persalinan').show(2000);
				        $('#stop').hide();
				    }
				    else if (id=='1') {
				        $("#bayi_lahir").hide(2000);
				        $('#hide_kehamilan').hide(2000);
				        $('#hide_persalinan').hide(2000);
				        $('#stop').show();
				    }
				    else{
				    	$("#bayi_lahir").hide(2000);
				    	$('#hide_kehamilan').hide(2000);
				    	$('#hide_persalinan').hide(2000);
				    	$('#stop').show();
				    }
				}
				function cekLahir() {

					var id = $('#id_lahir').val();
					if (id=='0') {
				        $("#lahir").show(2000);
				        $('#hide_kehamilan').show(2000);
				        $('#hide_persalinan').show(2000);
				        $('#stop').hide();
				    }
				    else if (id=='1' || id=='2') {
				        $("#lahir").hide(2000);
				        $('#hide_kehamilan').hide(2000);
				        $('#hide_persalinan').hide(2000);
				        $('#stop').show();
				    }
				    else{
				    	$("#lahir").hide(2000);
				    	$('#hide_kehamilan').hide(2000);
				    	$('#hide_persalinan').hide(2000);
				    	$('#stop').show();
				    }
				}
			</script>
				</div>
			</div>
	</div>
</div>
@stop