@extends('layouts.master')
@section('content')
<!-- awal class span12 -->
<div class="span12">
    <!-- awal class content -->
    <div class="content">
        <!-- awal class module -->
        <div class="module">
            <!-- awal class module-body -->
            <div class="module-body">
                <div class="profile-head media">
                    <h4>
                        Form Edit Penyakit Tetanus Neonatorum
                    </h4>
                    <hr>
                </div>

                <!-- awal form -->
                @foreach($tetanus as $hasil)
                {{
                    Form::open
                    (
                        array
                        (
                            'url'=>'tetanus_update',
                            'class'=>'form-horizontal'
                        )
                    )
                }}
                
                <!-- awal input kolom identitas pasien -->
                <fieldset>
                    <legend>Identitas pasien</legend>
                    <div class="control-group">
                        <label class="control-label">Nama anak</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'nama_anak',
                                    $hasil->nama_anak, 
                                    array
                                    (
                                        'class' => 'input-xlarge',
                                        'placeholder'=>'Nama anak'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">NIK</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'nik',
                                    $hasil->nik, 
                                    array
                                    (
                                        'class' => 'input-medium',
                                        'placeholder'=>'Nomer induk kependudukan'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Nama orang tua</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'nama_ortu',
                                    $hasil->nama_ortu, 
                                    array
                                    (
                                        'class' => 'input-xlarge',
                                        'placeholder'=>'Nama orang tua'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Jenis kelamin</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'jenis_kelamin',
                                    [
                                        
                                        null=>'Pilih',
                                        '1'=>'Laki-laki',
                                        '2'=>'Perempuan',
                                        '3'=>'Tidak Jelas'
                                    ],
                                    [$hasil->jenis_kelamin], 
                                    array
                                    (
                                        'class' => 'input-medium'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Tanggal lahir</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'tanggal_lahir',
                                    Helper::getDate($hasil->tanggal_lahir), 
                                    array
                                    (
                                        'class' => 'input-medium tgl_lahir',
                                        'placeholder'=>'Tanggal lahir',
                                        'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
                                        'onchange'=>'usia()'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                      <label class="control-label">Umur</label>
                        <div class="controls">
                            <input type="text" class="input-mini"  value="{{$hasil->umur}}" name="tgl_tahun" id="tgl_tahun">
                            Thn 
                            <input type="text" class="input-mini" value="{{$hasil->umur_bln}}" name="tgl_bulan" id="tgl_bulan">
                            Bln 
                            <input type="text" class="input-mini" value="{{$hasil->umur_hr}}" name="tgl_hari" id="tgl_hari">
                            Hr
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Provinsi</label>
                        <div class="controls">
                        <select name="" class="input-medium" id="id_provinsi" onchange="showKabupaten()">
                            <?php $pro=Provinsi::lists('provinsi','id_provinsi'); 
                             foreach ($pro as $id_provinsi => $provinsi) {
                                if($provinsi==$hasil->provinsi) {
                                  ?>
                                  <option value="{{$id_provinsi}}" selected="selected">{{$provinsi}}</option>
                                  <?php
                                } else {
                                ?>
                                  <option value="{{$id_provinsi}}">{{$provinsi}}</option>
                                <?php
                                }
                              } 
                            ?>
                            </select>
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Kabupaten/Kota</label>
                        <div class="controls">
                            <select name="" class="input-medium" id="id_kabupaten" onchange="showKecamatan()">
                                <option value="{{$hasil->id_kabupaten}}">{{$hasil->kabupaten}}</option>
                            </select>
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Kecamatan</label>
                        <div class="controls">
                            <select name="" class="input-medium" id="id_kecamatan" onchange="showKelurahan()">
                                <option value="{{$hasil->id_kecamatan}}">{{$hasil->kecamatan}}</option>
                            </select>
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Kelurahan/Desa</label>
                        <div class="controls">
                            <select name="id_kelurahan" class="input-medium" id="id_kelurahan" onchange="showEpidTetanus()">
                                <option value="{{$hasil->id_kelurahan}}">{{$hasil->kelurahan}}</option>
                            </select>
                          <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Alamat</label>
                        <div class="controls">
                            {{
                                Form::textarea
                                (
                                    'alamat',
                                    $hasil->alamat,
                                    array
                                    (
                                        'rows'=>'7'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">No Epidemologi</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'no_epid',
                                    $hasil->no_epid, 
                                    array
                                    (
                                        'class' => 'input-medium',
                                        'placeholder'=>'Nomer epidemologi',
                                        'id'=>'epid'
                                    )
                                )
                            }}
                            {{
                                Form::hidden
                                (
                                    'id_pasien_tetanus',
                                    $hasil->id_pasien_tetanus
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">No Epidemologi lama</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'no_epid_lama',
                                    $hasil->no_epid_lama, 
                                    array
                                    (
                                        'class' => 'input-medium',
                                        'placeholder'=>'Nomer epidemologi lama',
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                </fieldset>
                <!-- akhir input kolom identitas pasien -->

                <!-- awal input kolom data surveilans tetanus -->
                <fieldset>
                    <legend>Data surveilans tetanus neonatorum</legend>
                    <div class="control-group">
                        <label class="control-label">Tanggal mulai sakit</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'tanggal_mulai_sakit',
                                    date
                                    (
                                        'd-m-Y',strtotime($hasil->tanggal_mulai_sakit)
                                    ),
                                    array
                                    (
                                        'class'=>'input-medium tgl_mulai_sakit',
                                        'id'=>'tgl_mulai_sakit',
                                        'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
                                        'onchange'=>'usia(),showEpidTetanus()'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                    <label class="control-label">Tanggal laporan diterima</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'tanggal_laporan_diterima',
                                    Helper::getDate($hasil->tanggal_laporan_diterima),
                                    array
                                    (
                                        'class'=>'input-medium',
                                        'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Tanggal pelacakan</label>
                        <div class="controls">
                            {{
                                Form::text
                                (
                                    'tanggal_pelacakan',
                                    Helper::getDate($hasil->tanggal_pelacakan),
                                    array
                                    (
                                        'class'=>'input-medium',
                                        'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Antenatal care (ANC)</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'ANC',
                                    [
                                        ''=>'Pilih',
                                        '0'=>'Dokter',
                                        '1'=>'Bidan/Perawat',
                                        '2'=>'Dukun',
                                        '3'=>'Tidak ANC',
                                        '4'=>'Tidak jelas'
                                    ],
                                    [$hasil->ANC],
                                    array
                                    (
                                        'class'=>'input-medium'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Status imunisasi ibu</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'status_imunisasi',
                                    [
                                        ''=>'Pilih',
                                        '0'=>'TT2+',
                                        '1'=>'TT1',
                                        '2'=>'Tidak imunisasi',
                                        '3'=>'Tidak jelas'
                                    ],
                                    [$hasil->status_imunisasi],
                                    array
                                    (
                                        'class'=>'input-medium'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Penolong persalinan</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'penolong_persalinan',
                                    [
                                        ''=>'Pilih',
                                        '0'=>'Dokter',
                                        '1'=>'Bidan/Perawat',
                                        '2'=>'Dukun',
                                        '3'=>'Tidak jelas'
                                    ],
                                    [$hasil->penolong_persalinan],
                                    array
                                    (
                                        'class'=>'input-medium'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Pemotongan tali pusat</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'pemotongan_tali_pusat',
                                    [
                                        ''=>'Pilih',
                                        '0'=>'Gunting',
                                        '1'=>'Bambu',
                                        '2'=>'Lain-lain',
                                        '3'=>'Tidak jelas'
                                    ],
                                    [$hasil->pemotongan_tali_pusat],
                                    array
                                    (
                                        'class'=>'input-medium'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Rawat rumah sakit</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'rawat_rumah_sakit',
                                    [
                                        ''=>'Pilih',
                                        '0'=>'Ya',
                                        '1'=>'Tidak',
                                        '2'=>'Tidak jelas'
                                    ],
                                    [$hasil->rawat_rumah_sakit],
                                    array
                                    (
                                        'class'=>'input-medium'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label">Keadaan akhir</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'keadaan_akhir',
                                    [
                                        '0' => 'Hidup',
                                        '1'=>'Meninggal'
                                    ],
                                    [$hasil->keadaan_akhir], 
                                    array
                                    (
                                        'class' => 'input-small'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>
                </fieldset>
                <!-- akhir input kolom data surveilans tetanus -->

                <!-- awal input kolom klasifikasi final -->
                <fieldset>
                    <legend>Klasifikasi final</legend>
                    <div class="control-group">
                        <label class="control-label">Klasifikasi Final</label>
                        <div class="controls">
                            {{
                                Form::select
                                (
                                    'klasifikasi_akhir',
                                    [
                                        ''=>'Pilih',
                                        '1'=>'Konfirm TN',
                                        '2'=>'Tersangka TN',
                                        '3'=>'Bukan TN'
                                    ],
                                    [$hasil->klasifikasi_akhir], 
                                    array
                                    (
                                        'class' => 'input-medium'
                                    )
                                )
                            }}
                            <span class="help-inline"></span>
                        </div>
                    </div>
                </fieldset>
                <!-- akhir input kolom klasifikasi final -->

                <!-- btn -->
                <div class="form-actions">
                    {{
                        Form::submit
                        (
                            'simpan',
                            array
                            (
                                'class'=>'btn btn-primary'
                            )
                        )
                    }}
                    <a href="{{URL::to('tetanus')}}" class="btn btn-default">Kembali</a>
                </div>
                <br/>
                <script>
                    $(document).ready(function() 
                    {
                        $("select").select2();
                        showKabupaten();
                        showKecamatan();
                        showKelurahan();
                    });
                    function showKabupaten(){
                      var provinsi = $("#id_provinsi").val();
                      var kabupaten = $("#id_kabupaten").val();
                      // //kirim data ke server
                      $('.lodingKab').html('<img src="{{URL::to("style/images/loding.gif")}}">');
                      $.post('{{URL::to("provinsi/getkab")}}', {provinsi:provinsi,kabupaten:kabupaten}, function(response)
                      {
                          $('#id_kabupaten').removeAttr('disabled','');
                          $("#id_kabupaten").html(response);
                          $('.lodingKab').html('');
                      });

                    }

                    function showKecamatan(){
                      var kabupaten = $("#id_kabupaten").val();
                      var kecamatan = $("#id_kecamatan").val();
                      // //kirim data ke server
                      $('.lodingKec').html('<img src="{{URL::to("style/images/loding.gif")}}">');
                      $.post('{{URL::to("provinsi/getkec")}}', {kabupaten:kabupaten,kecamatan:kecamatan}, function(response)
                      {
                          $('#id_kecamatan').removeAttr('disabled','');
                          $("#id_kecamatan").html(response);
                          $('.lodingKec').html('');
                      });

                    }

                    function showKelurahan(){
                      var kecamatan= $("#id_kecamatan").val();
                      var kelurahan = $("#id_kelurahan").val();
                      // //kirim data ke server
                      $('.lodingKel').html('<img src="{{URL::to("style/images/loding.gif")}}">');
                      $.post('{{URL::to("provinsi/getkel")}}', {kecamatan:kecamatan,kelurahan:kelurahan}, function(response)
                      { 
                          $('#id_kelurahan').removeAttr('disabled','');
                          $("#id_kelurahan").html(response);
                          $('.lodingKel').html('');
                      });

                    }
                </script>
                <br/>
                {{Form::close()}}
                @endforeach()
                <!-- akhir form -->
            </div>
            <!-- akhir module-body -->
        </div>
        <!-- akhir class module -->
    </div>
    <!-- akhir class content -->
</div>
<!-- akhir class span12 -->

<!-- begin JS -->
<script type="text/javascript">
//fungsi js untuk mendapatkan no_epid tetanus scara otomatis
function showEpidTetanus() {

  $.ajax({
    data:'id_kelurahan='+$('#id_kelurahan').val()+'&date='+$('.tgl_mulai_sakit').val(),
    url:'{{URL::to("ambil_epid_tetanus")}}',
    success:function(data) {

       $('#epid').val(data);
   }
  });

}
function umur()
{

  $.ajax({

    data:'tanggal_lahir='+$('.tgl_lahir').val(),
    url:'{{URL::to("hitung/umur")}}',
    success:function(data) {

        //alert(data.tgl_tahun);
        
        $('#tgl_tahun').val(data.tgl_tahun);
        $('#tgl_bulan').val(data.tgl_bulan);
        $('#tgl_hari').val(data.tgl_hari);
    }
  });
}
</script>

<!-- end JS -->
@stop