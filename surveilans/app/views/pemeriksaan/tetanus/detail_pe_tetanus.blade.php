@extends('layouts.master')
@section('content')
<div class="span12">
		<div class="content">
				<div class="module">
						<div class="module-body">
								<div class="profile-head media">
										<h4>
												Detail penyelidikan epidemologi kasus penyakit tetanus
										</h4>
										<hr>
								</div>
<div class="tabbable-panel">
	<div class="tabbable-line">
		<ul class="nav nav-tabs ">
			<li class="active">
				<a href="#tab_default_1" data-toggle="tab">
				Identitas pelapor </a>
			</li>
			<li>
				<a href="#tab_default_2" data-toggle="tab">
				Identitas bayi </a>
			</li>
			<li>
				<a href="#tab_default_3" data-toggle="tab">
				Informasi Riwayat Kesakitan/Kematian Bayi 3-28 Hari
				</a>
			</li>
			<li>
				<a href="#tab_default_4" data-toggle="tab">
				Riwayat kehamilan
				</a>
			</li>
			<li>
				<a href="#tab_default_5" data-toggle="tab">
				Riwayat persalinan
				</a>
			</li>
		</ul>
		@if($data)
		@foreach($data as $row)
		<div class="tab-content">
		<div class="tab-pane active" id="tab_default_1">
			<table class="table table-striped">
		      <tr>
		          <td>No. Epidemologi</td>
		          <td>{{$row->no_epid}}</td>
		      </tr>
		      <tr>
		      <td>Sumber laporan</td>
		          <td>
			          <?php 
			          if ($row->sumber_laporan==0) {
			          	echo 'Rumah sakit';
			          } 
			          elseif ($row->sumber_laporan==1) {
			          	echo "Puskesmas";
			          }
			          elseif ($row->sumber_laporan==2) {
			          	echo "Praktek swasta";
			          }
			          elseif ($row->sumber_laporan==3) {
			          	echo $row->nama_sumber_laporan;
			          }
			          ?>
		          </td>
		      </tr>
		      <tr>
		          <td>Tanggal laporan diterima</td>
		          <td>{{date('d-m-Y',strtotime($row->tanggal_laporan_diterima))}}</td>
		      </tr>
		      <tr>
		          <td>Tanggal pelacakan</td>
		          <td>{{date('d-m-Y',strtotime($row->tanggal_pelacakan))}}</td>
		      </tr>
			</table>
		</div>
		<div class="tab-pane" id="tab_default_2">
				<table class="table table-striped">
			      <tr>
			          <td>Nama bayi</td>
			          <td>{{$row->nama_anak}}</td>
			      </tr>
			      <tr>
			          <td>
			          	@if($row->jenis_kelamin=='1')
				          Laki-laki
				          @elseif($row->jenis_kelamin=='2')
				          Perempuan
				          @elseif($row->jenis_kelamin=='3')
				          Tidak jelas
				          @else
				          -
				          @endif 
			          </td>
			      </tr>
			      <tr>
			          <td>Tanggal Lahir</td>
			          <td>{{date('d-m-Y',strtotime($row->tanggal_lahir))}}</td>
			      </tr>
			      <tr>
			          <td>Umur</td>
			          <td>{{$row->umur_hr}} hari</td>
			      <tr>
			          <td>Anak ke</td>
			          <td>{{$row->anak_ke}}</td>
			      </tr>
			      <tr>
			          <td>Nama ayah</td>
			          <td>{{$row->nama_ayah}}</td>
			      </tr>
			      <tr>
			          <td>Umur ayah</td>
			          <td>{{$row->umur_ayah}}</td>
			      </tr>
			      <tr>
			          <td>Tahun pendidikan ayah</td>
			          <td>{{$row->th_pendidikan_ayah}}</td>
			      </tr>
			      <tr>
			          <td>pekerjaan ayah</td>
			          <td>{{$row->pekerjaan_ayah}}</td>
			      </tr>
			      <tr>
			          <td>Nama ibu</td>
			          <td>{{$row->nama_ibu}}</td>
			      </tr>
			      <tr>
			          <td>Tahun pendidikan ibu</td>
			          <td>{{$row->th_pendidikan_ibu}}</td>
			      </tr>
			      <tr>
			          <td>Alamat</td>
			          <td>{{$row->alamat}}</td>
			      </tr>
				</table>
			</div>
			<div class="tab-pane" id="tab_default_3">
				<table class="table table-striped">
			      <tr>
			          <td>Nama yang diwawancarai</td>
			          <td>{{$row->nama_yang_diwawancarai}}</td>
			      </tr>
			      <tr>
			          <td>Hubungan dengan keluarga bayi</td>
			          <td>{{$row->hubungan_keluarga}}</td>
			      </tr>
			      <tr>
			          <td>Bayi lahir hidup</td>
			          <td><?php echo $retVal = ($row->bayi_hidup) ? 'Ya' : 'Tidak' ;?></td>
			      </tr>
			      <tr>
			          <td>Tanggal lahir bayi</td>
			          <td>{{date('d-m-Y',strtotime($row->tgl_lahir_bayi))}}</td>
			      </tr>
			      <tr>
			          <td>Tanggal mulai sakit</td>
			          <td>{{date('d-m-Y',strtotime($row->tanggal_mulai_sakit))}}</td>
			      </tr>
			      <tr>
			          <td>Apakah bayi meninggal</td>
			          <td><?php echo $retVal = ($row->bayi_meninggal) ? 'Ya' : 'Tidak' ;?></td>
			      </tr>
			      <tr>
			          <td>Tanggal meninggal</td>
			          <td><?php echo $retVal = ($row->tanggal_meninggal) ? 'Ya' : 'Tidak' ;?></td>
			      </tr>
			      <tr>
			          <td>Meninggal umur</td>
			          <td>{{$row->umur_mati}} Hari</td>
			      </tr>
			      <tr>
			          <td>Waktu lahir apakah bayi menangis</td>
			          <td>
			          <?php 
			          if ($row->bayi_menangis==0) {
			          	echo 'Ya';
			          } 
			          elseif ($row->bayi_menangis==1) {
			          	echo "Tidak";
			          }
			          else {
			          	echo "Tidak tahu";
			          }
			          ?></td>
			      </tr>
			      <tr>
			          <td>Apakah terlihat tanda-tanda kehidupan lain dari bayi (mis. Sedikit gerakan)</td>
			          <td><?php
			          echo $row->tanda_kehidupan_lain; 
			          if ($row->tanda_kehidupan_lain=='0') {
			          	echo 'Ya';
			          } 
			          elseif ($row->tanda_kehidupan_lain=='1') {
			          	echo "Tidak";
			          }
			          elseif($row->tanda_kehidupan_lain=='2') {
			          	echo "Tidak tahu";
			          }
			          elseif($row->tanda_kehidupan_lain=='NULL' || $row->tanda_kehidupan_lain=='') {
			          	echo "";
			          }
			          ?>
			          </td>
			      </tr>
			      <tr>
			          <td>Setelah lahir apakah bayi bisa menetek atau menghisap susu botol dengan baik</td>
			          <td>
			          <?php 
			          if ($row->bayi_menetek=='0') {
			          	echo 'Ya';
			          } 
			          elseif ($row->bayi_menetek=='1') {
			          	echo "Tidak";
			          }
			          elseif ($row->bayi_menetek=='2') {
			          	echo "Tidak tahu";
			          }
			          else {
			          	echo "";
			          }
			          ?>
			          </td>
			      </tr>
			      <tr>
			          <td>Apakah 3 hari kemudian tiba-tiba mulut bayi mencucu dan tidak bisa menetek?</td>
			          <td>
			          <?php 
			          if ($row->bayi_mecucu==0) {
			          	echo 'Ya';
			          } 
			          elseif ($row->bayi_mecucu==1) {
			          	echo "Tidak";
			          }
			          else {
			          	echo "Tidak tahu";
			          }
			          ?>
			          </td>
			      </tr>
			      <tr>
			          <td>Apakah bayi mudah kejang jika disentuh/terkena sinar atau bunyi?</td>
			          <td>
			          <?php 
			          if ($row->bayi_kejang==0) {
			          	echo 'Ya';
			          } 
			          elseif ($row->bayi_kejang==1) {
			          	echo "Tidak";
			          }
			          else {
			          	echo "Tidak tahu";
			          }
			          ?>
			          </td>
			      </tr>
			      <tr>
			          <td>Bayi dirawat</td>
			          <td>
			          <?php 
			          if ($row->dirawat==0) {
			          	echo 'Ya';
			          }
			          else {
			          	echo "Tidak";
			          }
			          ?>
			          </td>
			      </tr>
			      <tr>
			          <td>Dimana RS/Puskesmas</td>
			          <td>
			          <?php 
			          if ($row->tempat_berobat==0) {
			          	echo 'Rumah sakit';
			          } 
			          elseif ($row->tempat_berobat==1) {
			          	echo "Puskesmas";
			          }
			          elseif ($row->tempat_berobat==2) {
			          	echo "Dokter praktek swasta";
			          }
			          elseif ($row->tempat_berobat==3) {
			          	echo "Perawat/mantri/bidan";
			          }
			          else {
			          	echo "Tidak berobat";
			          }
			          ?>
			          </td>
			      </tr>
			      <tr>
			          <td>Tanggal mulai dirawat</td>
			          <td>{{date('d-m-Y',strtotime($row->tanggal_dirawat))}}</td>
			      </tr>
			      <tr>
			          <td>Keadaan bayi setelah dirawat</td>
			          <td>
			          <?php 
			          if ($row->keadaan_setelah_dirawat==0) {
			          	echo 'Sembuh';
			          } 
			          elseif ($row->keadaan_setelah_dirawat==1) {
			          	echo "Tidak sembuh";
			          }
			          else {
			          	echo "Meninggal";
			          }
			          ?>
			          </td>
			      </tr>
				</table>
			</div>
			<div class="tab-pane" id="tab_default_4">
			<tr><td>Petugas pemeriksa</td></tr>
				<table class="table table-bordered table-striped">
				<?php
					$table = '<tr>
								<td>Nama</td>
								<td>Profesi</td>
								<td>Alamat</td>
								<td>Frekuensi</td>
							  </tr>';
					$hasil = DB::select("select id,nama_pemeriksa,profesi,alamat,frekuensi from petugas_pemeriksa where identifikasi_petugas='K' and id_pe='".$row->id."'");
					
					foreach ($hasil as $value) {
						$table .= '<tr>
										<td>'.$value->nama_pemeriksa.'</td>
										<td>'.$value->profesi.'</td>
										<td>'.$value->alamat.'</td>
										<td>'.$value->frekuensi.'</td>
									</tr>';
					}
					echo $table;
					?>
				</table>
				</br>
				<table class="table table-striped">
				<tr>
					<td>Apakah ibu pernah mendapat imunisasi TT pada waktu hamil bayi ini?</td>
					<td>
					<?php 
			          if ($row->mendapat_imunisasi==0) {
			          	echo 'Ya';
			          }
			          else {
			          	echo "Tidak";
			          }
			          ?>
			          </td>
				</tr>
				<tr>
					<td>Sumber informasi imunisasi TT</td>
					<td>
					<?php 
			          if ($row->info_imunisasi==0) {
			          	echo 'Ingatan';
			          }
			          else {
			          	echo "Buku catatan";
			          }
			          ?>
			          </td>
				</tr>
				<tr>
					<td>Berapa kali mendapat imunisasi TT pada saat kehamilan bayi tersebut?</td>
					<td>
					<?php 
			          if ($row->jumlah_imunisasi==0) {
			          	echo 'Pertama kali';
			          }
			          else {
			          	echo "kedua kali";
			          }
			          ?>
			          </td>
				</tr>
				<tr>
					<td>Umur kehamilan saat mendapat imunisasi TT pertama (bulan)</td>
					<td>{{$row->umur_kehamilan_pertama}} bulan</td>
				</tr>
				<tr>
					<td>Umur kehamilan saat mendapat imunisasi TT kedua (bulan)</td>
					<td>{{$row->umur_kehamilan_kedua}} bulan</td>
				</tr> 
				<tr>
					<td>Tanggal imunisasi kedua saat kehamilan bayi tersebut?</td>
					<td>{{date('d-m-Y',strtotime($row->tanggal_imunisasi_kedua))}}</td>		
				</tr>
				
				<tr>
					<td>Pernahkah ibu mendapat suntikan TT calon pengantin</td>
					<td>
					<?php 
			          if ($row->ibu_mendapat_suntikan_pengantin==0) {
			          	echo 'Ya';
			          }
			          else {
			          	echo "Tidak";
			          }
			          ?>
			        </td>		
				</tr>
				<tr>
					<td>Status TT ibu pada saat kehamilan bayi tersebut</td>
					<td>
					<?php 
						if($row->status_ibu_saat_hamil==0) {
							echo "TT1"; 
						}
						elseif ($row->status_ibu_saat_hamil==1) {
							echo 'TT2';
						}
						elseif ($row->status_ibu_saat_hamil==2) {
							echo "TT3";
						}
						elseif ($row->status_ibu_saat_hamil==3) {
							echo "TT4";
						}
						else {
							echo 'TT5';
						}
					?>
					</td>
				</tr>   
				</table>
			</div>
			<div class="tab-pane" id="tab_default_5">
			<tr><td>Tim pelacak</td></tr>
				<table class="table table-bordered table-striped">
				<?php
					$table = '<tr>
								<td>Nama</td>
								<td>Profesi</td>
								<td>Alamat</td>
								<td>Frekuensi</td>
							  </tr>';
					$hasil = DB::table('petugas_pemeriksa')->select('id','nama_pemeriksa','profesi','alamat','frekuensi')->where('identifikasi_petugas','K')->where('id_pe',$row->id)->get();
					foreach ($hasil as $value) {
						$table .= '<tr>
										<td>'.$value->nama_pemeriksa.'</td>
										<td>'.$value->profesi.'</td>
										<td>'.$value->alamat.'</td>
										<td>'.$value->frekuensi.'</td>
									</tr>';
					}
					echo $table;
					?>
				</table>
				</br>
				<table class="table table-striped">
				<tr>
					<td>Obat yang dibubuhkan setelah tali pusat di potong</td>
					<td>
						<?php 
						if($row->obat_penyembuh==0) {
							echo "Alkohol/lod"; 
						}
						elseif ($row->obat_penyembuh==1) {
							echo "Ramuan tradisonal";
						}
						else {
							echo "Lainnya";
						}
					?>
					</td>
				</tr>
				<tr>
					<td>Jika jawaban ramuan tradisional atau lainnya</td>
					<td>{{$row->ramuan_lainnya}}</td>
				</tr>
				<tr>
					<td>Yang melakukan perawatan tali pusat awal-akhir</td>
					<td><?php echo $retVal = ($row->perawatan_tali_pusat) ? 'Tenaga kerja' : 'Bukan tenaga kerja' ; ?></td>
				</tr>
				<tr>
					<td>Obat atau ramuan apa saja yang dibubuhkan selama merawat tali pusat</td>
					@if($row->obat_merawat_tali_pusat=='0')
					<td>Alkohol/lod</td>
					@elseif($row->obat_merawat_tali_pusat=='1')
					<td>Ramuan tradisional</td>
					@elseif($row->obat_merawat_tali_pusat=='2')
					<td>Kasa kering</td>
					@elseif($row->obat_merawat_tali_pusat=='3')
					<td>Lainnya</td>
					@endif
				</tr>
				<tr>
					<td>Kesimpulan diagnosis</td>
					<td>
					<?php 
						if($row->kesimpulan_diagnosis==0) {
							echo "Konfirmasi TN";
						} 
						elseif ($row->kesimpulan_diagnosis==1) {
							echo "Tersangka TN";
						}
						else {
							echo "Bukan TN";
						}
					?>
					</td>
				</tr>
				<tr>
					<table class="table table-bordered">
						<caption>Cakupan imunisasi TT di desa kasus TN</caption>
						<tr>
							<td>TT1</td>
							<td>{{$row->TT1}}%</td>
						</tr>
						<tr>
							<td>TT2</td>
							<td>{{$row->TT2}}%</td>
						</tr>
						<tr>
							<td>TT3</td>
							<td>{{$row->TT3}}%</td>
						</tr>
						<tr>
							<td>TT4</td>
							<td>{{$row->TT4}}%</td>
						</tr>
						<tr>
							<td>TT5</td>
							<td>{{$row->TT5}}%</td>
						</tr>
					</table>
				</tr>
				<tr>
					<td>Cakupan persalinan tenaga kesehatan</td>
					<td>{{$row->cakupan_tenaga_kesehatan}} %</td>
				</tr>
				<tr>
					<table class="table table-bordered">
						<caption>Cakupan kunjungan neonatus</caption>
						<tr>
							<td>KN1</td>
							<td>{{$row->KN1}}%</td>
						</tr>
						<tr>
							<td>KN2</td>
							<td>{{$row->KN2}}%</td>
						</tr>
					</table>
				</tr>
				</table>
			</div>
	</div>
</div>
</div>
<div class="form-actions" style="text-align:center">
  <a href="{{URL::to('tetanus')}}" class="btn btn-warning">kembali</a>
  </div>
</div>
</div>

<script>
	$(document).ready(function(){
		$('.petugas_persalinan').load('{{URL::to("petugas_persalinan/".$row->id_pasien)}}');
		$('.petugas_kehamilan').load('{{URL::to("petugas_kehamilan/".$row->id_pasien)}}');
		$('.petugas_pelacak').load('{{URL::to("petugas_pelacak/".$row->id_pasien)}}');
	});
</script>
</div>
@endforeach
@endif
@stop