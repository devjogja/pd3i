@extends('layouts.master')
@section('content')
<div class="span12">
        <div class="content">
                <div class="module">
                        <div class="module-body">
                                <div class="profile-head media">
                                        <h4>
                                                Detail Pasien Kasus Penyakit Tetanus Neonatorum
                                        </h4>
                                        <hr>
                                </div>
<table class="table table-striped">
@if($tetanus)
@foreach($tetanus as $row)
    <tr>
        <td>No. Epidemologi</td>
        <td>{{$row->no_epid}}</td>
    </tr>
    <tr>
        <td>No. Epidemologi lama</td>
        <td>{{$row->no_epid_lama}}</td>
    </tr>
    <tr>
        <td>No induk kependudukan</td>
        <td>{{$row->nik}}</td>
    </tr>
    <tr>
        <td>Nama Pasien</td>
        <td>{{$row->nama_anak}}</td>
    </tr>
    <tr>
        <td>Nama Orang tua</td>
        <td>{{$row->nama_ortu}}</td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        <td>
        @if($row->jenis_kelamin=='1')
          Laki-laki
          @elseif($row->jenis_kelamin=='2')
          Perempuan
          @elseif($row->jenis_kelamin=='3')
          Tidak jelas
          @else
          -
          @endif 
    </tr>
    <tr>
        <td>Tanggal Lahir</td>
        <td>{{ date('d-m-Y',strtotime($row->tanggal_lahir))}}</td>
    </tr>
    <tr>
        <td>Umur</td>
        <td>{{ Pasien::getAgeAttribute($row->tanggal_lahir) }}</td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>{{$row->alamat}}</td>
    </tr>
</table>
<hr>
@endforeach
@endif
<br/>
<div class="module-body uk-overflow-container">
<div class="table-responsive">
<table class="table table-bordered" style="font-size:10px">
    <tr style="background-color:#eee;">
        <td>Tanggal mulai sakit</td>
        <td>Tanggal laporan diterima</td>
        <td>Tanggal pelacakan</td>
        <td>Antenatal care (ANC)</td>
        <td>Status imunisasi ibu</td>
        <td>Penolong persalinan</td>
        <td>Pemotongan tali pusat</td>
        <td>Rawat rumah sakit</td>
        <td>Keadaan akhir</td>
        <td>Klasifikasi akhir</td>
    </tr>
   @if($tetanus)
   @foreach($tetanus as $result)
    <tr>
        <td>{{ date('d-m-Y',strtotime($result->tanggal_mulai_sakit))}}</td>
        <td>{{ date('d-m-Y',strtotime($result->tanggal_laporan_diterima)) }}</td>
        <td>{{ date('d-m-Y',strtotime($result->tanggal_pelacakan)) }}</td>

        @if($result->ANC==0)
        <td>Dokter</td>
        @elseif($result->ANC==1)
        <td>Bidan/Perawat</td>
        @elseif($result->ANC==2)
        <td>Dukun</td>
        @elseif($result->ANC==3)
        <td>Tidak ANC</td>
        @elseif($result->ANC==4)
        <td>Tidak Jelas</td>
        @else
        <td>&nbsp;</td>
        @endif

        @if($result->status_imunisasi==0)
        <td>TT2+</td>
        @elseif($result->status_imunisasi==1)
        <td>TT1</td>
        @elseif($result->status_imunisasi==2)
        <td>Tidak imunisasi</td>
        @elseif($result->status_imunisasi==3)
        <td>Tidak jelas</td>
        @else
        <td>&nbsp;</td>
        @endif
        
        @if($result->penolong_persalinan==0)
        <td>Dokter</td>
        @elseif($result->penolong_persalinan==1)
        <td>Bidan/Perawat</td>
        @elseif($result->penolong_persalinan==2)
        <td>Dukun</td>
        @elseif($result->penolong_persalinan==3)
        <td>Tidak jelas</td>
        @else
        <td>&nbsp;</td>
        @endif
        
        @if($result->pemotongan_tali_pusat==0)
        <td>Gunting</td>
        @elseif($result->pemotongan_tali_pusat==1)
        <td>Bambu</td>
        @elseif($result->pemotongan_tali_pusat==2)
        <td>Silet</td>
        @elseif($result->pemotongan_tali_pusat==3)
        <td>Pisau</td>
        @elseif($result->pemotongan_tali_pusat==4)
        <td>Lain-lain</td>
        @elseif($result->pemotongan_tali_pusat==5)
        <td>Tidak tahu</td>
        @else
        <td>&nbsp;</td>
        @endif

        @if($result->rawat_rumah_sakit==0)
        <td>Ya</td>
        @elseif($result->rawat_rumah_sakit==1)
        <td>Tidak</td>
        @elseif($result->rawat_rumah_sakit==2)
        <td>Tidak jelas</td>
        @else
        <td>&nbsp;</td>
        @endif

        @if($result->keadaan_akhir==0)
        <td>Hidup/Sehat</td>
        @elseif($result->keadaan_akhir==1)
        <td>Meninggal</td>
        @else
        <td>&nbsp;</td>
        @endif

        @if($result->klasifikasi_akhir==1)
        <td>Konfirmasi TN</td>
        @elseif($result->klasifikasi_akhir==2)
        <td>Tersangka TN</td>
        @elseif($result->klasifikasi_akhir==3)
        <td>Bukan TN</td>
        @else
        <td>&nbsp;</td>
        @endif

    </tr>
  @endforeach
  @endif
</table>
</div>
<br>
<a href="{{URL::to('tetanus')}}" class="btn btn-primary">Kembali ke Daftar kasus</a>
</div>
</div>
</div>
@stop