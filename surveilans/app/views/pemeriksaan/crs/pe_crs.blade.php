@extends('layouts.master')
@section('content')
<!-- awal class span12 -->
<div class="span12">
    <!-- awal class content -->
    <div class="content">
        <!-- awal class module -->
        <div class="module">
            <!-- awal class module-body -->
            <div class="module-body">
                <div class="profile-head media">
                    <h4>
                        Input data penyelidikan epidemologi kasus CRS
                    </h4>
                    <hr>
                </div>
                {{Form::open(
                    array(
                        'route'=>'pe_crs',
                        'class'=>'form-horizontal'
                        )
                    )
                }}
                <?php $dt = $pe_crs[0];?>
                <input type="hidden" name="id_pe_crs" value="<?php echo $dt->id_pe_crs?>">
                <div class="module-body">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="media">
                                <fieldset>
                                    <legend>Riwayat Kontak</legend>
                                    <div class="control-group">
                                      <label class="control-label">Apakah selama kehamilan ini ibu pernah didiagnosa rubella dengan konfirmasi lab</label>
                                      <div class="controls">
                                      {{Form::select(
                                                'dpe[diagnosa_rubella]', 
                                                ['' => 'Pilih',
                                                'Ya'=>'Ya',
                                                'Tidak'=>'Tidak',
                                                'Tidak Tahu'=>'Tidak Tahu'],
                                                [$dt->diagnosa_rubella],
                                                ['id'=>'diagnosa_rubella',
                                                'onchange'=>'actiondate(this.id, this.value)',
                                                'class' => 'input-medium id_combobox']
                                        )
                                      }}&nbsp;
                                      {{ Form::text(
                                              'dpe[tgl_diagnosa_rubella]',
                                              Helper::getDate($dt->tgl_diagnosa_rubella),
                                              ['readonly'=>'readonly',
                                              'class' => 'input-large diagnosa_rubella_date', 
                                              'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
                                              'placeholder' => 'Kapan diagnosa dilakukan']
                                        )
                                      }}
                                      <span class="help-inline"></span>
                                      </div>
                                  </div>
                                  <div class="control-group">
                                      <label class="control-label">Apakah selama kehamilan ini ibu pernah terpapar atau berkontak dengan orang yang menderita ruam makulopapular ?</label>
                                      <div class="controls">
                                      {{Form::select(
                                            'dpe[kontak_ruam_makulopapular]', 
                                            ['' => 'Pilih',
                                            'Ya'=>'Ya',
                                            'Tidak'=>'Tidak',
                                            'Tidak Tahu'=>'Tidak Tahu',
                                            ],
                                            [$dt->kontak_ruam_makulopapular],
                                            ['id'=>'kontak_ruam_makulopapular',
                                            'onchange'=>'actiondate(this.id, this.value)',
                                            'class' => 'input-medium id_combobox']
                                            )
                                      }}
                                      <span class="help-inline"></span>
                                      </div>
                                  </div>
                                  <div class="control-group">
                                      <label class="control-label">Umur Kehamilan saat terpapar dengan penderita ruam makulopapular</label>
                                      <div class="controls">
                                      {{Form::selectRange(
                                            'dpe[umur_hamil_kontak_ruam_makulopapular]',
                                            0, 60,
                                            $dt->umur_hamil_kontak_ruam_makulopapular,
                                            array(
                                                'readonly'=>'readonly',
                                                  'class' => 'input-medium id_combobox kontak_ruam_makulopapular_date'
                                            ))
                                      }}&nbsp;
                                      {{ Form::textarea(
                                          'dpe[desc_kontak_ruam_makulopapular]',
                                          $dt->desc_kontak_ruam_makulopapular,
                                          ['readonly'=>'readonly',
                                          'rows' => '3',
                                          'class' => 'kontak_ruam_makulopapular_date', 
                                          'placeholder' => 'Dimana terkena kontak']
                                          )
                                      }}
                                      <span class="help-inline"></span>
                                      </div>
                                  </div>
                                  <div class="control-group">
                                      <label class="control-label">Apakah ibu bepergian selama kehamilan terakhir ini</label>
                                      <div class="controls">
                                      {{Form::select(
                                            'dpe[pergi_waktu_hamil]', 
                                            ['' => 'Pilih',
                                            'Ya'=>'Ya',
                                            'Tidak'=>'Tidak',
                                            'Tidak Tahu'=>'Tidak Tahu'],
                                            [$dt->pergi_waktu_hamil],
                                            ['id'=>'pergi_waktu_hamil',
                                            'onchange'=>'actiondate(this.id, this.value)',
                                            'class' => 'input-medium id_combobox']
                                            )
                                      }}
                                      <span class="help-inline"></span>
                                      </div>
                                  </div>
                                  <div class="control-group">
                                      <label class="control-label">Umur kehamilan ketika bepergian</label>
                                      <div class="controls">
                                      {{Form::selectRange(
                                            'dpe[umur_hamil_waktu_pergi]',
                                            0, 60,
                                            $dt->umur_hamil_waktu_pergi,
                                            array(
                                                'readonly'=>'readonly',
                                                  'class' => 'input-medium id_combobox pergi_waktu_hamil_date'
                                            ))
                                      }}&nbsp;
                                      {{ Form::textarea(
                                          'dpe[desc_pergi_waktu_hamil]',
                                          $dt->desc_pergi_waktu_hamil,
                                          ['readonly'=>'readonly',
                                          'rows'=>'3','class' => 'pergi_waktu_hamil_date', 
                                          'placeholder' => 'Bepergian kemana',
                                          ])
                                      }}
                                      <span class="help-inline"></span>
                                      </div>
                                  </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                     <div class="form-actions">
                        {{
                            Form::submit
                            (
                                'simpan',
                                array
                                (
                                    'class'=>'btn btn-primary'
                                )
                            )
                        }}
                        <a href="{{ URL::to('crs') }}" class="btn btn-warning">Kembali</a>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    checkactive();
    $("select").select2();

    function checkactive(){
      actiondate('diagnosa_rubella',$('#diagnosa_rubella').val());
      actiondate('kontak_ruam_makulopapular',$('#kontak_ruam_makulopapular').val());
      actiondate('pergi_waktu_hamil',$('#pergi_waktu_hamil').val());
    }
  });


  function actiondate(dt, val) {
        if (val == 'Ya') {
            $('.'+dt+'_date').removeAttr('readonly');
        } else{
            $('.'+dt+'_date').attr('readonly','readonly');
            $('.'+dt+'_date').val('');
        };
    }
</script>

@stop

