@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <h4>
                        Detail Pasien Kasus Penyakit CRS
                    </h4>
                    <hr>
                </div>
                @if($crs)
                <?php $dt = $crs[0];
                $pelapor_provinsi=DB::table('provinsi')->where('id_provinsi',$dt->pelapor_id_provinsi)->pluck('provinsi');
                $pelapor_kabupaten=DB::table('kabupaten')->where('id_kabupaten',$dt->pelapor_id_kabupaten)->pluck('kabupaten');
                ?>
                <div class="module-body uk-overflow-container">
                	<div class="table-responsive">
                		<table class="table table-striped">
                		<caption style="border:1px solid #eee;background-color:#666; color:#fff">Data Pelapor</caption>
                			<tr>
                				<td>Nama Rumah Sakit</td>
                				<td>{{$dt->pelapor_nama_rs}}</td>
                			</tr>
                			<tr>
                				<td>Provinsi</td>
                				<td>{{$pelapor_provinsi}}</td>
                			</tr>
                			<tr>
                				<td>Kabupaten</td>
                				<td>{{$pelapor_kabupaten}}</td>
                			</tr>
                			<tr>
                				<td>Tanggal Laporan</td>
                				<td>{{Helper::getDate($dt->pelapor_tanggal_laporan)}}</td>
                			</tr>
                			<tr>
                				<td>Tanggal Investigasi</td>
                				<td>{{Helper::getDate($dt->pelapor_tanggal_investigasi)}}</td>
                			</tr>
                		</table>
                	</div>
                </div>
                <div class="module-body uk-overflow-container">
                	<div class="table-responsive">
                		<table class="table table-striped">
                		<caption style="border:1px solid #eee;background-color:#666; color:#fff">Data Pasien</caption>
                			<tr>
                				<td>Nomor EPID</td>
                				<td>{{$dt->crs_no_epid}}</td>
                			</tr>
                			<tr>
                				<td>Nama Bayi</td>
                				<td>{{$dt->pasien_nama_anak}}</td>
                			</tr>
                			<tr>
                				<td>Jenis kelamin</td>
                				<td>
                                    @if($dt->pasien_jenis_kelamin=='1')
                                    Laki-laki
                                    @elseif($dt->pasien_jenis_kelamin=='2')
                                    Perempuan
                                    @elseif($dt->pasien_jenis_kelamin=='3')
                                    Tidak jelas
                                    @else
                                    -
                                    @endif            
                                </td>
                			</tr>
                			<tr>
                				<td>Tanggal lahir</td>
                				<td>{{Helper::getDate($dt->pasien_tanggal_lahir)}}</td>
                			</tr>
                			<tr>
                				<td>Umur</td>
                				<td>{{$dt->pasien_umur}} Tahun {{$dt->pasien_umur_bln}} Bulan {{$dt->pasien_umur_hr}} Hari</td>
                			</tr>
                			<tr>
                				<td>Alamat</td>
                				<td>{{$dt->alamat_new}}</td>
                			</tr>
                			<tr>
                				<td>Tempat bayi di lahirkan</td>
                				<td>{{$dt->pasien_tempat_lahir_bayi}}</td>
                			</tr>
                			<tr>
                				<td>Nama Ibu</td>
                				<td>{{$dt->pasien_nama_ortu}}</td>
                			</tr>
                			<tr>
                				<td>Umur Kehamilan saat bayi di lahirkan</td>
                				<td>{{$dt->pasien_umur_kehamilan_bayi}}</td>
                			</tr>
                			<tr>
                				<td>Berat badan bayi baru lahir</td>
                				<td>{{$dt->pasien_berat_badan_bayi}}</td>
                			</tr>
                		</table>
                	</div>
                </div>
                <div class="module-body uk-overflow-container">
                    <div class="table-responsive">
                        <table class="table table-striped">
                        <caption style="border:1px solid #eee;background-color:#666; color:#fff">Data Klinis Pasien</caption>
                            <tr>
                                <td>Nama Dokter Pemeriksa</td>
                                <td colspan="7">{{$dt->crs_nama_dokter_pemeriksa}}</td>
                            </tr>
                            <tr>
                                <td>Tanggal Periksa</td>
                                <td colspan="7">{{Helper::getDate($dt->crs_tanggal_periksa)}}</td>
                            </tr>
                            <tr>
                                <td>Keadaan Bayi saat ini</td>
                                <td colspan="7">
                                    {{$dt->crs_keadaan_akhir}}
                                    {{(empty($dt->crs_penyebab_meninggal))?'':'Penyebab Meninggal :'.$dt->crs_penyebab_meninggal}}
                                </td>
                            </tr>
                            <tr>
                                <td colspan="8">Group A</td>
                            </tr>
                            <tr>
                                <td style="text-align:center">Congenital heart disease</td>
                                <td style="text-align:center">Cataracts</td>
                                <td style="text-align:center">Congenital glaucoma</td>
                                <td style="text-align:center">Pigmentary retinopathy</td>
                                <td style="text-align:center">Hearing impairment</td>
                                <td colspan="3"></td>
                            </tr>
                            <tr>
                                <td style="text-align:center">{{(empty($dt->crs_congenital_heart_disease))?'-':$dt->crs_congenital_heart_disease}}</td>
                                <td style="text-align:center">{{(empty($dt->crs_cataracts))?'-':$dt->crs_cataracts}}</td>
                                <td style="text-align:center">{{(empty($dt->crs_congenital_glaucoma))?'-':$dt->crs_congenital_glaucoma}}</td>
                                <td style="text-align:center">{{(empty($dt->crs_pigmentary_retinopathy))?'-':$dt->crs_pigmentary_retinopathy}}</td>
                                <td style="text-align:center">{{(empty($dt->crs_hearing_impairment))?'-':$dt->crs_hearing_impairment}}</td>
                                <td colspan="3"></td>
                            </tr>
                            <tr>
                                <td colspan="8">Group B</td>
                            </tr>
                            <tr>
                                <td style="text-align:center">Purpura</td>
                                <td style="text-align:center">Microcephaly</td>
                                <td style="text-align:center">Meningoencephalitis</td>
                                <td style="text-align:center">Ikterik 24 jam post partum</td>
                                <td style="text-align:center">Splenomegaly</td>
                                <td style="text-align:center">Developmental delay</td>
                                <td style="text-align:center">Radiolucent bone disease</td>
                                <td style="text-align:center">Other abnormalities</td>
                            </tr>
                            <tr>
                                <td style="text-align:center">{{(empty($dt->crs_purpura))?'-':$dt->crs_purpura}}</td>
                                <td style="text-align:center">{{(empty($dt->crs_microcephaly))?'-':$dt->crs_microcephaly}}</td>
                                <td style="text-align:center">{{(empty($dt->crs_meningoencephalitis))?'-':$dt->crs_meningoencephalitis}}</td>
                                <td style="text-align:center">{{(empty($dt->crs_ikterik))?'-':$dt->crs_ikterik}}</td>
                                <td style="text-align:center">{{(empty($dt->crs_splenomegaly))?'-':$dt->crs_splenomegaly}}</td>
                                <td style="text-align:center">{{(empty($dt->crs_developmental_delay))?'-':$dt->crs_developmental_delay}}</td>
                                <td style="text-align:center">{{(empty($dt->crs_radiolucent))?'-':$dt->crs_radiolucent}}</td>
                                <td style="text-align:center">
                                    {{(empty($dt->crs_other_abnormal))?'-':$dt->crs_other_abnormal}}<br>
                                    {{(empty($dt->crs_dt_other_abnormal))?'':'Desc: '.$dt->crs_dt_other_abnormal}} 
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="module-body uk-overflow-container">
                    <div class="table-responsive">
                        <table class="table table-striped">
                        <caption style="border:1px solid #eee;background-color:#666; color:#fff">Data Riwayat Kehamilan Ibu</caption>
                            <tr>
                                <td style="width:25%">Jumlah kehamilan sebelumnya</td>
                                <td colspan="5">{{$dt->pe_jumlah_kehamilan_sebelumnya}}</td>
                            </tr>
                            <tr>
                                <td>Umur Ibu (dalam tahun)</td>
                                <td colspan="5">{{$dt->pe_umur_ibu}} Tahun</td>
                            </tr>
                            <tr>
                                <td>Hari Pertama haid terakhir (HPHT)</td>
                                <td colspan="5">{{Helper::getDate($dt->pe_hari_pertama_haid)}}</td>
                            </tr>
                            <tr>
                                <td colspan="6">Apakah selama kehamilan terakhir ini ibu pernah mengalami</td>
                            </tr>
                            <tr>
                                <td>Conjunctivitis</td>
                                <td style="text-align:center">{{(empty($dt->pe_conjunctivitis))?'-':$dt->pe_conjunctivitis}}</td>
                                <td style="text-align:center" colspan="4">{{Helper::getDate($dt->pe_conjunctivitis_date,'Tanggal Kejadian : ')}}</td>
                            </tr>
                            <tr>
                                <td>Pilek</td>
                                <td style="text-align:center">{{(empty($dt->pe_pilek))?'-':$dt->pe_pilek}}</td>
                                <td style="text-align:center" colspan="4">{{Helper::getDate($dt->pe_pilek_date,'Tanggal Kejadian : ')}}</td>
                            </tr>
                            <tr>
                                <td>Batuk</td>
                                <td style="text-align:center">{{(empty($dt->pe_batuk))?'-':$dt->pe_batuk}}</td>
                                <td style="text-align:center" colspan="4">{{Helper::getDate($dt->pe_batuk_date,'Tanggal Kejadian : ')}}</td>
                            </tr>
                            <tr>
                                <td>Ruam makulopapular</td>
                                <td style="text-align:center">{{(empty($dt->pe_ruam_makulopapular))?'-':$dt->pe_ruam_makulopapular}}</td>
                                <td style="text-align:center" colspan="4">{{Helper::getDate($dt->pe_ruam_makulopapular_date,'Tanggal Kejadian : ')}}</td>
                            </tr>
                            <tr>
                                <td>Pembengkakan kelenjar limfa</td>
                                <td style="text-align:center">{{(empty($dt->pe_pembengkakan_kelenjar_limfa))?'-':$dt->pe_pembengkakan_kelenjar_limfa}}</td>
                                <td style="text-align:center" colspan="4">{{Helper::getDate($dt->pe_pembengkakan_kelenjar_limfa_date,'Tanggal Kejadian : ')}}</td>
                            </tr>
                            <tr>
                                <td>Demam</td>
                                <td style="text-align:center">{{(empty($dt->pe_demam))?'-':$dt->pe_demam}}</td>
                                <td style="text-align:center" colspan="4">{{Helper::getDate($dt->pe_demam_date,'Tanggal Kejadian : ')}}</td>
                            </tr>
                            <tr>
                                <td>Arthralgia/arthritis</td>
                                <td style="text-align:center">{{(empty($dt->pe_arthralgia))?'-':$dt->pe_arthralgia}}</td>
                                <td style="text-align:center" colspan="4">{{Helper::getDate($dt->pe_arthralgia_date,'Tanggal Kejadian : ')}}</td>
                            </tr>
                            <tr>
                                <td>Komplikasi lain</td>
                                <td style="text-align:center">{{(empty($dt->pe_komplikasi_lain))?'-':$dt->pe_komplikasi_lain}}</td>
                                <td style="text-align:center" colspan="4">{{Helper::getDate($dt->pe_komplikasi_lain_date,'Tanggal Kejadian : ')}}</td>
                            </tr>
                            <tr>
                                <td>Mendapat Vaksinasi rubella</td>
                                <td style="text-align:center">{{(empty($dt->pe_vaksinasi_rubella))?'-':$dt->pe_vaksinasi_rubella}}</td>
                                <td style="text-align:center" colspan="4">{{Helper::getDate($dt->pe_vaksinasi_rubella_date,'Tanggal Kejadian : ')}}</td>
                            </tr>
                            <tr>
                                <td>Apakah selama kehamilan ini ibu pernah didiagnosa rubella dengan konfirmasi lab</td>
                                <td style="text-align:center">{{(empty($dt->pe_diagnosa_rubella))?'-':$dt->pe_diagnosa_rubella}}</td>
                                <td style="text-align:center" colspan="4">{{Helper::getDate($dt->pe_tgl_diagnosa_rubella, 'Pada tanggal : ')}}</td>
                            </tr>
                            <tr>
                                <td>Apakah selama kehamilan ini ibu pernah terpapar atau berkontak dengan orang yang menderita ruam makulopapular ?</td>
                                <td style="text-align:center;width:8%">{{(empty($dt->pe_kontak_ruam_makulopapular))?'-':$dt->pe_kontak_ruam_makulopapular}}</td>
                                <td style="width:14%">Umur Kehamilan</td>
                                <td style="text-align:center;width:8%">{{$dt->pe_umur_hamil_kontak_ruam_makulopapular.' bulan'}}</td>
                                <td style="width:14%">Lokasi terkena Kontak</td>
                                <td style="text-align:center;width:14%">{{(empty($dt->pe_desc_kontak_ruam_makulopapular))?'-':$dt->pe_desc_kontak_ruam_makulopapular}}</td>
                            </tr>
                            <tr>
                                <td>Apakah ibu bepergian selama kehamilan terakhir ini</td>
                                <td style="text-align:center">{{(empty($dt->pe_pergi_waktu_hamil))?'-':$dt->pe_pergi_waktu_hamil}}</td>
                                <td>Umur Kehamilan</td>
                                <td style="text-align:center">{{$dt->pe_umur_hamil_waktu_pergi.' bulan'}}</td>
                                <td>Bepergian Kemana</td>
                                <td style="text-align:center">{{(empty($dt->pe_desc_pergi_waktu_hamil))?'-':$dt->pe_desc_pergi_waktu_hamil}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="module-body uk-overflow-container">
                    <div class="table-responsive">
                        <table class="table table-striped">
                        <caption style="border:1px solid #eee;background-color:#666; color:#fff">Data Pemeriksaan dan Hasil Laboratorium pada bayi (Lab Rumah Sakit)</caption>
                            <tr>
                                <td>Pengambilan Spesimen</td>
                                <td colspan="3">{{(empty($dt->rs_spesimen))?'-':$dt->rs_spesimen}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Tanggal Ambil</td>
                                <td>Tanggal Kirim ke lab</td>
                                <td>Tanggal Tiba</td>
                            </tr>
                            <tr>
                                <td>Serum 1</td>
                                <td>{{Helper::getDate($dt->rs_tgl_ambil_serum1)}}</td>
                                <td>{{Helper::getDate($dt->rs_tgl_kirim_serum1)}}</td>
                                <td>{{Helper::getDate($dt->rs_tgl_tiba_serum1)}}</td>
                            </tr>
                            <tr>
                                <td>Serum 2</td>
                                <td>{{Helper::getDate($dt->rs_tgl_ambil_serum2)}}</td>
                                <td>{{Helper::getDate($dt->rs_tgl_kirim_serum2)}}</td>
                                <td>{{Helper::getDate($dt->rs_tgl_tiba_serum2)}}</td>
                            </tr>
                            <tr>
                                <td>Throat Swab</td>
                                <td>{{Helper::getDate($dt->rs_tgl_ambil_throat_swab)}}</td>
                                <td>{{Helper::getDate($dt->rs_tgl_kirim_throat_swab)}}</td>
                                <td>{{Helper::getDate($dt->rs_tgl_tiba_throat_swab)}}</td>
                            </tr>
                            <tr>
                                <td>Urine</td>
                                <td>{{Helper::getDate($dt->rs_tgl_ambil_urine)}}</td>
                                <td>{{Helper::getDate($dt->rs_tgl_kirim_urine)}}</td>
                                <td>{{Helper::getDate($dt->rs_tgl_tiba_urine)}}</td>
                            </tr>
                            <tr>
                                <td>Hasil Pemeriksaan Laboratorium</td>
                                <td colspan="3"></td>
                            </tr>
                            <tr>
                                <td>Jenis Pemeriksaan</td>
                                <td>Hasil</td>
                                <td>Jenis Virus</td>
                                <td>Tanggal hasil Lab</td>
                            </tr>
                            <tr>
                                <td>IgM serum ke 1</td>
                                <td>{{(empty($dt->rs_hasil_igm_serum1))?'-':$dt->rs_hasil_igm_serum1}}</td>
                                <td>{{(empty($dt->rs_virus_igm_serum1))?'-':$dt->rs_virus_igm_serum1}}</td>
                                <td>{{Helper::getDate($dt->rs_tgl_igm_serum1)}}</td>
                            </tr>
                            <tr>
                                <td>IgM serum 2 (ulangan)</td>
                                <td>{{(empty($dt->rs_hasil_igm_serum2))?'-':$dt->rs_hasil_igm_serum2}}</td>
                                <td>{{(empty($dt->rs_virus_igm_serum2))?'-':$dt->rs_virus_igm_serum2}}</td>
                                <td>{{Helper::getDate($dt->rs_tgl_igm_serum2)}}</td>
                            </tr>
                            <tr>
                                <td>IgG serum 1</td>
                                <td>{{(empty($dt->rs_hasil_igg_serum1))?'-':$dt->rs_hasil_igg_serum1}}</td>
                                <td>{{(empty($dt->rs_virus_igg_serum1))?'-':$dt->rs_virus_igg_serum1}}</td>
                                <td>{{Helper::getDate($dt->rs_tgl_igg_serum1)}}</td>
                            </tr>
                            <tr>
                                <td>IgG serum 2 (ulangan)</td>
                                <td>{{(empty($dt->rs_hasil_igg_serum2))?'-':$dt->rs_hasil_igg_serum2}}</td>
                                <td>{{(empty($dt->rs_virus_igg_serum2))?'-':$dt->rs_virus_igg_serum2}}</td>
                                <td>{{Helper::getDate($dt->rs_tgl_igg_serum2)}}</td>
                            </tr>
                            <tr>
                                <td>Isolasi</td>
                                <td>{{(empty($dt->rs_hasil_isolasi))?'-':$dt->rs_hasil_isolasi}}</td>
                                <td>{{(empty($dt->rs_virus_isolasi))?'-':$dt->rs_virus_isolasi}}</td>
                                <td>{{Helper::getDate($dt->rs_tgl_isolasi)}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="module-body uk-overflow-container">
                    <div class="table-responsive">
                        <table class="table table-striped">
                        <caption style="border:1px solid #eee;background-color:#666; color:#fff">Data Pemeriksaan dan Hasil Laboratorium pada bayi</caption>
                            <tr>
                                <td>Pengambilan Spesimen</td>
                                <td colspan="3">{{(empty($dt->lab_spesimen))?'-':$dt->lab_spesimen}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Tanggal Ambil</td>
                                <td>Tanggal Kirim ke lab</td>
                                <td>Tanggal Tiba</td>
                            </tr>
                            <tr>
                                <td>Serum 1</td>
                                <td>{{Helper::getDate($dt->lab_tgl_ambil_serum1)}}</td>
                                <td>{{Helper::getDate($dt->lab_tgl_kirim_serum1)}}</td>
                                <td>{{Helper::getDate($dt->lab_tgl_tiba_serum1)}}</td>
                            </tr>
                            <tr>
                                <td>Serum 2</td>
                                <td>{{Helper::getDate($dt->lab_tgl_ambil_serum2)}}</td>
                                <td>{{Helper::getDate($dt->lab_tgl_kirim_serum2)}}</td>
                                <td>{{Helper::getDate($dt->lab_tgl_tiba_serum2)}}</td>
                            </tr>
                            <tr>
                                <td>Throat Swab</td>
                                <td>{{Helper::getDate($dt->lab_tgl_ambil_throat_swab)}}</td>
                                <td>{{Helper::getDate($dt->lab_tgl_kirim_throat_swab)}}</td>
                                <td>{{Helper::getDate($dt->lab_tgl_tiba_throat_swab)}}</td>
                            </tr>
                            <tr>
                                <td>Urine</td>
                                <td>{{Helper::getDate($dt->lab_tgl_ambil_urine)}}</td>
                                <td>{{Helper::getDate($dt->lab_tgl_kirim_urine)}}</td>
                                <td>{{Helper::getDate($dt->lab_tgl_tiba_urine)}}</td>
                            </tr>
                            <tr>
                                <td>Hasil Pemeriksaan Laboratorium</td>
                                <td colspan="3"></td>
                            </tr>
                            <tr>
                                <td>Jenis Pemeriksaan</td>
                                <td>Hasil</td>
                                <td>Jenis Virus</td>
                                <td>Tanggal hasil Lab</td>
                            </tr>
                            <tr>
                                <td>IgM serum ke 1</td>
                                <td>{{(empty($dt->lab_hasil_igm_serum1))?'-':$dt->lab_hasil_igm_serum1}}</td>
                                <td>{{(empty($dt->lab_virus_igm_serum1))?'-':$dt->lab_virus_igm_serum1}}</td>
                                <td>{{Helper::getDate($dt->lab_tgl_igm_serum1)}}</td>
                            </tr>
                            <tr>
                                <td>IgM serum 2 (ulangan)</td>
                                <td>{{(empty($dt->lab_hasil_igm_serum2))?'-':$dt->lab_hasil_igm_serum2}}</td>
                                <td>{{(empty($dt->lab_virus_igm_serum2))?'-':$dt->lab_virus_igm_serum2}}</td>
                                <td>{{Helper::getDate($dt->lab_tgl_igm_serum2)}}</td>
                            </tr>
                            <tr>
                                <td>IgG serum 1</td>
                                <td>{{(empty($dt->lab_hasil_igg_serum1))?'-':$dt->lab_hasil_igg_serum1}}</td>
                                <td>{{(empty($dt->lab_virus_igg_serum1))?'-':$dt->lab_virus_igg_serum1}}</td>
                                <td>{{Helper::getDate($dt->lab_tgl_igg_serum1)}}</td>
                            </tr>
                            <tr>
                                <td>IgG serum 2 (ulangan)</td>
                                <td>{{(empty($dt->lab_hasil_igg_serum2))?'-':$dt->lab_hasil_igg_serum2}}</td>
                                <td>{{(empty($dt->lab_virus_igg_serum2))?'-':$dt->lab_virus_igg_serum2}}</td>
                                <td>{{Helper::getDate($dt->lab_tgl_igg_serum2)}}</td>
                            </tr>
                            <tr>
                                <td>Isolasi</td>
                                <td>{{(empty($dt->lab_hasil_isolasi))?'-':$dt->lab_hasil_isolasi}}</td>
                                <td>{{(empty($dt->lab_virus_isolasi))?'-':$dt->lab_virus_isolasi}}</td>
                                <td>{{Helper::getDate($dt->lab_tgl_isolasi)}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="module-body uk-overflow-container">
                    <div class="table-responsive">
                        <table class="table table-striped">
                        <caption style="border:1px solid #eee;background-color:#666; color:#fff">Klasifikasi Final</caption>
                            <tr>
                                <td>Klasifikasi Final</td>
                                <td>{{(empty($dt->crs_klasifikasi_final))?'-':$dt->crs_klasifikasi_final}}</td>
                                <td>{{(empty($dt->crs_klasifikasi_final_desc))?'':'Description : '.$dt->crs_klasifikasi_final_desc}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                @endif
            </div>
        </div>
        <div class="form-actions" style="text-align:center">
            @if(Sentry::getUser()->hak_akses==5)
                <a href="{{URL::to('labs/crs')}}#daftar" class="btn btn-warning">Kembali</a>
            @else
                <a href="{{URL::to('crs')}}" class="btn btn-warning">Kembali</a>
            @endif
		</div>
    </div>
</div>
@stop