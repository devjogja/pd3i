<div class="module-body uk-overflow-container">
	<table class="table table-bordered display" id="example_pe_crs">
	<thead>
			<tr>
				<th>No. Epid</th>
				<th>Nama pasien</th>
				<th>Nama Orang Tua</th>
				<th>Jenis kelamin</th>
				<th>Umur</th>
				<th>Alamat</th>
				@if(Sentry::getUser()->hak_akses==4 || Sentry::getUser()->hak_akses==6 || Sentry::getUser()->hak_akses==1)
					<td>Kode Faskes</td>
				@endif
				@if(Sentry::getUser()->hak_akses!=5)
					<th>Aksi</th>
				@endif
			</tr>
		</thead>
		<tbody>
		@if($pe_crs)
			@foreach($pe_crs as $row)
			<tr>
				<td>{{$row->no_epid}}</td>
				<td>{{$row->nama_anak}}</td>
				<td>{{$row->nama_ortu}}</td>
				<td>
				@if($row->jenis_kelamin=='1')
				Laki-laki
				@elseif($row->jenis_kelamin=='2')
				Perempuan
				@elseif($row->jenis_kelamin=='3')
				Tidak jelas
				@else
				-
				@endif
				</td>
				<td>{{$row->umur}} Th</td>
				<td>{{$row->alamat}}</td>
				@if(Sentry::getUser()->hak_akses==4 || Sentry::getUser()->hak_akses==6 || Sentry::getUser()->hak_akses==1)
					<td>{{$row->kode_faskes}}</td>
				@endif
				@if(Sentry::getUser()->hak_akses==2 || Sentry::getUser()->hak_akses==7 || Sentry::getUser()->hak_akses==3)
				<td>
					<div class="btn-group" role="group">
						<a href="{{URL::to('pe_crs_detail/'.$row->id_crs)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
						<a href="{{URL::to('crs/entricrs/'.$row->id_crs)}}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>
						<a href="{{URL::to('pe_crs_hapus/'.$row->id)}}" onclick="return confirm('apakah yakin ingin menghapus data ini?');return false;" class="btn btn-xs btn-danger" data-uk-tooltip title="hapus data"><i class="icon icon-remove"></i></a>
					</div>
				</td>
				@elseif(Sentry::getUser()->hak_akses==4)
				<td>
					<div class="btn-group" role="group">
						<a  href="{{URL::to('pe_crs_detail/'.$row->id_crs)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
						<a  href="{{URL::to('crs/entricrs/'.$row->id_crs)}}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>
					</div>
				</td>
				@elseif(Sentry::getUser()->hak_akses==1 || Sentry::getUser()->hak_akses==6)
				<td>
					<div class="btn-group" role="group">
						<a href="{{URL::to('pe_crs_detail/'.$row->id_crs)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
					</div>
				</td>
				@elseif(Sentry::getUser()->hak_akses==5)
				@endif      
			</tr>
			@endforeach
		@endif
		</tbody>
	</table>
	<script>
	$(document).ready(function() {
		$('#example_pe_crs').dataTable();
	});
	</script>
</div>