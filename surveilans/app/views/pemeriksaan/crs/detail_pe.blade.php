<!-- detail kasus difteri -->
@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <h4>
                        Detail PE Penyakit CRS
                    </h4>
                    <hr>
                </div>
                 @if($crs)
                <?php $dt = $crs[0]?>
                <div class="module-body uk-overflow-container">
                	<div class="table-responsive">
                		<table class="table table-striped">
                		<caption style="border:1px solid #eee;background-color:#666; color:#fff">Riwayat Kontak</caption>
                			<tr>
                                <td>Apakah selama kehamilan ini ibu pernah didiagnosa rubella dengan konfirmasi lab</td>
                                <td style="text-align:center">{{(empty($dt->pe_diagnosa_rubella))?'-':$dt->pe_diagnosa_rubella}}</td>
                                <td style="text-align:center" colspan="4">{{Crs::getDate($dt->pe_tgl_diagnosa_rubella, 'Pada tanggal : ')}}</td>
                            </tr>
                            <tr>
                                <td>Apakah selama kehamilan ini ibu pernah terpapar atau berkontak dengan orang yang menderita ruam makulopapular ?</td>
                                <td style="text-align:center;width:8%">{{(empty($dt->pe_kontak_ruam_makulopapular))?'-':$dt->pe_kontak_ruam_makulopapular}}</td>
                                <td style="width:14%">Umur Kehamilan</td>
                                <td style="text-align:center;width:8%">{{$dt->pe_umur_hamil_kontak_ruam_makulopapular.' bulan'}}</td>
                                <td style="width:14%">Lokasi terkena Kontak</td>
                                <td style="text-align:center;width:14%">{{(empty($dt->pe_desc_kontak_ruam_makulopapular))?'-':$dt->pe_desc_kontak_ruam_makulopapular}}</td>
                            </tr>
                            <tr>
                                <td>Apakah ibu bepergian selama kehamilan terakhir ini</td>
                                <td style="text-align:center">{{(empty($dt->pe_pergi_waktu_hamil))?'-':$dt->pe_pergi_waktu_hamil}}</td>
                                <td>Umur Kehamilan</td>
                                <td style="text-align:center">{{$dt->pe_umur_hamil_waktu_pergi.' bulan'}}</td>
                                <td>Bepergian Kemana</td>
                                <td style="text-align:center">{{(empty($dt->pe_desc_pergi_waktu_hamil))?'-':$dt->pe_desc_pergi_waktu_hamil}}</td>
                            </tr>
                		</table>
                	</div>
                </div>
                @endif
            </div>
        </div>
        <div class="form-actions" style="text-align:center">
		    <a href="{{URL::to('crs')}}" class="btn btn-warning">Kembali</a>
		</div>
    </div>
</div>
@stop