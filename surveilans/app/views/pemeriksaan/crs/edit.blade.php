@extends('layouts.master')
@section('content')
<div class="span12">
	<div class="content">
		<div class="module">
			<div class="module-body">
				<div class="profile-head media">
					<h4>
						Form Edit Pasien Kasus Penyakit CRS
					</h4>
					<hr>
				</div>
				{{Form::open(['url'=>'crs_update','class'=>'form-horizontal'])}}
				<div class="module-body">
					<?php
					$row = $crs[0];
					?>
					<input type="hidden" name="id_pelapor" value="{{$row->pelapor_id_pelapor}}">
					<input type="hidden" name="id_pasien" value="{{$row->pasien_id_pasien}}">
					<input type="hidden" name="id_crs" value="{{$row->crs_id_crs}}">
					<input type="hidden" name="id_pe_crs" value="{{$row->pe_id_pe_crs}}">
					<input type="hidden" name="id_hasil_uji_lab" value="{{$row->lab_id_hasil_uji_lab_crs}}">
					<div class="row-fluid">
						<div class="span12">
							<div class="media">
								<fieldset>
									<legend>Data Pelapor</legend>
									<div class="control-group">
										<label class="control-label">Nama Rumah Sakit</label>
										<div class="controls">
											{{ Form::text(
												'dp[nama_rs]',
												$row->pelapor_nama_rs,
												['data-validation' => '[MIXED]',
												'data' => '$ wajib di isi!',
												'data-validation-message' => 'wajib di isi!',
												'class' => 'input-xlarge',
												'placeholder' => 'Nama Rumah Sakit']);
											}}
											<span class="help-inline" style="color:red">(*)</span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Provinsi</label>
										<div class="controls">
											{{ Form::select(
												'dp[id_provinsi]',
												['0'=>'Pilih provinsi']+Provinsi::lists('provinsi','id_provinsi'),
												$row->pelapor_id_provinsi,
												['id'=>'id_provinsi_pelapor',
												'onchange'=>'showKabupaten("_pelapor")',
												'class'=>'input-large id_combobox']);
											}}
											<span class="help-inline"></span>
										</div>
									</div>
									<?php
										$pelapor_kabupaten = 'Pilih Kabupaten';
										$plp = DB::table('kabupaten')->select('id_kabupaten','kabupaten')->where('id_kabupaten',$row->pelapor_id_kabupaten)->get();
										$pelapor_kabupaten = (empty($plp[0])?'':$plp[0]->kabupaten);
									?>
									<div class="control-group">
										<label class="control-label">Kabupaten</label>
										<div class="controls">
											{{ Form::select(
												'dp[id_kabupaten]',
												[$row->pelapor_id_kabupaten=>$pelapor_kabupaten],
												$row->pelapor_id_kabupaten,
												['id'=>'id_kabupaten_pelapor',
												'class'=>'input-large id_combobox']);
											}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Tanggal Laporan</label>
										<div class="controls">
											{{ Form::text(
												'dp[tanggal_laporan]',
												Helper::getDate($row->pelapor_tanggal_laporan),
												['class' => 'input-medium',
												'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
												'placeholder' => 'Tanggal Laporan']);
											}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Tanggal Investigasi</label>
										<div class="controls">
											{{ Form::text(
												'dp[tanggal_investigasi]',
												Helper::getDate($row->pelapor_tanggal_investigasi),
												['class' => 'input-medium',
												'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
												'placeholder' => 'Tanggal Investigasi'])
											}}
											<span class="help-inline"></span>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<div class="media">
								<fieldset>
									<legend>Data Pasien</legend>
									<div class="control-group">
										<label class="control-label">Nomor EPID</label>
										<div class="controls">
											{{ Form::text(
												'dk[no_epid]',
												$row->crs_no_epid,
												['class' => 'input-xlarge',
												'readonly'=>'readonly',
												'id'=>'no_epid',
												'placeholder' => 'Nomor EPID (Diisi oleh kabupaten)'])
											}}
											<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										  <label class="control-label">Nomor RM</label>
										  <div class="controls">
										  {{ Form::text(
												'dpa[no_rm]',
												$row->pasien_no_rm,
												array(
													  'class' => 'input-xlarge',
													  'placeholder' => 'Nomor RM'
												))
										  }}
										  <span class="help-inline"></span>
										  </div>
									</div>
									<div class="control-group">
										<label class="control-label">Nama Bayi</label>
										<div class="controls">
											{{ Form::text(
												'dpa[nama_anak]',
												$row->pasien_nama_anak,
												['data-validation' => '[MIXED]',
												'id'=>'nama_anak',
												'data' => '$ wajib di isi!',
												'data-validation-message' => 'wajib di isi!',
												'class' => 'input-xlarge',
												'placeholder' => 'Nama Bayi'])
											}}
											<span class="help-inline" style="color:red">(*)</span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Jenis kelamin</label>
										<div class="controls">
											{{Form::select(
												'dpa[jenis_kelamin]',
												[null => 'Pilih',
												'1'=>'Laki-laki',
												'2'=>'Perempuan',
												'3'=>'Tidak Jelas'],
												$row->pasien_jenis_kelamin,
												['data-validation'=>'[MIXED]',
												'data'=>'$ wajib di isi!',
												'data-validation-message'=>'jenis kelamin wajib di isi!',
												'class' => 'input-medium id_combobox'])
											}}
											<span class="help-inline" style="color:red">(*)</span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Tanggal lahir</label>
										<div class="controls">
											{{ Form::text(
												'dpa[tanggal_lahir]',
												Helper::getDate($row->pasien_tanggal_lahir),
												['class' => 'input-medium tgl_lahir',
												'data-uk-datepicker' => '{keluformat:"DD-MM-YYYY"}',
												'placeholder' => 'Tanggal lahir',
												'id'=>'tgl_lahir',
												'onchange' => 'usia(), showEpidCrs("_pasien")',
												])
											}}
											<span class="help-inline" ></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Umur</label>
										<div class="controls">
											<input type="text" class="input-mini" value="{{$row->pasien_umur}}" name="dpa[umur]" id="tgl_tahun">Thn
											<input type="text" class="input-mini" value="{{$row->pasien_umur_bln}}" name="dpa[umur_bln]" id="tgl_bulan">Bln
											<input type="text" class="input-mini" value="{{$row->pasien_umur_hr}}" name="dpa[umur_hr]" id="tgl_hari">Hari
										</div>
									</div>
									<!-- awal input kolom provinsi -->
									<div class="control-group">
										<label class="control-label">Provinsi</label>
										<div class="controls">
											{{Form::select(
												'dpa[id_provinsi]',
												['0'=>'Pilih provinsi']+Provinsi::lists('provinsi','id_provinsi'),
												$row->pasien_id_provinsi,
												['id'       => 'id_provinsi_pasien',
												'onchange' => "show_kabupaten('_pasien')",
												'class'    => 'input-large id_combobox']);
											}}
											<span class="help-inline" id="error-id_provinsi_pasien"></span>
										</div>
									</div>
									<!-- awal input kolom kabupaten -->
									<div class="control-group">
										<label class="control-label">Kabupaten</label>
										<div class="controls">
											{{Form::select(
												'dpa[id_kabupaten]',
												[$row->pasien_id_kabupaten=>$row->pasien_kabupaten],
												$row->pasien_id_kabupaten,
												['id'       => 'id_kabupaten_pasien',
												'onchange' => "show_kecamatan('_pasien')",
												'class'    => 'input-large id_combobox']);
											}}
			                      			<span class="lodingKab"></span>
											<span class="help-inline" id="error-id_kabupaten_pasien"></span>
										</div>
									</div>
									<!-- awal input kolom kecamatan -->
									<div class="control-group">
										<label class="control-label">Kecamatan</label>
										<div class="controls">
											{{Form::select(
												'dpa[id_kecamatan]',
												[$row->pasien_id_kecamatan=>$row->pasien_kecamatan],
												$row->pasien_id_kecamatan,
												['id'       => 'id_kecamatan_pasien',
												'onchange' => "show_kelurahan('_pasien')",
												'class'    => 'input-large id_combobox']);
											}}
			                      			<span class="lodingKec"></span>
											<span class="help-inline" id="error-id_kecamatan_pasien"></span>
										</div>
									</div>
									<!-- awal input kolom kelurahan -->
									<div class="control-group">
										<label class="control-label">Kelurahan/Desa</label>
										<div class="controls">
											{{Form::select(
												'dpa[id_kelurahan]',
												[$row->pasien_id_kelurahan=>$row->pasien_kelurahan],
												$row->pasien_id_kelurahan,
												['id'       => 'id_kelurahan_pasien',
												'onchange'=>'showEpidCrs("_pasien")',
												'class'    => 'input-large id_combobox']);
											}}
			                      			<span class="lodingKel"></span>
											<span class="help-inline" style="color:red">(*)</span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Alamat</label>
										<div class="controls">
											{{ Form::textarea(
												'dpa[alamat]',
												$row->pasien_alamat,
												['rows' => '3',
												'placeholder' => 'Hanya diisi nama jalan, no. rumah dan no. RT - RW'])
												}}
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Tempat bayi di lahirkan</label>
											<div class="controls">
												{{ Form::text(
													'dpa[tempat_lahir_bayi]',
													$row->pasien_tempat_lahir_bayi,
													['class' => 'input-xlarge',
													'placeholder' => 'Tempat bayi di lahirkan'])
												}}
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Nama Ibu</label>
											<div class="controls">
												{{ Form::text(
													'dpa[nama_ortu]',
													$row->pasien_nama_ortu,
													['class' => 'input-xlarge',
													'id'=>'nama_ortu',
													'placeholder' => 'Nama Ibu'])
												}}
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Umur Kehamilan saat bayi di lahirkan</label>
											<div class="controls">
												{{Form::selectRange(
														'dpa[umur_kehamilan_bayi]',
														20, 60,
														$row->pasien_umur_kehamilan_bayi,
														['class' => 'input-medium id_combobox'])
												 }}
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Berat badan bayi baru lahir</label>
											<div class="controls">
											{{ Form::text(
													'dpa[berat_badan_bayi]',
													$row->pasien_berat_badan_bayi,
													array(
														  'class' => 'input-small',
														  'placeholder' => 'Berat badan bayi'
													))
											  }} Gram
												<span class="help-inline"></span>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
						<div class="row-fluid">
							<div class="span12">
								<div class="media">
									<fieldset>
										<legend>Data Klinis Pasien</legend>
										<div class="row-fluid">
											<div class="span6">
												<div class="media">
													<fieldset>
														<legend>Group A (Lengkapi semua tanda dan gejala yang ada)</legend>
														<div class="control-group">
															<label class="control-label">Congenital heart disease</label>
															<div class="controls">
																{{Form::select(
																	'dk[congenital_heart_disease]',
																	['' => 'Pilih',
																	'Ya'=>'Ya',
																	'Tidak'=>'Tidak',
																	'Tidak Tahu'=>'Tidak Tahu'],
																	$row->crs_congenital_heart_disease,
																	['class' => 'input-medium id_combobox'])
																}}
																<span class="help-inline"></span>
															</div>
														</div>
														<div class="control-group">
															<label class="control-label">Cataracts</label>
															<div class="controls">
																{{Form::select(
																	'dk[cataracts]',
																	['' => 'Pilih',
																	'Ya'=>'Ya',
																	'Tidak'=>'Tidak',
																	'Tidak Tahu'=>'Tidak Tahu'],
																	$row->crs_cataracts,
																	['class' => 'input-medium id_combobox'])
																}}
																<span class="help-inline"></span>
															</div>
														</div>
														<div class="control-group">
															<label class="control-label">Congenital glaucoma</label>
															<div class="controls">
																{{Form::select(
																	'dk[congenital_glaucoma]',
																	['' => 'Pilih',
																	'Ya'=>'Ya',
																	'Tidak'=>'Tidak',
																	'Tidak Tahu'=>'Tidak Tahu'],
																	$row->crs_congenital_glaucoma,
																	['class' => 'input-medium id_combobox'])
																}}
																<span class="help-inline"></span>
															</div>
														</div>
														<div class="control-group">
															<label class="control-label">Pigmentary retinopathy</label>
															<div class="controls">
																{{Form::select(
																	'dk[pigmentary_retinopathy]',
																	['' => 'Pilih',
																	'Ya'=>'Ya',
																	'Tidak'=>'Tidak',
																	'Tidak Tahu'=>'Tidak Tahu'],
																	$row->crs_pigmentary_retinopathy,
																	['class' => 'input-medium id_combobox'])
																}}
																<span class="help-inline"></span>
															</div>
														</div>
														<div class="control-group">
															<label class="control-label">Hearing impairment</label>
															<div class="controls">
																{{Form::select(
																	'dk[hearing_impairment]',
																	['' => 'Pilih',
																	'Ya'=>'Ya',
																	'Tidak'=>'Tidak',
																	'Tidak Tahu'=>'Tidak Tahu'],
																	$row->crs_hearing_impairment,
																	['class' => 'input-medium id_combobox'])
																}}
																<span class="help-inline"></span>
															</div>
														</div>
													</fieldset>
												</div>
												<div class="control-group">
													<label class="control-label">Nama Dokter Pemeriksa</label>
													<div class="controls">
														{{ Form::textarea(
						  									'dk[nama_dokter_pemeriksa]',
						  									$row->crs_nama_dokter_pemeriksa,
						  									array(
						  										  'rows' => '3',
						  										  'placeholder' => 'Nama Dokter Pemeriksa'
						  									))
						  							  	}}
														<span class="help-inline"></span>
													</div>
												</div>
												<div class="control-group">
													<label class="control-label">Tanggal periksa</label>
													<div class="controls">
													{{ Form::text(
														  'dk[tgl_mulai_sakit]',
														  Helper::getDate($row->crs_tanggal_periksa),
														  array(
																'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
																'class' => 'input-xlarge tgl_mulai_sakit',
																'id'=>'tgl_mulai_sakit',
																'onchange'=>'usia()',
																'placeholder' => 'Tanggal Periksa'
														  ))
													}}
													<span class="help-inline"></span>
													</div>
											  	</div>
												<div class="control-group">
													<label class="control-label">Keadaan Bayi saat ini</label>
													<div class="controls">
														<table>
															<tr>
																<td>
																	<input type="radio" name="dk[keadaan_akhir]" value="Hidup" id="hidup" <?php if($row->crs_keadaan_akhir== "Hidup") { echo 'checked="checked"'; } ?> >
																</td>
																<td>Hidup</td>
															</tr>
															<tr>
																<td>
																	<input type="radio" name="dk[keadaan_akhir]" value="Meninggal" id="meninggal" <?php if($row->crs_keadaan_akhir== "Meninggal") { echo 'checked="checked"'; } ?> >
																</td>
																<td>Meninggal</td>
																<td>
																	{{ Form::text(
																		'dk[penyebab_meninggal]',
																		$row->crs_penyebab_meninggal,
																		['class' => 'input-medium penyebab_meninggal',
																		'placeholder' => 'Penyebab Meninggal',
																		'readonly'=>'readonly']);
																	}}
																</td>
															</tr>
														</table>
														<span class="help-inline"></span>
													</div>
												</div>
											</div>
											<div class="span6">
												<div class="media">
													<fieldset>
														<legend>Group B (Lengkapi semua tanda dan gejala yang ada)</legend>
														<div class="control-group">
															<label class="control-label">Purpura</label>
															<div class="controls">
																{{Form::select(
																	'dk[purpura]',
																	['' => 'Pilih',
																	'Ya'=>'Ya',
																	'Tidak'=>'Tidak',
																	'Tidak Tahu'=>'Tidak Tahu'],
																	$row->crs_purpura,
																	['class' => 'input-medium id_combobox'])
																}}
																<span class="help-inline"></span>
															</div>
														</div>
														<div class="control-group">
															<label class="control-label">Microcephaly</label>
															<div class="controls">
																{{Form::select(
																	'dk[microcephaly]',
																	['' => 'Pilih',
																	'Ya'=>'Ya',
																	'Tidak'=>'Tidak',
																	'Tidak Tahu'=>'Tidak Tahu'],
																	$row->crs_microcephaly,
																	['class' => 'input-medium id_combobox'])
																}}
																<span class="help-inline"></span>
															</div>
														</div>
														<div class="control-group">
															<label class="control-label">Meningoencephalitis</label>
															<div class="controls">
																{{Form::select(
																	'dk[meningoencephalitis]',
																	['' => 'Pilih',
																	'Ya'=>'Ya',
																	'Tidak'=>'Tidak',
																	'Tidak Tahu'=>'Tidak Tahu'],
																	$row->crs_meningoencephalitis,
																	['class' => 'input-medium id_combobox'])
																}}
																<span class="help-inline"></span>
															</div>
														</div>
														<div class="control-group">
															<label class="control-label">Ikterik 24 jam post partum</label>
																<div class="controls">
																{{Form::select(
																	'dk[ikterik]',
																	['' => 'Pilih',
																	'Ya'=>'Ya',
																	'Tidak'=>'Tidak',
																	'Tidak Tahu'=>'Tidak Tahu'],
																	$row->crs_ikterik,
																	['class' => 'input-medium id_combobox'])
																}}
																<span class="help-inline"></span>
															</div>
														</div>
														<div class="control-group">
															<label class="control-label">Splenomegaly</label>
																<div class="controls">
																{{Form::select(
																	'dk[splenomegaly]',
																	['' => 'Pilih',
																	'Ya'=>'Ya',
																	'Tidak'=>'Tidak',
																	'Tidak Tahu'=>'Tidak Tahu'],
																	$row->crs_splenomegaly,
																	['class' => 'input-medium id_combobox'])
																}}
																<span class="help-inline"></span>
															</div>
														</div>
														<div class="control-group">
															<label class="control-label">Developmental delay</label>
															<div class="controls">
																{{Form::select(
																	'dk[developmental_delay]',
																	['' => 'Pilih',
																	'Ya'=>'Ya',
																	'Tidak'=>'Tidak',
																	'Tidak Tahu'=>'Tidak Tahu'],
																	$row->crs_developmental_delay,
																	['class' => 'input-medium id_combobox'])
																}}
																<span class="help-inline"></span>
															</div>
														</div>
														<div class="control-group">
															<label class="control-label">Radiolucent bone disease</label>
															<div class="controls">
																{{Form::select(
																	'dk[radiolucent]',
																	['' => 'Pilih',
																	'Ya'=>'Ya',
																	'Tidak'=>'Tidak',
																	'Tidak Tahu'=>'Tidak Tahu'],
																	$row->crs_radiolucent,
																	['class' => 'input-medium id_combobox'])
																}}
																<span class="help-inline"></span>
															</div>
														</div>
														<div class="control-group">
															<label class="control-label">Other abnormalities</label>
															<div class="controls">
																{{Form::select(
																	'dk[other_abnormal]',
																	['' => 'Pilih',
																	'Ya'=>'Ya',
																	'Tidak'=>'Tidak',
																	'Tidak Tahu'=>'Tidak Tahu'],
																	$row->crs_other_abnormal,
																	['onchange'=>'showabnornal()',
																	'class' => 'input-medium id_combobox',
																	'id'=>'other_abnornalities'])
																}}
																{{ Form::text(
																	'dk[dt_other_abnormal]',
																	$row->crs_dt_other_abnormal,
																	['class'=>'input-medium other_ab',
																	'style'=>'padding:3px',
																	'readonly'=>'readonly',
																	'placeholder' => 'Other Abnormalities'])
																}}
																<span class="help-inline"></span>
															</div>
														</div>
													</fieldset>
												</div>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
						<div class="row-fluid">
							<div class="span12">
								<div class="media">
									<fieldset>
										<legend>Data Riwayat Kehamilan Ibu</legend>
										<div class="control-group">
											<label class="control-label">Jumlah kehamilan sebelumnya</label>
											<div class="controls">
												{{ Form::text(
													'dpe[jumlah_kehamilan_sebelumnya]',
													($row->pe_jumlah_kehamilan_sebelumnya==0)?'':$row->pe_jumlah_kehamilan_sebelumnya,
													['placeholder' => 'Jumlah kehamilan sebelumnya'])
												}}
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Umur Ibu (dalam tahun)</label>
											<div class="controls">
												{{ Form::text(
													'dpe[umur_ibu]',
													($row->pe_umur_ibu==0)?'':$row->pe_umur_ibu,
													['placeholder' => 'Umur Ibu (dalam tahun)'])
												}}
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Hari Pertama haid terakhir (HPHT)</label>
											<div class="controls">
												{{ Form::text(
													'dpe[hari_pertama_haid]',
													$row->pe_hari_pertama_haid,
													['placeholder' => 'Hari pertama haid terakhir (HPHT)'])
												}}
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											Apakah selama kehamilan terakhir ini ibu pernah mengalami
										</div>
										<div class="control-group">
											<table>
												<tr>
													<td>Conjunctivitis</td>
													<td>
														{{Form::select(
															'dpe[conjunctivitis]',
															['' => 'Pilih',
															'Ya'=>'Ya',
															'Tidak'=>'Tidak',
															'Tidak Tahu'=>'Tidak Tahu'],
															$row->pe_conjunctivitis,
															['id'=>'conjunctivitis',
															'onchange'=>'actiondate(this.id, this.value)',
															'class' => 'input-medium id_combobox'])
														}}
													</td>
													<td>
														{{ Form::text(
															'dpe[conjunctivitis_date]',
															Helper::getDate($row->pe_conjunctivitis_date),
															['class'=>'conjunctivitis_date',
															'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
															'readonly'=>'readonly',
															'placeholder'=>'Tanggal Kejadian'])
														}}
													</td>
												</tr>
												<tr>
													<td>Pilek</td>
													<td>
														{{Form::select(
															'dpe[pilek]',
															['' => 'Pilih',
															'Ya'=>'Ya',
															'Tidak'=>'Tidak',
															'Tidak Tahu'=>'Tidak Tahu'],
															$row->pe_pilek,
															['id'=>'pilek',
															'onchange'=>'actiondate(this.id, this.value)',
															'class' => 'input-medium id_combobox'])
														}}
													</td>
													<td>
														{{ Form::text(
															'dpe[pilek_date]',
															Helper::getDate($row->pe_pilek_date),
															['data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
															'readonly'=>'readonly',
															'class'=>'pilek_date',
															'placeholder'=>'Tanggal Kejadian'])
														}}
													</td>
												</tr>
												<tr>
													<td>Batuk</td>
													<td>
														{{Form::select(
															'dpe[batuk]',
															['' => 'Pilih',
															'Ya'=>'Ya',
															'Tidak'=>'Tidak',
															'Tidak Tahu'=>'Tidak Tahu'],
															$row->pe_batuk,
															['id'=>'batuk',
															'onchange'=>'actiondate(this.id, this.value)',
															'class' => 'input-medium id_combobox'])
														}}
													</td>
													<td>
														{{ Form::text(
															'dpe[batuk_date]',
															Helper::getDate($row->pe_batuk_date),
															['data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
															'readonly'=>'readonly',
															'class'=>'batuk_date',
															'placeholder'=>'Tanggal Kejadian'])
														}}
													</td>
												</tr>
												<tr>
													<td>Ruam makulopapular</td>
													<td>
														{{Form::select(
															'dpe[ruam_makulopapular]',
															['' => 'Pilih',
															'Ya'=>'Ya',
															'Tidak'=>'Tidak',
															'Tidak Tahu'=>'Tidak Tahu'],
															$row->pe_ruam_makulopapular,
															['id'=>'ruam_makulopapular',
															'onchange'=>'actiondate(this.id, this.value)',
															'class' => 'input-medium id_combobox'])
														}}
													</td>
													<td>
														{{ Form::text(
															'dpe[ruam_makulopapular_date]',
															Helper::getDate($row->pe_ruam_makulopapular_date),
															['data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
															'readonly'=>'readonly',
															'class'=>'ruam_makulopapular_date',
															'placeholder'=>'Tanggal Kejadian'])
														}}
													</td>
												</tr>
												<tr>
													<td>Pembengkakan kelenjar limfa</td>
													<td>
														{{Form::select(
															'dpe[pembengkakan_kelenjar_limfa]',
															['' => 'Pilih',
															'Ya'=>'Ya',
															'Tidak'=>'Tidak',
															'Tidak Tahu'=>'Tidak Tahu'],
															$row->pe_pembengkakan_kelenjar_limfa,
															['id'=>'pembengkakan_kelenjar_limfa',
															'onchange'=>'actiondate(this.id, this.value)',
															'class' => 'input-medium id_combobox'])
														}}
													</td>
													<td>
														{{ Form::text(
															'dpe[pembengkakan_kelenjar_limfa_date]',
															Helper::getDate($row->pe_pembengkakan_kelenjar_limfa_date),
															['data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
															'readonly'=>'readonly',
															'class'=>'pembengkakan_kelenjar_limfa_date',
															'placeholder'=>'Tanggal Kejadian'])
														}}
													</td>
												</tr>
												<tr>
													<td>Demam</td>
													<td>
														{{Form::select(
															'dpe[demam]',
															['' => 'Pilih',
															'Ya'=>'Ya',
															'Tidak'=>'Tidak',
															'Tidak Tahu'=>'Tidak Tahu'],
															$row->pe_demam,
															['id'=>'demam',
															'onchange'=>'actiondate(this.id, this.value)',
															'class' => 'input-medium id_combobox'])
														}}
													</td>
													<td>
														{{ Form::text(
															'dpe[demam_date]',
															Helper::getDate($row->pe_demam_date),
															['data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
															'readonly'=>'readonly',
															'class'=>'demam_date',
															'placeholder'=>'Tanggal Kejadian'])
														}}
													</td>
												</tr>
												<tr>
													<td>Arthralgia/arthritis</td>
													<td>
														{{Form::select(
															'dpe[arthralgia]',
															['' => 'Pilih',
															'Ya'=>'Ya',
															'Tidak'=>'Tidak',
															'Tidak Tahu'=>'Tidak Tahu',],
															$row->pe_arthralgia,
															['id'=>'arthralgia',
															'onchange'=>'actiondate(this.id, this.value)',
															'class' => 'input-medium id_combobox'])
														}}
													</td>
													<td>
														{{ Form::text(
															'dpe[arthralgia_date]',
															Helper::getDate($row->pe_arthralgia_date),
															['data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
															'readonly'=>'readonly',
															'class'=>'arthralgia_date',
															'placeholder'=>'Tanggal Kejadian'])
														}}
													</td>
												</tr>
												<tr>
													<td>Komplikasi lain</td>
													<td>
														{{Form::select(
															'dpe[komplikasi_lain]',
															['' => 'Pilih',
															'Ya'=>'Ya',
															'Tidak'=>'Tidak',
															'Tidak Tahu'=>'Tidak Tahu'],
															$row->pe_komplikasi_lain,
															['id'=>'komplikasi_lain',
															'onchange'=>'actiondate(this.id, this.value)',
															'class' => 'input-medium id_combobox'])
														}}
													</td>
													<td>
														{{ Form::text(
															'dpe[komplikasi_lain_date]',
															Helper::getDate($row->pe_komplikasi_lain_date),
															['data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
															'readonly'=>'readonly',
															'class'=>'komplikasi_lain_date',
															'placeholder'=>'Tanggal Kejadian'])
														}}
													</td>
												</tr>
												<tr>
													<td>Mendapat Vaksinasi rubella</td>
													<td>
														{{Form::select(
															'dpe[vaksinasi_rubella]',
															['' => 'Pilih',
															'Ya'=>'Ya',
															'Tidak'=>'Tidak',
															'Tidak Tahu'=>'Tidak Tahu'],
															$row->pe_vaksinasi_rubella,
															['id'=>'vaksinasi_rubella',
															'onchange'=>'actiondate(this.id, this.value)',
															'class' => 'input-medium id_combobox'])
														}}
													</td>
													<td>
														{{ Form::text(
															'dpe[vaksinasi_rubella_date]',
															Helper::getDate($row->pe_vaksinasi_rubella_date),
															['data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
															'readonly'=>'readonly',
															'class'=>'vaksinasi_rubella_date',
															'placeholder'=>'Tanggal Kejadian'])
														}}
													</td>
												</tr>
											</table>
										</div>
										<div class="control-group">
											<label class="control-label">Apakah selama kehamilan ini ibu pernah didiagnosa rubella dengan konfirmasi lab</label>
											<div class="controls">
												{{Form::select(
													'dpe[diagnosa_rubella]',
													['' => 'Pilih',
													'Ya'=>'Ya',
													'Tidak'=>'Tidak',
													'Tidak Tahu'=>'Tidak Tahu'],
													$row->pe_diagnosa_rubella,
													['id'=>'diagnosa_rubella',
													'onchange'=>'actiondate(this.id, this.value)',
													'class' => 'input-medium id_combobox'])
												}}
												{{ Form::text(
													'dpe[tgl_diagnosa_rubella]',
													Helper::getDate($row->pe_tgl_diagnosa_rubella),
													['readonly'=>'readonly',
													'class' => 'input-large diagnosa_rubella_date',
													'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
													'placeholder' => 'Kapan diagnosa dilakukan'])
												}}
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Apakah selama kehamilan ini ibu pernah terpapar atau berkontak dengan orang yang menderita ruam makulopapular ?</label>
											<div class="controls">
												{{Form::select(
													'dpe[kontak_ruam_makulopapular]',
													['' => 'Pilih',
													'Ya'=>'Ya',
													'Tidak'=>'Tidak',
													'Tidak Tahu'=>'Tidak Tahu'],
													$row->pe_kontak_ruam_makulopapular,
													['id'=>'kontak_ruam_makulopapular',
													'onchange'=>'actiondate(this.id, this.value)',
													'class' => 'input-medium id_combobox'])
												}}
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Umur Kehamilan saat terpapar dengan penderita ruam makulopapular</label>
											<div class="controls">
											  	{{Form::selectRange(
														'dpe[umur_hamil_kontak_ruam_makulopapular]',
														1, 60,
														$row->pe_umur_hamil_kontak_ruam_makulopapular,
														['class' => 'input-medium id_combobox kontak_ruam_makulopapular_date'])
												  }}
												{{ Form::textarea(
													'dpe[desc_kontak_ruam_makulopapular]',
													$row->pe_desc_kontak_ruam_makulopapular,
													['readonly'=>'readonly',
													'rows' => '3',
													'class' => 'kontak_ruam_makulopapular_date',
													'placeholder' => 'Dimana terkena kontak'])
												}}
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Apakah ibu bepergian selama kehamilan terakhir ini</label>
											<div class="controls">
												{{Form::select(
													'dpe[pergi_waktu_hamil]',
													['' => 'Pilih',
													'Ya'=>'Ya',
													'Tidak'=>'Tidak',
													'Tidak Tahu'=>'Tidak Tahu'],
													$row->pe_pergi_waktu_hamil,
													['id'=>'pergi_waktu_hamil',
													'onchange'=>'actiondate(this.id, this.value)',
													'class' => 'input-medium id_combobox'])
												}}
												<span class="help-inline"></span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Umur kehamilan ketika bepergian</label>
											<div class="controls">
											  	{{Form::selectRange(
														'dpe[umur_hamil_waktu_pergi]',
														1, 60,
														$row->pe_umur_hamil_waktu_pergi,
														['class' => 'input-medium id_combobox pergi_waktu_hamil_date'])
												  }}
												{{ Form::textarea(
													'dpe[desc_pergi_waktu_hamil]',
													$row->pe_desc_pergi_waktu_hamil,
													['readonly'=>'readonly',
													'rows'=>'3',
													'class' => 'pergi_waktu_hamil_date',
													'placeholder' => 'Bepergian kemana'])
												}}
												<span class="help-inline"></span>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
						<div class="row-fluid">
							<div class="span12">
								<div class="media">
									<fieldset>
										<legend>Data Pemeriksaan dan Hasil Laboratorium pada bayi</legend>
										<div class="row-fluid">
											<div class="span12">
												<div class="media">
													<fieldset>
														<legend>Pemeriksaan Laboratorium</legend>
														<div class="control-group">
															<label class="control-label">Apakah spesimen diambil</label>
															<div class="controls">
																{{Form::select(
																	'dlab[spesimen]',
																	['' => 'Pilih',
																	'Ya'=>'Ya',
																	'Tidak'=>'Tidak',
																	'Tidak Tahu'=>'Tidak Tahu'],
																	$row->rs_spesimen,
																	['class' => 'input-medium id_combobox'])
																}}
																<span class="help-inline"></span>
															</div>
														</div>
														<div class="control-group">
															<label class="control-label">Jenis Spesimen</label>
															<div class="controls">
																<table>
																	<tr>
																		<td>Serum 1</td>
																		<td>
																			{{ Form::text(
																				'dlab[tgl_ambil_serum1]',
																				Helper::getDate($row->rs_tgl_ambil_serum1),
																				['class' => 'input-large',
																				'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																				'placeholder' => 'Tanggal Ambil'])
																			}}
																		</td>
																		<td>
																			{{ Form::text(
																				'dlab[tgl_kirim_serum1]',
																				Helper::getDate($row->rs_tgl_kirim_serum1),
																				['class' => 'input-large akseskab',
																				'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																				'placeholder' => 'Tanggal dikirim ke Lab (Di isi Kab/provinsi)'])
																			}}
																		</td>
																		<td>
																			{{ Form::text(
																				'dlab[tgl_tiba_serum1]',
																				Helper::getDate($row->rs_tgl_tiba_serum1),
																				['class' => 'input-large akseskab',
																				'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																				'placeholder' => 'Tanggal tiba di lab'])
																			}}
																		</td>
																	</tr>
																	<tr>
																		<td>Serum 2</td>
																		<td>
																			{{ Form::text(
																				'dlab[tgl_ambil_serum2]',
																				Helper::getDate($row->rs_tgl_ambil_serum2),
																				['class' => 'input-large',
																				'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																				'placeholder' => 'Tanggal Ambil'])
																			}}
																		</td>
																		<td>
																			{{ Form::text(
																				'dlab[tgl_kirim_serum2]',
																				Helper::getDate($row->rs_tgl_kirim_serum2),
																				['class' => 'input-large akseskab',
																				'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																				'placeholder' => 'Tanggal dikirim ke Lab (Di isi Kab/provinsi)'])
																			}}
																		</td>
																		<td>
																			{{ Form::text(
																				'dlab[tgl_tiba_serum2]',
																				Helper::getDate($row->rs_tgl_tiba_serum2),
																				['class' => 'input-large akseskab',
																				'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																				'placeholder' => 'Tanggal tiba di lab'])
																			}}
																		</td>
																	</tr>
																	<tr>
																		<td>Throat Swab</td>
																		<td>
																			{{ Form::text(
																				'dlab[tgl_ambil_throat_swab]',
																				Helper::getDate($row->rs_tgl_ambil_throat_swab),
																				['class' => 'input-large',
																				'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																				'placeholder' => 'Tanggal Ambil'])
																			}}
																		</td>
																		<td>
																			{{ Form::text(
																				'dlab[tgl_kirim_throat_swab]',
																				Helper::getDate($row->rs_tgl_kirim_throat_swab),
																				['class' => 'input-large akseskab',
																				'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																				'placeholder' => 'Tanggal dikirim ke Lab (Di isi Kab/provinsi)'])
																			}}
																		</td>
																		<td>
																			{{ Form::text(
																				'dlab[tgl_tiba_throat_swab]',
																				Helper::getDate($row->rs_tgl_tiba_throat_swab),
																				['class' => 'input-large akseskab',
																				'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																				'placeholder' => 'Tanggal tiba di lab'])
																			}}
																		</td>
																	</tr>
																	<tr>
																		<td>Urine</td>
																		<td>
																			{{ Form::text(
																				'dlab[tgl_ambil_urine]',
																				Helper::getDate($row->rs_tgl_ambil_urine),
																				['class' => 'input-large',
																				'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																				'placeholder' => 'Tanggal Ambil'])
																			}}
																		</td>
																		<td>
																			{{ Form::text(
																				'dlab[tgl_kirim_urine]',
																				Helper::getDate($row->rs_tgl_kirim_urine),
																				['class' => 'input-large akseskab',
																				'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																				'placeholder' => 'Tanggal dikirim ke Lab (Di isi Kab/provinsi)'])
																			}}
																		</td>
																		<td>
																			{{ Form::text(
																				'dlab[tgl_tiba_urine]',
																				Helper::getDate($row->rs_tgl_tiba_urine),
																				['class' => 'input-large akseskab',
																				'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																				'placeholder' => 'Tanggal tiba di lab'])
																				}}
																		</td>
																	</tr>
																</table>
															</div>
														</div>
													</fieldset>
												</div>
											</div>
										</div>
										<div class="row-fluid">
											<div class="span12">
												<div class="media">
													<fieldset>
														<legend>Hasil Pemeriksaan Laboratorium</legend>
														<div class="control-group">
															<div class="controls">
																<table>
																	<tr>
																		<td><b>Jenis Pemeriksaan</b></td>
																		<td colspan="3"><b>Hasil</b></td>
																	</tr>
																	<tr>
																		<td>IgM serum ke 1</td>
																		<td>
																			{{Form::select(
																				'dlab[hasil_igm_serum1]',
																				['' => 'Pilih',
																				'Positif'=>'Positif',
																				'Negatif'=>'Negatif'],
																				$row->rs_hasil_igm_serum1,
																				['class' => 'input-medium id_combobox'])
																			}}
																		</td>
																		<td>
																			{{ Form::text(
																				'dlab[virus_igm_serum1]',
																				$row->rs_virus_igm_serum1,
																				['class' => 'input-large',
																				'placeholder' => 'Jenis Virus'])
																			}}
																		</td>
																		<td>
																			{{ Form::text(
																				'dlab[tgl_igm_serum1]',
																				Helper::getDate($row->rs_tgl_igm_serum1),
																				['class' => 'input-large',
																				'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																				'placeholder' => 'Tanggal Hasil Lab'])
																				}}
																		</td>
																	</tr>
																	<tr>
																		<td>IgM serum 2 (ulangan)</td>
																		<td>
																			{{Form::select(
																				'dlab[hasil_igm_serum2]',
																				['' => 'Pilih',
																				'Positif'=>'Positif',
																				'Negatif'=>'Negatif'],
																				$row->rs_hasil_igm_serum2,
																				['class' => 'input-medium id_combobox'])
																			}}
																		</td>
																		<td>
																			{{ Form::text(
																				'dlab[virus_igm_serum2]',
																				$row->rs_virus_igm_serum2,
																				['class' => 'input-large',
																				'placeholder' => 'Jenis Virus'])
																			}}
																		</td>
																		<td>
																			{{ Form::text(
																				'dlab[tgl_igm_serum2]',
																				Helper::getDate($row->rs_tgl_igm_serum2),
																				['class' => 'input-large',
																				'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																				'placeholder' => 'Tanggal Hasil Lab'])
																			}}
																		</td>
																	</tr>
																	<tr>
																		<td>IgG serum 1</td>
																		<td>
																			{{Form::select(
																				'dlab[hasil_igg_serum1]',
																				['' => 'Pilih',
																				'Positif'=>'Positif',
																				'Negatif'=>'Negatif'],
																				$row->rs_hasil_igg_serum1,
																				['class' => 'input-medium id_combobox'])
																			}}
																		</td>
																		<td>
																			{{ Form::text(
																				'dlab[virus_igg_serum1]',
																				$row->rs_virus_igg_serum1,
																				['class' => 'input-large',
																				'placeholder' => 'Jenis Virus'])
																				}}
																		</td>
																		<td>
																			{{ Form::text(
																				'dlab[tgl_igg_serum1]',
																				Helper::getDate($row->rs_tgl_igg_serum1),
																				['class' => 'input-large',
																				'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																				'placeholder' => 'Tanggal Hasil Lab'])
																			}}
																		</td>
																	</tr>
																	<tr>
																		<td>IgG serum 2 (ulangan)</td>
																		<td>
																			{{Form::select(
																				'dlab[hasil_igg_serum2]',
																				['' => 'Pilih',
																				'Positif'=>'Positif',
																				'Negatif'=>'Negatif'],
																				$row->rs_hasil_igg_serum2,
																				['class' => 'input-medium id_combobox'])
																			}}
																		</td>
																		<td>
																			{{ Form::text(
																				'dlab[virus_igg_serum2]',
																				$row->rs_virus_igg_serum2,
																				['class' => 'input-large',
																				'placeholder' => 'Jenis Virus'])
																			}}
																		</td>
																		<td>
																			{{ Form::text(
																				'dlab[tgl_igg_serum2]',
																				Helper::getDate($row->rs_tgl_igg_serum2),
																				['class' => 'input-large',
																				'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																				'placeholder' => 'Tanggal Hasil Lab'])
																			}}
																		</td>
																	</tr>
																	<tr>
																		<td>Isolasi</td>
																		<td>
																			{{Form::select(
																				'dlab[hasil_isolasi]',
																				['' => 'Pilih',
																				'Positif'=>'Positif',
																				'Negatif'=>'Negatif'],
																				$row->rs_hasil_isolasi,
																				['class' => 'input-medium id_combobox'])
																			}}
																		</td>
																		<td>
																			{{ Form::text(
																				'dlab[virus_isolasi]',
																				$row->rs_virus_isolasi,
																				['class' => 'input-large',
																				'placeholder' => 'Jenis Virus'])
																			}}
																		</td>
																		<td>
																			{{ Form::text(
																				'dlab[tgl_isolasi]',
																				Helper::getDate($row->rs_tgl_isolasi),
																				['class' => 'input-large',
																				'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																				'placeholder' => 'Tanggal Hasil Lab'])
																			}}
																		</td>
																	</tr>
																</table>
															</div>
														</div>
													</fieldset>
												</div>
											</div>
										</div>
									</fieldset>
									<div class="control-group">
										<label class="control-label">Klasifikasi Final</label>
										<div class="controls">
											{{Form::select(
												'dk[klasifikasi_final]',
												['' => 'Pilih',
												'CRS pasti(Lab Positif)'=>'CRS pasti(Lab Positif)',
												'CRS Klinis'=>'CRS Klinis',
												'Bukan CRS'=>'Bukan CRS',
												'Suspek CRS'=>'Suspek CRS',
												],
												$row->crs_klasifikasi_final,
												['id'=>'klasifikasi_final',
												'onchange'=>'klasifikasifinal()',
												'class' => 'input-medium id_combobox'])
											}}
											{{ Form::textarea(
												'dk[klasifikasi_final_desc]',
												$row->crs_klasifikasi_final_desc,
												['id'=>'desc_klasifikasi_final',
												'readonly'=>'readonly',
												'rows' => '3',
												'placeholder' => 'Description'])
											}}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-actions">
					{{Form::submit('simpan',['class'=>'btn btn-primary'])}}
						<a href="{{URL::to('crs')}}" class="btn btn-warning">batal</a>
					</div>
					<input type="hidden" value="<?php echo Sentry::getUser()->hak_akses;?>" id='hakakses'>
				{{Form::close()}}
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("select").select2();
		$('#id_provinsi_pelapor').change();
		$('#id_kabupaten_pelapor').change();
		$('#other_abnornalities').change();
		var hakakses = $('#hakakses').val();
		if (hakakses == 2 || hakakses == 7) {
			$('.akseskab').attr('readonly','readonly')
		};
		$('#id_kelurahan_pasien').change(function(){
			var nama_anak  = $('#nama_anak').val();
			var nik  = '';
			var nama_ortu  = $('#nama_ortu').val();
			var alamat = $(this).val();
			$.post('{{URL::to("cekPasien")}}', {nama_anak:nama_anak,nik:nik,nama_ortu:nama_ortu,id_kelurahan:alamat}, function(response){
				if(response){
					if(response.sent.status=='1'){
						alert(response.sent.message);
					};
				};
			});
		});
	})
	function showKabupaten(dt){
		var provinsi = $("#id_provinsi"+dt).val();
		var kabupaten = $("#id_kabupaten"+dt).val();
		$('.lodingKab').html('<img src="{{URL::to("style/images/loding.gif")}}">');
		$.post('{{URL::to("provinsi/getkab")}}', {provinsi:provinsi,kabupaten:kabupaten}, function(response)
		{
			$('#id_kabupaten'+dt).removeAttr('disabled','');
			$("#id_kabupaten"+dt).html(response);
			$('.lodingKab').html('');
		});
	}
	function showKecamatan(dt){
		var kabupaten = $("#id_kabupaten"+dt).val();
		$('.lodingKec').html('<img src="{{URL::to("style/images/loding.gif")}}">');
		$.post('{{URL::to("provinsi/getkec")}}', {kabupaten:kabupaten}, function(response)
		{
			$('#id_kecamatan'+dt).removeAttr('disabled','');
			$("#id_kecamatan"+dt).html(response);
			$('.lodingKec').html('');
		});
	}
	function showKelurahan(dt){
		var kecamatan= $("#id_kecamatan"+dt).val();
		$('.lodingKel').html('<img src="{{URL::to("style/images/loding.gif")}}">');
		$.post('{{URL::to("provinsi/getkel")}}', {kecamatan:kecamatan}, function(response)
		{
			$('#id_kelurahan'+dt).removeAttr('disabled','');
			$("#id_kelurahan"+dt).html(response);
			$('.lodingKel').html('');
		});
	}

	$('#form_save_crs').validate({
		submit: {
		  settings: {
				scrollToError: {
					  offset: -100,
					  duration: 500
				}
		  }
		}
	});

	$('#hidup').click(function(){
		$('.penyebab_meninggal').attr('readonly','readonly');
	})

	$('#meninggal').click(function(){
		$('.penyebab_meninggal').removeAttr('readonly');
	})


	function showabnornal() {
		var te = $("#other_abnornalities").val();
		if (te == 'Ya') {
			$('.other_ab').removeAttr('readonly');
		} else{
			$('.other_ab').attr('readonly','readonly');
		};
	}
	function klasifikasifinal() {
		var te = $("#klasifikasi_final").val();
		if (te == 'Discarded') {
			$('#desc_klasifikasi_final').removeAttr('readonly');
		} else{
			$('#desc_klasifikasi_final').attr('readonly','readonly');
		};
	}
	function actiondate(dt, val) {
		if (val == 'Ya') {
			$('.'+dt+'_date').removeAttr('readonly');
		} else{
			$('.'+dt+'_date').attr('readonly','readonly');
		};
	}

	function tgl_lahir()
	{
		$.ajax({
		    data:'tgl_tahun='+$('#tgl_tahun').val()+'&tgl_bulan='+$('#tgl_bulan').val()+'&tgl_hari='+$('#tgl_hari').val()+'&tanggal_timbul_demam='+$('.tgl_mulai_sakit').val()+'&tanggal_lahir='+$('.tgl_lahir').val(),
		    url:'{{URL::to("hitung/tgl_lahir")}}',
		    success:function(data) {
		       if(data.tgl==1) {
		            $('.tgl_lahir').val('');
		            $('.tgl_lahir').val(data.tgl1);
		            $('#tgl_tahun').val(data.tgl_tahun);
		            $('#tgl_bulan').val(data.tgl_bulan);
		            $('#tgl_hari').val(data.tgl_hari);

		       }
		        $('.tgl_lahir').val('');
		        $('.tgl_lahir').val(data);
		    }
		});
	}

	function umur()
	{
		$.ajax({
		    data:'tanggal_lahir='+$('.tgl_lahir').val(),
		    url:'{{URL::to("hitung/umur")}}',
		    success:function(data) {
		        $('#tgl_tahun').val(data.tgl_tahun);
		        $('#tgl_bulan').val(data.tgl_bulan);
		        $('#tgl_hari').val(data.tgl_hari);
		    }
		});
	}

	function pilihOptionUmur()
	{
		$('#tgl_tahun').removeAttr('readonly');
		$('#tgl_bulan').removeAttr('readonly');
		$('#tgl_hari').removeAttr('readonly');
		$('.tgl_lahir').attr('readonly','readonly');
		$('.tgl_lahir').val('');
		$('#tgl_tahun').val('');
		$('#tgl_bulan').val('');
		$('#tgl_hari').val('');
	}
	function pilihOptionTglLahir()
	{
		$('.tgl_lahir').removeAttr('readonly');
		$('#tgl_tahun').attr('readonly','readonly');
		$('#tgl_bulan').attr('readonly','readonly');
		$('#tgl_hari').attr('readonly','readonly');
		$('.tgl_lahir').val('');
		$('#tgl_tahun').val('');
		$('#tgl_bulan').val('');
		$('#tgl_hari').val('');
	}

</script>
@stop
