<div class="module-body uk-overflow-container">
	<div class="table-responsive">
		<table class="table table-bordered display" id="example_crs">
			<thead>
				<tr>
					<th>No.</th>
					<th>No. Epid</th>
					<th>Nama pasien</th>
					<th>Usia</th>
					<th>Alamat</th>
					<th>Keadaan Akhir</th>
					<th>Klasifikasi akhir</th>
					@if(Sentry::getUser()->hak_akses==7 || Sentry::getUser()->hak_akses==2)
						<th>PE</th>
					@else
						<th>Nama Faskes</th>
					@endif
					<th style="text-align:center">Aksi</th>
				</tr>
			</thead>

			<tbody>
				<?php $i=1;?>
				@foreach($data as $row)
					<tr>
						<td>{{$i++;}}</td>
						<td>{{ (empty($row->no_epid)?'Tidak Diisi':$row->no_epid) }}</td>
						<td>{{ $row->nama_anak }}</td>
                        @if($row->umur || $row->umur_bln || $row->umur_hr)
                          <td>{{ $row->umur.' Th '.$row->umur_bln.' bln '.$row->umur_hr.' hr' }}</td>
                        @else
                          <td>-</td>
                        @endif
						<td>{{ $row->alamat }}</td>
						<td>{{ (empty($row->keadaan_akhir_nm)?'Tidak Diisi':$row->keadaan_akhir_nm)}}</td>
						<td>{{ (empty($row->klasifikasi_akhir_nm))?'Tidak Diisi':$row->klasifikasi_akhir_nm }}</td>
						@if(Sentry::getUser()->hak_akses==7 || Sentry::getUser()->hak_akses==2)
							<td>
								@if($row->id_pe_crs AND $row->pe_faskes_id AND $row->pe_faskes_id != 0)
								<a class="btn btn-danger">Sudah PE</a>
								@else
								<a href="{{URL::to('crs/entricrs/'.$row->id_crs)}}" data-uk-tooltip title="Penyelidikan epidemologi" class="btn btn-primary">PE</a>
								@endif
							</td>
						@else
							<td>{{$row->nama_faskes}}</td>
						@endif

						@if(Sentry::getUser()->hak_akses==2 || Sentry::getUser()->hak_akses==3 || Sentry::getUser()->hak_akses==7)
							<td>
								<div class="btn-group" role="group" >
								<a href="{{URL::to('crs_detail/'.$row->id_crs)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
								<a href="{{URL::to('crs_edit/'.$row->id_crs)}}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>
								<a href="{{URL::to('crs_hapus/'.$row->id_crs)}}" onclick="return confirm('apakah yakin ingin menghapus data ini?');return false;" class="btn btn-xs btn-danger" data-uk-tooltip title="hapus data"><i class="icon icon-remove"></i></a>
								</div>
							</td>
						@elseif(Sentry::getUser()->hak_akses==4)
							<td>
								<div class="btn-group" role="group" >
								<a href="{{URL::to('crs_detail/'.$row->id_crs)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
								<a href="{{URL::to('crs_edit/'.$row->id_crs)}}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>
								</div>
							</td>
						@elseif(Sentry::getUser()->hak_akses==1 || Sentry::getUser()->hak_akses==6)
							<td>
								<div class="btn-group" role="group" >
								<a href="{{URL::to('crs_detail/'.$row->id_crs)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
								<a href="{{URL::to('crs_hapus/'.$row->id_crs)}}" onclick="return confirm('apakah yakin ingin menghapus data ini?');return false;" class="btn btn-xs btn-danger" data-uk-tooltip title="hapus data"><i class="icon icon-remove"></i></a>
								</div>
							</td>
						@elseif(Sentry::getUser()->hak_akses==5)
							<td></td>
						@endif
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript" charset="utf-8">
	$('#example_crs').dataTable();
</script>