<div class="alert alert-error">
	<p style="font-size:12px">Text input yang bertanda bintang <strong>(*)</strong> wajib di isi
</div>
{{ Form::open(
	array(
		'url' =>'crs_store',
		'class' =>'form-horizontal',
		'id'=>'form_save_crs'
	))
}}
<div class="module-body">
	<div class="row-fluid">
		<div class="span12">
			<div class="media">
				<fieldset>
					<legend>Data Pelapor</legend>
					<div class="control-group">
						<label class="control-label">Nama Faskes</label>
						<div class="controls">
							<?php
								$type = Session::get('type');
								$kd_faskes = Session::get('kd_faskes');
								$id_provinsi=$id_kabupaten=$namainstansi='';
								$nama_kabupaten = 'Pilih Kabupaten';
								if (Sentry::getUser()->hak_akses==2 || Sentry::getUser()->hak_akses==7) {
									if($type=='rs') {
										$data = DB::table('rumahsakit2')
														->select('nama_faskes','alamat','kode_faskes')
														->where('kode_faskes',$kd_faskes)
														->get();
										$namainstansi = $data[0]->nama_faskes;
										$id_kabupaten = substr($kd_faskes, 0, 4);
									}else if($type=='puskesmas'){
										$data = DB::table('puskesmas')
													->select('puskesmas_name','puskesmas_code_faskes','kode_kab')
													->where('puskesmas_code_faskes',$kd_faskes)
													->get();
										$namainstansi = 'Puskesmas '.$data[0]->puskesmas_name;
										$id_kabupaten = $data[0]->kode_kab;
									}
									$dtKabupaten = DB::table('kabupaten')->select('id_kabupaten','id_provinsi','kabupaten')->where('id_kabupaten',$id_kabupaten)->get();
									$dtProvinsi = DB::table('provinsi')->select('id_provinsi','provinsi')->where('id_provinsi',$dtKabupaten[0]->id_provinsi)->get();
									$id_provinsi = $dtProvinsi[0]->id_provinsi;
									$nama_kabupaten = $dtKabupaten[0]->kabupaten;
								}

								echo Form::text(
									'dp[nama_rs]',
									$namainstansi,
									['class' => 'input-xlarge']
								);
							?>
						<span class="help-inline" style="color:red">(*)</span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Provinsi</label>
						<div class="controls">
						{{ Form::select(
							'dp[id_provinsi]',
							array('0'=>'Pilih provinsi')+Provinsi::lists('provinsi','id_provinsi'),
							$id_provinsi,
							array(
								'id'=>'id_provinsi',
								'placeholder' => 'Pilih Provinsi',
								'onchange'=>'showKabupaten()',
								'class'=>'input-large id_combobox'
							))
						}}
						<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Kabupaten</label>
						<div class="controls">
						{{ Form::select(
							'dp[id_kabupaten]',
							 array(
								$id_kabupaten => $nama_kabupaten
							),
							null,
							array(
								'class' => 'input-large id_combobox',
								'id' => 'id_kabupaten',
							))
						}}
						<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Tanggal Laporan</label>
						<div class="controls">
						{{ Form::text(
							'dp[tanggal_laporan]',
							Input::old('dp[tanggal_laporan]'),
							array(
								'class' => 'input-medium',
								'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
								'placeholder' => 'Tanggal Laporan',
							))
						}}
						<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Tanggal Investigasi</label>
						<div class="controls">
						{{ Form::text(
							'dp[tanggal_investigasi]',
							Input::old('dp[tanggal_investigasi]'),
							array(
								'class' => 'input-medium',
								'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
								'placeholder' => 'Tanggal Investigasi',
							))
						}}
						<span class="help-inline"></span>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
	  <div class="row-fluid">
			<div class="span12">
				  <div class="media">
						<fieldset>
						<legend>Data Pasien</legend>
						<div class="control-group">
							  <label class="control-label">Nomor EPID</label>
							  <div class="controls">
							  {{ Form::text(
									'dk[no_epid]',
									Input::old('dk[no_epid]'),
									array(
										'class' => 'input-xlarge',
										'id'=>'no_epid',
										'readonly'=>'readonly',
										'placeholder' => 'Nomor EPID (Diisi oleh kabupaten)'
									))
							  }}
							  <span class="help-inline"></span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Nomor RM</label>
							  <div class="controls">
							  {{ Form::text(
									'dpa[no_rm]',
									Input::old('dpa[no_rm]'),
									array(
										  'class' => 'input-xlarge',
										  'placeholder' => 'Nomor RM'
									))
							  }}
							  <span class="help-inline"></span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Nama Bayi</label>
							  <div class="controls">
							  {{ Form::text(
									'dpa[nama_anak]',
									Input::old('dpa[nama_anak]'),
									array(
										  'data-validation' => '[MIXED]',
										  'id'=>'nama_anak',
										  'data' => '$ wajib di isi!',
										  'data-validation-message' => 'wajib di isi!',
										  'class' => 'input-xlarge',
										  'placeholder' => 'Nama Bayi'
									))
							  }}
							  <span class="help-inline" style="color:red">(*)</span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Jenis kelamin</label>
							  <div class="controls">
							  {{Form::select(
									'dpa[jenis_kelamin]',
									array(
										  null => 'Pilih',
										  '1'=>'Laki-laki',
                                          '2'=>'Perempuan',
										  '3'=>'Tidak Jelas',
									),
									null,
									array(
										  'data-validation'=>'[MIXED]',
										  'data'=>'$ wajib di isi!',
										  'data-validation-message'=>'jenis kelamin wajib di isi!',
										  'class' => 'input-medium id_combobox'
									))
							  }}
							  <span class="help-inline" style="color:red">(*)</span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Tanggal lahir</label>
							  <div class="controls">
							  {{ Form::text(
									'dpa[tanggal_lahir]',
									Input::old('dpa[tanggal_lahir]'),
									array(
										'class' => 'input-medium tgl_lahir',
										'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
										'placeholder' => 'Tanggal lahir',
										'id'=>'tgl_lahir',
										'onchange' => 'usia(), showEpidCrs("_pasien")',
									))
							  }}
							  <span class="help-inline" ></span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Umur</label>
							  <div class="controls">
									<input type="text" class="input-mini"  name="dpa[umur]" id="tgl_tahun">Thn
									<input type="text" class="input-mini" name="dpa[umur_bln]" id="tgl_bulan">Bln
									<input type="text" class="input-mini" name="dpa[umur_hr]" id="tgl_hari">Hari
							  </div>
						</div>
					<!-- awal input kolom provinsi -->
						<div class="control-group">
							<label class="control-label">Provinsi</label>
							<div class="controls">
								{{
									Form::select(
										'dpa[id_provinsi]',
										array('0' => 'Pilih provinsi')+ Provinsi::lists('provinsi','id_provinsi'),
										null,
										array(
											'id'       => 'id_provinsi_pasien',
											'onchange' => 'show_kabupaten("_pasien")',
											'class'    => 'input-large id_combobox'
										)
									)
								}}
								<span class="help-inline" id="error-id_provinsi_pasien"></span>
							</div>
						</div>
						<!-- awal input kolom kabupaten -->
						<div class="control-group">
							<label class="control-label">Kabupaten</label>
							<div class="controls">
								{{
									Form::select(
										'dpa[id_kabupaten]',
										array('0' => 'Pilih kabupaten'),
										null,
										array(
											'class'    => 'input-large id_combobox',
											'id'       => 'id_kabupaten_pasien',
											'onchange' => 'show_kecamatan("_pasien")'
										)
									)
								}}
                      			<span class="lodingKab"></span>
								<span class="help-inline" id="error-id_kabupaten_pasien"></span>
							</div>
						</div>
						<!-- awal input kolom kecamatan -->
						<div class="control-group">
							<label class="control-label">Kecamatan</label>
							<div class="controls">
								{{
									Form::select(
										'dpa[id_kecamatan]',
										array(
											'0' => 'Pilih kecamatan'
										),
										null,
										array(
											'class'    => 'input-large id_combobox',
											'id'       => 'id_kecamatan_pasien',
											'onchange' => 'show_kelurahan("_pasien")'
										)
									)
								}}
                      			<span class="lodingKec"></span>
								<span class="help-inline" id="error-id_kecamatan_pasien"></span>
							</div>
						</div>
						<!-- awal input kolom kelurahan -->
						<div class="control-group">
							<label class="control-label">Kelurahan/Desa</label>
							<div class="controls">
								{{
									Form::select(
										'dpa[id_kelurahan]',
										array(
											'' => 'Pilih kelurahan/desa'
										),
										null,
										array(
											'class' => 'input-large id_combobox',
											'id'    => 'id_kelurahan_pasien',
											'onchange'=>'showEpidCrs("_pasien")'
										)
									)
								}}
                      			<span class="lodingKel"></span>
								<span class="help-inline" style="color:red">(*)</span>
							</div>
						</div>
						<div class="control-group">
							  <label class="control-label">Alamat</label>
							  <div class="controls">
							  {{ Form::textarea(
									'dpa[alamat]',
									null,
									array(
										  'rows' => '3',
										  'placeholder' => 'Hanya diisi nama jalan, no. rumah dan no. RT - RW'
									))
							  }}
							  <span class="help-inline"></span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Tempat bayi di lahirkan</label>
							  <div class="controls">
							  {{ Form::text(
									'dpa[tempat_lahir_bayi]',
									null,
									array(
										  'class' => 'input-xlarge',
										  'placeholder' => 'Tempat bayi di lahirkan'
									))
							  }}
							  <span class="help-inline"></span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Nama Ibu</label>
							  <div class="controls">
							  {{ Form::text(
									'dpa[nama_ortu]',
									null,
									array(
										  'class' => 'input-xlarge',
										  'id'=>'nama_ortu',
										  'placeholder' => 'Nama Ibu'
									))
							  }}
							  <span class="help-inline"></span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Nomer Telp</label>
							  <div class="controls">
							  {{ Form::text(
									'dpa[no_telp]',
									null,
									array(
										  'class' => 'input-xlarge',
										  'placeholder' => 'Nomer Telp'
									))
							  }}
							  <span class="help-inline"></span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Umur Kehamilan saat bayi di lahirkan</label>
							  <div class="controls">
							  {{Form::selectRange(
									'dpa[umur_kehamilan_bayi]',
									20, 60,null,
									['class' => 'input-medium id_combobox'])
							  }}
							  <span class="help-inline"></span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Berat badan bayi baru lahir</label>
							  <div class="controls">
							  {{ Form::text(
									'dpa[berat_badan_bayi]',
									null,
									array(
										  'class' => 'input-small',
										  'placeholder' => 'Berat badan bayi'
									))
							  }} Gram
							  <span class="help-inline"></span>
							  </div>
						</div>
				  </fieldset>
				  </div>
			</div>
	  </div>
	  <div class="row-fluid">
			<div class="span12">
				  <div class="media">
						<fieldset>
							  <legend>Data Klinis Pasien</legend>
							  <div class="row-fluid">
									<div class="span6">
										  <div class="media">
												<fieldset>
													  <legend>Group A (Lengkapi semua tanda dan gejala yang ada)</legend>
													  <div class="control-group">
															<label class="control-label">Congenital heart disease</label>
															<div class="controls">
															{{Form::select(
																  'dk[congenital_heart_disease]',
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																  null,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Cataracts</label>
															<div class="controls">
															{{Form::select(
																  'dk[cataracts]',
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																  null,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Congenital glaucoma</label>
															<div class="controls">
															{{Form::select(
																  'dk[congenital_glaucoma]',
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																  null,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Pigmentary retinopathy</label>
															<div class="controls">
															{{Form::select(
																  'dk[pigmentary_retinopathy]',
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																  null,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Hearing impairment</label>
															<div class="controls">
															{{Form::select(
																  'dk[hearing_impairment]',
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																  null,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
												</fieldset>
										  </div>
										  <div class="control-group">
												<label class="control-label">Nama Dokter Pemeriksa</label>
												<div class="controls">
													{{ Form::textarea(
					  									'dk[nama_dokter_pemeriksa]',
					  									null,
					  									array(
					  										  'rows' => '3',
					  										  'placeholder' => 'Nama Dokter Pemeriksa'
					  									))
					  							  	}}
												<span class="help-inline"></span>
												</div>
										  </div>
										  <div class="control-group">
												<label class="control-label">Tanggal periksa</label>
												<div class="controls">
												{{ Form::text(
													  'dk[tgl_mulai_sakit]',
													  null,
													  array(
															'data-validation' => '[MIXED]',
															'data' => '$ wajib di isi!',
															'data-validation-message' => 'wajib di isi!',
															'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
															'class' => 'input-xlarge tgl_mulai_sakit',
															'id'=>'tgl_mulai_sakit',
															'onchange'=>'usia()',
															'placeholder' => 'Tanggal Periksa'
													  ))
												}}
												<span class="help-inline" style="color:red">(*)</span>
												</div>
										  </div>
										  <div class="control-group">
												<label class="control-label">Keadaan Bayi saat ini</label>
												<div class="controls">
													<table>
														<tr>
															<td>{{ Form::radio('dk[keadaan_akhir]', 'Hidup', false, array('id'=>'hidup')) }}</td>
															<td>Hidup</td>
														</tr>
														<tr>
															<td>{{ Form::radio('dk[keadaan_akhir]', 'Meninggal', false, array('id'=>'meninggal')) }}</td>
															<td>Meninggal</td>
															<td>
																{{ Form::text(
																	'dk[penyebab_meninggal]',
																	null,
																	array(
																		'class' => 'input-medium penyebab_meninggal',
																		'placeholder' => 'Penyebab Meninggal',
																		'readonly'=>'readonly',
																	))
																}}
															</td>
														</tr>
													</table>
												<span class="help-inline"></span>
												</div>
										  </div>
									</div>
									<div class="span6">
										  <div class="media">
												<fieldset>
													  <legend>Group B (Lengkapi semua tanda dan gejala yang ada)</legend>
													  <div class="control-group">
															<label class="control-label">Purpura</label>
															<div class="controls">
															{{Form::select(
																  'dk[purpura]',
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																  null,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Microcephaly</label>
															<div class="controls">
															{{Form::select(
																  'dk[microcephaly]',
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																  null,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Meningoencephalitis</label>
															<div class="controls">
															{{Form::select(
																  'dk[meningoencephalitis]',
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																  null,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Ikterik 24 jam post partum</label>
															<div class="controls">
															{{Form::select(
																  'dk[ikterik]',
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																  null,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Splenomegaly</label>
															<div class="controls">
															{{Form::select(
																  'dk[splenomegaly]',
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																  null,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Developmental delay</label>
															<div class="controls">
															{{Form::select(
																  'dk[developmental_delay]',
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																  null,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Radiolucent bone disease</label>
															<div class="controls">
															{{Form::select(
																  'dk[radiolucent]',
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																  null,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Other abnormalities</label>
															<div class="controls">
															{{Form::select(
																  'dk[other_abnormal]',
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																  ),
																  null,
																  array(
																		'onchange'=>'showabnornal()',
																		'class' => 'input-medium id_combobox',
																		'id'=>'other_abnornalities'
																  ))
															}}
															{{ Form::text(
																  'dk[dt_other_abnormal]',
																  null,
																  array(
																		'class'=>'input-medium other_ab',
																		'style'=>'padding:3px',
																		'readonly'=>'readonly',
																		'placeholder' => 'Other Abnormalities'
																  ))
															  }}
															<span class="help-inline"></span>
															</div>
													  </div>
												</fieldset>
										  </div>
									</div>
							  </div>

						</fieldset>
				  </div>
			</div>
	  </div>
	  <div class="row-fluid">
			<div class="span12">
				  <div class="media">
					  <fieldset>
						<legend>Data Riwayat Kehamilan Ibu</legend>
						<div class="control-group">
							  <label class="control-label">Jumlah kehamilan sebelumnya</label>
							  <div class="controls">
							  {{ Form::text(
								  'dpe[jumlah_kehamilan_sebelumnya]',
								  Input::old('dpe[jumlah_kehamilan_sebelumnya]'),
								  array(
									  'placeholder' => 'Jumlah kehamilan sebelumnya',
									  'class'=>'input-medium'
								  ))
							  }}
							  &nbsp;(Dalam bentuk angka)
							  <span class="help-inline"></span>
							  </div>
						  </div>
						  <div class="control-group">
							  <label class="control-label">Umur Ibu (dalam tahun)</label>
							  <div class="controls">
							  {{ Form::text(
								  'dpe[umur_ibu]',
								  Input::old('dpe[umur_ibu]'),
								  array(
									  'placeholder' => 'Umur Ibu (dalam tahun)'
								  ))
							  }}
							  <span class="help-inline"></span>
							  </div>
						  </div>
						  <div class="control-group">
							  <label class="control-label">Hari Pertama haid terakhir (HPHT)</label>
							  <div class="controls">
							  {{ Form::text(
								  'dpe[hari_pertama_haid]',
								  Input::old('dpe[hari_pertama_haid]'),
								  array(
									  'placeholder' => 'Hari pertama haid terakhir (HPHT)',
								  ))
							  }}
							  <span class="help-inline"></span>
							  </div>
						  </div>
						  <div class="control-group">
							  Apakah selama kehamilan terakhir ini ibu pernah mengalami
						  </div>
						  <div class="control-group">
							  <table>
								  <tr>
									  <td>Conjunctivitis</td>
									  <td>
										  {{Form::select(
												'dpe[conjunctivitis]',
												array(
													  '' => 'Pilih',
													  'Ya'=>'Ya',
													  'Tidak'=>'Tidak',
													  'Tidak Tahu'=>'Tidak Tahu',
												),
												null,
												array(
													'id'=>'conjunctivitis',
													'onchange'=>'actiondate(this.id, this.value)',
													'class' => 'input-medium id_combobox'
												))
										  }}
									  </td>
									  <td>
										  {{ Form::text(
											  'dpe[conjunctivitis_date]',
											  null,
											  array(
												'class'=>'conjunctivitis_date',
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												'readonly'=>'readonly',
												'placeholder'=>'Tanggal Kejadian'
											  ))
										  }}
									  </td>
								  </tr>
								  <tr>
									  <td>Pilek</td>
									  <td>
										  {{Form::select(
												'dpe[pilek]',
												array(
													  '' => 'Pilih',
													  'Ya'=>'Ya',
													  'Tidak'=>'Tidak',
													  'Tidak Tahu'=>'Tidak Tahu',
												),
												null,
												array(
													'id'=>'pilek',
													'onchange'=>'actiondate(this.id, this.value)',
													'class' => 'input-medium id_combobox'
												))
										  }}
									  </td>
									  <td>
										  {{ Form::text(
											  'dpe[pilek_date]',
											  null,
											  array(
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												  'readonly'=>'readonly',
												  'class'=>'pilek_date',
												  'placeholder'=>'Tanggal Kejadian'
											  ))
										  }}
									  </td>
								  </tr>
								  <tr>
									  <td>Batuk</td>
									  <td>
										  {{Form::select(
												'dpe[batuk]',
												array(
													  '' => 'Pilih',
													  'Ya'=>'Ya',
													  'Tidak'=>'Tidak',
													  'Tidak Tahu'=>'Tidak Tahu',
												),
												null,
												array(
													'id'=>'batuk',
													'onchange'=>'actiondate(this.id, this.value)',
													'class' => 'input-medium id_combobox'
												))
										  }}
									  </td>
									  <td>
										  {{ Form::text(
											  'dpe[batuk_date]',
											  null,
											  array(
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												  'readonly'=>'readonly',
												  'class'=>'batuk_date',
												  'placeholder'=>'Tanggal Kejadian'
											  ))
										  }}
									  </td>
								  </tr>
								  <tr>
									  <td>Ruam makulopapular</td>
									  <td>
										  {{Form::select(
												'dpe[ruam_makulopapular]',
												array(
													  '' => 'Pilih',
													  'Ya'=>'Ya',
													  'Tidak'=>'Tidak',
													  'Tidak Tahu'=>'Tidak Tahu',
												),
												null,
												array(
													'id'=>'ruam_makulopapular',
													'onchange'=>'actiondate(this.id, this.value)',
													'class' => 'input-medium id_combobox'
												))
										  }}
									  </td>
									  <td>
										  {{ Form::text(
											  'dpe[ruam_makulopapular_date]',
											  null,
											  array(
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												  'readonly'=>'readonly',
												  'class'=>'ruam_makulopapular_date',
												  'placeholder'=>'Tanggal Kejadian'
											  ))
										  }}
									  </td>
								  </tr>
								  <tr>
									  <td>Pembengkakan kelenjar limfa</td>
									  <td>
										  {{Form::select(
												'dpe[pembengkakan_kelenjar_limfa]',
												array(
													  '' => 'Pilih',
													  'Ya'=>'Ya',
													  'Tidak'=>'Tidak',
													  'Tidak Tahu'=>'Tidak Tahu',
												),
												null,
												array(
													'id'=>'pembengkakan_kelenjar_limfa',
													'onchange'=>'actiondate(this.id, this.value)',
													'class' => 'input-medium id_combobox'
												))
										  }}
									  </td>
									  <td>
										  {{ Form::text(
											  'dpe[pembengkakan_kelenjar_limfa_date]',
											  null,
											  array(
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												  'readonly'=>'readonly',
												  'class'=>'pembengkakan_kelenjar_limfa_date',
												  'placeholder'=>'Tanggal Kejadian'
											  ))
										  }}
									  </td>
								  </tr>
								  <tr>
									  <td>Demam</td>
									  <td>
										  {{Form::select(
												'dpe[demam]',
												array(
													  '' => 'Pilih',
													  'Ya'=>'Ya',
													  'Tidak'=>'Tidak',
													  'Tidak Tahu'=>'Tidak Tahu',
												),
												null,
												array(
													'id'=>'demam',
													'onchange'=>'actiondate(this.id, this.value)',
													'class' => 'input-medium id_combobox'
												))
										  }}
									  </td>
									  <td>
										  {{ Form::text(
											  'dpe[demam_date]',
											  null,
											  array(
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												  'readonly'=>'readonly',
												  'class'=>'demam_date',
												  'placeholder'=>'Tanggal Kejadian'
											  ))
										  }}
									  </td>
								  </tr>
								  <tr>
									  <td>Arthralgia/arthritis</td>
									  <td>
										  {{Form::select(
												'dpe[arthralgia]',
												array(
													  '' => 'Pilih',
													  'Ya'=>'Ya',
													  'Tidak'=>'Tidak',
													  'Tidak Tahu'=>'Tidak Tahu',
												),
												null,
												array(
													'id'=>'arthralgia',
													'onchange'=>'actiondate(this.id, this.value)',
													'class' => 'input-medium id_combobox'
												))
										  }}
									  </td>
									  <td>
										  {{ Form::text(
											  'dpe[arthralgia_date]',
											  null,
											  array(
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												  'readonly'=>'readonly',
												  'class'=>'arthralgia_date',
												  'placeholder'=>'Tanggal Kejadian'
											  ))
										  }}
									  </td>
								  </tr>
								  <tr>
									  <td>Komplikasi lain</td>
									  <td>
										  {{Form::select(
												'dpe[komplikasi_lain]',
												array(
													  '' => 'Pilih',
													  'Ya'=>'Ya',
													  'Tidak'=>'Tidak',
													  'Tidak Tahu'=>'Tidak Tahu',
												),
												null,
												array(
													'id'=>'komplikasi_lain',
													'onchange'=>'actiondate(this.id, this.value)',
													'class' => 'input-medium id_combobox'
												))
										  }}
									  </td>
									  <td>
										  {{ Form::text(
											  'dpe[komplikasi_lain_date]',
											  null,
											  array(
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												  'readonly'=>'readonly',
												  'class'=>'komplikasi_lain_date',
												  'placeholder'=>'Tanggal Kejadian'
											  ))
										  }}
									  </td>
								  </tr>
								  <tr>
									  <td>Mendapat Vaksinasi rubella</td>
									  <td>
										  {{Form::select(
												'dpe[vaksinasi_rubella]',
												array(
													  '' => 'Pilih',
													  'Ya'=>'Ya',
													  'Tidak'=>'Tidak',
													  'Tidak Tahu'=>'Tidak Tahu',
												),
												null,
												array(
													'id'=>'vaksinasi_rubella',
													'onchange'=>'actiondate(this.id, this.value)',
													'class' => 'input-medium id_combobox'
												))
										  }}
									  </td>
									  <td>
										  {{ Form::text(
											  'dpe[vaksinasi_rubella_date]',
											  null,
											  array(
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												  'readonly'=>'readonly',
												  'class'=>'vaksinasi_rubella_date',
												  'placeholder'=>'Tanggal Kejadian'
											  ))
										  }}
									  </td>
								  </tr>
							  </table>
						  </div>
						  <div class="control-group">
							  <label class="control-label">Apakah selama kehamilan ini ibu pernah didiagnosa rubella dengan konfirmasi lab</label>
							  <div class="controls">
							  {{Form::select(
									'dpe[diagnosa_rubella]',
									array(
										  '' => 'Pilih',
										  'Ya'=>'Ya',
										  'Tidak'=>'Tidak',
										  'Tidak Tahu'=>'Tidak Tahu',
									),
									null,
									array(
										'id'=>'diagnosa_rubella',
										'onchange'=>'actiondate(this.id, this.value)',
										'class' => 'input-medium id_combobox'
									))
							  }}
							  {{ Form::text(
								  'dpe[tgl_diagnosa_rubella]',
								  null,
								  array(
										'readonly'=>'readonly',
									  'class' => 'input-large diagnosa_rubella_date',
									  'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
									  'placeholder' => 'Kapan diagnosa dilakukan',
								  ))
							  }}
							  <span class="help-inline"></span>
							  </div>
						  </div>
						  <div class="control-group">
							  <label class="control-label">Apakah selama kehamilan ini ibu pernah terpapar atau berkontak dengan orang yang menderita ruam makulopapular ?</label>
							  <div class="controls">
							  {{Form::select(
									'dpe[kontak_ruam_makulopapular]',
									array(
										  '' => 'Pilih',
										  'Ya'=>'Ya',
										  'Tidak'=>'Tidak',
										  'Tidak Tahu'=>'Tidak Tahu',
									),
									null,
									array(
										'id'=>'kontak_ruam_makulopapular',
										'onchange'=>'actiondate(this.id, this.value)',
										'class' => 'input-medium id_combobox'
									))
							  }}
							  <span class="help-inline"></span>
							  </div>
						  </div>
						  <div class="control-group">
							  <label class="control-label">Umur Kehamilan saat terpapar dengan penderita ruam makulopapular</label>
							  <div class="controls">
							  {{Form::selectRange(
									'dpe[umur_hamil_kontak_ruam_makulopapular]',
									0, 60,null,
									array(
										'readonly'=>'readonly',
										  'class' => 'input-medium id_combobox kontak_ruam_makulopapular_date'
									))
							  }}
							  {{ Form::textarea(
								  'dpe[desc_kontak_ruam_makulopapular]',
								  null,
								  array(
									'readonly'=>'readonly',
									'rows' => '3',
									  'class' => 'kontak_ruam_makulopapular_date',
									  'placeholder' => 'Dimana terkena kontak',
								  ))
							  }}
							  <span class="help-inline"></span>
							  </div>
						  </div>
						  <div class="control-group">
							  <label class="control-label">Apakah ibu bepergian selama kehamilan terakhir ini</label>
							  <div class="controls">
							  {{Form::select(
									'dpe[pergi_waktu_hamil]',
									array(
										  '' => 'Pilih',
										  'Ya'=>'Ya',
										  'Tidak'=>'Tidak',
										  'Tidak Tahu'=>'Tidak Tahu',
									),
									null,
									array(
										'id'=>'pergi_waktu_hamil',
										'onchange'=>'actiondate(this.id, this.value)',
										'class' => 'input-medium id_combobox'
									))
							  }}
							  <span class="help-inline"></span>
							  </div>
						  </div>
						  <div class="control-group">
							  <label class="control-label">Umur kehamilan ketika bepergian</label>
							  <div class="controls">
							  {{Form::selectRange(
									'dpe[umur_hamil_waktu_pergi]',
									0, 60,null,
									array(
										'readonly'=>'readonly',
										  'class' => 'input-medium id_combobox pergi_waktu_hamil_date'
									))
							  }}
							  {{ Form::textarea(
								  'dpe[desc_pergi_waktu_hamil]',
								  null,
								  array(
									'readonly'=>'readonly',
									'rows'=>'3',
									  'class' => 'pergi_waktu_hamil_date',
									  'placeholder' => 'Bepergian kemana',
								  ))
							  }}
							  <span class="help-inline"></span>
							  </div>
						  </div>
					  </fieldset>
				  </div>
			</div>
	  </div>
	  <div class="row-fluid">
		<div class="span12">
			<div class="media">
				<fieldset>
					<legend>Data Pemeriksaan dan Hasil Laboratorium pada bayi</legend>
					<div class="row-fluid">
						<div class="span12">
							<div class="media">
								<fieldset>
									<legend>Pemeriksaan Laboratorium</legend>
									<div class="control-group">
										<label class="control-label">Apakah spesimen diambil</label>
										<div class="controls">
										{{Form::select(
											'dlab[spesimen]',
											array(
												'' => 'Pilih',
												'Ya'=>'Ya',
												'Tidak'=>'Tidak',
												'Tidak Tahu'=>'Tidak Tahu',
											),
											null,
											array(
												'class' => 'input-medium id_combobox'
											))
										}}
										<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Jenis Spesimen</label>
										<div class="controls">
											<table>
												<tr>
													<td>Serum 1</td>
													<td>
														{{ Form::text(
															'dlab[tgl_ambil_serum1]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Ambil',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_kirim_serum1]',
															null,
															array(
																'class' => 'input-large akseskab',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal dikirim ke Lab (Di isi Kab/provinsi)',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_tiba_serum1]',
															null,
															array(
																'class' => 'input-large akseskab',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal tiba di lab',
															))
														}}
													</td>
												</tr>
												<tr>
													<td>Serum 2</td>
													<td>
														{{ Form::text(
															'dlab[tgl_ambil_serum2]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Ambil',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_kirim_serum2]',
															null,
															array(
																'class' => 'input-large akseskab',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal dikirim ke Lab (Di isi Kab/provinsi)',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_tiba_serum2]',
															null,
															array(
																'class' => 'input-large akseskab',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal tiba di lab',
															))
														}}
													</td>
												</tr>
												<tr>
													<td>Throat Swab</td>
													<td>
														{{ Form::text(
															'dlab[tgl_ambil_throat_swab]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Ambil',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_kirim_throat_swab]',
															null,
															array(
																'class' => 'input-large akseskab',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal dikirim ke Lab (Di isi Kab/provinsi)',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_tiba_throat_swab]',
															null,
															array(
																'class' => 'input-large akseskab',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal tiba di lab',
															))
														}}
													</td>
												</tr>
												<tr>
													<td>Urine</td>
													<td>
														{{ Form::text(
															'dlab[tgl_ambil_urine]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Ambil',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_kirim_urine]',
															null,
															array(
																'class' => 'input-large akseskab',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal dikirim ke Lab (Di isi Kab/provinsi)',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_tiba_urine]',
															null,
															array(
																'class' => 'input-large akseskab',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal tiba di lab',
															))
														}}
													</td>
												</tr>
											</table>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<div class="media">
								<fieldset>
									<legend>Hasil Pemeriksaan Laboratorium</legend>
									<div class="control-group">
										<div class="controls">
											<table>
												<tr>
													<td><b>Jenis Pemeriksaan</b></td>
													<td colspan="3"><b>Hasil</b></td>
												</tr>
												<tr>
													<td>IgM serum ke 1</td>
													<td>
														{{Form::select(
															  'dlab[hasil_igm_serum1]',
															  array(
																	'' => 'Pilih',
																	'Positif'=>'Positif',
																	'Negatif'=>'Negatif',
															  ),
															  null,
															  array(
																	'class' => 'input-medium id_combobox'
															  ))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[virus_igm_serum1]',
															null,
															array(
																'class' => 'input-medium',
																'placeholder' => 'Jenis Virus',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_igm_serum1]',
															null,
															array(
																'class' => 'input-medium',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Hasil Lab',
															))
														}}
													</td>
												</tr>
												<tr>
													<td>IgM serum 2 (ulangan)</td>
													<td>
														{{Form::select(
															  'dlab[hasil_igm_serum2]',
															  array(
																	'' => 'Pilih',
																	'Positif'=>'Positif',
																	'Negatif'=>'Negatif',
															  ),
															  null,
															  array(
																	'class' => 'input-medium id_combobox'
															  ))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[virus_igm_serum2]',
															null,
															array(
																'class' => 'input-medium',
																'placeholder' => 'Jenis Virus',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_igm_serum2]',
															null,
															array(
																'class' => 'input-medium',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Hasil Lab',
															))
														}}
													</td>
												</tr>
												<tr>
													<td>IgG serum 1</td>
													<td>
														{{Form::select(
															  'dlab[hasil_igg_serum1]',
															  array(
																	'' => 'Pilih',
																	'Positif'=>'Positif',
																	'Negatif'=>'Negatif',
															  ),
															  null,
															  array(
																	'class' => 'input-medium id_combobox'
															  ))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[virus_igg_serum1]',
															null,
															array(
																'class' => 'input-medium',
																'placeholder' => 'Jenis Virus',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_igg_serum1]',
															null,
															array(
																'class' => 'input-medium',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Hasil Lab',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[kadar_igg_serum1]',
															null,
															array(
																'class' => 'input-medium',
																'placeholder' => 'Kadar IgG',
															))
														}}
													</td>
												</tr>
												<tr>
													<td>IgG serum 2 (ulangan)</td>
													<td>
														{{Form::select(
															  'dlab[hasil_igg_serum2]',
															  array(
																	'' => 'Pilih',
																	'Positif'=>'Positif',
																	'Negatif'=>'Negatif',
															  ),
															  null,
															  array(
																	'class' => 'input-medium id_combobox'
															  ))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[virus_igg_serum2]',
															null,
															array(
																'class' => 'input-medium',
																'placeholder' => 'Jenis Virus',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_igg_serum2]',
															null,
															array(
																'class' => 'input-medium',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Hasil Lab',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[kadar_igg_serum2]',
															null,
															array(
																'class' => 'input-medium',
																'placeholder' => 'Kadar IgG',
															))
														}}
													</td>
												</tr>
												<tr>
													<td>Isolasi</td>
													<td>
														{{Form::select(
															  'dlab[hasil_isolasi]',
															  array(
																	'' => 'Pilih',
																	'Positif'=>'Positif',
																	'Negatif'=>'Negatif',
															  ),
															  null,
															  array(
																	'class' => 'input-medium id_combobox'
															  ))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[virus_isolasi]',
															null,
															array(
																'class' => 'input-medium',
																'placeholder' => 'Jenis Virus',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_isolasi]',
															null,
															array(
																'class' => 'input-medium',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Hasil Lab',
															))
														}}
													</td>
												</tr>
											</table>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
				</fieldset>
				<div class="control-group">
					<label class="control-label">Klasifikasi Final</label>
					<div class="controls">
					{{Form::select(
						  'dk[klasifikasi_final]',
						  array(
								'' => 'Pilih',
								'CRS pasti(Lab Positif)'=>'CRS pasti(Lab Positif)',
								'CRS Klinis'=>'CRS Klinis',
								'Bukan CRS'=>'Bukan CRS',
								'Suspek CRS'=>'Suspek CRS',
						  ),
						  null,
						  array(
								'id'=>'klasifikasi_final',
								'disabled'=>'disabled',
								'onchange'=>'klasifikasifinal()',
								'class' => 'input-medium id_combobox'
						  ))
					}}
					{{ Form::textarea(
							'dk[klasifikasi_final_desc]',
							null,
							array(
								'id'=>'desc_klasifikasi_final',
								'readonly'=>'readonly',
								'rows' => '3',
								'placeholder' => 'Description'
							))
					  }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="form-actions">
	{{Form::submit('Simpan',array('class'=>'btn btn-primary'))}}
	{{Form::reset('Batal',array('class'=>'btn btn-warning'))}}
</div>
{{Form::close()}}
<input type="hidden" value="<?php echo Sentry::getUser()->hak_akses;?>" id='hakakses'>

<script type="text/javascript" charset="utf-8">
$(function(){
	var hakakses = $('#hakakses').val();
	if (hakakses == 2 || hakakses == 7) {
		$('.akseskab').attr('readonly','readonly')
	};

	$('#form_save_crs').validate({
		submit: {
			settings: {
				scrollToError: {
					offset: -100,
					duration: 500
				}
			}
		}
	});

	$('#id_kelurahan_pasien').change(function(){
			var nama_anak  = $('#nama_anak').val();
			var nik  = '';
			var nama_ortu  = $('#nama_ortu').val();
			var alamat = $(this).val();
			$.post('{{URL::to("cekPasien")}}', {nama_anak:nama_anak,nik:nik,nama_ortu:nama_ortu,id_kelurahan:alamat}, function(response){
				if(response){
					if(response.sent.status=='1'){
						alert(response.sent.message);
					};
				};
			});
		});

	$('#form_save_crs input').keydown(function(e){
		if(e.keyCode==13){
			if($(':input:eq(' + ($(':input').index(this) + 1) + ')').attr('type')=='submit'){// check for submit button and submit form on enter press
				return true;
			}
			$(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();
			return false;
		}
	});

	$('#hidup').click(function(){
		$('.penyebab_meninggal').attr('readonly','readonly');
	});
	$('#meninggal').click(function(){
		$('.penyebab_meninggal').removeAttr('readonly');
	});
});

function showabnornal() {
	var te = $("#other_abnornalities").val();
	if (te == 'Ya') {
		$('.other_ab').removeAttr('readonly');
	} else{
		$('.other_ab').attr('readonly','readonly');
	};
}
function klasifikasifinal() {
	var te = $("#klasifikasi_final").val();
	if (te == 'Discarded' || te=='Bukan CRS') {
		$('#desc_klasifikasi_final').removeAttr('readonly');
	} else{
		$('#desc_klasifikasi_final').attr('readonly','readonly');
	};
}
function actiondate(dt, val) {
	if (val == 'Ya') {
		$('.'+dt+'_date').removeAttr('readonly');
	} else{
		$('.'+dt+'_date').attr('readonly','readonly');
	};
}
</script>
