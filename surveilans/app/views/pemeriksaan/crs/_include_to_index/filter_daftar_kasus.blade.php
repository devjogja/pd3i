<div class="module">
<div class="module-body">
{{Form::open(['url'=>'crs','method'=>'post','id'=>'form-filter-daftar-kasus','class'=>'filter_daftar_kasus'])}}
		<div class="chart inline-legend grid">
			<table class="table" style="width:100%;">
				<tr>
					<td style="text-align:right;width:10%;">
						Rentang Waktu
					</td>
					<td style="width:25%;">
						<select name="unit" class="unit" style="width: 90px;" onkeypress="focusNext('day_start', 'clinic_id', this, event)" onchange="setDisable(this, event)">
							<option value="all">All</option>
							<option value="day">Hari</option>
							<option value="month">Bulan</option>
							<option value="year">Tahun</option>
						</select>
					</td>

					<td style="text-align:right;">
						Rumah Sakit
					</td>
					<?php
						$type = Session::get('type');
						$kd_faskes = Session::get('kd_faskes');
						$idinstansi = $namainstansi = '';
						if ($type=='rs') {
							$data = DB::table('rumahsakit2')
											->select('nama_faskes','alamat','kode_faskes')
											->where('kode_faskes',$kd_faskes)
											->get();
							$namainstansi = $data[0]->nama_faskes;
							$idinstansi = $data[0]->kode_faskes;
						}
					?>
					<td>
						<select name="puskesmas_id" id="rumahsakit" style="width: 190px;" >
							<option value="{{$idinstansi}}">{{$namainstansi}}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td style="text-align:right;">
						Sejak
					</td>
					<td>
						<?php
							$start['mktime'] = strtotime("-1 day");
							$start['day']    = date("j", $start['mktime']);
							$start['month']  = date("n", $start['mktime']);
							$start['year']   = date("Y", $start['mktime']);

							$now['day']        = date("j");
							$now['month']      = date("n");
							$now['year']       = date("Y");
							$now['year_start'] = 1971;

							$arr_nama_bulan[1]  = "Januari";
							$arr_nama_bulan[2]  = "Februari";
							$arr_nama_bulan[3]  = "Maret";
							$arr_nama_bulan[4]  = "April";
							$arr_nama_bulan[5]  = "Mei";
							$arr_nama_bulan[6]  = "Juni";
							$arr_nama_bulan[7]  = "Juli";
							$arr_nama_bulan[8]  = "Agustus";
							$arr_nama_bulan[9]  = "September";
							$arr_nama_bulan[10] = "Oktober";
							$arr_nama_bulan[11] = "November";
							$arr_nama_bulan[12] = "Desember";
						?>

					{{-- Tanggal --}}
						<select name="day_start" class="day_start" style="width: 50px;" onkeypress="focusNext( 'month_start', 'unit', this, event)">
							@for($i=1; $i<32; $i++) :
								@if($i == $start['day'])
									<?php $sel = "selected"; ?>
								@else
									<?php $sel = "" ?>
								@endif

								<option value="{{ $i }}" {{ $sel }} >
									{{ $i }}
								</option>
							@endfor
						</select>

					{{-- Bulan --}}
						<select name="month_start" class="month_start" style="width: 100px;" onkeypress="focusNext( 'year_start', 'day_start', this, event)">
							@for($i=1; $i<13; $i++) :

								//$bln = tambahNol($i, 2);
								@if($i == $start['month'])
									<?php $sel = "selected"; ?>
								@else
									<?php $sel = ""; ?>
								@endif

								<option value="{{ $i }}" {{ $sel }} >
									{{ $arr_nama_bulan[$i] }}
								</option>
							@endfor
						</select>

					{{-- Tahun --}}
						<select name="year_start" class="year_start inputan" style="width: 70px;" onkeypress="focusNext( 'day_end', 'month_start', this, event)">
							@for($i=$now['year_start']; $i<=$now['year']; $i++)
								@if($i==$start['year'])
									<?php $sel = "selected" ?>
								@else
									<?php $sel = ""; ?>
								@endif

								<option value="{{ $i }}" {{ $sel }}>
									{{ $i }}
								</option>
							@endfor;
						</select>
					</td>

			{{-- Provinsi --}}
					<td style="text-align:right;width:15%;">
						Provinsi
					</td>
					<td>
						<select name="province_id" id="province_id" style="width:200px" onchange="get_district();" >
							<option value="">--- Pilih ---</option>
							<?php
							$q = "
							SELECT
							id_provinsi,
							provinsi
							FROM provinsi
							ORDER BY provinsi ASC
							";
							$combo_district=DB::select($q);
							?>
							@for($i=0;$i<count($combo_district);$i++)
							<option value="{{ $combo_district[$i]->id_provinsi }}">
								{{ $combo_district[$i]->provinsi; }}
							</option>
							@endfor
						</select>
					</td>
				</tr>

		{{-- Row 3 --}}
				<tr>

			{{-- Tanggal Akhir --}}
					<td style="text-align:right;">
						Sampai
					</td>

					<td>
				{{-- Tanggal --}}
						<select name="day_end" class="day_end" style="width: 50px;" >
							@for($i=1; $i<32; $i++)
								@if($i == $now['day'])
									<?php $sel = "selected"; ?>
								@else
									<?php $sel = "" ?>
								@endif

								<option value="{{ $i }}" {{ $sel }} >
									{{ $i }}
								</option>
							@endfor
						</select>

				{{-- Bulan --}}
						<select name="month_end" class="month_end" style="width: 100px;" >
							@for($i=1;$i<13;$i++)
								@if($i==$now['month'])
									<?php $sel = "selected" ?>
								@else
									<?php $sel = ""; ?>
								@endif

								<option value="{{ $i }}" {{ $sel }} >
									{{ $arr_nama_bulan[$i] }}
								</option>
							@endfor
						</select>

				{{-- Tahun --}}
						<select name="year_end" class="year_end" style="width: 70px;" class="inputan">
							@for($i=$now['year_start']; $i<=$now['year']; $i++)
								@if($i==$now['year'])
									<?php $sel = "selected"; ?>
								@else
									<?php $sel = ""; ?>
								@endif

								<option value="{{ $i }}" {{ $sel }} >
									{{ $i }}
								</option>
							@endfor
						</select>
					</td>

				{{-- Kabupaten --}}
					<td style="text-align:right;">
						Kabupaten
					</td>
					<td>
						<select name="district_id" id="district_id" style="width: 190px;" disabled="disabled"  onchange="get_sub_district();">
							<option value="">--- Pilih ---</option>
						</select>
					</td>
				{{-- Row 4 --}}
				<tr>
					<td style="text-align:right;"></td>
					<td></td>
					<td style="text-align:right;"></td>
					<td></td>
				</tr>
			</table>
			<center>
				<input id="filter_daftar_kasus" type="submit" value="Tampilkan" class="btn btn-success">
				<a href="#" id='export_dk' class="btn btn-success">Export Excel</a>
			</center>
		</div>
	</form>
</div>
</div>

<script type="text/javascript">
	$('#export_dk').click(function(e){ 
	    e.preventDefault(); 
	    var params = JSON.stringify($('.filter_daftar_kasus').serializeArray());
	    var url = "{{URL::to('export/'.strtolower(Session::get('penyakit')).'/xls')}}"+'/'+params;
	    window.open(url, '_blank');
	});
</script>