@extends('layouts.master')
@section('content')
<div class="span12">
	<div class="content">
		<div class="module">
			<div class="module-body">
				<div class="profile-head media">
					<h4>Penyakit CRS (Congenital Rubella Syndrom)</h4>
					<hr>
				</div>
				{{-- Tab Navigasi --}}
				@include('pemeriksaan.crs._include_to_index.tab_navigasi')
				{{-- Content Dari Tab --}}
				<div class="profile-tab-content tab-content">
					{{-- Daftar Kasus --}}
					<div class="tab-pane fade active in" id="daftar">
						@if(Session::get('type')=='rs')
						@include('pemeriksaan._include.filter_daftar_kasus_rs')
						@elseif(Session::get('type')=='puskesmas')
						@include('pemeriksaan._include.filter_daftar_kasus_puskesmas')
						@else
						@include('pemeriksaan._include.filter_daftar_kasus')
						@endif
						@include('pemeriksaan.crs._include_to_index.daftar_kasus')
					</div>
					<div id="results"></div>
					{{-- Input Data Individual Kasus --}}
					<div class="tab-pane fade" id="input">
						@include('pemeriksaan.crs._include_to_index.input_data_individual_kasus')
					</div>
					{{-- Analisis Kasus CRS --}}
					@include('pemeriksaan.campak.analisis')
					@include('pemeriksaan.campak.analisis-js')
					{{-- PE CRS --}}
					<div class="tab-pane fade in" id="PE">
						<div id="data_crs"></div>
					</div>
					<div class="tab-pane fade in" id="importcrs">
						@include('pemeriksaan.crs.import')
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop