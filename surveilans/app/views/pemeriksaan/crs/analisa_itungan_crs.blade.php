<script>
$(document).ready(function(){
	$('#populasi_1,#populasi_2,#populasi_3,#populasi_4,#populasi_5').tooltip();
});	
</script>
<div class="module">
	<div class="module-head">
		<h3>CRS (Congenital Rubella Syndrom)</h3>
	</div>
	<div class="module-body">
		<div class="chart inline-legend grid" id="" style="width:100%;">
			<table class="table" style="width:100%;">
				<tr>
					<td>
						<span id="populasi_4" title="(jumlah kasus crs meninggal / jumlah kasus crs ) x 100%">Case Fatality Rate (CFR) : </span>
					</td>
					<td>
						<?php 
							$q = "
								SELECT
								IFNULL(COUNT(*),0) AS jml 
								FROM getdetailcrs
								WHERE
								DATE_FORMAT(crs_tanggal_periksa,'%Y') = ".date('Y')."
								GROUP BY DATE_FORMAT(crs_tanggal_periksa,'%m %y')
							";
							$data=DB::select($q);
							$jml_kasus_crs = 0;
							if(count($data) > 0)$jml_kasus_crs = $data[0]->jml;
							
							$q = "
							SELECT
							IFNULL(COUNT(*),0) AS jml 
							FROM
							getdetailcrs
							WHERE 
							DATE_FORMAT(crs_tanggal_periksa,'%Y') = ".date('Y')."
							AND crs_keadaan_akhir='Meninggal'
							GROUP BY DATE_FORMAT(crs_tanggal_periksa,'%m %y')
							";
							$data=DB::select($q);
							$jml_kasus_crs_meninggal = 0;
							if(count($data) > 0){
								$jml_kasus_crs_meninggal = $data[0]->jml;
								$cfr = (($jml_kasus_crs_meninggal / $jml_kasus_crs) * 100);
							} else {
									$cfr = 0;
							}
							echo (int) $cfr;        
						?> %
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
