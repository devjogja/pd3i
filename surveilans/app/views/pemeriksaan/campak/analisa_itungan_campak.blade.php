<script>
$(document).ready(function(){
	$('#populasi_1,#populasi_2,#populasi_3,#populasi_4,#populasi_5').tooltip();
});	
</script>
<div class="module">
	<div class="module-head">
		<h3>Campak</h3>
	</div>
	<div class="module-body">
		<div class="chart inline-legend grid" id="" style="width:80%;">
			<table class="table" style="width:100%;">
				<!--
				<tr>
					<td>
						<span id="populasi_1" title="(jumlah populasi bayi di desa x pada tahun (y) - jumlah bayi yang diimunisasi didesa x pada tahun (y) ) + (15 % x jumlah bayi yang diimunisasi didesa x pada tahun (y) )">Populasi rentan / Susceptible : </span><br>
					</td>
					<td style="width:200px;">
						<?php 
							$q = "
								SELECT 
									IFNULL(COUNT(*),0) AS jml 
								FROM hasil_uji_lab_campak a 
								JOIN pasien b ON b.id_pasien=a.id_pasien
								JOIN campak c ON c.id_campak=a.id_campak
								WHERE 
								DATE_FORMAT(c.tanggal_periksa,'%Y') = '2014'
								GROUP BY DATE_FORMAT(c.tanggal_periksa,'%m %y')
							";
							$data=DB::select($q);
							$jml_bayi = 0;
							if(count($data) > 0)$jml_bayi = $data[0]->jml;
							
							$q = "
								SELECT 
									IFNULL(COUNT(*),0) AS jml 
								FROM hasil_uji_lab_campak a 
								JOIN pasien b ON b.id_pasien=a.id_pasien
								JOIN campak c ON c.id_campak=a.id_campak
								WHERE 
								DATE_FORMAT(c.tanggal_periksa,'%Y') = '2014'
								AND c.vaksin_campak_sebelum_sakit IN ('1x','2x','3x','4x','5x','6x')
								GROUP BY DATE_FORMAT(c.tanggal_periksa,'%m %y')
							";
							$data=DB::select($q);
							$jml_bayi_diimunisasi = 0;
							if(count($data) > 0)$jml_bayi_diimunisasi = $data[0]->jml;
							
							$par = ($jml_bayi-$jml_bayi_diimunisasi) + ((15 / 100) * $jml_bayi_diimunisasi);
							echo (int) $par;
						?>
						
					</td>
				</tr>
				<tr>
					<td>
						<span id="populasi_2" title="(jumlah kasus campak / jumlah populasi at risk) x 100%">Attack Rate  atau Insiden Rate (AR) : </span>
					</td>
					<td>
						<?php 
							$q = "
								SELECT 
									IFNULL(COUNT(*),0) AS jml 
								FROM hasil_uji_lab_campak a 
								JOIN pasien b ON b.id_pasien=a.id_pasien
								JOIN campak c ON c.id_campak=a.id_campak
								WHERE 
								DATE_FORMAT(c.tanggal_periksa,'%Y') = '2014'
								GROUP BY DATE_FORMAT(c.tanggal_periksa,'%m %y')
							";
							$data=DB::select($q);
							$jml_kasus_campak = 0;
							if(count($data) > 0){$jml_kasus_campak = $data[0]->jml;
							
							$ar = (($jml_kasus_campak / $par) * 100);
							} else $ar = 0;
							echo (int) $ar;     
						?> %                                            
						
					</td>
				</tr>
				<tr>
					<td>
						<span id="populasi_3" title="(jumlah kasus campak pada kelompok umur X / jumlah populasi at risk pada kelompok umur X tersebut) x 100%">Age Spesific Attack Rate (AR) :</span> 
					</td>
					<td>
						<?php
						
						$arr_kelompok_umur = array(
							"",
							"Di bawah 1 tahun",
							"1-4 tahun",
							"4-9 tahun",
							"9-14tahun",
							"Di atas 15 tahun",
						);
						for($k=1;$k<count($arr_kelompok_umur);$k++){
							$q = "  
								SELECT 
								COUNT(*) AS jml
								FROM hasil_uji_lab_campak a
								JOIN pasien b ON b.id_pasien=a.id_pasien
								JOIN campak c ON c.id_campak=a.id_campak
								WHERE 
								DATE_FORMAT(c.tanggal_periksa,'%Y')='2014'
								AND getKelompokUmurCampak(b.tanggal_lahir,c.tanggal_periksa)='".$k."'
								GROUP BY getKelompokUmurCampak(b.tanggal_lahir,c.tanggal_periksa)
							";
							$data=DB::select($q);
							if(count($data)>0) $jml_populasi = $data[0]->jml;
							else $jml_populasi = 0;
							
							$q = "  
								SELECT 
								COUNT(*) AS jml
								FROM hasil_uji_lab_campak a
								JOIN pasien b ON b.id_pasien=a.id_pasien
								JOIN campak c ON c.id_campak=a.id_campak
								WHERE 
								DATE_FORMAT(c.tanggal_periksa,'%Y')='2014'
								AND c.vaksin_campak_sebelum_sakit IN ('1x','2x','3x','4x','5x','6x')
								AND getKelompokUmurCampak(b.tanggal_lahir,c.tanggal_periksa)='".$k."'
								GROUP BY getKelompokUmurCampak(b.tanggal_lahir,c.tanggal_periksa)
							";
							$data=DB::select($q);
							if(count($data)>0)$jml_populasi_imunisasi = $data[0]->jml;
							else $jml_populasi_imunisasi= 0;
							$par = ($jml_populasi-$jml_populasi_imunisasi) + ((15 / 100) * $jml_populasi_imunisasi);
							echo $arr_kelompok_umur[$k]." : ".$par."<br>";
						}
						
						?>
					</td>
				</tr>
				-->
				<tr>
					<td>
						<span id="populasi_4" title="(jumlah kasus campak meninggal / jumlah kasus campak ) x 100%">Case Fatality Rate (CFR) : </span>
					</td>
					<td>
						<?php 
							$q = "
								SELECT 
									IFNULL(COUNT(*),0) AS jml 
								FROM hasil_uji_lab_campak a 
								JOIN pasien b ON b.id_pasien=a.id_pasien
								JOIN campak c ON c.id_campak=a.id_campak
								WHERE 
								DATE_FORMAT(c.tanggal_periksa,'%Y') = '2014'
								GROUP BY DATE_FORMAT(c.tanggal_periksa,'%m %y')
							";
							$data=DB::select($q);
							$jml_kasus_campak = 0;
							if(count($data) > 0)$jml_kasus_campak = $data[0]->jml;
							
							$q = "
								SELECT 
									IFNULL(COUNT(*),0) AS jml 
								FROM hasil_uji_lab_campak a 
								JOIN pasien b ON b.id_pasien=a.id_pasien
								JOIN campak c ON c.id_campak=a.id_campak
								WHERE 
								DATE_FORMAT(c.tanggal_periksa,'%Y') = '2014'
								AND c.keadaan_akhir='M'
								GROUP BY DATE_FORMAT(c.tanggal_periksa,'%m %y')
							";
							$data=DB::select($q);
							$jml_kasus_campak_meninggal = 0;
							if(count($data) > 0){
								$jml_kasus_campak_meninggal = $data[0]->jml;
								$cfr = (($jml_kasus_campak_meninggal / $jml_kasus_campak) * 100);
							} else {
									$cfr = 0;
							}
							echo (int) $cfr;        
						?> %
					</td>
				</tr>
				<!--
				<tr>
					<td>
						<span id="populasi_5" title="(AR tak imunisasi - AR imunisasi) / (AR tak imunisasi) x 100%">Efikasi vaksin  : </span>
					</td>
					<td>
						
					</td>
				</tr>
				-->
			</table>
		</div>
	</div>
</div>
<!--/.module-->