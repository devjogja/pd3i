<div class="module-body uk-overflow-container">
	<div class="table-responsive">
		<table class="display table table-bordered" id="example">
			<thead>
				<tr>
					<td>No.</td>
					<th>No. Epid</th>
					<th>Nama pasien</th>
					<th>Usia</th>
					<th>Alamat</th>
					<th>Keadaan Akhir</th>
					<th>Klasifikasi akhir</th>
					@if(Sentry::getUser()->hak_akses==2 || Sentry::getUser()->hak_akses==7)
					<th>PE</th>
					@else
					<th>Nama Faskes</th>
					@endif
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				@if($data)
				<?php $n=0;foreach($data as $row):$n++;?>
				<tr>
					<td>{{ $n }}</td>
					@if($row->no_epid_klb)
					<td>{{ $row->no_epid_klb }}</td>
					@else
					<td>{{ $row->no_epid }}</td>
					@endif
					<td>{{ $row->nama_anak }}</td>
					@if($row->umur || $row->umur_bln || $row->umur_hr)
					<td>{{ $row->umur.' Th '.$row->umur_bln.' bln '.$row->umur_hr.' hr' }}</td>
					@else
					<td>-</td>
					@endif
					<td>{{ $row->alamat }}</td>
					<td>{{ $row->keadaan_akhir_nm }}</td>
					<td>{{ $row->klasifikasi_final_nm }}</td>
					@if(Sentry::getUser()->hak_akses==2 || Sentry::getUser()->hak_akses==7)
					<td>
						@if(Campak::checkPE($row->id_campak))
							<a class="btn btn-danger">Sudah PE</a>
						@else
							<a href="{{URL::to('campak/entricampak/'.$row->id_campak)}}" class="btn btn-md btn-success">PE</a>
						@endif
					</td>
					@else
					<td>{{$row->nama_faskes}}</td>
					@endif
					@if(Sentry::getUser()->hak_akses==2 || Sentry::getUser()->hak_akses==7)
					<td>
						<div class="btn-group" role="group" >
							<a href="{{URL::to('campak_detail/'.$row->id_campak)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
							<a href="{{URL::to('campak_edit/'.$row->id_campak)}}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>
							<a href="{{URL::to('campak_hapus/'.$row->id_campak)}}" onclick="return confirm('apakah yakin ingin menghapus data ini?');return false;" class="btn btn-xs btn-danger" data-uk-tooltip title="hapus data"><i class="icon icon-remove"></i></a>
							<!-- <a href="" class="btn btn-xs btn-primary" data-uk-tooltip title="print label"><i class="icon icon-print"></i></a> -->
						</div>
					</td>
					@elseif(Sentry::getUser()->hak_akses==4)
					<td>
						<div class="btn-group" role="group" >
							<a href="{{URL::to('campak_detail/'.$row->id_campak)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
							<a href="{{URL::to('campak_edit/'.$row->id_campak)}}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>
							<!-- <a href="" class="btn btn-xs btn-primary" data-uk-tooltip title="print label"><i class="icon icon-print"></i></a> -->
						</div>
					</td>
					@elseif(Sentry::getUser()->hak_akses==1 || Sentry::getUser()->hak_akses==6)
					<td>
						<div class="btn-group" role="group" >
							<a href="{{URL::to('campak_detail/'.$row->id_campak)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
							<a href="{{URL::to('campak_hapus/'.$row->id_campak)}}" onclick="return confirm('apakah yakin ingin menghapus data ini?');return false;" class="btn btn-xs btn-danger" data-uk-tooltip title="hapus data"><i class="icon icon-remove"></i></a>
							<!-- <a href="" class="btn btn-xs btn-primary" data-uk-tooltip title="print label"><i class="icon icon-print"></i></a> -->
						</div>
					</td>
					@elseif(Sentry::getUser()->hak_akses==5)
					<td></td>
					@endif
				</tr>
				<?php endforeach;?>
				@endif
			</tbody>
		</table>
	</div>
</div>
