<ul class="profile-tab nav nav-tabs">
  @if(Sentry::getUser()->hak_akses==1 || Sentry::getUser()->hak_akses==4 || Sentry::getUser()->hak_akses==6)
      <li class="active"><a href="#activity" data-toggle="tab">Daftar kasus campak</a></li>
      <li><a href="#analisis" data-toggle="tab">Analisis kasus</a></li>
      <li><a href="#PE" data-toggle="tab">Daftar kasus Penyelidikan epidemologi campak</a></li>
  @elseif(Sentry::getUser()->hak_akses==2 || Sentry::getUser()->hak_akses==7 || Sentry::getUser()->hak_akses==3)
      <li class="active"><a href="#activity" data-toggle="tab">Daftar kasus campak</a></li>
      <li><a href="#friends" data-toggle="tab">Input data individual kasus</a></li>
      <li><a href="#analisis" data-toggle="tab">Analisis kasus</a></li>
      <li><a href="#PE" data-toggle="tab">Daftar kasus Penyelidikan epidemologi campak</a></li>
  @elseif(Sentry::getUser()->hak_akses==5)
      <li class="active"><a href="#friends" data-toggle="tab">Input data individual kasus</a></li>
  @endif
      <li><a href="#importcampak" data-toggle="tab">Import</a></li>
</ul>