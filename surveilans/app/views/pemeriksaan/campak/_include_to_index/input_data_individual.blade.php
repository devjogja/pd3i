<div class="alert alert-error" style="margin: 0px 16px;">
	<p style="font-size:12px">Text input yang bertanda bintang <strong>(*)</strong> wajib di isi</p>
</div>
{{ Form::open(array('url' =>'campak_store','class' =>'form-horizontal','id'=>'form_save_campak')) }}
<div class="module-body">
	<div class="row-fluid">
		<div class="span6">
			<div class="media">
				<fieldset>
					<legend>Identitas penderita</legend>
					<div class="control-group">
						<label class="control-label">Nama penderita</label>
						<div class="controls">
							{{ Form::text('nama_anak',Input::old('nama_anak'),array('data-validation'=>'[MIXED]','data' =>'$ wajib di isi!','data-validation-message' =>'nama penderita wajib di isi!','class' => 'input-xlarge','id'	=> 'nama_anak','placeholder' =>'Nama penderita')) }}
							<span class="help-inline" style="color:red">(*)</span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">NIK</label>
						<div class="controls">
							{{Form::text('nik',Input::old('nik'),array('class' => 'input-medium','id'=>'nik','placeholder'=>'Nomer induk kependudukan'))}}
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Nama orang tua</label>
						<div class="controls">
							{{Form::text('nama_ortu',Input::old('nama_ortu'),array('data-validation'=>'[MIXED]','data'=>'$ wajib di isi!','data-validation-message'=>'nama orang tua wajib di isi!','class' => 'input-xlarge','id'=>'nama_ortu','placeholder'=>'Nama orang tua'))}}
							<span class="help-inline" style="color:red">(*)</span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Jenis kelamin</label>
						<div class="controls">
							{{Form::select('jenis_kelamin',array(null => 'Pilih','1'=>'Laki-laki','2'=>'Perempuan','3' =>'Tidak jelas'),Input::old('jenis_kelamin'),array
							('data-validation'=>'[MIXED]','data'=>'$ wajib di isi!','data-validation-message'=>'jenis kelamin wajib di isi!','class' => 'input-medium id_combobox',
							'id'=>'jenis_kelamin'))}}
							<span class="help-inline" style="color:red">(*)</span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label"><input type="radio" name="option" onclick="pilihOptionTglLahir()">Tanggal lahir</label>
						<div class="controls">
							{{Form::text('tanggal_lahir',Input::old('tanggal_lahir'),array('class' => 'input-medium tgl_lahir','placeholder'=>'Tanggal lahir','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','onchange'=>'usia()','readonly'=>'readonly'))}}
							<span class="help-inline" style="color:red"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label"><input type="radio" name="option" onclick="pilihOptionUmur()">Umur</label>
						<div class="controls">
							<input type="text" class="input-mini"  name="tgl_tahun" id="tgl_tahun" readonly="readonly">Thn
							<input type="text" class="input-mini" name="tgl_bulan" id="tgl_bulan" readonly="readonly">Bln
							<input type="text" class="input-mini" name="tgl_hari" id="tgl_hari" readonly="readonly">Hr
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Provinsi</label>
						<div class="controls">
							{{ Form::select('id_provinsi',array(null=>'Pilih provinsi')+Provinsi::lists('provinsi','id_provinsi'),Input::old('id_provinsi'),array('id'=>'id_provinsi_pasien','placeholder' => 'Pilih Provinsi','onchange'=>"getKabupaten('_pasien')",'class'=>'input-large id_combobox'))}}
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Kabupaten</label>
						<div class="controls">
							{{Form::select('id_kabupaten',array(null=>'Pilih Kabupaten'),Input::old('id_kabupaten'),array('placeholder'=>'Pilih','class' => 'input-large id_combobox',
							'id'=>'id_kabupaten_pasien','onchange'=>"getKecamatan('_pasien')"))}}
							<span class="lodingKab"></span>
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Kecamatan</label>
						<div class="controls">
							{{Form::select('id_kecamatan',array(null=>'Pilih Kecamatan'),Input::old('id_kecamatan'),array('placeholder'=>'Pilih','class' => 'input-large id_combobox',
							'id'=>'id_kecamatan_pasien','onchange'=>"getKelurahan('_pasien')"))}}
							<span class="lodingKec"></span>
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Kelurahan/Desa</label>
						<div class="controls">
							{{Form::select('id_kelurahan',array(null=>'Pilih Kelurahan'),Input::old('id_kelurahan'),array('data-validation'=>'[MIXED]','data'=>'$ wajib di isi!',
							'data-validation-message'=>'Kelurahan harus di isi','placeholder'=>'Pilih','class' => 'input-large id_combobox','id'=>'id_kelurahan_pasien',
							'onchange'=>"showEpidCampak('_pasien'),showEpidKLB('_pasien')"))}}
							<span class="lodingKel"></span>
							<span class="help-inline" style="color:red">(*)</span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Alamat</label>
						<div class="controls">
							{{Form::textarea('alamat',Input::old('alamat'),array('rows'=>'3','id'=>'alamat','style'=>'width:270px;','placeholder' => 'Hanya diisi nama jalan, no. rumah dan no. RT - RW'))}}
							<span class="help-inline" style="color:red"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Nama faskes saat periksa</label>
						<div class="controls">
							<?php
							$type = Session::get('type');
							$kd_faskes = Session::get('kd_faskes');
							$namainstansi = '';
							if($type == "puskesmas")
							{
								$nama_instansi = 'PUSKESMAS '.DB::table('puskesmas')
								->select('puskesmas_name','puskesmas_code_faskes')
								->WHERE('puskesmas_code_faskes',$kd_faskes)->pluck('puskesmas_name');
								echo Form::text('nama_puskesmas',$nama_instansi, array('class' => 'input-large','readonly'=>'readonly','placeholder'=>'Masukan nama puskesmas'));
							}
							elseif ($type == "rs")
							{
								$nama_instansi = DB::table('rumahsakit2')
								->select('nama_faskes')
								->WHERE('kode_faskes',$kd_faskes)->pluck('nama_faskes');
								echo Form::text('nama_puskesmas',$nama_instansi, array('class' => 'input-large','readonly'=>'readonly','placeholder'=>'Masukan nama rumahsakit'));
							}
							?>
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">No Epidemologi</label>
						<div class="controls">
							{{Form::text('no_epid',Input::old('no_epid'),array('class' => 'input-medium','placeholder'=>'Nomer epidemologi','id'=>'epid'))}}
							<span class="help-inline" style="color:red"></span>
						</div>
					</div>
					<div class="control-group noKLB">
						<label class="control-label">No Epidemologi KLB</label>
						<div class="controls">
							{{Form::text('no_epid_klb',Input::old('no_epid_klb'),array('class' => 'input-medium','placeholder'=>'Nomer epidemologi KLB','id'=>'epidklb'))}}
							<span class="help-inline" style="color:red"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">No Epidemologi lama</label>
						<div class="controls">
							{{Form::text('no_epid_lama',Input::old('no_epid_lama'),array('class' => 'input-medium','placeholder'=>'Nomer epidemologi','id'=>'epid'))}}
							<span class="help-inline"></span>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
		<div class="span6">
			<div class="media">
				<fieldset>
					<legend>Data klinis campak</legend>
					<div class="control-group">
						<label class="control-label">Tanggal imunisasi campak terakhir</label>
						<div class="controls">
							{{Form::text('tanggal_imunisasi_terakhir',Input::old('tanggal_imunisasi_terakhir'),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
							<span class="help-inline" style="color:red"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Imunisasi campak sebelum sakit berapa kali?</label>
						<div class="controls">
							{{Form::select('vaksin_campak_sebelum_sakit',array(null => 'Pilih','1' => '1X','2'=>'2X','3'=>'3X','4'=>'4X','5'=>'5X','6'=>'6X','7'=>'Tidak','8'=>'Tidak tahu','9'=>'Belum Pernah'),Input::old('vaksin_campak_sebelum_sakit'),array('data-validation'=>'[MIXED]','data'=>'$ wajib di isi!','data-validation-message'=>'imunisasi campak sebelum sakit wajib di isi!','class' => 'input-small id_combobox'))}}
							<span class="help-inline" style="color:red">(*)</span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Tanggal mulai demam</label>
						<div class="controls">
							{{Form::text('tanggal_timbul_demam',Input::old('tanggal_timbul_demam'),array('data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','id'=>'tgl_mulai_sakit',
							'class'=>'input-medium','onchange'=> "showEpidCampak('_pasien'),usia()"))}}
							<span class="help-inline" style="color:red"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Tanggal mulai rash</label>
						<div class="controls">
							{{Form::text('tanggal_timbul_rash',Input::old('tanggal_timbul_rash'),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
							'id'=>'tgl_sakit','onchange'=>"showEpidCampak('_pasien'),usia()"))}}
							<span class="help-inline" style="color:red">(*)</span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Gejala / tanda lainnya</label>
						<div class="controls">
							<table>
								<?php $daftargejala = Campak::DaftarGejala();?>
								@foreach($daftargejala as $row)
								<tr>
									<td>
										<input type="checkbox" value="{{$row->id}}" name="gejala_lain[]" class="gejala_lain_{{$row->id}}" onclick="check({{$row->id}})">
										{{$row->nama}}
									</td>
									<td>
										<input type="text" class="input-medium tgl_gejala_lain_{{$row->id}}" disabled="disabled" name="tanggal_gejala_lain[]" data-uk-datepicker='{format:"DD-MM-YYYY"}'>
									</td>
								</tr>
								@endforeach
							</table>
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Komplikasi</label>
						<div class="controls">
							<table>
								<tr><td><input type="checkbox" name="komplikasi[]" value="0">&nbsp;Diare</td></tr>
								<tr><td><input type="checkbox" name="komplikasi[]" value="1">&nbsp;Pneumonia</td></tr>
								<tr><td><input type="checkbox" name="komplikasi[]" value="2">&nbsp;Bronchopneumonia</td></tr>
								<tr><td><input type="checkbox" name="komplikasi[]" value="3">&nbsp;OMA</td></tr>
								<tr><td><input type="checkbox" name="komplikasi[]" value="4">&nbsp;Ensefalitis</td></tr>
								<tr>
									<td><input type="checkbox" class="gejala_lain_komplikasi" onclick="chek_komplikasi_lain()">&nbsp;Lain-lain (sebutkan)</td>
									<td><input type="text" name="komplikasi[]" class="input-medium komplikasi_lainnya" disabled='disabled'></td>
								</tr>
							</table>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Tanggal laporan diterima</label>
						<div class="controls">
							{{Form::text('tanggal_laporan_diterima',Input::old('tanggal_laporan_diterima'),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Tanggal pelacakan</label>
						<div class="controls">
							{{Form::text('tanggal_pelacakan',Input::old('tanggal_pelacakan'),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Diberi vitamin A</label>
						<div class="controls">
							{{Form::select('vitamin_A',array(null => 'Pilih','1' => 'Ya','2'=>'Tidak'),Input::old('vitamin_A'),array('class' => 'input-small id_combobox'))}}
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Keadaan akhir</label>
						<div class="controls">
							{{Form::select('keadaan_akhir',array(null => 'Pilih','1' => 'Hidup','2'=>'Meninggal'),Input::old('keadaan_akhir'),array('class' => 'input-medium id_combobox'))}}
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Jenis kasus</label>
						<div class="controls">
							{{Form::select('jenis_kasus',array(null => 'Pilih','1' => 'KLB','2'=>'Bukan KLB'),Input::old('jenis_kasus'),array('data-validation'=>'[MIXED]',
							'data'=>'$ wajib di isi!','data-validation-message'=>'Jenis Kasus harus di isi','placeholder'=>'Pilih','class' => 'input-medium id_combobox','id'=>'jenis_kasus','onchange'=>'jenisKasus()'))}}
							<span class="help-inline" style="color:red">(*)</span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">KLB Ke</label>
						<div class="controls">
							{{Form::select('klb_ke',array(null => 'Pilih','KI' => 'KI','KII' => 'KII','KIII' => 'KIII','KIV' => 'KIV','KV' => 'KV','KVI' => 'KVI','KVII' => 'KVII','KVIII' => 'KVIII','KIX' => 'KIX','KX' => 'KX','KXI' => 'KXI','KXII' => 'KXII'),Input::old('klb_ke'),array('class' => 'input-medium id_combobox','id'=>'klb_ke','disabled'=>'disabled','data-validation'=>'[MIXED]','data'=>'$ wajib di isi!','data-validation-message'=>'KLB ke- harus di isi','onchange'=>'showEpidKLB("_pasien")'))}}
							<span class="help-inline" style="color:red">(*)</span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Status kasus</label>
						<div class="controls">
							{{Form::select('status_kasus',array(null => 'Pilih','1' => 'Index','2'=>'Bukan index'),Input::old('status_kasus'),array('class' => 'input-medium id_combobox'))}}
							<span class="help-inline"></span>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="media">
				<fieldset>
					<legend>Data spesimen dan hasil laboratorium</legend>
					<table class="table table-bordered" id="data_sampel">
						<tr>
							<td>Nama pemeriksaan</td>
							<td>Jenis sampel</td>
							<td>Tanggal ambil sampel</td>
							<td>aksi</td>
						</tr>
					</table>
					</br>
					<a class="btn btn-success" onclick="tambah_sampel()" id="tambah_sampel">tambah sampel</a>
					</br><hr>
					<div id="div_sampel">
						<table class="table table-bordered" id="data_sampel">
							<tr>
								<td>Nama pemeriksaan</td>
								<td>Jenis sampel</td>
								<td>Tanggal ambil sampel</td>
							</tr>
							<tr>
								<td>
									<select id="nama_sampel" class='input-medium' onchange="pilih_sampel()">
										<option value="">Pilih</option>
										<option value="0">Serologi</option>
										<option value="1">Virologi</option>
									</select>
								</td>
								<td>
									<select id="jenis_sampel" class='input-medium' onchange="jenissampel()">
										<option value="">Pilih</option>
									</select>
								</td>
								<td>
									{{Form::text('',null,array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','id'=>'tanggal_ambil_sampel'))}}
								</td>
							</tr>
						</table>
						<button class="btn btn-primary" style="margin-left:330px;margin-top:30px" onclick="add_sampel()" id="simpan">tambah</button>
						<button class="btn btn-warning" style="margin-left:10px;margin-top:30px" onclick="tutup_sampel()">tutup</button>
					</div>
					<br>
					@if(Sentry::getUser()->hak_akses==4)
					<div class="control-group">
						<label class="control-label">Hasil Laboratorium</label>
						<div class="controls">
							<table>
								<tr>Hasil Laboratorium Serologi</tr>
								<tr>
									<td>IgM Campak</td>
									<td>
										{{Form::select('hasil_serologi_igm_campak',array(null => 'Pilih','1' => 'Positif','2'=>'Negatif','3'=>'Equivocal','4'=>'pending'),null,array(
										'class' => 'id_combobox'))}}
									</td>
								</tr>
								<tr>
									<td>IgM Rubella</td>
									<td>
										{{Form::select('hasil_serologi_igm_rubella',array(null => 'Pilih','1' => 'Positif','2'=>'Negatif','3'=>'Equivocal','4'=>'pending'),null,array('class' => 'id_combobox'))}}
									</td>
								</tr>
							</table>
							<br>
							<table>
								<tr>Hasil Laboratorium Virologi</tr>
								<tr>
									<td>IgM Campak</td>
									<td>
										{{Form::select('hasil_virologi_igm_campak',array(null => 'Pilih','1' => 'Positif','2'=>'Negatif','3'=>'pending'),null,array('class' => 'id_combobox'))}}
									</td>
								</tr>
								<tr>
									<td>IgM Rubella</td>
									<td>
										{{Form::select('hasil_virologi_igm_rubella',array(null => 'Pilih','1' => 'Positif','2'=>'Negatif','3'=>'pending'),null,array('class' => 'id_combobox'))}}
									</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Klasifikasi final</label>
						<div class="controls">
							{{Form::select('klasifikasi_final',array(null => 'Pilih','1' => 'Campak (Lab)','2'=>'Campak (Epid)','3'=>'Campak (Klinis)','4'=>'Rubella','5'=>'Bukan campak/rubella','6'=>'Pending'),null,array('class' => 'id_combobox'))}}
							<span class="help-inline"></span>
						</div>
					</div>
					@endif
				</fieldset>
			</div>
		</div>
	</div>
</div>
<div class="form-actions">
	{{ Form::submit('Simpan', array('class' => 'btn btn-primary submit')) }}
	{{ Form::reset('Reset',array('class'=>'btn btn-warning'))}}
</div>
{{ Form::close(); }}

<script type="text/javascript">
	$(function(){
		$('.noKLB').hide();
		$('#id_kelurahan').change(function(){
			var nama_anak  = $('#nama_anak').val();
			var nik  = $('#nik').val();
			var nama_ortu  = $('#nama_ortu').val();
			var alamat = $(this).val();
			$.post('{{URL::to("cekPasien")}}', {nama_anak:nama_anak,nik:nik,nama_ortu:nama_ortu,id_kelurahan:alamat}, function(response){
				if(response){
					if(response.sent.status=='1'){
						alert(response.sent.message);
					};
				};
			});
		});
		$('#form_save_campak').validate({
			submit: {
				settings: {
					scrollToError: {
						offset: -100,
						duration: 500
					}
				}
			}
		});
	});
	function pilih_sampel()
	{
		if($('#nama_sampel').val()=='0')
		{
			$('#jenis_sampel').html('');
			$('#jenis_sampel').removeAttr('disabled','');
			$('#jenis_sampel').append('<option value="">Pilih</option><option value="0">Darah</option><option value="1">Urin</option>');
		}
		else if($('#nama_sampel').val()=='1')
		{
			$('#jenis_sampel').html('');
			$('#jenis_sampel').removeAttr('disabled','');
			$('#jenis_sampel').append('<option value="">Pilih</option><option value="0">Urin</option><option value="1">Usap tenggorokan</option><option value="2">Cairan mulut</option>');
		}
		else if($('#nama_sampel').val()=='')
		{
			$('#jenis_sampel').attr('disabled','disabled');
			$('#jenis_sampel').html('');
		}
	}
	function add_sampel()
	{
		var nama_sampel             = $("#nama_sampel").val();
		var jenis_sampel            = $("#jenis_sampel").val();
		var tanggal_ambil_sampel    = $("#tanggal_ambil_sampel").val();
		if(nama_sampel=='0')
		{
			var nama_sampel_baru = 'Serologi';
			var jenis_sampel_baru=(jenis_sampel==0)?'Darah':'Urin';
		}
		if(nama_sampel=='1')
		{
			var nama_sampel_baru = 'virologi';
			if (jenis_sampel==0) {
				var jenis_sampel_baru='Urin';
			}
			else if(jenis_sampel==1)
			{
				var jenis_sampel_baru='Usap tenggorokan';
			}
			else
			{
				var jenis_sampel_baru ='Cairan mulut';
			}
		}

		$("#data_sampel").append('<tr valign="top"><td><input id="data_nama_sampel" name="nama_sampel[]" type="hidden" value='+nama_sampel+'>'+nama_sampel_baru+'</td><td><input id="data_jenis_sampel" name="jenis_sampel[]" type="hidden" value='+jenis_sampel+'>'+jenis_sampel_baru+'</td><td><input id="data_tanggal_ambil_sampel" name="tanggal_ambil_sampel[]" type="hidden" value='+tanggal_ambil_sampel+'>'+tanggal_ambil_sampel+'</td><td><a href="javascript:void(0);" class="remCF">Remove</a></td></tr>');
		$("#nama_sampel").select2("val","");
		$("#jenis_sampel").select2("val","");
		$("#tanggal_ambil_sampel").val('');
	}
</script>