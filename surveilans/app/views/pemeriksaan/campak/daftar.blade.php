<div class="module-body uk-overflow-container">
<div class="table-responsive">
    <table class="display table table-bordered" id="example1">
    <thead>
        <tr>
          <th>No. Epid</th>
          <th>Nama </th>
          <th>Nama Orang Tua</th>
          <th>Alamat</th>
          <th>Jumlah Total Kasus Tambahan</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
      @if($pe_campak)
      @foreach($pe_campak as $row)
                <tr>
                    <td>{{$row->no_epid}}</td>
                    <td>{{$row->nama_anak}}</td>
                    <td>{{$row->nama_ortu}}</td>
                    <td>{{$row->alamat}}</td>
                    <td>{{$row->jumlah_total_kasus_tambahan}}</td>
                    @if(Sentry::getUser()->hak_akses==2 || Sentry::getUser()->hak_akses==3 || Sentry::getUser()->hak_akses==7)
                    <td>
                    <div class="btn-group" role="group" >
                      <a href="{{URL::to('pe_campak_detail/'.$row->id)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
                      <a href="{{URL::to('pe_campak_edit/'.$row->id)}}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>
                      <a href="{{URL::to('pe_campak_hapus/'.$row->id)}}" onclick="return confirm('apakah yakin ingin menghapus data ini?');return false;" class="btn btn-xs btn-danger" data-uk-tooltip title="hapus data"><i class="icon icon-remove"></i></a>
                    </div>
                    </td>
                    @elseif(Sentry::getUser()->hak_akses==4)
                    <td>
                    <div class="btn-group" role="group" >
                      <a href="{{URL::to('pe_campak_detail/'.$row->id)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
                      <a href="{{URL::to('pe_campak_edit/'.$row->id)}}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>
                      </div>
                    </td>
                    @elseif(Sentry::getUser()->hak_akses==1 || Sentry::getUser()->hak_akses==6)
                    <td>
                    <div class="btn-group" role="group" >
                      <a href="{{URL::to('pe_campak_detail/'.$row->id)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
                    </div>
                    </td>
                    @elseif(Sentry::getUser()->hak_akses==5)

                    @endif
                    
                </tr>
            @endforeach
        @endif
      </tbody>
    </table>

    <script>
        $(document).ready(function() 
        {
            $('#example1').dataTable();
        });
    </script>
</div>
</div>