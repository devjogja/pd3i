@extends('layouts.master')
@section('content')
<style type="text/css">
	.kasus_tambahan {
		border-collapse: collapse;
		width: 100%;
	}

	.kasus_tambahan th, td {
		text-align: left;
		padding: 8px;
	}

	.kasus_tambahan tr:nth-child(even){background-color: #f2f2f2}

	.kasus_tambahan th {
		background-color: #4CAF50;
		color: white;
		text-align: center;
	}

	.add{
		margin-bottom: 6px;
	}
</style>

<!-- awal span12 -->
<div class="span12">
	<!-- awal class content -->
	<div class="content">
		<!-- awal class module -->
		<div class="module">
			<!-- awal class module-body -->
			<div class="module-body">
				<div class="profile-head media">
					<h4>
						Input data penyelidikan epidemologi kasus campak
					</h4>
					<hr>
				</div>
				<!-- awal form -->
				{{Form::open(array('route'=>'pe_campak','class'=>'form-horizontal'))}}
				@foreach($pe_campak as $row)
				{{Form::hidden('id_pasien',$row->id_pasien)}}
				{{Form::hidden('no_epid',$row->no_epid)}}
				{{Form::hidden('id_campak',$row->idCampak)}}
				@endforeach
				<!-- awal class module-body -->
				<div class="module-body">
					<!-- awal class row-fluid -->
					<div class="row-fluid">
						<!-- awal class span6 -->
						<div class="span6">
							<div class="media">
								<fieldset>
									<legend>Riwayat pengobatan</legend>
									<!-- awal input kolom Kapan mendapatkan pengobatan -->
									<div class="control-group">
										<label class="control-label">Kapan mendapatkan pengobatan pertama kali?</label>
										<div class="controls">
											{{Form::text(
												'waktu_pengobatan',
												Input::old('waktu_pengobatan'),
												array(
													'class'=>'input-medium',
													'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
													)
												)
											}}
											<span class="help-inline"></span>
										</div>
									</div>
									<!-- akhir input kolom Kapan mendapatkan pengobatan -->

									<!-- awal input kolom dimana mendapatkan pengobatan -->
									<div class="control-group">
										<label class="control-label">Dimana mendapatkan pengobatan pertama kali?</label>
										<div class="controls">
											{{Form::text('tempat_pengobatan',Input::old('tempat_pengobatan'),array('class'=>'input-medium'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<!-- akhir input kolom dimana mendapatkan pengobatan -->

									<!-- awal input kolom obat yang sudah diberikan -->
									<div class="control-group">
										<label class="control-label">Obat yang sudah diberikan</label>
										<div class="controls">
											{{Form::text('obat_yang_diberikan',Input::old('obat_yang_diberikan'),array('class'=>'input-medium'))}}
											<span class="help-inline"></span>
										</div>
									</div>
									<!-- akhir input kolom yang sudah diberikan -->
								</fieldset>
							</div>
						</div>
						<!-- akhir class span6 -->
						
						<!-- awal class span6 -->
						<div class="span6">
							<div class="media">
								<fieldset>
									<legend>Riwayat kontak</legend>
									<!-- awal input kolom dirumah ada yang sakit -->
									<div class="control-group">
										<label class="control-label">Apakah dirumah ada yang sakit seperti yang di alami sekarang</label>
										<div class="controls">
											{{Form::select(
												'dirumah_orang_sakit_sama', 
												array(
													'0'=>'Pilih',
													'1' => 'Ya',
													'2'=>'Tidak'), 
												Input::old('dirumah_orang_sakit_sama'), 
												array(
													'class' => 'input-small id_rumah id_combobox',
													)
												)
											}}
											Kapan sakit : 
											{{Form::text(
												'kapan_sakit_dirumah',
												Input::old('kapan_sakit_dirumah'),
												array(
													'class'=>'input-small tgl_sakit_dirumah',
													'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
													'disabled'
													)
												)
											}}
											<span class="help-inline"></span>
										</div>
									</div>
									<!-- akhir input kolom dirumah ada yang sakit -->

									<!-- awal input kolom disekolah ada yang sakit -->
									<div class="control-group">
										<label class="control-label">Apakah disekolah anak ada yang sakit seperti yang dialami sekarang</label>
										<div class="controls">
											{{Form::select(
												'disekolah_orang_sakit_sama', 
												array(
													'0'=>'Pilih',
													'1' => 'Ya',
													'2'=>'Tidak'), 
												Input::old('disekolah_orang_sakit_sama'), 
												array(
													'class' => 'input-small id_sekolah id_combobox',
													)
												)
											}}
											Kapan sakit : 
											{{Form::text(
												'kapan_sakit_disekolah',
												Input::old('kapan_sakit_disekolah'),
												array(
													'class'=>'input-small tgl_sakit_disekolah',
													'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
													'disabled'
													)
												)
											}}
											<span class="help-inline"></span>
										</div>
									</div>
									<!-- awal input kolom disekolah ada yang sakit -->

									<!-- awal input kolom penderita menunjukan kekurangan gizi -->
									<div class="control-group">
										<label class="control-label">Apakah penderita menunjukkan keadaan kekurangan gizi (BB/U)</label>
										<div class="controls">
											{{Form::select(
												'kekurangan_gizi', 
												array(
													'0'=>'Pilih',
													'1' => 'Ya',
													'2'=>'Tidak'), 
												Input::old('kekurangan_gizi'), 
												array(
													'class' => 'input-small id_combobox'
													)
												)
											}}
											<span class="help-inline"></span>
										</div>
									</div>
									<!-- awal input kolom penderita menunjukan kekurangan gizi -->
								</fieldset>
							</div>
						</div>
						<!-- akhir class span6 -->
						<!-- 
							cek jika status kasus bernilai true/1 maka
							stop pelacakan form akan dimatikan
						-->
						<?php
						if($pe_campak[0]->status_kasus=='' OR $pe_campak[0]->status_kasus=='0' OR $pe_campak[0]->status_kasus=='2') 
						{

						}else{
							?>
						</div>
					</div>

					<!-- awal class module-body -->
					<div class="module-body">
						<!-- awal class row-fluid -->
						<div class="row-fluid">
							<!-- awal class span12 -->
							<div class="span12">
								<div class="media">
									<fieldset>
										<legend>Jumlah kasus tambahan di Lokasi PE</legend>
										<table class="kasus_tambahan">
											<thead>
												<tr>
													<th style="width: 45%"></th>
													<th>Tanggal PE</th>
													<th>Jumlah kasus tambahan</th>
													<th style="width: 2px"></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Alamat tinggal sementara saat sakit (bila ada)</td>
													<td style="text-align: center;">{{Form::text('tgl_pe_dialamat_sementara',null,array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}</td>
													<td style="text-align: center;">{{Form::text('jumlah_kasus_dialamat_sementara',null,array('class'=>'input-small sum','onchange'=>'total_kasus()'))}}</td>
												</tr>
												<tr>
													<td>Alamat tempat tinggal</td>
													<td style="text-align: center;">{{Form::text('tgl_pe_alamat_tempat_tinggal',null,array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}</td>
													<td style="text-align: center;">{{Form::text('jumlah_kasus_dialamat_tempat_tinggal',null,array('class'=>'input-small sum','onchange'=>'total_kasus()'))}}</td>
												</tr>
												<tr>
													<td>Sekolah</td>
													<td style="text-align: center;">{{Form::text('tgl_pe_sekolah',null,array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}</td>
													<td style="text-align: center;">{{Form::text('jumlah_kasus_disekolah',null,array('class'=>'input-small sum','onchange'=>'total_kasus()'))}}</td>
												</tr>
												<tr>
													<td>Tempat kerja</td>
													<td style="text-align: center;">{{Form::text('tgl_pe_tempat_kerja',null,array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}</td>
													<td style="text-align: center;">{{Form::text('jumlah_kasus_tempat_kerja',null,array('class'=>'input-small sum','onchange'=>'total_kasus()'))}}</td>
												</tr>
												<tr>
													<td>{{Form::text('lain_lain',null,array('class'=>'input-xlarge','placeholder'=>'Lain-lain'))}}</td>
													<td style="text-align: center;">{{Form::text('tgl_pe_lain_lain',null,array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}</td>
													<td style="text-align: center;">{{Form::text('jumlah_kasus_lain_lain',null,array('class'=>'input-small sum','onchange'=>'total_kasus()'))}}</td>
													<!-- <td>
														<a href="javascript:void(0)" class="btn btn-xs btn-success add_kasus" data-uk-tooltip title="Tambah Kasus" ><i class="icon icon-plus"></i></a>
													</td> -->
												</tr>
											</tbody>
											<tfoot>
												<tr>
													<th colspan="2" style="text-align: right !important;">Jumlah Total Kasus tambahan</th>
													<th colspan="2"><span class="total_kasus">0</span></th>
													<input type="hidden" name="jumlah_total_kasus_tambahan" class="jumlah_total_kasus_tambahan" value="">
												</tr>
											</tfoot>
										</table>
									</fieldset>
								</div>
							</div>
							<!-- Akhir class span12 -->
						</div>
						<!-- akhir class row-fluid -->
					</div>
					<!-- akhie class module-body -->
					<?php }?>
					<!-- awal class module-body -->
					<div class="module-body">
						<!-- awal class row-fluid -->
						<div class="row-fluid">
							<!-- awal class span12 -->
							<div class="span12">
								<div class="media">
									<fieldset>
										<legend>Informasi penyelidikan</legend>
										<!-- awal class control-group -->
										<!-- awal input kolom tanggal penyelidikan -->
										@if($pe_campak[0]->status_kasus !=='1')
										<div class="control-group">
											<label class="control-label">Tanggal penyelidikan</label>
											<div class="controls">
												{{Form::text(
													'tanggal_penyelidikan',
													Input::old('tanggal_penyelidikan'),
													array(
														'class'=>'input-medium',
														'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
														)
													)
												}}
												<span class="help-inline"></span>
											</div>
										</div>
										@endif
										<!-- akhir input kolom tanggal penyelidikan -->

										<!-- awal input pelaksana -->
										<div class="control-group">
											<label class="control-label">Pelaksana</label>
											<div class="controls" id="pelaksana">
												<div class="add">
													{{Form::text('pelaksana[]',null,array('class'=>'input-xlarge'))}}
													<a href="javascript:void(0)" class="btn btn-xs btn-success add_pelaksana" data-uk-tooltip title="Tambah data pelaksana" ><i class="icon icon-plus"></i></a>
												</div>
											</div>
										</div>
										<!-- akhir input pelaksana -->
									</fieldset>
								</div>
							</div>
							<!-- Akhir class span12 -->
						</div>
						<!-- akhir class row-fluid -->
					</div>
					<!-- akhie class module-body -->

					<div class="form-actions">
						{{Form::submit('simpan',array('class'=>'btn btn-primary'))}}
						<a href="{{URL::to('campak')}}" class="btn btn-warning">batal</a>
					</div>
					{{Form::close()}}
					<!-- akhir form -->

					<!-- awal js -->
					<script type="text/javascript">
						$(function(){
							$('.id_rumah').on('change',function(){
								var val = $(this).val();
								if(val == '1'){
									$('.tgl_sakit_dirumah').removeAttr('disabled');
								}else{
									$('.tgl_sakit_dirumah').val(null);
									$('.tgl_sakit_dirumah').attr('disabled','disabled');
								}
								return false;
							});
							$('.id_sekolah').on('change',function(){
								var val = $(this).val();
								if(val == '1'){
									$('.tgl_sakit_disekolah').removeAttr('disabled');
								}else{
									$('.tgl_sakit_disekolah').val(null);
									$('.tgl_sakit_disekolah').attr('disabled','disabled');
								}
								return false;
							});

							$('.add_pelaksana').on('click',function(){
								$('#pelaksana').append('<div class="add">{{Form::text('pelaksana[]',null,array('class'=>'input-xlarge'))}}</div>');
								return false;
							});
							$('.add_kasus').on('click',function(){
								$('.kasus_tambahan tbody').append('<tr>'+
										'<td>{{Form::text('kasus[]',null,array('class'=>'input-xlarge','placeholder'=>'Lain-lain'))}}</td>'+
										'<td style="text-align: center;">{{Form::text('tgl_pe[]',null,array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}</td>'+
										'<td style="text-align: center;">{{Form::text('jml_kasus[]',null,array('class'=>'input-small sum','onchange'=>'total_kasus()'))}}</td>'+
										'<td><a href="javascript:void(0)" class="btn btn-xs btn-warning remove" ><i class="icon icon-remove"></i></a></td>'+
										'</tr>'
									);
								$('.remove').on('click',function(){
									$(this).parent().parent().remove();
									return false;
								});
								return false;
							});
						});

						function total_kasus(){
							var sum = 0;
							$('.sum').each(function(){
								sum += Number($(this).val());
							});
							$('.jumlah_total_kasus_tambahan').val(sum);
							$('.total_kasus').html(sum);
							return false;
						}
					</script>
					<!-- akhir js -->
				</div>
				<!-- akhir class module-body -->
			</div>
			<!-- akhir class module -->
		</div>
		<!-- akhir class content -->
	</div>
	<!-- akhir class span12 -->
	@stop