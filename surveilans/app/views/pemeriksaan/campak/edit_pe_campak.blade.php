@extends('layouts.master')
@section('content')
<style type="text/css">
    .kasus_tambahan {
        border-collapse: collapse;
        width: 100%;
    }

    .kasus_tambahan th, td {
        text-align: left;
        padding: 8px;
    }

    .kasus_tambahan tr:nth-child(even){background-color: #f2f2f2}

    .kasus_tambahan th {
        background-color: #4CAF50;
        color: white;
        text-align: center;
    }

    .add{
        margin-bottom: 6px;
    }
</style>

<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <h4>
                        Edit data penyelidikan epidemologi kasus penyakit campak
                    </h4>
                    <hr>
                </div>
                {{Form::open(array('url'=>'update_pe_campak','class'=>'form-horizontal'))}}
                @foreach($data as $row)
                <div class="module-body">
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="media">
                                <fieldset>
                                    <legend>Identitas penderita</legend>
                                    <div class="control-group">
                                        <label class="control-label">No. epidemologi</label>
                                        <div class="controls">
                                            {{Form::text(
                                                'no_epid',
                                                $row->no_epid,
                                                array(
                                                    'class'=>'input-large',
                                                    'placeholder'=>'Masukan nomer epidemologi',
                                                    'id'=>'epid_pe'
                                                    )
                                                )
                                            }}
                                            {{Form::hidden(
                                                'id',
                                                $row->id,
                                                array(
                                                    'class'=>'input-medium'
                                                    )
                                                )
                                            }}
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Nama penderita</label>
                                        <div class="controls">
                                            {{Form::text(
                                                'nama_anak',
                                                $row->nama_anak, 
                                                array(
                                                    'class'=>'input-xlarge nama_anak',
                                                    'placeholder'=>'Nama anak'
                                                    )
                                                )
                                            }}
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Jenis kelamin</label>
                                        <div class="controls">
                                            {{Form::select(
                                                'jenis_kelamin',
                                                [
                                                '0' => 'Pilih',
                                                '1'=>'Laki-laki',
                                                '2'=>'Perempuan',
                                                '3'=>'Tidak jelas'],
                                                [$row->jenis_kelamin], 
                                                array(
                                                    'class' => 'input-medium id_combobox jenis_kelamin'
                                                    )
                                                )
                                            }}
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Tanggal lahir</label>
                                        <div class="controls">
                                            {{Form::text(
                                                'tanggal_lahir',
                                                Helper::getDate($row->tanggal_lahir),
                                                array(
                                                    'class' => 'input-medium tanggal_lahir',
                                                    'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
                                                    'onchange'=>'umur_pe()'
                                                    )
                                                )
                                            }}
                                        <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Umur</label>
                                        <div class="controls">
                                            <input type="text" class="input-mini" value="{{$row->umur}}" name="tgl_tahun" id="tanggal_tahun">
                                            <span class="add-on">Thn</span> 
                                            <input type="text" class="input-mini" value="{{$row->umur_bln}}" name="tgl_bulan" id="tanggal_bulan">
                                            <span class="add-on">Bln</span> 
                                            <input type="text" class="input-mini" value="{{$row->umur_hr}}" name="tgl_hari" id="tanggal_hari">
                                            <span class="add-on">Hr</span>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="media">
                                <fieldset>
                                    <legend>Wilayah</legend>
                                    <div class="control-group">
                                        <label class="control-label">Provinsi</label>
                                        <div class="controls">
                                            <select name="" class="input-medium" id="id_provinsi" onchange="showKabupaten()">
                                            <?php $pro=Provinsi::lists('provinsi','id_provinsi'); 
                                                foreach ($pro as $id_provinsi => $provinsi) 
                                                {
                                                    if($provinsi==$row->provinsi) 
                                                    {
                                                    ?>
                                                        <option value="{{$id_provinsi}}" selected="selected">{{$provinsi}}</option>
                                                    <?php
                                                    } else 
                                                    {
                                                    ?>
                                                        <option value="{{$id_provinsi}}">{{$provinsi}}</option>
                                                    <?php
                                                    }
                                                } 
                                            ?>
                                            </select>
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Kabupaten/Kota</label>
                                        <div class="controls">
                                            <select name="" class="input-medium" id="id_kabupaten" onchange="showKecamatan()">
                                                <option value="{{$row->id_kabupaten}}">{{$row->kabupaten}}</option>
                                            </select>
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Kecamatan</label>
                                        <div class="controls">
                                            <select name="" class="input-medium" id="id_kecamatan" onchange="showKelurahan()">
                                                <option value="{{$row->id_kecamatan}}">{{$row->kecamatan}}</option>
                                            </select>
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Kelurahan/Desa</label>
                                        <div class="controls">
                                            <select name="id_kelurahan" class="input-medium" id="id_kelurahan" onchange="showEpid()">
                                                <option value="{{$row->id_kelurahan}}">{{$row->kelurahan}}</option>
                                            </select>
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Alamat</label>
                                        <div class="controls">
                                            {{Form::text(
                                                'alamat',
                                                $row->alamat, 
                                                array(
                                                    'class' =>'input-xlarge alamat',
                                                    'placeholder'=>'tulisan alamat seperti nama dusun/gang/blok/no jalan/rt/rw'
                                                    )
                                                )
                                            }}
                                        <span class="help-inline"></span>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="module-body">
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="media">
                                <fieldset>
                                    <legend>Riwayat pengobatan</legend>
                                    <div class="control-group">
                                        <label class="control-label">Kapan mendapatkan pengobatan pertama kali?</label>
                                        <div class="controls">
                                            {{Form::text(
                                                'waktu_pengobatan',
                                                Helper::getDate($row->waktu_pengobatan),
                                                array(
                                                    'class'=>'input-medium',
                                                    'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}
                                                    ')
                                                )
                                            }}
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Dimana mendapatkan pengobatan pertama kali?</label>
                                        <div class="controls">
                                            {{Form::text(
                                                'tempat_pengobatan',
                                                $row->tempat_pengobatan,
                                                array(
                                                    'class'=>'input-medium'
                                                    )
                                                )
                                            }}
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Obat yang sudah diberikan</label>
                                        <div class="controls">
                                            {{Form::text(
                                                'obat_yang_diberikan',
                                                $row->obat_yang_diberikan,
                                                array(
                                                    'class'=>'input-medium'
                                                    )
                                                )
                                            }}
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="media">
                                <fieldset>
                                    <legend>Riwayat kontak</legend>
                                    <div class="control-group">
                                        <label class="control-label">Apakah dirumah ada yang sakit seperti yang di alami sekarang</label>
                                        <div class="controls">
                                            {{Form::select(
                                                'dirumah_orang_sakit_sama',
                                                [
                                                '0'=>'Pilih',
                                                '1' => 'Ya',
                                                '2'=>'Tidak'],
                                                [$row->dirumah_orang_sakit_sama], 
                                                array(
                                                    'class' => 'input-small id_rumah id_combobox',
                                                    'onchange'=>'ceklistrumah()'
                                                    )
                                                )
                                            }}
                                            Kapan sakit : 
                                            {{Form::text(
                                                'kapan_sakit_dirumah',
                                                Helper::getDate($row->kapan_sakit_dirumah),
                                                array(
                                                    'class'=>'input-small tgl_rumah sakit_dirumah',
                                                    'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
                                                    )
                                                )
                                            }}
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Apakah disekolah anak ada yang sakit seperti yang dialami sekarang</label>
                                        <div class="controls">
                                            {{Form::select(
                                                'disekolah_orang_sakit_sama',
                                                [
                                                '0'=>'Pilih',
                                                '1' => 'Ya',
                                                '2'=>'Tidak'], 
                                                [$row->disekolah_orang_sakit_sama], 
                                                array(
                                                    'class' => 'input-small id_sekolah id_combobox',
                                                    'onchange'=>'ceklistsekolah()'
                                                    )
                                                )
                                            }}
                                            Kapan sakit : 
                                            {{Form::text(
                                                'kapan_sakit_disekolah',
                                                Helper::getDate($row->kapan_sakit_disekolah),
                                                array(
                                                    'class'=>'input-small tgl_sekolah sakit_disekolah',
                                                    'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
                                                    )
                                                )
                                            }}
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Apakah penderita menunjukkan keadaan kekurangan gizi (BB/U</label>
                                        <div class="controls">
                                            {{Form::select(
                                                'kekurangan_gizi',
                                                [
                                                '0'=>'Pilih',
                                                '1' => 'Ya',
                                                '2'=>'Tidak'],
                                                [$row->kekurangan_gizi], 
                                                array(
                                                    'class' => 'input-small id_combobox'
                                                    )
                                                )
                                            }}
                                        <span class="help-inline"></span>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <?php
                        if($row->jenis_kasus=='' || $row->jenis_kasus=='0' || $row->jenis_kasus=='2')
                        {
                        }else{
                        ?>
                    </div>
                </div>
                <div class="module-body">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="media">
                                <fieldset>
                                    <legend>Jumlah kasus tambahan di Lokasi PE</legend>
                                        <table class="kasus_tambahan">
                                            <thead>
                                                <tr>
                                                    <th style="width: 45%"></th>
                                                    <th>Tanggal PE</th>
                                                    <th>Jumlah kasus tambahan</th>
                                                    <th style="width: 2px"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Alamat tinggal sementara saat sakit (bila ada)</td>
                                                    <td style="text-align: center;">{{Form::text('tgl_pe_dialamat_sementara',Helper::getDate($row->tgl_pe_dialamat_sementara),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}</td>
                                                    <td style="text-align: center;">{{Form::text('jumlah_kasus_dialamat_sementara',$row->jumlah_kasus_dialamat_sementara,array('class'=>'input-small sum','onchange'=>'total_kasus()'))}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Alamat tempat tinggal</td>
                                                    <td style="text-align: center;">{{Form::text('tgl_pe_alamat_tempat_tinggal',Helper::getDate($row->tgl_pe_alamat_tempat_tinggal),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}</td>
                                                    <td style="text-align: center;">{{Form::text('jumlah_kasus_dialamat_tempat_tinggal',$row->jumlah_kasus_dialamat_tempat_tinggal,array('class'=>'input-small sum','onchange'=>'total_kasus()'))}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Sekolah</td>
                                                    <td style="text-align: center;">{{Form::text('tgl_pe_sekolah',Helper::getDate($row->tgl_pe_sekolah),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}</td>
                                                    <td style="text-align: center;">{{Form::text('jumlah_kasus_disekolah',$row->jumlah_kasus_disekolah,array('class'=>'input-small sum','onchange'=>'total_kasus()'))}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tempat kerja</td>
                                                    <td style="text-align: center;">{{Form::text('tgl_pe_tempat_kerja',Helper::getDate($row->tgl_pe_tempat_kerja),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}</td>
                                                    <td style="text-align: center;">{{Form::text('jumlah_kasus_tempat_kerja',$row->jumlah_kasus_tempat_kerja,array('class'=>'input-small sum','onchange'=>'total_kasus()'))}}</td>
                                                </tr>
                                                <tr>
                                                    <td>{{Form::text('lain_lain',$row->lain_lain,array('class'=>'input-xlarge','placeholder'=>'Lain-lain'))}}</td>
                                                    <td style="text-align: center;">{{Form::text('tgl_pe_lain_lain',Helper::getDate($row->tgl_pe_lain_lain),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}</td>
                                                    <td style="text-align: center;">{{Form::text('jumlah_kasus_lain_lain',$row->jumlah_kasus_lain_lain,array('class'=>'input-small sum','onchange'=>'total_kasus()'))}}</td>
                                                    <!-- <td>
                                                        <a href="javascript:void(0)" class="btn btn-xs btn-success add_kasus" data-uk-tooltip title="Tambah Kasus" ><i class="icon icon-plus"></i></a>
                                                    </td> -->
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="2" style="text-align: right !important;">Jumlah Total Kasus tambahan</th>
                                                    <th colspan="2"><span class="total_kasus"><?php echo $row->jumlah_total_kasus_tambahan;?></span></th>
                                                    <input type="hidden" name="jumlah_total_kasus_tambahan" class="jumlah_total_kasus_tambahan" value="<?php echo $row->jumlah_total_kasus_tambahan;?>">
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <br>
                                    <?php }?>
                                    <div class="control-group">
                                        <label class="control-label">Tanggal penyelidikan</label>
                                        <div class="controls">
                                            {{Form::text(
                                                'tanggal_penyelidikan',
                                                Helper::getDate($row->tanggal_penyelidikan),
                                                array(
                                                    'class'=>'input-medium',
                                                    'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
                                                    )
                                                )
                                            }}
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Pelaksana</label>
                                        <div class="controls">
                                            {{Form::text(
                                                'pelaksana',
                                                $row->pelaksana,
                                                array(
                                                    'class'=>'input-medium'
                                                    )
                                                )
                                            }}
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    {{Form::submit('simpan',array('class'=>'btn btn-primary'))}}
                    <a href="{{URL::to('campak#PE')}}" class="btn btn-warning">batal</a>
                </div>
                @endforeach
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() 
        { 
            $("select").select2();
            showKabupaten();
            showKecamatan();
            showKelurahan(); 
            Totalkasus();
        }
    );

    function total_kasus(){
        var sum = 0;
        $('.sum').each(function(){
            sum += Number($(this).val());
        });
        $('.jumlah_total_kasus_tambahan').val(sum);
        $('.total_kasus').html(sum);
        return false;
    }

    function ceklistrumah() {
    
    var id_kom=$('.id_rumah').find('option:selected').val();
        if(id_kom==1)
        {
            $('.tgl_rumah').removeAttr('disabled');
        }
        else {
            $('.tgl_rumah').attr('disabled','disabled');
        }
    }

    function ceklistsekolah() {
        
        var id_kom=$('.id_sekolah').find('option:selected').val();
        if(id_kom==1)
        {
            $('.tgl_sekolah').removeAttr('disabled');
        }
        else {
            $('.tgl_sekolah').attr('disabled','disabled');
        }
    }
    
    function showKabupaten(){
      var provinsi = $("#id_provinsi").val();
      var kabupaten = $("#id_kabupaten").val();
      // //kirim data ke server
      $('.lodingKab').html('<img src="{{URL::to("style/images/loding.gif")}}">');
      $.post('{{URL::to("provinsi/getkab")}}', {provinsi:provinsi,kabupaten:kabupaten}, function(response)
      {
          $('#id_kabupaten').removeAttr('disabled','');
          $("#id_kabupaten").html(response);
          $('.lodingKab').html('');
      });

    }

    function showKecamatan(){
      var kabupaten = $("#id_kabupaten").val();
      var kecamatan = $("#id_kecamatan").val();
      // //kirim data ke server
      $('.lodingKec').html('<img src="{{URL::to("style/images/loding.gif")}}">');
      $.post('{{URL::to("provinsi/getkec")}}', {kabupaten:kabupaten,kecamatan:kecamatan}, function(response)
      {
          $('#id_kecamatan').removeAttr('disabled','');
          $("#id_kecamatan").html(response);
          $('.lodingKec').html('');
      });

    }

    function showKelurahan(){
      var kecamatan= $("#id_kecamatan").val();
      var kelurahan = $("#id_kelurahan").val();
      // //kirim data ke server
      $('.lodingKel').html('<img src="{{URL::to("style/images/loding.gif")}}">');
      $.post('{{URL::to("provinsi/getkel")}}', {kecamatan:kecamatan,kelurahan:kelurahan}, function(response)
      { 
          $('#id_kelurahan').removeAttr('disabled','');
          $("#id_kelurahan").html(response);
          $('.lodingKel').html('');
      });

    }

    function Totalkasus() {
        var jml = '';
        var id1 = $('.id1').val();
        var id2 = $('.id2').val();
        var id3 = $('.id3').val();
        var id4 = $('.id4').val();
        var id5 = $('.id5').val();
        $.ajax({
              data:'id1='+id1+'&id2='+id2+'&id3='+id3+'&id4='+id4+'&id5='+id5,
              url:'{{URL::to("hitung_kasus")}}',
              success:function(data) {
              $('.id6').val(data);
         }
        });
    }
</script>
@stop