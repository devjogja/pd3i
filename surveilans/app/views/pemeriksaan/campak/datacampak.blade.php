<div class="table-responsive">
<table class="display table table-bordered" id="example">
  <thead>
    <tr>
      <td>No.</td>
      <th>No. Epid</th>
      <th>Nama pasien</th>
      <th>Usia</th>
      <th>Alamat</th>
      <th>Keadaan Akhir</th>
      <th>Klasifikasi akhir</th>
     
      <th>Aksi</th>
    </tr>
  </thead>

  <tbody>
    <?php $n=1;?>
    @if($data)
      @foreach($data as $row)
        <tr>
            <td>{{ $n }}</td>
            <td>{{ $row->no_epid }}</td>
            <td>{{ $row->nama_anak }}</td>
            <td>{{ Pasien::getAgeAttribute($row->tanggal_lahir) }}</td>
            <td>{{ $row->alamat }}</td>
            <td>{{$row->keadaan_akhir_nm}}</td>
            <td>{{$row->klasifikasi_final_nm}}</td>
            @if(Sentry::getUser()->hak_akses==2 || Sentry::getUser()->hak_akses==2)
            <td>
              <?php
                if(Campak::checkPE($row->no_epid))
                {
                    echo '<a class="btn btn-danger">Sudah PE</a>';
                }
                else
                {
              ?>
                  <a href="{{ URL::to('campak/entricampak/'.$row->id_campak)}}" class="btn btn-md btn-success">PE</a>
              <?php
                }
              ?>
            </td>
            @endif
            @if(Sentry::getUser()->hak_akses==2 || Sentry::getUser()->hak_akses==3)
            <td>
              <div class="btn-group" role="group" >
                {{-- detail btn --}}
                <a href="{{URL::to('campak_detail/'.$row->id_campak)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>

                {{-- edit btn --}}
                <a href="{{URL::to('campak_edit/'.$row->id_campak)}}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>

                {{-- hapus btn --}}
                <a href="{{URL::to('campak_hapus/'.$row->id_campak.'/'.$row->id_hasil_uji_lab_campak)}}" onclick="return confirm('apakah yakin ingin menghapus data ini?');return false;" class="btn btn-xs btn-danger" data-uk-tooltip title="hapus data"><i class="icon icon-remove"></i></a>

                {{-- print btn --}}
                <!-- <a href="" class="btn btn-xs btn-primary" data-uk-tooltip title="print label"><i class="icon icon-print"></i></a> -->
              </div>
            </td>
            @elseif(Sentry::getUser()->hak_akses==4)
            <td>
              <div class="btn-group" role="group" >
                {{-- detail btn --}}
                <a href="{{URL::to('campak_detail/'.$row->id_campak)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>

                {{-- edit btn --}}
                <a href="{{URL::to('campak_edit/'.$row->id_campak)}}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>

                {{-- print btn --}}
                <!-- <a href="" class="btn btn-xs btn-primary" data-uk-tooltip title="print label"><i class="icon icon-print"></i></a> -->
              </div>
            </td>
            @elseif(Sentry::getUser()->hak_akses==1 || Sentry::getUser()->hak_akses==6)
            <td>
              <div class="btn-group" role="group" >
                {{-- detail btn --}}
                <a href="{{URL::to('campak_detail/'.$row->id_campak)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>

               {{-- print btn --}}
                <!-- <a href="" class="btn btn-xs btn-primary" data-uk-tooltip title="print label"><i class="icon icon-print"></i></a> -->
              </div>
            </td>
            @elseif(Sentry::getUser()->hak_akses==5)
            @endif
            
            
        </tr>
        <?php $n++; ?>
      @endforeach
    @endif
  </tbody>
</table>
</div>
