@extends('layouts.master')
@section('content')
<div class="span12">
	<div class="content">
		<div class="module">
			<div class="module-body">
				<div class="profile-head media">
					<h4>
						Detail Pasien Kasus Penyakit Campak
					</h4>
					<hr>
				</div>
				@if($dt)
				<div class="module-body uk-overflow-container">
					<div class="table-responsive">
						<table class="table table-striped">
							<caption style="">Identitas Pasien</caption>
							<tr>
								<td class="title">No Epidemologi</td>
								<td>{{$dt->campak_no_epid}}</td>
							</tr>
							@if($dt->campak_no_epid_klb)
							<tr>
								<td class="title">No Epidemologi KLB</td>
								<td>{{$dt->campak_no_epid_klb}}</td>
							</tr>
							@endif
							<tr>
								<td class="title">No Epidemologi Lama</td>
								<td>{{$dt->campak_no_epid_lama}}</td>
							</tr>
							<tr>
								<td class="title">NIK</td>
								<td>{{$dt->pasien_nik}}</td>
							</tr>
							<tr>
								<td class="title">Nama orang tua</td>
								<td>{{$dt->pasien_nama_ortu}}</td>
							</tr>
							<tr>
								<td class="title">Jenis kelamin</td>
								<td>{{$dt->pasien_jenis_kelamin}}</td>
							</tr>
							<tr>
								<td class="title">Tanggal Lahir(Umur)</td>
								<td>{{$dt->pasien_tanggal_lahir}} ({{$dt->pasien_umur}}Th {{$dt->pasien_umur_bln}}Bln {{$dt->pasien_umur_hr}}hr )</td>
							</tr>
							<tr>
								<td class="title">Alamat</td>
								<td>{{$dt->alamat_new}}</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="module-body uk-overflow-container">
					<div class="table-responsive">
						<table class="table table-striped">
							<caption style="">Data Klinis Pasien</caption>
							<tr>
								<td class="title">Tanggal imunisasi campak terakhir</td>
								<td>{{Helper::getDate($dt->campak_tanggal_imunisasi_terakhir)}}</td>
							</tr>
							<tr>
								<td class="title">Imunisasi campak sebelum sakit berapa kali?</td>
								<?php
								$imunisasicampak = 0;
								if ($dt->campak_vaksin_campak_sebelum_sakit=='7') {
									$imunisasicampak = 'Tidak';
								}else if ($dt->campak_vaksin_campak_sebelum_sakit=='8') {
									$imunisasicampak = 'Tidak tahu';
								}else if ($dt->campak_vaksin_campak_sebelum_sakit=='9') {
									$imunisasicampak = 'Belum pernah';
								}else if($dt->campak_vaksin_campak_sebelum_sakit=='' OR $dt->campak_vaksin_campak_sebelum_sakit==null){
									$imunisasicampak = 'Tidak diisi';
								}else{
									$imunisasicampak = $dt->campak_vaksin_campak_sebelum_sakit.' x';
								}
								?>
								<td>{{$imunisasicampak}}</td>
							</tr>
							<tr>
								<td class="title">Tanggal mulai demam</td>
								<td>{{Helper::getDate($dt->campak_tanggal_timbul_demam)}}</td>
							</tr>
							<tr>
								<td class="title">Tanggal mulai rash</td>
								<td>{{Helper::getDate($dt->campak_tanggal_timbul_rash)}}</td>
							</tr>
							<tr>
								<td class="title">Gejala / tanda lainnya</td>
								<td>
									@if($listgejala)
									<table style="width:60%">
										<thead>
											<tr>
												<th style="text-align:center">Nama Gejala</th>
												<th style="text-align:center">Tanggal sakit</th>
											</tr>
										</thead>
										<tbody>
											@foreach($listgejala AS $key=>$val)
											<tr>
												<td>{{$val['nama_gejala']}}</td>
												<td>{{$val['tgl_sakit']}}</td>
											</tr>
											@endforeach
										</tbody>
									</table>
									@else
									-
									@endif
								</td>
							</tr>
							<tr>
								<td class="title">Komplikasi</td>
								<td>
									@if($listkomplikasi)
									<table style="width:40%">
										<thead>
											<tr>
												<th style="text-align:center">Komplikasi</th>
											</tr>
										</thead>
										<tbody>
											@foreach($listkomplikasi AS $key=>$val)
											<tr>
												<td>{{$val['name_komplikasi']}}</td>
											</tr>
											@endforeach
										</tbody>
									</table>
									@else
									-
									@endif
								</td>
							</tr>
							<tr>
								<td class="title">Tanggal laporan diterima</td>
								<td>{{Helper::getDate($dt->campak_tanggal_laporan_diterima)}}</td>
							</tr>
							<tr>
								<td class="title">Tanggal pelacakan</td>
								<td>{{Helper::getDate($dt->campak_tanggal_pelacakan)}}</td>
							</tr>
							<tr>
								<td class="title">Diberi vitamin A</td>
								<td>
									@if($dt->campak_vitamin_A=='1')
									Ya
									@elseif($dt->campak_vitamin_A=='2')
									Tidak
									@else
									-
									@endif
								</td>
							</tr>
							<tr>
								<td class="title">Keadaan akhir</td>
								<td>
									@if($dt->campak_keadaan_akhir=='1')
									Hidup
									@elseif($dt->campak_keadaan_akhir=='2')
									Meninggal
									@else
									-
									@endif
								</td>
							</tr>
							<tr>
								<td class="title">Jenis kasus</td>
								<td>
									@if($dt->campak_jenis_kasus=='1')
									KLB
									@elseif($dt->campak_jenis_kasus=='2')
									Bukan KLB
									@else
									-
									@endif
								</td>
							</tr>
							<tr>
								<td class="title">KLB Ke</td>
								<td>{{$dt->campak_klb_ke}}</td>
							</tr>
							<tr>
								<td class="title">Status kasus</td>
								<td>
									@if($dt->campak_status_kasus=='1')
									Index
									@elseif($dt->campak_status_kasus=='2')
									Bukan Index
									@else
									-
									@endif
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="module-body uk-overflow-container">
					<div class="table-responsive">
						<table class="table table-striped">
							<caption style="">Data Spesimen</caption>
							<thead>
								<tr>
									<td class="title">Jenis pemeriksaan</td>
									<td class="title">Jenis Sampel</td>
									<td class="title">Tanggal ambil sampel</td>
								</tr>
							</thead>
							<tbody>
								@if(count($listSpesimen)>='1')
								@foreach($listSpesimen AS $key=>$val)
								<tr>
									<td>{{$val['jenis_pemeriksaan']}}</td>
									<td>{{$val['jenis_sampel']}}</td>
									<td>{{$val['tgl_ambil_sampel']}}</td>
								</tr>
								@endforeach
								@else
								<tr>
									<td colspan="3">Data spesimen tidak ada</td>
								</tr>
								@endif
							</tbody>
						</table>
					</div>
				</div>
				<div class="module-body uk-overflow-container">
					<div class="table-responsive">
						<table class="table table-striped">
							<caption style="">Hasil Klasifikasi Final</caption>
							<thead>
								<tr>
									<td class="title" style="text-align:center" colspan="2">Hasil Laboratorium Serologi</td>
									<td class="title" style="text-align:center" colspan="2">Hasil Laboratorium Virologi</td>
								</tr>
								<tr>
									<td class="title">IgM Campak</td>
									<td class="title">Igm Rubella</td>
									<td class="title">IgM Campak</td>
									<td class="title">Igm Rubella</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									@if($dt->campak_hasil_serologi_igm_campak=='1')
									<td>Positif</td>
									@elseif($dt->campak_hasil_serologi_igm_campak=='2')
									<td>Negatif</td>
									@elseif($dt->campak_hasil_serologi_igm_campak=='3')
									<td>Equivocal</td>
									@elseif($dt->campak_hasil_serologi_igm_campak=='4')
									<td>pending</td>
									@else
									<td>-</td>
									@endif
									@if($dt->campak_hasil_serologi_igm_rubella=='1')
									<td>Positif</td>
									@elseif($dt->campak_hasil_serologi_igm_rubella=='2')
									<td>Negatif</td>
									@elseif($dt->campak_hasil_serologi_igm_rubella=='3')
									<td>Equivocal</td>
									@elseif($dt->campak_hasil_serologi_igm_rubella=='4')
									<td>Pending</td>
									@else
									<td>-</td>
									@endif
									@if($dt->campak_hasil_virologi_igm_campak=='1')
									<td>Positif</td>
									@elseif($dt->campak_hasil_virologi_igm_campak=='2')
									<td>Negatif</td>
									@elseif($dt->campak_hasil_virologi_igm_campak=='3')
									<td>Pending</td>
									@else
									<td>-</td>
									@endif
									@if($dt->campak_hasil_virologi_igm_rubella=='1')
									<td>Positif</td>
									@elseif($dt->campak_hasil_virologi_igm_rubella=='2')
									<td>Negatif</td>
									@elseif($dt->campak_hasil_virologi_igm_rubella=='3')
									<td>Pending</td>
									@else
									<td>-</td>
									@endif
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="module-body uk-overflow-container">
					<div class="table-responsive">
						<table class="table table-striped">
							<caption style="">Data Klasifikasi Final</caption>
							<tr>
								<td class="title">Hasil klasifikasi final</td>
								@if($dt->campak_klasifikasi_final=='1')
								<td>Campak (Lab)</td>
								@elseif($dt->campak_klasifikasi_final=='2')
								<td>Campak (Epid)</td>
								@elseif($dt->campak_klasifikasi_final=='3')
								<td>Campak (Klinis)</td>
								@elseif($dt->campak_klasifikasi_final=='4')
								<td>Rubella</td>
								@elseif($dt->campak_klasifikasi_final=='5')
								<td>Bukan campak/rubella</td>
								@elseif($dt->campak_klasifikasi_final=='6')
								<td>Pending</td>
								@else
								<td>Belum ada klasifikasi final</td>
								@endif
							</tr>
						</table>
					</div>
				</div>
				@endif
			</div>
		</div>
		<div class="form-actions" style="text-align:center">
			<a href="{{URL::to('campak')}}" class="btn btn-warning">Kembali</a>
		</div>
	</div>
</div>
<style type="text/css" media="screen">
	caption{
		border:1px solid #eee;background-color:#666; color:#fff;
	}
	.title{
		width: 30%;
		font-weight: bold;
	}
</style>

@stop
