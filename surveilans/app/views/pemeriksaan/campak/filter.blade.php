	<div class="module">
	<!--
	<div class="module-head">
		<h3>Filter</h3>
	</div>
	-->
	<div class="module-body">
		<form id="search2" method="post" action="{{URL::to('region/get_chart_campak_js')}}">
			<div class="chart inline-legend grid">
				<table class="table" style="width:100%;">
					<tr>
						<td style="text-align:right;width:10%;">
							Unit
						</td>
						<td style="width:25%;">
							<select name="unit" id="unit" style="width: 90px;" onkeypress="focusNext('day_start', 'clinic_id', this, event)" onchange="setDisable(this, event)">
								<option value="all">All</option>
								<option value="day">Hari</option>
								<option value="month">Bulan</option>
								<option value="year">Tahun</option>
							</select>
						</td>
						<td style="text-align:right;width:15%;">
							Provinsi
						</td>
						<td>


								<select name="province_id" id="id_province" style="width:200px" onchange="get_district();" >
								<option value="">--- Pilih ---</option>
								<?php
								$q = "
									SELECT
										id_provinsi,
										provinsi
									FROM provinsi
									order by provinsi ASC
								";
								$combo_district=DB::select($q);
								?>
								<?php for($i=0;$i<count($combo_district);$i++) :?>
								<?php //if($combo_district[$i]->id_provinsi == "34") $sel='selected'; else $sel='';?>
								<option value="<?php echo $combo_district[$i]->id_provinsi?>" <?php //echo $sel; ?>><?php echo $combo_district[$i]->provinsi; ?></option>
								<?php endfor;?>
							</select>
						</td>
					</tr>
					<tr>
						<td style="text-align:right;">
							Sejak
						</td>
						<td>
							<?php
								$start['mktime'] = strtotime("-1 day");
								$start['day'] = date("j", $start['mktime']);
								$start['month'] = date("n", $start['mktime']);
								$start['year'] = date("Y", $start['mktime']);

								$now['day'] = date("j");
								$now['month'] = date("n");
								$now['year'] = date("Y");
								$now['year_start'] = 1971;

								$arr_nama_bulan[1] = "Januari";
								$arr_nama_bulan[2] = "Februari";
								$arr_nama_bulan[3] = "Maret";
								$arr_nama_bulan[4] = "April";
								$arr_nama_bulan[5] = "Mei";
								$arr_nama_bulan[6] = "Juni";
								$arr_nama_bulan[7] = "Juli";
								$arr_nama_bulan[8] = "Agustus";
								$arr_nama_bulan[9] = "September";
								$arr_nama_bulan[10] = "Oktober";
								$arr_nama_bulan[11] = "November";
								$arr_nama_bulan[12] = "Desember";

								?>
								<select name="day_start" id="day_start" style="width: 50px;" onkeypress="focusNext( 'month_start', 'unit', this, event)">
								<?php for($i=1;$i<32;$i++) :
										if($i==$start['day']) $sel = "selected"; else $sel = "";    ?>
									<option value="<?=$i?>" <?=$sel?> ><?=$i?></option>
								<?php endfor; ?>
								</select>

								<select name="month_start" id="month_start" style="width: 100px;" onkeypress="focusNext( 'year_start', 'day_start', this, event)">
									<?php for($i=1;$i<13;$i++) :
											//$bln = tambahNol($i, 2);
											if($i==$start['month']) $sel = "selected"; else $sel = ""; ?>
										<option value="<?php echo $i?>" <?php echo $sel?> ><?php echo $arr_nama_bulan[$i]; ?></option>
									<?php endfor; ?>
								</select>
								<select name="year_start" id="year_start" style="width: 70px;" onkeypress="focusNext( 'day_end', 'month_start', this, event)" class="inputan">
									<?php for($i=$now['year_start'];$i<=$now['year'];$i++) :
											if($i==$start['year']) $sel = "selected"; else $sel = "";   ?>
										<option value="<?php echo $i?>" <?php echo $sel?>><?php echo $i?></option>
									<?php endfor; ?>
								</select>
						</td>
						<td style="text-align:right;">
							Kabupaten
						</td>
						<td>
							<select name="district_id" id="district_id" style="width: 190px;" disabled="disabled"  onchange="get_sub_district();">

							</select>
						</td>
					</tr>
					<tr>
						<td style="text-align:right;">
							Sampai
						</td>
						<td>
							<select name="day_end" id="day_end" style="width: 50px;" >
							<?php for($i=1;$i<32;$i++) :
									if($i==$now['day']) $sel = "selected"; else $sel = "";  ?>
								<option value="<?=$i?>" <?=$sel?> ><?=$i?></option>
							<?php endfor; ?>
							</select>

							<select name="month_end" id="month_end" style="width: 100px;" >
								<?php for($i=1;$i<13;$i++) :
										//$bln = tambahNol($i, 2);
										if($i==$now['month']) $sel = "selected"; else $sel = ""; ?>
									<option value="<?php echo $i?>" <?php echo $sel?> ><?php echo $arr_nama_bulan[$i];?></option>
								<?php endfor; ?>
							</select>
							<select name="year_end" id="year_end" style="width: 70px;" class="inputan">
								<?php for($i=$now['year_start'];$i<=$now['year'];$i++) :
										if($i==$now['year']) $sel = "selected"; else $sel = ""; ?>
									<option value="<?php echo $i?>" <?php echo $sel?> ><?php echo $i?></option>
								<?php endfor; ?>
							</select>
						</td>
						<td style="text-align:right;">
							Kecamatan
						</td>
						<td>
							<select name="sub_district_id" id="sub_district_id" style="width: 190px;" disabled="disabled"  onchange="get_village();">

							</select>
						</td>
					</tr>
					<tr>
						<td style="text-align:right;">

						</td>
						<td>

						</td>
						<td style="text-align:right;">
							Puskesmas
						</td>
						<td>
							<select name="puskesmas_id" id="puskesmas_id" style="width: 190px;" disabled="disabled" >

							</select>
						</td>
					</tr>
					<tr>
						<td style="text-align:right;">

						</td>
						<td>

						</td>
						<td style="text-align:right;">
							<!--Desa-->
						</td>
						<td><!--
							<select name="village_id" id="village_id" style="width: 190px;" disabled="disabled" >

							</select>
							-->
						</td>
					</tr>
				</table>
				<center>
					<input id="tampilkan_analisa" type="submit" value="Tampilkan" class="btn btn-success">
					<!--
					<a href="{{-- URL::to('export/'.strtolower(Session::get('penyakit')).'/xls') --}}" class="btn btn-success">Export Excel</a>
					-->
				</center>
			</div>
		</form>
	</div>
</div>