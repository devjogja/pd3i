@extends('layouts.master')
@section('content')
<!-- awal class span12 -->
<div class="span12">
	<!-- awal class content -->
	<div class="content">
		<!-- awal class module -->
		<div class="module">
			<!-- awal class module-body -->
			<div class="module-body">
				<div class="profile-head media">
					<h4>
						Edit data kasus individual penyakit campak
					</h4>
					<hr>
				</div>
				@foreach($campak as $hasil)
				{{Form::open(array('url'=>'campak_update','class'=>'form-horizontal'))}}
				<fieldset>
					<legend>Identitas penderita</legend>
					<!-- awal input kolom nik -->
					<div class="control-group">
						<label class="control-label">NIK</label>
						<div class="controls">
							{{Form::text(
								'nik',
								$hasil->pasien_nik,
								array(
									'class' => 'input-medium',
									'placeholder'=>'Nomer induk kependudukan'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>
					<!-- akhir input kolom nik -->

					<!-- awal input kolom nama penderita -->
					<div class="control-group">
						<label class="control-label">Nama penderita</label>
						<div class="controls">
							{{Form::text(
								'nama_anak',
								$hasil->pasien_nama_anak,
								array(
									'class' => 'input-xlarge',
									'placeholder'=>'Nama anak')
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>
					<!-- akhir input kolom nama penderita -->

					<!-- awal input kolom Nama orang tua -->
					<div class="control-group">
						<label class="control-label">Nama orang tua</label>
						<div class="controls">
							{{Form::text(
								'nama_ortu',
								$hasil->pasien_nama_ortu,
								array(
									'class' => 'input-xlarge',
									'placeholder'=>'Nama orang tua')
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>
					<!-- akhir input kolom Nama orang tua -->

					<!-- awal input kolom jenis kelamin -->
					<div class="control-group">
						<label class="control-label">Jenis kelamin</label>
						<div class="controls">
							{{Form::select(
								'jenis_kelamin',
								[
								'0' => 'Pilih',
								'1'=>'Laki-laki',
								'2'=>'Perempuan',
								'3' => 'Tidak jelas'],
								$hasil->pasien_jenis_kelamin,
								array(
									'class' => ' id_combobox',
									'id'=>'jenis_kelamin')
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>
					<!-- akhir input kolom jenis kelamin -->

					<!-- awal input kolom tanggal lahir -->
					<div class="control-group">
						<label class="control-label">Tanggal lahir</label>
						<div class="controls">
							{{Form::text(
								'tanggal_lahir',
								Helper::getDate($hasil->pasien_tanggal_lahir),
								array(
									'class' => 'input-medium tgl_lahir',
									'placeholder'=>'Tanggal lahir',
									'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
									'onchange'=>'umur()')
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>
					<!-- akhir input kolom tanggal lahir -->

					<!-- awal input kolom umur -->
					<div class="control-group">
						<label class="control-label">Umur</label>
						<div class="controls">
							<input type="text" class="input-mini"  value="{{$hasil->pasien_umur}}" name="umur_tahun" id="tgl_tahun">
							Thn
							<input type="text" class="input-mini" value="{{$hasil->pasien_umur_bln}}" name="umur_bln" id="tgl_bulan">
							Bln
							<input type="text" class="input-mini" value="{{$hasil->pasien_umur_hr}}" name="umur_hr" id="tgl_hari">
							Hr
						</div>
					</div>
					<!-- akhir input kolom umur -->

					<!-- awal input kolom provinsi -->
					<div class="control-group">
						<label class="control-label">Provinsi</label>
						<div class="controls">
							{{ Form::select(
								'id_provinsi',
								array(
									''=>'Pilih provinsi')+Provinsi::lists('provinsi','id_provinsi'),
								$hasil->pasien_id_provinsi,
								array(
									'id'=>'id_provinsi_pasien',
									'placeholder' => 'Pilih Provinsi',
									'onchange'=>"getKabupaten('_pasien')",
									'class'=>'input-large id_combobox'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>
					<!-- akhir input kolom provinsi -->

					<!-- awal input kolom kabupaten -->
					<div class="control-group">
						<label class="control-label">Kabupaten</label>
						<div class="controls">
							{{Form::select(
								'id_kabupaten',
								[$hasil->pasien_id_kabupaten=>$hasil->pasien_kabupaten],
								$hasil->pasien_id_kabupaten,
								array(
									'placeholder'=>'Pilih',
									'class' => 'input-large id_combobox',
									'id'=>'id_kabupaten_pasien',
									'onchange'=>"getKecamatan('_pasien')"
									)
								)
							}}
							<span class="lodingKab"></span>
							<span class="help-inline"></span>
						</div>
					</div>
					<!-- akhir input kolom kabupaten -->

					<!-- awal input kolom kecamatan -->
					<div class="control-group">
						<label class="control-label">Kecamatan</label>
						<div class="controls">
							{{Form::select(
								'id_kecamatan',
								[$hasil->pasien_id_kecamatan=>$hasil->pasien_kecamatan],
								$hasil->pasien_id_kecamatan,
								array(
									'placeholder'=>'Pilih',
									'class' => 'input-large id_combobox',
									'id'=>'id_kecamatan_pasien',
									'onchange'=>"getKelurahan('_pasien')"
									)
								)
							}}
							<span class="lodingKec"></span>
							<span class="help-inline"></span>
						</div>
					</div>
					<!-- akhir input kolom kecamatan -->

					<!-- awal input kolom kelurahan atau desa -->
					<div class="control-group">
						<label class="control-label">Kelurahan/Desa</label>
						<div class="controls">
							{{Form::select(
								'id_kelurahan',
								[$hasil->pasien_id_kelurahan=>$hasil->pasien_kelurahan],
								$hasil->pasien_id_kelurahan,
								array(
									'data-validation'=>'[MIXED]',
									'data'=>'$ wajib di isi!',
									'data-validation-message'=>'Kelurahan harus di isi',
									'placeholder'=>'Pilih',
									'class' => 'input-large id_combobox',
									'id'=>'id_kelurahan_pasien',
									'onchange'=>"showEpidCampak('_pasien'),showEpidKLB('_pasien')"
									)
								)
							}}
							<span class="lodingKel"></span>
							<span class="help-inline" style="color:red">(*)</span>
						</div>
					</div>
					<!-- akhir input kolom kelurahan atau desa -->

					<!-- awal input kolom alamat -->
					<div class="control-group">
						<label class="control-label">Alamat</label>
						<div class="controls">
							{{Form::textarea('alamat',$hasil->pasien_alamat,array('rows'=>'3'))}}
							<span class="help-inline"></span>
						</div>
					</div>
					<!-- akhir input kolom alamat -->

					<!-- awal input kolom nama puskesmas -->
					<?php
					$nama_faskes='';
					if ($hasil->campak_kode_faskes=='rs') {
						$nama_faskes = $hasil->rs_nama_faskes;
					} elseif($hasil->campak_kode_faskes=='puskesmas') {
						$nama_faskes = $hasil->puskesmas_name;
					}
					?>

					<div class="control-group">
						<label class="control-label">Nama faskes saat periksa</label>
						<div class="controls">
							{{
								Form::text
								(
									'nama_puskesmas',
									$nama_faskes,
									array
									(
										'class' => 'input-large',
										'placeholder'=>'Masukan nama faskes'
										)
									)
								}}
								<span class="help-inline"></span>
							</div>
						</div>
						<!-- akhir input kolom nama puskesmas -->

						<!-- awal input kolom No epidemologi -->
						<div class="control-group">
							<label class="control-label">No Epidemologi</label>
							<div class="controls">
								{{Form::text(
									'no_epid',
									$hasil->campak_no_epid,
									array(
										'class' => 'input-medium',
										'placeholder'=>'Nomer epidemologi',
										'id'=>'epid')
									)
								}}
								{{Form::hidden(
									'id_campak',
									$hasil->campak_id_campak)
								}}
								{{Form::hidden(
									'id_hasil_uji_lab_campak',
									$hasil->lab_id_hasil_uji_lab_campak)
								}}
								<span class="help-inline"></span>
							</div>
						</div>
						<!-- akhir input kolom No epidemologi -->

						<!-- awal input kolom No epid baru-->
						<div class="control-group noKLB">
							<label class="control-label">No Epidemologi KLB</label>
							<div class="controls">
								{{Form::text(
									'no_epid_klb',
									$hasil->campak_no_epid_klb,
									array(
										'class' => 'input-medium',
										'placeholder'=>'Nomer epidemologi KLB',
										'id'=>'epidklb'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						<!-- akhir input kolom no epid baru -->


						<!-- awal input kolom No epidemologi lama -->
						<div class="control-group">
							<label class="control-label">No Epidemologi lama</label>
							<div class="controls">
								{{
									Form::text(
										'no_epid_lama',
										$hasil->campak_no_epid_lama,
										array(
											'class' => 'input-medium',
											'placeholder'=>'Nomer epidemologi'
											)
										)
									}}
									<span class="help-inline"></span>
								</div>
							</div>
							<!-- akhir input kolom No epidemologi lama -->
						</fieldset>
						<!-- akhir fieldset -->

						<!-- awal fieldset -->
						<fieldset>
							<legend>Data surveilans campak</legend>
							<!-- awal input kolom tanggal imunisasi campak terakhir -->
					{{-- <div class="control-group">
						<label class="control-label">ID outbreak</label>
						<div class="controls">
							{{
								Form::text('id_outbreak',
								null,
								['class' => 'input-large akseskab','readonly'=>'readonly','id'=>'id_outbreak'])
							}}
							<span class="help-inline"></span>
						</div>
					</div> --}}
					<div class="control-group">
						<label class="control-label">Tanggal imunisasi campak terakhir</label>
						<div class="controls">
							{{Form::text(
								'tanggal_imunisasi_terakhir',
								Helper::getDate($hasil->campak_tanggal_imunisasi_terakhir),
								array(
									'class'=>'input-medium tgl_sakit',
									'onchange'=>'umur()',
									'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
									)
								)
							}}
							<span class="help-inline" style="color:red"></span>
						</div>
					</div>
					<!-- akhir input kolom tanggal imunisasi campak terakhir -->

					<!-- awal input kolom imunisasi campak sebelum sakit -->
					<div class="control-group">
						<label class="control-label">Imunisasi campak sebelum sakit berapa kali?</label>
						<div class="controls">
							{{Form::select(
								'vaksin_campak_sebelum_sakit',
								[
								'0' => 'Pilih',
								'1' => '1X',
								'2'=>'2X',
								'3'=>'3X',
								'4'=>'4X',
								'5'=>'5X',
								'6'=>'6X',
								'7'=>'Tidak',
								'8'=>'Tidak tahu',
								'9'=>'Belum Pernah'
								],
								$hasil->campak_vaksin_campak_sebelum_sakit,
								array(
									'class'=>'input-large'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>
					<!-- akhir input kolom imunisasi campak sebelum sakit -->

					<div class="control-group">
						<label class="control-label">Tanggal mulai demam</label>
						<div class="controls">
							{{Form::text(
								'tanggal_timbul_demam',
								Helper::getDate($hasil->campak_tanggal_timbul_demam),
								array(
									'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
									'id'=>'tgl_mulai_sakit',
									'class'=>'input-medium',
									'onchange'=> 'usia()'
									)
								)
							}}
							<span class="help-inline" style="color:red"></span>
						</div>
					</div>
					<!-- awal input kolom tanggal mulai rash -->
					<div class="control-group">
						<label class="control-label">Tanggal mulai rash</label>
						<div class="controls">
							{{Form::text(
								'tanggal_timbul_rash',
								Helper::getDate($hasil->campak_tanggal_timbul_rash),
								array(
									'class'=>'input-medium',
									'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
									'id'=>'tgl_sakit',
									'onchange'=>"showEpidCampak('_pasien'),usia()"
									)
								)
							}}
							<span class="help-inline" style="color:red">(*)</span>
						</div>
					</div>
					<!-- akhir input kolom tanggal mulai rash -->


					<!-- awal input kolom gejala lain -->
					<div class="control-group">
						<label class="control-label">Gejala lain</label>
						<div class="controls">
							<table>
								<?php
								$dg = array();
								$gejala = Campak::getDaftarGejala($hasil->campak_id_campak);
								foreach ($gejala as $key => $val) {
									$dg[] = array(
										'id_gejala'=>$val->id_daftar_gejala_campak,
										'tgl_sakit'=>$val->tgl_mulai
										);
								}
								?>
								<?php $daftargejala = Campak::DaftarGejala();?>
								@foreach($daftargejala as $row)
								<?php
								$checked = $tgl_sakit = '';
								$disabled='disabled';
								foreach ($dg as $k => $v) {
									if ($v['id_gejala']==$row->id) {
										$checked='checked';
										$tgl_sakit=$v['tgl_sakit'];
										$disabled = '';
									}
								}
								?>
								<tr>
									<td>
										<input type="checkbox" {{$checked}} value="{{$row->id}}" name="gejala_lain[]" class="gejala_lain_{{$row->id}}" onclick="check({{$row->id}})">
										{{$row->nama}}
									</td>
									<td>
										<input type="text" class="input-medium tgl_gejala_lain_{{$row->id}}" {{$disabled}} value="{{Helper::getDate($tgl_sakit)}}" name="tanggal_gejala_lain[]" data-uk-datepicker='{format:"DD-MM-YYYY"}'>
									</td>
								</tr>
								@endforeach
							</table>
							<span class="help-inline"></span>
						</div>
					</div>
					<!-- akhir input kolom gejala lain -->
					<?php
					$komplikasi=DB::table('komplikasi')->where('id_campak',$hasil->campak_id_campak)->get();
					$other = null;
					foreach ($komplikasi as $k => $v) {
						$c[$v->komplikasi] = $v->komplikasi;
						if(!in_array($v->komplikasi,['0','1','2','3','4'])){
							$other = $v->komplikasi;
						}
					}
					?>
					<!-- awal input kolom komplikasi -->
					<div class="control-group">
						<label class="control-label">Komplikasi</label>
						<div class="controls">
							<table>
								<tr>
									<td>
										<input type="checkbox" name="komplikasi[]" value="0" <?php if(isset($c[0]))  echo ($c[0]=='0') ? 'checked' : '' ;?> >&nbsp;Diare
									</td>
								</tr>
								<tr>
									<td>
										<input type="checkbox" name="komplikasi[]" value="1" <?php if(isset($c[1]))  echo ($c[1]=='1') ? 'checked' : '' ;?>>&nbsp;Pneumonia
									</td>
								</tr>
								<tr>
									<td>
										<input type="checkbox" name="komplikasi[]" value="2" <?php if(isset($c[2])) echo ($c[2]=='2') ? 'checked' : '' ;?>>&nbsp;Bronchopneumonia

									</td>
								</tr>
								<tr>
									<td>
										<input type="checkbox" name="komplikasi[]" value="3" <?php if(isset($c[3])) echo ($c[3]=='3') ? 'checked' : '' ;?>>&nbsp;OMA
									</td>
								</tr>
								<tr>
									<td>
										<input type="checkbox" name="komplikasi[]" value="4" <?php if(isset($c[4])) echo ($c[4]=='4') ? 'checked' : '' ;?>>&nbsp;Ensefalitis
									</td>
								</tr>
								<tr>
									<td>
										<input type="checkbox" class="gejala_lain_komplikasi" onclick="chek_komplikasi_lain()" <?php if(isset($other)) echo ($other) ? 'checked' : '' ;?>>&nbsp;Lain-lain (sebutkan)
									</td>
									<td>
										<input type="text" name="komplikasi[]" class="input-medium komplikasi_lainnya" <?php $attr=(isset($other))?'':"disabled='disabled'" ;echo $attr;?> value='<?php echo $other;?>'>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<!-- akhir input kolom gejala lain -->
					<!-- awal input kolom tanggal laporan diterima -->
					<div class="control-group">
						<label class="control-label">Tanggal laporan diterima</label>
						<div class="controls">
							{{Form::text(
								'tanggal_laporan_diterima',
								Helper::getDate($hasil->campak_tanggal_laporan_diterima),
								array(
									'class'=>'input-medium',
									'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
									)
								)
							}}
							<span class="help-inline"></span>
						</div>
					</div>
					<!-- akhir input kolom tanggal laporan diterima -->

					<!-- awal input kolom tanggal pelacakan -->
					<div class="control-group">
						<label class="control-label">Tanggal pelacakan</label>
						<div class="controls">
							{{
								Form::text
								(
									'tanggal_pelacakan',
									Helper::getDate($hasil->campak_tanggal_pelacakan),
									array
									(
										'class'=>'input-medium',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
										)
									)
								}}
								<span class="help-inline"></span>
							</div>
						</div>
						<!-- akhir input kolom tanggal pelacakan -->

						<!-- awal input kolom diberi vitamin A -->
						<div class="control-group">
							<label class="control-label">Diberi vitamin A</label>
							<div class="controls">
								{{Form::select(
									'vitamin_A',
									[
									'0' => 'Pilih',
									'1' => 'Ya',
									'2'=>'Tidak'
									],
									$hasil->campak_vitamin_A,
									array(
										'class' => ' id_combobox')
									)
								}}
								<span class="help-inline"></span>
							</div>
						</div>
						<!-- akhir input kolom diberi vitamin A -->

						<!-- awal input kolom keadaan akhir -->
						<div class="control-group">
							<label class="control-label">Keadaan akhir</label>
							<div class="controls">
								{{Form::select(
									'keadaan_akhir',
									[
									'0' => 'Pilih',
									'1' => 'Hidup/Sehat',
									'2'=>'Meninggal'
									],
									$hasil->campak_keadaan_akhir,
									array('class' => ' id_combobox'))}}
									<span class="help-inline"></span>
								</div>
							</div>
							<!-- akhir input kolom keadaan akhir -->

							<div class="control-group">
								<label class="control-label">Jenis kasus</label>
								<div class="controls">
									{{Form::select(
										'jenis_kasus',
										array(
											'0' => 'Pilih',
											'1' => 'KLB',
											'2'=>'Bukan KLB'),
										$hasil->campak_jenis_kasus,
										array(
											'class' => 'input-medium id_combobox',
											'id' => 'jenis_kasus',
											'onchange'=>'jenisKasus()',
											'data-validation'=>'[MIXED]',
											'data'=>'$ wajib di isi!',
											'data-validation-message'=>'Jenis kasus harus di isi',
											)
										)
									}}
									<span class="help-inline" style="color:red">(*)</span>
								</div>
							</div>

							<div class="control-group">
								<label class="control-label">KLB Ke</label>
								<div class="controls">
									{{Form::select(
										'klb_ke',
										array(
											'0' => 'Pilih',
											'KI' => 'KI',
											'KII' => 'KII',
											'KIII' => 'KIII',
											'KIV' => 'KIV',
											'KV' => 'KV',
											'KVI' => 'KVI',
											'KVII' => 'KVII',
											'KVIII' => 'KVIII',
											'KIX' => 'KIX',
											'KX' => 'KX',
											'KXI' => 'KXI',
											'KXII' => 'KXII'
											),
										$hasil->campak_klb_ke,
										array(
											'class' => 'input-medium id_combobox',
											'id'=>'klb_ke',
											'disabled'=>'disabled',
											'data-validation'=>'[MIXED]',
											'data'=>'$ wajib di isi!',
											'data-validation-message'=>'KLB ke- harus di isi',
											'onchange'=>"showEpidKLB('_pasien')"
											)
										)
									}}
									<span class="help-inline" style="color:red">(*)</span>
								</div>
							</div>

							<!-- awal input kolom status kasus -->
							<div class="control-group">
								<label class="control-label">Status kasus</label>
								<div class="controls">
									{{Form::select(
										'status_kasus',
										[
										'0' => 'Pilih',
										'1' => 'Index',
										'2'=>'Bukan index'
										],
										$hasil->campak_status_kasus,
										array(
											'class' => 'id_combobox'
											)
										)
									}}
									<span class="help-inline"></span>
								</div>
							</div>
							<!-- akhir input kolom status kasus -->
						</fieldset>
						<!-- akhir fieldset-->

						<!-- awal fieldset -->
						<fieldset>
							<legend>Data spesimen dan laboratorium</legend>
							<!-- awal input kolom Pemeriksaan Serologi -->
							<!-- daftar sampel -->
							<table class="table table-bordered" id="data_sampel">
								<tr>
									<td>Nama pemeriksaan</td>
									<td>Jenis sampel</td>
									<td>Tanggal ambil sampel</td>
									<td>aksi</td>
								</tr>
								<?php $data=DB::table('uji_spesimen')->where('id_campak','=',$hasil->campak_id_campak)->get(); ?>
								@foreach($data as $row)
								<?php
								if ($row->jenis_pemeriksaan==0) {
									$jenis_pemeriksaan_baru='Serologi';
									$jenis_sampel_baru= ($row->jenis_sampel==0)?'Darah':'Urin';
								}
								if ($row->jenis_pemeriksaan==1) {
									$jenis_pemeriksaan_baru='Virologi';
									if ($row->jenis_sampel==0) {
										$jenis_sampel_baru='Urin';
									} elseif($row->jenis_sampel==1) {
										$jenis_sampel_baru ='Usap tenggorokan';
									}else
									{
										$jenis_sampel_baru='Cairan mulut';
									}

								}
								?>
								<tr>
									<td><input  name="nama_sampel[]" type="hidden" value="{{$row->jenis_pemeriksaan}}">{{$jenis_pemeriksaan_baru}}</td>
									<td><input  name="jenis_sampel[]" type="hidden" value="{{$row->jenis_sampel}}">{{$jenis_sampel_baru}}</td>
									<td><input  name="tanggal_ambil_sampel[]" type="hidden" value="{{date('d-m-Y',strtotime($row->tgl_ambil_sampel))}}">{{date('d-m-Y',strtotime($row->tgl_ambil_sampel))}}</td>
									<td><a href="javascript:void(0);" class="remCF">Remove</a></td>
								</tr>
								@endforeach
							</table>
							<!-- akhir daftar sampel-->
						</br>
						<a class="btn btn-success" onclick="tambah_sampel()" id="tambah_sampel">tambah sampel</a>
					</br>
					<hr>

					<div id="div_sampel">
						<table class="table table-bordered" id="data_sampel">
							<tr>
								<td>Nama pemeriksaan</td>
								<td>Jenis sampel</td>
								<td>Tanggal ambil sampel</td>
							</tr>
							<tr>
								<td>
									<select id="nama_sampel" onchange="pilih_sampel()">
										<option value="">Pilih</option>
										<option value="0">Serologi</option>
										<option value="1">Virologi</option>
									</select>
								</td>
								<td>
									<select id="jenis_sampel" onchange="jenissampel()">
										<option value="">Pilih</option>
									</select>
								</td>
								<td>
									{{
										Form::text(
											'',
											Input::old('tanggal_ambil_sampel'),
											array(
												'class'=>'input-medium',
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												'id'=>'tanggal_ambil_sampel'
												)
											)
										}}
									</td>
								</tr>
							</table>
							<div id="tombol"style="margin-left:330px;margin-top:30px">
								<a href="javascript:void(0);" class="simpan btn btn-primary" id="simpan">tambah</a>
								<a href="javascript:void(0);" class="btn btn-warning" id="close">tutup</a>
							</div>
						</div>
						<br>
						<!-- akhir input kolom Pemeriksaan Serologi -->
						@if(Sentry::getUser()->hak_akses==4)
						<div class="control-group">
							<label class="control-label">Hasil Laboratorium</label>
							<div class="controls">
								<table>
									<tr>Hasil Laboratorium Serologi</tr>
									<tr>
										<td>IgM Campak</td>
										<td>
											{{Form::select(
												'hasil_serologi_igm_campak',
												[
												'0' => 'Pilih',
												'1' => 'Positif',
												'2'=>'Negatif',
												'3'=>'Equivocal',
												'4'=>'pending'],
												$hasil->campak_hasil_serologi_igm_campak,
												array(
													'class' => 'id_combobox'
													))
												}}
											</td>
										</tr>
										<tr>
											<td>IgM Rubella</td>
											<td>
												{{Form::select(
													'hasil_serologi_igm_rubella',
													[
													'0' => 'Pilih',
													'1' => 'Positif',
													'2'=>'Negatif',
													'3'=>'Equivocal',
													'4'=>'Pending'],
													$hasil->campak_hasil_serologi_igm_rubella,
													array(
														'class' => 'id_combobox')
													)
												}}
											</td>
										</tr>
									</table>
								</div>
							</div>
							@endif

							<!-- akhir input kolom Pemeriksaan virologi -->
							@if(Sentry::getUser()->hak_akses==4)
							<div class="control-group">
								<label class="control-label">Hasil Laboratorium</label>
								<div class="controls">
									<table>
										<tr>Hasil Laboratorium Virologi</tr>
										<tr>
											<td>IgM Campak</td>
											<td>
												{{Form::select(
													'hasil_virologi_igm_campak',
													[
													'0' => 'Pilih',
													'1' => 'Positif',
													'2'=>'Negatif',
													'3'=>'pending'],
													$hasil->campak_hasil_virologi_igm_campak,
													array(
														'class' => 'id_combobox'
														)
													)
												}}
											</td>
										</tr>
										<tr>
											<td>IgM Rubella</td>
											<td>
												{{Form::select(
													'hasil_virologi_igm_rubella',
													[
													'0' => 'Pilih',
													'1' => 'Positif',
													'2'=>'Negatif',
													'3'=>'pending'],
													$hasil->campak_hasil_virologi_igm_rubella,
													array(
														'class' => 'id_combobox'
														)
													)
												}}
											</td>
										</tr>
									</table>
								</div>
							</div>
							<!-- akhir input kolom hasil laboratorium -->
							@endif
							@if(Sentry::getUser()->hak_akses==4)
							<!-- awal input kolom klasifikasi final -->
							<div class="control-group">
								<label class="control-label">Klasifikasi final</label>
								<div class="controls">
									{{Form::select(
										'klasifikasi_final',
										[
										'0' => 'Pilih',
										'1' => 'Campak (Lab)',
										'2'=>'Campak (Epid)',
										'3'=>'Campak (Klinis)',
										'4'=>'Rubella',
										'5'=>'Bukan campak/rubella',
										'6'=>'Pending'
										],
										$hasil->campak_klasifikasi_final,
										array(
											'class' => ' id_combobox'
											)
										)
									}}
									<span class="help-inline"></span>
								</div>
							</div>
							<!-- akhir input kolom klasifikasi final -->
							@endif

						</fieldset>
						<!-- akhir class fieldset -->
						<div class="form-actions">
							{{Form::submit(
								'simpan',
								array(
									'class'=>'btn btn-success'
									)
								)
							}}
							<a href="{{URL::to('campak')}}" class="btn btn-primary">batal</a>
						</div>
						{{Form::close()}}
						<!-- akhir form -->

					</div>
					<!-- akhir class module-body -->
				</div>
				@endforeach
				<input type="hidden" value="<?php echo Sentry::getUser()->hak_akses;?>" id='hakakses'>

				<!-- akhir class modul -->
				@include('pemeriksaan.campak.analisis-js')
				<script>
					$(document).ready(function()
					{
						$('.noKLB').hide();
						var noklb = "<?php echo $hasil->campak_no_epid_klb; ?>";
						if(noklb != ''){
							$('#klb_ke').removeAttr('disabled');
							$('.noKLB').show();
						}
						$("select").select2();
						$('#div_sampel').hide();
						$('#jenis_sampel').attr('disabled','disabled');
						$('#simpan').attr('disabled','disabled');
						$('#tanggal_ambil_sampel').attr('disabled','disabled');
						showKabupaten();
						showKecamatan();
						showKelurahan();

						var hakakses = $('#hakakses').val();
						if (hakakses == '4') {
							$('.akseskab').removeAttr('readonly');
						};

					});
					function tambah_sampel(){
						$('#div_sampel').show(2000);
						$('#tambah_sampel').hide();
					}

					function tutup_sampel(){
						$('#div_sampel').hide(2000);
						$('#tambah_sampel').show(2000);
						$('#jenis_sampel').val('');
						$('#tanggal_ambil_sampel').val('');
						$('#jenis_sampel').attr('disabled','disabled');
						$('#simpan').attr('disabled','disabled');
						$('#tanggal_ambil_sampel').attr('disabled','disabled');

					}

					$( "#id_kelurahan" ).change(function() {
						$.ajax({
							data:'id_kelurahan='+$('#id_kelurahan').val()+'&date='+$('.tgl_mulai_rash').val(),
							url:'{{ URL::to("ambil_epid_campak") }}',
							success:function(data) {
								$('#epid').val(data);
							}
						});
					});

					function add_sampel()
					{
						var nama_sampel             = $("#nama_sampel").val();
						var jenis_sampel            = $("#jenis_sampel").val();
						var tanggal_ambil_sampel    = $("#tanggal_ambil_sampel").val();
						if(nama_sampel=='0')
						{
							var nama_sampel_baru = 'Serologi';
							var jenis_sampel_baru=(jenis_sampel==0)?'Darah':'Urin';
						}

						if(nama_sampel=='1')
						{
							var nama_sampel_baru = 'Virologi';
							if (jenis_sampel==0) {
								var jenis_sampel_baru='Urin';
							}
							else if(jenis_sampel==1)
							{
								var jenis_sampel_baru='Usap tenggorokan';
							}
							else
							{
								var jenis_sampel_baru ='Cairan mulut';
							}
						}

						$("#data_sampel").append('<tr valign="top"><td><input id="data_nama_sampel" name="nama_sampel[]" type="hidden" value='+nama_sampel+'>'+nama_sampel_baru+'</td><td><input id="data_jenis_sampel" name="jenis_sampel[]" type="hidden" value='+jenis_sampel+'>'+jenis_sampel_baru+'</td><td><input id="data_tanggal_ambil_sampel" name="tanggal_ambil_sampel[]" type="hidden" value='+tanggal_ambil_sampel+'>'+tanggal_ambil_sampel+'</td><td><a href="javascript:void(0);" class="remCF">Remove</a></td></tr>');
					}

			//fungsi js untuk mendapatkan no_epid campak scara otomatis
			function showEpidCampak() {

				$.ajax({
					data:'id_kelurahan='+$('#id_kelurahan').val()+'&date='+$('.tgl_mulai_rash').val(),
					url:'{{URL::to("ambil_epid_campak")}}',
					success:function(data) {

						$('#epid').val(data);
					}
				});

			}

			function pilih_sampel()
			{
				if($('#nama_sampel').val()=='0')
				{
					$('#jenis_sampel').html('');
					$('#jenis_sampel').append('<option value="">Pilih</option><option value="0">Darah</option><option value="1">Urin</option>');
					$('#jenis_sampel').removeAttr('disabled','');
				}
				else if($('#nama_sampel').val()=='1')
				{
					$('#jenis_sampel').html('');
					$('#jenis_sampel').removeAttr('disabled','');
					$('#jenis_sampel').append('<option value="">Pilih</option><option value="0">Urin</option><option value="1">Usap tenggorokan</option><option value="2">Cairan mulut</option>');
				}
				else if($('#nama_sampel').val()=='')
				{
					$('#jenis_sampel').attr('disabled','disabled');
					$('#jenis_sampel').html('');
				}
			}

			function jenissampel()
			{
				if ($('#jenis_sampel').val()=='') {
					$('#tanggal_ambil_sampel').attr('disabled','disabled');
					$('#simpan').attr('disabled','disabled');
				}
				else
				{
					$('#tanggal_ambil_sampel').removeAttr('disabled','');
				}

			}

			$(document).ready(function(){
				$('#id_kelurahan').change(function(){
					var nama_anak  = $('#nama_anak').val();
					var nik  = $('#nik').val();
					var nama_ortu  = $('#nama_ortu').val();
					var alamat = $(this).val();
					$.post('{{URL::to("cekPasien")}}', {nama_anak:nama_anak,nik:nik,nama_ortu:nama_ortu,id_kelurahan:alamat}, function(response){
						if(response){
							if(response.sent.status=='1'){
								alert(response.sent.message);
							};
						};
					});
				});

				$("#tanggal_ambil_sampel").change(function(){
					$('#simpan').removeAttr('disabled','');
				});
				$("#data_sampel").on('click','.remCF',function(){
					$(this).parent().parent().remove();
				});
				$("#tombol").on('click','.simpan',function(){

					var nama_sampel             = $("#nama_sampel").val();
					var jenis_sampel            = $("#jenis_sampel").val();
					var tanggal_ambil_sampel    = $("#tanggal_ambil_sampel").val();
					if(nama_sampel=='0')
					{
						var nama_sampel_baru = 'Serologi';
						var jenis_sampel_baru=(jenis_sampel==0)?'Darah':'Urin';
					}

					if(nama_sampel=='1')
					{
						var nama_sampel_baru = 'Virologi';
						if (jenis_sampel==0) {
							var jenis_sampel_baru='Urin';
						}
						else if(jenis_sampel==1)
						{
							var jenis_sampel_baru='Usap tenggorokan';
						}
						else
						{
							var jenis_sampel_baru ='Cairan mulut';
						}
					}

					$("#data_sampel").append('<tr valign="top"><td><input id="data_nama_sampel" name="nama_sampel[]" type="hidden" value='+nama_sampel+'>'+nama_sampel_baru+'</td><td><input id="data_jenis_sampel" name="jenis_sampel[]" type="hidden" value='+jenis_sampel+'>'+jenis_sampel_baru+'</td><td><input id="data_tanggal_ambil_sampel" name="tanggal_ambil_sampel[]" type="hidden" value='+tanggal_ambil_sampel+'>'+tanggal_ambil_sampel+'</td><td><a href="javascript:void(0);" class="remCF">Remove</a></td></tr>');
					$("#nama_sampel").select2("val","");
					$("#jenis_sampel").select2("val","");
					$("#tanggal_ambil_sampel").val('');

				});
				$('#tombol').on('click','#close',function(){
					$('#div_sampel').hide(2000);
					$('#tambah_sampel').show(2000);
					$('#jenis_sampel').val('');
					$('#tanggal_ambil_sampel').val('');
					$('#jenis_sampel').attr('disabled','disabled');
					$('#simpan').attr('disabled','disabled');
					$('#tanggal_ambil_sampel').attr('disabled','disabled');
				});
			});

			function showKabupaten(){
				var provinsi = $("#id_provinsi").val();
				var kabupaten = $("#id_kabupaten").val();
			// //kirim data ke server
			$('.lodingKab').html('<img src="{{URL::to("style/images/loding.gif")}}">');
			$.post('{{URL::to("provinsi/getkab")}}', {provinsi:provinsi,kabupaten:kabupaten}, function(response)
			{
				$('#id_kabupaten').removeAttr('disabled','');
				$("#id_kabupaten").html(response);
				$('.lodingKab').html('');
			});

		}

		function showKecamatan(){
			var kabupaten = $("#id_kabupaten").val();
			var kecamatan = $("#id_kecamatan").val();
			// //kirim data ke server
			$('.lodingKec').html('<img src="{{URL::to("style/images/loding.gif")}}">');
			$.post('{{URL::to("provinsi/getkec")}}', {kabupaten:kabupaten,kecamatan:kecamatan}, function(response)
			{
				$('#id_kecamatan').removeAttr('disabled','');
				$("#id_kecamatan").html(response);
				$('.lodingKec').html('');
			});
		}
		function showKelurahan(){
			var kecamatan= $("#id_kecamatan").val();
			var kelurahan = $("#id_kelurahan").val();
			// //kirim data ke server
			$('.lodingKel').html('<img src="{{URL::to("style/images/loding.gif")}}">');
			$.post('{{URL::to("provinsi/getkel")}}', {kecamatan:kecamatan,kelurahan:kelurahan}, function(response)
			{
				$('#id_kelurahan').removeAttr('disabled','');
				$("#id_kelurahan").html(response);
				$('.lodingKel').html('');
			});
		}
	</script>
</div>
<!-- akhir class content -->
</div>
<!-- akhir class span12 -->
@stop
