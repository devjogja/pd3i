<?php
$penyakit = Session::get('penyakit');
$arr_penyakit['Campak'] = "Campak";
$arr_penyakit['AFP'] = "Polio";
$arr_penyakit['Difteri'] = "Difteri";
$arr_penyakit['Tetanus'] = "Tetanus Neonatorum";
$arr_penyakit['CRS'] = "CRS (Congenital Rubella Syndrom)";
?>
<div class="tab-pane fade in" id="analisis">
	<div class="module">
	<!--
	<div class="module-head">
		<h3>Filter</h3>
	</div>
	-->
	<div class="module-body">
		<form id="search2" method="post" action="{{URL::to('region/get_chart_campak_js')}}">
			<div class="chart inline-legend grid">
				@if(Session::get('type')=='rs')
                    @include('pemeriksaan._include.filter_analisis_rs')
                @elseif(Session::get('type')=='puskesmas')
                    @include('pemeriksaan._include.filter_analisis_puskesmas')
                @else
                    @include('pemeriksaan._include.filter_analisis')
                @endif
				<center>
					<input id="tampilkan_analisa" type="submit" value="Tampilkan" class="btn btn-success">
					<a href="#" id='export_analisis' disabled='disabled' class="btn btn-success">Export Excel</a>
				</center>
			</div>
		</form>
	</div>
</div>
<div class="module">
	<div class="module-head">
		<h3 style="float:left;">Grafik Penderita <?php if($penyakit) echo $arr_penyakit[$penyakit]; ?> Berdasar Jenis Kelamin</h3>
		<!-- <div style="float:right;">
			<a href="{{URL::to('download/jenis_kelamin/excel')}}"><img src="{{URL::to('asset/images/excel.png')}}" style="width:20px;"></a>
		</div> -->
		<div style="clear:both;"></div>
	</div>
	<div class="module-body">
		<div class="chart inline-legend grid" id="container_jk">
			Silahkan Klik tombol tampilkan di atas !
		</div>
	</div>
</div>
<!--/.module-->
<br />
<?php if($penyakit != 'CRS'):?>
<div class="module">
	<div class="module-head">
		<h3 style="float:left;">Grafik Penderita <?php if($penyakit) echo $arr_penyakit[$penyakit]; ?> Berdasar Waktu</h3>
		<!-- <div style="float:right;">
			<a href="{{URL::to('download/waktu/excel')}}"><img src="{{URL::to('asset/images/excel.png')}}" style="width:20px;"></a>
		</div> -->
		<div style="clear:both;"></div>
	</div>
	<div class="module-body">
		<div class="chart inline-legend grid" id="container_waktu">
			Silahkan Klik tombol tampilkan di atas !
		</div>
	</div>
</div>
<br />
<?php endif?>
<div class="module">
	<div class="module-head">
		<h3 style="float:left;">Grafik Penderita <?php if($penyakit) echo $arr_penyakit[$penyakit]; ?> Berdasar Umur</h3>
		<!-- <div style="float:right;">
			<a href="{{URL::to('download/umur/excel')}}"><img src="{{URL::to('asset/images/excel.png')}}" style="width:20px;"></a>
		</div> -->
		<div style="clear:both;"></div>
	</div>
	<div class="module-body">
		<div class="chart inline-legend grid" id="container_umur">
			Silahkan Klik tombol tampilkan di atas !
		</div>
	</div>
</div>
<br />
<?php if($penyakit != 'CRS'):?>
<div class="module">
	<div class="module-head">
		<h3 style="float:left;">Grafik Penderita <?php if($penyakit) echo $arr_penyakit[$penyakit]; ?> Berdasar Status Imunisasi</h3>
		<!-- <div style="float:right;">
			<a href="{{URL::to('download/imunisasi/excel')}}"><img src="{{URL::to('asset/images/excel.png')}}" style="width:20px;"></a>
		</div> -->
		<div style="clear:both;"></div>
	</div>
	<div class="module-body">
		<div class="chart inline-legend grid" id="container_imunisasi">
			Silahkan Klik tombol tampilkan di atas !
		</div>
	</div>
</div>
<br />
<?php endif?>
<div class="module">
	<div class="module-head">
		<h3 style="float:left;">Grafik Penderita <?php if($penyakit) echo $arr_penyakit[$penyakit]; ?> Berdasar Klasifikasi Final</h3>
		<!-- <div style="float:right;">
			<a href="{{URL::to('download/klasifikasi_final/excel')}}"><img src="{{URL::to('asset/images/excel.png')}}" style="width:20px;"></a>
		</div> -->
		<div style="clear:both;"></div>
	</div>
	<div class="module-body">
		<div class="chart inline-legend grid" id="container_klasifikasi_final">
			<center>Silahkan Klik tombol tampilkan di atas !<center>
		</div>
	</div>
</div>
<br />
<?php if($penyakit != 'CRS'):?>
<div class="module">
	<div class="module-head">
		<div class="module-head">
			<h3 style="float:left;">Peta Penderita <?php if($penyakit) echo $arr_penyakit[$penyakit]; ?></h3>
			<!-- <div style="float:right;">
				<input type="hidden" id="last_maps_id" name="last_maps_id" value="0">
				<input type="hidden" id="max_maps_id" name="max_maps_id" value="50">
				<a href="{{URL::to('download/wilayah/excel')}}"><img src="{{URL::to('asset/images/excel.png')}}" style="width:20px;"></a>
			</div> -->
			<div style="clear:both;"></div>
		</div>
	</div>
	<div class="module-body">
		<div class="chart inline-legend grid" id="map_dashboard" style="height:600px;width:100%;">
			Silahkan Klik tombol tampilkan di atas !
		</div>
	</div>
</div>
<br />
<?php endif?>
<?php if($penyakit == "Campak"): ?> @include('pemeriksaan.campak.analisa_itungan_campak')
<?php elseif($penyakit == "AFP"): ?> @include('pemeriksaan.afp.analisa_itungan_afp')
<?php elseif($penyakit == "Difteri"): ?> @include('pemeriksaan.difteri.analisa_itungan_difteri')
<?php elseif($penyakit == "Tetanus"): ?> @include('pemeriksaan.tetanus.analisa_itungan_tetanus')
<?php elseif($penyakit == "CRS"): ?> @include('pemeriksaan.crs.analisa_itungan_crs')
<?php endif; ?>
</div>

<script type="text/javascript">
	$('#export_analisis').click(function(e){ 
	    e.preventDefault(); 
	    var params = JSON.stringify($('#search2').serializeArray());
	    var url = "{{URL::to('export/'.strtolower(Session::get('penyakit')).'/xls')}}"+'/'+params;
	    window.open(url, '_blank');
	});
</script>