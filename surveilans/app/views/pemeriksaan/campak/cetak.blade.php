<html>

<body style="font-size:12px;">

<br>
<table  align="center" width="600" style="border-collapse: collapse;">

<tr>
	<td colspan="2" align="center">
		<h2>Laporan Kasus Campak Individu</h2>
		<h5><hr/></h5>
	</td>
</tr>

</table>
<table width="100%" border="1" style="border-collapse: collapse">
  <tr style="background-color: #f1f1c1;">
    <td rowspan="3">No. Epidemologi</td>
    <td rowspan="3">Nama Anak</td>
    <td rowspan="3">Nama Orang Tua</td>
    <td rowspan="3">Alamat Lengkap Desa/RT/RW</td>
    <td colspan="2">Umur/Sex</td>
    <td colspan="2" rowspan="2">Vaksin Campak Sebelum Sakit</td>
    <td colspan="2" rowspan="2">Tanggal Timbul</td>
    <td colspan="2" rowspan="2">Tanggal Diambil Spesimen</td>
    <td colspan="2" rowspan="2">Hasil Spesimen</td>
    <td rowspan="2">Diberi Vitamin A</td>
    <td rowspan="2">Keadaan Akhir</td>
    <td colspan="5">Klasifikasi Final</td>
    <td colspan="2">Tanggal</td>
  </tr>
  <tr style="background-color: #f1f1c1;">
    <td rowspan="2">L</td>
    <td rowspan="2">P</td>
    <td colspan="3">Campak</td>
    <td>Rubella</td>
    <td>Bukan</td>
    <td rowspan="2">Laporan Diterima</td>
    <td rowspan="2">Pelacakan</td>
  </tr>
  <tr style="background-color: #f1f1c1;">
    <td>Berapa kali</td>
    <td>Tidak/Tidak Tahu</td>
    <td>Demam</td>
    <td>Rash</td>
    <td>Serum</td>
    <td>Urin</td>
    <td>Serum</td>
    <td>Urin</td>
    <td>Y/T</td>
    <td>(H/M)</td>
    <td>Lab</td>
    <td>Epid</td>
    <td>Klinis</td>
    <td>Lab</td>
    <td>Campak/Rubella</td>
  </tr>
  <tr style="background-color: #f1f100;">
    <td>1</td>
    <td>2</td>
    <td>3</td>
    <td>4</td>
    <td>5</td>
    <td>6</td>
    <td>7</td>
    <td>8</td>
    <td>9</td>
    <td>10</td>
    <td>11</td>
    <td>12</td>
    <td>13</td>
    <td>14</td>
    <td>15</td>
    <td>16</td>
    <td>17</td>
    <td>18</td>
    <td>19</td>
    <td>20</td>
    <td>21</td>
    <td>22</td>
    <td>23</td>
  </tr>
  <?php $data = Campak::pasiencampak();?>
  @if($data)
  @foreach($data as $row)
  <tr>
    <td>{{$row->no_epid}}</td>
    <td>{{$row->nama_anak}}</td>
    <td>{{$row->nama_ortu}}</td>
    <td>{{$row->alamat}}</td>
    @if($row->jenis_kelamin==1)
    <td>{{$row->umur}}</td>
    <td></td>
    @else
    <td></td>
    <td>{{$row->umur}}</td>
    @endif
    @if($row->vaksin_campak_sebelum_sakit=='belum pernah' or $row->vaksin_campak_sebelum_sakit=='tidak tahu')
    <td></td>
    <td>{{$row->vaksin_campak_sebelum_sakit}}</td>
    @else
    <td>{{$row->vaksin_campak_sebelum_sakit}}</td>
    <td></td>
    @endif
    <td>{{$row->tanggal_timbul_demam}}</td>
    <td>{{$row->tanggal_timbul_rash}}</td>
    <td>{{$row->tanggal_diambil_spesimen_darah}}</td>
    <td>{{$row->tanggal_diambil_spesimen_urin}}</td>
    <td>{{$row->hasil_spesimen_darah}}</td>
    <td>{{$row->hasil_spesimen_urin}}</td>
    <td>{{$row->vitamin_A}}</td>
    <td>{{$row->keadaan_akhir}}</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>{{$row->tanggal_laporan_diterima}}</td>
    <td>{{$row->tanggal_pelacakan}}</td>
  </tr>
  @endforeach
  @endif
</table>
	
</body>
</html>