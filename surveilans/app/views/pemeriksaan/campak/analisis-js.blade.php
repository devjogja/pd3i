<script type="text/javascript">
function general_spesifikx(val){
    if(val == 's') {
        $('#province_id').removeAttr('disabled');
        $('#province_id_analisis').removeAttr('disabled');
    }
    if(val == 'g') { 
        $('#province_id').removeAttr('disabled'); 
        $('#province_id_analisis').removeAttr('disabled'); 
        $('#district_id').attr('disabled',true); 
        $('#district_id_analisis').attr('disabled',true); 
        $('#sub_district_id').attr('disabled',true); 
        $('#sub_district_id_analisis').attr('disabled',true); 
        $('#village_id').attr('disabled',true); 
        $('#village_id_analisis').attr('disabled',true); 
        $('#puskesmas_id').attr('disabled',true); 
        $('#puskesmas_id_analisis').attr('disabled',true); 
        
        $('#province_id').val('');
        $('#province_id_analisis').val('');
        $('#district_id').val('');
        $('#district_id_analisis').val('');
        $('#sub_district_id').val('');
        $('#sub_district_id_analisis').val('');
        $('#village_id').val('');
        $('#village_id_analisis').val('');
        $('#puskesmas_id').val('');
		$('#puskesmas_id_analisis').val('');
    }
}

$(document).on("shown", 'a[data-toggle="tab"]', function (e) {
	if(
		e.target=='{{URL::to("/")}}/campak#analisis' ||
		e.target=='{{URL::to("/")}}/afp#analisis' ||
		e.target=='{{URL::to("/")}}/tetanus#analisis' ||
        e.target=='{{URL::to("/")}}/difteri#analisis' ||
		e.target=='{{URL::to("/")}}/crs#analisis'
	)
    {
		$('#tampilkan_analisa').click();
		init_map();
	}
});

//fungsi untuk mendapat nama puskesmas daerah wilayah tempat tinggal pasien
function getfaskes()
{
    //ambil isian
    var id_kelurahan = $('#id_kelurahan').val();
    $.ajax({
        //dataType:'json',
        data: 'id_kelurahan='+id_kelurahan,
        url:'{{URL::to("tampil_faskes")}}',
        success:function(data) {
            $('#faskes').html(data);
        }
    });
}



$(document).ready(function(){
    $('#catatan_komplikasi').attr('disabled', 'disabled');
    $('.sakit_dirumah').attr('disabled', 'disabled');
    $('.sakit_disekolah').attr('disabled', 'disabled');
    tampilkan_maps();
    
    $('#search2').submit(function() {
        loading_state2();
        $('#search2').ajaxSubmit({
            type: 'POST',
            dataType:'script',
            success:function(data) {
                //alert('asdasd');
                //$('#test_output').html(data);
            }
        });
        return false;
    });

    $('#data_afp').load('{{URL::to("daftar_pe_afp")}}')
    $('#data_campak').load('{{URL::to("daftar_pe_campak")}}')
    $('#list_campak').load('{{URL::to("data_campak")}}')
    $('#data_difteri').load('{{URL::to("daftar_pe_difteri")}}')
    $('#data_tetanus').load('{{URL::to("daftar_pe_tetanus")}}')
    $('#data_crs').load('{{URL::to("daftar_pe_crs")}}')
    //$('#tampilkan').click();
});


//fungsi untuk menampilkan data campak sesuai date and regional yg dipilih
function tampil_campak() {

    //ambil isian
    var tgl_mulai = $('.tgl_mulai').val();
    var tgl_sampai = $('#tgl_sampai').val();
    $.ajax({
        //dataType:'json',
        data: 'tgl_mulai='+tgl_mulai+'tgl_sampai='+tgl_sampai,
        url:'{{URL::to("tampil_data_campak")}}',
        success:function(data) {
            //alert(data);
            $('#list_campak').html('');
            $('#list_campak').html(data);
        }
    });

    
}

function kembali_afp() {
    $.ajax({
        //dataType:'json',
        //data: 'sub_district_id='+$('#sub_district_id').val(),
        url:'{{URL::to("daftar_pe_afp")}}',
        success:function(data) {
            //alert(data);
            $('#data_afp').html('');
            $('#data_afp').html(data);
        }
    });
}

function entriafp() {
    $.ajax({
        //dataType:'json',
        //data: 'sub_district_id='+$('#sub_district_id').val(),
        url:'{{URL::to("afp/entriafp")}}',
        success:function(data) {
            //alert(data);
            $('#data_afp').html('');
            $('#data_afp').html(data);
            
        }
    });
}

function kembali_campak() {
    $.ajax({
        //dataType:'json',
        //data: 'sub_district_id='+$('#sub_district_id').val(),
        url:'{{URL::to("daftar_pe_campak")}}',
        success:function(data) {
            //alert(data);
            $('#data_campak').html('');
            $('#data_campak').html(data);
        }
    });
}

function entricampak() {
    $.ajax({
        //dataType:'json',
        //data: 'sub_district_id='+$('#sub_district_id').val(),
        url:'{{URL::to("campak/entricampak")}}',
        success:function(data) {
            //alert(data);
            $('#data_campak').html('');
            $('#data_campak').html(data);
            $('.id_entri').remove();
        }
    });
}

function kembali_difteri() {
    $.ajax({
        //dataType:'json',
        //data: 'sub_district_id='+$('#sub_district_id').val(),
        url:'{{URL::to("daftar_pe_difteri")}}',
        success:function(data) {
            //alert(data);
            $('#data_difteri').html('');
            $('#data_difteri').html(data);
        }
    });
}

function entri_difteri() {
    $.ajax({
        //dataType:'json',
        //data: 'sub_district_id='+$('#sub_district_id').val(),
        url:'{{URL::to("difteri/entridifteri")}}',
        success:function(data) {
            //alert(data);
            $('#data_difteri').html('');
            $('#data_difteri').html(data);
            
        }
    });
}

function kembali_crs() {
    $.ajax({
        //dataType:'json',
        //data: 'sub_district_id='+$('#sub_district_id').val(),
        url:'{{URL::to("daftar_pe_crs")}}',
        success:function(data) {
            //alert(data);
            $('#data_crs').html('');
            $('#data_crs').html(data);
        }
    });
}

function entri_crs() {
    $.ajax({
        url:'{{URL::to("crs/entricrs")}}',
        success:function(data) {
            $('#data_crs').html('');
            $('#data_crs').html(data);
        }
    });
}

function kembali_tetanus() {
    $.ajax({
        //dataType:'json',
        //data: 'sub_district_id='+$('#sub_district_id').val(),
        url:'{{URL::to("daftar_pe_tetanus")}}',
        success:function(data) {
            //alert(data);
            $('#data_tetanus').html('');
            $('#data_tetanus').html(data);
        }
    });
}

function entri_tetanus() {
    $.ajax({
        //dataType:'json',
        //data: 'sub_district_id='+$('#sub_district_id').val(),
        url:'{{URL::to("tetanus/entritetanus")}}',
        success:function(data) {
            //alert(data);
            $('#data_tetanus').html('');
            $('#data_tetanus').html(data);
        }
    });
}


function loading_state2(){
    $('#container_jk').html('<img src="{{URL::to("assets/img/loading1.gif")}}" style="width:40px;"><br>Loading...');
    $('#container_waktu').html('<img src="{{URL::to("assets/img/loading1.gif")}}" style="width:40px;"><br>Loading...');
    $('#container_umur').html('<img src="{{URL::to("assets/img/loading1.gif")}}" style="width:40px;"><br>Loading...');
    $('#container_imunisasi').html('<img src="{{URL::to("assets/img/loading1.gif")}}" style="width:40px;"><br>Loading...');
    $('#container_klasifikasi_final').html('<img src="{{URL::to("assets/img/loading1.gif")}}" style="width:40px;"><br>Loading...');
    //$('#map-canvas').html('<img src="{{URL::to("assets/img/loading1.gif")}}" style="width:40px;"><br>Loading...');
}   
    
function getPasien() {
    
    $.ajax({
        //dataType:'json',
        data: 'no_epid='+$('#epid_pe').val(),
        url:'{{route("getPasien")}}',
        success:function(data) {
        $('.id_pasien').val(data.id_pasien);
        $('.nama_anak').val(data.nama_anak);
        $('.alamat').val(data.alamat);        
        $('.tgl_lahir').val(data.tanggal_lahir);
        $('#tanggal_tahun').val(data.umur);
        $('#tanggal_bulan').val(data.umur_bln);
        $('#tanggal_hari').val(data.umur_hr);
        $('.provinsi').val(data.provinsi);
        $('.kabupaten').val(data.kabupaten);
        $('.kecamatan').val(data.kecamatan);
        $('.kelurahan').val(data.kelurahan);

        }
    });
} 

function showKabupaten(dt){
    if (dt==null) {
        dt='';
    }
  var provinsi = $("#id_provinsi"+dt).val();
  // //kirim data ke server
  $('.lodingKab').html('<img src="{{URL::to("style/images/loding.gif")}}">');
  $.post('{{URL::to("provinsi/getkab")}}', {provinsi:provinsi}, function(response)
  {
      $('#id_kabupaten'+dt).removeAttr('disabled','');
      $("#id_kabupaten"+dt).html(response);
      $('.lodingKab').html('');
  });

}

function showKecamatan(dt){
    if (dt==null) {
        dt='';
    }
  var kabupaten = $("#id_kabupaten"+dt).val();
  // //kirim data ke server
  $('.lodingKec').html('<img src="{{URL::to("style/images/loding.gif")}}">');
  $.post('{{URL::to("provinsi/getkec")}}', {kabupaten:kabupaten}, function(response)
  {
      $('#id_kecamatan'+dt).removeAttr('disabled','');
      $("#id_kecamatan"+dt).html(response);
      $('.lodingKec').html('');
  });

}

function showKabu(){
  var provinsi = $("#id_pro").val();
  // //kirim data ke server
  //$('.loading').html("<img src='../public/images/loading.gif' width='35' height='35'>");
  $.post('{{URL::to("provinsi/getkab")}}', {provinsi:provinsi}, function(response)
  { 

      $("#id_ka").html(response);
     // $('.loading').html('');
  });

}



function showKelurahan(dt){
    if (dt==null) {
        dt='';
    }
  var kecamatan= $("#id_kecamatan"+dt).val();
  // //kirim data ke server
  $('.lodingKel').html('<img src="{{URL::to("style/images/loding.gif")}}">');
  $.post('{{URL::to("provinsi/getkel")}}', {kecamatan:kecamatan}, function(response)
  { 
      $('#id_kelurahan'+dt).removeAttr('disabled','');
      $("#id_kelurahan"+dt).html(response);
      $('.lodingKel').html('');
  });

}

function showKab(){
  var provinsi = $("#id_prov").val();
  // //kirim data ke server
  //$('.loading').html("<img src='../public/images/loading.gif' width='35' height='35'>");
  $.post('{{URL::to("provinsi/getkab")}}', {provinsi:provinsi}, function(response)
  {
      $("#id_kab").html(response);
     // $('.loading').html('');
  });

}

function showKec(){
  var kabupaten = $("#id_kab").val();
  // //kirim data ke server
  //$('.loading').html("<img src='../public/images/loading.gif' width='35' height='35'>");
  $.post('{{URL::to("provinsi/getkec")}}', {kabupaten:kabupaten}, function(response)
  {
      $("#id_kec").html(response);
     // $('.loading').html('');
  });

}

function showKel(){
  var kecamatan= $("#id_kec").val();
  // //kirim data ke server
  //$('.loading').html("<img src='../public/images/loading.gif' width='35' height='35'>");
  $.post('{{URL::to("provinsi/getkel")}}', {kecamatan:kecamatan}, function(response)
  {
      $("#id_kel").html(response);
     // $('.loading').html('');
  });

}

function cek_demam() {
    
    var cek =$('input[name=gejala_sakit_demam]').is(':checked');
    if(cek){
        $('.cek2').removeAttr('disabled');
    }
    else {
        $('.cek2').attr('disabled','disabled');
    }
}

function check(id) {
    
    var cek =$('.gejala_lain_'+id).is(':checked');
    if(cek){
        $('.tgl_gejala_lain_'+id).removeAttr('disabled');
    }
    else {
        $('.tgl_gejala_lain_'+id).attr('disabled','disabled');
    }
}

function chek_komplikasi_lain() {
    
    var cek =$('.gejala_lain_komplikasi').is(':checked');
    if(cek){
        $('.komplikasi_lainnya').removeAttr('disabled');
    }
    else {
        $('.komplikasi_lainnya').attr('disabled','disabled').val(null);
    }
}

function kerongkongan() {
    
    var cek =$('input[name=gejala_sakit_kerongkongan]').is(':checked');
    if(cek){
        $('.cek4').removeAttr('disabled');
    }
    else {
        $('.cek4').attr('disabled','disabled');
    }
}

function cek_edit_demam() {
    
    var cek =$('input[name=gejala_sakit_demam]').is(':checked');
    if(cek){
        $('.cek_sakit_demam').removeAttr('disabled');
    }
    else {
        $('.cek_sakit_demam').attr('disabled','disabled');
        $('.cek_sakit_demam').val('');
    }
}

function edit_kerongkongan() {
    
    var cek =$('input[name=gejala_sakit_kerongkongan]').is(':checked');
    if(cek){
        $('.cek_sakit_kerongkongan').removeAttr('disabled');
    }
    else {
        $('.cek_sakit_kerongkongan').attr('disabled','disabled');
        $('.cek_sakit_kerongkongan').val('');
    }
}

function edit_bengkak() {
    
    var cek =$('input[name=gejala_leher_bengkak]').is(':checked');
    if(cek){
        $('.cek_sakit_leher').removeAttr('disabled');
    }
    else {
        $('.cek_sakit_leher').attr('disabled','disabled');
        $('.cek_sakit_leher').val('');
    }
}

function edit_sesak() {
    
    var cek =$('input[name=gejala_sesak_nafas]').is(':checked');
    if(cek){
        $('.cek_sesak').removeAttr('disabled');
    }
    else {
        $('.cek_sesak').attr('disabled','disabled');
        $('.cek_sesak').val('');
    }
}

function edit_membran() {
    
    var cek =$('input[name=gejala_pseudomembran]').is(':checked');
    if(cek){
        $('.cek_membran').removeAttr('disabled');
    }
    else {
        $('.cek_membran').attr('disabled','disabled');
        $('.cek_membran').val('');
    }
}

function edit_lain() {
    
    var cek =$('input[name=gejala]').is(':checked');
    if(cek){
        $('.cek_gejala').removeAttr('disabled');
    }
    else {
        $('.cek_gejala').attr('disabled','disabled');
        $('.cek_gejala').val('');
    }
}


function modal_detail_campak(id) { 

    $('.detail_campak').load('{{URL::to("campak_detail/")}}'+id,function(){

        $('.detail_campak').modal('show');

    });
}

function berpergian() {
    
    var cek =$('input[name=sebelum_sakit_berpergian]').val();
    if(cek=='ya'){
        $('.lokasi').removeAttr('disabled');
        $('.tanggal_pergi').removeAttr('disabled');
    }
    else {
        $('.lokasi').attr('disabled','disabled');
         $('.tanggal_pergi').attr('disabled','disabled');
    }
}

function bengkak() {
    
    var cek =$('input[name=gejala_leher_bengkak]').is(':checked');
    if(cek){
        $('.cek5').removeAttr('disabled');
    }
    else {
        $('.cek5').attr('disabled','disabled');
    }
}

function berobat() {
    $('.id_unit_pelayanan').removeAttr('disabled');
    $('.id_berobat').removeAttr('disabled');
    $('.id_diagnosis').removeAttr('disabled');
    $('.id_rekam_medis').removeAttr('disabled');
}

function tidakberobat() {
    $('.id_unit_pelayanan').attr('disabled','disabled');
    $('.id_berobat').attr('disabled','disabled');
    $('.id_diagnosis').attr('disabled','disabled');
    $('.id_rekam_medis').attr('disabled','disabled');
}

function sesak() {
    
    var cek =$('input[name=gejala_sesak_nafas]').is(':checked');
    if(cek){
        $('.cek7').removeAttr('disabled');
    }
    else {
        $('.cek7').attr('disabled','disabled');
    }
}

function membran() {
    
    var cek =$('input[name=gejala_pseudomembran]').is(':checked');
    if(cek){
        $('.cek9').removeAttr('disabled');
    }
    else {
        $('.cek9').attr('disabled','disabled');
    }
}

function lain() {
    
    var cek =$('input[name=gejala]').is(':checked');
    if(cek){
        $('.cek10').removeAttr('disabled');
    }
    else {
        $('.cek10').attr('disabled','disabled');
    }
}

function cekpergi() {
    
    var cek =$('#id_pergi').val();
    if(cek=='1'){
        $('.cek_pergi').removeAttr('disabled');
    }
    else {
        $('.cek_pergi').attr('disabled','disabled');
    }
}

function cekmeninggal() {
    
    var cek =$('#id_meninggal').val();
    if(cek=='1'){
        $('.cek_meninggal').removeAttr('disabled');
    }
    else {
        $('.cek_meninggal').attr('disabled','disabled');
    }
}
function ceksama() {
    
    var cek =$('#id_sama').val();
    if(cek=='1'){
        $('.cek_sama').removeAttr('disabled');
    }
    else {
        $('.cek_sama').attr('disabled','disabled');
    }
}


function ceklist() {
    
    var id_kom=$('.id_komplikasi').find('option:selected').val();
    if(id_kom==0)
    {
        $('#catatan_komplikasi').removeAttr('disabled');
    }
    else {
        $('#catatan_komplikasi').attr('disabled','disabled');
    }
}
    
function get_district(){
    var type = "<?php echo Session::get('type')?>";
    var kd_faskes = "<?php echo Session::get('kd_faskes')?>";
    var prov = $('#province_id').val();

    if ((type=='provinsi'&&kd_faskes==prov)||(type=='kemenkes')){
        $('#exportall').removeAttr('disabled');
    }else{
        $('#exportall').attr('disabled',true);
    };
    $.ajax({
        //dataType:'json',
        data: 'province_id='+$('#province_id').val(),
        url:'{{URL::to("region/get_district")}}',
        success:function(data) {
            $('#district_id').html(data);
            if($('#province_id').val()=='') {
                $('#district_id').attr('disabled',true);
                $('#sub_district_id').attr('disabled',true);
                $('#village_id').attr('disabled',true);
                $('#puskesmas_id').attr('disabled',true);
            } else {
                $('#district_id').removeAttr('disabled');
            }   
            get_sub_district();
            get_village();
            get_puskesmas();
        }
    });
}   

function get_district_analisis(){
    var type = "<?php echo Session::get('type')?>";
    var kd_faskes = "<?php echo Session::get('kd_faskes')?>";
    var prov = $('#province_id_analisis').val();
    if ((type=='provinsi'&&kd_faskes==prov)||(type=='kemenkes')){
        $('#exportanalis').removeAttr('disabled');
    }else{
        $('#exportanalis').attr('disabled',true);
    };
    $.ajax({
        //dataType:'json',
        data: 'province_id='+$('#province_id_analisis').val(),
        url:'{{URL::to("region/get_district")}}',
        success:function(data) {
            $('#district_id_analisis').html(data);
            if($('#province_id_analisis').val()=='') {
                $('#district_id_analisis').attr('disabled',true);
                $('#sub_district_id_analisis').attr('disabled',true);
                $('#village_id_analisis').attr('disabled',true);
                $('#puskesmas_id_analisis').attr('disabled',true);
            } else {
                $('#district_id_analisis').removeAttr('disabled');
            }   
            get_sub_district_analis();
            get_village_analis();
            get_puskesmas_analis();
        }
    });
}   

function get_sub_district(){
    /*var type = "<?php echo Session::get('type')?>";
    var kd_faskes = "<?php echo Session::get('kd_faskes')?>";
    var kab = $('#district_id').val();
    if ((type=='kemenkes')||(type=='provinsi')||(type=='kabupaten' && kd_faskes==kab)){
        $('#exportall').removeAttr('disabled');
    }else{
        $('#exportall').attr('disabled',true);
    };*/
    $.ajax({
        //dataType:'json',
        data: 'district_id='+$('#district_id').val(),
        url:'{{URL::to("region/get_sub_district")}}',
        success:function(data) {
            //alert(data);
            $('#sub_district_id').html(data);
            if($('#district_id').val()=='') {
                $('#sub_district_id').attr('disabled',true);
                $('#village_id').attr('disabled',true);
				$('#puskesmas_id').attr('disabled',true);
            } else {
                $('#sub_district_id').removeAttr('disabled');
            }
			get_puskesmas();
            get_village();
        }
    });
} 

function get_sub_district_analis(){
    var type = "<?php echo Session::get('type')?>";
    var kd_faskes = "<?php echo Session::get('kd_faskes')?>";
    var kab = $('#district_id_analisis').val();
    if ((type=='kemenkes')||(type=='provinsi')||(type=='kabupaten' && kd_faskes==kab)){
        $('#exportanalis').removeAttr('disabled');
    }else{
        $('#exportanalis').attr('disabled',true);
    };
    $.ajax({
        //dataType:'json',
        data: 'district_id='+$('#district_id_analisis').val(),
        url:'{{URL::to("region/get_sub_district")}}',
        success:function(data) {
            //alert(data);
            $('#sub_district_id_analisis').html(data);
            if($('#district_id_analisis').val()=='') {
                $('#sub_district_id_analisis').attr('disabled',true);
                $('#village_id_analisis').attr('disabled',true);
                $('#puskesmas_id_analisis').attr('disabled',true);
            } else {
                $('#sub_district_id_analisis').removeAttr('disabled');
            }
            get_puskesmas_analis();
            get_village_analis();
        }
    });
} 

function get_village(){
    /*var type = "<?php echo Session::get('type')?>";
    var kd_faskes = "<?php echo Session::get('kd_faskes')?>";
    if ((type=='kemenkes')||(type=='provinsi')||(type=='kabupaten')){
        $('#exportall').removeAttr('disabled');
    }else{
        $('#exportall').attr('disabled',true);
    };*/
    $.ajax({
        //dataType:'json',
        data: 'sub_district_id='+$('#sub_district_id').val(),
        url:'{{URL::to("region/get_village")}}',
        success:function(data) {
            //alert(data);
            $('#village_id').html(data);
            if($('#sub_district_id').val()=='') $('#village_id').attr('disabled',true);
            else {
				$('#village_id').removeAttr('disabled');
				get_puskesmas();
			}
        }
    });
}   

function get_village_analis(){
    var type = "<?php echo Session::get('type')?>";
    var kd_faskes = "<?php echo Session::get('kd_faskes')?>";
    if ((type=='kemenkes')||(type=='provinsi')||(type=='kabupaten')){
        $('#exportanalis').removeAttr('disabled');
    }else{
        $('#exportanalis').attr('disabled',true);
    };
    $.ajax({
        //dataType:'json',
        data: 'sub_district_id='+$('#sub_district_id_analisis').val(),
        url:'{{URL::to("region/get_village")}}',
        success:function(data) {
            //alert(data);
            $('#village_id').html(data);
            if($('#sub_district_id_analisis').val()=='') $('#village_id_analisis').attr('disabled',true);
            else {
                $('#village_id_analisis').removeAttr('disabled');
                get_puskesmas_analis();
            }
        }
    });
}   

function get_puskesmas(){
    /*var type = "<?php echo Session::get('type')?>";
    var kd_faskes = "<?php echo Session::get('kd_faskes')?>";
    var puskesmas = $('#puskesmas_id').val();
    if ((type=='kemenkes')||(type=='provinsi')||(type=='kabupaten')||(type=='puskesmas'&&kd_faskes==puskesmas)){
        $('#exportall').removeAttr('disabled');
    }else{
        $('#exportall').attr('disabled',true);
    };*/
	$.ajax({
		//dataType:'json',
		data: 'sub_district_id='+$('#sub_district_id').val(),
		url:'{{URL::to("region/get_puskesmas")}}',
		success:function(data) {
			//alert(data);
			$('#puskesmas_id').html(data);
			if($('#sub_district_id').val()=='') $('#puskesmas_id').attr('disabled',true);
			else $('#puskesmas_id').removeAttr('disabled');
		}
	});
}

function get_puskesmas_analis(){
    var type = "<?php echo Session::get('type')?>";
    var kd_faskes = "<?php echo Session::get('kd_faskes')?>";
    var puskesmas = $('#puskesmas_id_analisis').val();
    if ((type=='kemenkes')||(type=='provinsi')||(type=='kabupaten')||(type=='puskesmas'&&kd_faskes==puskesmas)){
        $('#exportanalis').removeAttr('disabled');
    }else{
        $('#exportanalis').attr('disabled',true);
    };
    $.ajax({
        //dataType:'json',
        data: 'sub_district_id='+$('#sub_district_id_analisis').val(),
        url:'{{URL::to("region/get_puskesmas")}}',
        success:function(data) {
            //alert(data);
            $('#puskesmas_id_analisis').html(data);
            if($('#sub_district_id_analisis').val()=='') $('#puskesmas_id_analisis').attr('disabled',true);
            else $('#puskesmas_id_analisis').removeAttr('disabled');
        }
    });
}

function tampilkan(){
    $.ajax({
        type:'POST',
        dataType:'script',
        url:'{{URL::to("region/get_chart_campak_js")}}',
        success:function(data) {
            //alert('asdasd');
            //$('#test_output').html(data);
        }
    });
    
}

function umur()
{
  $.ajax({
    data:'tanggal_lahir='+$('.tgl_lahir').val(),
    url:'{{URL::to("hitung/umur")}}',
    success:function(data) {
        $('#tgl_tahun').val(data.tgl_tahun);
        $('#tgl_bulan').val(data.tgl_bulan);
        $('#tgl_hari').val(data.tgl_hari);
    }
  });
}

function tgl_lahir()
{
    var tgl_tahun = $('#tgl_tahun').val();
    var tgl_bulan = $('#tgl_bulan').val();
    var tgl_hari = $('#tgl_hari').val();
    var tgl_sakit = $('.tgl_mulai_sakit').val();
    var tgl_lahir = $('.tgl_lahir').val();
  $.ajax({
    data:'tgl_tahun='+tgl_tahun+'&tgl_bulan='+tgl_bulan+'&tgl_hari='+tgl_hari+'&tanggal_timbul_demam='+tgl_sakit+'&tanggal_lahir='+tgl_lahir,
    url:'{{URL::to("hitung/tgl_lahir")}}',
    success:function(data) {
       if(data.tgl==1) {
            $('.tgl_lahir').val('');
            $('.tgl_lahir').val(data.tgl1);
            $('#tgl_tahun').val(data.tgl_tahun);
            $('#tgl_bulan').val(data.tgl_bulan);
            $('#tgl_hari').val(data.tgl_hari);

       }
        $('.tgl_lahir').val('');
        $('.tgl_lahir').val(data);
       
        
    }
  });
}

function umur_pe()
{

  $.ajax({

    data:'tanggal_lahir='+$('.tanggal_lahir').val(),
    url:'{{URL::to("hitung/umur")}}',
    success:function(data) {

        //alert(data.tgl_tahun);
        $('#tanggal_tahun').val(data.tgl_tahun);
        $('#tanggal_bulan').val(data.tgl_bulan);
        $('#tanggal_hari').val(data.tgl_hari);
    }
  });
}

function showTglLahir() {
    //alert('oke');
}

//fungsi js untuk mendapatkan no_epid tetanus scara otomatis
function showEpidTetanus() {
  $.ajax({
    data:'id_kelurahan='+$('#id_kelurahan').val()+'&date='+$('.tgl_mulai_sakit').val(),
    url:'{{URL::to("ambil_epid_tetanus")}}',
    success:function(data) {
       $('#epid').val(data);
   }
  });
}

//fungsi js untuk mendapatkan no_epid difteri scara otomatis
function showEpidDifteri() {

  $.ajax({
    data:'id_kelurahan='+$('#id_kelurahan').val()+'&date='+$('.tgl_mulai_sakit').val(),
    url:'{{URL::to("ambil_epid_difteri")}}',
    success:function(data) {

       $('#epid').val(data);
   }
  });

}
//fungsi js untuk mendapatkan no_epid afp scara otomatis
function showEpidAfp() {

    var id_kelurahan = $('#id_kelurahan_pasien').val() != 'undefined' ? $('#id_kelurahan_pasien').val() : '';
    var tgl_mulai_sakit = $('#tgl_mulai_sakit_pasien').val() != 'undefined' ? $('#tgl_mulai_sakit').val() : '';
  $.ajax({
    data:'id_kelurahan='+id_kelurahan+'&date='+tgl_mulai_sakit,
    url:'{{URL::to("ambil_epid_afp")}}',
    success:function(data) {

       $('#epid').val(data);
   }
  });

}


function tampilkan_maps(){
    /*
    $.ajax({
        type:'POST',
        dataType:'script',
        url:'{{URL::to("region/get_chart_campak_js")}}',
    });
    */
}
</script>
<style>
.gmnoprint img {
    max-width: none; 
}

#map_dashboard img {
    max-width: none;
}
</style>
@include('admin.maps')
<script>

function loading_state(){
	$('#status').html('Loading...');
}

function init_map(){
	$.ajax({
		dataType:'script',
		//type: 'POST',
		//data: 'penyakit='+$('#combo_grafik_maps').val()+'&num='+num,
		url:'{{URL::to("get_js_init_maps_instansi")}}',
		success:function(data) {
			combo_grafik_maps();
		}
	});
}

function combo_grafik_maps(){
	<?php
	$nama_instansi = "";
	$type = Session::get('type');
	$kd_faskes = Session::get('kd_faskes');
	$latitude = "-7.8936254";
	$longitude = "110.4029268";
	if($type == "puskesmas"){
		$q = "
			SELECT b.puskesmas_name,b.alamat,b.kode_kab,b.latitude,b.longitude
			FROM puskesmas b
			JOIN kabupaten c ON c.id_kabupaten=b.kode_kab
			WHERE b.puskesmas_code_faskes='".$kd_faskes."'
		";
		$data=DB::select($q);
		$kode_kab = "";
		for($i=0;$i<count($data);$i++){
			$nama_instansi = "PUSKESMAS ". $data[0]->puskesmas_name;
			$kode_kab = $data[0]->kode_kab;
			$latitude = $data[0]->latitude;
			$longitude = $data[0]->longitude;
		}
		$maps_wilayah = "e.id_kabupaten='".$kode_kab."'";
		$level = "desa";
		$q = "
		SELECT * FROM polygon a 
		LEFT JOIN peta_dati b ON b.polygon_id=a.id
		LEFT JOIN kelurahan c ON c.id_kelurahan=b.desa_id
		LEFT JOIN kecamatan d ON d.id_kecamatan=c.id_kecamatan
		LEFT JOIN kabupaten e ON e.id_kabupaten=d.id_kabupaten
		LEFT JOIN provinsi f ON f.id_provinsi=e.id_provinsi
		WHERE 
		1=1
		AND
		".$maps_wilayah."
		AND b.level='".$level."'
		";
	} else if($type == "rs"){
		$q = "
			SELECT 
				b.kode_faskes,
				b.nama_faskes,
				b.alamat,
				LEFT(b.kode_faskes,4) as kode_kab,
				b.latitude,
				b.longitude
			FROM rumahsakit2 b
			JOIN kabupaten c ON c.id_kabupaten=LEFT(b.kode_faskes,4)
			WHERE b.kode_faskes='".$kd_faskes."'
		";
		$data=DB::select($q);
		$kode_kab = "";
		for($i=0;$i<count($data);$i++){
			$nama_instansi = $data[0]->nama_faskes;
			$kode_kab = $data[0]->kode_kab;
			$latitude = $data[0]->latitude;
			$longitude = $data[0]->longitude;
		}
		$maps_wilayah = "e.id_kabupaten='".$kode_kab."'";
		$level = "desa";
		$q = "
		SELECT * FROM polygon a 
		LEFT JOIN peta_dati b ON b.polygon_id=a.id
		LEFT JOIN kelurahan c ON c.id_kelurahan=b.desa_id
		LEFT JOIN kecamatan d ON d.id_kecamatan=c.id_kecamatan
		LEFT JOIN kabupaten e ON e.id_kabupaten=d.id_kabupaten
		LEFT JOIN provinsi f ON f.id_provinsi=e.id_provinsi
		WHERE 
		1=1
		AND
		".$maps_wilayah."
		AND b.level='".$level."'
		";
	} else if($type == "kabupaten"){
		$q = "
			SELECT * FROM kabupaten b 
			WHERE b.id_kabupaten='".$kd_faskes."'
		";
		$data=DB::select($q);
		$id_kabupaten = "";
		for($i=0;$i<count($data);$i++){
			$nama_instansi = "DINAS KESEHATAN KABUPATEN ". $data[0]->kabupaten;
			$id_kabupaten = $data[0]->id_kabupaten;
		}
		$maps_wilayah = "e.id_kabupaten='".$id_kabupaten."'";
		$level = "kecamatan";
		$q = "
		SELECT * FROM polygon a 
		LEFT JOIN peta_dati b ON b.polygon_id=a.id
		LEFT JOIN kecamatan d ON d.id_kecamatan=b.desa_id
		LEFT JOIN kabupaten e ON e.id_kabupaten=d.id_kabupaten
		LEFT JOIN provinsi f ON f.id_provinsi=e.id_provinsi
		WHERE 
		1=1
		AND
		".$maps_wilayah."
		AND b.level='".$level."'
		";
	} else if($type == "provinsi"){
		$q = "
			SELECT * FROM provinsi b 
			WHERE b.id_provinsi='".$kd_faskes."'
		";
		$data=DB::select($q);
		$id_provinsi = "";
		for($i=0;$i<count($data);$i++){
			$nama_instansi = "DINAS KESEHATAN PROVINSI ".$data[0]->provinsi;
			$id_provinsi = $data[0]->id_provinsi;
		}
		$maps_wilayah = "f.id_provinsi='".$id_provinsi."'";
		$level = "kabupaten";
		$q = "
		SELECT * 
		FROM polygon a 
		LEFT JOIN peta_dati b ON b.polygon_id=a.id
		LEFT JOIN kabupaten e ON e.id_kabupaten=b.desa_id
		LEFT JOIN provinsi f ON f.id_provinsi=e.id_provinsi
		WHERE 
		1=1
		AND
		".$maps_wilayah."
		AND b.level='".$level."'
		";
	} else if($type == "kemenkes"){
		$q = "
			SELECT * FROM provinsi b 
		";
		$data=DB::select($q);
		$parent_instance = "";
		for($i=0;$i<count($data);$i++){
			$nama_instansi = "DINAS KESEHATAN PROVINSI ".$data[0]->provinsi;
			$parent_instance = $data[0]->id_provinsi;
		}
		$maps_wilayah = "f.id_provinsi='".$parent_instance."'";
		$parent_level = "country";
		$level = "kabupaten";
		$q = "
		SELECT a.id,e.id_kabupaten as id_instansi,e.kabupaten as kelurahan,b.center_lat,b.center_lon 
		FROM polygon a 
		LEFT JOIN peta_dati b ON b.polygon_id=a.id
		LEFT JOIN kabupaten e ON e.id_kabupaten=b.desa_id
		LEFT JOIN provinsi f ON f.id_provinsi=e.id_provinsi
		WHERE 
		1=1
		AND b.level='".$level."'
		";
		$infowindow_title_region = "Kabupaten";
		
		$latitude = "-7.4296311";
		$longitude = "110.6927889";
		$default_zoom  = "10";
		$min_zoom  = "8";
	} else if($type == "laboratorium"){
		$q = "
			SELECT * FROM laboratorium b 
			WHERE b.lab_code='".$kd_faskes."'
		";
		$data=DB::select($q);
		for($i=0;$i<count($data);$i++){
			$nama_instansi = "LABORATORIUM ".$data[0]->nama_laboratorium;
		}
		$maps_wilayah = "";
		$level = "provinsi";
	}
	$arr_jml=DB::select($q);
	$jml = count($arr_jml);
	?>
	$('#max_maps_id').val('<?php echo $jml; ?>');
	//for(var i=0;i<<?php echo $jml; ?>;i++){
	get_map();
	//}
}

function get_map(){
	$.ajax({
		dataType:'script',
		//type: 'POST',
		data: 'penyakit='+$('#combo_grafik_maps').val()+'&num='+$('#last_maps_id').val(),
		url:'{{URL::to("get_js_grafik_maps_instansi")}}',
		success:function(data) {
			var num = (parseInt ($('#last_maps_id').val()))+1;
			$('#last_maps_id').val(num);
			if(parseInt($('#last_maps_id').val()) < parseInt($('#max_maps_id').val())) get_map();
		}
	});
}

/*<<<<<<< HEAD
=======
//simpan difteri

$('#save_difteri').submit(function(e) {
        //loading_state();
        $('#save_difteri').ajaxSubmit({
            type: 'POST',
            dataType:'json',
            success:function(response) {
            //callback ketika response status kosong berarti ada error   
            if(response.status=='')
            {
                //tampilkan masing2 error ke form input 
                if(response.no_epid){
                    //alert(response.no_epid);
                  $('#error-no_epid').removeClass('').addClass('alert alert-error');
                  $('#error-no_epid').text(response.no_epid);
                }
                else 
                {
                  $('#error-no_epid').removeClass('alert alert-error').addClass('');
                  $('#error-no_epid').text('');
                }
                if(response.nama_anak){
                  $('#error-nama_anak').removeClass('').addClass('alert alert-error');
                  $('#error-nama_anak').text(response.nama_anak);
                }
                else 
                {
                  $('#error-nama_anak').removeClass('alert alert-error').addClass('');
                  $('#error-nama_anak').text('');
                }
                if(response.nama_ortu){
                    //alert(response.no_epid);
                  $('#error-nama_ortu').removeClass('').addClass('alert alert-error');
                  $('#error-nama_ortu').text(response.nama_ortu);
                }
                else 
                {
                  $('#error-nama_ortu').removeClass('alert alert-error').addClass('');
                  $('#error-nama_ortu').text('');
                }
                if(response.jenis_kelamin){
                    //alert(response.no_epid);
                  $('#error-jenis_kelamin').removeClass('').addClass('alert alert-error');
                  $('#error-jenis_kelamin').text(response.jenis_kelamin);
                }
                else 
                {
                  $('#error-jenis_kelamin').removeClass('alert alert-error').addClass('');
                  $('#error-jenis_kelamin').text('');
                }
                if(response.tanggal_lahir){
                    //alert(response.no_epid);
                  $('#error-tanggal_lahir').removeClass('').addClass('alert alert-error');
                  $('#error-tanggal_lahir').text(response.tanggal_lahir);
                }
                else 
                {
                  $('#error-tanggal_lahir').removeClass('alert alert-error').addClass('');
                  $('#error-tanggal_lahir').text('');
                }
                if(response.id_propinsi){
                    //alert(response.no_epid);
                  $('#error-id_propinsi').removeClass('').addClass('alert alert-error');
                  $('#error-id_propinsi').text(response.id_propinsi);
                }
                else 
                {
                  $('#error-id_propinsi').removeClass('alert alert-error').addClass('');
                  $('#error-id_propinsi').text('');
                }
                if(response.id_kabupaten){
                    //alert(response.no_epid);
                  $('#error-id_kabupaten').removeClass('').addClass('alert alert-error');
                  $('#error-id_kabupaten').text(response.id_kabupaten);
                }
                else 
                {
                  $('#error-id_kabupaten').removeClass('alert alert-error').addClass('');
                  $('#error-id_kabupaten').text('');
                }
                if(response.id_kecamatan){
                    //alert(response.no_epid);
                  $('#error-id_kecamatan').removeClass('').addClass('alert alert-error');
                  $('#error-id_kecamatan').text(response.id_kecamatan);
                }
                else 
                {
                  $('#error-id_kecamatan').removeClass('alert alert-error').addClass('');
                  $('#error-id_kecamatan').text('');
                }
                if(response.id_kelurahan){
                    //alert(response.no_epid);
                  $('#error-id_kelurahan').removeClass('').addClass('alert alert-error');
                  $('#error-id_kelurahan').text(response.no_epid);
                }
                else 
                {
                  $('#error-id_kelurahan').removeClass('alert alert-error').addClass('');
                  $('#error-id_kelurahan').text('');
                }
                if(response.tanggal_timbul_demam){
                    //alert(response.no_epid);
                  $('#error-tanggal_timbul_demam').removeClass('').addClass('alert alert-error');
                  $('#error-tanggal_timbul_demam').text(response.tanggal_timbul_demam);
                }
                else 
                {
                  $('#error-tanggal_timbul_demam').removeClass('alert alert-error').addClass('');
                  $('#error-tanggal_timbul_demam').text('');
                }
                if(response.vaksin_DPT_sebelum_sakit){
                    //alert(response.no_epid);
                  $('#error-vaksin_DPT_sebelum_sakit').removeClass('').addClass('alert alert-error');
                  $('#error-vaksin_DPT_sebelum_sakit').text(response.vaksin_DPT_sebelum_sakit);
                }
                else 
                {
                  $('#error-vaksin_DPT_sebelum_sakit').removeClass('alert alert-error').addClass('');
                  $('#error-vaksin_DPT_sebelum_sakit').text('');
                }
                if(response.tanggal_vaksinasi_difteri_terakhir){
                    //alert(response.no_epid);
                  $('#error-tanggal_vaksinasi_difteri_terakhir').removeClass('').addClass('alert alert-error');
                  $('#error-tanggal_vaksinasi_difteri_terakhir').text(response.tanggal_vaksinasi_difteri_terakhir);
                }
                else 
                {
                  $('#error-tanggal_vaksinasi_difteri_terakhir').removeClass('alert alert-error').addClass('');
                  $('#error-tanggal_vaksinasi_difteri_terakhir').text('');
                }
                if(response.tanggal_pelacakan){
                    //alert(response.no_epid);
                  $('#error-tanggal_pelacakan').removeClass('').addClass('alert alert-error');
                  $('#error-tanggal_pelacakan').text(response.tanggal_pelacakan);
                }
                else 
                {
                  $('#error-tanggal_pelacakan').removeClass('alert alert-error').addClass('');
                  $('#error-tanggal_pelacakan').text('');
                }
                if(response.keadaan_akhir){
                    //alert(response.no_epid);
                  $('#error-keadaan_akhir').removeClass('').addClass('alert alert-error');
                  $('#error-keadaan_akhir').text(response.keadaan_akhir);
                }
                else 
                {
                  $('#error-keadaan_akhir').removeClass('alert alert-error').addClass('');
                  $('#error-keadaan_akhir').text('');
                }
                if(response.tanggal_diambil_spesimen_hidung){
                    //alert(response.no_epid);
                  $('#error-tanggal_diambil_spesimen_hidung').removeClass('').addClass('alert alert-error');
                  $('#error-tanggal_diambil_spesimen_hidung').text(response.tanggal_diambil_spesimen_hidung);
                }
                else 
                {
                  $('#error-tanggal_diambil_spesimen_hidung').removeClass('alert alert-error').addClass('');
                  $('#error-tanggal_diambil_spesimen_hidung').text('');
                }
                if(response.tanggal_diambil_spesimen_tenggorokan){
                    //alert(response.no_epid);
                  $('#error-tanggal_diambil_spesimen_tenggorokan').removeClass('').addClass('alert alert-error');
                  $('#error-tanggal_diambil_spesimen_tenggorokan').text(response.tanggal_diambil_spesimen_tenggorokan);
                }
                else 
                {
                  $('#error-tanggal_diambil_spesimen_tenggorokan').removeClass('alert alert-error').addClass('');
                  $('#error-tanggal_diambil_spesimen_tenggorokan').text('');
                }
                if(response.jenis_pemeriksaan){
                    //alert(response.no_epid);
                  $('#error-jenis_pemeriksaan').removeClass('').addClass('alert alert-error');
                  $('#error-jenis_pemeriksaan').text(response.jenis_pemeriksaan);
                }
                else 
                {
                  $('#error-jenis_pemeriksaan').removeClass('alert alert-error').addClass('');
                  $('#error-jenis_pemeriksaan').text('');
                }
                if(response.tipe_spesimen){
                    //alert(response.no_epid);
                  $('#error-tipe_spesimen').removeClass('').addClass('alert alert-error');
                  $('#error-tipe_spesimen').text(response.tipe_spesimen);
                }
                else 
                {
                  $('#error-tipe_spesimen').removeClass('alert alert-error').addClass('');
                  $('#error-tipe_spesimen').text('');
                }
            }
         //pesan data berhasil di simpan
        //$('#test_output').html(data);
        }
    });
    e.preventDefault();
});

>>>>>>> a0c95cd1f2de35b2166ec0a0a5fbf953bc799be7*/
</script>