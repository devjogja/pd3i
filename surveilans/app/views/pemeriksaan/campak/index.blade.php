@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <h4>Penyakit Campak</h4>
                    <hr>
                </div>
                @include('pemeriksaan.campak._include_to_index.tab_navigasi')
            </div>
            <div class="profile-tab-content tab-content">
                <div class="tab-pane fade in active" id="activity">
                    @if(Session::get('type')=='rs')
                        @include('pemeriksaan._include.filter_daftar_kasus_rs')
                    @elseif(Session::get('type')=='puskesmas')
                        @include('pemeriksaan._include.filter_daftar_kasus_puskesmas')
                    @else
                        @include('pemeriksaan._include.filter_daftar_kasus')
                    @endif
                    <div id="tabel-daftar-kasus">
                        @include('pemeriksaan.campak._include_to_index.daftar_kasus')
                    </div>
                </div>
                <div class="tab-pane fade in" id="friends">
                    @include('pemeriksaan.campak._include_to_index.input_data_individual')
                </div>
                @include('pemeriksaan.campak.analisis')
                <div class="tab-pane fade in" id="PE">
                    <div id="data_campak"></div>
                </div>
                <div class="tab-pane fade in" id="importcampak">
                    @include('pemeriksaan.campak.import_campak')
                </div>
            </div><!-- akhir profile-tab-content tab-content -->
        </div><!-- akhir class module -->
    </div><!--/.akhir content-->
</div><!--/.akhir span12-->

@include('pemeriksaan.campak.analisis-js')

<script type="text/javascript">
    $(document).ready(function() {
        //get tabs
        var url = document.location.toString();
        if (url.match('#')) {
            $('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
        }
        $('#example').dataTable();
        $('#div_sampel').hide();
        $('#jenis_sampel').attr('disabled','disabled');
        $('#simpan').attr('disabled','disabled');
        $('#tanggal_ambil_sampel').attr('disabled','disabled');

        $('#form_save_campak').validate({
            submit: {
                settings: {
                    scrollToError: {
                       offset: -100,
                       duration: 500
                    }
                }
            }
        });

        $('#form_save_campak input').keydown(function(e){
            // check for submit button and submit form on enter press
            if(e.keyCode==13){
                if($(':input:eq(' + ($(':input').index(this) + 1) + ')').attr('type')=='submit'){
                    return true;
                }

                $(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();

                return false;
            }
        });
    });

    function tambah_sampel(){
        $('#div_sampel').show(2000);
        $('#tambah_sampel').hide();
    }

    function tutup_sampel(){
        $('#div_sampel').hide(2000);
        $('#tambah_sampel').show(2000);
        $('#jenis_sampel').val('');
        $('#tanggal_ambil_sampel').val('');
        $('#jenis_sampel').attr('disabled','disabled');
        $('#simpan').attr('disabled','disabled');
        $('#tanggal_ambil_sampel').attr('disabled','disabled');
    }

    function jenissampel()
    {
        if ($('#jenis_sampel').val()=='') {
            $('#tanggal_ambil_sampel').attr('disabled','disabled');
            $('#simpan').attr('disabled','disabled');
        }
        else
        {
             $('#tanggal_ambil_sampel').removeAttr('disabled','');
        }
    }

$(document).ready(function(){
    $("#tanggal_ambil_sampel").change(function(){
        $('#simpan').removeAttr('disabled','');    
    });
    $("#data_sampel").on('click','.remCF',function(){
        $(this).parent().parent().remove();
    });
});

</script>

@stop