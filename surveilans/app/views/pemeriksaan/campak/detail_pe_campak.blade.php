@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <h4>
                        Detail penyelidikan epidemologi kasus penyakit campak
                    </h4>
                    <hr>
                </div>
<div class="tabbable-panel">
	<div class="tabbable-line">
		<ul class="nav nav-tabs ">
			<li class="active">
				<a href="#tab_default_1" data-toggle="tab">
				Identitas penderita </a>
			</li>
			<li>
				<a href="#tab_default_3" data-toggle="tab">
				Riwayat pengobatan
				</a>
			</li>
			<li>
				<a href="#tab_default_4" data-toggle="tab">
				Riwayat kontak
				</a>
			</li>
			<li>
				<a href="#tab_default_6" data-toggle="tab">
				Jumlah kasus tambahan di Lokasi PE
				</a>
			</li>
			<li>
				<a href="#tab_default_5" data-toggle="tab">
				Pemeriksaan spesimen
				</a>
			</li>
		</ul>
		@foreach($data as $row)
		<div class="tab-content">
			<div class="tab-pane active" id="tab_default_1">
				<table class="table table-striped">
			      <tr>
			          <td>No. Epidemologi</td>
			          <td>{{$row->no_epid}}</td>
			      </tr>
			      <tr>
			          <td>No induk kependudukan</td>
			          <td>{{$row->nik}}</td>
			      </tr>
			      <tr>
			          <td>Nama Pasien</td>
			          <td>{{$row->nama_anak}}</td>
			      </tr>
			      <tr>
			          <td>Nama Orang tua</td>
			          <td>{{$row->nama_ortu}}</td>
			      </tr>
			      <tr>
			          <td>Jenis Kelamin</td>
			          @if($row->jenis_kelamin==1)
			          <td>Laki-laki</td>
			          @elseif($row->jenis_kelamin==2)
			          <td>Perempuan</td>
			          @else
			          <td>Tidak jelas</td>
			          @endif
			      </tr>
			      <tr>
			          <td>Tanggal Lahir</td>
			          <td>{{Helper::getDate($row->tanggal_lahir)}}</td>
			      </tr>
			      <tr>
			          <td>Umur</td>
			          <td>{{$row->umur}} tahun {{$row->umur_bln}} bulan {{$row->umur_hr}} hari</td>
			      </tr>
			      <tr>
			          <td>Alamat</td>
			          <td>{{$row->alamat}}</td>
			      </tr>
				</table>
			</div>
			<div class="tab-pane" id="tab_default_3">
				<table class="table table-striped">
				<tr>
					<td>Kapan mendapatkan pengobatan pertama kali</td>
					<td>{{Helper::getDate($row->waktu_pengobatan)}}</td>
				</tr>
				<tr>
					<td>Dimana mendapatkan pengobatan pertama kali?</td>
					<td>{{$row->tempat_pengobatan}}</td>
				</tr>
				<tr>
					<td>Obat yang sudah diberikan</td>
					<td>{{$row->obat_yang_diberikan}}</td>
				</tr>
				</table>
			</div>
			<div class="tab-pane" id="tab_default_4">
				<table class="table table-striped">
				<tr>
					<td>Apakah dirumah ada yang sakit seperti yang di alami sekarang</td>
					@if($row->dirumah_orang_sakit_sama==1)
					<td>Ya</td>
					@elseif($row->dirumah_orang_sakit_sama==2)
					<td>Tidak</td>
					@else
					<td>-</td>
					@endif
				</tr>
				<tr>
					<td>Kapan sakit</td>
					<td>{{Helper::getDate($row->kapan_sakit_dirumah)}}</td>
				</tr>
				<tr>
					<td>Apakah disekolah anak ada yang sakit seperti yang dialami sekarang</td>
					@if($row->disekolah_orang_sakit_sama==1)
					<td>Ya</td>
					@elseif($row->disekolah_orang_sakit_sama==2)
					<td>Tidak</td>
					@else
					<td>-</td>
					@endif
				</tr>
				<tr>
					<td>Kapan sakit</td>
					<td>{{Helper::getDate($row->kapan_sakit_disekolah)}}</td>
				</tr>
				<tr>
					<td>Apakah penderita menunjukkan keadaan kekurangan gizi (BB/U)</td>
					@if($row->kekurangan_gizi==1)
					<td>Ya</td>
					@elseif($row->kekurangan_gizi==2)
					<td>Tidak</td>
					@else
					<td>-</td>
					@endif
				</tr>
				</table>
			</div>
			<div class="tab-pane" id="tab_default_6">
				<table class="table table-striped">
				<tr>
					<th></th>
					<th>Tanggal PE</th>
					<th>Jumlah kasus tambahan</th>
				</tr>
				<tr>
					<td>Alamat tinggal sementara saat sakit (bila ada)</td>
					<td>{{Helper::getDate($row->tgl_pe_dialamat_sementara)}}</td>
					<td>{{ $row->jumlah_kasus_dialamat_sementara }}</td>
				</tr>
				<tr>
					<td>Alamat tempat tinggal</td>
					<td>{{Helper::getDate($row->tgl_pe_alamat_tempat_tinggal)}}</td>
					<td>{{ $row->jumlah_kasus_dialamat_tempat_tinggal }}</td>
				</tr>
				<tr>
					<td>Sekolah</td>
					<td>{{Helper::getDate($row->tgl_pe_sekolah)}}</td>
					<td>{{ $row->jumlah_kasus_disekolah }}</td>
				</tr>
				<tr>
					<td>Tempat kerja</td>
					<td>{{Helper::getDate($row->tgl_pe_tempat_kerja)}}</td>
					<td>{{ $row->jumlah_kasus_tempat_kerja }}</td>
				</tr>
				<tr>
					<td>{{ $row->lain_lain }}</td>
					<td>{{Helper::getDate($row->tgl_pe_lain_lain)}}</td>
					<td>{{ $row->jumlah_kasus_lain_lain }}</td>
				</tr>
				<tr>
					<td>Total Kasus tambahan</td>
					<td></td>
					<td>{{ $row->jumlah_total_kasus_tambahan }}</td>
				</tr>
				</table>
			</div>
			<div class="tab-pane" id="tab_default_5">
				<table class="table table-striped">
				<tr>
					<td>Tanggal penyelidikan</td>
					<td>{{Helper::getDate($row->tanggal_penyelidikan)}}</td>
				</tr>
				<tr>
					<td>Pelaksana</td>
					<td>{{$row->pelaksana}}</td>
				</tr>
				</table>
			</div>
		</div>
		@endforeach
	</div>
</div>
<div class="form-actions" style="text-align:center">
  	<a href="{{URL::to('campak#PE')}}" class="btn btn-warning">kembali</a>
</div>
</div>
</div>
</div>
</div>
@stop
