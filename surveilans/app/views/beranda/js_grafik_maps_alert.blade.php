var image = {
	url: '{{URL::to("asset/images/notif.png")}}',
	size: new google.maps.Size(54, 73),
	origin: new google.maps.Point(0,0),
	anchor: new google.maps.Point(27, 71)
};

<?php

$type = Session::get('type');
$kd_faskes = Session::get('kd_faskes');

if($type == "puskesmas" || $type == "rs"){
	$string = "
	SELECT 
		j.polygon_id,
		j.center_lat,
		j.center_lon	 
	FROM hasil_uji_lab_campak a 
	JOIN pasien b ON b.id_pasien=a.id_pasien
	JOIN campak c ON c.id_campak=a.id_campak
	LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
	LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
	LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
	LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
	JOIN peta_dati j  ON j.desa_id=d.id_kelurahan
	WHERE f.id_kabupaten IS NOT NULL
	AND c.klasifikasi_final IN ('0','1','2')
	AND DATE_FORMAT(c.tanggal_timbul_rash,'%Y') >= '2014'
	AND DATE_FORMAT(c.tanggal_timbul_rash,'%Y') <= '2015'
	GROUP BY d.id_kelurahan
	";
} else if($type == "kabupaten"){
	$string = "
	SELECT 
		j.polygon_id,
		j.center_lat,
		j.center_lon	 
	FROM hasil_uji_lab_campak a 
	JOIN pasien b ON b.id_pasien=a.id_pasien
	JOIN campak c ON c.id_campak=a.id_campak
	LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
	LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
	LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
	LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
	JOIN peta_dati j  ON j.desa_id=e.id_kecamatan
	WHERE f.id_kabupaten IS NOT NULL
	AND c.klasifikasi_final IN ('0','1','2')
	AND DATE_FORMAT(c.tanggal_timbul_rash,'%Y') >= '2014'
	AND DATE_FORMAT(c.tanggal_timbul_rash,'%Y') <= '2015'
	GROUP BY e.id_kecamatan
	";
} else if($type == "provinsi"){
	$string = "
	SELECT 
		j.polygon_id,
		j.center_lat,
		j.center_lon	 
	FROM hasil_uji_lab_campak a 
	JOIN pasien b ON b.id_pasien=a.id_pasien
	JOIN campak c ON c.id_campak=a.id_campak
	LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
	LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
	LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
	LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
	JOIN peta_dati j  ON j.desa_id=f.id_kabupaten
	WHERE f.id_kabupaten IS NOT NULL
	AND c.klasifikasi_final IN ('0','1','2')
	AND DATE_FORMAT(c.tanggal_timbul_rash,'%Y') >= '2014'
	AND DATE_FORMAT(c.tanggal_timbul_rash,'%Y') <= '2015'
	GROUP BY f.id_kabupaten
	";
} else if($type == "kemenkes"){
	$string = "
	SELECT 
		j.polygon_id,
		j.center_lat,
		j.center_lon	 
	FROM hasil_uji_lab_campak a 
	JOIN pasien b ON b.id_pasien=a.id_pasien
	JOIN campak c ON c.id_campak=a.id_campak
	LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
	LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
	LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
	LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
	JOIN peta_dati j  ON j.desa_id=f.id_kabupaten
	WHERE f.id_kabupaten IS NOT NULL
	AND c.klasifikasi_final IN ('0','1','2')
	AND DATE_FORMAT(c.tanggal_timbul_rash,'%Y') >= '2014'
	AND DATE_FORMAT(c.tanggal_timbul_rash,'%Y') <= '2015'
	GROUP BY f.id_kabupaten
	";
} else if($type == "laboratorium"){
	$string = "
	SELECT 
		j.polygon_id,
		j.center_lat,
		j.center_lon	 
	FROM hasil_uji_lab_campak a 
	JOIN pasien b ON b.id_pasien=a.id_pasien
	JOIN campak c ON c.id_campak=a.id_campak
	LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
	LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
	LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
	LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
	JOIN peta_dati j ON j.desa_id=e.id_kecamatan
	WHERE f.id_kabupaten IS NOT NULL
	AND c.klasifikasi_final IN ('0','1','2')
	AND DATE_FORMAT(c.tanggal_timbul_rash,'%Y') >= '2014'
	AND DATE_FORMAT(c.tanggal_timbul_rash,'%Y') <= '2015'
	GROUP BY e.id_kecamatan
	";
}


$q=DB::select($string);
for($i=0;$i<count($q);$i++):?>	
	var myLatLng = new google.maps.LatLng(<?php echo $q[$i]->center_lon; ?>,<?php echo $q[$i]->center_lat; ?>);
	var marker = new google.maps.Marker({
		position: myLatLng,
		map: map,
		icon: image,
		title: 'asd',
		zIndex: 100
	});
	
	google.maps.event.addListener(marker, 'click', function() {
		var lokasi_db  = new google.maps.LatLng(<?php echo $q[$i]->center_lon; ?>,<?php echo $q[$i]->center_lat; ?>);
		var marker_ss_<?php echo $q[$i]->polygon_id; ?> = new google.maps.Marker({
			position: lokasi_db
		});
		infowindow_Triangle<?php echo $q[$i]->polygon_id; ?>.open(map,marker_ss_<?php echo $q[$i]->polygon_id; ?>); 
	});
<?php endfor; ?>
