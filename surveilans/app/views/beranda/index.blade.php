@extend('layouts.master')
@section('content')

<style>
	.tombol{
		width:30px;
		height:150px;
		background:#eee;
		cursor:pointer;
	}
	
	.tombol.kiri{
		float:left;
	}
	
	.tombol.kanan{
		float:right;
	}
	
	.tombol.kiri:hover{ 
		background:#ddd;
	}
	
	.tombol.kanan:hover{
		background:#ddd;
	}
</style>

<!--/.span3-->
<div class="span12">
    <div class="content">
    @if(Sentry::check())
    @if(Sentry::getUser()->hak_akses==2)
      <!--   <div class="btn-controls">
            <div class="alert alert-info" role="alert">Selamat datang di puskesmas XXX, terimakasih telah login di halaman ini.(sekedar contoh)</div>
        </div> -->
        
        <div class="btn-controls">
            <div class="btn-box-row row-fluid">
				<div class="tombol kiri">
					<img src="{{URL::to('assets/img/icon/btn1.png')}}">
				</div>
				<div class="tombol kanan">
					<img src="{{URL::to('assets/img/icon/btn2.png')}}">
				</div>
				
				<a href="{{URL::to('campak')}}" class="btn-box small span2">
					<img src="{{URL::to('assets/img/icon/campak.png')}}" style="width:70px;">
                    <p class="text-muted">
                        Campak</p>
                </a><a href="{{URL::to('tetanus')}}" class="btn-box small span2">
					<img src="{{URL::to('assets/img/icon/tetanus.png')}}" style="width:70px;"> 
					<p class="text-muted">
                        Tetanus neonatorum</p>
                </a><a href="{{URL::to('difteri')}}" class="btn-box small span2">
					<img src="{{URL::to('assets/img/icon/difteri.png')}}" style="width:70px;">
                    <p class="text-muted">
                        Difteri</p>
                </a>
				</a><a href="{{URL::to('afp')}}" class="btn-box small span2">
                    <img src="{{URL::to('assets/img/icon/polio.png')}}" style="width:70px;">
					<p class="text-muted">
                        AFP</p>
                </a>
				</a><a href="#" class="btn-box small span2">
                    <img src="{{URL::to('assets/img/icon/btn_none.png')}}" style="width:70px;">
					<p class="text-muted">
                        Nama Penyakit</p>
                </a>
				
            </div>
        </div>
    @endif
    @endif
        <!--/#btn-controls-->
        <div class="module">
            <div class="module-head">
                <h3>
                    Chart</h3>
            </div>
            <div class="module-body">
                <div class="chart inline-legend grid">
                    <div id="klasifikasi_final_penyakit_campak" style="float:left;width:25%;">
                    </div>
					<div id="klasifikasi_final_penyakit_difteri" style="float:left;width:25%;">
                    </div>
					<div id="klasifikasi_final_penyakit_tetanus" style="float:left;width:25%;">
                    </div>
					<div id="klasifikasi_final_penyakit_afp" style="float:left;width:25%;">
                    </div>
					<div style="clear:both;"></div>
					<div id="map_dashboard" style="float:left;width:100%;height:500px;">
                    </div>
                </div>
				<div style="clear:both;"></div>
            </div>
        </div>
        <!--/.module-->
        <div class="module hide">
            <div class="module-head">
                <h3>
                    Adjust Budget Range</h3>
            </div>
            <div class="module-body">
                <div class="form-inline clearfix">
                    <a href="#" class="btn pull-right">Update</a>
                    <label for="amount">
                        Price range:</label>
                    &nbsp;
                    <input type="text" id="amount" class="input-" />
                </div>
                <hr />
                <div class="slider-range">
                </div>
            </div>
        </div>
    </div>
    <!--/.content-->
</div>

<script>
$(document).ready(function(){
	$('#klasifikasi_final_penyakit_campak').highcharts({
			chart: {
				type: 'pie',
				options3d: {
					enabled: true,
					alpha: 45
				}
			},
			title: {
				text: 'Klasifikasi Final Penderita Campak',
				style: { "color": "#333333", "fontSize": "12px" }
			},
			subtitle: {
				text: 'Puskesmas xxx periode xxx'
			},
			plotOptions: {
				pie: {
					innerSize: 70,
					depth: 45
				}
			},
			series: [{
				name: 'Jumlah Penderita',
				data: [
					['Lab', 8],
					['Klinis', 3],
					['Epid', 1],
					['Tersangka', 6]
				]
			}]
		});
	$('#klasifikasi_final_penyakit_difteri').highcharts({
			chart: {
				type: 'pie',
				options3d: {
					enabled: true,
					alpha: 45
				}
			},
			title: {
				text: 'Klasifikasi Final Penderita Difteri',
				style: { "color": "#333333", "fontSize": "12px" }
			},
			subtitle: {
				text: 'Puskesmas xxx periode 2014'
			},
			plotOptions: {
				pie: {
					innerSize: 60,
					depth: 45
				}
			},
			series: [{
				name: 'Jumlah Penderita',
				data: [
					['Lab', 3],
					['Klinis nuts', 1],
					['Epid', 6],
					['Tersangka', 8]
				]
			}]
		});
	$('#klasifikasi_final_penyakit_tetanus').highcharts({
			chart: {
				type: 'pie',
				options3d: {
					enabled: true,
					alpha: 45
				}
			},
			title: {
				text: 'Klasifikasi Final Penderita Tetanus',
				style: { "color": "#333333", "fontSize": "12px" }
			},
			subtitle: {
				text: 'Puskesmas xxx periode 2014'
			},
			plotOptions: {
				pie: {
					innerSize: 70,
					depth: 45
				}
			},
			series: [{
				name: 'Jumlah Penderita',
				data: [
					['Lab', 1],
					['Klinis', 6],
					['Epid', 8],
					['Tersangka', 4], 
				]
			}]
		});
	$('#klasifikasi_final_penyakit_afp').highcharts({
			chart: {
				type: 'pie',
				options3d: {
					enabled: true,
					alpha: 45
				}
			},
			title: {
				text: 'Klasifikasi Final Penderita AFP',
				style: { "color": "#333333", "fontSize": "12px" }
			},
			subtitle: {
				text: 'Puskesmas xxx periode 2014'
			},
			plotOptions: {
				pie: {
					innerSize: 70,
					depth: 45
				}
			},
			series: [{
				name: 'Jumlah Penderita',
				data: [
					['Lab', 8],
					['Klinis', 4], 
					['Epid', 4],
					['Tersangka', 1]
				]
			}]
		});
	});
	
</script>

<?php
	$q = "SELECT * FROM polygon order by id asc";
	$polygon2=DB::select($q);
	$poly_map = "";
	for($i=0;$i<count($polygon2);$i++){
		
		$q2 = "SELECT * FROM peta WHERE polygon_id='".$polygon2[$i]->id."' ORDER BY id ASC";
		$peta=DB::select($q2);	
		$polygon = "";
		$koma = "";
		$last_lat = "0";
		$last_lon = "0";
		
		for($j=0;$j<count($peta);$j++){
			$polygon .= $koma;
			$polygon .= "new google.maps.LatLng(".$peta[$j]->latitude.",".$peta[$j]->longitude.")";
			$koma = ",";
			$last_lat = $peta[$j]->latitude;
			$last_lon = $peta[$j]->longitude;
		}
		$poly_map .= "
			var Triangle".$polygon2[$i]->id.";
			var triangleCoords".$polygon2[$i]->id." = [
				".$polygon."
			];
			Triangle".$polygon2[$i]->id." = new google.maps.Polygon({
				paths: triangleCoords".$polygon2[$i]->id.",
				strokeColor: '#000',
				strokeOpacity: 0.7,
				strokeWeight: 1,
				fillColor: '#afa',
				fillOpacity: 0.35
			});
			Triangle".$polygon2[$i]->id.".setMap(map);
			
			var opt1_".$polygon2[$i]->id." = 
			{
				fillColor:'#aaf'
			};
			var opt2_".$polygon2[$i]->id." = {
				fillColor:'#afa'
			};
			google.maps.event.addListener(Triangle".$polygon2[$i]->id.", 'mouseover', function() {
				var currentPolygon = this;
				currentPolygon.setOptions(opt1_".$polygon2[$i]->id.");
				//alert('".$polygon2[$i]->id."');
			});
			google.maps.event.addListener(Triangle".$polygon2[$i]->id.", 'mouseout', function() {
				var currentPolygon = this;
				currentPolygon.setOptions(opt2_".$polygon2[$i]->id.");
			});
			var lokasi_db  = new google.maps.LatLng(".$last_lat.",".$last_lon.");
			var mapLabel = new MapLabel({
			  text: '???',
			  position: lokasi_db,
			  
			  map: map,
			  fontSize: 12,
			  align: 'center',
			  minZoom : 11
			});
		";
		$poly_map .= "
				var content = 
					'<table style=\'width:120px;height:120px;\'><tr><td>'+
					'Desa aaa <br>'+
					'Jumlah Total Pasien : <br>'+
					'ccc penderita'+
					'</td></tr></table>'
					;
				var infowindow_Triangle".$polygon2[$i]->id." = new google.maps.InfoWindow({
					content: content
				});
				var myLatlng = new google.maps.LatLng(-7.9622172, 110.6031254);
				var marker_".$polygon2[$i]->id." = new google.maps.Marker({
					position: lokasi_db
				});
				";
				$poly_map .= "
				google.maps.event.addListener(Triangle".$polygon2[$i]->id.", 'click', function() {
					infowindow_Triangle".$polygon2[$i]->id.".open(map,marker_".$polygon2[$i]->id.");
				});
				";
		
	}
?>
<style>
.gmnoprint img {
    max-width: none; 
}

#map_dashboard img {
    max-width: none;
}
</style>
<script>
function initialize() {
  var mapOptions = {
    zoom: 10,
    center: new google.maps.LatLng(-7.8936254,110.4029268),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var map = new google.maps.Map(document.getElementById('map_dashboard'),
                                mapOptions);

  setMarkers(map, penderita);
  
  var styles = [
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            }
        ]
    },
    {
        "featureType": "administrative.country",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "hue": "#00ff1e"
            }
        ]
    },
    {
        "featureType": "administrative.country",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.province",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.locality",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.neighborhood",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2f2f2"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.attraction",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.business",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.government",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.medical",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "hue": "#ff0000"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#46bcec"
            },
            {
                "visibility": "on"
            }
        ]
    }
];

	map.setOptions({styles: styles});	
}

/**
 * Data for the markers consisting of a name, a LatLng and a zIndex for
 * the order in which these markers should display on top of each
 * other.
 */

var penderita = [
  <?php //echo $lokasi_penderita; ?>
];

function setMarkers(map, locations) {
  
  var image = {
    url: 'img/blue_dot.PNG',
    size: new google.maps.Size(10, 10),
    origin: new google.maps.Point(0,0),
    anchor: new google.maps.Point(0, 0)
  };

  

  for (var i = 0; i < locations.length; i++) {
    var beach = locations[i];
    var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: image,
        title: beach[0],
        zIndex: beach[3]
    });
  }
  
  <?php echo $poly_map; ?>
  
}
google.maps.event.addDomListener(window, 'load', initialize);
    </script>
@stop