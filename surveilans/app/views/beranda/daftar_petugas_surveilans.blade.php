@extends('layouts.master')
@section('content')
<script>
function delete_data(){
	if (confirm('Yakin ingin mereset user ?')) return true; else return false;		
}

function hapus_data(){
	if (confirm('Yakin ingin menghapus data user ?')) return true; else return false;		
}
</script>
<?php
$type = Session::get('type');
?>
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <h4>
                        Data Petugas Surveilans
                    </h4>
                </div>
                <br>
                <div class="table-responsive">
                  <h4>
					Puskesmas
				  </h4>
				  <table id="puskesmas" class="uk-table uk-table-hover uk-table-striped uk-table-condensed table-bordered">
					<thead>
						<tr>
							<th class="span1">No.</th>
							<th class="span3">Nama Petugas</th>
							<th class="span2">Instansi</th>
							<th class="span2">Jabatan</th>
							<th class="span2">No Telp</th>
							<th class="span2">Email</th>
							<?php if($type=="kemenkes"): ?>
							<th>Password</th>
							<th>Aksi</th>
							<?php endif; ?>
						</tr>
					</thead>
					<tbody>
						<?php
						$id_profil = Session::get('id_profil');
						$q = "
							SELECT
								a.id,
								a.kode_faskes,
								a.alamat,
								a.penanggungjawab_1,
								a.penanggungjawab_2,
								b.puskesmas_id,
								b.kode_desa,
								b.kode_kec,
								b.kode_kab,
								b.kode_prop,
								b.puskesmas_name,
								c.first_name,
								c.last_name,
								c.email,
								c.no_telp,
								c.jabatan,
								c.saved_pass,
								c.id as user_id
							FROM profil_puskesmas a
							JOIN puskesmas b ON b.puskesmas_code_faskes=a.kode_faskes
							JOIN users c ON c.id=a.created_by
						";
						$data=DB::select($q);
						for($i=0;$i<count($data);$i++):
						?>
						<tr>
							<td><?php echo $i+1; ?></td>
							<td><?php echo $data[$i]->first_name." ".$data[$i]->last_name; ?></td>
							<td><?php echo $data[$i]->puskesmas_name; ?></td>
							<td><?php echo $data[$i]->jabatan; ?></td>
							<td><?php echo $data[$i]->no_telp; ?></td>
							<td><?php echo $data[$i]->email; ?></td>
							<?php if($type=="kemenkes"): ?>
							<td><?php echo $data[$i]->saved_pass; ?></td>
							<td>
								@if(Sentry::getUser()->email=='dev.jogja@gmail.com' || Sentry::getUser()->email=='root@gmail.com')
								<a class="btn btn-success btn-xs" href="<?php echo URL::to('delete_user/'.$data[$i]->user_id); ?>" onclick="return hapus_data();"><i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus"></i>Delete</a>
								@endif
								<a class="btn btn-warning btn-xs" href="<?php echo URL::to('reset_password/'.$data[$i]->user_id); ?>" onclick="return delete_data();"><i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus"></i>Reset Password</a>
							</td>
							<?php endif; ?>
						</tr>
						<?php endfor; ?>
					</tbody>
                  </table>

                  {{-- Petugas Rumah sakit --}}
				  <br>
				  <hr>
				  <h4>
					Rumah Sakit
				  </h4>
				  <table id="rs" class="uk-table uk-table-hover uk-table-striped uk-table-condensed table-bordered">
					<thead>
						<tr>
							<th class="span1">No.</th>
							<th class="span3">Nama Petugas</th>
							<th class="span2">Instansi</th>
							<th class="span2">Jabatan</th>
							<th class="span2">No Telp</th>
							<th class="span2">Email</th>
							<?php if($type=="kemenkes"): ?>
							<th>Password</th>
							<th>Aksi</th>
							<?php endif ?>
						</tr>
					</thead>
					<tbody>
						<?php
						$id_profil = Session::get('id_profil');
						$q = "
							SELECT
								a.id,
								a.kode_faskes,
								a.alamat,
								a.penanggungjawab_1,
								a.penanggungjawab_2,
								b.kode_faskes,
								b.kode_desa,
								b.kode_kec,
								b.kode_kab,
								b.kode_prop,
								b.nama_faskes,
								c.first_name,
								c.last_name,
								c.email,
								c.no_telp,
								c.jabatan,
								c.saved_pass,
								c.id as user_id
							FROM
								profil_rs AS a
							JOIN rumahsakit2 AS b ON a.kode_faskes=b.kode_faskes
							JOIN users AS c ON a.created_by=c.id
						";
						$data=DB::select($q);
						for($i=0;$i<count($data);$i++):
						?>
						<tr>
							<td><?php echo $i+1; ?></td>
							<td><?php echo $data[$i]->first_name." ".$data[$i]->last_name; ?></td>
							<td><?php echo $data[$i]->nama_faskes; ?></td>
							<td><?php echo $data[$i]->jabatan; ?></td>
							<td><?php echo $data[$i]->no_telp; ?></td>
							<td><?php echo $data[$i]->email; ?></td>
							<?php if($type=="kemenkes"): ?>
							<td><?php echo $data[$i]->saved_pass; ?></td>
							<td>
								@if(Sentry::getUser()->email=='dev.jogja@gmail.com' || Sentry::getUser()->email=='root@gmail.com')
								<a class="btn btn-success btn-xs" href="<?php echo URL::to('delete_user/'.$data[$i]->user_id); ?>" onclick="return hapus_data();"><i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus"></i>Delete</a>
								@endif
							  <a class="btn btn-warning btn-xs" href="<?php echo URL::to('reset_password/'.$data[$i]->user_id); ?>" onclick="return delete_data();"><i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus"></i>Reset Password</a>
							</td>
							<?php endif; ?>
						</tr>
						<?php endfor; ?>
					</tbody>
                  </table>

        {{-- Dinan Kesehatan Kabupaten --}}
				  <br>
				  <hr>
				  <h4>
					Dinas Kesehatan Kabupaten
				  </h4>
				  <table id="din-kes-kab" class="uk-table uk-table-hover uk-table-striped uk-table-condensed table-bordered">
					<thead>
						<tr>
							<th class="span1">No.</th>
							<th class="span3">Nama Petugas</th>
							<th class="span2">Instansi</th>
							<th class="span2">Jabatan</th>
							<th class="span2">No Telp</th>
							<th class="span2">Email</th>
							<?php if($type=="kemenkes"): ?>
							<th>Password</th>
							<th>Aksi</th>
							<?php endif ?>
						</tr>
					</thead>
					<tbody>
						<?php
						$id_profil = Session::get('id_profil');
						$q = "
							SELECT
								a.kode_kabupaten,
								a.alamat,
								a.penanggungjawab_1,
								a.penanggungjawab_2,
								b.id_provinsi,
								b.id_kabupaten,
								b.kabupaten,
								c.first_name,
								c.last_name,
								c.email,
								c.no_telp,
								c.jabatan,
								c.saved_pass,
								c.id as user_id
							FROM profil_dinkes_kabupaten a
							JOIN kabupaten b ON b.id_kabupaten=a.kode_kabupaten
							JOIN users c ON c.id=a.created_by
						";
						$data=DB::select($q);
						for($i=0;$i<count($data);$i++):
						?>
						<tr>
							<td><?php echo $i+1; ?></td>
							<td><?php echo $data[$i]->first_name." ".$data[$i]->last_name; ?></td>
							<td>DKK <?php echo $data[$i]->kabupaten; ?></td>
							<td><?php echo $data[$i]->jabatan; ?></td>
							<td><?php echo $data[$i]->no_telp; ?></td>
							<td><?php echo $data[$i]->email; ?></td>
							<?php if($type=="kemenkes"): ?>
							<td><?php echo $data[$i]->saved_pass; ?></td>
							<td>
								@if(Sentry::getUser()->email=='dev.jogja@gmail.com' || Sentry::getUser()->email=='root@gmail.com')
								<a class="btn btn-success btn-xs" href="<?php echo URL::to('delete_user/'.$data[$i]->user_id); ?>" onclick="return hapus_data();"><i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus"></i>Delete</a>
								@endif
							  <a class="btn btn-warning btn-xs" href="<?php echo URL::to('reset_password/'.$data[$i]->user_id); ?>" onclick="return delete_data();"><i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus"></i>Reset Password</a>
							</td>
							<?php endif; ?>
						</tr>
						<?php endfor; ?>
					</tbody>
                  </table>

        {{-- Dinas Kesehatan Provinsi --}}
				  <br>
				  <hr>
				  <h4>
					Dinas Kesehatan Provinsi
				  </h4>
				  <table id="din-kes-prov" class="uk-table uk-table-hover uk-table-striped uk-table-condensed table-bordered">
					<thead>
						<tr>
							<th class="span1">No.</th>
							<th class="span3">Nama Petugas</th>
							<th class="span2">Instansi</th>
							<th class="span2">Jabatan</th>
							<th class="span2">No Telp</th>
							<th class="span2">Email</th>
							<?php if($type=="kemenkes"): ?>
							<th>Password</th>
							<th>Aksi</th>
							<?php endif; ?>
						</tr>
					</thead>
					<tbody>
						<?php
						$q = "
							SELECT
								a.kode_provinsi,
								a.alamat,
								a.penanggungjawab_1,
								a.penanggungjawab_2,
								b.id_provinsi,
								c.first_name,
								c.last_name,
								c.email,
								c.no_telp,
								b.provinsi,
								c.jabatan,
								c.saved_pass,
								c.id as user_id
							FROM profil_dinkes_provinsi a
							JOIN provinsi b ON b.id_provinsi=a.kode_provinsi
							JOIN users c ON c.id=a.created_by
						";
						$data=DB::select($q);
						for($i=0;$i<count($data);$i++):
						?>
						<tr>
							<td><?php echo $i+1; ?></td>
							<td><?php echo $data[$i]->first_name." ".$data[$i]->last_name; ?></td>
							<td>DKP <?php echo $data[$i]->provinsi; ?></td>
							<td><?php echo $data[$i]->jabatan; ?></td>
							<td><?php echo $data[$i]->no_telp; ?></td>
							<td><?php echo $data[$i]->email; ?></td>
							<?php if($type=="kemenkes"): ?>
							<td><?php echo $data[$i]->saved_pass; ?></td>
							<td>
								@if(Sentry::getUser()->email=='dev.jogja@gmail.com' || Sentry::getUser()->email=='root@gmail.com')
								<a class="btn btn-success btn-xs" href="<?php echo URL::to('delete_user/'.$data[$i]->user_id); ?>" onclick="return hapus_data();"><i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus"></i>Delete</a>
								@endif
							  <a class="btn btn-warning btn-xs" href="<?php echo URL::to('reset_password/'.$data[$i]->user_id); ?>" onclick="return delete_data();"><i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus"></i>Reset Password</a>
							</td>
							<?php endif; ?>
						</tr>
						<?php endfor; ?>
					</tbody>
                  </table>


    {{-- Kemenkes --}}
				  <br>
				  <hr>
				  <h4>
					Kementerian Kesehatan & WHO
				  </h4>
				  <table id="kemenkes" class="uk-table uk-table-hover uk-table-striped uk-table-condensed table-bordered">
					<thead>
						<tr>
							<th class="span1">No.</th>
							<th class="span3">Nama Petugas</th>
							<th class="span2">Instansi</th>
							<th class="span2">Jabatan</th>
							<th class="span2">No Telp</th>
							<th class="span2">Email</th>
							<?php if($type=="kemenkes"): ?>
							<th>Password</th>
							<th>Aksi</th>
							<?php endif; ?>
						</tr>
					</thead>
					<tbody>
						<?php
						$q = "
							SELECT
								c.first_name,
								c.last_name,
								c.email,
								c.no_telp,
								c.jabatan,
								c.saved_pass,
								c.id as user_id
							FROM profil_kemenkes a
							JOIN users c ON c.id=a.created_by
						";
						$data=DB::select($q);
						for($i=0;$i<count($data);$i++):
						?>
						<tr>
							<td><?php echo $i+1; ?></td>
							<td><?php echo $data[$i]->first_name." ".$data[$i]->last_name; ?></td>
							<td>KEMENKES / WHO</td>
							<td><?php echo $data[$i]->jabatan; ?></td>
							<td><?php echo $data[$i]->no_telp; ?></td>
							<td><?php echo $data[$i]->email; ?></td>
							<?php if($type=="kemenkes"): ?>
							<td><?php echo $data[$i]->saved_pass; ?></td>
							<td>
								@if(Sentry::getUser()->email=='dev.jogja@gmail.com' || Sentry::getUser()->email=='root@gmail.com')
								<a class="btn btn-success btn-xs" href="<?php echo URL::to('delete_user/'.$data[$i]->user_id); ?>" onclick="return hapus_data();"><i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus"></i>Delete</a>
								@endif
							  <a class="btn btn-warning btn-xs" href="<?php echo URL::to('reset_password/'.$data[$i]->user_id); ?>" onclick="return delete_data();"><i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus"></i>Reset Password</a>
							</td>
							<?php endif; ?>
						</tr>
						<?php endfor; ?>
					</tbody>
                  </table>
                  <ul class="pagination pagination-sm">

                  </ul>
                </div>
            </div>
            <!--/.module-body-->
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<!--/.span9-->

<script type="text/javascript">
	$(document).ready(function() {
	    $('#puskesmas').dataTable();
	    $('#rs').dataTable();
	    $('#din-kes-kab').dataTable();
	    $('#din-kes-prov').dataTable();
	    $('#kemenkes').dataTable();
	});
</script>



@stop