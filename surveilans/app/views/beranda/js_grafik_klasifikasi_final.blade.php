<?php 
	$type = Session::get('type');
	$kd_faskes = Session::get('kd_faskes');

	$penyakit = Input::get('penyakit');
	$arr_penyakit['campak'] = "Campak";
	$arr_penyakit['afp'] = "Polio";
	$arr_penyakit['difteri'] = "Difteri";
	$arr_penyakit['tetanus'] = "Tetanus Neonatorum";
	$arr_penyakit['crs'] = "CRS";
	
	$arr_nama_bulan[1] = "Januari";
	$arr_nama_bulan[2] = "Februari";
	$arr_nama_bulan[3] = "Maret";
	$arr_nama_bulan[4] = "April";
	$arr_nama_bulan[5] = "Mei";
	$arr_nama_bulan[6] = "Juni";
	$arr_nama_bulan[7] = "Juli";
	$arr_nama_bulan[8] = "Agustus";
	$arr_nama_bulan[9] = "September";
	$arr_nama_bulan[10] = "Oktober";
	$arr_nama_bulan[11] = "November";
	$arr_nama_bulan[12] = "Desember";
	
	$x_data = "";
	if($type == "rs"){
		$asd = "AND i.kode_faskes='".$kd_faskes."'";
	} else if($type == "puskesmas") {
		$asd = "AND h.puskesmas_code_faskes='".$kd_faskes."' ";
	} else if($type == "kabupaten") {
		$asd = "AND (h.kode_kab='".$kd_faskes."' OR i.kode_kab='".$kd_faskes."')";
	} else if($type == "provinsi") {
		$asd = "AND (h.kode_prop='".$kd_faskes."' OR i.kode_prop='".$kd_faskes."')";
	} else if($type == "kemenkes") {
		$asd = "";
	}
	
	//CAMPAK
	$data=DB::select("
		SELECT 
			COUNT(*) AS jml 
		FROM hasil_uji_lab_campak a 
		JOIN pasien b ON b.id_pasien=a.id_pasien
		JOIN campak c ON c.id_campak=a.id_campak
		LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
		LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
		LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
		LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
		LEFT JOIN puskesmas h  ON h.puskesmas_id=c.id_tempat_periksa
		LEFT JOIN rumahsakit2 i ON i.id=c.id_tempat_periksa
		WHERE 
		c.deleted_at IS NULL
		AND c.klasifikasi_final IN ('1','2','3')
		AND
			CASE
				WHEN c.tanggal_timbul_rash='1970-01-01' OR c.tanggal_timbul_rash='0000-00-00' THEN YEAR(c.tanggal_timbul_demam)
				ELSE YEAR(c.tanggal_timbul_rash)
			END=YEAR(NOW())
		$asd
	");
	if($data[0]->jml > 0)$x_data .= 	"['Campak',".$data[0]->jml."]";
	
	//AFP
	$data=DB::select("
		SELECT 
			COUNT(*) AS jml 
		FROM hasil_uji_lab_afp a 
		JOIN pasien b ON b.id_pasien=a.id_pasien
		JOIN afp c ON c.id_afp=a.id_afp
		LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
		LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
		LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
		LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
		LEFT JOIN puskesmas h  ON h.puskesmas_id=c.id_tempat_periksa
		WHERE 
		1=1
		AND c.klasifikasi_final IN ('1')
		-- AND DATE_FORMAT(c.tanggal_mulai_lumpuh,'%Y') >= '2014'
		AND DATE_FORMAT(c.tanggal_mulai_lumpuh,'%Y') = YEAR(NOW())
		$asd
	");
	if($data[0]->jml > 0)$x_data .= 	",['AFP',".$data[0]->jml."]";
	
	//DIFTERI
	$data=DB::select("
		SELECT 
			COUNT(*) AS jml 
		FROM hasil_uji_lab_difteri a 
		JOIN pasien b ON b.id_pasien=a.id_pasien
		JOIN difteri c ON c.id_difteri=a.id_difteri
		LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
		LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
		LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
		LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
		LEFT JOIN puskesmas h  ON h.puskesmas_id=c.id_tempat_periksa
		WHERE 
		1=1
		AND c.klasifikasi_final IN ('konfirm')
		-- AND DATE_FORMAT(c.tanggal_timbul_demam,'%Y') >= '2014'
		AND DATE_FORMAT(c.tanggal_timbul_demam,'%Y') <= YEAR(NOW())
		$asd
	");
	if($data[0]->jml > 0)$x_data .= 	",['Difteri',".$data[0]->jml."]";
	
	//TETANUS
	$data=DB::select("
		SELECT 
			COUNT(*) AS jml 
		FROM pasien_terserang_tetanus a 
		JOIN pasien b ON b.id_pasien=a.id_pasien
		JOIN tetanus c ON c.id_tetanus=a.id_tetanus
		LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
		LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
		LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
		LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
		LEFT JOIN puskesmas h  ON h.puskesmas_id=c.id_tempat_periksa
		WHERE 
		c.deleted_at IS NULL
		AND c.klasifikasi_akhir IN ('Konfirm TN')
		-- AND DATE_FORMAT(c.tanggal_mulai_sakit,'%Y') >= '2014'
		AND DATE_FORMAT(c.tanggal_mulai_sakit,'%Y') <= YEAR(NOW())
		$asd
	");
	if($data[0]->jml > 0)$x_data .= 	",['Tetanus Neonatorum',".$data[0]->jml."]";

	$data=DB::SELECT("
		SELECT
			COUNT(*) AS jml
		FROM crs AS a
		JOIN pasien AS b ON a.id_pasien=b.id_pasien
		LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
		LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
		LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
		LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
		LEFT JOIN puskesmas h  ON h.puskesmas_id=a.id_tempat_periksa
		LEFT JOIN rumahsakit2 i ON i.id=a.id_tempat_periksa
		WHERE a.deleted_at IS NULL
		AND a.klasifikasi_final IN ('CRS pasti(Lab Positif)','CRS Klinis')
		-- AND DATE_FORMAT(a.tgl_mulai_sakit,'%Y') >= '2014'
		AND DATE_FORMAT(a.tgl_mulai_sakit,'%Y') <= YEAR(NOW())
		$asd
		");
	if($data[0]->jml > 0)$x_data .= 	",['CRS',".$data[0]->jml."]";
	
?>
	
	
$('#container_klasifikasi_final').highcharts({
	chart: {
		type: 'pie',
		options3d: {
			enabled: true,
			alpha: 45
		}
	},
	title: {
		text: 'Jumlah Penderita PD3I'
	},
	subtitle: {
		text: 'Tahun <?php  echo date('Y'); ?>'
	},
	plotOptions: {
		pie: {
			innerSize: 70,
			depth: 45
		}
	},
	series: [{
		name: 'Jumlah Penderita',
		data: [
			<?php echo $x_data; ?>
		],
		dataLabels: {
			formatter: function () {
				// display only if larger than 1
				return this.y >= 1 ? '<br>' + this.point.name + ' : ' + this.y + ' jiwa'  : null;
			}
		}
	}]
});
		