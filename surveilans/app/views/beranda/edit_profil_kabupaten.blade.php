@include('admin.header')
<script>
$(document).ready(function(){
	$('#inputForm').submit(function() {
		loading_state();
		$('#inputForm').ajaxSubmit({
			type: 'POST',
			dataType:'json',
			success:function(jsonData) {
				$('#status').html(jsonData.message);
				if(jsonData.status == 'success')location.href  = jsonData.address;
				//alert(jsonData.address);
			}
		});
		return false;
	});
});

function loading_state(){
	$('#status').html('Loading...');
}	
</script>

@include('admin.heading')

<div class="wrapper">
    <div class="container">
        <div class="row">
            <!--/.span3-->
            <div class="span12">
                <div class="content">
                    <div class="btn-controls">
                        
                        <div class="btn-box-row row-fluid">
                            <ul class="widget widget-usage unstyled span12">
                                <div class="module-head">
                                    <h3>Profil Kabupaten</h3>
                                </div>
                                <div class="module-body">
									<?php
									$id_profil = Session::get('id_profil');
									$q = "
										SELECT 
											a.kode_kabupaten,
											a.alamat,
											a.penanggungjawab_1,
											a.penanggungjawab_2,
											b.id_provinsi,
											b.id_kabupaten
										FROM profil_dinkes_kabupaten a
										JOIN kabupaten b ON b.id_kabupaten=a.kode_kabupaten
										WHERE a.id='".$id_profil."'
									";
									$data=DB::select($q);
									?>
									{{Form::open(array('url'=>'save_edit_profil_kabupaten','class'=>'form-horizontal row-fluid','id'=>'inputForm'))}}
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Provinsi</label>
											<div class="controls span6">
												<select name="province_id" id="province_id" style="width:200px" onchange="get_district();" >
													<option value="">--- Semua ---</option>
													<?php
													$q = "
														SELECT 
															id_provinsi,
															provinsi
														FROM provinsi
														order by provinsi ASC
													";
													$combo_province=DB::select($q);
													?>
													<?php for($i=0;$i<count($combo_province);$i++) :?>
													<?php if($combo_province[$i]->id_provinsi == $data[0]->id_provinsi) $sel='selected'; else $sel='';?>
													<option value="<?php echo $combo_province[$i]->id_provinsi?>" <?php echo $sel; ?>><?php echo $combo_province[$i]->provinsi; ?></option>
													<?php endfor;?>
												</select>
												
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Kabupaten</label>
											<div class="controls span6">
												<select name="district_id" id="district_id" style="width: 190px;"  onchange="get_sub_district();">
													<option value="">--- Semua ---</option>
													<?php
													$q = "
														SELECT 
															id_kabupaten,
															kabupaten
														FROM kabupaten
														WHERE id_provinsi='".$data[0]->id_provinsi."'
														order by kabupaten ASC
													";
													$combo_district=DB::select($q);
													?>
													<?php for($i=0;$i<count($combo_district);$i++) :?>
													<?php if($combo_district[$i]->id_kabupaten == $data[0]->id_kabupaten) $sel='selected'; else $sel='';?>
													<option value="<?php echo $combo_district[$i]->id_kabupaten?>" <?php echo $sel; ?>><?php echo $combo_district[$i]->kabupaten; ?></option>
													<?php endfor;?>
												</select>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Alamat</label>
											<div class="controls span6">
												<input name="alamat" type="text" value="<?php echo $data[0]->alamat; ?>" class="form-control span8"/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Penanggungjawab 1</label>
											<div class="controls span6">
												<input name="penanggungjawab_1" type="text" value="<?php echo $data[0]->penanggungjawab_1; ?>" class="form-control span8"/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Penanggungjawab 2</label>
											<div class="controls span6">
												<input name="penanggungjawab_2" type="text" value="<?php echo $data[0]->penanggungjawab_2; ?>" class="form-control span8"/>
											</div>
										</div>
											
											
										<div class="control-group">
											<div class="controls">
												<div class="span3">
													<button type="submit" class="btn btn-success">Simpan</button>
													<button type="reset" class="btn">Batal</button>
													<a href="{{URL::to('setting/level')}}" class="btn btn-warning">Kembali</a>
												</div>
												<div class="span5" id="status">&nbsp;</div>
											</div>
										</div>
									{{Form::close()}}
							</div>
                            </ul>
                        </div>
                    </div>
                    
                    <!--/.module-->
                </div>
                <!--/.content-->
            </div>
            <!--/.span9-->
        </div>
    </div>
    <!--/.container-->
</div>

<script>
	
function get_district(){
	$.ajax({
		//dataType:'json',
		data: 'province_id='+$('#province_id').val(),
		url:'{{URL::to("region/get_district")}}',
		success:function(data) {
			$('#district_id').html(data);
			if($('#province_id').val()=='') {
				$('#district_id').attr('disabled',true);
				$('#sub_district_id').attr('disabled',true);
				$('#village_id').attr('disabled',true);
			} else {
				$('#district_id').removeAttr('disabled');
			}	
			get_sub_district();
			get_village();
		}
	});
}	

function get_sub_district(){
	$.ajax({
		//dataType:'json',
		data: 'district_id='+$('#district_id').val(),
		url:'{{URL::to("region/get_sub_district")}}',
		success:function(data) {
			//alert(data);
			$('#sub_district_id').html(data);
			if($('#district_id').val()=='') {
				$('#sub_district_id').attr('disabled',true);
				$('#village_id').attr('disabled',true);
			} else {
				$('#sub_district_id').removeAttr('disabled');
			}
			get_village();
		}
	});
}	

function get_village(){
	$.ajax({
		//dataType:'json',
		data: 'sub_district_id='+$('#sub_district_id').val(),
		url:'{{URL::to("region/get_village")}}',
		success:function(data) {
			//alert(data);
			$('#village_id').html(data);
			if($('#sub_district_id').val()=='') $('#village_id').attr('disabled',true);
			else $('#village_id').removeAttr('disabled');
		}
	});
}
	
</script>

<!--/.wrapper-->
<div class="footer">
    <div class="container">
        <b class="copyright">&copy; 2014 surveilan PD3I </b>All rights reserved.
    </div>
</div>

</body>
</html>
