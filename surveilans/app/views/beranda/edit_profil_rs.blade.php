@include('admin.header')
<script>
$(document).ready(function(){
	$('#inputForm').submit(function() {
		loading_state();
		$('#inputForm').ajaxSubmit({
			type: 'POST',
			dataType:'json',
			success:function(jsonData) {
				$('#status').html(jsonData.message);
				if(jsonData.status == 'success')location.href  = jsonData.address;
				//alert(jsonData.address);
			}
		});
		return false;
	});
});

function loading_state(){
	$('#status').html('Loading...');
}	
</script>
@include('admin.heading')
<div class="wrapper">
    <div class="container">
        <div class="row">
            <!--/.span3-->
            <div class="span12">
                <div class="content">
                    <div class="btn-controls">
                        
                        <div class="btn-box-row row-fluid">
                            <ul class="widget widget-usage unstyled span12">
                                <div class="module-head">
                                    <h3>Profil Rumah Sakit</h3>
                                </div>
                                <div class="module-body">
									<?php
									$id_profil = Session::get('id_profil');
									$q = "
										SELECT 
											a.id,
											a.kode_faskes,
											a.konfirmasi,
											a.alamat,
											a.penanggungjawab_1,
											a.penanggungjawab_2,
											LEFT(a.kode_faskes, 2) as kode_prop,
											LEFT(a.kode_faskes, 4) as kode_kab
										FROM profil_rs a
										WHERE a.id='".$id_profil."'
									";
									$data=DB::select($q);
									?>
									{{Form::open(array('url'=>'save_edit_profil_rs','class'=>'form-horizontal row-fluid','id'=>'inputForm'))}}
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Provinsi</label>
											<div class="controls span6">
												<select name="province_id" id="province_id" style="width:200px" onchange="get_district();" >
													<option value="">--- Semua ---</option>
													<?php
													$q = "
														SELECT 
															id_provinsi,
															provinsi
														FROM provinsi
														order by provinsi ASC
													";
													$combo_province=DB::select($q);
													?>
													<?php for($i=0;$i<count($combo_province);$i++) :?>
													<?php if($combo_province[$i]->id_provinsi == $data[0]->kode_prop) $sel='selected'; else $sel='';?>
													<option value="<?php echo $combo_province[$i]->id_provinsi?>" <?php echo $sel; ?>><?php echo $combo_province[$i]->provinsi; ?></option>
													<?php endfor;?>
												</select>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Kabupaten</label>
											<div class="controls span6">
												<select name="district_id" id="district_id" style="width: 190px;" onchange="get_sub_district();">
													<option value="">--- Semua ---</option>
													<?php
													$q = "
														SELECT 
															id_kabupaten,
															kabupaten
														FROM kabupaten
														WHERE id_provinsi='".$data[0]->kode_prop."'
														order by kabupaten ASC
													";
													$combo_kabupaten=DB::select($q);
													?>
													<?php for($i=0;$i<count($combo_kabupaten);$i++) :?>
													<?php if($combo_kabupaten[$i]->id_kabupaten == $data[0]->kode_kab) $sel='selected'; else $sel='';?>
													<option value="<?php echo $combo_kabupaten[$i]->id_kabupaten?>" <?php echo $sel; ?>><?php echo $combo_kabupaten[$i]->kabupaten; ?></option>
													<?php endfor;?>
												</select>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Rumah Sakit</label>
											<div class="controls span6">
												<select name="puskesmas_id" id="puskesmas_id" style="width: 190px;" onchange="get_code();">
													<option value="">--- Semua ---</option>
													<?php
													$q = "
														SELECT 
															kode_faskes,
															nama_faskes
														FROM rumahsakit2
														WHERE kode_faskes LIKE '".$data[0]->kode_kab."%'
														order by nama_faskes ASC
													";
													$combo_rs=DB::select($q);
													?>
													<?php for($i=0;$i<count($combo_rs);$i++) :?>
													<?php if($combo_rs[$i]->kode_faskes == $data[0]->kode_faskes) $sel='selected'; else $sel='';?>
													<option value="<?php echo $combo_rs[$i]->kode_faskes?>" <?php echo $sel; ?>><?php echo $combo_rs[$i]->nama_faskes; ?></option>
													<?php endfor;?> 	
												</select>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Kode Rumah Sakit</label>
											<div class="controls span6">
												<input name="puskesmas_code" id="puskesmas_code" type="text" value="<?php echo $data[0]->kode_faskes; ?>" class="form-control span8"/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Alamat</label>
											<div class="controls span6">
												<input name="alamat" id="alamat" type="text" value="<?php echo $data[0]->alamat; ?>" class="form-control span8"/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Penanggungjawab 1</label>
											<div class="controls span6">
												<input name="penanggungjawab_1" type="text" value="<?php echo $data[0]->penanggungjawab_1; ?>" class="form-control span8"/>
											</div>
										</div>
										
										<div class="control-group">
											<label class="control-label span3" for="basicinput">Penanggungjawab 2</label>
											<div class="controls span6">
												<input name="penanggungjawab_2" type="text" value="<?php echo $data[0]->penanggungjawab_2; ?>" class="form-control span8"/>
											</div>
										</div>										
										
										<div class="control-group">
											<div class="controls">
												<div class="span3">
													<button type="submit" class="btn btn-success">Simpan</button>
													<button type="reset" class="btn">Batal</button>
													<a href="{{URL::to('setting/level')}}" class="btn btn-warning">Kembali</a>
												</div>
												<div class="span5" id="status">&nbsp;</div>
											</div>
										</div>
									{{Form::close()}}
							</div>
                            </ul>
                        </div>
                    </div>
                    
                    <!--/.module-->
                </div>
                <!--/.content-->
            </div>
            <!--/.span9-->
        </div>
    </div>
    <!--/.container-->
</div>

<script>
	
function get_district(){
	$.ajax({
		//dataType:'json',
		data: 'province_id='+$('#province_id').val(),
		url:'{{URL::to("region/get_district")}}',
		success:function(data) {
			$('#district_id').html(data);
			if($('#province_id').val()=='') {
				$('#district_id').attr('disabled',true);
				$('#sub_district_id').attr('disabled',true);
				$('#puskesmas_id').attr('disabled',true);
			} else {
				$('#district_id').removeAttr('disabled');
			}	
			get_sub_district();
			get_puskesmas();
		}
	});
}	

function get_sub_district(){
	$.ajax({
		//dataType:'json',
		data: 'district_id='+$('#district_id').val(),
		url:'{{URL::to("region/get_sub_district")}}',
		success:function(data) {
			//alert(data);
			$('#sub_district_id').html(data);
			if($('#district_id').val()=='') {
				$('#sub_district_id').attr('disabled',true);
				$('#puskesmas_id').attr('disabled',true);
			} else {
				$('#sub_district_id').removeAttr('disabled');
			}
			get_puskesmas();
		}
	});
}	

function get_puskesmas(){
	$.ajax({
		//dataType:'json',
		data: 'sub_district_id='+$('#sub_district_id').val(),
		url:'{{URL::to("region/get_puskesmas")}}',
		success:function(data) {
			//alert(data);
			$('#puskesmas_id').html(data);
			if($('#sub_district_id').val()=='') $('#puskesmas_id').attr('disabled',true);
			else $('#puskesmas_id').removeAttr('disabled');
		}
	});
}

function get_code(){
	$('#puskesmas_code').val($('#puskesmas_id').val());
	$.ajax({
		//dataType:'json',
		data: 'puskesmas_code='+$('#puskesmas_code').val(),
		url:'{{URL::to("region/get_puskesmas_detail")}}',
		success:function(data) {
			//alert(data);
			$('#alamat').val(data);
			if($('#sub_district_id').val()=='') $('#puskesmas_code').attr('disabled',true);
			else $('#puskesmas_code').removeAttr('disabled');
		}
	});
}
</script>

<!--/.wrapper-->
<div class="footer">
    <div class="container">
        <b class="copyright">&copy; 2014 surveilan PD3I </b>All rights reserved.
    </div>
</div>

</body>
</html>
