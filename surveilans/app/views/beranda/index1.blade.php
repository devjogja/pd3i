@extends('layouts.master')
@section('content')

<style>
	.tombol{
		width:30px;
		height:150px;
		background:#eee;
		cursor:pointer;
	}
	
	.tombol.kiri{
		float:left;
	}
	
	.tombol.kanan{
		float:right;
	}
	
	.tombol.kiri:hover{ 
		background:#ddd;
	}
	
	.tombol.kanan:hover{
		background:#ddd;
	}
</style>

<!--/.span3-->
<div class="span12">
	<div class="content">
		@if(Sentry::check())
		<div class="btn-controls">
			<ul class="widget widget-usage unstyled">
				<div class="module-body">
					<div class="control-group">
						<div class="controls row-fluid">
							<div class="span11">
								<div class="span10">
									SELAMAT DATANG DI   
									<?php
									$type = Session::get('type');
									$kd_faskes = Session::get('kd_faskes');
									if($type == "puskesmas"){
										$q = "
										SELECT b.puskesmas_name,b.alamat
										FROM puskesmas b
										WHERE b.puskesmas_code_faskes='".$kd_faskes."'
										";
										$data=DB::select($q);
										echo $nama_instansi = "PUSKESMAS ". $data[0]->puskesmas_name;
								/*for($i=0;$i<count($data);$i++){
									echo $nama_instansi = "PUSKESMAS ". $data[0]->puskesmas_name;
								}*/
							} else if($type == "rs"){
								$q = "
								SELECT b.nama_faskes,b.alamat
								FROM rumahsakit2 b
								WHERE b.kode_faskes='".$kd_faskes."'
								";
								$data=DB::select($q);
								echo $data[0]->nama_faskes;
								/*for($i=0;$i<count($data);$i++){
									echo $nama_instansi = $data[0]->nama_faskes;
								}*/
							} else if($type == "kabupaten"){
								$q = "
								SELECT * FROM kabupaten b 
								WHERE b.id_kabupaten='".$kd_faskes."'
								";
								$data=DB::select($q);
								echo $nama_instansi = "DINAS KESEHATAN KABUPATEN ". $data[0]->kabupaten;
								/*for($i=0;$i<count($data);$i++){
									echo $nama_instansi = "DINAS KESEHATAN KABUPATEN ". $data[0]->kabupaten;
								}*/
							} else if($type == "provinsi"){
								$q = "
								SELECT * FROM provinsi b 
								WHERE b.id_provinsi='".$kd_faskes."'
								";
								$data=DB::select($q);
								echo $nama_instansi = "DINAS KESEHATAN PROVINSI ".$data[0]->provinsi;
								/*for($i=0;$i<count($data);$i++){
									echo $nama_instansi = "DINAS KESEHATAN PROVINSI ".$data[0]->provinsi;
								}*/
							} else if($type == "laboratorium"){
								$q = "
								SELECT * FROM laboratorium b 
								WHERE b.lab_code='".$kd_faskes."'
								";
								$data=DB::select($q);
								echo $nama_instansi = "LABORATORIUM ".$data[0]->nama_laboratorium;
								/*for($i=0;$i<count($data);$i++){
									echo $nama_instansi = "LABORATORIUM ".$data[0]->nama_laboratorium;
								}*/
							} else if($type == "kemenkes"){
								echo " KEMENTERIAN KESEHATAN";
							}
							?>
							<a href="pilih_profil" class="btn btn-success"> Pilih Profil </a> 
						</div>
						<div class="span2">
							<a href="daftar_petugas_surveilans" class="btn btn-success"> Daftar Petugas Surveilans </a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</ul>
</div>
@if(Sentry::getUser()->hak_akses==6 || Sentry::getUser()->hak_akses==7 || Sentry::getUser()->hak_akses==2 || Sentry::getUser()->hak_akses==1 || Sentry::getUser()->hak_akses==3 || Sentry::getUser()->hak_akses==4)
<div style="clear:both;"></div>
<div class="btn-controls">
	<div class="btn-box-row row-fluid">
		<div class="tombol kiri">
			<img src="{{URL::to('assets/img/icon/btn1.png')}}">
		</div>
		<div class="tombol kanan">
			<img src="{{URL::to('assets/img/icon/btn2.png')}}">
		</div>

		<a href="{{URL::to('campak')}}" class="btn-box small span2">
			<img src="{{URL::to('assets/img/icon/campak.png')}}" style="width:70px;">
			<p class="text-muted">
				Campak</p>
			</a><a href="{{URL::to('tetanus')}}" class="btn-box small span2">
			<img src="{{URL::to('assets/img/icon/tetanus.png')}}" style="width:70px;"> 
			<p class="text-muted">
				Tetanus Neonatorum</p>
			</a><a href="{{URL::to('difteri')}}" class="btn-box small span2">
			<img src="{{URL::to('assets/img/icon/difteri.png')}}" style="width:70px;">
			<p class="text-muted">
				Difteri</p>
			</a>
		</a><a href="{{URL::to('afp')}}" class="btn-box small span2">
		<img src="{{URL::to('assets/img/icon/polio.png')}}" style="width:70px;">
		<p class="text-muted">
			AFP</p>
		</a>
		<a href="{{URL::to('crs')}}" class="btn-box small span2">
			<img src="{{URL::to('assets/img/icon/crs.png')}}" style="width:70px;">
			<p class="text-muted">CRS</p>
		</a>
	</div>
</div>
@elseif(Sentry::getUser()->hak_akses==5)
<div class="btn-controls">
	<div class="btn-box-row row-fluid">
		<div class="tombol kiri">
			<img src="{{URL::to('assets/img/icon/btn1.png')}}">
		</div>
		<div class="tombol kanan">
			<img src="{{URL::to('assets/img/icon/btn2.png')}}">
		</div>

		<a href="{{URL::to('labs/campak')}}" class="btn-box span2">
			<img src="{{URL::to('assets/img/icon/campak.png')}}" style="width:70px;">
			<p class="text-muted">Campak</p>
		</a>
		<a href="{{URL::to('labs/afp')}}" class="btn-box span2">
			<img src="{{URL::to('assets/img/icon/polio.png')}}" style="width:70px;">
			<p class="text-muted">AFP</p>
		</a>
		<a href="{{URL::to('labs/crs')}}" class="btn-box span2">
			<img src="{{URL::to('assets/img/icon/crs.png')}}" style="width:70px;">
			<p class="text-muted">CRS</p>
		</a> 

		<a href="#" class="btn-box  span2">
			<img src="{{URL::to('assets/img/icon/btn_none.png')}}" style="width:70px;">
			<p class="text-muted">
				Nama Penyakit</p>
			</a>
		</div>
	</div>
	@endif
	@endif
	<!--/#btn-controls-->
	<div class="module">
		<div class="module-head">
			<h3>
				Graph</h3>
			</div>
			<div class="module-body">
				<div class="chart inline-legend grid">
					<div id="container_klasifikasi_final" class="span4"></div>
					@if(Sentry::getUser()->hak_akses!=5)
					<div class="span2">
						<?php
						$type = Session::get('type');
						$kd_faskes = Session::get('kd_faskes');
						$asd = "";
						if($type == "puskesmas"){
							$asd = "AND h.puskesmas_code_faskes = '".$kd_faskes."'"; 
						} else if($type == "kabupaten"){
							$asd = "AND (h.kode_kab = '".$kd_faskes."' OR i.kode_kab='".$kd_faskes."')"; 
						} else if($type == "provinsi"){
							$asd = "AND (h.kode_prop = '".$kd_faskes."' OR i.kode_prop='".$kd_faskes."')";
						} else if($type == "rs"){
							$asd = "AND i.kode_faskes='".$kd_faskes."'";
						} else {
							$asd = ""; 
						}

						?>
						<table style="width:80%;">
							<tr><td><span class="btn" style="width:85%;">Kasus PD3I <br>Pending / Jml Spesimen / Total<br><?php echo date('Y');?></span></td></tr>
							<tr>
								<td>
									<span class="btn btn-success" style="width:85%;">
										Campak
										<br>
										<?php
										$q = "
										SELECT count(*) as jml FROM campak AS a
										LEFT JOIN puskesmas AS h ON a.id_tempat_periksa=h.puskesmas_id
										LEFT JOIN rumahsakit2 AS i ON a.id_tempat_periksa=i.id
										JOIN uji_spesimen AS d ON a.id_campak=d.id_campak
										WHERE a.deleted_at IS NULL
										AND (a.klasifikasi_final IS NULL OR a.klasifikasi_final='')
										AND CASE
										WHEN a.tanggal_timbul_demam NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(a.tanggal_timbul_demam)
										WHEN a.tanggal_timbul_rash NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(a.tanggal_timbul_rash)
										END = YEAR(CURDATE())
										$asd
										";
										$data=DB::select($q);
										$jml = 0;
										if(count($data) > 0) $jml = $data[0]->jml;
										if(Sentry::getUser()->hak_akses==7)
											$jml = 0;
										echo $jml;
										?> /
										<?php
										$q = "
										SELECT count(*) AS jml FROM campak AS a
										LEFT JOIN puskesmas AS h ON a.id_tempat_periksa=h.puskesmas_id
										LEFT JOIN rumahsakit2 AS i ON a.id_tempat_periksa=i.id
										JOIN uji_spesimen AS d ON a.id_campak=d.id_campak
										WHERE a.deleted_at IS NULL
										AND CASE
										WHEN a.tanggal_timbul_demam NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(a.tanggal_timbul_demam)
										WHEN a.tanggal_timbul_rash NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(a.tanggal_timbul_rash)
										END = YEAR(CURDATE())
										$asd
										";
										$data=DB::select($q);
										$jml = 0;
										if(count($data) > 0) $jml = $data[0]->jml;
										if(Sentry::getUser()->hak_akses==7)
											$jml = 0;
										echo $jml;
										?> /
										<?php
										$q = "
										SELECT count(*) AS jml FROM campak AS a
										LEFT JOIN puskesmas AS h ON a.id_tempat_periksa=h.puskesmas_id
										LEFT JOIN rumahsakit2 AS i ON a.id_tempat_periksa=i.id
										WHERE a.deleted_at IS NULL
										AND CASE
										WHEN a.tanggal_timbul_demam NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(a.tanggal_timbul_demam)
										WHEN a.tanggal_timbul_rash NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(a.tanggal_timbul_rash)
										END = YEAR(CURDATE())
										$asd
										";

										$data=DB::select($q);
										$jml = 0;
										if(count($data) > 0) $jml = $data[0]->jml;
										if(Sentry::getUser()->hak_akses==7)
											$jml = 0;
										echo $jml;
										?>
									</span>
								</td>
							</tr>
							<tr>
								<td>
									<span class="btn btn-success" style="width:85%;">
										Difteri
										<br>
										<?php
										$q = "
										SELECT count(*) as jml 
										FROM difteri c
										LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
										LEFT JOIN rumahsakit2 AS i ON c.id_tempat_periksa=i.id
										WHERE
										c.deleted_at IS NULL
										AND (klasifikasi_final='' OR klasifikasi_final IS NULL)
										AND CASE
										WHEN c.tanggal_diambil_spesimen_hidung NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(c.tanggal_diambil_spesimen_hidung)
										WHEN c.tanggal_diambil_spesimen_tenggorokan NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(c.tanggal_diambil_spesimen_tenggorokan)
										END = YEAR(CURDATE())
										AND CASE
										WHEN c.tanggal_timbul_demam NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(c.tanggal_timbul_demam)
										END = YEAR(CURDATE())
										$asd	
										";
										$data=DB::select($q);
										$jml = 0;
										if(count($data) > 0) $jml = $data[0]->jml;
										if(Sentry::getUser()->hak_akses==7)
											$jml = 0;
										echo $jml;
										?> /
										<?php
										$q = "
										SELECT count(*) as jml 
										FROM difteri c
										LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
										LEFT JOIN rumahsakit2 AS i ON c.id_tempat_periksa=i.id
										WHERE 
										c.deleted_at IS NULL
										AND CASE
										WHEN c.tanggal_diambil_spesimen_hidung NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(c.tanggal_diambil_spesimen_hidung)
										WHEN c.tanggal_diambil_spesimen_tenggorokan NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(c.tanggal_diambil_spesimen_tenggorokan)
										END = YEAR(CURDATE())
										AND CASE
										WHEN c.tanggal_timbul_demam NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(c.tanggal_timbul_demam)
										END = YEAR(CURDATE())
										$asd
										";
										$data=DB::select($q);
										$jml = 0;
										if(count($data) > 0) $jml = $data[0]->jml;
										if(Sentry::getUser()->hak_akses==7)
											$jml = 0;
										echo $jml;
										?> /
										<?php
										$q = "
										SELECT count(*) as jml 
										FROM difteri c
										LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
										LEFT JOIN rumahsakit2 AS i ON c.id_tempat_periksa=i.id
										WHERE 
										c.deleted_at IS NULL
										AND CASE
										WHEN c.tanggal_timbul_demam NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(c.tanggal_timbul_demam)
										END = YEAR(CURDATE())
										$asd
										";
										$data=DB::select($q);
										$jml = 0;
										if(count($data) > 0) $jml = $data[0]->jml;
										if(Sentry::getUser()->hak_akses==7)
											$jml = 0;
										echo $jml;
										?>
									</span>
								</td>
							</tr>
							<tr>
								<td>
									<span class="btn btn-success" style="width:85%;">
										AFP
										<br>
										<?php
										$q = "
										SELECT count(*) as jml 
										FROM afp c
										LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
										LEFT JOIN rumahsakit2 AS i ON c.id_tempat_periksa=i.id
										WHERE
										c.deleted_at IS NULL
										AND (klasifikasi_final='' OR klasifikasi_final IS NULL)
										AND CASE
										WHEN c.tanggal_pengambilan_spesimen1 NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(c.tanggal_pengambilan_spesimen1)
										WHEN c.tanggal_pengambilan_spesimen2 NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(c.tanggal_pengambilan_spesimen2)
										END = YEAR(CURDATE())
										AND CASE
										WHEN c.tanggal_mulai_lumpuh NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(c.tanggal_mulai_lumpuh)
										END = YEAR(CURDATE())
										$asd
										";
										$data=DB::select($q);
										$jml = 0;
										if(count($data) > 0) $jml = $data[0]->jml;
										if(Sentry::getUser()->hak_akses==7)
											$jml = 0;
										echo $jml;
										?>/
										<?php
										$q = "
										SELECT count(*) as jml 
										FROM afp c
										LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
										LEFT JOIN rumahsakit2 AS i ON c.id_tempat_periksa=i.id
										WHERE 
										c.deleted_at IS NULL
										AND CASE
										WHEN c.tanggal_pengambilan_spesimen1 NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(c.tanggal_pengambilan_spesimen1)
										WHEN c.tanggal_pengambilan_spesimen2 NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(c.tanggal_pengambilan_spesimen2)
										END = YEAR(CURDATE())
										AND CASE
										WHEN c.tanggal_mulai_lumpuh NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(c.tanggal_mulai_lumpuh)
										END = YEAR(CURDATE())
										$asd
										";
										$data=DB::select($q);
										$jml = 0;
										if(count($data) > 0) $jml = $data[0]->jml;
										if(Sentry::getUser()->hak_akses==7)
											$jml = 0;
										echo $jml;
										?>
										/
										<?php
										$q = "
										SELECT count(*) as jml 
										FROM afp c
										LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
										LEFT JOIN rumahsakit2 AS i ON c.id_tempat_periksa=i.id
										WHERE 
										c.deleted_at IS NULL
										AND CASE
										WHEN c.tanggal_mulai_lumpuh NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(c.tanggal_mulai_lumpuh)
										END = YEAR(CURDATE())
										$asd
										";
										$data=DB::select($q);
										$jml = 0;
										if(count($data) > 0) $jml = $data[0]->jml;
										if(Sentry::getUser()->hak_akses==7)
											$jml = 0;
										echo $jml;
										?>
									</span>
								</td>
							</tr>
							<tr>
								<td>
									<span class="btn btn-success" style="width:85%;">
										Crs
										<br>
										<?php
										$q = "
										SELECT count(*) as jml FROM
										crs AS a
										JOIN hasil_uji_lab_crs AS b ON a.id_hasil_uji_lab_crs=b.id_hasil_uji_lab_crs
										LEFT JOIN rumahsakit2 AS i ON a.id_tempat_periksa=i.id
										LEFT JOIN puskesmas AS h ON a.id_tempat_periksa=h.puskesmas_code_faskes
										WHERE
										a.deleted_at IS NULL
										AND (a.klasifikasi_final='' OR a.klasifikasi_final IS NULL)
										AND b.spesimen='Ya'
										AND CASE
										WHEN a.tgl_mulai_sakit NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(a.tgl_mulai_sakit)
										END = YEAR(CURDATE())
										$asd
										";
										$data=DB::select($q);
										$jml = 0;
										if(count($data) > 0) $jml = $data[0]->jml;
										echo $jml;
										?>/
										<?php
										$q = "
										SELECT count(*) as jml FROM
										crs AS a
										JOIN hasil_uji_lab_crs AS b ON a.id_hasil_uji_lab_crs=b.id_hasil_uji_lab_crs
										LEFT JOIN rumahsakit2 AS i ON a.id_tempat_periksa=i.id
										LEFT JOIN puskesmas AS h ON a.id_tempat_periksa=h.puskesmas_code_faskes
										WHERE
										a.deleted_at IS NULL
										AND CASE
										WHEN a.tgl_mulai_sakit NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(a.tgl_mulai_sakit)
										END = YEAR(CURDATE())
										AND b.spesimen='Ya'
										$asd
										";
										$data=DB::select($q);
										$jml = 0;
										if(count($data) > 0) $jml = $data[0]->jml;
										echo $jml;
										?>
										/
										<?php
										$q = "
										SELECT count(*) as jml FROM
										crs AS a
										JOIN hasil_uji_lab_crs AS b ON a.id_hasil_uji_lab_crs=b.id_hasil_uji_lab_crs
										LEFT JOIN rumahsakit2 AS i ON a.id_tempat_periksa=i.id
										LEFT JOIN puskesmas AS h ON a.id_tempat_periksa=h.puskesmas_code_faskes
										WHERE
										a.deleted_at IS NULL
										AND CASE
										WHEN a.tgl_mulai_sakit NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(a.tgl_mulai_sakit)
										END = YEAR(CURDATE())
										$asd
										";
										$data=DB::select($q);
										$jml = 0;
										if(count($data) > 0) $jml = $data[0]->jml;
										echo $jml;
										?>
									</span>
								</td>
							</tr>
							<tr><td><span class="btn" style="width:85%;">Kasus PD3I <br>Jml Spesimen / Total<br><?php echo date('Y');?></span></td></tr>
							<tr>
								<td>
									<span class="btn btn-success" style="width:85%;">
										TN
										<br>
										<?php
										$q = "
										SELECT count(*) as jml 
										FROM tetanus c
										LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
										LEFT JOIN rumahsakit2 AS i ON c.id_tempat_periksa=i.id
										WHERE
										deleted_at IS NULL
										AND CASE
										WHEN c.tanggal_mulai_sakit NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(c.tanggal_mulai_sakit)
										END = YEAR(CURDATE())
										$asd
										";
										$data=DB::select($q);
										$jml = 0;
										if(count($data) > 0) $jml = $data[0]->jml;
										if(Sentry::getUser()->hak_akses==7)
											$jml = 0;
										echo $jml;
										?>
									</span>
								</td>
							</tr>
						</table>
					</div>
					@else
					<div class="span2">
						<table style="width:100%;">
							<tr><td><span class="btn" style="width:80%;">Jumlah Sampel</span></td></tr>
							<tr>
								<td>
									<span class="btn btn-success" style="width:85%;">
										Campak
										<br>
										<?php
										$asd = '';
										$q = "
										SELECT count(*) as jml 
										FROM campak c
										LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
										LEFT JOIN rumahsakit2 AS i ON c.id_tempat_periksa=i.id
										WHERE 
										c.deleted_at IS NULL
										AND (klasifikasi_final='' OR klasifikasi_final IS NULL)
										AND CASE
										WHEN c.tanggal_timbul_demam NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(c.tanggal_timbul_demam)
										WHEN c.tanggal_timbul_rash NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(c.tanggal_timbul_rash)
										END = YEAR(CURDATE())
										$asd			
										";
										$data=DB::select($q);
										$jml = 0;
										if(count($data) > 0) $jml = $data[0]->jml;
										if(Sentry::getUser()->hak_akses==7)
											$jml = 0;
										echo $jml;
										?>
									</span>
								</td>
							</tr>
						</table>
					</div>
					@endif

					<div class="span5" style="overflow-y:scroll;overflow-x: hidden;height:400px;">
						<span class="btn" style="width:100%;">Notifikasi <br>(Pemberitahuan)</span>
						<div class="timeline-centered">
							<?php
							$kd_faskes = Session::get('kd_faskes');
							$type = Session::get('type');

							if($type=="puskesmas"){
								$kode_kec = DB::table('puskesmas')->WHERE('puskesmas_code_faskes',$kd_faskes)->PLUCK('kode_kec');
								$q = "
								SELECT 
								b.puskesmas_name,
								a.submit_dttm,
								c.kecamatan,
								a.type_id,
								a.global_id
								FROM notification a
								JOIN puskesmas b ON b.puskesmas_id=a.from_faskes_id
								JOIN kecamatan c ON c.id_kecamatan=a.to_faskes_kec_id
								WHERE a.to_faskes_kec_id='".$kode_kec."'
								AND a.deleted_at IS NULL	
								ORDER BY a.id DESC
								";
							} else if($type=="kabupaten"){
								$q = "
								SELECT 
								b.puskesmas_name,
								a.submit_dttm,
								c.kecamatan,
								a.type_id,
								a.global_id
								FROM notification a
								LEFT JOIN puskesmas b ON b.puskesmas_id=a.from_faskes_id
								LEFT JOIN kecamatan c ON c.id_kecamatan=a.to_faskes_kec_id
								LEFT JOIN kabupaten d ON d.id_kabupaten=c.id_kabupaten
								LEFT JOIN provinsi e ON e.id_provinsi=d.id_provinsi
								WHERE d.id_kabupaten='".$kd_faskes."'	
								AND a.deleted_at IS NULL
								ORDER BY a.id DESC
								";
							} else if($type=="provinsi"){
								$q = "
								SELECT 
								b.puskesmas_name,
								a.submit_dttm,
								c.kecamatan,
								a.type_id,
								a.global_id
								FROM notification a
								LEFT JOIN puskesmas b ON b.puskesmas_id=a.from_faskes_id
								LEFT JOIN kecamatan c ON c.id_kecamatan=a.to_faskes_kec_id
								LEFT JOIN kabupaten d ON d.id_kabupaten=c.id_kabupaten
								LEFT JOIN provinsi e ON e.id_provinsi=d.id_provinsi
								WHERE d.id_provinsi='".$kd_faskes."'	
								AND a.deleted_at IS NULL
								ORDER BY a.id DESC
								";
							} else if($type=='rs'){
								$q = "
								SELECT
								b.nama_faskes AS puskesmas_name,
								a.submit_dttm,
								d.kecamatan,
								a.type_id,
								a.global_id
								FROM
								notification AS a
								JOIN rumahsakit2 AS b ON a.from_faskes_id=b.kode_faskes
								LEFT JOIN kecamatan AS d ON a.to_faskes_kec_id=d.id_kecamatan
								WHERE b.kode_faskes='".$kd_faskes."'
								AND a.deleted_at IS NULL
								ORDER BY a.id DESC
								";
							} else {
								$q = "
								SELECT 
								b.puskesmas_name,
								a.submit_dttm,
								c.kecamatan,
								a.type_id,
								a.global_id
								FROM notification a
								LEFT JOIN puskesmas b ON b.puskesmas_id=a.from_faskes_id
								LEFT JOIN kecamatan c ON c.id_kecamatan=a.to_faskes_kec_id
								LEFT JOIN kabupaten d ON d.id_kabupaten=c.id_kabupaten
								LEFT JOIN provinsi e ON e.id_provinsi=d.id_provinsi
								WHERE a.to_faskes_kec_id IS NOT NULL AND a.to_faskes_kec_id != ''
								AND a.deleted_at IS NULL
								ORDER BY a.id DESC
								";
							}
							$data=DB::select($q);
							for($i=0;$i<count($data);$i++):
								?>

							<?php
							if($data[$i]->type_id == 'campak'){
								$q2 = "
								SELECT
								a.id_campak, c.nama_anak, c.nama_ortu,c.alamat,c.tanggal_lahir,c.umur,c.umur_bln,c.umur_hr,c.jenis_kelamin,d.kelurahan, d.kecamatan, d.kabupaten, d.provinsi,
								CASE
								WHEN a.kode_faskes='puskesmas' THEN CONCAT('PUSKESMAS ',e.puskesmas_name)
								WHEN a.kode_faskes='rs' THEN f.rs_nama_faskes
								ELSE NULL
								END AS nama_faskes
								FROM
								campak AS a
								JOIN hasil_uji_lab_campak AS b ON a.id_campak=b.id_campak
								JOIN pasien AS c ON b.id_pasien=c.id_pasien
								LEFT JOIN located AS d ON c.id_kelurahan=d.id_kelurahan
								LEFT JOIN view_puskesmas AS e ON a.id_tempat_periksa=e.puskesmas_id
								LEFT JOIN view_rumahsakit AS f ON a.id_tempat_periksa=f.rs_id
								WHERE a.id_campak='".$data[$i]->global_id."'
								AND a.deleted_at IS NULL
								";
							} else if($data[$i]->type_id == 'tetanus'){
								$q2 = "
								SELECT
								a.id_tetanus, c.nama_anak, c.nama_ortu,c.alamat,c.tanggal_lahir,c.umur,c.umur_bln,c.umur_hr,c.jenis_kelamin,d.kelurahan, d.kecamatan, d.kabupaten, d.provinsi,
								CASE
								WHEN a.kode_faskes='puskesmas' THEN CONCAT('PUSKESMAS ',e.puskesmas_name)
								WHEN a.kode_faskes='rs' THEN f.rs_nama_faskes
								ELSE NULL
								END AS nama_faskes
								FROM tetanus AS a
								JOIN pasien_terserang_tetanus AS b ON a.id_tetanus=b.id_pasien_tetanus
								JOIN pasien AS c ON b.id_pasien=c.id_pasien
								LEFT JOIN located AS d  ON c.id_kelurahan=d.id_kelurahan
								LEFT JOIN view_puskesmas AS e ON a.id_tempat_periksa=e.puskesmas_id
								LEFT JOIN view_rumahsakit AS f ON a.id_tempat_periksa=f.rs_id
								WHERE a.id_tetanus='".$data[$i]->global_id."'
								AND a.deleted_at IS NULL
								";
							} else if($data[$i]->type_id == 'difteri'){
								$q2 = "
								SELECT
								a.id_difteri, c.nama_anak, c.nama_ortu,c.alamat,c.tanggal_lahir,c.umur,c.umur_bln,c.umur_hr,c.jenis_kelamin,d.kelurahan, d.kecamatan, d.kabupaten, d.provinsi,
								CASE
								WHEN a.kode_faskes='puskesmas' THEN CONCAT('PUSKESMAS ',e.puskesmas_name)
								WHEN a.kode_faskes='rs' THEN f.rs_nama_faskes
								ELSE NULL
								END AS nama_faskes
								FROM difteri AS a
								JOIN hasil_uji_lab_difteri AS b ON a.id_difteri=b.id_difteri
								JOIN pasien AS c ON b.id_pasien=c.id_pasien
								LEFT JOIN located AS d ON c.id_kelurahan=d.id_kelurahan
								LEFT JOIN view_puskesmas AS e ON a.id_tempat_periksa=e.puskesmas_id
								LEFT JOIN view_rumahsakit AS f ON a.id_tempat_periksa=f.rs_id
								WHERE a.id_difteri='".$data[$i]->global_id."'
								AND a.deleted_at IS NULL
								";
							} else if($data[$i]->type_id == 'afp'){
								$q2 = "
								SELECT
								a.id_afp, c.nama_anak, c.nama_ortu,c.alamat,c.tanggal_lahir,c.umur,c.umur_bln,c.umur_hr,c.jenis_kelamin,d.kelurahan, d.kecamatan, d.kabupaten, d.provinsi,
								CASE
								WHEN a.kode_faskes='puskesmas' THEN CONCAT('PUSKESMAS ',e.puskesmas_name)
								WHEN a.kode_faskes='rs' THEN f.rs_nama_faskes
								ELSE NULL
								END AS nama_faskes
								FROM afp AS a
								JOIN hasil_uji_lab_afp AS b ON a.id_afp=b.id_afp
								JOIN pasien AS c ON b.id_pasien=c.id_pasien
								LEFT JOIN located AS d ON c.id_kelurahan=d.id_kelurahan
								LEFT JOIN view_puskesmas AS e ON a.id_tempat_periksa=e.puskesmas_id
								LEFT JOIN view_rumahsakit AS f ON a.id_tempat_periksa=f.rs_id
								WHERE a.id_afp='".$data[$i]->global_id."'
								AND a.deleted_at IS NULL
								";
							} else if($data[$i]->type_id == 'crs'){
								$q2 = "
								SELECT
								a.id_crs, c.nama_anak, c.nama_ortu,c.alamat,c.tanggal_lahir,c.umur,c.umur_bln,c.umur_hr,c.jenis_kelamin,d.kelurahan, d.kecamatan, d.kabupaten, d.provinsi,
								CASE
								WHEN a.kode_faskes='puskesmas' THEN CONCAT('PUSKESMAS ',e.puskesmas_name)
								WHEN a.kode_faskes='rs' THEN f.rs_nama_faskes
								ELSE NULL
								END AS nama_faskes
								FROM crs AS a
								JOIN pasien AS c ON a.id_pasien=c.id_pasien
								LEFT JOIN located AS d ON c.id_kelurahan=d.id_kelurahan
								LEFT JOIN view_puskesmas AS e ON a.id_tempat_periksa=e.puskesmas_id
								LEFT JOIN view_rumahsakit AS f ON a.id_tempat_periksa=f.rs_id
								WHERE a.id_crs='".$data[$i]->global_id."'
								AND a.deleted_at IS NULL
								";
							}
							$data2=DB::select($q2);
							?>
							@if(count($data2)>0)
							<div id="dialog_<?php echo $i; ?>" title="Cross Notification" style="display:none;">
								<p>
									Nama : <?php if(count($data2)>0)echo $data2[0]->nama_anak; ?><br>
									Nama Orangtua : <?php if(count($data2)>0)echo $data2[0]->nama_anak; ?><br>
									Tanggal Lahir :<?php if(count($data2)>0)echo $data2[0]->tanggal_lahir; ?><br>

									Umur :
									<?php if(count($data2)>0) if($data2[0]->umur) echo $data2[0]->umur; else echo "0"; ?> tahun  
									<?php if(count($data2)>0) if($data2[0]->umur_bln) echo $data2[0]->umur_bln; else echo "0"; ?> bulan  
									<?php if(count($data2)>0) if($data2[0]->umur_hr) echo $data2[0]->umur_hr; else echo "0"; ?> hari 
									<br>
									Jenis Kelamin :<?php if(count($data2)>0)echo $data2[0]->jenis_kelamin; ?><br>
									Alamat:<?php if(count($data2)>0)echo $data2[0]->alamat; ?><br>
									Kelurahan :<?php if(count($data2)>0)echo $data2[0]->kelurahan; ?><br>
									Kecamatan :<?php if(count($data2)>0)echo $data2[0]->kecamatan; ?><br>
									Kabupaten :<?php if(count($data2)>0)echo $data2[0]->kabupaten; ?><br>
									Provinsi :<?php if(count($data2)>0)echo $data2[0]->provinsi; ?><br>
								</p>
							</div>
							<script>
								$(document).ready(function(){
									$("#populasi_<?php echo $i; ?>").click(function( event ) {
										$( "#dialog_<?php echo $i; ?>" ).dialog();
										event.preventDefault();
									});	
								});
							</script>

							<article class="timeline-entry">
								<div class="timeline-entry-inner">
									<div class="timeline-icon bg-success">
										<i class="icon-bullhorn"></i>
									</div>
									<div class="timeline-label">
										<h2>
											<a href="#" id="populasi_<?php echo $i; ?>" title="<?php if(count($data2)>0)echo $data2[0]->nama_faskes; ?>" >
												<?php if(count($data2)>0)echo $data2[0]->nama_faskes; ?>&nbsp;
											</a> 
											<span>
												Tanggal <?php echo $data[$i]->submit_dttm; ?>
											</span>
										</h2>
										<p>
											Ada Penderita {{$data[$i]->type_id}} dari Kecamatan <?php echo $data[$i]->kecamatan; ?> 
											yang periksa di <?php if(count($data2)>0)echo $data2[0]->nama_faskes; ?>
										</p>
									</div>
								</div>
							</article>
							@endif
						<?php endfor; ?>
					</div>
				</div>
			</div>
			<div style="clear:both;"></div>
		</div>
		@if(Sentry::getUser()->hak_akses!=7)
		<div class="module-body">
			<div class="chart inline-legend grid">
				<input type="hidden" id="last_maps_id" name="last_maps_id" value="0">
				<input type="hidden" id="max_maps_id" name="max_maps_id" value="50">
				<div id="map_dashboard" style="float:left;width:100%;height:500px;"></div>
			</div>
			<div style="clear:both;"></div>
		</div>
		@endif
	</div>
	<!--/.module-->
	<div class="module hide">
		<div class="module-head">
			<h3>
				Adjust Budget Range</h3>
			</div>
			<div class="module-body">
				<div class="form-inline clearfix">
					<a href="#" class="btn pull-right">Update</a>
					<label for="amount">
						Price range:</label>
						&nbsp;
						<input type="text" id="amount" class="input-" />
					</div>
					<hr />
					<div class="slider-range">
					</div>
				</div>
			</div>
		</div>
		<!--/.content-->
	</div>
	<style>
		.gmnoprint img {
			max-width: none; 
		}

		#map_dashboard img {
			max-width: none;
		}
	</style>

	<script>
		function init_map(){
			$.ajax({
				dataType:'script',
				url:'{{URL::to("get_js_init_maps_instansi")}}',
				success:function(data) {
					combo_grafik_maps();
				}
			});
		}

		$(document).ready(function(){
			combo_grafik_klasifikasi_final();
			init_map();
		});	

		function loading_state(){
			$('#status').html('Loading...');
		}

		function combo_grafik_klasifikasi_final(){
			$.ajax({
				dataType:'script',
				url:'{{URL::to("get_js_grafik_klasifikasi_final_instansi")}}',
				success:function(data) {

				}
			});
		}

		function combo_grafik_maps(){
			<?php
			$nama_instansi = "";
			$type = Session::get('type');
			$kd_faskes = Session::get('kd_faskes');
			$latitude = "-7.8936254";
			$longitude = "110.4029268";
			if($type == "puskesmas"){
				$q = "
				SELECT b.puskesmas_name,b.alamat,b.kode_kab,b.latitude,b.longitude
				FROM puskesmas b
				JOIN kabupaten c ON c.id_kabupaten=b.kode_kab
				WHERE b.puskesmas_code_faskes='".$kd_faskes."'
				";
				$data=DB::select($q);
				$kode_kab = "";
				for($i=0;$i<count($data);$i++){
					$nama_instansi = "PUSKESMAS ". $data[0]->puskesmas_name;
					$kode_kab = $data[0]->kode_kab;
					$latitude = $data[0]->latitude;
					$longitude = $data[0]->longitude;
				}
				$maps_wilayah = "e.id_kabupaten='".$kode_kab."'";
				$level = "desa";
				$q = "
				SELECT * FROM polygon a 
				LEFT JOIN peta_dati b ON b.polygon_id=a.id
				LEFT JOIN kelurahan c ON c.id_kelurahan=b.desa_id
				LEFT JOIN kecamatan d ON d.id_kecamatan=c.id_kecamatan
				LEFT JOIN kabupaten e ON e.id_kabupaten=d.id_kabupaten
				LEFT JOIN provinsi f ON f.id_provinsi=e.id_provinsi
				WHERE 
				1=1
				AND
				".$maps_wilayah."
				AND b.level='".$level."'
				";
			} else if($type == "rs"){
				$q = "
				SELECT 
				b.kode_faskes,
				b.nama_faskes,
				b.alamat,
				LEFT(b.kode_faskes,4) as kode_kab,
				b.latitude,
				b.longitude
				FROM rumahsakit2 b
				JOIN kabupaten c ON c.id_kabupaten=LEFT(b.kode_faskes,4)
				WHERE b.kode_faskes='".$kd_faskes."'
				";
				$data=DB::select($q);
				$kode_kab = "";
				for($i=0;$i<count($data);$i++){
					$nama_instansi = "PUSKESMAS ". $data[0]->nama_faskes;
					$kode_kab = $data[0]->kode_kab;
					$latitude = $data[0]->latitude;
					$longitude = $data[0]->longitude;
				}
				$maps_wilayah = "e.id_kabupaten='".$kode_kab."'";
				$level = "desa";
				$q = "
				SELECT * FROM polygon a 
				LEFT JOIN peta_dati b ON b.polygon_id=a.id
				LEFT JOIN kelurahan c ON c.id_kelurahan=b.desa_id
				LEFT JOIN kecamatan d ON d.id_kecamatan=c.id_kecamatan
				LEFT JOIN kabupaten e ON e.id_kabupaten=d.id_kabupaten
				LEFT JOIN provinsi f ON f.id_provinsi=e.id_provinsi
				WHERE 
				1=1
				AND
				".$maps_wilayah."
				AND b.level='".$level."'
				";
			} else if($type == "kabupaten"){
				$q = "
				SELECT * FROM kabupaten b 
				WHERE b.id_kabupaten='".$kd_faskes."'
				";
				$data=DB::select($q);
				$id_kabupaten = "";
				for($i=0;$i<count($data);$i++){
					$nama_instansi = "DINAS KESEHATAN KABUPATEN ". $data[0]->kabupaten;
					$id_kabupaten = $data[0]->id_kabupaten;
				}
				$maps_wilayah = "e.id_kabupaten='".$id_kabupaten."'";
				$level = "kecamatan";
				$q = "
				SELECT * FROM polygon a 
				LEFT JOIN peta_dati b ON b.polygon_id=a.id
				LEFT JOIN kecamatan d ON d.id_kecamatan=b.desa_id
				LEFT JOIN kabupaten e ON e.id_kabupaten=d.id_kabupaten
				LEFT JOIN provinsi f ON f.id_provinsi=e.id_provinsi
				WHERE 
				1=1
				AND
				".$maps_wilayah."
				AND b.level='".$level."'
				";
			} else if($type == "provinsi"){
				$q = "
				SELECT * FROM provinsi b 
				WHERE b.id_provinsi='".$kd_faskes."'
				";
				$data=DB::select($q);
				$id_provinsi = "";
				for($i=0;$i<count($data);$i++){
					$nama_instansi = "DINAS KESEHATAN PROVINSI ".$data[0]->provinsi;
					$id_provinsi = $data[0]->id_provinsi;
				}
				$maps_wilayah = "f.id_provinsi='".$id_provinsi."'";
				$level = "kabupaten";
				$q = "
				SELECT * 
				FROM polygon a 
				LEFT JOIN peta_dati b ON b.polygon_id=a.id
				LEFT JOIN kabupaten e ON e.id_kabupaten=b.desa_id
				LEFT JOIN provinsi f ON f.id_provinsi=e.id_provinsi
				WHERE 
				1=1
				AND
				".$maps_wilayah."
				AND b.level='".$level."'
				";
			} else if($type == "kemenkes"){
				$q = "
				SELECT * FROM provinsi b 
				";
				$data=DB::select($q);
				$parent_instance = "";
				for($i=0;$i<count($data);$i++){
					$nama_instansi = "DINAS KESEHATAN PROVINSI ".$data[0]->provinsi;
					$parent_instance = $data[0]->id_provinsi;
				}
				$maps_wilayah = "f.id_provinsi='".$parent_instance."'";
				$parent_level = "country";
				$level = "kabupaten";
				$q = "
				SELECT a.id,e.id_kabupaten as id_instansi,e.kabupaten as kelurahan,b.center_lat,b.center_lon 
				FROM polygon a 
				LEFT JOIN peta_dati b ON b.polygon_id=a.id
				LEFT JOIN kabupaten e ON e.id_kabupaten=b.desa_id
				LEFT JOIN provinsi f ON f.id_provinsi=e.id_provinsi
				WHERE 
				1=1
				AND b.level='".$level."'
				";
				$infowindow_title_region = "Kabupaten";

				$latitude = "-7.4296311";
				$longitude = "110.6927889";
				$default_zoom  = "10";
				$min_zoom  = "8";
			} else if($type == "laboratorium"){
				$q = "
				SELECT * FROM laboratorium b 
				WHERE b.lab_code='".$kd_faskes."'
				";
				$data=DB::select($q);
				for($i=0;$i<count($data);$i++){
					$nama_instansi = "LABORATORIUM ".$data[0]->nama_laboratorium;
				}
				$maps_wilayah = "";
				$level = "provinsi";
			}
			$arr_jml=DB::select($q);
			$jml = count($arr_jml);
			?>
			$('#max_maps_id').val('<?php echo $jml; ?>');
			get_map();
			get_map_alert();
		}

		function get_map(){
			$.ajax({
				dataType:'script',
				data: 'penyakit='+$('#combo_grafik_maps').val()+'&num='+$('#last_maps_id').val(),
				url:'{{URL::to("get_js_grafik_maps_instansi")}}',
				success:function(data) {
					var num = (parseInt ($('#last_maps_id').val()))+1;
					$('#last_maps_id').val(num);
					if(parseInt($('#last_maps_id').val()) < parseInt($('#max_maps_id').val())) get_map();
				}
			});
		}

		function get_map_alert(){
			$.ajax({
				dataType:'script',
				data: 'penyakit=xx&num=xx',
				url:'{{URL::to("get_js_grafik_maps_alert_instansi")}}',
				success:function(data) {

				}
			});
		}
	</script>	

	@stop