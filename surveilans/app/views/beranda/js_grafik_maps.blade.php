<?php
	$nama_instansi = "";
	$type = Session::get('type');
	$kd_faskes = Session::get('kd_faskes');
	//$latitude = "-7.8936254";
	//$longitude = "110.4029268";
	$latitude = "0";
	$longitude = "0";
	$default_zoom = "0";
	$min_zoom = "0";
	//$last_id = Session::get('sess_id');
	//Session::put('sess_id', ((int) $last_id+1));
	//$now_id = Session::get('sess_id');
	$now_id = "";
	$last_id = "";
	if($type == "puskesmas"){
		$q = "
			SELECT b.puskesmas_name,b.alamat,b.kode_kab,b.latitude,b.longitude
			FROM puskesmas b
			JOIN kabupaten c ON c.id_kabupaten=b.kode_kab
			WHERE b.puskesmas_code_faskes='".$kd_faskes."'
		";
		$data=DB::select($q);
		$parent_instance = "";
		for($i=0;$i<count($data);$i++){
			$nama_instansi = "PUSKESMAS ". $data[0]->puskesmas_name;
			$parent_instance = $data[0]->kode_kab;
		}
		$maps_wilayah = "e.id_kabupaten='".$parent_instance."'";
		$parent_level = "kabupaten";
		$level = "desa";
		$q = "
			SELECT a.id,c.id_kelurahan as id_instansi,c.kelurahan,b.center_lat,b.center_lon FROM polygon a 
			LEFT JOIN peta_dati b ON b.polygon_id=a.id
			LEFT JOIN kelurahan c ON c.id_kelurahan=b.desa_id
			LEFT JOIN kecamatan d ON d.id_kecamatan=c.id_kecamatan
			LEFT JOIN kabupaten e ON e.id_kabupaten=d.id_kabupaten
			LEFT JOIN provinsi f ON f.id_provinsi=e.id_provinsi
			WHERE 
			1=1
			AND
			".$maps_wilayah."
			AND b.level='".$level."'
			order by a.id asc
			limit ".Input::get('num').",1
			";
		$infowindow_title_region = "Desa";	
	} else if($type == "rs"){
		$q = "
			SELECT 
				b.kode_faskes,
				b.nama_faskes,
				b.alamat,
				LEFT(b.kode_faskes,4) as kode_kab,
				b.latitude,
				b.longitude
			FROM rumahsakit2 b
			JOIN kabupaten c ON c.id_kabupaten=LEFT(b.kode_faskes,4)
			WHERE b.kode_faskes='".$kd_faskes."'
		";
		$data=DB::select($q);
		$parent_instance = "";
		for($i=0;$i<count($data);$i++){
			$nama_instansi = $data[0]->nama_faskes;
			$parent_instance = $data[0]->kode_kab;
		}
		$maps_wilayah = "e.id_kabupaten='".$parent_instance."'";
		$parent_level = "kabupaten";
		$level = "desa";
		$q = "
			SELECT a.id,c.id_kelurahan as id_instansi,c.kelurahan,b.center_lat,b.center_lon FROM polygon a 
			LEFT JOIN peta_dati b ON b.polygon_id=a.id
			LEFT JOIN kelurahan c ON c.id_kelurahan=b.desa_id
			LEFT JOIN kecamatan d ON d.id_kecamatan=c.id_kecamatan
			LEFT JOIN kabupaten e ON e.id_kabupaten=d.id_kabupaten
			LEFT JOIN provinsi f ON f.id_provinsi=e.id_provinsi
			WHERE 
			1=1
			AND
			".$maps_wilayah."
			AND b.level='".$level."'
			order by a.id asc
			limit ".Input::get('num').",1
			";
		$infowindow_title_region = "Desa";	
	} else if($type == "kabupaten"){
		$q = "
			SELECT * FROM kabupaten b 
			WHERE b.id_kabupaten='".$kd_faskes."'
		";
		$data=DB::select($q);
		$parent_instance = "";
		for($i=0;$i<count($data);$i++){
			$nama_instansi = "DINAS KESEHATAN KABUPATEN ". $data[0]->kabupaten;
			$parent_instance = $data[0]->id_kabupaten;
		}
		$maps_wilayah = "e.id_kabupaten='".$parent_instance."'";
		$parent_level = "kabupaten";
		$level = "kecamatan";
		$q = "
		SELECT a.id,d.id_kecamatan as id_instansi,d.kecamatan as kelurahan,b.center_lat,b.center_lon FROM polygon a 
		LEFT JOIN peta_dati b ON b.polygon_id=a.id
		LEFT JOIN kecamatan d ON d.id_kecamatan=b.desa_id
		LEFT JOIN kabupaten e ON e.id_kabupaten=d.id_kabupaten
		LEFT JOIN provinsi f ON f.id_provinsi=e.id_provinsi
		WHERE 
		1=1
		AND
		".$maps_wilayah."
		AND b.level='".$level."'
		order by a.id asc
		limit ".Input::get('num').",1
		";
		$infowindow_title_region = "Kecamatan";	
	} else if($type == "provinsi"){
		$q = "
			SELECT * FROM provinsi b 
			WHERE b.id_provinsi='".$kd_faskes."'
		";
		$data=DB::select($q);
		$parent_instance = "";
		for($i=0;$i<count($data);$i++){
			$nama_instansi = "DINAS KESEHATAN PROVINSI ".$data[0]->provinsi;
			$parent_instance = $data[0]->id_provinsi;
		}
		$maps_wilayah = "f.id_provinsi='".$parent_instance."'";
		$parent_level = "provinsi";
		$level = "kabupaten";
		$q = "
		SELECT a.id,e.id_kabupaten as id_instansi,e.kabupaten as kelurahan,b.center_lat,b.center_lon 
		FROM polygon a 
		LEFT JOIN peta_dati b ON b.polygon_id=a.id
		LEFT JOIN kabupaten e ON e.id_kabupaten=b.desa_id
		LEFT JOIN provinsi f ON f.id_provinsi=e.id_provinsi
		WHERE 
		1=1
		AND
		".$maps_wilayah."
		AND b.level='".$level."'
		LIMIT ".Input::get('num').",1
		";
		$infowindow_title_region = "Kabupaten";
	} else if($type == "kemenkes"){
		$q = "
			SELECT * FROM provinsi b 
		";
		$data=DB::select($q);
		$parent_instance = "";
		for($i=0;$i<count($data);$i++){
			$nama_instansi = "DINAS KESEHATAN PROVINSI ".$data[0]->provinsi;
			$parent_instance = $data[0]->id_provinsi;
		}
		$maps_wilayah = "f.id_provinsi='".$parent_instance."'";
		$parent_level = "country";
		$level = "kabupaten";
		$q = "
		SELECT a.id,e.id_kabupaten as id_instansi,e.kabupaten as kelurahan,b.center_lat,b.center_lon 
		FROM polygon a 
		LEFT JOIN peta_dati b ON b.polygon_id=a.id
		LEFT JOIN kabupaten e ON e.id_kabupaten=b.desa_id
		LEFT JOIN provinsi f ON f.id_provinsi=e.id_provinsi
		WHERE 
		1=1
		AND b.level='".$level."'
		LIMIT ".Input::get('num').",1
		";
		$infowindow_title_region = "Kabupaten";
		
		$latitude = "-7.4296311";
		$longitude = "110.6927889";
		$default_zoom  = "8";
		$min_zoom  = "9";
		
		
	} else if($type == "laboratorium"){
		$q = "
			SELECT * FROM laboratorium b 
			WHERE b.lab_code='".$kd_faskes."'
		";
		$data=DB::select($q);
		for($i=0;$i<count($data);$i++){
			$nama_instansi = "LABORATORIUM ".$data[0]->nama_laboratorium;
		}
		$maps_wilayah = "";
		$level = "provinsi";
		$infowindow_title_region = "";
	}
	
	if($parent_level == "kabupaten"){
		$q2 = "
			SELECT a.latitude,a.longitude,a.min_zoom,a.default_zoom
			FROM kabupaten a
			WHERE a.id_kabupaten='".$parent_instance."'
		";
		$data2=DB::select($q2);
		if(count($data2)>0){
			$latitude = $data2[0]->latitude;
			$longitude = $data2[0]->longitude;
			$default_zoom  = $data2[0]->default_zoom;
			$min_zoom  = $data2[0]->min_zoom;
			if($level=="desa") $min_zoom = $min_zoom +1;
		}	
	} else if($parent_level == "provinsi"){
		$q2 = "
			SELECT a.longitude,a.latitude,a.min_zoom,a.default_zoom
			FROM provinsi a
			WHERE a.id_provinsi='".$parent_instance."'
		";
		$data2=DB::select($q2);
		if(count($data2)>0){
			$latitude = $data2[0]->latitude;
			$longitude = $data2[0]->longitude;
			$default_zoom  = $data2[0]->default_zoom;
			$min_zoom  = $data2[0]->min_zoom;
		}
	}
	
	
	$penyakit = Input::get('penyakit');
	$arr_penyakit['campak'] = "Campak";
	$arr_penyakit['afp'] = "Polio";
	$arr_penyakit['difteri'] = "Difteri";
	$arr_penyakit['tetanus'] = "Tetanus";
	
	
	$polygon_delete=DB::select($q);
	$poly_map = "";
	for($i=0;$i<count($polygon_delete);$i++){
		
		//if($last_id) $poly_map .= "Triangle".$last_id.$polygon_delete[$i]->id.".setMap(null);";
		
		//$poly_map .= "
		//	var Triangle".$now_id.$polygon_delete[$i]->id.";		
		//";
	}
	
?>


var myLatlng = new google.maps.LatLng(<?php  echo $latitude; ?>,<?php  echo $longitude; ?>);
map.setCenter(myLatlng);
map.setZoom(<?php echo $default_zoom; ?>);


<?php
	$polygon2=DB::select($q);
	for($i=0;$i<count($polygon2);$i++){
		
		////////////////////////////////////////////////////////////////////////////
		if($type=='puskesmas'){
			$q_wilayah = "d.id_kelurahan";
		} else if($type=='rs'){
			$q_wilayah = "d.id_kelurahan";
		} else if($type=='kabupaten'){
			$q_wilayah = "e.id_kecamatan";
		} else if($type=='provinsi'){
			$q_wilayah = "f.id_kabupaten";
		} else if($type=='kemenkes'){
			$q_wilayah = "f.id_kabupaten";
		}
		 
		
		//Campak
		$q_campak = "
			SELECT 
				COUNT(*) AS jml 
			FROM hasil_uji_lab_campak a 
			JOIN pasien b ON b.id_pasien=a.id_pasien
			JOIN campak c ON c.id_campak=a.id_campak
			LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
			LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
			LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
			LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
			WHERE 
			1=1
			AND c.klasifikasi_final IN ('0','1','2')
			AND DATE_FORMAT(c.tanggal_timbul_rash,'%Y') >= '2014'
			AND DATE_FORMAT(c.tanggal_timbul_rash,'%Y') <= '2015'
			AND ".$q_wilayah." = '".$polygon2[$i]->id_instansi."'
		";
		$data_campak = DB::select($q_campak);
		//AFP
		$q_afp = "
			SELECT 
				COUNT(*) AS jml 
			FROM hasil_uji_lab_afp a 
			JOIN pasien b ON b.id_pasien=a.id_pasien
			JOIN afp c ON c.id_afp=a.id_afp
			LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
			LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
			LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
			LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
			WHERE 
			1=1
			AND c.klasifikasi_final IN ('0')
			AND DATE_FORMAT(c.tanggal_mulai_lumpuh,'%Y') >= '2014'
			AND DATE_FORMAT(c.tanggal_mulai_lumpuh,'%Y') <= '2015'
			AND ".$q_wilayah." = '".$polygon2[$i]->id_instansi."'
		";
		$data_afp = DB::select($q_afp);
		//Difteri
		$q_difteri = "
			SELECT 
				COUNT(*) AS jml 
			FROM hasil_uji_lab_difteri a 
			JOIN pasien b ON b.id_pasien=a.id_pasien
			JOIN difteri c ON c.id_difteri=a.id_difteri
			LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
			LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
			LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
			LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
			WHERE 
			1=1
			AND ".$q_wilayah." = '".$polygon2[$i]->id_instansi."'
			AND c.klasifikasi_final IN ('konfirm')
			AND DATE_FORMAT(c.tanggal_timbul_demam,'%Y') >= '2014'
			AND DATE_FORMAT(c.tanggal_timbul_demam,'%Y') <= '2015'
		";
		$data_difteri = DB::select($q_difteri);
		//Tetanus
		$q_tetanus = "
			SELECT 
				COUNT(*) AS jml 
			FROM pasien_terserang_tetanus a 
			JOIN pasien b ON b.id_pasien=a.id_pasien
			JOIN tetanus c ON c.id_tetanus=a.id_tetanus
			LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
			LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
			LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
			LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
			WHERE 
			1=1
			AND ".$q_wilayah." = '".$polygon2[$i]->id_instansi."'
			AND c.klasifikasi_akhir IN ('Konfirm TN')
			AND DATE_FORMAT(c.tanggal_mulai_sakit,'%Y') >= '2014'
			AND DATE_FORMAT(c.tanggal_mulai_sakit,'%Y') <= '2015'
		";
		$data_tetanus = DB::select($q_tetanus);
		$total_penderita_pd3i = $data_campak[0]->jml + $data_difteri[0]->jml + $data_tetanus[0]->jml+ $data_afp[0]->jml;
		////////////////////////////////////////////////////////////////////////////
		
		
		$q2 = "SELECT * FROM peta WHERE polygon_id='".$polygon2[$i]->id."' AND MOD(id,1)='0' ORDER BY id ASC";
		$peta=DB::select($q2);	
		$polygon = "";
		$koma = "";
		
		$last_lat = "0";
		$last_lon = "0";
		
		//echo 
		$center_lat = $polygon2[$i]->center_lat;
		//echo "|";
		$center_lon = $polygon2[$i]->center_lon;
		
		
		
		for($j=0;$j<count($peta);$j++){
			$polygon .= $koma;
			$polygon .= "new google.maps.LatLng(".$peta[$j]->latitude.",".$peta[$j]->longitude.")";
			$koma = ",";
			$last_lat = $peta[$j]->latitude;
			$last_lon = $peta[$j]->longitude;
		}
		
		if($center_lat == "0" || $center_lat == "" || $center_lon == "0" || $center_lon == ""){
			
		} else {
			$last_lat = $center_lon;
			//echo "|";
			$last_lon = $center_lat;
		}
		
		$warna_poly = "#afa";
		if($total_penderita_pd3i > 20) $warna_poly = "#f00";
		else if($total_penderita_pd3i >= 1) $warna_poly = "#ff0";
		
		
		$poly_map .= "
			var triangleCoords".$polygon2[$i]->id." = [
				".$polygon."
			];
			Triangle".$now_id.$polygon2[$i]->id." = new google.maps.Polygon({
				paths: triangleCoords".$polygon2[$i]->id.",
				strokeColor: '#000',
				strokeOpacity: 0.5,
				strokeWeight: 1,
				fillColor: '".$warna_poly."',
				fillOpacity: 0.4
			});
			
			Triangle".$now_id.$polygon2[$i]->id.".setMap(map);
		";
		
		$poly_map .= "	
			var opt1_".$polygon2[$i]->id." = 
			{
				fillColor:'#aaf'
			};
			var opt2_".$polygon2[$i]->id." = {
				fillColor:'".$warna_poly."'
			};
			google.maps.event.addListener(Triangle".$now_id.$polygon2[$i]->id.", 'mouseover', function() {
				var currentPolygon = this;
				currentPolygon.setOptions(opt1_".$polygon2[$i]->id.");
				//alert('".$polygon2[$i]->id."');
				//this.setMap(null);
			});
			google.maps.event.addListener(Triangle".$now_id.$polygon2[$i]->id.", 'mouseout', function() {
				var currentPolygon = this;
				currentPolygon.setOptions(opt2_".$polygon2[$i]->id.");
			});
			var lokasi_db  = new google.maps.LatLng(".$last_lat.",".$last_lon.");
			var mapLabel = new MapLabel({
			  text: '".$polygon2[$i]->kelurahan."',
			  position: lokasi_db,
			  
			  map: map,
			  fontSize: 12,
			  align: 'center',
			  minZoom : ".$min_zoom."
			});
		";
		
		
		
		$poly_map .= "
				var content = 
					'<table style=\'width:220px;height:120px;\'><tr><td>'+
					'".$infowindow_title_region." ".$polygon2[$i]->kelurahan." <br>'+
					'Jumlah Total Pasien : <br>'+
					'<b> '+
					'Campak : ".$data_campak[0]->jml." penderita <br>'+
					'Difteri : ".$data_difteri[0]->jml." penderita <br>'+
					'Tetanus : ".$data_tetanus[0]->jml." penderita <br>'+
					'AFP : ".$data_afp[0]->jml." penderita <br>'+
					'</b>' +
					'</td></tr></table>'
					;
				var infowindow_Triangle".$now_id.$polygon2[$i]->id." = new google.maps.InfoWindow({
					content: content
				});
				var myLatlng = new google.maps.LatLng(-7.9622172, 110.6031254);
				var marker_".$polygon2[$i]->id." = new google.maps.Marker({
					position: lokasi_db
				});
				";
		$poly_map .= "
				google.maps.event.addListener(Triangle".$now_id.$polygon2[$i]->id.", 'click', function() {
					infowindow_Triangle".$now_id.$polygon2[$i]->id.".open(map,marker_".$polygon2[$i]->id.");
				});
				
				";
		
	}
	
	echo $poly_map; 
	
?>