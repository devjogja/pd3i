@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <h4>
                        Data master Puskesmas
                    </h4>
                </div>
                <br>
                <div class="table-responsive">
                  <table class="uk-table uk-table-hover uk-table-striped uk-table-condensed table-bordered">
                    <thead>
                      <tr>
                        <th >No.</th>
                        <th >Nama puskesmas</th>
                        <th >Kode faskes puskesmas</th>
                        <th >Lokasi puskesmas</th>
                        <th >Alamat</th>
                        <th >Nama kabupaten</th>
                        <!-- <th >Aksi</th> -->
                      </tr>
                    </thead>
                  <tbody>
                    <?php $no=1; ?>
                    @if($puskesmas)
                    @foreach($puskesmas as $data)
                    <tr>
                      <td>{{$no}}</td>
                      <td>{{$data->puskesmas_name}}</td>
                      <td>{{$data->puskesmas_code_faskes}}</td>
                      <td>{{$data->lokasi_puskesmas}}</td>
                      <td>{{$data->alamat}}</td>
                      <td>{{$data->kabupaten_name}}</td>
                      <!-- <td>
                        <a class="btn btn-primary btn-xs" href="{{URL::to('puskesmas/'.$data->puskesmas_id.'/edit')}}"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ubah"></i></a>
                        <a class="btn btn-warning btn-xs" href="{{URL::to('puskesmas/hapus/'.$data->puskesmas_id)}}" onclick="alert('yakin ini menghapus data ini?')"><i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus"></i></a>
                      </td> -->
                    </tr>
                    <?php $no++?>
                    @endforeach
                    @endif
                  </tbody>
                  </table>
                  <ul class="pagination pagination-sm">
                    {{$puskesmas->links()}}
                  </ul>
                </div>
            </div>
            <!--/.module-body-->
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<!--/.span9-->
@stop