@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Daftar akun pengguna untuk level puskesmas
                &nbsp; <a href="{{URL::to('puskesmas/createakun')}}" class="btn btn-xs btn-success">Tambah Akun</a>
                </h3>
        </div>
    <div class="module-body">
        <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
            width="100%">
            <thead>
                <tr class="heading">
                    <th>
                        Nama Puskesmas
                    </th>
                    <th>
                        Username
                    </th>
                    <th>
                        Telepon
                    </th>
                    <th>
                        Aksi
                    </th>
                </tr>
            </thead>
            <tbody>
            @if($puskesmas)
            @foreach($puskesmas as $row)
                <tr class="odd gradeX">
                    <td>
                        {{$row->nama_puskesmas}}
                    </td>
                    <td>
                        {{$row->email}}
                    </td>
                    <td class="center">
                        {{$row->telepon}}
                    </td>
                    <td class="center">
                        <a href="{{URL::to('#')}}" class="btn btn-sm btn-warning">edit</a>
                    </td>
                </tr>
            @endforeach
            @endif
            </tbody>
        </table>
    </div>
    </div>
    </div>
</div>
@stop