@extends('layouts.master')
@section('content')
<section class="scrollable padder">
    <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
        <li><a href="{{URL::to('beranda')}}"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{URL::to('puskesmas')}}">Puskesmas</a></li>
    	<li class="active">Edit Puskesmas</li>
    </ul>
<!-- 	<div class="m-b-md">
        <h3 class="m-b-none"></h3>
    </div> -->
    <section class="panel panel-default">
        <header class="panel-heading font-bold">
            Edit Puskesmas
        </header>
    <div class="panel-body">
    {{ Form::model($puskesmas,  array('route' => array('puskesmas.update', $puskesmas->id_puskesmas), 'method' => 'PUT', 'class'=>'form-horizontal','parsley-validate'=>'parsley-validate','role'=>'form','novalidate'=>'novalidate')) }}
     <div class="form-group">
    {{Form::label('code','Nama puskesmas',array('class'=>'col-sm-2 control-label'))}}            
    <div class="col-sm-10">
    {{ Form::text('nama_puskesmas', null, array(
        'class' => 'form-control',
        'placeholder' => '',
        'parsley-required' => 'true',
        'parsley-remote' => ''
        )) 
    }}
    </div>
    </div>
    <div class="line line-dashed line-lg pull-in"></div>
    <div class="form-group">
    {{Form::label('code','Nama Petugas',array('class'=>'col-sm-2 control-label'))}}          
    <div class="col-sm-10">
    {{ Form::text('nama_petugas', null, array(
        'class' => 'form-control',
        'placeholder' => '',
        'parsley-required' => 'true',
        'parsley-remote' => ''
        )) 
    }}
    </div>
	</div>
	<div class="line line-dashed line-lg pull-in"></div>
	<div class="form-group">
        {{Form::label('code','Provinsi',array('class'=>'col-sm-2 control-label'))}}            
        <div class="col-sm-10"> 
        {{ Form::select('', array(''=>'')+Provinsi::lists('provinsi','id_provinsi'),substr($puskesmas->id_kelurahan,0,2),array('id'=>'id_provinsi','placeholder' => 'Pilih Provinsi','onchange'=>'showKabupaten()')) }}
        </div>
	</div>
	<div class="line line-dashed line-lg pull-in"></div>
	<div class="form-group">
        <label for="title" class="col-sm-2 control-label">Kabupaten</label>           
        <div class="col-sm-10">
          <?php $data = Kabupaten::getKab(substr($puskesmas->id_kelurahan,0,2))?>
          <select id='id_kabupaten' placeholder ='Pilih Kabupaten' onchange='showKecamatan()'>
            @foreach($data as $kab)
            <?php 
            if($kab->id_kabupaten==substr($puskesmas->id_kelurahan,0,4))
            echo '<option value="'.$kab->id_kabupaten.'" selected="selected">'.$kab->kabupaten.'</option>';
            else
            echo '<option value="'.$kab->id_kabupaten.'">'.$kab->kabupaten.'</option>';
            ?>
            @endforeach   
          </select>
        </div>
	</div>
    <div class="line line-dashed line-lg pull-in"></div>
    <div class="form-group">
        <label for="title" class="col-sm-2 control-label">Kecamatan</label>            
        <div class="col-sm-10">
            <?php $data = Kecamatan::getKec(substr($puskesmas->id_kelurahan,0,4))?>
            <select id='id_kecamatan' placeholder ='Pilih Kecamatan' onchange='showKelurahan()'>
                @foreach($data as $kec)
                <?php 
                if($kec->id_kecamatan==substr($puskesmas->id_kelurahan,0,7))
                echo '<option value="'.$kec->id_kecamatan.'" selected="selected">'.$kec->kecamatan.'</option>';
                else
                echo '<option value="'.$kec->id_kecamatan.'">'.$kec->kecamatan.'</option>';
                ?>
                @endforeach   
            </select>
        </div>
    </div>
    <div class="line line-dashed line-lg pull-in"></div>
    <div class="form-group">
        <label for="title" class="col-sm-2 control-label">Kelurahan</label>            
        <div class="col-sm-10">
            <?php $data = Kelurahan::getKel(substr($puskesmas->id_kelurahan,0,7))?>
            <select name="id_kelurahan" id='id_kelurahan' placeholder ='Pilih Kelurahan'>
                @foreach($data as $kel)
                <?php 
                if($kel->id_kelurahan==substr($puskesmas->id_kelurahan,0,7))
                echo '<option value="'.$kel->id_kelurahan.'" selected="selected">'.$kel->kelurahan.'</option>';
                else
                echo '<option value="'.$kel->id_kelurahan.'">'.$kel->kelurahan.'</option>';
                ?>
                @endforeach   
            </select>
        </div>
    </div>
	<div class="line line-dashed line-lg pull-in"></div>
	<div class="form-group">
        {{Form::label('code','Alamat',array('class'=>'col-sm-2 control-label'))}}              
        <div class="col-sm-10">
        {{ Form::text('alamat', null, array(
            'class' => 'form-control',
            'placeholder' => '',
            'parsley-required' => 'true',
            'parsley-remote' => ''
            )) 
        }}
        </div>
    </div>
	<div class="line line-dashed line-lg pull-in"></div>
	<div class="form-group">
        {{Form::label('code','Telepon',array('class'=>'col-sm-2 control-label'))}}             
        <div class="col-sm-10">
        {{ Form::text('telepon', null, array(
            'class' => 'form-control',
            'placeholder' => '',
            'parsley-required' => 'true',
            'parsley-remote' => ''
            )) 
        }}
        </div>
    </div>
	<div class="line line-dashed line-lg pull-in"></div>
	<div class="form-group">
        {{Form::label('code','Email',array('class'=>'col-sm-2 control-label'))}}            
        <div class="col-sm-10">
        {{ Form::text('email', null, array(
            'class' => 'form-control',
            'placeholder' => '',
            'parsley-required' => 'true',
            'parsley-remote' => ''
            )) 
        }}
    </div>
    </div>
    <div class="line line-dashed line-lg pull-in"></div>
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-2">
            {{ Form::submit('Rubah', array('class'=>'btn btn-primary')) }}&nbsp;&nbsp;<a class="btn btn-default" href="{{URL::to('puskesmas')}}">Batal simpan</a>
        </div>
    </div>
    {{Form::close()}}    
    </div>
	</section>
</section>
@stop