@extends('layouts.master')
@section('content')
<!-- This is the modal -->
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                 <h4 class="modal-title">Kasus penyakit campak</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
            <a href="{{URL::to('campak')}}" class="btn btn-sm btn-warning">kembali</a>
            </div>
        </div>
        <!-- /.modal-content -->
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


  <div class="span12">
      <div class="content">
          <div class="module">
              <div class="module-body">
                  <div class="profile-head media">
                      <h4>Daftar Petugas Surveilans</h4>
                      <hr>
                      <table class="display table table-bordered" id="example">
                        <thead>
                          <tr>
                            <td>No.</td>
                            <th>Nama Petugas</th>
                            <th>Intansi</th>
                            <th>Email</th>
                            <th>Telepon</th>
                            {{-- <th>action</th> --}}
                          </tr>
                        </thead>

                        <tbody>
                          @foreach($users as $user)
                            <tr>
                              <td>#</td>
                              <td>{{ $user->first_name." ".$user->last_name }}</td>
                              <td>{{ $user->instansi }}</td>
                              <td>{{ $user->email }}</td>
                              <td>{{ $user->no_telp }}</td>
                              {{-- <th>action</th> --}}
                            </tr>
                          @endforeach
                        </tbody>

                      </table>

                      <script>
                        $(document).ready(function() {
                            $('#example').dataTable();
                            $("#example_length select").select2();
                        });
                      </script>

                  </div>
              </div>
          </div>
      </div>
  </div>

@stop