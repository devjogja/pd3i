@extends('layouts.master')
@section('content')
<section class="scrollable padder">  
  <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
    <li><a href=""><i class="fa fa-home"></i> Home</a></li> 
  </ul>     
  <h3>Informasi Puskesmas</h3>
  <h4 class="inline text-muted m-t-n">Total <span class="m-l-xl m-r-sm">: </span></h4><h3 class="inline">{{$jml}}</h3>
  <section class="panel panel-default">
    <header class="panel-heading">
      <a href="{{route('puskesmas.create')}}" class="btn btn-sm btn-default btn-rounded">Tambah Puskesmas</a>
    </header>
    <div class="table-responsive">
      <table class="table table-striped m-b-none ZsB5sTrf" data-ride="datatables">
        <thead>
          <tr>
            <th >No.</th>
            <th >Nama puskesmas</th>
            <th >Kode faskes puskesmas</th>
            <th >Lokasi puskesmas</th>
            <th >Alamat</th>
            <th >Nama kabupaten</th>
            <th >Aksi</th>
          </tr>
        </thead>
      <tbody>
      <?php $no=1; ?>
      @if($puskesmas)
      @foreach($puskesmas as $data)
      <tr>
        <td>{{$no}}</td>
        <td>{{$data->puskesmas_name}}</td>
        <td>{{$data->puskesmas_code_faskes}}</td>
        <td>{{$data->lokasi_puskesmas}}</td>
        <td>{{$data->alamat}}</td>
        <td>{{$data->kabupaten_name}}</td>
        <td>
          <a class="btn btn-primary btn-xs" href="{{URL::to('puskesmas/'.$data->puskesmas_id.'/edit')}}"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ubah"></i></a>
          <a class="btn btn-warning btn-xs" href="{{URL::to('puskesmas/hapus/'.$data->puskesmas_id)}}" onclick="alert('yakin ini menghapus data ini?')"><i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus"></i></a>
      </tr>
      <?php $no++?>
      @endforeach
      @endif
      </tbody>
      </table>
    </div>
  </section>
</section>
@stop