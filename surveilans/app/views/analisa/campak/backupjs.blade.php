<?php
	$nama_instansi = "INDONESIA";
	$type = Session::get('type');
	$kd_faskes = Session::get('kd_faskes');
	$penyakit = Session::get('penyakit');

	$names=$arr_penyakit=$nk=$suspek='';
	if ($penyakit=='Campak') {
		$arr_penyakit = 'Campak';
		$suspek = 'Suspek Campak';
		$names = 'Campak Lab, Campak Epid dan Campak Klinis';
		$nk = 'Campak Lab, Campak Epid, Campak Klinis, Rubella, Bukan campak/rubella dan Pending';
	} elseif($penyakit=='Difteri') {
		$arr_penyakit = 'Difteri';
		$suspek = 'Suspek Difteri';
		$names = 'Konfirm Difteri, Probable';
		$nk = 'Konfirm Difteri, Negatif Difteri, Probable';
	} elseif($penyakit=='AFP') {
		$arr_penyakit = 'AFP';
		$suspek = 'Suspek Polio';
		$names = 'Polio';
		$nk = 'Polio,Bukan polio dan Compatible';
	} elseif($penyakit=='Tetanus Neonatorum') {
		$arr_penyakit = 'Tetanus';
		$suspek = 'Tetanus';
		$names = 'Konfirm TN';
		$nk = 'Konfirm TN, Tersangka TN dan Bukan TN';
	} elseif($penyakit=='CRS') {
		$arr_penyakit = 'CRS';
		$suspek = 'CRS';
		$names = 'CRS Pasti (Lab Positif),CRS Klinis';
		$nk = 'CRS Pasti (Lab Positif), CRS Klinis, CRS, Bukan CRS dan Suspek CRS';
	}
?>

<?php
	$arr_nama_bulan[1] = "Januari";
	$arr_nama_bulan[2] = "Februari";
	$arr_nama_bulan[3] = "Maret";
	$arr_nama_bulan[4] = "April";
	$arr_nama_bulan[5] = "Mei";
	$arr_nama_bulan[6] = "Juni";
	$arr_nama_bulan[7] = "Juli";
	$arr_nama_bulan[8] = "Agustus";
	$arr_nama_bulan[9] = "September";
	$arr_nama_bulan[10] = "Oktober";
	$arr_nama_bulan[11] = "November";
	$arr_nama_bulan[12] = "Desember";
	
    $unit 				= Input::get('unit');
	$day_start 			= Input::get('day_start');
	$month_start 		= Input::get('month_start');
	$year_start 		= empty(Input::get('year_start'))?date('Y'):Input::get('year_start');
	$day_end 			= Input::get('day_end');
	$month_end 			= Input::get('month_end');
	$year_end 			= empty(Input::get('year_end'))?date('Y'):Input::get('year_end');
	$province_id 		= Input::get('province_id');
	$district_id 		= Input::get('district_id');
	$sub_district_id 	= Input::get('sub_district_id');
	$village_id 		= Input::get('village_id');
	$puskesmas_id 		= Input::get('puskesmas_id');
	$rs_id 				= Input::get('rs_id');

	switch($unit) {
		case "all" :
			$between = " ";
			$start_disp = $year_start;
			$end_disp = $year_end;
		break;
		case "year" :
			$start = $year_start;
			$end = $year_end;
			if($penyakit == "Campak") $between = " AND YEAR(c.tanggal_timbul_rash) BETWEEN '".$start."' AND '".$end."' ";
			else if($penyakit == "Difteri") $between = " AND YEAR(c.tanggal_timbul_demam) BETWEEN '".$start."' AND '".$end."' ";
			else if($penyakit == "AFP") $between = " AND YEAR(c.tanggal_mulai_lumpuh) BETWEEN '".$start."' AND '".$end."' ";
			else if($penyakit == "Tetanus") $between = " AND YEAR(c.tanggal_mulai_sakit) BETWEEN '".$start."' AND '".$end."' ";
			else if($penyakit == "CRS") $between = " AND YEAR(c.tgl_mulai_sakit) BETWEEN '".$start."' AND '".$end."' ";
			$start_disp = $year_start;
			$end_disp = $year_end;
		break;
		case "month" :
			$start = $year_start . '-' . $month_start . '-1';
			$end = $year_end . '-' . $month_end . '-' . date('t', mktime(0,0,0,$month_end, 1, $year_end));
			if($penyakit == "Campak") $between = " AND DATE(c.tanggal_timbul_rash) BETWEEN STR_TO_DATE('".$start."', '%Y-%c-%e') AND STR_TO_DATE('".$end."', '%Y-%c-%e') ";
			else if($penyakit == "Difteri") $between = " AND DATE(c.tanggal_timbul_demam) BETWEEN STR_TO_DATE('".$start."', '%Y-%c-%e') AND STR_TO_DATE('".$end."', '%Y-%c-%e') ";
			else if($penyakit == "AFP") $between = " AND DATE(c.tanggal_mulai_lumpuh) BETWEEN STR_TO_DATE('".$start."', '%Y-%c-%e') AND STR_TO_DATE('".$end."', '%Y-%c-%e') ";
			else if($penyakit == "Tetanus") $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('".$start."', '%Y-%c-%e') AND STR_TO_DATE('".$end."', '%Y-%c-%e') ";
			else if($penyakit == "CRS") $between = " AND DATE(c.tgl_mulai_sakit) BETWEEN STR_TO_DATE('".$start."', '%Y-%c-%e') AND STR_TO_DATE('".$end."', '%Y-%c-%e') ";
			$start_disp = $arr_nama_bulan[$month_start]." ".$year_start;
			$end_disp = $arr_nama_bulan[$month_end]." ".$year_end;
		break;
		default :
			$start = $year_start . '-' . $month_start . '-' . $day_start;
			$end = $year_end . '-' . $month_end . '-' . $day_end;
			if($penyakit == "Campak") $between = " AND DATE(c.tanggal_timbul_rash) BETWEEN STR_TO_DATE('".$start."', '%Y-%c-%e') AND STR_TO_DATE('".$end."', '%Y-%c-%e') ";
			else if($penyakit == "Difteri") $between = " AND DATE(c.tanggal_timbul_demam) BETWEEN STR_TO_DATE('".$start."', '%Y-%c-%e') AND STR_TO_DATE('".$end."', '%Y-%c-%e') ";
			else if($penyakit == "AFP") $between = " AND DATE(c.tanggal_mulai_lumpuh) BETWEEN STR_TO_DATE('".$start."', '%Y-%c-%e') AND STR_TO_DATE('".$end."', '%Y-%c-%e') ";
			else if($penyakit == "Tetanus") $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('".$start."', '%Y-%c-%e') AND STR_TO_DATE('".$end."', '%Y-%c-%e') ";
			else if($penyakit == "CRS") $between = " AND DATE(c.tgl_mulai_sakit) BETWEEN STR_TO_DATE('".$start."', '%Y-%c-%e') AND STR_TO_DATE('".$end."', '%Y-%c-%e') ";
			$start_disp = $day_start." ".$arr_nama_bulan[$month_start]." ".$year_start;
			$end_disp = $day_end." ".$arr_nama_bulan[$month_end]." ".$year_end;
		break;
	}

	$wilayah="";
	if($puskesmas_id){
		$wilayah = "AND h.puskesmas_code_faskes='".$puskesmas_id."' ";
		$nama_instansi = DB::table('puskesmas')->WHERE('puskesmas_code_faskes',$puskesmas_id)->pluck('puskesmas_name');
	} else if($sub_district_id) {
		if ($type=='rs') {
			$wilayah = "AND j.kode_kec='".$sub_district_id."'";
		} else if($type=='puskesmas') {
			$wilayah = "AND h.kode_kec='".$sub_district_id."' ";
		} else {
			$wilayah = "AND (j.kode_kec='".$sub_district_id."' OR h.kode_kec='".$sub_district_id."')";
		}
		$nama_instansi = DB::table('kecamatan')->WHERE('id_kecamatan',$sub_district_id)->pluck('kecamatan');
	} else if($rs_id) {
		$wilayah = "AND j.kode_faskes='".$rs_id."' ";
		$nama_instansi = DB::table('rumahsakit2')->WHERE('kode_faskes',$rs_id)->pluck('nama_faskes');
	} else if($district_id) {
		if ($type=='rs') {
			$wilayah = "AND j.kode_kab='".$district_id."'";
		} else if($type=='puskesmas') {
			$wilayah = "AND h.kode_kab='".$district_id."' ";
		} else {
			$wilayah = "AND (j.kode_kab='".$district_id."' OR h.kode_kab='".$district_id."')";
		}
		$nama_instansi = DB::table('kabupaten')->WHERE('id_kabupaten',$district_id)->pluck('kabupaten');
	} else if($province_id) {
		if ($type=='rs') {
			$wilayah = "AND j.kode_prop='".$province_id."'";
		} else if($type=='puskesmas') {
			$wilayah = "AND h.kode_prop='".$province_id."' ";
		} else {
			$wilayah = "AND (j.kode_prop='".$province_id."' OR h.kode_prop='".$province_id."')";
		}
		$nama_instansi = DB::table('provinsi')->WHERE('id_provinsi',$province_id)->pluck('provinsi');
	} 

	?>
	
    //###################################################################
	//#JENIS KELAMIN
	//###################################################################
	<?php 
		if($penyakit=="Campak"){
			$q = "
				SELECT
					CASE
						WHEN b.jenis_kelamin='1' THEN 'Laki-Laki'
						WHEN b.jenis_kelamin='2' THEN 'Perempuan'
						WHEN b.jenis_kelamin='3' THEN 'Tidak Jelas'
						ELSE 'Tidak di isi'
					END AS 'jenis_kelamin',
					COUNT(*) AS jml
				FROM
				campak AS c
				JOIN hasil_uji_lab_campak AS d ON c.id_campak=d.id_campak
				JOIN pasien AS b ON d.id_pasien=b.id_pasien
				LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
				LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
				WHERE 
				c.deleted_at IS NULL
				AND c.klasifikasi_final IN ('1','2','3')
				$between
				$wilayah
				GROUP BY b.jenis_kelamin
			";
		} else if($penyakit=="AFP"){
			$q = "
				SELECT 
					CASE
						WHEN b.jenis_kelamin='1' THEN 'Laki-Laki'
						WHEN b.jenis_kelamin='2' THEN 'Perempuan'
						WHEN b.jenis_kelamin='3' THEN 'Tidak Jelas'
						ELSE 'Tidak di isi'
					END AS 'jenis_kelamin',
					COUNT(*) AS jml
				FROM afp AS c
				JOIN hasil_uji_lab_afp AS d ON c.id_afp=d.id_afp
				JOIN pasien b ON d.id_pasien=b.id_pasien
				LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
				LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
				WHERE 
				c.deleted_at IS NULL
				AND c.klasifikasi_final IN ('1')
				$between
				$wilayah
				GROUP BY b.jenis_kelamin
			";
		} else if($penyakit=="Difteri"){
			$q = "
				SELECT 
					CASE
						WHEN b.jenis_kelamin='1' THEN 'Laki-Laki'
						WHEN b.jenis_kelamin='2' THEN 'Perempuan'
						WHEN b.jenis_kelamin='3' THEN 'Tidak Jelas'
						ELSE 'Tidak di isi'
					END AS 'jenis_kelamin',
					COUNT(*) AS jml 
				FROM difteri AS c 
				JOIN hasil_uji_lab_difteri AS d ON c.id_difteri=d.id_difteri
				JOIN pasien b ON b.id_pasien=d.id_pasien
				LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
				LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
				WHERE 
				c.deleted_at IS NULL
				AND c.klasifikasi_final IN ('1','2')
				$between
				$wilayah
				GROUP BY b.jenis_kelamin
			";
		} else if($penyakit=="Tetanus"){
			$q = "
				SELECT 
					CASE
						WHEN b.jenis_kelamin='1' THEN 'Laki-Laki'
						WHEN b.jenis_kelamin='2' THEN 'Perempuan'
						WHEN b.jenis_kelamin='3' THEN 'Tidak Jelas'
						ELSE 'Tidak di isi'
					END AS 'jenis_kelamin',
					COUNT(*) AS jml
				FROM pasien_terserang_tetanus a 
				JOIN pasien b ON b.id_pasien=a.id_pasien
				JOIN tetanus c ON c.id_tetanus=a.id_tetanus
				LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
				LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
				WHERE 
				c.deleted_at IS NULL
				AND c.klasifikasi_akhir IN ('0')
				$between
				$wilayah
				GROUP BY b.jenis_kelamin
			";
		} else if($penyakit=='CRS'){
			$q = "
				SELECT
					CASE
						WHEN b.jenis_kelamin='1' THEN 'Laki-Laki'
						WHEN b.jenis_kelamin='2' THEN 'Perempuan'
						WHEN b.jenis_kelamin='3' THEN 'Tidak Jelas'
						WHEN b.jenis_kelamin='' OR b.jenis_kelamin='0' THEN 'Tidak di isi'
					END AS 'jenis_kelamin',
					COUNT(*) AS jml
				FROM
				crs AS c
				JOIN pasien AS b ON c.id_pasien=b.id_pasien
				LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
				LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
				WHERE
				c.deleted_at IS NULL
				AND c.klasifikasi_final IN ('CRS pasti(Lab Positif)','CRS Klinis')
				$between
				$wilayah
				GROUP BY b.jenis_kelamin
				";
		}
		$data=DB::select($q);
		$x_data = $comma = "";
		for($i=0;$i<count($data);$i++){
			$x_data .= 	$comma."['".$data[$i]->jenis_kelamin."',".$data[$i]->jml."]";
			$comma = ",";
		}
	?>
	
	$('#container_jk').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: 'Grafik Penderita <?php echo $suspek; ?> Berdasar Jenis Kelamin<br><?php echo $names?>',
        },
		subtitle: {
            text: '<?php echo $nama_instansi; ?><br><?php echo $start_disp." s/d ".$end_disp; ?>',
            x: 0,
            y: 55
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name} <b>({point.y} Jiwa)<br>{point.percentage:.1f}%</b>'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Jumlah Penderita ',
            data: [
				<?php echo $x_data; ?>
            ]
        }]
    });
	
	
	//###################################################################
	//#BERDASAR BULAN & TAHUN
	//###################################################################
	
	<?php 
		$x_data = $x_categories = $comma = "";
		for($i=$year_start;$i<=$year_end;$i++){
			for($j=1;$j<=12;$j++){
				if($j < 10) $bln_num = "0".$j;
				else $bln_num = $j;
				$bln_name = $arr_nama_bulan[$j];
				
				if($penyakit=="Campak"){
					$q = "
						SELECT 
							DATE_FORMAT(c.tanggal_timbul_rash,'%m') AS bln,	
							DATE_FORMAT(c.tanggal_timbul_rash,'%Y') AS tahun,	
							DATE_FORMAT(c.tanggal_timbul_rash,'%m %Y') AS bln_num,	
							IFNULL(COUNT(*),0) AS jml
						FROM
						campak AS c
						JOIN hasil_uji_lab_campak AS d ON c.id_campak=d.id_campak
						JOIN pasien AS b ON d.id_pasien=b.id_pasien
						LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
						LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
						WHERE 
						c.deleted_at IS NULL
						AND c.klasifikasi_final IN ('1','2','3')
						AND DATE_FORMAT(c.tanggal_timbul_rash,'%m') = '".$bln_num."'
						AND DATE_FORMAT(c.tanggal_timbul_rash,'%Y') = '".$i."'
						$between
						$wilayah
						GROUP BY DATE_FORMAT(c.tanggal_timbul_rash,'%m %y')
					";
				} else if($penyakit=="AFP"){
					$q = "
						SELECT 
							DATE_FORMAT(c.tanggal_mulai_lumpuh,'%m') AS bln,	
							DATE_FORMAT(c.tanggal_mulai_lumpuh,'%Y') AS tahun,	
							DATE_FORMAT(c.tanggal_mulai_lumpuh,'%m %Y') AS bln_num,	
							IFNULL(COUNT(*),0) AS jml 
						FROM afp AS c
						JOIN hasil_uji_lab_afp AS d ON c.id_afp=d.id_afp
						JOIN pasien b ON d.id_pasien=b.id_pasien
						LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
						LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
						WHERE 
						c.deleted_at IS NULL
						AND c.klasifikasi_final IN ('1')
						AND DATE_FORMAT(c.tanggal_mulai_lumpuh,'%m') = '".$bln_num."'
						AND DATE_FORMAT(c.tanggal_mulai_lumpuh,'%Y') = '".$i."'
						$between
						$wilayah
						GROUP BY DATE_FORMAT(c.tanggal_mulai_lumpuh,'%m %y')
					";
				} else if($penyakit=="Difteri"){
					$q = "
						SELECT 
							DATE_FORMAT(c.tanggal_timbul_demam,'%m') AS bln,	
							DATE_FORMAT(c.tanggal_timbul_demam,'%Y') AS tahun,	
							DATE_FORMAT(c.tanggal_timbul_demam,'%m %Y') AS bln_num,	
							IFNULL(COUNT(*),0) AS jml 
						FROM difteri AS c 
						JOIN hasil_uji_lab_difteri AS d ON c.id_difteri=d.id_difteri
						JOIN pasien b ON b.id_pasien=d.id_pasien
						LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
						LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
						WHERE 
						c.deleted_at IS NULL
						AND c.klasifikasi_final IN ('1','2')
						AND DATE_FORMAT(c.tanggal_timbul_demam,'%m') = '".$bln_num."'
						AND DATE_FORMAT(c.tanggal_timbul_demam,'%Y') = '".$i."'
						$between
						$wilayah
						GROUP BY DATE_FORMAT(c.tanggal_timbul_demam,'%m %y')
					";
				} else if($penyakit=="Tetanus"){
					$q = "
						SELECT 
							DATE_FORMAT(c.tanggal_mulai_sakit,'%m') AS bln,	
							DATE_FORMAT(c.tanggal_mulai_sakit,'%Y') AS tahun,	
							DATE_FORMAT(c.tanggal_mulai_sakit,'%m %Y') AS bln_num,	
							IFNULL(COUNT(*),0) AS jml 
						FROM pasien_terserang_tetanus a 
						JOIN pasien b ON b.id_pasien=a.id_pasien
						JOIN tetanus c ON c.id_tetanus=a.id_tetanus
						LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
						LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
						WHERE 
						c.deleted_at IS NULL
						AND c.klasifikasi_akhir IN ('0')
						AND DATE_FORMAT(c.tanggal_mulai_sakit,'%m') = '".$bln_num."'
						AND DATE_FORMAT(c.tanggal_mulai_sakit,'%Y') = '".$i."'
						$between
						$wilayah
						GROUP BY DATE_FORMAT(c.tanggal_mulai_sakit,'%m %y')
					";
				} else if($penyakit=='CRS'){
					$q = "
						SELECT
							DATE_FORMAT(c.tgl_mulai_sakit,'%m') AS bln,	
							DATE_FORMAT(c.tgl_mulai_sakit,'%Y') AS tahun,	
							DATE_FORMAT(c.tgl_mulai_sakit,'%m %Y') AS bln_num,	
							IFNULL(COUNT(*),0) AS jml 
						FROM
						crs AS c
						JOIN pasien AS b ON c.id_pasien=b.id_pasien
						LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
						LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
						WHERE
						c.deleted_at IS NULL
						AND c.klasifikasi_final IN ('CRS pasti(Lab Positif)','CRS Klinis')
						AND DATE_FORMAT(c.tgl_mulai_sakit,'%m')  = '".$bln_num."'
						AND DATE_FORMAT(c.tgl_mulai_sakit,'%Y')  = '".$i."'
						$between
						$wilayah
						GROUP BY DATE_FORMAT(c.tgl_mulai_sakit,'%m %y')
					";
				}
				$data=DB::select($q);
				$val_jml = $val_bln = $val_tahun = 0;
				if(count($data)>0) {
					$val_jml = $data[0]->jml;
					$val_bln = $data[0]->bln;
					$val_tahun = $data[0]->tahun;
				}
				$x_data .= 	$comma.$val_jml;
				$x_categories .= $comma."'".$bln_name." ".$i."'";
				$comma = ",";
			}
		}
	?>
	$('#container_waktu').highcharts({
		chart: {
			type: 'column'
		},
        title: {
            text: 'Grafik Penderita <?php echo $suspek; ?> Bulanan<br><?php echo $names?>',
            x: -20 //center
        },
        subtitle: {
            text: '<?php echo $nama_instansi; ?><br><?php echo $start_disp." s/d ".$end_disp; ?>',
            x: -20,
            y: 55
        },
        xAxis: {
            categories: [
			<?php echo $x_categories; ?>
			],
			labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        yAxis: {
            title: {
                text: 'Jumlah Penderita'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' penderita'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 1
        },
        series: [{
            name: 'Penderita',
            colorByPoint: true,
            data: [
				<?php echo $x_data; ?>
			]
        }]
    });
		
	//###################################################################
	//# USIA
	//###################################################################
		
	<?php	
	$categories = $series = $comma2 = $comma3 = $kelompok_umur = "";
	$arr_kelompok_umur = array(
		"",
		"Di bawah 1 tahun",
		"1-4 tahun",
		"5-9 tahun",
		"10-14tahun",
		"Lebih dari sama dengan 15 tahun",
	);
	for($k=1;$k<count($arr_kelompok_umur);$k++){
		$x_data = "";
		$comma = "";
		for($i=$year_start;$i<=$year_end;$i++){
			if($penyakit=="Campak"){
				$q = "	
					SELECT 
						COUNT(*) AS jml
					FROM
					campak AS c
					JOIN hasil_uji_lab_campak AS d ON c.id_campak=d.id_campak
					JOIN pasien AS b ON d.id_pasien=b.id_pasien
					LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
					LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
					WHERE 
					c.deleted_at IS NULL
					AND YEAR(c.tanggal_timbul_rash) =  '".$i."'
					AND getKelompokUmurCampak(b.tanggal_lahir,c.tanggal_timbul_rash)='".$k."'
					AND c.klasifikasi_final IN ('1','2','3')
					$between
					$wilayah
					GROUP BY getKelompokUmurCampak(b.tanggal_lahir,c.tanggal_timbul_rash)
				";
			} else if($penyakit=="AFP") {	
				$q = "	
					SELECT 
					COUNT(*) AS jml
					FROM afp AS c
					JOIN hasil_uji_lab_afp AS d ON c.id_afp=d.id_afp
					JOIN pasien b ON d.id_pasien=b.id_pasien
					LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
					LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
					WHERE 
					c.deleted_at IS NULL
					AND YEAR(c.tanggal_mulai_lumpuh) =  '".$i."'
					$between
					$wilayah
					AND getKelompokUmurCampak(b.tanggal_lahir,c.tanggal_mulai_lumpuh)='".$k."'
					AND c.klasifikasi_final IN ('1')
					GROUP BY getKelompokUmurCampak(b.tanggal_lahir,c.tanggal_mulai_lumpuh)
				";
			} else if($penyakit=="Difteri") {	
				$q = "	
					SELECT 
					COUNT(*) AS jml
					FROM difteri AS c 
					JOIN hasil_uji_lab_difteri AS d ON c.id_difteri=d.id_difteri
					JOIN pasien b ON b.id_pasien=d.id_pasien
					LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
					LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
					WHERE 
					c.deleted_at IS NULL
					AND YEAR(c.tanggal_timbul_demam) =  '".$i."'
					$between
					$wilayah
					AND getKelompokUmurCampak(b.tanggal_lahir,c.tanggal_timbul_demam)='".$k."'
					AND c.klasifikasi_final IN ('1','2')
					GROUP BY getKelompokUmurCampak(b.tanggal_lahir,c.tanggal_timbul_demam)
				";
			} else if($penyakit=="Tetanus") {	
				$q = "	
					SELECT 
					COUNT(*) AS jml
					FROM pasien_terserang_tetanus a 
					JOIN pasien b ON b.id_pasien=a.id_pasien
					JOIN tetanus c ON c.id_tetanus=a.id_tetanus
					LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
					LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
					WHERE 
					c.deleted_at IS NULL
					AND YEAR(c.tanggal_mulai_sakit) =  '".$i."'
					$between
					$wilayah
					AND getKelompokUmurCampak(b.tanggal_lahir,c.tanggal_mulai_sakit)='".$k."'
					AND c.klasifikasi_akhir IN ('0')
					GROUP BY getKelompokUmurCampak(b.tanggal_lahir,c.tanggal_mulai_sakit)
				";
			} else if ($penyakit=="CRS"){
				$q = "
					SELECT
					COUNT(*) AS jml
					FROM
					crs AS c
					JOIN pasien AS b ON c.id_pasien=b.id_pasien
					LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
					LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
					WHERE
					c.deleted_at IS NULL
					AND YEAR(c.tgl_mulai_sakit) = '".$i."'
					AND getKelompokUmurCampak(b.tanggal_lahir,c.tgl_mulai_sakit)='".$k."'
					AND c.klasifikasi_final IN ('CRS pasti(Lab Positif)','CRS Klinis')
					$between
					$wilayah
					GROUP BY getKelompokUmurCampak(b.tanggal_lahir,c.tgl_mulai_sakit)
				";
			}
			$data=DB::select($q);

			if(count($data)>0)$val = $data[0]->jml;
			else $val = 0;
			$x_data .= $comma.$val;
			$comma = ",";
		}
		$series .= $comma2."
			[
				'".$arr_kelompok_umur[$k]."',
				".$x_data."
			]
		";
		$comma2 = ",";
	}	
	$comma = "";
	for($i=$year_start;$i<=$year_end;$i++){
		$categories .= $comma."'".$i."'";
		$comma = ",";
	}
	?>

	$('#container_umur').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Jumlah Penderita <?php echo $suspek; ?> Berdasar Kelompok Umur<br><?php echo $names?>'
        },
        subtitle: {
            text: '<?php echo $nama_instansi; ?><br><?php echo $start_disp." s/d ".$end_disp; ?>',
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah Penderita'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: '<b>Penderita : {point.y:.1f}</b>'
        },
        series: [{
            name: 'Jumlah Penderita',
            data: [<?php echo $series?>],
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
		
	//###################################################################
	//# Status IMUNISASI
	//###################################################################		
		<?php 
			if($penyakit=="Campak"){
				$q = "
					SELECT 
						IF(c.vaksin_campak_sebelum_sakit='1',' 1 Kali ',
						IF(c.vaksin_campak_sebelum_sakit='2',' 2 Kali ',
						IF(c.vaksin_campak_sebelum_sakit='3',' 3 Kali ',
						IF(c.vaksin_campak_sebelum_sakit='4',' 4 Kali ',
						IF(c.vaksin_campak_sebelum_sakit='5',' 5 Kali ',
						IF(c.vaksin_campak_sebelum_sakit='6',' 6 Kali ',
						IF(c.vaksin_campak_sebelum_sakit='7',' Tidak ',
						IF(c.vaksin_campak_sebelum_sakit='8',' Tidak Tahu ',
						' Tidak Diisi '	
						)))))))) AS name,		
						COUNT(*) AS jml 
					FROM
					campak AS c
					JOIN hasil_uji_lab_campak AS d ON c.id_campak=d.id_campak
					JOIN pasien AS b ON d.id_pasien=b.id_pasien
					LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
					LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
					WHERE
					c.deleted_at IS NULL
					AND c.klasifikasi_final IN ('1','2','3')
					$between
					$wilayah
					GROUP BY c.vaksin_campak_sebelum_sakit
				";
			} else if($penyakit=="AFP"){
				$q = "
					SELECT 
						IF(c.imunisasi_polio_sebelum_sakit='1','1 Kali',
						IF(c.imunisasi_polio_sebelum_sakit='2','2 Kali',
						IF(c.imunisasi_polio_sebelum_sakit='3','3 Kali',
						IF(c.imunisasi_polio_sebelum_sakit='4','4 Kali',
						IF(c.imunisasi_polio_sebelum_sakit='5','5 Kali',
						IF(c.imunisasi_polio_sebelum_sakit='6','6 Kali',
						IF(c.imunisasi_polio_sebelum_sakit='7','Tidak',
						IF(c.imunisasi_polio_sebelum_sakit='8','Tidak tahu',
						'Tidak Diisi')))))))) AS name,	
						COUNT(*) AS jml 
					FROM afp AS c
					JOIN hasil_uji_lab_afp AS d ON c.id_afp=d.id_afp
					JOIN pasien b ON d.id_pasien=b.id_pasien
					LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
					LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
					WHERE
					c.deleted_at IS NULL
					$between
					$wilayah
					AND c.klasifikasi_final IN ('1')
					GROUP BY c.imunisasi_polio_sebelum_sakit
				";
			} else if($penyakit=="Difteri"){
				$q = "
					SELECT 
						IF(c.vaksin_DPT_sebelum_sakit='1','1 Kali',
						IF(c.vaksin_DPT_sebelum_sakit='2','2 Kali',
						IF(c.vaksin_DPT_sebelum_sakit='3','3 Kali',
						IF(c.vaksin_DPT_sebelum_sakit='4','4 Kali',
						IF(c.vaksin_DPT_sebelum_sakit='5','5 Kali',
						IF(c.vaksin_DPT_sebelum_sakit='6','6 Kali',
						IF(c.vaksin_DPT_sebelum_sakit='7','Tidak',
						IF(c.vaksin_DPT_sebelum_sakit='8','Tidak tahu',
						'Tidak Diisi')))))))) AS name,	
						COUNT(*) AS jml 
					FROM difteri AS c 
					JOIN hasil_uji_lab_difteri AS d ON c.id_difteri=d.id_difteri
					JOIN pasien b ON b.id_pasien=d.id_pasien
					LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
					LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
					WHERE
					c.deleted_at IS NULL
					$between
					$wilayah
					AND c.klasifikasi_final IN ('1','2')
					GROUP BY c.vaksin_DPT_sebelum_sakit
				";
			} else if($penyakit=="Tetanus"){
				$q = "
					SELECT 
						IF(c.status_imunisasi='1','TT2+',
						IF(c.status_imunisasi='2','TT1',
						IF(c.status_imunisasi='3','Tidak Imunisasi',
						IF(c.status_imunisasi='4','Tidak Jelas',
						'Tidak Diisi')))) AS name,
						COUNT(*) AS jml 
					FROM pasien_terserang_tetanus a 
					JOIN pasien b ON b.id_pasien=a.id_pasien
					JOIN tetanus c ON c.id_tetanus=a.id_tetanus
					LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
					LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
					WHERE
					c.deleted_at IS NULL
					$between
					$wilayah
					AND c.klasifikasi_akhir IN ('0')
					GROUP BY c.status_imunisasi
				";
			}
			
			$data=DB::select($q);

			$x_data = "";
			$comma = "";
			if($penyakit!='CRS'){
				for($i=0;$i<count($data);$i++){
					$x_data .= 	$comma."['".$data[$i]->name."',".$data[$i]->jml."]";
					$comma = ",";
				}
			}
		?>
		
		$('#container_imunisasi').highcharts({
			chart: {
				type: 'pie',
				options3d: {
					enabled: true,
					alpha: 45,
					beta: 0
				}
			},
			title: {
				text: 'Grafik Penderita <?php echo $suspek; ?> Berdasar Status Imunisasi<br><?php echo $names?>'
			},
			subtitle: {
				text: '<?php echo $nama_instansi; ?><br><?php echo $start_disp." s/d ".$end_disp; ?>',
				x: 0,
            	y: 55
			},
			tooltip: {
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					depth: 35,
					dataLabels: {
						enabled: true,
						format: '{point.name} <b>({point.y} Jiwa)<br>{point.percentage:.1f}%</b>'
					}
				}
			},
			series: [{
				type: 'pie',
				name: ' Penderita',
				data: [
					<?php echo $x_data; ?>
				]
			}]
		});
		
	//###################################################################
	//# KLASIFIKASI FINAL
	//###################################################################		
		<?php
			if($penyakit=="Campak"){
				$q = "
					SELECT
						CASE
							WHEN c.klasifikasi_final='1' THEN 'Campak(Lab)'
							WHEN c.klasifikasi_final='2' THEN 'Campak(Epid)'
							WHEN c.klasifikasi_final='3' THEN 'Campak(Klinis)'
							WHEN c.klasifikasi_final='4' THEN 'Rubella'
							WHEN c.klasifikasi_final='5' THEN 'Bukan Campak/rubella'
							WHEN c.klasifikasi_final='6' THEN 'Pending'
							ELSE 'Pending'
						END AS name,
						COUNT(*) AS jml  
					FROM campak AS c
					JOIN hasil_uji_lab_campak AS d ON c.id_campak=d.id_campak
					JOIN pasien AS b ON d.id_pasien=b.id_pasien
					LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
					LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
					WHERE 
					c.deleted_at IS NULL
					AND c.klasifikasi_final IS NOT NULL
					$between
					$wilayah
					GROUP BY c.klasifikasi_final
				";
			} else if($penyakit=="AFP"){
				$q = "
					SELECT
						CASE
							WHEN c.klasifikasi_final=NULL OR c.klasifikasi_final='' THEN 'Tidak Diisi'
							WHEN c.klasifikasi_final='1' THEN 'Polio'
							WHEN c.klasifikasi_final='2' THEN 'Bukan Polio'
							WHEN c.klasifikasi_final='2' THEN 'Compatible'
							ELse 'Undefined'
						END AS name,
						COUNT(*) AS `jml` 
					FROM afp AS c
					JOIN hasil_uji_lab_afp AS d ON c.id_afp=d.id_afp
					JOIN pasien b ON d.id_pasien=b.id_pasien
					LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
					LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
					WHERE 
					c.deleted_at IS NULL
					AND c.klasifikasi_final IS NOT NULL
					$between
					$wilayah
					GROUP BY c.klasifikasi_final
				";
			} else if($penyakit=="Difteri"){
				$q = "
					SELECT 
						CASE
							WHEN c.klasifikasi_final=NULL OR c.klasifikasi_final='' THEN 'Tidak Diisi'
							WHEN c.klasifikasi_final='1' THEN 'Probable'
							WHEN c.klasifikasi_final='2' THEN 'Konfirm'
							WHEN c.klasifikasi_final='2' THEN 'Negatif'
							ELse 'Undefined'
						END AS name,
						COUNT(*) AS `jml` 
					FROM difteri AS c 
					JOIN hasil_uji_lab_difteri AS d ON c.id_difteri=d.id_difteri
					JOIN pasien b ON b.id_pasien=d.id_pasien
					LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
					LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
					WHERE 
					c.deleted_at IS NULL
					AND c.klasifikasi_final IS NOT NULL
					$between
					$wilayah
					GROUP BY c.klasifikasi_final
				";
			} else if($penyakit=="Tetanus"){
				$q = "
					SELECT 
						CASE
							WHEN c.klasifikasi_final=NULL OR c.klasifikasi_final='' THEN 'Tidak Diisi'
							WHEN c.klasifikasi_final='1' THEN 'Konfirm TN'
							WHEN c.klasifikasi_final='2' THEN 'Tersangka TN'
							WHEN c.klasifikasi_final='2' THEN 'Bukan TN'
							ELse 'Undefined'
						END AS name,
						COUNT(*) AS `jml` 
					FROM pasien_terserang_tetanus a 
					JOIN pasien b ON b.id_pasien=a.id_pasien
					JOIN tetanus c ON c.id_tetanus=a.id_tetanus
					LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
					LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
					WHERE 
					c.deleted_at IS NULL
					AND c.klasifikasi_final IS NOT NULL
					$between
					$wilayah
					GROUP BY c.klasifikasi_akhir
				";
			} else if($penyakit='CRS'){
				$q = "
					SELECT
						c.klasifikasi_final AS name,
						COUNT(*) AS jml
					FROM
					crs AS c
					JOIN pasien AS b ON c.id_pasien=b.id_pasien
					LEFT JOIN rumahsakit2 AS j ON c.id_tempat_periksa=j.id
					LEFT JOIN puskesmas AS h ON c.id_tempat_periksa=h.puskesmas_id
					WHERE
					c.deleted_at IS NULL
					AND c.klasifikasi_final IS NOT NULL AND c.klasifikasi_final != ''
					$between
					$wilayah
					GROUP BY c.klasifikasi_final
				";
			}
			$data=DB::select($q);
			
			$x_data = "";
			$comma = "";
			for($i=0;$i<count($data);$i++){
				$x_data .= 	$comma."['".$data[$i]->name."',".$data[$i]->jml."]";
				$comma = ",";
			}
		?>
		
		$('#container_klasifikasi_final').highcharts({
			chart: {
				type: 'pie',
				options3d: {
					enabled: true,
					alpha: 45,
					beta: 0
				}
			},
			title: {
				text: 'Grafik Penderita <?php echo $arr_penyakit; ?> Berdasar Klasifikasi Final<br><?php echo $nk?>'
			},
			subtitle: {
				text: '<?php echo $nama_instansi; ?><br><?php echo $start_disp." s/d ".$end_disp; ?>',
				x: 0,
            	y: 55
			},
			tooltip: {
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					depth: 35,
					dataLabels: {
						enabled: true,
						format: '{point.name} <b>({point.y} Jiwa)<br>{point.percentage:.1f}%</b>'
					}
				}
			},
			series: [{
				type: 'pie',
				name: 'Penderita',
				data: [
					<?php echo $x_data; ?>
				]
			}]
		});