@extends('layouts.master')
@section('content')
<section class="scrollable padder">  
  <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
    <li><a href=""><i class="fa fa-home"></i> Home</a></li> 
  </ul>     
  <h3>Informasi Laboratorium</h3>
  <h4 class="inline text-muted m-t-n">Total <span class="m-l-xl m-r-sm">: </span></h4><h3 class="inline">{{$jml}}</h3>
  @if(Session::has('pesan'))
  <div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <strong>Horeee!</strong> {{Session::get('pesan')}}
  </div>
  @endif
  <section class="panel panel-default">
    <header class="panel-heading">
      <a href="{{URL::to('laboratorium/create')}}" class="btn btn-sm btn-default btn-rounded">Tambah Laboratorium</a>
    </header>
    <div class="table-responsive">
      <table class="table table-striped m-b-none ZsB5sTrf" data-ride="datatables">
        <thead>
          <tr>
            <th >No.</th>
            <th >Nama Laboratorium</th>
            <th >Email</th>
            <th >Alamat</th>
            <th >Telepon</th>
            <th>Faxmile</th>
            <!-- <th >Aksi</th> -->
          </tr>
        </thead>
      <tbody>
      <?php $no=1; ?>
      @if($laboratorium)
      @foreach($laboratorium as $data)
      <tr>
        <td>{{$no}}</td>
        <td>{{$data->nama_laboratorium}}</td>
        <td>{{$data->email}}</td>
        <td>{{$data->alamat}}</td>
        <td>{{$data->telepon}}</td>
        <td>{{$data->fax}}</td>
        <!-- <td>
          <a class="btn btn-primary btn-xs" href="{{URL::to('laboratorium/edit/'.$data->id_lab)}}"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ubah"></i></a>
          <a class="btn btn-warning btn-xs" href="{{URL::to('laboratorium/hapus/'.$data->id_lab)}}" onclick="alert('yakin ini menghapus data ini?')"><i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus"></i></a>
        </td> -->
      </tr>
      <?php $no++?>
      @endforeach
      @endif
      </tbody>
      </table>
    </div>
  </section>
</section>
@stop