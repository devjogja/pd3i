@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <h4>Pemeriksaan Lab Campak </h4> 
                    <hr>
                </div>
                <ul class="profile-tab nav nav-tabs">
                    <li class="active"><a href="#inputDataIndividu" data-toggle="tab">Input data individual kasus</a></li>
                    <li ><a href="#inputHasilLaboratorium" data-toggle="tab">Input Hasil Laboratorium</a></li>
                </ul>
                <div class="profile-tab-content tab-content">
                    <div class="tab-pane fade active in" id="inputDataIndividu">
                        @include('laboratorium.campak.inputkasus')
                    </div>
                    <div class="tab-pane fade  in" id="inputHasilLaboratorium">
                        @include('laboratorium.campak.inputlab')
                        @include('laboratorium.campak.inputserologi')
                        @include('laboratorium.campak.inputvirologi')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop