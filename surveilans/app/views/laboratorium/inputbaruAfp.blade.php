@extends('layouts.master')
@section('content')
<div class="span12">
	<div class="content">
		<div class="module">
			<div class="module-body" >
				<div class="profile-head media">
					<h4>Penyakit AFP</h4>
					<hr>
				</div>

			{{-- Tab Navigasi --}}
			<ul class="profile-tab nav nav-tabs">
				<li class="active"><a href="#input" data-toggle="tab">Input data individual kasus</a></li>
				<li><a href="#input_lab" data-toggle="tab">Input hasil laboratorium</a></li>
			</ul>

			{{-- Content Dari Tab --}}
				<div class="profile-tab-content tab-content">
				{{-- Daftar Kasus --}}
				{{-- Input Data Individual Kasus --}}
					<div class="tab-pane fade active in" id="input">
					<div class="alert alert-error">
					   <p style="font-size:12px">Text input yang bertanda bintang <strong>(*)</strong> wajib di isi
					</div>
					{{ Form::open(array(
					    'route' =>'afp.store',
					    'class' =>'form-horizontal',
					    'id'    =>'form_save_afp'
					)) }}
					<div class="module-body">
					  <div class="row-fluid">
					    <div class="span6">
					      <div class="media">
					        <fieldset>
					          <legend>Identitas pasien</legend>
					          <div class="control-group">
					            <label class="control-label">Nama penderita</label>
					            <div class="controls">
					              {{ 
					                Form::text
					                (
					                  'nama_anak', 
					                  Input::old('nama_anak'), 
					                  array
					                  (
					                    'data-validation' => '[MIXED]',
					                    'data' => '$ wajib di isi!', 
					                    'data-validation-message' => 'wajib di isi!',
					                    'class' => 'input-xlarge',
					                    'placeholder'=>'Nama penderita'
					                  )
					                ) 
					              }}
					              <span class="help-inline" id="error-nama_anak" style="color:red">(*)</span>
					            </div>
					          </div>

					          <div class="control-group">
					            <label class="control-label">NIK</label>
					            <div class="controls">
					                {{ Form::text('nik', Input::old('nik'), array('class' => 'input-medium', 'placeholder' => 'Nomer induk kependudukan')) }}
					              <span class="help-inline"></span>
					            </div>
					          </div>

					          <div class="control-group">
					            <label class="control-label">Nama orang tua</label>
					            <div class="controls">
					              {{ Form::text('nama_ortu', Input::old('nama_ortu'), array(
					                    'data-validation' => '[MIXED]',
					                    'data' => '$ wajib di isi!', 
					                    'data-validation-message' => 'wajib di isi!',
					                    'class' => 'input-xlarge',
					                    'placeholder' => 'Nama orang tua')) }}
					              <span class="help-inline" id="error-nama_ortu" style="color:red">(*)</span>
					            </div>
					          </div>

					          <div class="control-group">
					            <label class="control-label">Jenis kelamin</label>
					            <div class="controls">
					              {{Form::select
					                (
					                  'jenis_kelamin', 
					                  array
					                  (
					                    '' => 'Pilih',
					                    '0'=>'Laki-laki',
					                    '1'=>'Perempuan',
					                    '2' => 'Tidak jelas'
					                  ),
					                  null,
					                  array
					                  (
					                    'data-validation'=>'[MIXED]',
					                    'data'=>'$ wajib di isi!',
					                    'data-validation-message'=>'jenis kelamin wajib di isi!',
					                    'class' => 'input-medium id_combobox'
					                  )
					                )
					              }}
					              <span class="help-inline" style="color:red">(*)</span>
					            </div>
					          </div>

					          <div class="control-group">
					            <label class="control-label">Tanggal lahir</label>
					            <div class="controls">
					              {{Form::text('tanggal_lahir', Input::old('tanggal_lahir'), array(
					                    'data-validation' => '[MIXED]',
					                    'data' => '$ wajib di isi!', 
					                    'data-validation-message' => 'wajib di isi!',
					                    'class' => 'input-medium tgl_lahir', 
					                    'placeholder' => 'Tanggal lahir', 
					                    'data-uk-datepicker' => '{format:"DD-MM-YYYY"}', 
					                    'onchange' => 'umur()'))}}
					              <span class="help-inline" id="error-tanggal_lahir" style="color:red">(*)</span>
					            </div>
					          </div>

					          <div class="control-group">
					          <label class="control-label">Umur</label>
					            <div class="controls">
					              <input type="text" class="input-mini"  name="tgl_tahun" id="tgl_tahun" onchange="tgl_lahir()">&nbsp Thn &nbsp
					              <input type="text" class="input-mini" name="tgl_bulan" id="tgl_bulan" onchange="tgl_lahir()">&nbsp Bln &nbsp
					              <input type="text" class="input-mini" name="tgl_hari" id="tgl_hari" onchange="tgl_lahir()">&nbsp Hr &nbsp
					            </div>
					          </div>

					          <div class="control-group">
					            <label class="control-label">Nama puskesmas</label>
					            <div class="controls">
					              <?php
					                //ambil nama puskesmas scara otomatis sesuai user yg login
					                    $type      = Session::get('type');
					                    $kd_faskes = Session::get('kd_faskes');
					                    if($type == "puskesmas") {
					                        $q = "
					                            SELECT b.puskesmas_name,b.alamat
					                            FROM puskesmas b
					                            WHERE b.puskesmas_code_faskes='".$kd_faskes."'
					                        ";
					                        $data=DB::select($q);
					                        for($i=0;$i<count($data);$i++) {
					                            $nama_instansi = "PUSKESMAS ". $data[0]->puskesmas_name;
					                        echo Form::text('nama_puskesmas', $nama_instansi, array('class' => 'input-large id_combobox','placeholder'=>'Masukan nama puskesmas')) ;
					                        }
					                    }
					                ?>
					              <span class="help-inline"></span>
					            </div>
					          </div>

					          <div class="control-group">
					            <label class="control-label">Propinsi</label>
					            <div class="controls">
					              {{ Form::select(
					                '', 
					                array(
					                  'placeholder'=>'Pilih provinsi')+Provinsi::lists('provinsi','id_provinsi'),
					                  null,
					                  array(
					                    'id'=>'id_provinsi',
					                    'placeholder' => 'Pilih Provinsi',
					                    'onchange'=>'showKabupaten()',
					                    'class'=>'input-large id_combobox'
					                  )
					                )
					              }}
					              <span class="help-inline" id="error-id_propinsi"></span>
					            </div>
					          </div>

					          <div class="control-group">
					            <label class="control-label">Kabupaten</label>
					            <div class="controls">
					              {{ Form::select('id_kabupaten', array(''), null, array('class' => 'input-medium id_combobox', 'id' => 'id_kabupaten', 'onchange' => 'showKecamatan()')) }}
					              <span class="help-inline" id="error-id_kabupaten"></span>
					            </div>
					          </div>

					          <div class="control-group">
					            <label class="control-label">Kecamatan</label>
					            <div class="controls">
					              {{ Form::select('id_kecamatan', array(''), null, array('class' => 'input-medium id_combobox','id' => 'id_kecamatan', 'onchange' => 'showKelurahan()')) }}
					              <span class="help-inline" id="error-id_kecamatan"></span>
					            </div>
					          </div>

					          <div class="control-group">
					            <label class="control-label">Kelurahan/Desa</label>
					            <div class="controls">
					              {{ Form::select('id_kelurahan', array(''), null, array(
					                    'data-validation' => '[MIXED]',
					                    'data' => '$ wajib di isi!', 
					                    'data-validation-message' => 'wajib di isi!',
					                    'class' => 'input-medium id_combobox', 
					                    'id' => 'id_kelurahan', 
					                    'onchange' => 'showEpidAfp()')) }}
					              <span class="help-inline" id="error-id_kelurahan" style="color:red">*)</span>
					            </div>
					          </div>

					          <div class="control-group">
					            <label class="control-label">Alamat</label>
					            <div class="controls">
					              {{ Form::textarea('alamat', null, array('rows' => '7', 'placeholder' => 'Hanya diisi nama jalan, no. rumah dan no. RT - RW')) }}
					              <span class="help-inline"></span>
					            </div>
					          </div>

					          <div class="control-group">
					            <label class="control-label">No Epidemologi</label>
					            <div class="controls">
					             {{ 
					              Form::text
					              (
					                'no_epid', 
					                Input::old('no_epid'), 
					                array
					                (
					                  'data-validation' => '[MIXED]',
					                  'data' => '$ wajib di isi!', 
					                  'data-validation-message' => 'no epid wajib di isi!', 
					                  'class' => 'input-medium', 
					                  'placeholder' => 'Nomer epidemologi', 
					                  'id' => 'epid', 
					                  'onchange' => 'showEpidAfp()'
					                )
					              ) 
					            }}
					              <span class="help-inline" id="error-no_epid" style="color:red">(*)</span>
					            </div>
					          </div>

					          <div class="control-group">
					            <label class="control-label">No Epidemologi lama</label>
					            <div class="controls">
					             {{ Form::text('no_epid_lama', Input::old('no_epid_lama'), array('class' => 'input-medium', 'placeholder' => 'Nomer epidemologi lama')) }}
					              <span class="help-inline" style="color:red"></span>
					            </div>
					          </div>
					          </fieldset>
					        </div> <!-- /.media -->
					      </div> <!-- /.span6 -->

					      <div class="span6">
					        <div class="media">
					            <fieldset>
					              <legend>Data surveilans AFP</legend>
					                <div class="control-group">
					                  <label class="control-label">Tanggal mulai lumpuh (bukan karena ruda paksa)</label>
					                  <div class="controls">
					                    {{ Form::text('tanggal_mulai_lumpuh', Input::old('tanggal_mulai_lumpuh'), array(
					                      'data-validation' => '[MIXED]',
					                      'data' => '$ wajib di isi!', 
					                      'data-validation-message' => 'wajib di isi!',
					                      'class' => 'input-medium tgl_mulai_sakit', 
					                      'data-uk-datepicker' => '{format:"DD-MM-YYYY"}', 
					                      'onchange' => 'tgl_lahir()')) }}
					                    <span class="help-inline" id="error-tanggal_mulai_lumpuh" style="color:red">(*)</span>
					                  </div>
					                </div>

					                <div class="control-group">
					                  <label class="control-label">Demam sebelum lumpuh (bukan karena ruda paksa)</label>
					                  <div class="controls">
					                    {{ Form::select('demam_sebelum_lumpuh', array('' => 'Pilih', '0' => 'Ya', '1' => 'Tidak'), Input::old('demam_sebelum_lumpuh'), array(
					                    'data-validation' => '[MIXED]',
					                    'data' => '$ wajib di isi!', 
					                    'data-validation-message' => 'wajib di isi!',
					                    'class' => 'input-small id_combobox')) }}
					                    <span class="help-inline" id="error-demam_sebelum_lumpuh" style="color:red">(*)</span>
					                  </div>
					                </div>

					                <div class="control-group">
					                  <label class="control-label">Kelumpuhan anggota gerak kanan</label>
					                  <div class="controls">
					                    {{ Form::select('kelumpuhan_anggota_gerak_kanan', array('' => 'Pilih', '0' => 'lengan (1)', '1' => 'tungkai (1)', '2' => 'lengan dan tungkai (2)', '3' => 'Tidak'), Input::old('kelumpuhan_anggota_gerak_kanan'), array('class' => 'input-medium id_combobox')) }}
					                    <span class="help-inline"></span>
					                  </div>
					                </div>

					                <div class="control-group">
					                  <label class="control-label" >Kelumpuhan anggota gerak kiri</label>
					                  <div class="controls">
					                    {{ Form::select('kelumpuhan_anggota_gerak_kiri', array('' => 'Pilih', '0' => 'lengan (1)', '1' => 'tungkai (1)', '2' => 'lengan dan tungkai (2)', '3' => 'Tidak'), Input::old('kelumpuhan_anggota_gerak_kiri'), array('class' => 'input-medium id_combobox')) }}
					                    <span class="help-inline"></span>
					                  </div>
					                </div>

					                <div class="control-group">
					                  <label class="control-label" >Gangguan raba anggota gerak kanan</label>
					                  <div class="controls">
					                    {{ Form::select('gangguan_raba_gerak_kanan', array('' => 'Pilih', '0' => 'lengan (1)', '1'=>'tungkai (1)', '2' => 'lengan dan tungkai (2)', '3' => 'Tidak'), Input::old('gangguan_raba_gerak_kanan'), array('class' => 'input-medium id_combobox')) }}
					                    <span class="help-inline"></span>
					                  </div>
					                </div>

					                <div class="control-group">
					                  <label class="control-label" >Gangguan raba anggota gerak kiri</label>
					                  <div class="controls">
					                    {{ Form::select('gangguan_raba_gerak_kiri', array('' => 'Pilih', '0' => 'lengan (1)', '1' => 'tungkai (1)', '2' => 'lengan dan tungkai (2)', '3' => 'Tidak'), Input::old('gangguan_raba_gerak_kiri'), array('class' => 'input-medium id_combobox')) }}
					                    <span class="help-inline"></span>
					                  </div>
					                </div>

					                <div class="control-group">
					                  <label class="control-label" >Imunisasi polio sebelum sakit</label>
					                  <div class="controls">
					                    {{ Form::select('imunisasi_polio_sebelum_sakit', array('' => 'Pilih', '0' => '1X', '1' => '2X', '2' => '3X', '3' => '4X', '4' => '5X', '5' => '6X', '7' => 'Tidak', '8' => 'Tidak tahu'), Input::old('imunisasi_polio_sebelum_sakit'), array(
					                    'data-validation' => '[MIXED]',
					                    'data' => '$ wajib di isi!', 
					                    'data-validation-message' => 'wajib di isi!',
					                    'class' => 'input-medium id_combobox')) }}
					                    <span class="help-inline" id="error-imunisasi_polio_sebelum_sakit" style="color:red">(*)</span>
					                  </div>
					                </div>

					                <div class="control-group">
					                  <label class="control-label" >Tanggal imunisasi polio terakhir</label>
					                  <div class="controls">
					                    {{ Form::text('tanggal_vaksinasi_polio', Input::old('tanggal_vaksinasi_polio'), array(
					                    'data-validation' => '[MIXED]',
					                    'data' => '$ wajib di isi!', 
					                    'data-validation-message' => 'wajib di isi!',
					                    'class' => 'input-medium', 
					                    'data-uk-datepicker' => '{format:"DD-MM-YYYY"}')) }}
					                    <span class="help-inline" style="color:red">(*)</span>
					                  </div>
					                </div>

					                <div class="control-group">
					                  <label class="control-label">Tanggal laporan diterima</label>
					                  <div class="controls">
					                    {{ Form::text('tanggal_laporan_diterima', Input::old('tanggal_laporan_diterima'), array(
					                    'data-validation' => '[MIXED]',
					                    'data' => '$ wajib di isi!', 
					                    'data-validation-message' => 'wajib di isi!',
					                    'class' => 'input-medium', 
					                    'data-uk-datepicker' => '{format:"DD-MM-YYYY"}')) }}
					                    <span class="help-inline" style="color:red">(*)</span>
					                  </div>

					                </div>

					                <div class="control-group">
					                  <label class="control-label">Tanggal spesimenan</label>
					                  <div class="controls">
					                    {{ Form::text('tanggal_spesimenan', Input::old('tanggal_spesimenan'), array(
					                    'data-validation' => '[MIXED]',
					                    'data' => '$ wajib di isi!', 
					                    'data-validation-message' => 'wajib di isi!',
					                    'class' => 'input-medium', 
					                    'data-uk-datepicker' => '{format:"DD-MM-YYYY"}')) }}
					                    <span class="help-inline" id="error-tanggal_spesimenan" style="color:red">(*)</span>
					                  </div>
					                </div>

					                <div class="control-group">
					                  <label class="control-label" >Kontak</label>
					                  <div class="controls">
					                    {{ Form::select('kontak', array('' => 'Pilih', '0' => 'Ya', '1' => 'Tidak'), Input::old('kontak'), array(
					                    'data-validation' => '[MIXED]',
					                    'data' => '$ wajib di isi!', 
					                    'data-validation-message' => 'wajib di isi!',
					                    'class' => 'input-small id_combobox')) }}
					                    <span class="help-inline" id="error-kontak" style="color:red">(*)</span>
					                  </div>
					                </div>

					                <div class="control-group">
					                  <label class="control-label" >Keadaan akhir</label>
					                  <div class="controls">
					                    {{ Form::select('keadaan_akhir', array('' => 'Pilih', '0' => 'Hidup', '1' => 'Meninggal'), Input::old('keadaan_akhir'), array(
					                    'data-validation' => '[MIXED]',
					                    'data' => '$ wajib di isi!', 
					                    'data-validation-message' => 'wajib di isi!',
					                    'class' => 'input-medium id_combobox')) }}
					                    <span class="help-inline" id="error-keadaan_akhir" style="color:red">(*)</span>
					                  </div>
					                </div>

					              </fieldset>
					            </div> <!-- /.media -->
					          </div><!-- /.span6 -->
					          </div>

                        		<div class="row-fluid">
					          	<div class="span12">
					            <div class="media">
					              <fieldset>
					                <legend>Data spesimen dan laboratorium</legend>

					                <div class="control-group">
					                  <label class="control-label" >Tanggal pengambilan spesimen I</label>
					                  <div class="controls">
					                    {{ Form::text('tanggal_pengambilan_spesimen1', Input::old('tanggal_pengambilan_spesimen1'), array(
					                    'data-validation' => '[MIXED]',
					                    'data' => '$ wajib di isi!', 
					                    'data-validation-message' => 'wajib di isi!',
					                    'class' => 'input-medium', 
					                    'data-uk-datepicker' => '{format:"DD-MM-YYYY"}')) }}
					                    <span class="help-inline" style="color:red">(*)</span>
					                  </div>
					                </div>

					                <div class="control-group">
					                  <label class="control-label" >Tanggal pengambilan spesimen II</label>
					                  <div class="controls">
					                    {{ Form::text('tanggal_pengambilan_spesimen2', Input::old('tanggal_pengambilan_spesimen2'), array(
					                    'data-validation' => '[MIXED]',
					                    'data' => '$ wajib di isi!', 
					                    'data-validation-message' => 'wajib di isi!',
					                    'class' => 'input-medium', 
					                    'data-uk-datepicker' => '{format:"DD-MM-YYYY"}')) }}
					                    <span class="help-inline" style="color:red">(*)</span>
					                  </div>
					                </div>

					                <div class="control-group">
					                  <label class="control-label" >Jenis pemeriksaan laboratorium spesimen I
					                    <input type="checkbox" name="jenis_pemeriksaan1" value="I" onclick="pilih_periksa1(this)">
					                  </label>
					                  <div class="controls">
					                    <table>
					                      <tr>
					                        <td>
					                          <input type="checkbox" id="isolasi_virus1" value="Isolasi virus" name="nama_jenis_pemeriksaan1[]" disabled onchange="change_virus1(this)">
					                          Isolasi virus
					                        </td>
					                        <td>
					                          {{ Form::text('ket_jenis_pemeriksaan1[]', null, array('class' => 'input-medium cek_periksa_virus1', 'placeholder' => '','disabled' => 'disabled')) }}
					                        </td>
					                      </tr>
					                      <tr>
					                        <td>
					                          <input type="checkbox" id="ITD1" value="ITD" name="nama_jenis_pemeriksaan1[]" disabled onchange="change_itd1(this)">
					                          ITD
					                        </td>
					                        <td>
					                          {{ Form::text('ket_jenis_pemeriksaan1[]', null, array('class' => 'input-medium cek_periksa_itd1', 'placeholder' => '', 'disabled' => 'disabled')) }}
					                        </td>
					                      </tr>
					                      <tr>
					                        <td>
					                          <input type="checkbox" id="Sequencing1" value="Sequencing" name="nama_jenis_pemeriksaan1[]" disabled onchange="change_sq1(this)">
					                          Sequencing
					                        </td>
					                        <td>
					                          {{ Form::text('ket_jenis_pemeriksaan1[]', null, array('placeholder' => '','disabled' => 'disabled', 'class' => 'cek_periksa_sq1')) }}
					                        </td>
					                      </tr>
					                    </table>
					                  </div>
					                </div>

					                <div class="control-group">
					                  <label class="control-label" >Jenis pemeriksaan laboratorium spesimen II
					                    <input type="checkbox" name="jenis_pemeriksaan2" value="II" onclick="pilih_periksa2(this)">
					                  </label>
					                  <div class="controls">
					                    <table>
					                      <tr>
					                        <td>
					                          <input type="checkbox" id="isolasi_virus2" value="Isolasi virus" name="nama_jenis_pemeriksaan2[]" disabled onchange="change_virus2(this)">
					                          Isolasi virus
					                        </td>
					                        <td>
					                          {{ Form::text('ket_jenis_pemeriksaan2[]', null, array('class' => 'input-medium cek_periksa_virus2', 'placeholder' => '', 'disabled' => 'disabled')) }}
					                        </td>
					                      </tr>
					                      <tr>
					                        <td>
					                          <input type="checkbox" id="ITD2" value="ITD" name="nama_jenis_pemeriksaan2[]" disabled onchange="change_itd2(this)">
					                          ITD
					                        </td>
					                        <td>
					                          {{ Form::text('ket_jenis_pemeriksaan2[]', null, array('class' => 'input-medium cek_periksa_itd2', 'placeholder' => '', 'disabled' => 'disabled')) }}
					                        </td>
					                      </tr>
					                      <tr>
					                        <td>
					                          <input type="checkbox" id="Sequencing2" value="Sequencing" name="nama_jenis_pemeriksaan2[]" disabled onchange="change_sq2(this)">
					                          Sequencing
					                        </td>
					                        <td>
					                          {{ Form::text('ket_jenis_pemeriksaan2[]', null, array('placeholder' => '', 'class' => 'input-medium cek_periksa_sq2', 'disabled' => 'disabled')) }}
					                        </td>
					                      </tr>
					                    </table>
					                  </div>
					                </div>

					                <div class="control-group">
					                  <label class="control-label" >Jenis spesimen</label>
					                  <div class="controls">
					                    {{ Form::select('jenis_spesimen', array('' => 'Pilih', '0' => 'Darah', '1' => 'Urin'), Input::old('jenis_spesimen'), array(
					                    'data-validation' => '[MIXED]',
					                    'data' => '$ wajib di isi!', 
					                    'data-validation-message' => 'wajib di isi!',
					                    'class' => 'input-small id_combobox')) }}
					                    <span class="help-inline" id="error-jenis_spesimen" style="color:red">(*)</span>
					                  </div>
					                </div>

					                <!-- <div class="control-group">
					                  <label class="control-label" >Hasil lab spesimen I
					                 </label>
					                  <div class="controls">
					                    <table>
					                      <tr>
					                        <td>
					                          Isolasi virus
					                        </td>
					                        <td>
					                          {{ Form::text('ket_jenis_pemeriksaan1[]', null, array('class' => 'input-medium', 'placeholder' => '')) }}
					                        </td>
					                      </tr>
					                      <tr>
					                        <td>
					                          ITD
					                        </td>
					                        <td>
					                          {{ Form::text('ket_jenis_pemeriksaan1[]', null, array('class' => 'input-medium', 'placeholder' => '')) }}
					                        </td>
					                      </tr>
					                      <tr>
					                        <td>
					                          Sequencing
					                        </td>
					                        <td>
					                          {{ Form::text('ket_jenis_pemeriksaan1[]', null, array('placeholder' => '')) }}
					                        </td>
					                      </tr>
					                    </table>
					                  </div>
					                </div>

					                <div class="control-group">
					                  <label class="control-label" >Hasil lab spesimen II
									 </label>
					                  <div class="controls">
					                    <table>
					                      <tr>
					                        <td>
					                          Isolasi virus
					                        </td>
					                        <td>
					                          {{ Form::text('ket_jenis_pemeriksaan1[]', null, array('class' => 'input-medium ', 'placeholder' => '')) }}
					                        </td>
					                      </tr>
					                      <tr>
					                        <td>
					                          ITD
					                        </td>
					                        <td>
					                          {{ Form::text('ket_jenis_pemeriksaan1[]', null, array('class' => 'input-medium', 'placeholder' => '')) }}
					                        </td>
					                      </tr>
					                      <tr>
					                        <td>
					                          Sequencing
					                        </td>
					                        <td>
					                          {{ Form::text('ket_jenis_pemeriksaan1[]', null, array('placeholder' => '')) }}
					                        </td>
					                      </tr>
					                    </table>
					                  </div>
					                </div> -->

					                <div class="control-group">
										<label class="control-label">Hasil Spesimen</label>
										<div class="controls">
											<table class="table table-bordered hasil_spesimen">
												<tr>
													<td>Spesimen ke</td>
													<td>Isolasi virus</td>
													<td>ITD</td>
													<td>Squencing</td>
												</tr>
											</table>
											</br>
											<div id="R_spesimen">
												<table class="table">
													<tr>
														<td>Isolasi virus</td>
														<td><input type="text" class="isolasi_virus"></td>
													</tr>
													<tr>
														<td>ITD</td>
														<td><input type="text" class="ITD"></td>
													</tr>
													<tr>
														<td>Squencing</td>
														<td><input type="text" class="squencing"></td>
													</tr>
												</table>
												<br/>
												<a class="btn tutup_spesimen">Tutup</a>&nbsp;
												<button class="btn btn-success simpan_spesimen">Tambah</button>
												<button class="btn btn-success edit_spesimen">edit</button>
											</div>
											<a class="btn show_spesimen">Tampilkan untuk tambah hasil spesimen</a>
										</div>
									</div>

					                <div class="control-group">
					                  <label class="control-label">Klasifikasi final</label>
					                  <div class="controls">
					                    {{ Form::select('klasifikasi_final', array('' => 'Pilih', '0' => 'Polio', '1' => 'Bukan polio'), Input::old('klasifikasi_final'), array('class' => 'input-medium id_combobox')) }}
					                    <span class="help-inline" id="error-klasifikasi_final"></span>
					                  </div>
					              </div>

					            </fieldset>
					          </div> <!-- /.media -->
					        </div> <!-- /.span11 -->

					      </div> <!-- /.row-fluid -->
					    </div> <!-- /.module-body -->

					    <!-- awal class form-actions -->
					    <div class="form-actions">
					        {{Form::submit('simpan',array('class'=>'btn btn-primary'))}}
					        {{Form::reset('batal',array('class'=>'btn btn-warning'))}}
					    </div>
					    <!-- akhir class form-actions -->
					{{Form::close()}}
					</div>
					<div class="tab-pane fade" id="input_lab">
						oke
					</div>
				</div> <!-- /.tab-content-->
			</div> <!--/.module-body-->
		</div> <!--/.module-->
	</div> <!--/.content-->
</div> <!--/.span9-->

<script>
	$( "#id_kelurahan" ).change(function() {
    $.ajax({
          data:'id_kelurahan='+$('#id_kelurahan').val()+'&date='+$('.tgl_mulai_sakit').val(),
          url:'{{URL::to("ambil_epid_afp")}}',
          success:function(data) {
          $('#epid').val(data);
        }
        });
    });

    $( ".tgl_mulai_sakit" ).change(function() {
    $.ajax({
          data:'id_kelurahan='+$('#id_kelurahan').val()+'&date='+$('.tgl_mulai_sakit').val(),
          url:'{{URL::to("ambil_epid_afp")}}',
          success:function(data) {
          $('#epid').val(data);
        }
        });
    });
   
    $(document).ready(function(){

    	$("select").select2();
    	$('.edit_spesimen').hide();
		$('.hasil_spesimen').load('{{URL::to("hasil_spesimen/'+$('.no_epid').val()+'")}}');
		$("#R_spesimen").hide();
    	$("#example_afp_length select").select2();
	    

	    $(".show_spesimen").click(function(e){
					e.preventDefault();
					$("#R_spesimen").show(1000);
					$('.show_spesimen').hide();
				});

	  	$(".tutup_spesimen").click(function(e){
			e.preventDefault();
			$("#R_spesimen").hide(2000);
			$('.nama_spesimen').val('');
	        $('.profesi_spesimen').val('');;
			$('.show_spesimen').show();
			$('.edit_spesimen').hide();
			$('.simpan_spesimen').show();
		});

		
		$(".simpan_spesimen").click(function(e){
			e.preventDefault();
			$.ajax({
	    	data:'no_epid='+$('.no_epid').val()+'&nama='+$('.nama_spesimen').val()+'&profesi='+$('.profesi_spesimen').val(),
	        url:'{{URL::to("tambah_spesimen")}}',
	        success:function(data) {
	        $('.nama_spesimen').val('');
	        $('.profesi_spesimen').val('');
	        $("#R_spesimen").hide(2000);
			$('.show_spesimen').show(1000)
			if(data=='sukses') 
			{
				$('.hasil_spesimen').load('{{URL::to("hasil_spesimen/'+$('.no_epid').val()+'")}}');
				
			}
	   		}
	  	});
		});

		$(".edit_spesimen").click(function(e){
			e.preventDefault();
			$.ajax({
	    	data:'id='+$('.id').val()+'&nama='+$('.nama_spesimen').val()+'&profesi='+$('.profesi_spesimen').val(),
	        url:'{{URL::to("edit_spesimen")}}',
	        success:function(data) {
	        $('.id_spesimen').val('');
	        $('.nama_spesimen').val('');
	        $('.profesi_spesimen').val('');
	        $("#R_spesimen").hide(2000);
			$('.show_spesimen').show(1000)
			if(data=='sukses') 
			{
				$('.hasil_spesimen').load('{{URL::to("hasil_spesimen/'+$('.no_epid').val()+'")}}');
				$('.edit_spesimen').hide();
				$('.simpan_spesimen').show();
			}
	   		}
	  	});
		});

      $('#form_save_afp').validate({
              submit: {
                settings: {
                  scrollToError: {
                      offset: -100,
                      duration: 500
                  }
                }
              }
            });

            $('#form_save_afp input').keydown(function(e){
             if(e.keyCode==13){       

                if($(':input:eq(' + ($(':input').index(this) + 1) + ')').attr('type')=='submit'){// check for submit button and submit form on enter press
                 return true;
                }

                $(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();

               return false;
             }

            });
        });

		//fungsi edit petugas spesimen pada modul PE tetanus
		function edit_spesimen(id)
		{
		    $.ajax({
		        //dataType:'json',
		        data: 'id='+id,
		        url:'{{URL::to("tampil_edit_hasil_spesimen")}}',
		        success:function(data) {
		            $('.id').val(data.id);
		            $('.nama_spesimen').val(data.nama_pemeriksa);
		            $('.profesi_spesimen').val(data.profesi);
		            $("#R_spesimen").show(2000);
		            $('.show_spesimen').hide()
		            $('.edit_spesimen').show();
		            $('.simpan_spesimen').hide();
		        }
		    });
		    return false;
		}

</script>
	@include('pemeriksaan.campak.analisis-js')

    @include('pemeriksaan.afp.afp-js')
@stop