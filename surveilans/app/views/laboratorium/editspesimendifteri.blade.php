@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <div class="media-body">
                        <h4>
                            Pemeriksaan spesimen <small>Laboratorium Sardjito</small>
                        </h4>
                        <div class="span6">
                        <p style="font-size:9px;">
                            <table class="table table-striped">
                            @if($tersangka)
                            @foreach($tersangka as $row)
                                <tr>
                                    <td>No. Epidemologi</td>
                                    <td>{{$row->no_epid}}</td>
                                </tr>
                                <tr>
                                    <td>Nama Pasien</td>
                                    <td>{{$row->nama_anak}}</td>
                                </tr>
                                <tr>
                                    <td>Jenis Kelamin</td>
                                    @if($row->jenis_kelamin==1)
                                    <td>Laki-laki</td>
                                    @else
                                    <td>Perempuan</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Tanggal Lahir</td>
                                    <td>{{$row->tanggal_lahir}}</td>
                                </tr>
                                <tr>
                                    <td>Umur</td>
                                    <td>{{$row->umur}} tahun</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Di ambil Spesimen Darah</td>
                                    <td>{{$row->tanggal_diambil_spesimen_tenggorokan}}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Di ambil Spesimen Urin</td>
                                    <td>{{$row->tanggal_diambil_spesimen_hidung}}</td>
                                </tr>
                            </table>
                            @endforeach
                            @endif
                            <hr>
                        </p>
                        </div>
                    </div>
                </div>
                <ul class="profile-tab nav nav-tabs">
                    <li class="active"><a href="#activity" data-toggle="tab">Masukan Hasil Laborarotium</a></li>
                </ul>
                <div class="profile-tab-content tab-content">
                    <div class="tab-pane fade active in" id="activity">
                        <div class="stream-list">
                            <div class="media stream">
                                <div class="media-body">
                                <div class="stream-list">
                        <div class="module-body">
                            {{Form::open(array('url'=>'laboratorium/simpanspesimendifteri','class'=>'form-horizontal row-fluid'))}}
                            @foreach($tersangka as $rows)
                                <input name="id_pasien" value="{{$rows->id_pasien}}" type="hidden"/>
                                <input name="id_difteri" value="{{$rows->id_difteri}}" type="hidden"/>
                            @endforeach
                            <div class="control-group">
                                <label class="control-label span3" for="basicinput">No. Spesimen</label>
                                <div class="controls span6">
                                    {{Form::text('no_spesimen',null,array('class'=>'form-control span8'))}}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label span3" for="basicinput">Tanggal terima spesimen di laboratorium</label>
                                <div class="controls span6">
                                    <input data-uk-datepicker="{format:'YYYY-MM-DD'}" name="tanggal_terima_spesimen" type="text" class="form-control span8"/>  
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label span3" for="basicinput">Kondisi spesimen waktu di laboratorium</label>
                                <div class="controls span6">
                                    <select name="kondisi_spesimen" type="text" class="form-control span8">
                                        <option value="adekuat">Adekuat</option>
                                        <option value="non adekuat">Non adekuat</option>
                                    </select>  
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label span3" for="basicinput">Hasil Spesimen Tenggorokan</label>
                                <div class="controls span6">
                                    <input name="hasil_spesimen_tenggorokan" type="text" class="form-control span8"/>  
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label span3" for="basicinput">Hasil Spesimen Hidung</label>
                                <div class="controls span6">
                                    <input name="hasil_spesimen_hidung" type="text" class="form-control span8"/>  
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label span3" for="basicinput">Hasil pemeriksaan</label>
                                <div class="controls span6">
                                    <select name="hasil_pemeriksaan_nonpolio_enterovirus" type="text" class="form-control span8">
                                        <option value="probable">Probable (negatif)</option>
                                        <option value="konfirmasi">Konfirmasi (positif)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label span3" for="basicinput">Klasifikasi Akhir</label>
                                <div class="controls span6">
                                    <input name="klasifikasi_akhir" type="text" class="form-control span8"/>  
                                </div>
                            </div>

                             <div class="control-group">
                                <label class="control-label span3">Keterangan</label>
                                <div class="controls span6">
                                    <textarea class="form-control span8" name="keterangan"></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label span3">Tanggal Uji laboratorium</label>
                                <div class="controls span6">
                                    <input type="text"  data-uk-datepicker="{format:'YYYY-MM-DD'}" class="form-control" name="tanggal_uji_laboratorium">  
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label span3">Tanggal kirim hasil</label>
                                <div class="controls span6">
                                    <input type="text" data-uk-datepicker="{format:'YYYY-MM-DD'}" class="form-control span8"  name="tanggal_kirim_hasil">  
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="controls">
                                    <input name="id_lab" type="hidden" class="form-control span8"/>
                                    <input name="nik" type="hidden" class="form-control span8"/>
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                    <a href="{{URL::to('laboratorium')}}" class="btn btn-warning">Kembali</a>
                                </div>
                            </div>
                            {{Form::close()}}
                            </div>
                            </div>
                            </div>
                        </div>
                        <!--/.stream-list-->
                    </div>
                </div>
            </div>
            <!--/.module-body-->
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
</div>
<!--/.span9-->
@stop
