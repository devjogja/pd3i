@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <h4>
                        Detail informasi pasien kasus campak
                    </h4>
                    <hr>
                </div>
<h4 style="text-align:center;">data kasus individual penyakit campak</h4>
<div class="tabbable-panel">
	<div class="tabbable-line">
		<ul class="nav nav-tabs ">
			<li class="active">
				<a href="#tab_default_1" data-toggle="tab">
				Input data individual kasus </a>
			</li>
			<li>
				<a href="#tab_default_2" data-toggle="tab">
				Input hasil laboratorium </a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_default_1">
				@foreach($campak as $hasil)
				{{ Form::open(array('route' => array('campak.update', $hasil->no_epid), 'method' => 'PUT', 'class'=>'form-horizontal','role'=>'form')) }}
				<input id="id_pasien" name="id_pasien" type="hidden" value="{{$hasil->id_pasien}}">
				<input id="id_campak" name="id_campak" type="hidden" value="{{$hasil->id_campak}}">
				<div class="module-body">
				<div class="row-fluid">
				    <div class="span6">
				        <div class="media">
				            <fieldset>
				            <legend>Identitas penderita</legend>
				            <div class="control-group">
				                <label class="control-label">No Epidemologi</label>
				                <div class="controls">
				                 {{Form::text('no_epid',$hasil->no_epid, array('class' => 'input-medium','placeholder'=>'Nomer epidemologi','id'=>'epid'))}}
				                 {{Form::hidden('id_hasil_uji_lab_campak',$hasil->id_hasil_uji_lab_campak)}}
				                <span class="help-inline"></span>
				                </div>
				              </div>
				              <div class="control-group">
				                <label class="control-label">NIK</label>
				                <div class="controls">
				                  {{Form::text('nik',$hasil->nik, array('class' => 'input-medium','placeholder'=>'Nomer induk kependudukan'))}}
				                  <span class="help-inline"></span>
				                </div>
				              </div>
				              <div class="control-group">
				                <label class="control-label">Nama penderita</label>
				                <div class="controls">
				                  {{Form::text('nama_anak',$hasil->nama_anak, array('class' => 'input-xlarge','placeholder'=>'Nama anak'))}}
				                  <span class="help-inline"></span>
				                </div>
				              </div>
				              <div class="control-group">
				                <label class="control-label">Nama orang tua</label>
				                <div class="controls">
				                  {{Form::text('nama_ortu',$hasil->nama_ortu, array('class' => 'input-xlarge','placeholder'=>'Nama orang tua'))}}
				                  <span class="help-inline"></span>
				                </div>
				              </div>
				              <div class="control-group">
				                <label class="control-label">Jenis kelamin</label>
				                <div class="controls">
				                  {{Form::select('jenis_kelamin', ['' => 'Pilih','0'=>'Laki-laki','1'=>'Perempuan','2' => 'Tidak diketahui','3'=>'Lainnya'],[$hasil->jenis_kelamin], array('class' => 'input-medium id_combobox','id'=>'jenis_kelamin'))}}
				                  <span class="help-inline"></span>
				                </div>
				              </div>
				              <div class="control-group">
				                <label class="control-label">Tanggal lahir</label>
				                <div class="controls">
				                  {{Form::text('tanggal_lahir',$hasil->tanggal_lahir, array('class' => 'input-medium tgl_lahir','placeholder'=>'Tanggal lahir','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}','onchange'=>'umur()'))}}
				                  <span class="help-inline"></span>
				                </div>
				              </div>
				              <div class="control-group">
				                <div class="controls">
				                <input type="text" class="input-mini"  value="{{$hasil->umur}}" name="umur_tahun" id="tgl_tahun">
				                Thn 
				                <input type="text" class="input-mini" value="{{$hasil->umur_bln}}" name="umur_bln" id="tgl_bulan">
				                Bln 
				                <input type="text" class="input-mini" value="{{$hasil->umur_hr}}" name="umur_hr" id="tgl_hari">
				                Hr
				                </div>
				              </div>
				              <div class="control-group">
				                <label class="control-label">Nama puskesmas</label>
				                <div class="controls">
				                  {{Form::text('id_puskesmas',null, array('class' => 'input-large','placeholder'=>'Masukan nama puskesmas'))}}
				                  <span class="help-inline"></span>
				                </div>
				              </div>
				              <div class="control-group">
				                <label class="control-label">Provinsi</label>
				                <div class="controls">
				                <select name="" class="input-medium id_combobox" id="id_provinsi" onchange="showKabupaten()">
				                <?php $pro=Provinsi::lists('provinsi','id_provinsi'); 
				                 foreach ($pro as $id_provinsi => $provinsi) {
				                    if($provinsi==$hasil->provinsi) {
				                      ?>
				                      <option value="{{$id_provinsi}}" selected="selected">{{$provinsi}}</option>
				                      <?php
				                    } else {
				                    ?>
				                      <option value="{{$id_provinsi}}">{{$provinsi}}</option>
				                    <?php
				                    }
				                  } 
				                ?>
				                
				                  </select>
				              <span class="help-inline"></span>
				                </div>
				              </div>
				              <div class="control-group">
				                <label class="control-label">Kabupaten/Kota</label>
				                <div class="controls">
				                  <select name="" class="input-medium id_combobox" id="id_kabupaten" onchange="showKecamatan()">
				                    <option value="">{{$hasil->kabupaten}}</option>
				                  </select>
				                  <span class="help-inline"></span>
				                </div>
				              </div>
				              <div class="control-group">
				                <label class="control-label">Kecamatan</label>
				                <div class="controls">
				                  <select name="" class="input-medium id_combobox" id="id_kecamatan" onchange="showKelurahan()">
				                    <option value="">{{$hasil->kecamatan}}</option>
				                  </select>
				                  <span class="help-inline"></span>
				                </div>
				              </div>
				              <div class="control-group">
				                <label class="control-label">Kelurahan/Desa</label>
				                <div class="controls">
				                  <select name="id_kelurahan" class="input-medium id_combobox" id="id_kelurahan" onchange="showEpid()">
				                    <option value="{{$hasil->id_kelurahan}}">{{$hasil->kelurahan}}</option>
				                  </select>
				                  <span class="help-inline"></span>
				                </div>
				              </div>
				              <div class="control-group">
				                <label class="control-label">Alamat</label>
				                <div class="controls">
				                  {{Form::textarea('alamat',$hasil->alamat,array('rows'=>'7'))}}
				                  <span class="help-inline"></span>
				                </div>
				              </div>
				              </fieldset>
				            </div>
				        </div>
				        <div class="span6">
				            <div class="media">
				              <fieldset>
				              <legend>Data surveilans campak</legend>
				              <div class="control-group">
				                <label class="control-label">Imunisasi campak sebelum sakit berapa kali?</label>
				                <div class="controls">
				                {{Form::select('vaksin_campak_sebelum_sakit', ['' => 'Pilih','0' => '1X','1'=>'2X','2'=>'3X','3'=>'4X','4'=>'5X','5'=>'6X','6'=>'Tidak','7'=>'Tidak tahu'],[$hasil->vaksin_campak_sebelum_sakit])}}
				                <span class="help-inline"></span>
				                </div>
				              </div>
				              <div class="control-group">
				                <label class="control-label">Tanggal timbul sakit/demam</label>
				                <div class="controls">
				                  {{Form::text('tanggal_timbul_demam',$hasil->tanggal_timbul_demam,array('class'=>'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
				                  <span class="help-inline"></span>
				                </div>
				              </div>
				              <div class="control-group">
				                <label class="control-label">Tanggal timbul rash</label>
				                <div class="controls">
				                  {{Form::text('tanggal_timbul_rash',$hasil->tanggal_timbul_rash,array('class'=>'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
				                  <span class="help-inline"></span>
				                </div>
				              </div>
				              <div class="control-group">
				                <label class="control-label">Tanggal laporan diterima</label>
				                <div class="controls">
				                  {{Form::text('tanggal_laporan_diterima',$hasil->tanggal_laporan_diterima,array('class'=>'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
				                  <span class="help-inline"></span>
				                </div>
				              </div>
				              <div class="control-group">
				                <label class="control-label">Tanggal pelacakan</label>
				                <div class="controls">
				                  {{Form::text('tanggal_pelacakan',$hasil->tanggal_pelacakan,array('class'=>'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
				                  <span class="help-inline"></span>
				                </div>
				              </div>
				              <div class="control-group">
				                <label class="control-label">Diberi vitamin A</label>
				                <div class="controls">
				                {{Form::select('vitamin_A',['' => 'Pilih','0' => 'Ya','1'=>'Tidak'], [$hasil->vitamin_A], array('class' => 'input-small id_combobox'))}}
				                <span class="help-inline"></span>
				                </div>
				              </div>
				              <div class="control-group">
				                <label class="control-label">Keadaan akhir</label>
				                <div class="controls">
				                {{Form::select('keadaan_akhir', ['' => 'Pilih','0' => 'Hidup/Sehat','1'=>'Meninggal'], [$hasil->keadaan_akhir], array('class' => 'input-medium id_combobox'))}}
				                <span class="help-inline"></span>
				                </div>
				              </div>
				              </fieldset>
				            </div>
				        </div>
				    </div>
				    <div class="row-fluid">
				        <div class="span12">
				            <div class="media">
				              <fieldset>
				              <legend>Data spesimen dan laboratorium</legend>
				              <!-- daftar sampel -->
		                        <table class="table table-bordered" id="data_sampel">
		                            <tr>
		                                <td>Nama pemeriksaan</td>
		                                <td>Jenis sampel</td>
		                                <td>Tanggal ambil sampel</td>
		                            </tr>
		                            <?php $data=DB::table('uji_spesimen')->where('id_campak','=',$hasil->id_campak)->get(); ?>
		                            @foreach($data as $row)
		                            <?php
		                                if ($row->jenis_pemeriksaan==0) {
		                                    $jenis_pemeriksaan_baru='Serologi';
		                                    $jenis_sampel_baru= ($row->jenis_sampel==0)?'Darah':'Urin';
		                                }
		                                if ($row->jenis_pemeriksaan==1) {
		                                    $jenis_pemeriksaan_baru='Verologi';
		                                    if ($row->jenis_sampel==0) {
		                                        $jenis_sampel_baru='Urin';
		                                    } elseif($row->jenis_sampel==1) {
		                                        $jenis_sampel_baru ='Usap tenggorokan';
		                                    }else
		                                    {
		                                        $jenis_sampel_baru='Cairan mulut';
		                                    }
		                                    
		                                }
		                            ?>
		                            <tr>
		                                <td><input  name="nama_sampel[]" type="hidden" value="{{$row->jenis_pemeriksaan}}">{{$jenis_pemeriksaan_baru}}</td>
		                                <td><input  name="jenis_sampel[]" type="hidden" value="{{$row->jenis_sampel}}">{{$jenis_sampel_baru}}</td>
		                                <td><input  name="tanggal_ambil_sampel[]" type="hidden" value="{{date('d-m-Y',strtotime($row->tgl_ambil_sampel))}}">{{date('d-m-Y',strtotime($row->tgl_ambil_sampel))}}</td>
		                            </tr>
		                            @endforeach
		                        </table> 
		                        <!-- akhir daftar sampel-->
				              </fieldset>
				            </div>
				        </div>
				    </div>
				    </div>
				      <div class="form-actions">
				      <!-- {{Form::submit('simpan',array('class'=>'btn btn-primary'))}}
				      {{Form::reset('batal',array('class'=>'btn btn-warning'))}} -->
				      	
				      </div>
				  {{Form::close()}}
				  @endforeach()
			</div>
			<div class="tab-pane" id="tab_default_2">
                    <div class="module-body">
	                <table class="table table-bordered" style="margin-bottom:10px;">
	                <tr>
						<td style="text-align:center;">No epid</td>
						<td style="text-align:center;">Nama</td>
						<td style="text-align:center;">Alamat</td>
						<td style="text-align:center;">Asal Puskesmas</td>
					</tr>
	                @foreach($campak as $hsl)
						<tr>
							<td style="text-align:center;">{{ $hsl->no_epid }}</td>
							<td style="text-align:center;">{{ $hsl->nama_anak }}</td>
							<td style="text-align:center;">{{ $hsl->alamat }}</td>
							<td style="text-align:center;">{{ Puskesmas::getNamePuskesmas($hsl->id_tempat_periksa) }}</td>
						</tr>
					@endforeach
					</table>
					<div class="profile-tab-content tab-content">
					<div class="tab-pane fade active in" id="input_lab">
					{{Form::open(array('route'=>'simpan_hasil_lab_campak','class'=>'form-horizontal','id'=>'simpan_hasil_lab'))}}
					    <div class="row-fluid">
                            <div class="span6">
                                <div class="media">
                                    <fieldset>
                                        <legend>Informasi spesimen</legend>
                                        <div class="control-group">
                                            <label class="control-label">No. Spesimen</label>
                                            <div class="controls">
                                             {{Form::text('no_spesimen',Input::old('no_spesimen'), array('class' => 'input-medium'))}}
                                            <span class="help-inline"></span>
                                            </div>
                                          </div>
                                          <div class="control-group">
                                            <label class="control-label">Tipe spesimen</label>
                                            <div class="controls">
                                              {{Form::text('tipe_spesimen',Input::old('tipe_spesimen'), array('class' => 'input-medium'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                          <div class="control-group">
                                            <label class="control-label">Sampel lainnya</label>
                                            <div class="controls">
                                              {{Form::text('sampel_lainnya',Input::old('sampel_lainnya'), array('class' => 'input-medium'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                          <div class="control-group">
                                            <label class="control-label">Tanggal pengiriman spesimen ke propinsi</label>
                                            <div class="controls">
                                              {{Form::text('tanggal_pengiriman_spesimen_propinsi',Input::old('tanggal_pengiriman_spesimen_propinsi'), array('class' => 'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                           <div class="control-group">
                                            <label class="control-label">Tanggal pengiriman spesimen ke laboratorium</label>
                                            <div class="controls">
                                              {{Form::text('tanggal_pengiriman_spesimen_laboratorium',Input::old('tanggal_pengiriman_spesimen_laboratorium'), array('class' => 'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                           <div class="control-group">
                                            <label class="control-label">Tanggal terima spesimen di laboratorium</label>
                                            <div class="controls">
                                              {{Form::text('tanggal_terima_spesimen_laboratorium',Input::old('tanggal_terima_spesimen_laboratorium'), array('class' => 'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                           <div class="control-group">
                                            <label class="control-label">Permintaan tes serologi</label>
                                            <div class="controls">
                                              {{Form::text('permintaan_tes_serologi',Input::old('permintaan_tes_ser'), array('class' => 'input-medium'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                          <div class="control-group">
                                            <label class="control-label">Kondisi spesimen waktu di terima di propinsi (diganti laboratorium)</label>
                                            <div class="controls">
                                              {{Form::select('kondisi_spesimen_dipropinsi', array('' => 'Pilih','0'=>'Baik','1'=>'Volum kurang','2' => 'Tidak dingin','3'=>'Kering(lisis)','4'=>'Tube bocor','5'=>'Tidak baik'),Input::old('kondisi_spesimen_dipropinsi'), array('class' => 'input-medium'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                          <div class="control-group">
                                            <label class="control-label">Kondisi spesimen waktu di terima di laboratorium</label>
                                            <div class="controls">
                                              {{Form::select('kondisi_spesimen_dilab', array('' => 'Pilih','0'=>'Adekuat(baik)','1'=>'Non adekuat(tidak baik)'),Input::old('kondisi_spesimen_dilab'), array('class' => 'input-medium'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="media">
                                <fieldset>
                                  <legend>Proses pemeriksaan sampel dan hasil lab</legend>
                                  <div class="control-group">
                                    <label class="control-label">Tanggal pemeriksaan sampel</label>
                                    <div class="controls">
                                      {{Form::text('tanggal_pemeriksaan_sampel',Input::old('tanggal_pemeriksaan_sampel'),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                      <span class="help-inline"></span>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label">Kit ELISA yang digunakan</label>
                                    <div class="controls">
                                      {{Form::text('kit_elisa',Input::old('kit_elisa'),array('class'=>'input-medium'))}}
                                      <span class="help-inline"></span>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label">Lainnya</label>
                                    <div class="controls">
                                      {{Form::text('lainnya',Input::old('lainnya'),array('class'=>'input-medium'))}}
                                      <span class="help-inline"></span>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label">Measles Antigen OD</label>
                                    <div class="controls">
                                      {{Form::text('campak_antigen_OD',Input::old('campak_antigen_OD'),array('class'=>'input-medium'))}}
                                      <span class="help-inline"></span>
                                    </div>
                                    <label class="control-label">Rubella Antigen OD</label>
                                    <div class="controls">
                                      {{Form::text('rubella_antigen_OD',Input::old('rubella_antigen_OD'),array('class'=>'input-medium'))}}
                                      <span class="help-inline"></span>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label">Control Antigen OD</label>
                                    <div class="controls">
                                      {{Form::text('control_antigen_OD',Input::old('control_antigen_OD'),array('class'=>'input-medium'))}}
                                      <span class="help-inline"></span>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label">Tanggal hasil tersedia</label>
                                    <div class="controls">
                                      {{Form::text('tanggal_hasil_tersedia',Input::old('tanggal_hasil_tersedia'),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                      <span class="help-inline"></span>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label">Tanggal hasil dilaporkan</label>
                                    <div class="controls">
                                      {{Form::text('tanggal_hasil_dilaporkan',Input::old('tanggal_hasil_dilaporkan'),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                      <span class="help-inline"></span>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label">Tanggal kirim hasil</label>
                                    <div class="controls">
                                      {{Form::text('tanggal_kirim_hasil',Input::old('tanggal_kirim_hasil'),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                      <span class="help-inline"></span>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label">Hasil pemeriksaan akhir</label>
                                    <div class="controls">
                                    {{Form::select('hasil_pemeriksaan_akhir', array('' => 'Pilih','0' => 'IgM Campak positif','1'=>'IgM Rubella positif','3'=>'Negatif IgM Campak dan Rubella'), Input::old('hasil_pemeriksaan_akhir'), array('class' => 'input-large'))}}
                                    <span class="help-inline"></span>
                                    </div>
                                  </div>
                                  </fieldset>
                                </div>
                            </div>
                        </div>

                        <div class="row-fluid">
                            <div class="span12">
                                <div class="media">
                                    <fieldset>
                                      <legend>Informasi lab rujukan</legend>
                                      <div class="control-group">
                                        <label class="control-label">Regional reference lab</label>
                                        <div class="controls">
                                          {{Form::text('regional_reference_lab',Input::old('regional_reference_lab'),array('class'=>'input-medium'))}}
                                          <span class="help-inline"></span>
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Tanggal pengiriman ke RRL</label>
                                        <div class="controls">
                                          {{Form::text('tanggal_RRL',Input::old('tanggal_RRL'),array('class'=>'input-small','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                          <span class="help-inline"></span>
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Hasil dari RRL</label>
                                        <div class="controls">
                                          {{Form::text('hasil_RRL',Input::old('hasil_RRL'),array('class'=>'input-medium'))}}
                                          <span class="help-inline"></span>
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Tanggal penerimaan hasil dari RRL</label>
                                        <div class="controls">
                                        {{Form::text('tanggal_terima_hasil_RRL',Input::old('tanggal_terima_hasil_RRL'), array('class' => 'input-small','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                        <span class="help-inline"></span>
                                        </div>
                                      </div>
                                      </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                              {{Form::submit('simpan',array('class'=>'btn btn-primary'))}}
                              {{Form::reset('reset',array('class'=>'btn btn-warning'))}}
                              <a href="{{URL::to('input_hasil_lab/'.$campak[0]->no_epid)}}" class="btn btn-danger">kembali</a>
                              <span id="msg"></span>
                              </div>
                          {{Form::close()}}
					</div>
				</div> <!-- /.tab-content-->
				<br/>
                    <table class="table table-bordered" id="data_hasil_uji_spesimen">
                        <tr>
                            <th>No.</th>
                            <th>Jenis pemeriksaan</th>
                            <th>Jenis sampel</th>
                            <th>IGM Campak</th>
                            <th>IGM Rubella</th>
                            <th>Nama penyakit</th>
                            <th>aksi</th>
                        </tr>
                    </table>
                    </br>
                    <span id="tombol">
                      
                    </span>

                    <!-- <span id="pesan">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>Well done!</strong> Now you are listening me :) 
                    </div>
                    </span> -->
                    <div class="row-fluid" id="inputhasillab">
                            <div class="span12">
                                <div class="media">
                                </br>
                                <table class="table table-bordered">
                                    <tr>
                                        <td>Jenis pemeriksaan</td>
                                        <td>
                                        {{
                                        Form::select
                                        (
                                            'jenis_pemeriksaan', 
                                            array
                                            (
                                              '3' => 'Pilih',
                                              '0' => 'Serologi',
                                              '1'=>'Verologi'
                                            ), 
                                            Input::old('jenis_pemeriksaan'), 
                                            array
                                            (
                                                'class' => 'id_combobox',
                                                'id'=>'jenis_pemeriksaan',
                                                'onchange'=>'pilihJenisPemeriksaan()'
                                            )
                                        )
                                        }}
                                        </td>
                                    </tr>
                                    <tbody id="tbody">
                                    <tr>
                                        <td>Jenis sampel</td>
                                        <td>
                                        <select name="jenis_sampel" id="jenis_sampel">
                                          <option value=''>Pilih</option>
                                        </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Hasil IGM campak</td>
                                        <td>
                                        <select name="hasil_igm_campak" id="hasil_igm_campak">
                                          <option value=''>Pilih</option>
                                        </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Hasil IGM Rubella</td> <td>
                                        <select name="hasil_igm_rubella" id="hasil_igm_rubella">
                                          <option value=''>Pilih</option>
                                        </select>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	var id_hasil_igm_campak = $('#id_hasil_igm_campak0').text();
	$('#simpan_hasil_lab').submit(function() {
		loading_msg();
		$('#simpan_hasil_lab').ajaxSubmit({
			type: 'POST',
			success:function(data) {
				$('#msg').html('');
				$('#msg').html(data.msg);
				if(data.msg == 'tersimpan')location.href  = '{{URL::to("lab/campak")}}';
			}
		});
		return false;
	});
});

function loading_state(){
	$('#status').html('Loading...');
}

function loading_msg(){
	$('#msg').html('Loading...');
}	

</script>
<script>
$(document).ready(function(){
	
	$('select').select2();
  	$('#input_baru').submit(function() {
    loading_state();
    $('#input_baru').ajaxSubmit({
      type: 'POST',
      success:function(data) {
        $('#status').html('');
        $('#status').html(data.msg);
        $('#no_epid').html(data.no_epid);
        $('#nama_pasien').html(data.nama_pasien);
        $('#usia').html(data.usia);
        $('#klasifikasi_akhir').html(data.klasifikasi_final);
        $('#keadaan_akhir').html(data.keadaan_akhir);
        $('#id_pasien').val(data.id_pasien);
        $('#id_campak').val(data.id_campak);
        //if(jsonData.status == 'success')location.href  = jsonData.address;
      }
    });
    return false;
  });
});

function hasilIgmCampak(){
  alert('oke');
} 

</script>

<script>
$(document).ready(function(){
  $('#simpan_hasil_lab').submit(function() {
    loading_msg();
    $('#simpan_hasil_lab').ajaxSubmit({
      type: 'POST',
      success:function(data) {
        $('#msg').html('');
        $('#msg').html(data.msg);
        if(data.msg == 'tersimpan')location.href  = '{{URL::to("lab/campak")}}';
      }
    });
    return false;
  });
});

function loading_state(){
  $('#status').html('Loading...');
}

function loading_msg(){
  $('#msg').html('Loading...');
} 
</script>

<script type="text/javascript">
    $(document).ready(function() {
      $('#inputhasillab').hide();
      $('#data_hasil_uji_spesimen').load('{{URL::to("get_hasil_uji_spesimen/'+$('#id_campak').val()+'")}}');
      $('#tombol').html('<tombol class="btn btn-medium btn-success" onclick="tampilInputhasillab()" >Input hasil laboratorium</tombol>');
      $('#form_save_campak').validate({
              submit: {
                settings: {
                  scrollToError: {
                      offset: -100,
                      duration: 500
                  }
                }
              }
            });

      $( "#id_kelurahan" ).change(function() {
         $.ajax({

              data:'id_kelurahan='+$('#id_kelurahan').val()+'&date='+$('.tgl_mulai_rash').val(),
              url:'{{URL::to("ambil_epid_campak")}}',
              success:function(data) {
              $('#epid').val(data);
         }
        });
      });

      
      
    });

    function pilihJenisPemeriksaan(){

        $.ajax({

              data:'jenis_pemeriksaan='+$('#jenis_pemeriksaan').val(),
              url:'{{URL::to("pilih_jenis_sampel")}}',
              success:function(data) {
               $('#tbody').html('');
              $('#tbody').html(data);
         }
        });

    }

    function inputDetailPemeriksaan()
    {
        $.ajax({
        //dataType:'json',
        //data: 'sub_district_id='+$('#sub_district_id').val(),
        url:'{{URL::to("input_detail_pemeriksaan")}}',
        success:function(data) {

            
        }
    });
    }
    function tampilInputhasillab()
    {
     
      $('#inputhasillab').show(2000);
      $('#tombol').html('');
      $('#tombol').html('<tombol class="btn btn-medium btn-success" onclick="hideInputhasillab()">batalkan</tombol>');
    }

    function hideInputhasillab()
    {
      $('#inputhasillab').hide(2000);
      $('#tombol').html('');
      $('#tombol').html('<tombol class="btn btn-medium btn-success" onclick="tampilInputhasillab()">input hasil laboratorium</tombol>');
    }

    function simpanhasillab()
    {
        var jenis_pemeriksaan     = $("#jenis_pemeriksaan").val();
        var jenis_sampel          = $("#jenis_sampel").val();
        var hasil_igm_campak      = $("#hasil_igm_campak").val();
        var hasil_igm_rubella     = $("#hasil_igm_rubella").val();
        var nama_penyakit         = $('#nama_penyakit').val();
        var id 	 = $('#id_campak').val();
        
        // //kirim data ke server
      
        $.post('{{URL::to("hasil_lab")}}', {id:id,jenis_pemeriksaan:jenis_pemeriksaan,jenis_sampel:jenis_sampel,hasil_igm_campak:hasil_igm_campak,hasil_igm_rubella:hasil_igm_rubella,nama_penyakit:nama_penyakit}, function(response)
        {
            alert(response.feedback);
            $('#data_hasil_uji_spesimen').load('{{URL::to("get_hasil_uji_spesimen/'+$('#id_campak').val()+'")}}');
            $('#inputhasillab').hide(2000);
            $('#tombol').html('');
            $('#tombol').html('<tombol class="btn btn-medium btn-success" onclick="tampilInputhasillab()">input hasil laboratorium</tombol>');
        });


    }

    function cek(){
        var nama_penyakit = $('#hasil_igm_campak').val();
        if(nama_penyakit==0 && nama_penyakit!='')
        {
           $('#nama_penyakit').removeAttr('disabled','');
        }
        else
        {
           $('#nama_penyakit').attr('disabled','disabled');
        }
    }

    function hapus(id)
    {
    	$.ajax({
            data:'id='+id,
            url:'{{ URL::to("hapusUjispesimen") }}',
            success:function(data) {
            alert(data.feedback);
        	$('#data_hasil_uji_spesimen').load('{{URL::to("get_hasil_uji_spesimen/'+$('#id_campak').val()+'")}}');
            }
        });
    }

    $(document).ready(function(){
            $('#form_save_campak input').keydown(function(e){
             if(e.keyCode==13){       

                if($(':input:eq(' + ($(':input').index(this) + 1) + ')').attr('type')=='submit'){// check for submit button and submit form on enter press
                 return true;
                }

                $(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();

               return false;
             }

            });
        });
    </script>
    <script type="text/javascript">
      $(document).ready(function() {
            $("select").select2();
            $('#example').dataTable();
            $('#div_sampel').hide();
            //$('#jenis_sampel').attr('disabled','disabled');
            $('#simpan').attr('disabled','disabled');
            $('#tanggal_ambil_sampel').attr('disabled','disabled');

            $('#form_save_campak').validate({
                submit: {
            settings: {
                scrollToError: {
                 offset: -100,
                 duration: 500
                }
            }
          }
        });

            $( "#id_kelurahan" ).change(function() {
                $.ajax({
                    data:'id_kelurahan='+$('#id_kelurahan').val()+'&date='+$('.tgl_mulai_rash').val(),
                    url:'{{ URL::to("ambil_epid_campak") }}',
                    success:function(data) {
                $('#epid').val(data);
                    }
                });
            });

            /*
            $( '#form-filter-daftar-kasus' ).submit(function( event ){
                event.preventDefault();
                alert('gagal kirim');
                var form_data = $(this).serialize();
            });
            */

            $('#form_save_campak input').keydown(function(e){

                // check for submit button and submit form on enter press
                if(e.keyCode==13){
                    if($(':input:eq(' + ($(':input').index(this) + 1) + ')').attr('type')=='submit'){
                        return true;
                    }

                    $(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();

                    return false;
                }
            });
      });

      function tambah_sampel(){
        $('#div_sampel').show(2000);
            $('#tambah_sampel').hide();
        }

        function tutup_sampel(){
            $('#div_sampel').hide(2000);
            $('#tambah_sampel').show(2000);
            $('#jenis_sampel').val('');
            $('#tanggal_ambil_sampel').val('');
            $('#jenis_sampel').attr('disabled','disabled');
            $('#simpan').attr('disabled','disabled');
            $('#tanggal_ambil_sampel').attr('disabled','disabled');

        }

        function add_sampel()
        {
            var nama_sampel             = $("#nama_sampel").val();
            var jenis_sampel            = $("#jenis_sampel").val();
            var tanggal_ambil_sampel    = $("#tanggal_ambil_sampel").val();
            if(nama_sampel=='0')
            {
              var nama_sampel_baru = 'Serologi';
              var jenis_sampel_baru=(jenis_sampel==0)?'Darah':'Urin';
            }

            if(nama_sampel=='1')
            {
                var nama_sampel_baru = 'verologi';
                if (jenis_sampel==0) {
                   var jenis_sampel_baru='Urin';
                } 
                else if(jenis_sampel==1)
                {
                   var jenis_sampel_baru='Usap tenggorokan';
                }
                else
                {
                    var jenis_sampel_baru ='Cairan mulut';
                }
            }

            $("#data_sampel").append('<tr valign="top"><td><input id="data_nama_sampel" name="nama_sampel[]" type="hidden" value='+nama_sampel+'>'+nama_sampel_baru+'</td><td><input id="data_jenis_sampel" name="jenis_sampel[]" type="hidden" value='+jenis_sampel+'>'+jenis_sampel_baru+'</td><td><input id="data_tanggal_ambil_sampel" name="tanggal_ambil_sampel[]" type="hidden" value='+tanggal_ambil_sampel+'>'+tanggal_ambil_sampel+'</td><td><a href="javascript:void(0);" class="remCF">Remove</a></td></tr>');
        

        }

        function pilih_sampel()
        {
            if($('#nama_sampel').val()=='0')
            {
                $('#jenis_sampel').html('');
                $('#jenis_sampel').append('<option value="">Pilih</option><option value="0">Darah</option><option value="1">Urin</option>');
                $('#jenis_sampel').removeAttr('disabled','');
            }
            else if($('#nama_sampel').val()=='1')
            {
                $('#jenis_sampel').html('');
                $('#jenis_sampel').removeAttr('disabled','');
                $('#jenis_sampel').append('<option value="">Pilih</option><option value="0">Urin</option><option value="1">Usap tenggorokan</option><option value="2">Cairan mulut</option>');
            }
            else if($('#nama_sampel').val()=='')
            {
                $('#jenis_sampel').attr('disabled','disabled');
                $('#jenis_sampel').html('');
            }
        }

        function jenissampel()
        {
            if ($('#jenis_sampel').val()=='') {
                $('#tanggal_ambil_sampel').attr('disabled','disabled');
                $('#simpan').attr('disabled','disabled');
            }
            else
            {
                 $('#tanggal_ambil_sampel').removeAttr('disabled','');
            }

        }
        $( "#id_kelurahan" ).change(function() {
                $.ajax({
                    data: 'id_kelurahan='+$('#id_kelurahan').val()+'&date='+$('.tgl_mulai_sakit').val(),
                    url: '{{ URL::to("ambil_epid_tetanus") }}',
                    success:function(data) {
                        $('#epid').val(data);
                    }
                });
            });

        $(document).ready(function(){
        $("#tanggal_ambil_sampel").change(function(){
            $('#simpan').removeAttr('disabled','');    
        });
        $("#data_sampel").on('click','.remCF',function(){
            $(this).parent().parent().remove();
        });
    });

        function simpan_sampel(){
            var jenis_spesimen_serologi         = $("#jenis_spesimen_serologi").val();
            var tanggal_ambil_sampel_serologi   = $("#tanggal_ambil_sampel_serologi").val();
            var jenis_spesimen_virologi         = $("#jenis_spesimen_virologi").val();
            var tanggal_ambil_sampel_virologi   = $("#tanggal_ambil_sampel_virologi").val();
            // //kirim data ke server
          
            $.post
                (
                    '{{URL::to("simpan_sampel")}}', 
                    {
                        jenis_spesimen_serologi:jenis_spesimen_serologi,
                        tanggal_ambil_sampel_serologi:tanggal_ambil_sampel_serologi,
                        jenis_spesimen_virologi:jenis_spesimen_virologi,
                        tanggal_ambil_sampel_virologi:tanggal_ambil_sampel_virologi
                    }, 
                    function(response)
                    {
                        alert(response.feedback);
                        $('#data_hasil_uji_spesimen').load('{{URL::to("get_hasil_uji_spesimen/'+$('#id_campak').val()+'")}}');
                        $('#inputhasillab').hide(2000);
                        $('#tombol').html('');
                        $('#tombol').html('<tombol class="btn btn-medium btn-success" onclick="tampilInputhasillab()">input hasil laboratorium</tombol>');
                    }
                );
        }
    </script>
</div>
</div>
</div>
@stop
