@extends('layouts.master')
@section('content')
<!-- This is the modal -->
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Kasus penyakit difteri</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
            <a href="{{URL::to('lab/difteri')}}" class="btn btn-sm btn-warning">kembali</a>
            </div>
        </div>
        <!-- /.modal-content -->
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <h4>Penyakit difteri </h4> 
                    <hr>
                </div>
                <ul class="profile-tab nav nav-tabs">
                    <li  class="active"><a href="#cari_pasien" data-toggle="tab">Cari pasien</a></li>
                    <!-- <li><a href="#friends" data-toggle="tab">Input data individual kasus</a></li>
                    <li><a href="#input-hasil" data-toggle="tab">Input hasil laboratorium</a></li> -->
                    <li><a href="#activity" data-toggle="tab">Daftar Hasil laboratorium pasien difteri</a></li>
                    <li><a href="#analisis" data-toggle="tab">Analisis kasus</a></li>
                </ul>
                <div class="profile-tab-content tab-content">
                <div class="tab-pane fade active in" id="cari_pasien">
                    <div class="module-body">
                        <div class="row-fluid">
                            <table class="table form-horizontal" style="width:100%;">
                            <!-- <caption>Silahkan mencari pasien terlebih dahulu untuk mengisi hasil data laboratorium</caption> -->
                                <tr>
                                    <td style="text-align:right;width:10%;">
                                        No. Epid
                                    </td>
                                    <td style="width:30%;">
                                        <input type="text" id="cari_no_epid" name="no_epid" required="required" placeholder="masukan no. epidemologi">
                                        <button id="cari" class="btn btn-success" onclick="cari_pasien_dilab()" >cari pasien</button>
                                    </td>
                                    <td style="text-align:left;">
                                        <!-- <button id="tampil" class="btn">advanced search</button> -->
                                    </td>
                                    <td></td>
                                </tr><!-- 
                                <div id="advan_seacrh" style="display:none">
                                <tr>
                                    <td style="text-align:right;">
                                        Nama pasien
                                    </td>
                                    <td>
                                    <input type="text" id="cari_nama_penderita" name="nama_penderita" placeholder="masukan nama pasien">
                                    </td>
                                    <td style="text-align:right;">
                                       
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:right;">
                                        Jenis kelamin
                                    </td>
                                    <td>
                                        <select type="text" id="cari_jenis_kelamin" name="jenis_kelamin" class="input-medium" >
                                            <option value="">Pilih</option>
                                            <option value="0">Laki-laki</option>
                                            <option value="1">Perempuan</option>
                                        </select>                                   
                                    </td>
                                    <td style="text-align:right;">
                                       
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                </div> -->
                                <!-- <tr>
                                    <td style="text-align:right;">
                                        
                                    </td>
                                    <td>
                                        <center>
                                            <button id="cari" class="btn btn-success">cari pasien</button>
                                        </center>
                                    </td>
                                    <td style="text-align:right;">
                                   
                                    </td>
                                    <td>
                                    </td>
                                </tr> -->
                                <tr>
                                    <td style="text-align:right;">
                                        
                                    </td>
                                    <td>
                                        
                                    </td>
                                    <td style="text-align:right;">
                                        
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="module message">
                    <div class="module-body table">                             
                        <table class="table table-message">
                            <tbody id="hasil_cari">
                                
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
                    <div class="tab-pane fade in" id="activity">
                        <div class="module-option clearfix">
                            <form>
                            <div class="input-append pull-left">
                            <select name="bln" id="e5" style="width:150px" class="bulan">
                            <?php
                                $data=DB::select('select distinct(month(tanggal_periksa))as bln from afp');
                                foreach($data as $row){
                                    if($row->bln==1){
                                        ?>
                                        <option value="01">Januari</option>
                                        <?php
                                    }elseif($row->bln==2){
                                        ?>
                                        <option value="02">Februari</option>
                                        <?php
                                    }elseif($row->bln==3){
                                        ?>
                                        <option value="03">Maret</option>
                                        <?php
                                    }elseif($row->bln==4){
                                        ?>
                                        <option value="04">April</option>
                                        <?php
                                    }elseif($row->bln==5){
                                        ?>
                                        <option value="05">Mei</option>
                                        <?php
                                    }elseif($row->bln==6){
                                        ?>
                                        <option value="6">Juni</option>
                                        <?php
                                    }elseif($row->bln==7){
                                        ?>
                                        <option value="07">Juli</option>
                                        <?php
                                    }elseif($row->bln==8){
                                        ?>
                                        <option value="08">Agustus</option>
                                        <?php
                                    }elseif($row->bln==9){
                                        ?>
                                        <option value="09">September</option>
                                        <?php
                                    }elseif($row->bln==10){
                                        ?>
                                        <option value="10">Oktober</option>
                                        <?php
                                    }elseif($row->bln==11){
                                        ?>
                                        <option value="11">November</option>
                                        <?php
                                    }elseif($row->bln==12){
                                        ?>
                                        <option value="12">Desember</option>
                                        <?php
                                    }
                                }
                            ?>
                            </select>
                            </div>
                            <div class="input-append pull-left bb">
                            <select name="thn" id="e3" style="width:150px ;placeholder:'tahun'" calss="tahun">                               
                            <?php
                                $data=DB::select('select distinct(year(tanggal_periksa))as thn from afp');
                                foreach($data as $row){
                                    ?>
                                    <option value="{{$row->thn}}">{{$row->thn}}</option>
                                    <?php
                                }   
                            ?>
                            </select>
                            </div>
                            </form>
                            <div class="btn-group pull-left bb" data-toggle="buttons-radio">
                            <a href="{{URL::to('/cetak/campak')}}" class="btn btn-sm btn-success">cetak</a>
                            </div>
                        </div>
                        <div class="module-body uk-overflow-container" id="data">
                            <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                            <thead>
                                <tr>
                                  <th>Kota/Kab</th>
                                  <th>No. Epid</th>
                                  <th>No. Lab</th>
                                  <th>Nama pasien</th>
                                  <th>Umur</th>
                                  <th>Tanggal Pengiriman spesimen</th>
                                  <th>Tanggal terima spesimen</th>
                                  <th>Kondisi spesimen</th>
                                  <th>Hasil pemeriksaan IgM Campak</th>
                                  <th>Hasil pemeriksaan IgM Rubella</th>
                                </tr>
                              </thead>
                              <tbody>
                              <?php $data = Campak::pasienLabcampak();?>
                              @foreach($data as $row)
                                <tr>
                                    <td>{{$row->kabupaten}}</td>
                                    <td>{{$row->no_epid}}</td>
                                    <td>{{$row->id_uji_lab}}</td>
                                    <td>{{$row->nama_anak}}</td>
                                    <td>@if($row->umur){{$row->umur}} tahun @endif</td>
                                    <td>{{$row->tanggal_pengiriman_spesimen_laboratorium}}</td>
                                    <td>{{$row->tanggal_terima_spesimen_laboratorium}}</td>
                                    <td><?php echo $retVal = ($row->kondisi_spesimen_dipropinsi==0) ? 'Adekuat(baik)' : 'Nonadekuat(tidak baik)' ; ?></td>
                                    <td><?php echo $retVal = ($row->hasil_igm_campak==0) ? 'Positif' : 'Negatif' ; ?></td>
                                    <td><?php echo $retVal = ($row->hasil_igm_rubella==0) ? 'Positif' : 'Negatif' ; ?></td>
                                </tr>
                            @endforeach
                              
                              </tbody>
                            </table>

                        </div>
                    </div>                    
                    @include('pemeriksaan.campak.analisis')
                    @include('pemeriksaan.campak.analisis-js')
                </div>
            </div>
            <!--/.module-body-->
        </div>
        <!--/.module-->
    </div>
    <script>
        $(document).ready(function(){
            $("#hide").click(function(){
                $("p").hide();
            });
            $("#show").click(function(){
                $("p").show();
            });
        });
    </script>
    <!--/.content-->
</div>
<!--/.span9--><!-- 
@include('pemeriksaan.campak.analisis-js') -->
@stop
