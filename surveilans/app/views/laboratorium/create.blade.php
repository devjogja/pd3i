@extends('layouts.master')
@section('content')
<section class="scrollable padder">
    <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
        <li><a href="{{URL::to('beranda')}}"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{URL::to('laboratorium')}}">Laboratorium</a></li>
    	<li class="active">Tambah Laboratorium</li>
    </ul>
	<div class="m-b-md">
        <h3 class="m-b-none">Tambah Laboratorium</h3>
    </div>
    <section class="panel panel-default">
        <header class="panel-heading font-bold">
            Silahkan lengkapi form berikut.
        </header>
        <div class="panel-body">
            {{Form::open(array('url'=>'laboratorium/simpan','class'=>'form-horizontal','parsley-validate'=>'parsley-validate','role'=>'form','novalidate'=>'novalidate'))}}
        <div class="form-group">
            {{Form::label('code','Nama Laboratorium',array('class'=>'col-sm-2 control-label'))}}          
        <div class="col-sm-10">
            {{ Form::text('nama_laboratorium', null, array(
                    'class' => 'form-control',
                    'placeholder' => '',
                    'parsley-required' => 'true',
                    'parsley-remote' => ''
                    )) 
            }}
        </div>
        </div>
        <div class="line line-dashed line-lg pull-in"></div>
        <div class="form-group"> 
            {{Form::label('code','Nama Petugas',array('class'=>'col-sm-2 control-label'))}}  
        <div class="col-sm-10">
        {{ Form::text('nama_petugas', null, array(
                'class' => 'form-control',
                'placeholder' => '',
                'parsley-required' => 'true',
                'parsley-remote' => ''
                )) 
        }}
        </div>
    	</div>
    	<div class="line line-dashed line-lg pull-in"></div>
    	<div class="form-group">
            {{Form::label('code','Provinsi',array('class'=>'col-sm-2 control-label'))}}             
            <div class="col-sm-10">
                {{ Form::select('', array(''=>'')+Provinsi::lists('provinsi','id_provinsi'),null,array('id'=>'id_provinsi','placeholder' => 'Pilih Provinsi','onchange'=>'showKabupaten()')) }}
            </div>
    	</div>
    	<div class="line line-dashed line-lg pull-in"></div>
    	<div class="form-group">
            {{Form::label('code','Kabupaten',array('class'=>'col-sm-2 control-label'))}}            
            <div class="col-sm-10">
	            <select id="id_kabupaten" placeholder="Pilih Kabupaten" onchange="showKecamatan()">
	           		<option value="" selected="selected"></option>
	           	</select>
                <span id='spinner'></span>
            </div>
    	</div>
    	<div class="line line-dashed line-lg pull-in"></div>
    	<div class="form-group">
            {{Form::label('code','Kecamatan',array('class'=>'col-sm-2 control-label'))}}              
            <div class="col-sm-10">
                <select id="id_kecamatan" placeholder="Pilih Kecamatan" onchange="showKelurahan()">
	           		<option value="" selected="selected"></option>
	           	</select>
            </div>
    	</div>
    	<div class="line line-dashed line-lg pull-in"></div>
    	<div class="form-group">
            {{Form::label('code','Kelurahan',array('class'=>'col-sm-2 control-label'))}}  
            <div class="col-sm-10">
            	<select id="id_kelurahan" placeholder="Pilih Kelurahan" name="id_kelurahan">
	           		<option value="" selected="selected"></option>
	        	</select>
            </div>
    	</div>
    	<div class="line line-dashed line-lg pull-in"></div>
    	<div class="form-group">
            {{Form::label('code','Alamat',array('class'=>'col-sm-2 control-label'))}}              
            <div class="col-sm-10">
        	{{ Form::text('alamat', null, array(
                'class' => 'form-control',
                'placeholder' => '',
                'parsley-required' => 'true',
                'parsley-remote' => ''
                )) 
            }}
        </div>
    	</div>
    	<div class="line line-dashed line-lg pull-in"></div>
    	<div class="form-group">
            {{Form::label('code','Telepon',array('class'=>'col-sm-2 control-label'))}}             
            <div class="col-sm-10">
            {{ Form::text('telepon', null, array(
                'class' => 'form-control',
                'placeholder' => '',
                'parsley-required' => 'true',
                'parsley-remote' => ''
                )) 
            }}
        </div>
    	</div>
    	<div class="line line-dashed line-lg pull-in"></div>
    	<div class="form-group">
            {{Form::label('code','Email',array('class'=>'col-sm-2 control-label'))}}            
            <div class="col-sm-10">
            {{ Form::text('email', null, array(
                'class' => 'form-control',
                'placeholder' => '',
                'parsley-required' => 'true',
                'parsley-remote' => ''
                )) 
            }}
        </div>
    	</div>
        <div class="line line-dashed line-lg pull-in"></div>
        <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
            {{ Form::submit('Simpan', array('class'=>'btn btn-primary')) }}
            &nbsp;&nbsp;
            <a class="btn btn-default" href="{{URL::to('laboratorium')}}">Batal simpan</a>
            </div>
        </div>
            {{Form::close()}} 
    </div>
	</section>
</section>
@stop