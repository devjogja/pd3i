@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Tambah akun pengguna untuk level Laboratorium</h3>
        </div>
        <div class="module-body">
    	{{Form::open(array('url'=>'laboratorium/daftarakun','class'=>'form-horizontal row-fluid'))}}
        <div class="control-group">
            <label class="control-label span3" for="basicinput">Nama laboratorium</label>             
            <div class="controls span6">
                {{ Form::select('id_lab', array('placeholder'=>'Pilih')+Laboratorium::lists('nama_laboratorium','id_lab'),null,array('id'=>'id_lab')) }}
            </div>
        </div>

        <div class="control-group">
        <label class="control-label span3" for="basicinput">Username</label>
            <div class="controls span6">
                <input name="username" type="text" class="form-control span8"/>
            </div>
        </div>

        <div class="control-group">
        <label class="control-label span3" for="basicinput">Password</label>
            <div class="controls span6">
                <input name="password" type="password" class="form-control span8"/>
            </div>
        </div>

        <div class="control-group">
            <div class="controls">
                <input name="id_lab" type="hidden" class="form-control span8"/>
                <input name="nik" type="hidden" class="form-control span8"/>
                <button type="submit" class="btn btn-success">Simpan</button>
                <a href="{{URL::to('#')}}" class="btn btn-warning">Kembali</a>
            </div>
        </div>
        {{Form::close()}}
        </div>
    </div>
    </div>
</div>
@stop