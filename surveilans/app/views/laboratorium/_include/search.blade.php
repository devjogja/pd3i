<div class="module-body">
	<div class="row-fluid">
		<table class="table form-horizontal" style="width:100%;">
			<tr>
				<td style="text-align:right;width:15%;">
					No. Epid / Nama
				</td>
				<td style="width:30%;">
					<input type="text" id="cari_no_epid" name="no_epid" required="required" placeholder="masukan no. epidemologi atau nama">
					<button id="cari" class="btn btn-success" onclick="search_pasien_campak()" >cari pasien</button>
				</td>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td colspan="4"></td>
			</tr>
		</table>
	</div>
</div>
<div class="module message">
	<div class="module-body table" id="result" style="display:none">
		<table class="display table table-bordered">
			<thead>
				<tr>
					<th colspan="5" style="text-align:right" >
						<a class='btn btn-warning' href="{{URL::to('input_hasil_lab/0')}}">+ Tambah Pasien</a>
					</th>
				</tr>
				<tr>
					<th>No.Epid</th>
					<th>Nama</th>
					<th>Nama Orang Tua</th>
					<th>Alamat</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>