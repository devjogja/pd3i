<div class="tab-pane fade in" id="analisis">
	<div class="module">
		<div class="module-body">
			<form id="search2" method="post" action="{{URL::to('labs/get_analisa')}}">
				<div class="chart inline-legend grid">
					@include('laboratorium._include.filter_analisis')
					<center>
						<input id="tampilkan_analisa" type="submit" value="Tampilkan" class="btn btn-success">
						<a href="#"  id='export_analisis' disabled='disabled' class="btn btn-success">Export Excel</a>
					</center>
				</div>
			</form>
		</div>
	</div>
	<div class="module">
		<div class="module-head">
			<h3 style="float:left;">Jumlah Sampel Berdasarkan Wilayah</h3>
			<div style="float:right;">
				<a href="{{URL::to('download/jenis_kelamin/excel')}}"><img src="{{URL::to('asset/images/excel.png')}}" style="width:20px;"></a>
			</div>
			<div style="clear:both;"></div>
		</div>
		<div class="module-body">
			<div class="chart inline-legend grid" id="container_jk">
				Silahkan Klik tombol tampilkan di atas !
			</div>
		</div>
	</div>
	<div class="module">
		<div class="module-head">
			<h3 style="float:left;">Grafik Kasus Berdasarkan hasil Serologi</h3>
			<div style="float:right;">
				<a href="{{URL::to('download/jenis_kelamin/excel')}}"><img src="{{URL::to('asset/images/excel.png')}}" style="width:20px;"></a>
			</div>
			<div style="clear:both;"></div>
		</div>
		<div class="module-body">
			<div class="chart inline-legend grid" id="">
				Silahkan Klik tombol tampilkan di atas !
			</div>
		</div>
	</div>
	<div class="module">
		<div class="module-head">
			<h3 style="float:left;">Jumlah Kasus Campak Berdasarkan Usia</h3>
			<div style="float:right;">
				<a href="{{URL::to('download/jenis_kelamin/excel')}}"><img src="{{URL::to('asset/images/excel.png')}}" style="width:20px;"></a>
			</div>
			<div style="clear:both;"></div>
		</div>
		<div class="module-body">
			<div class="chart inline-legend grid" id="">
				Silahkan Klik tombol tampilkan di atas !
			</div>
		</div>
	</div>
	<div class="module">
		<div class="module-head">
			<h3 style="float:left;">Jumlah Kasus Rubella Berdasarkan Usia</h3>
			<div style="float:right;">
				<a href="{{URL::to('download/jenis_kelamin/excel')}}"><img src="{{URL::to('asset/images/excel.png')}}" style="width:20px;"></a>
			</div>
			<div style="clear:both;"></div>
		</div>
		<div class="module-body">
			<div class="chart inline-legend grid" id="">
				Silahkan Klik tombol tampilkan di atas !
			</div>
		</div>
	</div>
	<div class="module">
		<div class="module-head">
			<div class="module-head">
				<h3 style="float:left;">Peta Penderita Kasus Campak</h3>
				<div style="float:right;">
					<input type="hidden" id="last_maps_id" name="last_maps_id" value="0">
					<input type="hidden" id="max_maps_id" name="max_maps_id" value="50">
					<a href="{{URL::to('download/wilayah/excel')}}"><img src="{{URL::to('asset/images/excel.png')}}" style="width:20px;"></a>
				</div>
				<div style="clear:both;"></div>
			</div>
		</div>
		<div class="module-body">
			{{-- <div class="chart inline-legend grid" id="map_dashboard" style="height:600px;width:100%;">
				Silahkan Klik tombol tampilkan di atas !
			</div> --}}
		</div>
	</div>
</div>