<table class="table" style="width:100%;">
	<tr>
		<td style="text-align:right;width:10%;">
			Unit
		</td>
		<td style="width:25%;">
			<select name="unit" class="unit" style="width: 90px;" onkeypress="focusNext('day_start', 'clinic_id', this, event)" onchange="setDisable(this, event)">
				<option value="all">All</option>
				<option value="day">Hari</option>
				<option value="month">Bulan</option>
				<option value="year">Tahun</option>
			</select>
		</td>
		<td style="text-align:right;">
			Laboratorium
		</td>
		<td>
			<select name="lab_id" id="lab_id" style="width: 40%;">
				<option value="">--- Pilih ---</option>
				<?php
					$q = "
						SELECT
							id_laboratorium,
							lab_code,
							nama_laboratorium
						FROM laboratorium
					";
					$cb=DB::select($q);
				?>
				@for($i=0;$i<count($cb);$i++)
					<option value="{{ $cb[$i]->id_laboratorium }}">
						{{ $cb[$i]->nama_laboratorium; }}
					</option>
				@endfor
			</select>
		</td>
	</tr>
	<tr>
		<td style="text-align:right;">
			Sejak
		</td>
		<td>
			<?php
				$start['mktime'] = strtotime("-1 day");
				$start['day'] = date("j", $start['mktime']);
				$start['month'] = date("n", $start['mktime']);
				$start['year'] = date("Y", $start['mktime']);
				
				$now['day'] = date("j");
				$now['month'] = date("n");
				$now['year'] = date("Y");
				$now['year_start'] = 1971;
				
				$arr_nama_bulan[1] = "Januari";
				$arr_nama_bulan[2] = "Februari";
				$arr_nama_bulan[3] = "Maret";
				$arr_nama_bulan[4] = "April";
				$arr_nama_bulan[5] = "Mei";
				$arr_nama_bulan[6] = "Juni";
				$arr_nama_bulan[7] = "Juli";
				$arr_nama_bulan[8] = "Agustus";
				$arr_nama_bulan[9] = "September";
				$arr_nama_bulan[10] = "Oktober";
				$arr_nama_bulan[11] = "November";
				$arr_nama_bulan[12] = "Desember";
				
				?>
				<select name="day_start" class="day_start" style="width: 50px;" onkeypress="focusNext( 'month_start', 'unit', this, event)">
				<?php for($i=1;$i<32;$i++) :
						if($i==$start['day']) $sel = "selected"; else $sel = "";    ?>
					<option value="<?=$i?>" <?=$sel?> ><?=$i?></option>
				<?php endfor; ?>
				</select>
				<select name="month_start" class="month_start" style="width: 100px;" onkeypress="focusNext( 'year_start', 'day_start', this, event)">
					<?php for($i=1;$i<13;$i++) :
							if($i==$start['month']) $sel = "selected"; else $sel = ""; ?>
						<option value="<?php echo $i?>" <?php echo $sel?> ><?php echo $arr_nama_bulan[$i]; ?></option>
					<?php endfor; ?>
				</select>
				<select name="year_start" class="year_start" style="width: 70px;" onkeypress="focusNext( 'day_end', 'month_start', this, event)" class="inputan">
					<?php for($i=$now['year_start'];$i<=$now['year'];$i++) :
							if($i==$start['year']) $sel = "selected"; else $sel = "";   ?>
						<option value="<?php echo $i?>" <?php echo $sel?>><?php echo $i?></option>
					<?php endfor; ?>
				</select>
		</td>
		<td style="text-align:right;width:15%;">
			Provinsi
		</td>
		<td>
			<select name="province_id" id="province_id_analisis" style="width:40%" onchange="gets_district('_analisis');" >
			<option value="">--- Pilih ---</option>
			<?php
				$q = "
					SELECT
						id_provinsi,
						provinsi
					FROM provinsi
					ORDER BY provinsi ASC
				";
				$combo_district=DB::select($q);
			?>
			@for($i=0;$i<count($combo_district);$i++)
				<option value="{{ $combo_district[$i]->id_provinsi }}" >
					{{ $combo_district[$i]->provinsi; }}
				</option>
			@endfor
			</select>
		</td>
	</tr>
	<tr>
		<td style="text-align:right;">
			Sampai
		</td>
		<td>
			<select name="day_end" class="day_end" style="width: 50px;" >
			<?php for($i=1;$i<32;$i++) :
					if($i==$now['day']) $sel = "selected"; else $sel = "";  ?>
				<option value="<?=$i?>" <?=$sel?> ><?=$i?></option>
			<?php endfor; ?>
			</select>
			<select name="month_end" class="month_end" style="width: 100px;" >
				<?php for($i=1;$i<13;$i++) :
						if($i==$now['month']) $sel = "selected"; else $sel = ""; ?>
					<option value="<?php echo $i?>" <?php echo $sel?> ><?php echo $arr_nama_bulan[$i];?></option>
				<?php endfor; ?>
			</select>
			<select name="year_end" class="year_end inputan" style="width: 70px;" >
				<?php for($i=$now['year_start'];$i<=$now['year'];$i++) :
						if($i==$now['year']) $sel = "selected"; else $sel = ""; ?>
					<option value="<?php echo $i?>" <?php echo $sel?> ><?php echo $i?></option>
				<?php endfor; ?>
			</select>                                   
		</td>
		<td style="text-align:right;">
			Kabupaten
		</td>
		<td>
			<select name="district_id" id="district_id_analisis" style="width: 40%;" disabled="disabled"  onchange="gets_rs('_analisis')">
				<option value="">--- Pilih ---</option>
			</select>
		</td>
	</tr>
	<tr>
		<td style="text-align:right;"></td>
		<td></td>
		<td style="text-align:right;">
			Kecamatan
		</td>
		<td>
			<select name="sub_district_id" id="sub_district_id" style="width: 40%;" disabled="disabled"  onchange="get_village();">
				<option value="">--- Pilih ---</option>
			</select>
		</td>
	</tr>
	<tr>
		<td style="text-align:right;"></td>
		<td></td>
		<td style="text-align:right;"></td>
		<td></td>
	</tr>
</table>