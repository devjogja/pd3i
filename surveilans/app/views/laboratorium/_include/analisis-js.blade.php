<?php
    $name_lab = 'Campak';
    $names = 'Campak';
    $nama_instansi = 'Indonesia';
    $start_disp = '2015';
    $end_disp = '2015';
    $x_data = "['1']"
?>
$('#container_jk').highcharts({
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45,
            beta: 0
        }
    },
    title: {
        text: 'Grafik Jumlah sampel <?php echo $name_lab; ?> Berdasar Wilayah'
    },
	subtitle: {
        text: '<?php echo $names?><br><?php echo $nama_instansi; ?><br><?php echo $start_disp." s/d ".$end_disp; ?>',
        x: -20
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 35,
            dataLabels: {
                enabled: true,
                format: '{point.name} <b>({point.y} Jiwa)<br>{point.percentage:.1f}%</b>'
            }
        }
    },
    series: [{
        type: 'pie',
        name: 'Jumlah Sampel ',
        data: [
			<?php echo $x_data; ?>
        ]
    }]
});