@extends('layouts.master')
@section('content')
<div class="span12">
	<div class="content">
		<div class="module">
			<div class="module-body" >
				<div class="profile-head media">
					<h4>Penyakit Campak</h4>
					<hr>
				</div>

			{{-- Tab Navigasi --}}
			<ul class="profile-tab nav nav-tabs">
				<li class="active"><a href="#input" data-toggle="tab">Input data individual kasus</a></li>
				<li><a href="#input_lab" data-toggle="tab">Input hasil laboratorium</a></li>
			</ul>

			{{-- Content Dari Tab --}}
				<div class="profile-tab-content tab-content">
				{{-- Daftar Kasus --}}
				{{-- Input Data Individual Kasus --}}
					<div class="tab-pane fade active in" id="input">
					<!-- awal class alert -->
                  <div class="alert alert-error">
                      <p style="font-size:12px">Text input yang bertanda bintang <strong>(*)</strong> wajib di isi
                  </div>
                  <!-- akhir class alert -->

                  <!-- awal form -->
                    {{Form::open(array('route'=>'campak.store','class'=>'form-horizontal','id'=>'form_save_campak'))}}
                    <!-- awal class module-body -->
                    <div class="module-body">
                      <!-- awal class row-fluid -->
                      <div class="row-fluid">
                        <!-- awal class span6 -->
                          <div class="span6">
                            <!-- awal class media -->
                              <div class="media">
                                <!-- awal code fieldset -->
                                  <fieldset>
                                    <legend>Identitas penderita</legend>
                                    <!-- awal input kolom nama penderita -->
                                    <div class="control-group">
                                        <label class="control-label">Nama penderita</label>
                                        <div class="controls">
                                            {{Form::text(
                                              'nama_anak',
                                              Input::old('nama_anak'), 
                                              array(
                                                'data-validation'=>'[MIXED]',
                                                'data'=>'$ wajib di isi!',
                                                'data-validation-message'=>'nama penderita wajib di isi!',
                                                'class' => 'input-xlarge',
                                                'placeholder'=>'Nama penderita'
                                                )
                                              )
                                            }}
                                            <span class="help-inline" style="color:red">(*)</span>
                                        </div>
                                      </div>
                                      <!-- akhir input kolom nama penderita -->

                                      <!-- awal input kolom nik -->
                                      <div class="control-group">
                                        <label class="control-label">NIK</label>
                                        <div class="controls">
                                            {{Form::text(
                                              'nik',
                                              Input::old('nik'), 
                                              array(
                                                'class' => 'input-medium',
                                                'placeholder'=>'Nomer induk kependudukan'
                                                )
                                              )
                                            }}
                                            <span class="help-inline"></span>
                                        </div>
                                      </div>
                                      <!-- akhir input kolom nik -->

                                      <!-- awal input kolom nama ortu -->
                                      <div class="control-group">
                                        <label class="control-label">Nama orang tua</label>
                                        <div class="controls">
                                            {{Form::text(
                                              'nama_ortu',
                                              Input::old('nama_ortu'), 
                                              array(
                                                'data-validation'=>'[MIXED]',
                                                'data'=>'$ wajib di isi!',
                                                'data-validation-message'=>'nama orang tua wajib di isi!',
                                                'class' => 'input-xlarge',
                                                'placeholder'=>'Nama orang tua'
                                                )
                                              )
                                            }}
                                            <span class="help-inline" style="color:red">(*)</span>
                                        </div>
                                      </div>
                                      <!-- akhir input kolom nama ortu -->

                                      <!-- awal input kolom jenis kelamin -->
                                      <div class="control-group">
                                        <label class="control-label">Jenis kelamin</label>
                                        <div class="controls">
                                              {{Form::select
                                              (
                                                'jenis_kelamin', 
                                                array
                                                (
                                                  '' => 'Pilih',
                                                  '0'=>'Laki-laki',
                                                  '1'=>'Perempuan',
                                                  '2' => 'Tidak jelas'
                                                ),
                                                null,
                                                array
                                                (
                                                  'data-validation'=>'[MIXED]',
                                                  'data'=>'$ wajib di isi!',
                                                  'data-validation-message'=>'jenis kelamin wajib di isi!',
                                                  'class' => 'input-medium id_combobox',
                                                  'id'=>'jenis_kelamin'
                                                )
                                              )
                                              }}
                                            <span class="help-inline" style="color:red">(*)</span>
                                        </div>
                                      </div>
                                      <!-- akhir input kolom jenis kelamin -->

                                      <!-- awal input kolom tanggal lahir -->
                                      <div class="control-group">
                                        <label class="control-label">Tanggal lahir</label>
                                        <div class="controls">
                                            {{Form::text(
                                              'tanggal_lahir',
                                              Input::old('tanggal_lahir'), 
                                              array(
                                                'data-validation'=>'[MIXED]',
                                                'data'=>'$ wajib di isi!',
                                                'data-validation-message'=>'tanggal lahir wajib di isi!',
                                                'class' => 'input-medium tgl_lahir',
                                                'placeholder'=>'Tanggal lahir',
                                                'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
                                                'onchange'=>'umur()'
                                                )
                                              )
                                            }}
                                            <span class="help-inline" style="color:red">(*)</span>
                                        </div>
                                      </div>
                                      <!-- akhir input kolom tanggal lahir -->

                                      <!-- awal input kolom umur -->
                                      <div class="control-group">
                                          <label class="control-label">Umur</label>
                                          <div class="controls">
                                            <input type="text" class="input-mini"  name="tgl_tahun" id="tgl_tahun" onchange="tgl_lahir()">
                                            Thn
                                            <input type="text" class="input-mini" name="tgl_bulan" id="tgl_bulan" onchange="tgl_lahir()">
                                            Bln 
                                            <input type="text" class="input-mini" name="tgl_hari" id="tgl_hari" onchange="tgl_lahir()">
                                            Hr
                                          </div>
                                      </div>
                                      <!-- akhir input kolom umur -->

                                      <!-- awal input kolom nama faskes -->
                                      <div class="control-group">
                                        <label class="control-label">Nama faskes saat periksa</label>
                                        <div class="controls">
                                            <?php
                                            //ambil nama puskesmas scara otomatis sesuia user yg login
                                                $type = Session::get('type');
                                                $kd_faskes = Session::get('kd_faskes');
                                                if($type == "puskesmas")
                                                {
                                                    $q = "
                                                        SELECT b.puskesmas_name,b.alamat
                                                        FROM puskesmas b
                                                        WHERE b.puskesmas_code_faskes='".$kd_faskes."'";
                                                    $data=DB::select($q);
                                                    for($i=0;$i<count($data);$i++)
                                                    {
                                                        $nama_instansi = "PUSKESMAS ". $data[0]->puskesmas_name;
                                                      echo Form::text('nama_puskesmas',$nama_instansi, array('class' => 'input-large','placeholder'=>'Masukan nama puskesmas')) ;
                                                    }
                                                }
                                                elseif ($type == "rumahsakit") 
                                                {
                                                
                                                }
                                            ?>
                                              <span class="help-inline"></span>
                                          </div>
                                      </div>
                                      <!-- akhir input kolom faskes -->

                                      <!-- awal input kolom provinsi -->
                                      <div class="control-group">
                                        <label class="control-label">Provinsi</label>
                                        <div class="controls">
                                            {{ Form::select(
                                              '', 
                                              array(
                                                'placeholder'=>'Pilih provinsi')+Provinsi::lists('provinsi','id_provinsi'),
                                                null,
                                                array(
                                                  'id'=>'id_provinsi',
                                                  'placeholder' => 'Pilih Provinsi',
                                                  'onchange'=>'showKabupaten()',
                                                  'class'=>'input-large id_combobox'
                                                )
                                              )
                                            }}
                                            <span class="help-inline"></span>
                                        </div>
                                      </div>
                                      <!-- akhir input kolom provinsi -->

                                      <!-- awal input kolom kabupaten -->
                                      <div class="control-group">
                                        <label class="control-label">Kabupaten</label>
                                        <div class="controls">
                                            {{Form::select(
                                              '',
                                              array(''),
                                              null, 
                                              array(
                                                'placeholder'=>'Pilih',
                                                'class' => 'input-medium id_combobox',
                                                'id'=>'id_kabupaten',
                                                'onchange'=>'showKecamatan()'
                                                )
                                              )
                                            }}
                                            <span class="lodingKab"></span>
                                            <span class="help-inline"></span>
                                        </div>
                                      </div>
                                      <!-- akhir input kolom kabupaten -->

                                      <!-- awal input kolom kecamatan -->
                                      <div class="control-group">
                                        <label class="control-label">Kecamatan</label>
                                        <div class="controls">
                                            {{Form::select(
                                              '',
                                              array(''),
                                              null, 
                                              array(
                                                'placeholder'=>'Pilih',
                                                'class' => 'input-medium id_combobox',
                                                'id'=>'id_kecamatan',
                                                'onchange'=>'showKelurahan()'
                                                )
                                              )
                                            }}
                                            <span class="lodingKec"></span>
                                            <span class="help-inline"></span>
                                        </div>
                                      </div>
                                      <!-- akhir input kolom kecamatan -->

                                      <!-- awal input kolom kelurahan atau desa -->
                                      <div class="control-group">
                                        <label class="control-label">Kelurahan/Desa</label>
                                        <div class="controls">
                                            {{Form::select(
                                              'id_kelurahan',
                                              array(''),
                                              null, 
                                              array(
                                                'placeholder'=>'Pilih',
                                                'class' => 'input-medium id_combobox',
                                                'id'=>'id_kelurahan',
                                                'onchange'=>'getfaskes()'
                                                )
                                              )
                                            }}
                                            <span class="lodingKel"></span>
                                            <span class="help-inline" style="color:red">(*)</span>
                                        </div>
                                      </div>
                                      <!-- akhir input kolom kelurahan atau desa -->

                                      <!-- awal input kolom alamat -->
                                      <div class="control-group">
                                        <label class="control-label">Alamat</label>
                                        <div class="controls">
                                            {{Form::textarea(
                                              'alamat',
                                              null,
                                              array(
                                                'rows'=>'3',
                                                'style'=>'width:270px;',
                                                'placeholder' => 'Hanya diisi nama jalan, no. rumah dan no. RT - RW'
                                                )
                                              )
                                            }}
                                            <span class="help-inline" style="color:red">(*)</span>
                                        </div>
                                      </div>
                                      <!-- akhir input kolom alamat -->
                                      
                                      <!-- awal input kolom nama puskesmas wilayah -->
                                      <!-- <div class="control-group">
                                        <label class="control-label">Nama puskesmas wilayah tempat tinggal pasien</label>
                                        <div class="controls" id="faskes">
                                          <span class="help-inline"></span>
                                        </div>
                                      </div> -->
                                      <!-- akhir input kolom nama puskesmas wilayah -->
                                      
                                      <!-- awal input kolom No epid baru-->
                                      <div class="control-group">
                                        <label class="control-label">No Epidemologi</label>
                                        <div class="controls">
                                            {{Form::text(
                                              'no_epid',
                                              Input::old('no_epid'), 
                                              array(
                                                'data-validation'=>'[MIXED]',
                                                'data'=>'$ wajib di isi!',
                                                'data-validation-message'=>'no epid wajib di isi!',
                                                'class' => 'input-medium',
                                                'placeholder'=>'Nomer epidemologi',
                                                'id'=>'epid'
                                                )
                                              )
                                            }}
                                            <span class="help-inline" style="color:red">(*)</span>
                                        </div>
                                      </div>
                                      <!-- akhir input kolom no epid baru -->

                                      <!-- awal input kolom no epid lama -->
                                      <div class="control-group">
                                        <label class="control-label">No Epidemologi lama</label>
                                        <div class="controls">
                                          {{Form::text(
                                            'no_epid_lama',
                                            Input::old('no_epid_lama'), 
                                            array(
                                              'class' => 'input-medium',
                                              'placeholder'=>'Nomer epidemologi',
                                              'id'=>'epid'
                                              )
                                            )
                                          }}
                                          <span class="help-inline"></span>
                                        </div>
                                      </div>
                                      <!-- akhir input kolom no epid lama -->
                                    </fieldset>
                                    <!-- akhir code fieldset -->
                                </div>
                                <!-- akhir class media -->
                            </div>
                            <!-- akhir class span6 -->

                            <!-- awal class span6 -->
                            <div class="span6">
                              <!-- awal class media -->
                                <div class="media">
                                  <!-- awal code fieldset -->
                                    <fieldset>
                                      <legend>Data klinis campak</legend>
                                      <!-- awal input kolom tanggal imunisasi campak terakhir -->
                                      <div class="control-group">
                                        <label class="control-label">Tanggal imunisasi campak terakhir</label>
                                        <div class="controls">
                                          {{Form::text(
                                            'tanggal_imunisasi_terakhir',
                                            Input::old('tanggal_imunisasi_terakhir'),
                                            array(
                                              'data-validation'=>'[MIXED]',
                                              'data'=>'$ wajib di isi!',
                                              'data-validation-message'=>'tanggal imunisasi campak terakhir wajib di isi!',
                                              'class'=>'input-medium',
                                              'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
                                              )
                                            )
                                          }}
                                            <span class="help-inline" style="color:red">(*)</span>
                                        </div>
                                      </div>
                                      <!-- akhir input kolom tanggal imunisasi campak terakhir -->
                                      
                                      <!-- awal input kolom Imunisasi campak sebelum sakit -->
                                      <div class="control-group">
                                        <label class="control-label">Imunisasi campak sebelum sakit berapa kali?</label>
                                        <div class="controls">
                                          {{Form::select(
                                            'vaksin_campak_sebelum_sakit', 
                                            array('' => 'Pilih',
                                              '0' => '1X',
                                              '1'=>'2X',
                                              '2'=>'3X',
                                              '3'=>'4X',
                                              '4'=>'5X',
                                              '5'=>'6X',
                                              '6'=>'Tidak',
                                              '7'=>'Tidak tahu'
                                              ),
                                            null, 
                                            array(
                                              'data-validation'=>'[MIXED]',
                                              'data'=>'$ wajib di isi!',
                                              'data-validation-message'=>'imunisasi campak sebelum sakit wajib di isi!',
                                              'class' => 'input-small id_combobox'
                                              )
                                            )
                                          }}
                                          <span class="help-inline" style="color:red">(*)</span>
                                        </div>
                                      </div>
                                      <!-- akhir input kolom Imunisasi campak sebelum sakit -->
                                      
                                      <!-- awal input kolom tanggal mulai demam -->
                                      <div class="control-group">
                                        <label class="control-label">Tanggal mulai demam</label>
                                        <div class="controls">
                                            {{Form::text(
                                              'tanggal_timbul_demam',
                                              Input::old('tanggal_timbul_demam'),
                                              array(
                                                'data-validation'=>'[MIXED]',
                                                'data'=>'$ wajib di isi!',
                                                'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
                                                'data-validation-message'=>'tanggal mulai sakit/demam wajib di isi!',
                                                'class'=>'input-medium tgl_mulai_sakit',
                                                'onchange'=>'tgl_lahir()'
                                                )
                                              )
                                            }}
                                            <span class="help-inline" style="color:red">(*)</span>
                                        </div>
                                      </div>
                                      <!-- akhir input kolom tanggal mulai demam -->
                                      
                                      <!-- awal input kolom tanggal mulai rash -->
                                      <div class="control-group">
                                        <label class="control-label">Tanggal mulai rash</label>
                                        <div class="controls">
                                            {{Form::text(
                                              'tanggal_timbul_rash',
                                              Input::old('tanggal_timbul_rash'),
                                              array(
                                                'data-validation'=>'[MIXED]',
                                                'data'=>'$ wajib di isi!',
                                                'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
                                                'data-validation-message'=>'tanggal mulai rash wajib di isi!',
                                                'class'=>'input-medium tgl_mulai_rash',
                                                'onchange'=>'showEpidCampak()'
                                                )
                                              )
                                            }}
                                            <span class="help-inline" style="color:red">(*)</span>
                                        </div>
                                      </div>
                                      <!-- akhir input kolom tanggal mulai rash -->
                                  
                                      <!-- awal input kolom gejala -->
                                      <div class="control-group">
                                        <label class="control-label">Gejala / tanda lainnya</label>
                                        <div class="controls">
                                          <table>
                                            <?php  $daftargejala = Campak::DaftarGejala()?>
                                              @foreach($daftargejala as $row)
                                              <tr>
                                                  <td>
                                                    <input type="checkbox" value="{{$row->id}}" name="gejala_lain[]" class="gejala_lain_{{$row->id}}" onclick="check({{$row->id}})">
                                                    {{$row->nama}}
                                                  </td>
                                                  <td>
                                                    <input type="text" class="input-medium tgl_gejala_lain_{{$row->id}}" disabled="disabled" name="tanggal_gejala_lain[]" data-uk-datepicker='{format:"DD-MM-YYYY"}'>
                                                  </td> 
                                              </tr>
                                              @endforeach
                                            </table>
                                            <span class="help-inline"></span>
                                        </div>
                                      </div>
                                      <!-- akhir input kolom gejala -->
                                      
                                      <!-- awal input komplikasi -->
                                      <div class="control-group">
                                        <label class="control-label">Komplikasi</label>
                                        <div class="controls">
                                          <table>
                                              <tr>
                                                <td>
                                                  <input type="checkbox" name="komplikasi[]" value="0">Diare
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <input type="checkbox" name="komplikasi[]" value="1">Pneumonia
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <input type="checkbox" name="komplikasi[]" value="2">Bronchopneumonia
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <input type="checkbox" name="komplikasi[]" value="3">OMA
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <input type="checkbox" name="komplikasi[]" value="4">Ensefalitis
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <input type="checkbox" class="gejala_lain_komplikasi" onclick="chek_komplikasi_lain()">Lain-lain (sebutkan)
                                                </td>
                                                <td>
                                                  <input type="text" name="komplikasi[]" class="input-medium komplikasi_lainnya">
                                                </td>
                                              </tr>
                                          </table>
                                        </div>
                                      </div>
                                      <!-- awal input komplikasi -->
                                  
                                      <!-- awal input kolom tanggal laporan diterima -->
                                      <div class="control-group">
                                        <label class="control-label">Tanggal laporan diterima</label>
                                        <div class="controls">
                                            {{Form::text(
                                              'tanggal_laporan_diterima',
                                              Input::old('tanggal_laporan_diterima'),
                                              array(
                                                'data-validation'=>'[MIXED]',
                                                'data'=>'$ wajib di isi!',
                                                'data-validation-message'=>'tanggal laporan diterima wajib di isi!',
                                                'class'=>'input-medium',
                                                'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
                                                )
                                              )
                                            }}
                                            <span class="help-inline" style="color:red">(*)</span>
                                        </div>
                                      </div>
                                      <!-- akhir input kolom tanggal laporan diterima -->

                                      <!-- awal input kolom tanggal pelacakan -->
                                        <div class="control-group">
                                        <label class="control-label">Tanggal pelacakan</label>
                                        <div class="controls">
                                            {{Form::text('tanggal_pelacakan',Input::old('tanggal_pelacakan'),array('data-validation'=>'[MIXED]','data'=>'$ wajib di isi!','data-validation-message'=>'tanggal pelacakan wajib di isi!','class'=>'input-medium','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))}}
                                            <span class="help-inline" style="color:red">(*)</span>
                                        </div>
                                      </div>
                                      <!-- akhir input kolom tanggal pelacakan -->
                                      
                                      <!-- awal input kolom diberi vitamin -->
                                      <div class="control-group">
                                        <label class="control-label">Diberi vitamin A</label>
                                          <div class="controls">
                                            {{Form::select(
                                              'vitamin_A', 
                                              array(
                                                '' => 'Pilih',
                                                '0' => 'Ya',
                                                '1'=>'Tidak'),
                                                 Input::old('vitamin_A'), 
                                                 array(
                                                  'class' => 'input-small id_combobox'
                                                )
                                               )
                                            }}
                                            <span class="help-inline"></span>
                                          </div>
                                      </div>
                                      <!-- akhir input kolom diberi vitamin -->

                                      <!-- awal input kolom keadaan akhir-->
                                      <div class="control-group">
                                        <label class="control-label">Keadaan akhir</label>
                                        <div class="controls">
                                          {{Form::select(
                                            'keadaan_akhir', 
                                            array(
                                              '' => 'Pilih',
                                              '0' => 'Hidup',
                                              '1'=>'Meninggal'), 
                                              Input::old('keadaan_akhir'), 
                                              array(
                                                'data-validation'=>'[MIXED]',
                                                'data'=>'$ wajib di isi!',
                                                'data-validation-message'=>'keadaan akhir wajib di isi!',
                                                'class' => 'input-medium id_combobox'
                                              )
                                            )
                                          }}
                                          <span class="help-inline" style="color:red">(*)</span>
                                        </div>
                                      </div>
                                      <!-- akhir input kolom keadaan vitamin -->
                                    </fieldset>
                                    <!-- akhir code fieldset -->
                                </div>
                                <!-- akhir class media -->
                            </div>
                            <!-- akhir class span6 -->
                          </div>

                          <div class="row-fluid">
                            <!-- awal class span6 -->
                            <div class="span12">
                              <!-- awal class media -->
                                <div class="media">
                                    <!-- awal class fieldset -->
                                    <fieldset>
                                    <legend>Data spesimen dan hasil laboratorium</legend>
                                    <!-- daftar sampel -->
                                    <table class="table table-bordered" id="data_sampel">
                                        <tr>
                                            <td>Nama pemeriksaan</td>
                                            <td>Jenis sampel</td>
                                            <td>Tanggal ambil sampel</td>
                                            <td>aksi</td>
                                        </tr>
                                    </table> 
                                    <!-- akhir daftar sampel-->
                                    </br>
                                    <a class="btn btn-success" onclick="tambah_sampel()" id="tambah_sampel">tambah sampel</a>
                                    </br>
                                    <hr>

                                    <div id="div_sampel">
                                    <table class="table table-bordered" id="data_sampel">
                                        <tr>
                                            <td>Nama pemeriksaan</td>
                                            <td>Jenis sampel</td>
                                            <td>Tanggal ambil sampel</td>
                                        </tr>
                                         <tr>
                                            <td>
                                                <select id="nama_sampel" onchange="pilih_sampel()">
                                                    <option value="">Pilih</option>
                                                    <option value="0">Serologi</option>
                                                    <option value="1">Verologi</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="jenis_sampel" onchange="jenissampel()">
                                                    <option value="">Pilih</option>
                                                </select>
                                            </td>
                                            <td>
                                                {{
                                                    Form::text(
                                                        '',
                                                        Input::old('tanggal_ambil_sampel'),
                                                        array(
                                                            'class'=>'input-medium',
                                                            'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
                                                            'id'=>'tanggal_ambil_sampel'
                                                            )
                                                        )
                                                }}
                                            </td>
                                        </tr>
                                    </table>
                                    <button class="btn btn-primary" style="margin-left:330px;margin-top:30px" onclick="add_sampel()" id="simpan">tambah</button>
                                  <button class="btn btn-warning" style="margin-left:10px;margin-top:30px" onclick="tutup_sampel()">tutup</button>
                                    <!-- akhir input kolom pemeriksaan virologi -->
                                    </div>
                                    
                                      <!-- awal input kolom pemeriksaan serologi -->
                                      <!-- <div class="control-group">
                                        <label class="control-label">Pemeriksaan Serologi</label>
                                        <div class="controls">
                                          <table>
                                              <tr>
                                                <td>Jenis Sampel</td>
                                                <td>
                                                  {{Form::select(
                                                    'jenis_spesimen_serologi', 
                                                    array(
                                                      '' => 'Pilih',
                                                      '0' => 'Darah',
                                                      '1'=>'Serum'), 
                                                      Input::old('jenis_spesimen_serologi'), 
                                                      array(
                                                        'class' => 'id_combobox'
                                                      )
                                                    )
                                                  }}
                                                </td> 
                                              </tr>
                                              <tr>
                                                <td>Tanggal ambil sampel</td>
                                                <td>
                                                  {{Form::text(
                                                    'tanggal_ambil_sampel_serologi',
                                                    Input::old('tanggal_ambil_sampel_serologi'),
                                                    array(
                                                      'class'=>'input-medium',
                                                      'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
                                                      )
                                                    )
                                                  }}                                         
                                    </td>
                                  </tr>
                                          </table>
                                        </div>
                                      </div> -->
                                      <!-- akhir input kolom pemeriksaan serologi -->

                                      <!-- awal input kolom pemeriksaan virologi -->
                                      <!-- <div class="control-group">
                                        <label class="control-label">Pemeriksaan Virologi</label>
                                        <div class="controls">
                                          <table>
                                            <tr>
                                                <td>Jenis Sampel</td>
                                                <td>
                                                  {{Form::select(
                                                    'jenis_spesimen_virologi', 
                                                    array(
                                                      '' => 'Pilih',
                                                      '0' => 'Urin',
                                                      '1'=>'Usap tenggorokan',
                                                      '2'=>'Cairan mulut'), 
                                                      Input::old('jenis_spesimen_virologi'), 
                                                      array(
                                                        'class' => 'id_combobox'
                                                      )
                                                    )
                                                  }}
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>Tanggal ambil sampel</td>
                                                <td>
                                                  {{Form::text(
                                                    'tanggal_ambil_sampel_virologi',
                                                    Input::old('tanggal_ambil_sampel_virologi'),
                                                    array('class'=>'input-medium',
                                                      'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}
                                                      '
                                                      )
                                                    )
                                                  }}                                         
                                                </td>
                                              </tr>
                                          </table>
                                        </div>
                                      </div> -->
                                      <!-- akhir input kolom pemeriksaan virologi -->
                                      

                                      <!-- 
                                        cek jika user yang login mempunyai hak_akses 4
                                        yang berarti user level kabupaten
                                        maka input hasil laboratorium akan ditampilkan 
                                      -->
                                      @if(Sentry::getUser()->hak_akses==4 || Sentry::getUser()->hak_akses==5)
                                      <!-- awal input kolom hasil laboratorium-->
                                      <!-- <div class="control-group">
                                        <label class="control-label">Hasil Laboratorium</label>
                                        <div class="controls">
                                          <table>
                                            <tr>Hasil Laboratorium Serologi</tr>
                                              <tr>
                                                <td>IgM Campak</td>
                                                <td>
                                                  {{Form::select(
                                                    'hasil_serologi_igm_campak', 
                                                    array(
                                                      '' => 'Pilih',
                                                      '0' => 'Positif',
                                                      '1'=>'Negatif',
                                                      '2'=>'Equivocal',
                                                      '3'=>'pending'), 
                                                      Input::old('hasil_serologi_igm_campak'), 
                                                      array(
                                                        'class' => 'id_combobox'
                                                      )
                                                    )
                                                  }}
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>IgM Rubella</td>
                                                <td>
                                                  {{Form::select(
                                                    'hasil_serologi_igm_rubella', 
                                                    array(
                                                      '' => 'Pilih',
                                                      '0' => 'Positif',
                                                      '1'=>'Negatif',
                                                      '2'=>'Equivocal',
                                                      '3'=>'pending'), 
                                                      Input::old('hasil_serologi_igm_rubella'), 
                                                      array(
                                                        'class' => 'id_combobox'
                                                      )
                                                    )
                                                  }}
                                                </td>
                                              </tr>
                                            </table>
                                            <table>
                                              <br>
                                              <tr>Hasil Laboratorium Virologi</tr>
                                            <tr>
                                              <td>IgM Campak</td>
                                              <td>
                                                {{Form::select(
                                                  'hasil_virologi_igm_campak', 
                                                  array(
                                                    '' => 'Pilih',
                                                    '0' => 'Positif',
                                                    '1'=>'Negatif',
                                                    '2'=>'pending'), 
                                                    Input::old('hasil_virologi_igm_campak'), 
                                                    array(
                                                      'class' => 'id_combobox'
                                                    )
                                                  )
                                                }}
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>IgM Rubella</td>
                                              <td>
                                                {{Form::select(
                                                  'hasil_virologi_igm_rubella', 
                                                array(
                                                  '' => 'Pilih',
                                                  '0' => 'Positif',
                                                  '1'=>'Negatif',
                                                  '2'=>'pending'), 
                                                  Input::old('hasil_virologi_igm_rubella'), 
                                                  array(
                                                    'class' => 'id_combobox'
                                                  )
                                                  )
                                                }}
                                              </td>
                                            </tr>
                                          </table>
                                        </div>
                                      </div> -->
                                      <!-- akhir input kolom hasil laboratorium -->
                                      @endif
                                      <!-- akhir cek user -->
                                    </fieldset>
                                    <!-- akhir code fieldset -->
                                </div>
                                <!-- akhir class media -->
                            </div>
                            <!-- akhir class span6 -->
                        </div>
                        <!-- akhir clas row-fluid -->
                </div>
                <!-- akhir class module-body -->
                    
                    <!-- awal class form-actions -->
                    <div class="form-actions">
                        {{Form::submit('simpan',array('class'=>'btn btn-primary'))}}
                        {{Form::reset('batal',array('class'=>'btn btn-warning'))}}
                    </div>
                    <!-- akhir class form-actions -->
                    {{Form::close()}}
                    <!-- akhir form -->
					</div>
					<div class="tab-pane fade" id="input_lab">
                    <input id="id_pasien" name="id_pasien" type="hidden">
					          <input id="id_campak" name="id_campak" type="hidden">
                    <div class="module-body">
                    <table class="table table-bordered">
						          <tr>
	                      <th>No. Epid</th>
	                     	<th>Nama pasien</th>
	                      <th>Usia</th>
                        <th>Jenis kelamin</th>
	                      <th>Keadaan Akhir</th>
	                    </tr>
	                    <tr>
	                    	<td id="no_epid"></td>
	                    	<td id="nama_pasien"></td>
	                    	<td id="usia"></td>
                        <td id="jenis_kelamin"></td>
	                    	<td id="keadaan_akhir"></td>
	                    </tr>
					</table>
					</br>
					</br>
                    <table class="table table-bordered" id="data_hasil_uji_spesimen">
                        <tr>
                            <th>No.</th>
                            <th>Jenis pemeriksaan</th>
                            <th>Jenis sampel</th>
                            <th>IGM Campak</th>
                            <th>IGM Rubella</th>
                            <th>Nama penyakit</th>
                            <th>aksi</th>
                        </tr>
                    </table>
                    </br>
                    <span id="tombol">
                      
                    </span>

                    <!-- <span id="pesan">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>Well done!</strong> Now you are listening me :) 
                    </div>
                    </span> -->
                    <div class="row-fluid" id="inputhasillab">
                            <div /class="span12">
                                <div class="media">
                                </br>
                                <table class="table table-bordered">
                                    <tr>
                                        <td>Jenis pemeriksaan</td>
                                        <td>
                                        {{
                                        Form::select
                                        (
                                            'jenis_pemeriksaan', 
                                            array
                                            (
                                              '3' => 'Pilih',
                                              '0' => 'Serologi',
                                              '1'=>'Verologi'
                                            ), 
                                            Input::old('jenis_pemeriksaan'), 
                                            array
                                            (
                                                'class' => 'id_combobox',
                                                'id'=>'jenis_pemeriksaan',
                                                'onchange' => 'pilihJenisPemeriksaan()'
                                            )
                                        )
                                        }}
                                        </td>
                                    </tr>
                                    <tbody id="tbody">
                                    <tr>
                                        <td>Jenis sampel</td>
                                        <td>
                                        <select name="jenis_sampel" class="id_combobox" id="jenis_sampel">
                                          <option value=''>Pilih</option>
                                        </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Hasil IGM campak</td>
                                        <td>
                                        <select name="hasil_igm_campak" class="id_combobox" id="hasil_igm_campak">
                                          <option value=''>Pilih</option>
                                        </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Hasil IGM Rubella</td> <td>
                                        <select name="hasil_igm_rubella" class="id_combobox" id="hasil_igm_rubella">
                                          <option value=''>Pilih</option>
                                        </select>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div> <!-- /.tab-content-->
			</div> <!--/.module-body-->
		</div> <!--/.module-->
	</div> <!--/.content-->
</div> <!--/.span9-->
@include('pemeriksaan.campak.analisis-js')
<script>
$(document).ready(function(){
	$('select').select2();
  	$('#form_save_campak').submit(function() {
    loading_state();
    $('#form_save_campak').ajaxSubmit({
      type: 'POST',
      success:function(data) {
        $('#status').html('');
        $('#status').html(data.msg);
        $('#no_epid').html(data.no_epid);
        $('#nama_pasien').html(data.nama_pasien);
        $('#usia').html(data.usia);
        $('#jenis_kelamin').html(data.jenis_kelamin);
        $('#keadaan_akhir').html(data.keadaan_akhir);
        $('#id_pasien').val(data.id_pasien);
        $('#id_campak').val(data.id_campak);
        //if(jsonData.status == 'success')location.href  = jsonData.address;
      }
    });
    return false;
  });
});

function loading_state(){
  $('#status').html('Loading...');
} 

</script>

<script>
/*$(document).ready(function(){
  $('#simpan_hasil_lab').submit(function() {
    loading_msg();
    $('#simpan_hasil_lab').ajaxSubmit({
      type: 'POST',
      success:function(data) {
        $('#msg').html('');
        $('#msg').html(data.msg);
        if(data.msg == 'tersimpan')location.href  = '{{URL::to("lab/campak")}}';
      }
    });
    return false;
  });
});*/
/*
function loading_state(){
  $('#status').html('Loading...');
}

function loading_msg(){
  $('#msg').html('Loading...');
}*/ 
</script>

<script type="text/javascript">
    $(document).ready(function() {
      $('#inputhasillab').hide();
      $('#data_hasil_uji_spesimen').load('{{URL::to("get_hasil_uji_spesimen/'+$('#id_campak').val()+'/'+$('#id_pasien').val()+'")}}');
      $('#tombol').html('<tombol class="btn btn-medium btn-success" onclick="tampilInputhasillab()" >Input hasil laboratorium</tombol>');
      $('#validate').validate({
              submit: {
                settings: {
                  scrollToError: {
                      offset: -100,
                      duration: 500
                  }
                }
              }
            });

      $( "#id_kelurahan" ).change(function() {
         $.ajax({

              data:'id_kelurahan='+$('#id_kelurahan').val()+'&date='+$('.tgl_mulai_rash').val(),
              url:'{{URL::to("ambil_epid_campak")}}',
              success:function(data) {
              $('#epid').val(data);
         }
        });
      });

      
      
    });

    function pilihJenisPemeriksaan(){

        $.ajax({

              data:'jenis_pemeriksaan='+$('#jenis_pemeriksaan').val(),
              url:'{{URL::to("pilih_jenis_sampel")}}',
              success:function(data) {
               $('#tbody').html('');
              $('#tbody').html(data);
         }
        });

    }

    function inputDetailPemeriksaan()
    {
        $.ajax({
        //dataType:'json',
        //data: 'sub_district_id='+$('#sub_district_id').val(),
        url:'{{URL::to("input_detail_pemeriksaan")}}',
        success:function(data) {

            
        }
    });
    }
    function tampilInputhasillab()
    {
      $('#inputhasillab').show(2000);
      $('#tombol').html('');
      $('#tombol').html('<tombol class="btn btn-medium btn-success" onclick="hideInputhasillab()">batalkan</tombol>');
    }

    function hideInputhasillab()
    {
      $('#inputhasillab').hide(2000);
      $('#tombol').html('');
      $('#tombol').html('<tombol class="btn btn-medium btn-success" onclick="tampilInputhasillab()">input hasil laboratorium</tombol>');
    }

    function simpanhasillab()
    {
        var jenis_pemeriksaan     = $("#jenis_pemeriksaan").val();
        var jenis_sampel          = $("#jenis_sampel").val();
        var hasil_igm_campak      = $("#hasil_igm_campak").val();
        var hasil_igm_rubella     = $("#hasil_igm_rubella").val();
        var nama_penyakit         = $('#nama_penyakit').val();
        var id                    = $('#id_uji_spesimen').val();
        var id_campak             = $('#id_campak').val();
        // //kirim data ke server
      
        $.post('{{URL::to("hasil_lab")}}', {id_campak:id_campak,id:id,jenis_pemeriksaan:jenis_pemeriksaan,jenis_sampel:jenis_sampel,hasil_igm_campak:hasil_igm_campak,hasil_igm_rubella:hasil_igm_rubella,nama_penyakit:nama_penyakit}, function(response)
        {
            alert(response.feedback);
            $('#inputhasillab').hide(2000);
            $('#tombol').html('');
            $('#tombol').html('<tombol class="btn btn-medium btn-success" onclick="tampilInputhasillab()">input hasil laboratorium</tombol>');
        });


    }

    function cek(){
        var nama_penyakit = $('#hasil_igm_campak').val();
        if(nama_penyakit==0 && nama_penyakit!='')
        {
           $('#nama_penyakit').removeAttr('disabled','');
        }
        else
        {
           $('#nama_penyakit').attr('disabled','disabled');
        }
    }

    $(document).ready(function(){
            $('#form_save_campak input').keydown(function(e){
             if(e.keyCode==13){       

                if($(':input:eq(' + ($(':input').index(this) + 1) + ')').attr('type')=='submit'){// check for submit button and submit form on enter press
                 return true;
                }

                $(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();

               return false;
             }

            });
        });
    </script>
    <script type="text/javascript">
      $(document).ready(function() {
            $("select").select2();
            $('#example').dataTable();
            $('#div_sampel').hide();
            $('#jenis_sampel').attr('disabled','disabled');
            $('#simpan').attr('disabled','disabled');
            $('#tanggal_ambil_sampel').attr('disabled','disabled');

            $('#form_save_campak1').validate({
                submit: {
            settings: {
                scrollToError: {
                 offset: -100,
                 duration: 500
                }
            }
          }
        });

            $( "#id_kelurahan" ).change(function() {
                $.ajax({
                    data:'id_kelurahan='+$('#id_kelurahan').val()+'&date='+$('.tgl_mulai_rash').val(),
                    url:'{{ URL::to("ambil_epid_campak") }}',
                    success:function(data) {
                $('#epid').val(data);
                    }
                });
            });

            /*
            $( '#form-filter-daftar-kasus' ).submit(function( event ){
                event.preventDefault();
                alert('gagal kirim');
                var form_data = $(this).serialize();
            });
            */

            $('#form_save_campak input').keydown(function(e){

                // check for submit button and submit form on enter press
                if(e.keyCode==13){
                    if($(':input:eq(' + ($(':input').index(this) + 1) + ')').attr('type')=='submit'){
                        return true;
                    }

                    $(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();

                    return false;
                }
            });
      });

      function tambah_sampel(){
        $('#div_sampel').show(2000);
            $('#tambah_sampel').hide();
        }

        function tutup_sampel(){
            $('#div_sampel').hide(2000);
            $('#tambah_sampel').show(2000);
            $('#jenis_sampel').val('');
            $('#tanggal_ambil_sampel').val('');
            $('#jenis_sampel').attr('disabled','disabled');
            $('#simpan').attr('disabled','disabled');
            $('#tanggal_ambil_sampel').attr('disabled','disabled');

        }

        function add_sampel()
        {
            var nama_sampel             = $("#nama_sampel").val();
            var jenis_sampel            = $("#jenis_sampel").val();
            var tanggal_ambil_sampel    = $("#tanggal_ambil_sampel").val();
            if(nama_sampel=='0')
            {
              var nama_sampel_baru = 'Serologi';
              var jenis_sampel_baru=(jenis_sampel==0)?'Darah':'Urin';
            }

            if(nama_sampel=='1')
            {
                var nama_sampel_baru = 'verologi';
                if (jenis_sampel==0) {
                   var jenis_sampel_baru='Urin';
                } 
                else if(jenis_sampel==1)
                {
                   var jenis_sampel_baru='Usap tenggorokan';
                }
                else
                {
                    var jenis_sampel_baru ='Cairan mulut';
                }
            }

            $("#data_sampel").append('<tr valign="top"><td><input id="data_nama_sampel" name="nama_sampel[]" type="hidden" value='+nama_sampel+'>'+nama_sampel_baru+'</td><td><input id="data_jenis_sampel" name="jenis_sampel[]" type="hidden" value='+jenis_sampel+'>'+jenis_sampel_baru+'</td><td><input id="data_tanggal_ambil_sampel" name="tanggal_ambil_sampel[]" type="hidden" value='+tanggal_ambil_sampel+'>'+tanggal_ambil_sampel+'</td><td><a href="javascript:void(0);" class="remCF">Remove</a></td></tr>');
        

        }

        function pilih_sampel()
        {
            if($('#nama_sampel').val()=='0')
            {
                $('#jenis_sampel').html('');
                $('#jenis_sampel').append('<option value="">Pilih</option><option value="0">Darah</option><option value="1">Urin</option>');
                $('#jenis_sampel').removeAttr('disabled','');
            }
            else if($('#nama_sampel').val()=='1')
            {
                $('#jenis_sampel').html('');
                $('#jenis_sampel').removeAttr('disabled','');
                $('#jenis_sampel').append('<option value="">Pilih</option><option value="0">Urin</option><option value="1">Usap tenggorokan</option><option value="2">Cairan mulut</option>');
            }
            else if($('#nama_sampel').val()=='')
            {
                $('#jenis_sampel').attr('disabled','disabled');
                $('#jenis_sampel').html('');
            }
        }

        function jenissampel()
        {
            if ($('#jenis_sampel').val()=='') {
                $('#tanggal_ambil_sampel').attr('disabled','disabled');
                $('#simpan').attr('disabled','disabled');
            }
            else
            {
                 $('#tanggal_ambil_sampel').removeAttr('disabled','');
            }

        }
        $( "#id_kelurahan" ).change(function() {
                $.ajax({
                    data: 'id_kelurahan='+$('#id_kelurahan').val()+'&date='+$('.tgl_mulai_sakit').val(),
                    url: '{{ URL::to("ambil_epid_tetanus") }}',
                    success:function(data) {
                        $('#epid').val(data);
                    }
                });
            });

        $(document).ready(function(){
        $("#tanggal_ambil_sampel").change(function(){
            $('#simpan').removeAttr('disabled','');    
        });
        $("#data_sampel").on('click','.remCF',function(){
            $(this).parent().parent().remove();
        });
    });

        function simpan_sampel(){
            var jenis_spesimen_serologi         = $("#jenis_spesimen_serologi").val();
            var tanggal_ambil_sampel_serologi   = $("#tanggal_ambil_sampel_serologi").val();
            var jenis_spesimen_virologi         = $("#jenis_spesimen_virologi").val();
            var tanggal_ambil_sampel_virologi   = $("#tanggal_ambil_sampel_virologi").val();
            // //kirim data ke server
          
            $.post
                (
                    '{{URL::to("simpan_sampel")}}', 
                    {
                        jenis_spesimen_serologi:jenis_spesimen_serologi,
                        tanggal_ambil_sampel_serologi:tanggal_ambil_sampel_serologi,
                        jenis_spesimen_virologi:jenis_spesimen_virologi,
                        tanggal_ambil_sampel_virologi:tanggal_ambil_sampel_virologi
                    }, 
                    function(response)
                    {
                        alert(response.feedback);
                        $('#inputhasillab').hide(2000);
                        $('#tombol').html('');
                        $('#tombol').html('<tombol class="btn btn-medium btn-success" onclick="tampilInputhasillab()">input hasil laboratorium</tombol>');
                    }
                );
        }
    </script>
@stop