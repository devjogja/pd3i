@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <h4>
                        Data master Laboratorium
                    </h4>
                </div>
                <br>
                <div class="table-responsive">
                  <table class="uk-table uk-table-hover uk-table-striped uk-table-condensed table-bordered">
                    <thead>
                      <tr>
                        <th >No.</th>
                        <th >Nama Laboratorium</th>
                        <th >Alamat</th>
                        <th >Telepon</th>
                        <th>Faxmile</th>
                        <!-- <th >Aksi</th> -->
                      </tr>
                    </thead>
                  <tbody>
                      <?php $no=1; ?>
                      @if($laboratorium)
                      @foreach($laboratorium as $data)
                      <tr>
                        <td>{{$no}}</td>
                        <td>{{$data->nama_laboratorium}}</td>
                        <td>{{$data->alamat}}</td>
                        <td>{{$data->telepon}}</td>
                        <td>{{$data->fax}}</td>
                        <!-- <td>
                          <a class="btn btn-primary btn-xs" href="{{URL::to('laboratorium/edit/'.$data->id_laboratorium)}}"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ubah"></i></a>
                          <a class="btn btn-warning btn-xs" href="{{URL::to('laboratorium/hapus/'.$data->id_laboratorium)}}" onclick="alert('yakin ini menghapus data ini?')"><i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus"></i></a>
                        </td> -->
                      </tr>
                      <?php $no++?>
                      @endforeach
                      @endif
                  </tbody>
                  </table>
                  <ul class="pagination pagination-sm">
                    {{$laboratorium->links()}}
                  </ul>
                </div>
            </div>
            <!--/.module-body-->
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<!--/.span9-->
@stop