@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <h4>Pemeriksaan Lab {{$response['title'];}} </h4> 
                    <hr>
                </div>
                @include($response['content'])
            </div>
        </div>
    </div>
</div>
@stop
