@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <h4>Penyakit {{$response['title']}}</h4> 
                    <hr>
                </div>
                <ul class="profile-tab nav nav-tabs" id="tabs">
                    <li class="active"><a href="#cari_pasien" data-toggle="tab">Cari pasien</a></li>
                    <li><a href="#daftar" data-toggle="tab">Daftar Sampel</a></li>
                    <!-- <li><a href="#analisis" data-toggle="tab">Analisis kasus</a></li> -->
                </ul>
                <div class="profile-tab-content tab-content">
                    <div class="tab-pane fade active in" id="cari_pasien">
                        @include('laboratorium._include.searching')
                    </div>
                    <div class="tab-pane fade in" id="daftar">
                        @include($response['daftarSampel'])
                    </div>
                    <div class="tab-pane fade in" id="analisis">
                        @include('laboratorium._include.analisis')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(function(){
    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
    }
});

$(document).on("shown", '.nav-tabs a[data-toggle="tab"]', function (e) {
    if (e.target=='{{URL::to("/")}}/labs/campak#analisis'){
        $('#tampilkan_analisa').click();
    };
});
</script>

@stop