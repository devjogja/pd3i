@extends('layouts.master')
@section('content')
<div class="span12">
	<div class="content">
		<div class="module">
			<div class="module-body" >
				<div class="profile-head media">
					<h4>Penyakit Campak</h4>
					<hr>
				</div>

			{{-- Tab Navigasi --}}
			<ul class="profile-tab nav nav-tabs">
				<li class="active"><a href="#input_lab" data-toggle="tab">Input detail pemeriksaan hasil laboratorium</a></li>
			</ul>

			{{-- Content Dari Tab --}}
				<div class="profile-tab-content tab-content">
					<div class="tab-pane fade active in" id="input_lab">
					{{Form::open(array('route'=>'simpan_hasil_lab_campak','class'=>'form-horizontal','id'=>'simpan_hasil_lab'))}}
					                <div class="row-fluid">
                            <div class="span6">
                                <div class="media">
                                    <fieldset>
                                        <legend>Informasi spesimen</legend>
                                        <div class="control-group">
                                            <label class="control-label">No. Spesimen</label>
                                            <div class="controls">
                                             {{Form::text('no_spesimen',Input::old('no_spesimen'), array('class' => 'input-medium'))}}
                                            <span class="help-inline"></span>
                                            </div>
                                          </div>
                                          <div class="control-group">
                                            <label class="control-label">Tipe spesimen</label>
                                            <div class="controls">
                                              {{Form::text('tipe_spesimen',Input::old('tipe_spesimen'), array('class' => 'input-medium'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                          <div class="control-group">
                                            <label class="control-label">Sampel lainnya</label>
                                            <div class="controls">
                                              {{Form::text('sampel_lainnya',Input::old('sampel_lainnya'), array('class' => 'input-medium'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                          <div class="control-group">
                                            <label class="control-label">Tanggal pengiriman spesimen ke propinsi</label>
                                            <div class="controls">
                                              {{Form::text('tanggal_pengiriman_spesimen_propinsi',Input::old('tanggal_pengiriman_spesimen_propinsi'), array('class' => 'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                           <div class="control-group">
                                            <label class="control-label">Tanggal pengiriman spesimen ke laboratorium</label>
                                            <div class="controls">
                                              {{Form::text('tanggal_pengiriman_spesimen_laboratorium',Input::old('tanggal_pengiriman_spesimen_laboratorium'), array('class' => 'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                           <div class="control-group">
                                            <label class="control-label">Tanggal terima spesimen di laboratorium</label>
                                            <div class="controls">
                                              {{Form::text('tanggal_terima_spesimen_laboratorium',Input::old('tanggal_terima_spesimen_laboratorium'), array('class' => 'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                           <div class="control-group">
                                            <label class="control-label">Permintaan tes serologi</label>
                                            <div class="controls">
                                              {{Form::text('permintaan_tes_serologi',Input::old('permintaan_tes_ser'), array('class' => 'input-medium'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                          <div class="control-group">
                                            <label class="control-label">Kondisi spesimen waktu di terima di propinsi (diganti laboratorium)</label>
                                            <div class="controls">
                                              {{Form::select('kondisi_spesimen_dipropinsi', array('' => 'Pilih','0'=>'Baik','1'=>'Volum kurang','2' => 'Tidak dingin','3'=>'Kering(lisis)','4'=>'Tube bocor','5'=>'Tidak baik'),Input::old('kondisi_spesimen_dipropinsi'), array('class' => 'input-medium'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                          <div class="control-group">
                                            <label class="control-label">Kondisi spesimen waktu di terima di laboratorium</label>
                                            <div class="controls">
                                              {{Form::select('kondisi_spesimen_dilab', array('' => 'Pilih','0'=>'Adekuat(baik)','1'=>'Non adekuat(tidak baik)'),Input::old('kondisi_spesimen_dilab'), array('class' => 'input-medium'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="media">
                                <fieldset>
                                  <legend>Proses pemeriksaan sampel dan hasil lab</legend>
                                  <div class="control-group">
                                    <label class="control-label">Tanggal pemeriksaan sampel</label>
                                    <div class="controls">
                                      {{Form::text('tanggal_pemeriksaan_sampel',Input::old('tanggal_pemeriksaan_sampel'),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                      <span class="help-inline"></span>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label">Kit ELISA yang digunakan</label>
                                    <div class="controls">
                                      {{Form::text('kit_elisa',Input::old('kit_elisa'),array('class'=>'input-medium'))}}
                                      <span class="help-inline"></span>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label">Lainnya</label>
                                    <div class="controls">
                                      {{Form::text('lainnya',Input::old('lainnya'),array('class'=>'input-medium'))}}
                                      <span class="help-inline"></span>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label">Measles Antigen OD</label>
                                    <div class="controls">
                                      {{Form::text('campak_antigen_OD',Input::old('campak_antigen_OD'),array('class'=>'input-medium'))}}
                                      <span class="help-inline"></span>
                                    </div>
                                    <label class="control-label">Rubella Antigen OD</label>
                                    <div class="controls">
                                      {{Form::text('rubella_antigen_OD',Input::old('rubella_antigen_OD'),array('class'=>'input-medium'))}}
                                      <span class="help-inline"></span>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label">Control Antigen OD</label>
                                    <div class="controls">
                                      {{Form::text('control_antigen_OD',Input::old('control_antigen_OD'),array('class'=>'input-medium'))}}
                                      <span class="help-inline"></span>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label">Tanggal hasil tersedia</label>
                                    <div class="controls">
                                      {{Form::text('tanggal_hasil_tersedia',Input::old('tanggal_hasil_tersedia'),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                      <span class="help-inline"></span>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label">Tanggal hasil dilaporkan</label>
                                    <div class="controls">
                                      {{Form::text('tanggal_hasil_dilaporkan',Input::old('tanggal_hasil_dilaporkan'),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                      <span class="help-inline"></span>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label">Tanggal kirim hasil</label>
                                    <div class="controls">
                                      {{Form::text('tanggal_kirim_hasil',Input::old('tanggal_kirim_hasil'),array('class'=>'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                      <span class="help-inline"></span>
                                    </div>
                                  </div>
                                  <div class="control-group">
                                    <label class="control-label">Hasil pemeriksaan akhir</label>
                                    <div class="controls">
                                    {{Form::select('hasil_pemeriksaan_akhir', array('' => 'Pilih','0' => 'IgM Campak positif','1'=>'IgM Rubella positif','3'=>'Negatif IgM Campak dan Rubella'), Input::old('hasil_pemeriksaan_akhir'), array('class' => 'input-large'))}}
                                    <span class="help-inline"></span>
                                    </div>
                                  </div>
                                  </fieldset>
                                </div>
                            </div>
                        </div>

                        <div class="row-fluid">
                            <div class="span12">
                                <div class="media">
                                    <fieldset>
                                      <legend>Informasi lab rujukan</legend>
                                      <div class="control-group">
                                        <label class="control-label">Regional reference lab</label>
                                        <div class="controls">
                                          {{Form::text('regional_reference_lab',Input::old('regional_reference_lab'),array('class'=>'input-medium'))}}
                                          <span class="help-inline"></span>
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Tanggal pengiriman ke RRL</label>
                                        <div class="controls">
                                          {{Form::text('tanggal_RRL',Input::old('tanggal_RRL'),array('class'=>'input-small','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                          <span class="help-inline"></span>
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Hasil dari RRL</label>
                                        <div class="controls">
                                          {{Form::text('hasil_RRL',Input::old('hasil_RRL'),array('class'=>'input-medium'))}}
                                          <span class="help-inline"></span>
                                        </div>
                                      </div>
                                      <div class="control-group">
                                        <label class="control-label">Tanggal penerimaan hasil dari RRL</label>
                                        <div class="controls">
                                        {{Form::text('tanggal_terima_hasil_RRL',Input::old('tanggal_terima_hasil_RRL'), array('class' => 'input-small','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                        <span class="help-inline"></span>
                                        </div>
                                      </div>
                                      </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                              {{Form::submit('simpan',array('class'=>'btn btn-primary'))}}
                              {{Form::reset('reset',array('class'=>'btn btn-warning'))}}
                              <a href="{{URL::to('input_hasil_lab/'.$no_epid)}}" class="btn btn-danger">kembali</a>
                              <span id="msg"></span>
                              </div>
                          {{Form::close()}}
					</div>
				</div> <!-- /.tab-content-->
			</div> <!--/.module-body-->
		</div> <!--/.module-->
	</div> <!--/.content-->
</div> <!--/.span9-->
@stop