<ul class="profile-tab nav nav-tabs">
	<li class="active"><a href="#inputDataIndividu" data-toggle="tab">Input data individual kasus</a></li>
	<li ><a href="#inputDataPE" data-toggle="tab">Input data PE</a></li>
	<li ><a href="#inputDataLab" data-toggle="tab">Input data lab</a></li>
</ul>
<div class="profile-tab-content tab-content">
	<div class="tab-pane fade active in" id="inputDataIndividu">
		@include('laboratorium.afp.inputkasus')
	</div>
	<div class="tab-pane fade  in" id="inputDataPE">
		@include('laboratorium.afp.inputpe')
	</div>
	<div class="tab-pane fade  in" id="inputDataLab">
		@include('laboratorium.afp.inputlab')
	</div>
</div>