{{ Form::open(array('url' => '','class' => 'form-horizontal')) }}
<div class="module-body">
	<div class="row-fluid">
		<div class="span12">
			<div class="media">
				<fieldset>
					<legend>Pemeriksaan spesimen</legend>
					<div class="row-fluid">
						<div class="span6">
							<fieldset>
								<legend>Spesimen I</legend>
								<div class="control-group">
									<label class="control-label">Apakah spesimen I diperiksa</label>
									<div class="controls">
										{{ Form::select('pemeriksaan_spesimen_I',array(null=>'Pilih','1' => 'Ya','2'=>'Tidak'),null,array('class'=>'input-large pemeriksaan_spesimen_I'))}}
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Tanggal terima spesimen I (stool)</label>
									<div class="controls">
										{{Form::text('tgl_terima_spesimen_I',null,array('class'=>'input-large tgl_terima_spesimen_I','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','disabled'))}}
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Jenis Pemeriksaan Spesimen I</label>
									<div class="controls">
										{{ Form::select('jenis_pemeriksaan_spesimen_I',array(null=>'Pilih','1' => 'Isolasi Virus','2'=>'ITD','3'=>'Sequencing'),null,array('class'=>'input-large jenis_pemeriksaan_spesimen_I','disabled'))}}
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Hasil pemeriksaan spesimen I (stool)</label>
									<div class="controls" id="hasil_spesimen_I">
										{{Form::text('hasil_spesimen_I',null,array('class'=>'input-large hasil_spesimen_I','disabled'))}}
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Tanggal kirim hasil spesimen I (stool)</label>
									<div class="controls">
										{{Form::text('tgl_kirim_hasil_spesimen_I',null,array('class'=>'input-large tgl_kirim_hasil_spesimen_I','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','disabled'))}}
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Keterangan spesimen I (stool)</label>
									<div class="controls">
										{{Form::textarea('keterangan_spesimen_I',null,array('class'=>'input-large keterangan_spesimen_I','disabled','rows'=>'3'))}}
									</div>
								</div>
							</fieldset>
						</div>
						<div class="span6">
							<fieldset>
								<legend>Spesimen II</legend>
								<div class="control-group">
									<label class="control-label">Apakah spesimen II diperiksa</label>
									<div class="controls">
										{{ Form::select('pemeriksaan_spesimen_II',array(null=>'Pilih','1' => 'Ya','2'=>'Tidak'),null,array('class'=>'input-large pemeriksaan_spesimen_II'))}}
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Tanggal terima spesimen II (stool)</label>
									<div class="controls">
										{{Form::text('tgl_terima_spesimen_II',null,array('class'=>'input-large tgl_terima_spesimen_II','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','disabled'))}}
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Jenis Pemeriksaan Spesimen II</label>
									<div class="controls">
										{{ Form::select('jenis_pemeriksaan_spesimen_II',array(null=>'Pilih','1' => 'Isolasi Virus','2'=>'ITD','3'=>'Sequencing'),null,array('class'=>'input-large jenis_pemeriksaan_spesimen_II','disabled'))}}
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Hasil pemeriksaan spesimen II (stool)</label>
									<div class="controls" id="hasil_spesimen_II">
										{{Form::text('hasil_spesimen_II',null,array('class'=>'input-large hasil_spesimen_II','disabled'))}}
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Tanggal kirim hasil spesimen II (stool)</label>
									<div class="controls">
										{{Form::text('tgl_kirim_hasil_spesimen_II',null,array('class'=>'input-large tgl_kirim_hasil_spesimen_II','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','disabled'))}}
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Keterangan spesimen II (stool)</label>
									<div class="controls">
										{{Form::textarea('keterangan_spesimen_II',null,array('class'=>'input-large keterangan_spesimen_II','disabled','rows'=>'3'))}}
									</div>
								</div>
							</fieldset>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
	<div class="form-actions">
		<a style="margin-left:20%;" class="btn btn-primary submitinput">Submit</a>
		<a href="{{URL::to('labs/afp')}}" class="btn btn-danger">Kembali</a>
	</div>
</div>
{{Form::close()}}

<script type="text/javascript">
	$(function(){
		$('.pemeriksaan_spesimen_I').on('click',function(){
			var val = $(this).val();
			if(val=='1'){
				$('.tgl_terima_spesimen_I').removeAttr('disabled');
				$('.jenis_pemeriksaan_spesimen_I').removeAttr('disabled');
				$('.hasil_spesimen_I').removeAttr('disabled');
				$('.tgl_kirim_hasil_spesimen_I').removeAttr('disabled');
				$('.keterangan_spesimen_I').removeAttr('disabled');
			}else{
				$('.tgl_terima_spesimen_I').attr('disabled','disabled');
				$('.jenis_pemeriksaan_spesimen_I').attr('disabled','disabled');
				$('.hasil_spesimen_I').attr('disabled','disabled');
				$('.tgl_kirim_hasil_spesimen_I').attr('disabled','disabled');
				$('.keterangan_spesimen_I').attr('disabled','disabled');

				$('.tgl_terima_spesimen_I').val(null);
				$('.jenis_pemeriksaan_spesimen_I').val(null);
				$('.hasil_spesimen_I').val(null);
				$('.tgl_kirim_hasil_spesimen_I').val(null);
				$('.keterangan_spesimen_I').val(null);
			}
			return false;
		});

		$('.jenis_pemeriksaan_spesimen_I').on('change',function(){
			var val = $(this).val();
			if(val == '1'){
				var hasil = '{{ Form::select('hasil_spesimen_I', array(null=>'--Pilih--','1'=>'P1 Only','2'=>'P2 Only','3'=>'P3','4'=>'NPEV','5'=>'Enterovirus Mixtures'), null, ['class' => 'hasil_spesimen_I input-large']) }}';
				$('#hasil_spesimen_I').html(hasil);
				$('.hasil_spesimen_I').select2();
			}else if(val == '2'){
				$('#hasil_spesimen_I').html(hasil);
				$('.hasil_spesimen_I').select2();
			}else{
				var hasil = '{{Form::text('hasil_spesimen_I',null,array('class'=>'input-large hasil_spesimen_I'))}}';
				$('#hasil_spesimen_I').html(hasil);
				var hasil = '{{ Form::select('hasil_spesimen_I', array(null=>'--Pilih--','1'=>'Type 1 Wild','2'=>'Type 1 Sabin','3'=>'Type 2 Wild','4'=>'Type 2 Sabin','5'=>'Type 3 Wild','6'=>'Type 3 Sabin'), null, ['class' => 'hasil_spesimen_I input-large']) }}';
			}
			return false;
		});

		$('.jenis_pemeriksaan_spesimen_II').on('change',function(){
			var val = $(this).val();
			if(val == '1'){
				var hasil = '{{ Form::select('hasil_spesimen_II', array(null=>'--Pilih--','1'=>'P1 Only','2'=>'P2 Only','3'=>'P3','4'=>'NPEV','5'=>'Enterovirus Mixtures'), null, ['class' => 'hasil_spesimen_II input-large']) }}';
				$('#hasil_spesimen_II').html(hasil);
				$('.hasil_spesimen_II').select2();
			}else if(val == '2'){
				var hasil = '{{ Form::select('hasil_spesimen_II', array(null=>'--Pilih--','1'=>'Type 1 Wild','2'=>'Type 1 Sabin','3'=>'Type 2 Wild','4'=>'Type 2 Sabin','5'=>'Type 3 Wild','6'=>'Type 3 Sabin'), null, ['class' => 'hasil_spesimen_II input-large']) }}';
				$('#hasil_spesimen_II').html(hasil);
			}else{
				$('.hasil_spesimen_II').select2();
				var hasil = '{{Form::text('hasil_spesimen_II',null,array('class'=>'input-large hasil_spesimen_II'))}}';
				$('#hasil_spesimen_II').html(hasil);
			}
			return false;
		});

		$('.pemeriksaan_spesimen_II').on('click',function(){
			var val = $(this).val();
			if(val=='1'){
				$('.tgl_terima_spesimen_II').removeAttr('disabled');
				$('.jenis_pemeriksaan_spesimen_II').removeAttr('disabled');
				$('.hasil_spesimen_II').removeAttr('disabled');
				$('.tgl_kirim_hasil_spesimen_II').removeAttr('disabled');
				$('.keterangan_spesimen_II').removeAttr('disabled');
			}else{
				$('.tgl_terima_spesimen_II').attr('disabled','disabled');
				$('.jenis_pemeriksaan_spesimen_II').attr('disabled','disabled');
				$('.hasil_spesimen_II').attr('disabled','disabled');
				$('.tgl_kirim_hasil_spesimen_II').attr('disabled','disabled');
				$('.keterangan_spesimen_II').attr('disabled','disabled');

				$('.tgl_terima_spesimen_II').val(null);
				$('.jenis_pemeriksaan_spesimen_II').val(null);
				$('.hasil_spesimen_II').val(null);
				$('.tgl_kirim_hasil_spesimen_II').val(null);
				$('.keterangan_spesimen_II').val(null);
			}
			return false;
		});
	});
</script>