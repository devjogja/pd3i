<style type="text/css">
	.pengumpulan_spesimen {
		border-collapse: collapse;
		width: 100%;
	}
	.pengumpulan_spesimen th, td {
		text-align: left;
		padding: 6px;
	}
	.pengumpulan_spesimen tr:nth-child(even){background-color: #f2f2f2}
	.pengumpulan_spesimen th {
		background-color: #4CAF50;
		color: white;
		text-align: left;
	}
</style>
{{ Form::open(array('url' => '','class' => 'form-horizontal')) }}
<div class="module-body">
	<div class="row-fluid">
		<div class="span12">
			<div class="media">
				<fieldset>
					<legend>Pengumpulan spesimen</legend>
					<div class="control-group">
						<label class="control-label">Apakah spesimen diambil</label>
						<div class="controls">
							{{ Form::select('spesimen_diambil',array(null=>'Pilih','1' => 'Ya','2'=>'Tidak'),null,array('class'=>'input-large spesimen','disabled'))}}
						</div>
					</div>
					<div class="control-group">
						<table class="pengumpulan_spesimen">
							<tr>
								<th style="width: 25%;"></th>
								<th style="width: 25%;">Tanggal Ambil</th>
								<th style="width: 25%;">Tanggal Kirim ke kabupaten/kota</th>
								<th style="width: 25%;">Tanggal Kirim ke provinsi</th>
							</tr>
							<tr>
								<td style="text-align: center;font-weight: bold">Spesimen I</td>
								<td>{{Form::text('tanggal_ambil_spesimen_I',null,array('class'=>'input-large tgl_spesimen','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','disabled'))}}</td>
								<td>{{Form::text('tanggal_kirim_spesimen_I_kabupaten',null,array('class'=>'input-large tgl_spesimen','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','disabled'))}}</td>
								<td>{{Form::text('tanggal_kirim_spesimen_I_provinsi',null,array('class'=>'input-large tgl_spesimen','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','disabled'))}}</td>
							</tr>
							<tr>
								<td style="text-align: center;font-weight: bold">Spesimen II</td>
								<td>{{Form::text('tanggal_ambil_spesimen_II',null,array('class'=>'input-large tgl_spesimen','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','disabled'))}}</td>
								<td>{{Form::text('tanggal_kirim_spesimen_II_kabupaten',null,array('class'=>'input-large tgl_spesimen','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','disabled'))}}</td>
								<td>{{Form::text('tanggal_kirim_spesimen_II_provinsi',null,array('class'=>'input-large tgl_spesimen','data-uk-datepicker'=>'{format:"DD-MM-YYYY"}','disabled'))}}</td>
							</tr>
						</table>
					</div>
					<div class="control-group">
						<label class="control-label">Tidak diambil spesimen, berikan alasannya</label>
						<div class="controls">
							{{Form::textarea('catatan_tidak_diambil_spesimen',Input::old('catatan_tidak_diambil_spesimen'),array('rows'=>'4','class'=>'input-xlarge tidak_ambil_spesimen','disabled'))}}
							<span class="help-inline"></span>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
	<div class="form-actions">
		<a style="margin-left:20%;" class="btn btn-primary submitinput">Submit</a>
		<a href="{{URL::to('labs/afp')}}" class="btn btn-danger">Kembali</a>
	</div>
</div>
{{Form::close()}}
