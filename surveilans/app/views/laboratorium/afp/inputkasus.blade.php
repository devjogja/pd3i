<div class="alert alert-error">
	<p style="font-size:12px">Text input yang bertanda bintang <strong>(*)</strong> wajib di isi
</div>
{{
	Form::open(array(
		'url' => '',
		'class' => 'form-horizontal',
		'id'    =>'forminputkasus'
	))
}}

<div class="module-body">
	<div class="row-fluid">
		<div class="span12">
			<div class="media">
				<fieldset>
					<legend>Informasi pasien</legend>
					<div class="row-fluid">
						<div class="span6">
							<div class="control-group">
								<label class="control-label">Nama penderita</label>
								<div class="controls">
									{{
										Form::text(
											'dp[nama_anak]',
											null,
											['data-validation'         =>'[MIXED]',
											'data'                    =>'$ wajib di isi!',
											'data-validation-message' =>'nama penderita wajib di isi!',
											'class'                   => 'input-xlarge',
											'id'                   	=> 'nama_anak',
											'placeholder'             =>'Nama penderita']
											)
									}}
									<span class="help-inline" style="color:red">(*)</span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">NIK</label>
								<div class="controls">
									{{Form::text(
										'dp[nik]',
										null,
										['class' => 'input-medium',
										'id'=>'nik',
										'placeholder'=>'Nomer induk kependudukan']
										)
									}}
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Nama orang tua</label>
								<div class="controls">
									{{Form::text(
										'dp[nama_ortu]',
										null,
										['data-validation'=>'[MIXED]',
											'data'=>'$ wajib di isi!',
											'id'=>'nama_ortu',
											'data-validation-message'=>'nama orang tua wajib di isi!',
											'class' => 'input-xlarge',
											'placeholder'=>'Nama orang tua']
										)
									}}
									<span class="help-inline" style="color:red">(*)</span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Jenis kelamin</label>
								<div class="controls">
									{{Form::SELECT('dp[jenis_kelamin]',
										[null => 'Pilih',
										'1'=>'Laki-laki',
										'2'=>'Perempuan',
										'3' =>'Tidak jelas'],
										null,
										['data-validation'=>'[MIXED]',
											'data'=>'$ wajib di isi!',
											'data-validation-message'=>'jenis kelamin wajib di isi!',
											'class' => 'input-medium id_combobox',
											'id'=>'jenis_kelamin']
										)
									}}
									<span class="help-inline" style="color:red">(*)</span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label"><input type="radio" name="option" onclick="pilihOptionTglLahir()">Tanggal lahir</label>
								<div class="controls">
									{{Form::text(
										'dp[tanggal_lahir]',
										null,
										array(
											'id'=>'tanggal_lahir',
											'class' => 'input-medium tgl_lahir',
											'placeholder'=>'Tanggal lahir',
											'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
											'onchange'=>'umur()',
											'disabled'=>'disabled'
											)
										)
									}}
									<span class="help-inline" style="color:red"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label"><input type="radio" name="option" onclick="pilihOptionUmur()">Umur</label>
								<div class="controls">
									{{Form::text(
										'dp[umur]',
										null,
										array(
											'class' => 'input-mini',
											'id'=>'tgl_tahun',
											'disabled'=>'disabled'
											)
										)
									}} Thn
									{{Form::text(
										'dp[umur_bln]',
										null,
										array(
											'class' => 'input-mini',
											'id'=>'tgl_bulan',
											'disabled'=>'disabled'
											)
										)
									}} Bln
									{{Form::text(
										'dp[umur_hr]',
										null,
										array(
											'class' => 'input-mini',
											'id'=>'tgl_hari',
											'disabled'=>'disabled'
											)
										)
									}} Hr
								</div>
							</div>
						</div>
						<div class="span6">
							<div class="control-group">
								<label class="control-label">Jalan/RT/RW/Dusun</label>
								<div class="controls">
									{{Form::textarea(
										'dp[alamat]',
										null,
										array(
											'rows'=>'3',
											'id'=>'alamat',
											'style'=>'width:270px;',
											'placeholder' => 'Hanya diisi nama jalan, no. rumah dan no. RT - RW'
											)
										)
									}}
									<span class="help-inline" style="color:red"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Cari data wilayah</label>
								<div class="controls">
									{{Form::text('',null,['id'=>'wilayah','class' => 'input-xlarge','placeholder'=>'Ketik nama desa/kecamatan'])}}
									<span class="help-inline" style="color:red">(*)</span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Kelurahan/Desa</label>
								<div class="controls">
									{{Form::text('kelurahan_pasien',null,['id'=>'kelurahan_pasien','class' => 'input-xlarge','placeholder'=>'Kelurahan','readonly'=>'readonly'])}}
									{{Form::hidden('id_kelurahan_pasien',null,['id'=>'id_kelurahan_pasien'])}}
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Kecamatan</label>
								<div class="controls">
									{{Form::text('kecamatan_pasien',null,['id'=>'kecamatan_pasien','class' => 'input-xlarge','placeholder'=>'Kecamatan','readonly'=>'readonly'])}}
									{{Form::hidden('id_kecamatan_pasien',null,['id'=>'id_kecamatan_pasien'])}}
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Kabupaten</label>
								<div class="controls">
									{{Form::text('kabupaten_pasien',null,['id'=>'kabupaten_pasien','class' => 'input-xlarge','placeholder'=>'Kabupaten','readonly'=>'readonly'])}}
									{{Form::hidden('id_kabupaten_pasien',null,['id'=>'id_kabupaten_pasien'])}}
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Provinsi</label>
								<div class="controls">
									{{Form::text('provinsi_pasien',null,['id'=>'provinsi_pasien','class' => 'input-xlarge','placeholder'=>'Provinsi','readonly'=>'readonly'])}}
									{{Form::hidden('id_provinsi_pasien',null,['id'=>'id_provinsi_pasien'])}}
									<span class="help-inline"></span>
								</div>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
	<div class="form-actions">
		<a style="margin-left:20%;" class="btn btn-primary submitinput">Submit</a>
		<a href="{{URL::to('labs/afp')}}" class="btn btn-danger">Kembali</a>
	</div>
</div>
{{Form::close()}}

<style>
	.ui-autocomplete {
		max-height: 220px;
		overflow-y: auto;
		overflow-x: hidden;
	}
</style>

<script type="text/javascript">
	$(function(){
		$('.submitinput').click(function(){
			$("html, body").animate({scrollTop: 0}, 100);
			$('.nav-tabs li:eq(1) a').tab('show');
		});

		$('#wilayah').autocomplete({
			source:"{{URL::to('getArea')}}",
			minLength:2,
			select:function(evt, ui)
			{
				$('#kelurahan_pasien').val(ui.item.kelurahan);
				$('#kecamatan_pasien').val(ui.item.kecamatan);
				$('#kabupaten_pasien').val(ui.item.kabupaten);
				$('#provinsi_pasien').val(ui.item.provinsi);
				$('#id_kelurahan_pasien').val(ui.item.id_kelurahan);
				$('#id_kecamatan_pasien').val(ui.item.id_kecamatan);
				$('#id_kabupaten_pasien').val(ui.item.id_kabupaten);
				$('#id_provinsi_pasien').val(ui.item.id_provinsi);
				$('#wilayah').val(null);
				return false;
			}
		})
		.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
			return $("<li>")
			.append("<a>"
				+item.kelurahan +"<br><small>Kelurahan: <i>"
				+item.kelurahan +"</i><br>Kecamatan: "
				+item.kecamatan+"<br>Kabupaten: "
				+item.kabupaten+"<br>Provinsi: "
				+item.provinsi+"</small></a>")
			.appendTo( ul );
		};
	});
</script>