{{
	Form::open(array(
		'url' => '',
		'class' => 'form-horizontal',
		'id'    =>'forminputlab'
	))
}}
<div class="module-body" id="inputlab">
	<div class="profile-tab-content tab-content">
		<div class="tab-pane fade active in" id="input_lab">
			<div class="row-fluid">
				<div class="span6">
					<div class="media">
						<fieldset>
							<legend>Informasi Outbreak / Geografik</legend>
							<div class="control-group">
								<label class="control-label">Sumber Spesimen</label>
								<div class="controls">
									{{Form::select('do[sumber_spesimen]',
										[
										null=>'Pilih',
										"KLB"=>'KLB',
										"CBMS"=>'CBMS',
										],
										null,
										['class' => 'input-medium',
										'id'=>'sumberSpesimen'
										])
									}}
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">ID outbreak</label>
								<div class="controls">
									{{
										Form::text('dc[id_outbreak]',
										null,
										['class' => 'input-large','id'=>'id_outbreak'])
									}}
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Dilaporkan Oleh</label>
								<div class="controls">
									{{Form::select('do[dilaporkan_oleh]',
										[
										''=>'Pilih',
										'Puskesmas'=>'Puskesmas',
										'Klinik/Balai Pengobatan'=>'Klinik/Balai Pengobatan',
										'Rumah Sakit'=>'Rumah Sakit',
										'Praktek Swasta'=>'Praktek Swasta',
										'SMO/SO'=>'SMO/SO',
										'Lainnya'=>'Lainnya',
										'Tidak Diketahui'=>'Tidak Diketahui',
										],
										null,
										['class' => 'input-medium',
										'id'=>'dilaporkan_oleh'])
									}}
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Provinsi</label>
								<div class="controls">
									{{
										Form::select(
											'do[id_provinsi_outbreak]',
											array('' => 'Pilih provinsi')+ Provinsi::lists('provinsi','id_provinsi'),
											null,
											array(
												'id'       => 'id_provinsi_outbreak',
												'onchange' => 'show_kabupaten("_outbreak")',
												'class'    => 'input-large id_combobox'
											)
										)
									}}
									<span class="help-inline" id="error-id_provinsi_pasien"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Kabupaten</label>
								<div class="controls">
									{{
										Form::select(
											'do[id_kabupaten_outbreak]',
											array('' => 'Pilih kabupaten'),
											null,
											array(
												'class'    => 'input-large id_combobox',
												'id'       => 'id_kabupaten_outbreak',
												'onchange' => 'show_kecamatan("_outbreak")'
											)
										)
									}}
									<span class="lodingKab"></span>
									<span class="help-inline" id="error-id_kabupaten_pasien"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Rumah sakit</label>
								<div class="controls">
									{{
										Form::select(
											'do[id_rs_outbreak]',
											array('' => 'Pilih Rumah sakit'),
											null,
											array(
												'class'    => 'input-large id_combobox',
												'id'       => 'id_rs_outbreak',
												'disabled' => 'disabled'
											)
										)
									}}
									<span class="loadingrs"></span>
									<span class="help-inline" id="error-id_kabupaten_pasien"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Kecamatan</label>
								<div class="controls">
									{{
										Form::select(
											'do[id_kecamatan_outbreak]',
											array(
												'' => 'Pilih kecamatan'
											),
											null,
											array(
												'class'    => 'input-large id_combobox',
												'id'       => 'id_kecamatan_outbreak',
												'onchange' => 'show_kelurahan("_outbreak")'
											)
										)
									}}
									<span class="lodingKec"></span>
									<span class="help-inline" id="error-id_kecamatan_pasien"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Kelurahan/Desa</label>
								<div class="controls">
									{{
										Form::select(
											'do[id_kelurahan_outbreak]',
											array(
												'' => 'Pilih kelurahan/desa'
											),
											null,
											array(
												'class' => 'input-large id_combobox',
												'id'    => 'id_kelurahan_outbreak',
												'onchange'=>'show_puskesmas("_outbreak")'
											)
										)
									}} 
									<span class="lodingKel"></span>
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Puskesmas</label>
								<div class="controls">
									{{
										Form::select(
											'do[id_puskesmas_outbreak]',
											array(
												'' => 'Pilih Puskesmas'
											),
											null,
											array(
												'class'    => 'input-large id_combobox',
												'id'       => 'id_puskesmas_outbreak',
												'disabled' => 'disabled'
											)
										)
									}}
									<span class="lodingPus"></span>
									<span class="help-inline" id="error-id_kecamatan_pasien"></span>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="span6">
					<div class="media">
						<fieldset>
							<legend>Informasi spesimen</legend>
							<div class="control-group">
								<label class="control-label">No. Laboratorium</label>
								<div class="controls">
									{{Form::text('do[no_spesimen]',null, ['class' => 'input-medium','id'=>'no_spesimen'])}}
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Tanggal Terima Spesimen</label>
								<div class="controls">
									{{Form::text('do[tgl_terima_spesimen]',null, [
										'class' => 'input-medium',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
										'onchange'=>'noSpesimen()',
										'placeholder'=>'(DD-MM-YYYY)',
										'id'=>'tglTerimaSpesimen'])}}
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Kondisi spesimen waktu di terima di laboratorium</label>
								<div class="controls">
									{{Form::select('do[kondisi_spesimen_dilab]',
										[
										''=>'Pilih',
										'Baik'=>'Baik',
										'Volum Kurang'=>'Volum Kurang',
										'Tidak Dingin'=>'Tidak Dingin',
										'Lisis'=>'Lisis',
										'Bocor/Tumpah'=>'Bocor/Tumpah',
										'Tidak Baik'=>'Tidak Baik',
										],
										null,
										['class' => 'input-medium',
										'id'=>'kondisi_spesimen_dilab'
										])
									}}
									<span class="help-inline"></span>
								</div>
							</div>
							{{-- <div class="control-group">
								<label class="control-label">Kondisi spesimen waktu di terima di laboratorium</label>
								<div class="controls">
									{{Form::select('do[kondisi_spesimen_waktu_dilab]',
										[
										''=>'Pilih',
										'1'=>'Adekuat (baik)',
										'2'=>'Non Adekuat (tidak baik)',
										],
										null,
										['class' => 'input-medium'])
									}}
									<span class="help-inline"></span>
								</div>
							</div> --}}
						</fieldset>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="media">
						<fieldset>
							<legend>Pemeriksaan laboratorium</legend>
							<table class="table table-bordered" id="data_sampel">
								<thead>
									<tr>
										<td>No.Spesimen</td>
										<td>Jenis pemeriksaan</td>
										<td>Tipe Spesimen</td>
										<td>Tanggal</td>
										<td>Sampel Ke-</td>
										<td>Hasil</td>
										<td style="width:25%">Aksi</td>
									</tr>
								</thead>
								<tbody id="listSampel">
								</tbody>
							</table> 
							<br>
								<a class="btn btn-success" onclick="tambah_sampel()" id="tambah_sampel">Tambah sampel</a>
							<br><br>
							<div id="div_sampel">
								<table class="table table-bordered">
									<tr>
										<td>Jenis pemeriksaan</td>
										<td>Tipe Spesimen</td>
										<td>Tanggal</td>
										<td>Sampel Ke-</td>
									</tr>
									 <tr>
										<td>
										{{Form::select(null,
											[
												''=>'Pilih',
												'Serologi'=>'Serologi',
												'Virologi'=>'Virologi'
											],
											null,
											['class' => 'input-medium',
											'id'=>'jenis_sampel'
											])
										}}
										</td>
										<td>
										{{Form::select(null,
											[
												''=>'Pilih',
												'Serum'=>'Serum',
												'Urin'=>'Urin',
												'ThroatSwab'=>'ThroatSwab'
											],
											null,
											['class' => 'input-medium',
											'id'=>'tipeSpesimen'
											])
										}}
										</td>
										<td>
											<table>
												<tr>
													<td>Pengambilan Sampel</td>
													<td>
														{{
															Form::text(null,
															null,
															[
																'class' => 'input-medium',
																'id'=>'ambilSampel',
																'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
															])
														}}
													</td>		                                			
												</tr>
												<tr>
													<td>Pengiriman Sampel ke Lab</td>
													<td>
														{{
															Form::text(null,
															null,
															[
																'class' => 'input-medium',
																'id'=>'kirimSampel',
																'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
															])
														}}
													</td>
												</tr>
												<tr>
													<td>Terima Sampel</td>
													<td>
														{{
															Form::text(null,
															null,
															[
																'class' => 'input-medium',
																'id'=>'terimaSampel',
																'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
															])
														}}
													</td>
												</tr>
											</table>
										</td>
										<td>
										{{Form::select(null,
											[
												'1'=>'1',
												'2'=>'2',
												'3'=>'3',
												'4'=>'4',
												'5'=>'5',
											],
											null,
											['class' => 'input-medium',
											'id'=>'sample_ke'
											])
										}}
										</td>
									</tr>
								</table>
								<input type="button" class="btn btn-primary" style="margin-left:400px;margin-top:30px"  onclick="add_sampel()" value="Tambah">
								<input type="button" class="btn btn-warning" style="margin-top:30px"  onclick="tutup_sampel()" value="Tutup">
							</div>
							<div class="control-group" style="border-top: 1px solid #f5f5f5;padding-top: 15px;">
								<label class="control-label">Klasifikasi final</label>
								<div class="controls">
									{{Form::select(
										'dc[klasifikasi_final]',
										[
											'0' => 'Pilih',
											'1' => 'Campak (Lab)',
											'2'=>'Campak (Epid)',
											'3'=>'Campak (Klinis)',
											'4'=>'Rubella',
											'5'=>'Bukan campak/rubella',
											'6'=>'Pending'
										],
										'', 
										array(
											'class' => 'input-medium id_combobox',
											'id'=>'klasifikasiFinal'
											)
										)
									}}
									<span class="help-inline"></span>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="form-actions">
				{{Form::submit('simpan',array('class'=>'btn btn-primary','id'=>'submitinputlab','style'=>'margin-left:20%'))}}
				{{Form::reset('reset',array('class'=>'btn btn-warning resetForm'))}}
				<a href="{{URL::to('labs/campak')}}" class="btn btn-danger">kembali</a>
			</div>	
		</div>
	</div>
</div>
{{Form::close()}}

<input type="hidden" name="dp[tglPeriksa]" value="{{(empty($dt))?'':$dt->campak_tanggal_periksa}}" class="tglPeriksa">

<script type="text/javascript">
$(function(){
	$("#div_sampel").hide();
	$("#inputserologi").hide();
	$("#inputvirologi").hide();
	$("#data_sampel").on('click','.remove',function(){
		var id = $(this).parent().parent().find('.id_sampel').val();
		delete_sampel(id);
		$(this).parent().parent().remove();
	});
	$('#data_sampel').on('click','.inputSerologi',function(){
		$("html, body").animate({scrollTop: 0}, 500);
		$('#inputlab').hide(500);
		$('#inputserologi').show(500);
		tutup_sampel();
		id_sampel = $(this).parent().parent().find('.id_sampel').val();
		sampel_ke = $(this).parent().parent().find('.sampel_ke').html();
		jenis_sampel = $(this).parent().parent().find('.jenis_sampel').html();
		noSpesimen = $(this).parent().parent().find('.noSpesimen').html();
		ambilSampel = $(this).parent().parent().find('.ambilSampel').html();
		kirimSampel = $(this).parent().parent().find('.kirimSampel').html();
		terimaSampel = $(this).parent().parent().find('.terimaSampel').html();
		typeSpesimen = $(this).parent().parent().find('.type_spesimen').html();
		var push = [sampel_ke,noSpesimen,ambilSampel,kirimSampel,terimaSampel,typeSpesimen,jenis_sampel,id_sampel];
		detail($(this).attr('id'), push);
	});
	$('#data_sampel').on('click','.inputVirologi',function(){
		$("html, body").animate({scrollTop: 0}, 500);
		$('#inputlab').hide(500);
		$('#inputvirologi').show(500);
		tutup_sampel();
		id_sampel = $(this).parent().parent().find('.id_sampel').val();
		sampel_ke = $(this).parent().parent().find('.sampel_ke').html();
		jenis_sampel = $(this).parent().parent().find('.jenis_sampel').html();
		noSpesimen = $(this).parent().parent().find('.noSpesimen').html();
		ambilSampel = $(this).parent().parent().find('.ambilSampel').html();
		kirimSampel = $(this).parent().parent().find('.kirimSampel').html();
		terimaSampel = $(this).parent().parent().find('.terimaSampel').html();
		typeSpesimen = $(this).parent().parent().find('.type_spesimen').html();
		var push = [sampel_ke,noSpesimen,ambilSampel,kirimSampel,terimaSampel,typeSpesimen,jenis_sampel,id_sampel];
		detail($(this).attr('id'), push);
	});
	$('#submitinputlab').click(function(){
		var url = '{{URL::to("lab/storelab")}}';
		var data = $('#forminputkasus').serialize()+'&'+$('#forminputlab').serialize();
		$.ajax({
			type: 'POST',
			url: url,
			data: data,
			success: function(response) {
				window.location.href = '{{URL::to("labs/campak")}}#daftar';
			}
		});
		return false;
	});
});

function delete_sampel(id) {
	var url = '{{URL::to("deletesampel/'+id+'")}}';
	$.ajax({
		type:'GET',
		url: url,
		success: function(){
			return 'succes';
		}
	});
	return false;
}

function detail(id, push) {
	$('.nama_pasien').html($('#nama_anak').val());
	$('.epid').html($('#epid').val());
	$('.no_outbreak').html($('#id_outbreak').val());
	$('.no_spesimen').html(push[1]);
	$('.seq').val(id);
	$('.sampel_ke').val(push[0]);
	$('.noSpesimenSerologi').val(push[1]);
	$('.ambil_sampel').val(push[2]);
	$('.kirim_sampel').val(push[3]);
	$('.terima_sampel').val(push[4]);
	$('.id_spesimen').select2('val',push[5]);
	$('.jenis_sampel').val(push[5]);
	var id_sampel = push[6];
	$.ajax({
		method: "POST",
		url: "{{URL::to('getDetailSampel')}}",
		data: {id_sampel:id_sampel},
	})
	.done(function(response){
		if (response.data) {
			
		};
	});
}

function tambah_sampel(){
	$('#div_sampel').show(500);
	$('#tambah_sampel').hide(500);
}

function tutup_sampel(){
	$('#div_sampel').hide(500);
	$('#tambah_sampel').show(500);
	$("#jenis_sampel").select2("val", "");
	$("#tipeSpesimen").select2("val", "");
	$("#sample_ke").select2("val", "");
	$("#ambilSampel").val('');
	$("#kirimSampel").val('');
	$("#terimaSampel").val('');
}

function add_sampel() {
	var jenisSampel = $('#jenis_sampel').val();
	var sample_ke = $('#sample_ke').val();
	var type_spesimen = $('#tipeSpesimen').val();
	var ambilSampel = $('#ambilSampel').val();
	var kirimSampel = $('#kirimSampel').val();
	var terimaSampel = $('#terimaSampel').val();
	if (type_spesimen=='Serum'){
		var typeSpesimen = 'SR';
	}else if(type_spesimen=='Urin'){
		var typeSpesimen = 'U';
	}else if(type_spesimen=='ThroatSwab'){
		var typeSpesimen = 'TS';
	}else{
		var typeSpesimen = '';
	};
	var noSpesimen = $('#no_spesimen').val()+'/'+typeSpesimen;
	var push = [noSpesimen,jenisSampel,type_spesimen,ambilSampel,kirimSampel,terimaSampel,sample_ke,'',''];
	sampelCreate(push);
}

function sampelCreate(push){
	$("#listSampel").append('<tr><input type="hidden" name="sampel_lab[]" class="id_sampel" value="'+push[8]+'"><td><span class="noSpesimen">'+push[0]+'</span></td><td><span class="jenis_sampel">'+push[1]+'</span></td><td><span class="type_spesimen">'+push[2]+'</span></td><td><table><tr><td>Ambil Sampel</td><td><span class="ambilSampel">'+push[3]+'</span></td></tr><tr><td>Kirim Sampel</td><td><span class="kirimSampel">'+push[4]+'</span></td></tr><tr><td>Terima Sampel</td><td><span class="terimaSampel">'+push[5]+'</span></td></tr></table></td><td><span class="sampel_ke">'+push[6]+'</span></td><td><span class="hasil">'+push[7]+'</span></td><td><input type="button" class="btn btn-primary sampel_id input'+push[1]+'" value="Input Data Pemeriksaan"><input type="button" class="btn btn-warning remove" value="Remove"></td></tr>');
	$('.sampel_id').attr('id',function(i){return 'push'+(i+1)});
	$('.hasil').attr('id',function(i){return 'push'+(i+1)});
	$('.id_sampel').attr('id',function(i){return 'id_sampel_push'+(i+1)});
	//clear input
	$("#jenis_sampel").select2("val", "");
	$("#tipeSpesimen").select2("val", "");
	$("#tipeSpesimen").select2("val", "");
	$("#sampel_ke").select2("val", "");
	$("#ambilSampel").val('');
	$("#kirimSampel").val('');
	$("#terimaSampel").val('');
}

function cleardetail(){
	$('.nama_pasien').html('');
	$('.epid').html('');
	$('.no_outbreak').html('');
	$('.no_spesimen').html('');
}
</script>