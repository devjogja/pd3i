@extends('layouts.master')
@section('content')

<style type="text/css" media="screen">
	.heads{
		width: 24%;
	}	
</style>

<div class="span12">
	<div class="content">
		<div class="module">
			<div class="module-body">
				<div class="profile-head media">
					<h4>
						Detail Hasil Laboratorium
					</h4>
					<hr>
				</div>
				@if($dt)
				<div class="module-body uk-overflow-container">
					<div class="table-responsive">
						<table class="table table-striped">
						<caption style="border:1px solid #eee;background-color:#666; color:#fff">Informasi Pasien</caption>
							<tr>
								<td class='heads'>Nama penderita</td>
								<td>{{$dt->pasien_nama_anak}}</td>
							</tr>
							<tr>
								<td class='heads'>NIK</td>
								<td>{{$dt->pasien_nik}}</td>
							</tr>
							<tr>
								<td class='heads'>Nama orang tua</td>
								<td>{{$dt->pasien_nama_ortu}}</td>
							</tr>
							<tr>
								<td>Jenis kelamin</td>
								@if($dt->pasien_jenis_kelamin=='1')
								<td>Laki-laki</td>
								@elseif($dt->pasien_jenis_kelamin=='2')
								<td>Perempuan</td>
								@elseif($dt->pasien_jenis_kelamin=='3')
								<td>Tidak diketahui</td>
								@endif
							</tr>
							<tr>
								<td class='heads'>Tanggal Lahir</td>
								<td>{{Helper::getDate($dt->pasien_tanggal_lahir)}}</td>
							</tr>
							<tr>
								<td class='heads'>Umur</td>
								<td>{{$dt->pasien_umur}} Th {{$dt->pasien_umur_bln}} Bln {{$dt->pasien_umur_hr}} Hari</td>
							</tr>
							<tr>
								<td class='heads'>Alamat</td>
								<td>{{$dt->alamat_new}}</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="module-body uk-overflow-container">
					<div class="table-responsive">
						<table class="table table-striped">
						<caption style="border:1px solid #eee;background-color:#666; color:#fff">Data Klinis Campak</caption>
							<tr>
								<td class='heads'>No Epidemologi</td>
								<td>{{$dt->campak_no_epid}}</td>
							</tr>
							<tr>
								<td class='heads'>No Epidemologi Lama</td>
								<td>{{$dt->campak_no_epid_lama}}</td>
							</tr>
							<tr>
								<td class='heads'>Tanggal imunisasi campak terakhir</td>
								<td>{{Helper::getDate($dt->campak_tanggal_imunisasi_terakhir)}}</td>
							</tr>
							<tr>
								<td class='heads'>Imunisasi campak sebelum sakit</td>
								<td>{{$dt->campak_vaksin_campak_sebelum_sakit}} X</td>
							</tr>
							<tr>
								<td class='heads'>Tanggal mulai demam</td>
								<td>{{Helper::getDate($dt->campak_tanggal_timbul_demam)}}</td>
							</tr>
							<tr>
								<td class='heads'>Tanggal mulai rash</td>
								<td>{{Helper::getDate($dt->campak_tanggal_timbul_rash)}}</td>
							</tr>
							<tr>
								<td class='heads'>Gejala / tanda lainnya</td>
								<?php
									$gejalaLain = DB::SELECT("
											SELECT
												GROUP_CONCAT(b.nama SEPARATOR ', ') AS gejalaLain
											FROM gejala_campak AS a
											JOIN daftar_gejala_campak AS b ON a.id_daftar_gejala_campak=b.id
											WHERE
											a.id_campak='$dt->campak_id_campak'
										");
									$gejalaLain = (empty($gejalaLain))?'':$gejalaLain[0]->gejalaLain;
								?>
								<td>{{$gejalaLain}}</td>
							</tr>
							<tr>
								<td class='heads'>Komplikasi</td>
								<?php
									$komplikasi = DB::SELECT("
											SELECT komplikasi
											FROM komplikasi AS a
											WHERE
											a.id_campak='$dt->campak_id_campak'
										");
								?>
								<td>
								@foreach($komplikasi as $key=>$val)
									@if($val->komplikasi=='0')
										Diare, 
									@elseif($val->komplikasi=='1')
										Pneumonia, 
										@elseif($val->komplikasi=='2')
										Bronchopneumonia, 
									@elseif($val->komplikasi=='3')
										OMA, 
									@elseif($val->komplikasi=='4')
										Ensefalitis.
									@endif
								@endforeach
								</td>
							</tr>
							<tr>
								<td class='heads'>Status Vaksinasi</td>
								<td>{{$dt->campak_status_vaksinasi}}</td>
							</tr>
							<tr>
								<td class='heads'>Dosis</td>
								<td>{{$dt->campak_dosis}}</td>
							</tr>
							<tr>
								<td class='heads'>Riwayat Berpergian</td>
								<td>
									{{$dt->campak_riwayat_pergi}}
									@if($dt->campak_riwayat_pergi=='Ya')
									, Tujuan : {{$dt->campak_tujuan_riwayat_pergi}}
									@endif
								</td>
							</tr>
							<tr>
								<td class='heads'>Tanggal laporan diterima</td>
								<td>{{Helper::getDate($dt->campak_tanggal_laporan_diterima)}}</td>
							</tr>
							<tr>
								<td class='heads'>Tanggal pelacakan</td>
								<td>{{Helper::getDate($dt->campak_tanggal_pelacakan)}}</td>
							</tr>
							<tr>
								<td class='heads'>Diberi vitamin A</td>
								<td>{{$dt->campak_vitamin_A}}</td>
							</tr>
							<tr>
								<td class='heads'>Keadaan akhir</td>
								@if($dt->campak_keadaan_akhir=='1')
									<td>Hidup</td>
								@elseif($dt->campak_keadaan_akhir=='2')
									<td>Meninggal</td>
								@else
									<td>-</td>
								@endif
							</tr>
							<tr>
								<td class='heads'>Jenis kasus</td>
								<td>{{$dt->campak_jenis_kasus}}</td>
							</tr>
							<tr>
								<td class='heads'>KLB Ke</td>
								<td>{{$dt->campak_klb_ke}}</td>
							</tr>
							<tr>
								<td class='heads'>Status kasus</td>
								@if($dt->campak_status_kasus=='1')
									<td>Index</td>
								@elseif($dt->campak_status_kasus=='2')
									<td>Bukan Index</td>
								@else
									<td>-</td>
								@endif
							</tr>
						</table>
					</div>
				</div>
				<div class="module-body uk-overflow-container">
					<div class="table-responsive">
						<table class="table table-striped">
						<caption style="border:1px solid #eee;background-color:#666; color:#fff">Informasi Outbreak / Geografik</caption>
							<tr>
								<td class='heads'>Sumber Spesimen</td>
								<td>{{$dt->lab_sumber_spesimen}}</td>
							</tr>
							<tr>
								<td class='heads'>ID outbreak</td>
								<td>{{$dt->campak_id_outbreak}}</td>
							</tr>
							<tr>
								<td class='heads'>Dilaporkan Oleh</td>
								<td>{{$dt->lab_dilaporkan_oleh}}</td>
							</tr>
							<tr>
								<td class='heads'>Faskes</td>
								@if($dt->lab_id_rs_outbreak!=NULL)
									<?php
										$faskes = DB::SELECT("
												SELECT CONCAT_WS(', ', a.nama_faskes, a.desa, a.kecamatan, a.kabupaten, a.provinsi) AS faskes
												FROM rumahsakit2 AS a
												WHERE a.id='$dt->lab_id_rs_outbreak'
											");
										$faskes = (empty($faskes))?'':$faskes[0]->faskes;
									?>
								@else
									<?php
										$faskes = DB::SELECT("
												SELECT CONCAT_WS(', ', a.puskesmas_name, b.kelurahan, b.kecamatan, b.kabupaten, b.provinsi) AS faskes
												FROM puskesmas AS a
												JOIN located AS b ON a.kode_desa=b.id_kelurahan
												WHERE a.puskesmas_id='$dt->lab_id_puskesmas_outbreak'
											");
										$faskes = (empty($faskes))?'':$faskes[0]->faskes;
									?>
								@endif
								<td>{{$faskes}}</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="module-body uk-overflow-container">
					<div class="table-responsive">
						<table class="table table-striped">
						<caption style="border:1px solid #eee;background-color:#666; color:#fff">Informasi Spesimen</caption>
							<tr>
								<td class='heads'>No. Laboratorium</td>
								<td>{{$dt->lab_no_spesimen}}</td>
							</tr>
						</table>
					</div>
				</div>
				{{-- <div class="module-body uk-overflow-container">
					<div class="table-responsive">
						<table class="table table-striped">
						<caption style="border:1px solid #eee;background-color:#666; color:#fff">Pemeriksaan Laboratorium</caption>
							<tr>
								<td>No Spesimen</td>
								<td>{{$dt->campak_no_epid}}</td>
							</tr>
							<tr>
								<td>Jenis Pemeriksaan</td>
								<td>{{$dt->campak_no_epid}}</td>
							</tr>
							<tr>
								<td>Tipe Spesimen</td>
								<td>{{$dt->campak_no_epid}}</td>
							</tr>
							<tr>
								<td>Tanggal</td>
								<td>{{$dt->campak_no_epid}}</td>
							</tr>
							<tr>
								<td>Sampel Ke-</td>
								<td>{{$dt->campak_no_epid}}</td>
							</tr>
							<tr>
								<td>Hasil</td>
								<td>{{$dt->campak_no_epid}}</td>
							</tr>
						</table>
					</div>
				</div> --}}
				<div class="module-body uk-overflow-container">
					<div class="table-responsive">
						<table class="table table-striped">
						<caption style="border:1px solid #eee;background-color:#666; color:#fff">Klasifikasi Final</caption>
							<tr>
								<td class='heads'>Klasifikasi Final</td>
								@if($dt->campak_klasifikasi_final=='1')
									<td>Campak (Lab)</td>
								@elseif($dt->campak_klasifikasi_final=='2')
									<td>Campak (Epid)</td>
								@elseif($dt->campak_klasifikasi_final=='3')
									<td>Campak (Klinis)</td>
								@elseif($dt->campak_klasifikasi_final=='4')
									<td>Rubella</td>
								@elseif($dt->campak_klasifikasi_final=='5')
									<td>Bukan campak/rubella</td>
								@elseif($dt->campak_klasifikasi_final=='6')
									<td>Pending</td>	
								@endif
							</tr>
						</table>
					</div>
				</div>
				@endif
			</div>
		</div>
		<div class="form-actions" style="text-align:center">
			<a href="{{URL::to('labs/campak')}}" class="btn btn-warning">Kembali</a>
		</div>
	</div>
</div>
@stop