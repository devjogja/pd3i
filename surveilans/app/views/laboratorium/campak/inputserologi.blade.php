<div class="module-body" id="inputserologi">
    <div class="profile-head media">
        <h4>Pemeriksaan Serologi</h4>
        <hr>
    </div>
    {{
	  Form::open(array(
	    'url' => '',
	    'class' => 'form-horizontal',
	    'id'    =>'forminputserologi'
	  ))
	}}
	<input type="hidden" class="seq" value="">
	<input type="hidden" name="sr[jenis_pemeriksaan]" class="jenis_periksa" value="Serologi">
	<input type="hidden" name="sr[sampel_ke]" class="sampel_ke" value="">
	<input type="hidden" name="sr[jenis_sampel]" class="jenis_sampel" value="">
	<input type="hidden" name="sr[no_spesimen]" class="noSpesimenSerologi" value="">
	<input type="hidden" name="sr[ambil_sampel]" class="ambil_sampel" value="">
	<input type="hidden" name="sr[kirim_sampel]" class="kirim_sampel" value="">
	<input type="hidden" name="sr[terima_sampel]" class="terima_sampel" value="">
    <div class="module-body">
		<div class="row-fluid">
			<div class="span12">
				<div class="media">
					<fieldset>
						<legend>Data Dasar Spesimen</legend>
						<table class="table" style="width:100%;">
							<tr>
								<td style="text-align:right;width:16%;">Nama :</td>
								<td style="width:25%;"><span class='nama_pasien'></span></td>
								<td style="text-align:right;width:16%;">Tanggal Pemeriksaan IgM Campak:</td>
								<td style="width:25%;"><span class='tglPeriksaCampak'></span></td>
							</tr>
							<tr>
								<td style="text-align:right;width:16%;">No.EPID :</td>
								<td style="width:25%;"><span class='epid'></span></td>
								<td style="text-align:right;width:16%;">Tanggal Pemeriksaan IgM Rubella:</td>
								<td style="width:25%;"><span class='tglPeriksaRubella'></span></td>
							</tr>
							<tr>
								<td style="text-align:right;width:16%;">No.Outbreak :</td>
								<td style="width:25%;"><span class='no_outbreak'></span></td>
								<td style="text-align:right;width:16%;"></td>
								<td style="width:25%;"></td>
							</tr>
							<tr>
								<td style="text-align:right;width:16%;">No.Laboratorium :</td>
								<td style="width:25%;"><span class='no_spesimen'></span></td>
								<td style="text-align:right;width:16%;"></td>
								<td style="width:25%;"></td>
							</tr>
							<tr>
								<td style="text-align:right;width:16%;"></td>
								<td style="width:25%;"></td>
								<td style="text-align:right;width:16%;"></td>
								<td style="width:25%;"></td>
							</tr>
						</table>
					</fieldset>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span6">
				<div class="media">
					<fieldset>
						<legend>Igm Campak</legend>
						<div class="control-group">
							<label class="control-label">Tanggal Pemeriksaan Sampel</label>
							<div class="controls">
								{{Form::text(
									'sr[tgl_pemeriksaan_sampel_campak]',
									Input::old('sr[tgl_pemeriksaan_sampel_campak]'),
									array(
										'class'=>'input-medium',
										'id'=>'tglPeriksaCampak',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Kit Elisa yang digunakan </label>
							<div class="controls">
								{{Form::select(
									'sr[kit_elisa_campak]',
									array('' => 'Pilih',
										'Siemens' => 'Siemens',
										'Lainnya'=>'Lainnya',
										),
									null,
									array(
										'class' => 'input-small',
										'id'=>'kitElisa_campak',
										'onchange'=>'kitElisa("_campak")'
										)
									)
								}}
								{{Form::text(
									'sr[kit_elisa_lain_campak]',
									Input::old('sr[kit_elisa_lain_campak]'),
									array(
										'disabled'=>'disabled',
										'class'=>'input-medium kitElisaLain_campak',
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						{{-- <div class="control-group">
							<label class="control-label">Measles Antigen OD</label>
							<div class="controls">
								{{Form::text(
									'sr[measles_antigen_od_campak]',
									Input::old('sr[measles_antigen_od_campak]'),
									array(
										'class'=>'input-medium',
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div> --}}
						{{-- <div class="control-group">
							<label class="control-label">Control Antigen OD</label>
							<div class="controls">
								{{Form::text(
									'sr[control_antigen_od_campak]',
									Input::old('sr[control_antigen_od_campak]'),
									array(
										'class'=>'input-medium',
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div> --}}
						<div class="control-group">
							<label class="control-label">Hasil IgM Campak</label>
							<div class="controls">
								{{Form::select(
									'sr[hasil_igm_campak]',
									array('' => 'Pilih',
										'Positif' => 'Positif',
										'Negatif'=>'Negatif',
										'Equivocal'=>'Equivocal',
										),
									null,
									array(
										'class' => 'input-medium id_combobox',
										'id'=>'hasilIgmCampak',
										'onchange'=>'cekHasil()'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						{{-- <div class="control-group">
							<label class="control-label">Tanggal hasil tersedia</label>
							<div class="controls">
								{{Form::text(
									'sr[tgl_hasil_tersedia_campak]',
									Input::old('sr[tgl_hasil_tersedia_campak]'),
									array(
										'class'=>'input-medium',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div> --}}
						<div class="control-group">
							<label class="control-label">Tanggal hasil dilaporkan</label>
							<div class="controls">
								{{Form::text(
									'sr[tgl_hasil_dilaporkan_campak]',
									Input::old('sr[tgl_hasil_dilaporkan_campak]'),
									array(
										'class'=>'input-medium',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="span6">
				<div class="media" id="igmRubella">
					<fieldset>
						<legend>IgM Rubella</legend>
						<div class="control-group">
							<label class="control-label">Tanggal Pemeriksaan Sampel</label>
							<div class="controls">
								{{Form::text(
									'sr[tgl_pemeriksaan_sampel_rubella]',
									Input::old('sr[tgl_pemeriksaan_sampel_rubella]'),
									array(
										'class'=>'input-medium',
										'id'=>'tglPeriksaRubella',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Kit Elisa yang digunakan </label>
							<div class="controls">
								{{Form::select(
									'sr[kit_elisa_rubella]',
									array('' => 'Pilih',
										'Siemens' => 'Siemens',
										'Lainnya'=>'Lainnya',
										),
									null,
									array(
										'class' => 'input-small',
										'id'=>'kitElisa_rubella',
										'onchange'=>'kitElisa("_rubella")'
										)
									)
								}}
								{{Form::text(
									'sr[kit_elisa_lain_rubella]',
									Input::old('sr[kit_elisa_lain_rubella]'),
									array(
										'disabled'=>'disabled',
										'class'=>'input-medium kitElisaLain_rubella',
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						{{-- <div class="control-group">
							<label class="control-label">Rubella Antigen OD</label>
							<div class="controls">
								{{Form::text(
									'sr[rubella_antigen_od_rubella]',
									Input::old('sr[rubella_antigen_od_rubella]'),
									array(
										'class'=>'input-medium',
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div> --}}
						{{-- <div class="control-group">
							<label class="control-label">Control Antigen OD</label>
							<div class="controls">
								{{Form::text(
									'sr[control_antigen_od_rubella]',
									Input::old('sr[control_antigen_od_rubella]'),
									array(
										'class'=>'input-medium',
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div> --}}
						<div class="control-group">
							<label class="control-label">Hasil IgM Rubella</label>
							<div class="controls">
								{{Form::select(
									'sr[hasil_igm_rubella]',
									array('' => 'Pilih',
										'Positif' => 'Positif',
										'Negatif'=>'Negatif',
										'Equivocal'=>'Equivocal',
										),
									null,
									array(
										'class' => 'input-medium id_combobox',
										'id'=>'hasilIgmRubella'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						{{-- <div class="control-group">
							<label class="control-label">Tanggal hasil tersedia</label>
							<div class="controls">
								{{Form::text(
									'sr[tgl_hasil_tersedia_rubella]',
									Input::old('sr[tgl_hasil_tersedia_rubella]'),
									array(
										'class'=>'input-medium',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div> --}}
						<div class="control-group">
							<label class="control-label">Tanggal hasil dilaporkan</label>
							<div class="controls">
								{{Form::text(
									'sr[tgl_hasil_dilaporkan_rubella]',
									Input::old('sr[tgl_hasil_dilaporkan_rubella]'),
									array(
										'class'=>'input-medium',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						{{-- <div class="control-group">
							<label class="control-label">Tanggal kirim hasil</label>
							<div class="controls">
								{{Form::text(
									'sr[tgl_kirim_hasil_rubella]',
									Input::old('sr[tgl_kirim_hasil_rubella]'),
									array(
										'class'=>'input-medium',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div> --}}
					</fieldset>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span12">
				<div class="media">
					<fieldset>
						<div class="control-group">
							<label class="control-label">Penanggung Jawab</label>
							<div class="controls">
								{{Form::text(
									'sr[penanggung_jawab_serologi]',
									Input::old('sr[penanggung_jawab_serologi]'),
									array(
										'class'=>'input-medium',
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions">
		<input type="button" class="btn btn-primary submitserologi" style="margin-left:20%;" value="Submit">
		{{Form::reset('Tutup',array('class'=>'btn btn-warning','onclick'=>"closeserologi()"))}}
	</div>
	{{Form::close()}}
</div>

<script type="text/javascript">
function closeserologi(){
	$("html, body").animate({scrollTop: 0}, 500);
	$('#inputserologi').hide(500);
	$('#inputlab').show(500);
	cleardetail();
}

function cekHasil() {
	var hasilIgmCampak = $('#hasilIgmCampak').val();
	if (hasilIgmCampak=='Negatif') {
		$('#igmRubella').show();
	}else{
		$('#igmRubella').hide();
	};
};

function kitElisa(clas) {
	var dt = $('#kitElisa'+clas).val();
	if (dt=='Lainnya') {
		$('.kitElisaLain'+clas).removeAttr('disabled');
	}else{
		$('.kitElisaLain'+clas).attr('disabled','disabled');
	};
}

$('#tglPeriksaCampak').change(function(){
	$('.tglPeriksaCampak').html($(this).val());
});
$('#tglPeriksaRubella').change(function(){
	$('.tglPeriksaRubella').html($(this).val());
});

$(function(){
	$('#hasilIgmCampak').change(function(){
		if($(this).val()=='Positif'){
			$('#klasifikasiFinal').select2('val','1');
		}else if($(this).val()=='Negatif' && $('#hasilIgmRubella').val()=='Negatif'){
			$('#klasifikasiFinal').select2('val','5');
		}else{
			$('#klasifikasiFinal').select2('val','6');
		}
	})
	$('#hasilIgmRubella').change(function(){
		if($(this).val()=='Positif'){
			$('#klasifikasiFinal').select2('val','4');
		}else if($(this).val()=='Negatif' && $('#hasilIgmCampak').val()=='Negatif'){
			$('#klasifikasiFinal').select2('val','5');
		}else{
			$('#klasifikasiFinal').select2('val','6');
		}
	})

	$('#igmRubella').hide();
	$('.submitserologi').click(function(e){
		var url = '{{URL::to("submitujisampel")}}';
	    $.ajax({
	    	type: "POST",
	        url: url,
	        data: $("#forminputserologi").serialize(),
	        success: function(response)
	        {
	        	if (response.sent!=null) {
	        		var igmCampak = (response.sent.hasil_igm_campak=='')?'':response.sent.hasil_igm_campak;
	        		var igmRubella = (response.sent.hasil_igm_rubella=='')?'':response.sent.hasil_igm_rubella;
	        		var sent = igmCampak+'<br>'+igmRubella;
	        		$('#'+$('.seq').val()).html(sent);
	        		$('#id_sampel_'+$('.seq').val()).val(response.sent.id_sampel);
	        		$('#forminputserologi').trigger('reset');
	        		closeserologi();
	        	};
	        }
	    });
	    return false;
	});
})
</script>
