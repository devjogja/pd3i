<div class="module-body" id="inputvirologi">
    <div class="profile-head media">
        <h4>Pemeriksaan Virologi</h4>
        <hr>
    </div>
    {{
	  Form::open(array(
	    'url' => '',
	    'class' => 'form-horizontal',
	    'id'    =>'forminputvirologi'
	  ))
	}}
	<input type="hidden" class="seq" value="">
	<input type="hidden" name="sr[jenis_pemeriksaan]" class="jenis_periksa" value="Virologi">
	<input type="hidden" name="sr[sampel_ke]" class="sampel_ke" value="">
	<input type="hidden" name="sr[jenis_sampel]" class="jenis_sampel" value="">
	<input type="hidden" name="sr[no_spesimen]" class="noSpesimenSerologi" value="">
	<input type="hidden" name="sr[ambil_sampel]" class="ambil_sampel" value="">
	<input type="hidden" name="sr[kirim_sampel]" class="kirim_sampel" value="">
	<input type="hidden" name="sr[terima_sampel]" class="terima_sampel" value="">
    <div class="module-body">
		<div class="row-fluid">
			<div class="span12">
				<div class="media">
					<fieldset>
						<legend>Data Dasar Spesimen</legend>
						<table class="table" style="width:100%;">
							<tr>
								<td style="text-align:right;width:15%;">Nama :</td>
								<td style="width:25%;"><span class='nama_pasien'></span></td>
								<td style="text-align:right;width:15%;">Tanggal Pemeriksaan Kultur :</td>
								<td style="width:25%;"><span class='tglPemeriksaanKultur'></span></td>
							</tr>
							<tr>
								<td style="text-align:right;width:15%;">No.EPID :</td>
								<td style="width:25%;"><span class='epid'></span></td>
								<td style="text-align:right;width:15%;">Tanggal Pemeriksaan PCR :</td>
								<td style="width:25%;"><span class='tglPemeriksaanPCR'></span></td>
							</tr>
							<tr>
								<td style="text-align:right;width:15%;">No.Outbreak :</td>
								<td style="width:25%;"><span class='no_outbreak'></span></td>
								<td style="text-align:right;width:15%;">Tanggal Pemeriksaan Sequencing :</td>
								<td style="width:25%;"><span class='tglPemeriksaanSequencing'></span></td>
							</tr>
							<tr>
								<td style="text-align:right;width:15%;">No.Laboratorium :</td>
								<td style="width:25%;"><span class='no_spesimen'></span></td>
								<td style="text-align:right;width:15%;"></td>
								<td style="width:25%;"></td>
							</tr>
							<tr>
								<td style="text-align:right;width:15%;"></td>
								<td style="width:25%;"></td>
								<td style="text-align:right;width:15%;"></td>
								<td style="width:25%;"></td>
							</tr>
						</table>
					</fieldset>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span12">
				<div class="media">
					<fieldset>
						<legend>Informasi Spesimen</legend>
						<div class="control-group">
							<label class="control-label">Tipe Spesimen</label>
							<div class="controls">
								{{Form::select(
									'sr[tipe_spesimen]',
									array('' => 'Pilih',
										'Serum' => 'Serum',
										'Urin'=>'Urin',
										'Swab Tenggorokan'=>'Swab Tenggorokan',
										'Lainnya'=>'Lainnya',
										),
									null,
									array(
										'class' => 'input-large id_spesimen'
										)
									)
								}}
								{{Form::text(
									'sr[tipe_spesimen_lain]',
									Input::old('sr[tipe_spesimen_lain]'),
									array(
										'class'=>'input-large tipe_spesimen_lain',
										'disabled'=>'disabled'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						{{-- <div class="control-group">
							<label class="control-label">Tanggal Pengambilan Spesimen</label>
							<div class="controls">
								{{Form::text(
									'sr[tgl_pengambilan_spesimen]',
									Input::old('sr[tgl_pengambilan_spesimen]'),
									array(
										'class'=>'input-large',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Tanggal Pengiriman Spesimen ke Lab</label>
							<div class="controls">
								{{Form::text(
									'sr[tgl_pengiriman_spesimen]',
									Input::old('sr[tgl_pengiriman_spesimen]'),
									array(
										'class'=>'input-large',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Tanggal Penerimaan Spesimen ke Lab</label>
							<div class="controls">
								{{Form::text(
									'sr[tgl_penerimaan_spesimen]',
									Input::old('sr[tgl_penerimaan_spesimen]'),
									array(
										'class'=>'input-large',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div> --}}
						<div class="control-group">
							<label class="control-label">Kondisi Spesimen</label>
							<div class="controls">
								{{Form::select(
									'sr[kondisi_spesimen]',
									array('' => 'Pilih',
										'Baik' => 'Baik',
										'Volume Kurang'=>'Volume Kurang',
										'Bocor/Tumpah'=>'Bocor/Tumpah',
										'Tidak Dingin'=>'Tidak Dingin',
										'Tidak Baik'=>'Tidak Baik',
										'Lainnya'=>'Lainnya'
										),
									null,
									array(
										'class' => 'input-large id_combobox'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Apakah diperiksa</label>
							<div class="controls">
								{{Form::select(
									'sr[apa_diperiksa]',
									array('' => 'Pilih',
										'Ya' => 'Ya',
										'Tidak' => 'Tidak',
										),
									null,
									array(
										'class' => 'input-large id_combobox'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span12">
				<div class="media">
					<fieldset>
						<legend>Kultur</legend>
						<div class="control-group">
							<label class="control-label">Tanggal Pemeriksaan Kultur</label>
							<div class="controls">
								{{Form::text(
									'sr[tgl_pemeriksaan_kultur]',
									Input::old('sr[tgl_pemeriksaan_kultur]'),
									array(
										'class'=>'input-large',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
										'id'=>'tglPeriksaKultur'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Tanggal hasil kultur dilaporkan</label>
							<div class="controls">
								{{Form::text(
									'sr[tgl_hasil_kultur_dilaporkan]',
									Input::old('sr[tgl_hasil_kultur_dilaporkan]'),
									array(
										'class'=>'input-large',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Hasil Pemeriksaan Kultur</label>
							<div class="controls">
								{{Form::select(
									'sr[hasil_pemeriksaan_kultur]',
									array('' => 'Pilih',
										'Positif virus campak' => 'Positif virus campak',
										'Negatif virus campak' => 'Negatif virus campak',
										'Positif virus rubella' => 'Positif virus rubella',
										'Negatif virus rubella' => 'Negatif virus rubella',
										'Pending' => 'Pending',
										),
									null,
									array(
										'class' => 'input-large id_combobox'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
		@if(Session::get('kd_faskes')!='LAB0004')
		<div class="row-fluid">
			<div class="span12">
				<div class="media">
					<fieldset>
						<legend>PCR</legend>
						<div class="control-group">
							<label class="control-label">Onset kualifikasi spesimen PCR</label>
							<div class="controls">
								{{Form::text(
									'sr[onset_kualifikasi_spesimen_pcr]',
									Input::old('sr[onset_kualifikasi_spesimen_pcr]'),
									array(
										'class'=>'input-large',
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Tanggal Pemeriksaan PCR</label>
							<div class="controls">
								{{Form::text(
									'sr[tgl_pemeriksaan_pcr]',
									Input::old('sr[tgl_pemeriksaan_pcr]'),
									array(
										'class'=>'input-large',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
										'id'=>'tglPeriksaPCR'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Tanggal Keluar Hasil PCR</label>
							<div class="controls">
								{{Form::text(
									'sr[tgl_keluar_hasil_pcr]',
									Input::old('sr[tgl_keluar_hasil_pcr]'),
									array(
										'class'=>'input-large',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Metode PCR</label>
							<div class="controls">
								<table>
									<tr>
										<td>
										{{
											Form::checkbox('sr[metode_pcr_konvensional]',
												null
											);
										}}&nbsp;Konvensional
										</td>
									</tr>
									<tr>
										<td>
										{{
											Form::checkbox('sr[metode_pcr_realtime]',
												null
											);
										}}&nbsp;Realtime
										</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Hasil Pemeriksaan PCR</label>
							<div class="controls">
								{{Form::select(
									'sr[hasil_periksa_pcr]',
									array('' => 'Pilih',
										'Positif virus campak' => 'Positif virus campak',
										'Negatif virus campak' => 'Negatif virus campak',
										'Positif virus rubella' => 'Positif virus rubella',
										'Negatif virus rubella' => 'Negatif virus rubella',
										'Pending' => 'Pending',
										),
									null,
									array(
										'class' => 'input-large id_combobox'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
		@endif
		@if(Session::get('kd_faskes')=='LAB0001' || Session::get('kd_faskes')=='LAB0002')
		<div class="row-fluid">
			<div class="span12">
				<div class="media">
					<fieldset>
						<legend>Sequencing</legend>
						<div class="control-group">
							<label class="control-label">Tanggal Pemeriksaan Sequencing</label>
							<div class="controls">
								{{Form::text(
									'sr[tgl_periksa_sequencing]',
									Input::old('sr[tgl_periksa_sequencing]'),
									array(
										'class'=>'input-large',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
										'id'=>'tglPeriksaSequencing'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Hasil Sequencing</label>
							<div class="controls">
								{{Form::select(
									'sr[hasil_sequencing_virologi]',
									array('' => 'Pilih',
										'A' => 'A',
										'B1' => 'B1',
										'B2' => 'B2',
										'B3' => 'B3',
										'C1' => 'C1',
										'C2' => 'C2',
										'D1' => 'D1',
										'D2' => 'D2',
										'D3' => 'D3',
										'D4' => 'D4',
										'D5' => 'D5',
										'D6' => 'D6',
										'D7' => 'D7',
										'D8' => 'D8',
										'D9' => 'D9',
										'D10' => 'D10',
										'E' => 'E',
										'F' => 'F',
										'G1' => 'G1',
										'G2' => 'G2',
										'G3' => 'G3',
										'H1' => 'H1',
										'H2' => 'H2',
										'1a' => '1a',
										'1B' => '1B',
										'1C' => '1C',
										'1D' => '1D',
										'1E' => '1E',
										'1F' => '1F',
										'1G' => '1G',
										'1h' => '1h',
										'1i' => '1i',
										'1j' => '1j',
										'2A' => '2A',
										'2B' => '2B',
										'2C' => '2C',
										),
									null,
									array(
										'class' => 'input-large id_combobox'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						<div class="control-group hide" id="tglKirimSpesimen">
							<label class="control-label">Tanggal Pengiriman Hasil Sequencing</label>
							<div class="controls">
								{{Form::text(
									'sr[tgl_kirim_sequencing]',
									Input::old('sr[tgl_kirim_sequencing]'),
									array(
										'class'=>'input-large',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
		@else
		<div class="row-fluid">
			<div class="span12">
				<div class="media">
					<fieldset>
						<legend>Sequencing</legend>
						<div class="control-group">
							<label class="control-label">Sampel dirujuk</label>
							<div class="controls">
								{{Form::select(
									'rujukan',
									array('' => 'Pilih',
										'Ya' => 'Ya',
										'Tidak' => 'Tidak',
										),
									null,
									array(
										'class' => 'input-large id_combobox',
										'id'=>'sampeldirujuk',
										'onchange'=>'sampelrujukan()',
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Laboratorium tujuan rujukan</label>
							<div class="controls">
								{{Form::select(
									'to_lab_rujukan',
									array('' => 'Pilih',
										'1' => 'Biofarma',
										'2' => 'BBLK Jakarta',
										),
									null,
									array(
										'class' => 'input-large id_combobox',
										'id'=>'labtujuanrujukan',
										'disabled'=>'disabled'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Tanggal Spesimen dirujuk</label>
							<div class="controls">
								{{Form::text(
									'tgl_lab_rujukan',
									Input::old('sr[tgl_lab_rujukan]'),
									array(
										'class'=>'input-large',
										'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
										'id'=>'tglsampeldirujuk',
										'disabled'=>'disabled'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
		@endif
		<div class="row-fluid">
			<div class="span12">
				<div class="media">
					<fieldset>
						<div class="control-group">
							<label class="control-label">MeaNS/Ruben bank ref number</label>
							<div class="controls">
								{{Form::text(
									'sr[gen_bank_ref_num]',
									Input::old('sr[gen_bank_ref_num]'),
									array(
										'class'=>'input-large',
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions">
		<input type="button" class="btn btn-primary submitvirologi" style="margin-left:20%;" value="Submit">
		{{Form::reset('Tutup',array('class'=>'btn btn-warning','onclick'=>"closevirologi()"))}}
	</div>
	{{Form::close()}}
</div>


<script type="text/javascript">
function closevirologi(){
	$("html, body").animate({scrollTop: 0}, 500);
	$('#inputvirologi').hide(500);
	$('#inputlab').show(500);
	cleardetail();
};

function sampelrujukan() {
	var val = $('#sampeldirujuk').val();
	if (val=='Ya'){
		$('#labtujuanrujukan').removeAttr('disabled');
		$('#tglsampeldirujuk').removeAttr('disabled');
	}else{
		$('#labtujuanrujukan').attr('disabled','disabled');
		$('#tglsampeldirujuk').attr('disabled','disabled');
	};
}

$(function(){
	$('#tglPeriksaKultur').change(function(){
		$('.tglPemeriksaanKultur').html($(this).val());
	});
	$('#tglPeriksaPCR').change(function(){
		$('.tglPemeriksaanPCR').html($(this).val());
	});
	$('#tglPeriksaSequencing').change(function(){
		$('.tglPemeriksaanSequencing').html($(this).val());
	});

	$('.id_spesimen').click(function(){
		var x = $(this).val();
		if (x=='Lainnya') {
			$('.tipe_spesimen_lain').removeAttr('disabled');
		}else{
			$('.tipe_spesimen_lain').attr('disabled','disabled');
		};
	});
	$('.submitvirologi').click(function(e){
		var url = '{{URL::to("submitujisampel")}}';
		var id = "{{Request::segment(3)}}";
	    $.ajax({
	    	type: "POST",
	        url: url,
	        data: $("#forminputvirologi").serialize()+'&id_campak='+id,
	        success: function(response)
	        {
	        	if (response.sent!=null) {
	        		var kultur = (response.sent.hasil_pemeriksaan_kultur=='')?'':'Hasil Pemeriksaan Kultur : '+response.sent.hasil_pemeriksaan_kultur;
	        		var pcr = (response.sent.hasil_periksa_pcr=='')?'':'Hasil Pemeriksaan PCR : '+response.sent.hasil_periksa_pcr;
	        		var sequencing = (response.sent.hasil_sequencing_virologi=='')?'':'Hasil Pemeriksaan Sequencing : '+response.sent.hasil_sequencing_virologi;
	        		var sent = kultur+'<br>'+pcr+'<br>'+sequencing;
	        		$('#'+$('.seq').val()).html(sent);
	        		$('#id_sampel_'+$('.seq').val()).val(response.sent.id_sampel);
	        		$('#forminputvirologi').trigger('reset');
	        		closevirologi();
	        	};
	        }
	    });
	    return false;
	})
})
</script>