@include('laboratorium._include.filter_daftar_kasus')
<div class="module-body uk-overflow-container" id="data">
    <table class="display table table-bordered" id="example">
        <thead>
            <tr>
                <th rowspan="2">No.Lab</th>
                <th rowspan="2">Nama pasien</th>
                <th rowspan="2">Umur</th>
                <th rowspan="2">Jenis Kelamin</th>
                <th rowspan="2">Kabupaten</th>
                <th colspan="4" style="text-align:center">Hasil Lab</th>
                <th rowspan="2">Klasifikasi Final</th>
                <th rowspan="2">Aksi</th>
            </tr>
            <tr>
                <th>Tgl Pemeriksaan</th>
                <th>Jenis Pemeriksaan</th>
                <th>Jenis Sampel</th>
                <th>Hasil Lab</th>
            </tr>
        </thead>
        <tbody>
        @if($response['data'])
            @foreach($response['data'] as $key=>$row)
            <tr>
                <td>{{$row->no_lab}}</td>
                <td>{{$row->nama_pasien}}</td>
                <td>{{$row->umur.' Th '.$row->umur_bln.' Bln '.$row->umur_hr.' Hari'}}</td>
                <td>{{$row->jenis_kelamin}}</td>
                <td>{{$row->kabupaten}}</td>
                <td>{{$row->tgl_pemeriksaan}}</td>
                <td>{{$row->jenis_pemeriksaan}}</td>
                <td>{{$row->jenis_sampel}}</td>
                <td>{{$row->hasil_uji_sampel}}</td>
                <td>{{$row->klasifikasi_final}}</td>
                <td>
                    <div class="btn-group" role="group" >
                        <a href="{{URL::to('labdetailCampak/'.$row->id_campak)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
                        <a href="{{URL::to('inputHasilLabCampak/'.$row->id_campak)}}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>
                        <a href="{{URL::to('labhapus/'.$row->id_campak)}}" onclick="return confirm('apakah yakin ingin menghapus data ini?');return false;" class="btn btn-xs btn-danger delete" data-uk-tooltip title="hapus data"><i class="icon icon-remove"></i></a>
                    </div>
                </td>
            </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
