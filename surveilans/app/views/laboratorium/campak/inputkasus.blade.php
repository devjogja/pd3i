<div class="alert alert-error">
	<p style="font-size:12px">Text input yang bertanda bintang <strong>(*)</strong> wajib di isi
</div>
{{
	Form::open(array(
		'url' => '',
		'class' => 'form-horizontal',
		'id'    =>'forminputkasus'
	))
}}

{{ Form::hidden('id_campak', '',array('id'=>'id_campak')) }}
<div class="module-body">
	<div class="row-fluid">
		<div class="span12">
			<div class="media">
				<fieldset>
					<legend>Informasi pasien</legend>
					<div class="row-fluid">
						<div class="span6">
							<div class="control-group">
								<label class="control-label">Nama penderita</label>
								<div class="controls">
									{{
										Form::text(
											'dp[nama_anak]',
											null,
											['data-validation'         =>'[MIXED]',
											'data'                    =>'$ wajib di isi!',
											'data-validation-message' =>'nama penderita wajib di isi!',
											'class'                   => 'input-xlarge',
											'id'                   	=> 'nama_anak',
											'placeholder'             =>'Nama penderita']
											)
									}}
									<span class="help-inline" style="color:red">(*)</span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">NIK</label>
								<div class="controls">
									{{Form::text(
										'dp[nik]',
										null,
										['class' => 'input-medium',
										'id'=>'nik',
										'placeholder'=>'Nomer induk kependudukan']
										)
									}}
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Nama orang tua</label>
								<div class="controls">
									{{Form::text(
										'dp[nama_ortu]',
										null,
										['data-validation'=>'[MIXED]',
											'data'=>'$ wajib di isi!',
											'id'=>'nama_ortu',
											'data-validation-message'=>'nama orang tua wajib di isi!',
											'class' => 'input-xlarge',
											'placeholder'=>'Nama orang tua']
										)
									}}
									<span class="help-inline" style="color:red">(*)</span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Jenis kelamin</label>
								<div class="controls">
									{{Form::SELECT('dp[jenis_kelamin]',
										[null => 'Pilih',
										'1'=>'Laki-laki',
										'2'=>'Perempuan',
										'3' =>'Tidak jelas'],
										null,
										['data-validation'=>'[MIXED]',
											'data'=>'$ wajib di isi!',
											'data-validation-message'=>'jenis kelamin wajib di isi!',
											'class' => 'input-medium id_combobox',
											'id'=>'jenis_kelamin']
										)
									}}
									<span class="help-inline" style="color:red">(*)</span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label"><input type="radio" name="option" onclick="pilihOptionTglLahir()">Tanggal lahir</label>
								<div class="controls">
									{{Form::text(
										'dp[tanggal_lahir]',
										null,
										array(
											'id'=>'tanggal_lahir',
											'class' => 'input-medium tgl_lahir',
											'placeholder'=>'Tanggal lahir',
											'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
											'onchange'=>'umur()',
											'disabled'=>'disabled'
											)
										)
									}}
									<span class="help-inline" style="color:red"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label"><input type="radio" name="option" onclick="pilihOptionUmur()">Umur</label>
								<div class="controls">
									{{Form::text(
										'dp[umur]',
										null,
										array(
											'class' => 'input-mini',
											'id'=>'tgl_tahun',
											'disabled'=>'disabled'
											)
										)
									}} Thn
									{{Form::text(
										'dp[umur_bln]',
										null,
										array(
											'class' => 'input-mini',
											'id'=>'tgl_bulan',
											'disabled'=>'disabled'
											)
										)
									}} Bln
									{{Form::text(
										'dp[umur_hr]',
										null,
										array(
											'class' => 'input-mini',
											'id'=>'tgl_hari',
											'disabled'=>'disabled'
											)
										)
									}} Hr
								</div>
							</div>
						</div>
						<div class="span6">
							<div class="control-group">
								<label class="control-label">Jalan/RT/RW/Dusun</label>
								<div class="controls">
									{{Form::textarea(
										'dp[alamat]',
										null,
										array(
											'rows'=>'3',
											'id'=>'alamat',
											'style'=>'width:270px;',
											'placeholder' => 'Hanya diisi nama jalan, no. rumah dan no. RT - RW'
											)
										)
									}}
									<span class="help-inline" style="color:red"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Cari data wilayah</label>
								<div class="controls">
									{{Form::text('',null,['id'=>'wilayah','class' => 'input-xlarge','placeholder'=>'Ketik nama desa/kecamatan'])}}
									<span class="help-inline" style="color:red">(*)</span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Kelurahan/Desa</label>
								<div class="controls">
									{{Form::text('kelurahan_pasien',null,['id'=>'kelurahan_pasien','class' => 'input-xlarge','placeholder'=>'Kelurahan','readonly'=>'readonly'])}}
									{{Form::hidden('id_kelurahan_pasien',null,['id'=>'id_kelurahan_pasien'])}}
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Kecamatan</label>
								<div class="controls">
									{{Form::text('kecamatan_pasien',null,['id'=>'kecamatan_pasien','class' => 'input-xlarge','placeholder'=>'Kecamatan','readonly'=>'readonly'])}}
									{{Form::hidden('id_kecamatan_pasien',null,['id'=>'id_kecamatan_pasien'])}}
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Kabupaten</label>
								<div class="controls">
									{{Form::text('kabupaten_pasien',null,['id'=>'kabupaten_pasien','class' => 'input-xlarge','placeholder'=>'Kabupaten','readonly'=>'readonly'])}}
									{{Form::hidden('id_kabupaten_pasien',null,['id'=>'id_kabupaten_pasien'])}}
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Provinsi</label>
								<div class="controls">
									{{Form::text('provinsi_pasien',null,['id'=>'provinsi_pasien','class' => 'input-xlarge','placeholder'=>'Provinsi','readonly'=>'readonly'])}}
									{{Form::hidden('id_provinsi_pasien',null,['id'=>'id_provinsi_pasien'])}}
									<span class="help-inline"></span>
								</div>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="media">
				<fieldset>
					<legend>Data klinis campak</legend>
					<div class="row-fluid">
						<div class="span6">
							<div class="control-group">
								<label class="control-label">No Epidemologi</label>
								<div class="controls">
									{{Form::text(
										'dc[no_epid]',
										null,
										array(
											'class' => 'input-medium',
											'placeholder'=>'Nomer epidemologi',
											'id'=>'epid',
											'disabled'=>'disabled'
											)
										)
									}}
									<span class="help-inline" style="color:red"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">No Epidemologi lama</label>
								<div class="controls">
									{{Form::text(
										'dc[no_epid_lama]',
										null,
										array(
											'class' => 'input-medium',
											'placeholder'=>'Nomer epidemologi lama',
											'id'=>'epid_lama'
											)
										)
									}}
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Tanggal imunisasi campak terakhir</label>
								<div class="controls">
									{{Form::text(
										'dc[tanggal_imunisasi_terakhir]',
										null,
										array(
											'class'=>'input-medium',
											'id'=>'tanggal_imunisasi_terakhir',
											'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
											'placeholder'=>'Tanggal imunisasi campak terakhir (DD-MM-YYYY)'
											)
										)
									}}
									<span class="help-inline" style="color:red"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Imunisasi campak sebelum sakit berapa kali?</label>
								<div class="controls">
									{{Form::select(
										'dc[vaksin_campak_sebelum_sakit]',
										[
										null => 'Pilih',
										'1' => '1X',
										'2'=>'2X',
										'3'=>'3X',
										'4'=>'4X',
										'5'=>'5X',
										'6'=>'6X',
										'7'=>'Tidak',
										'8'=>'Tidak tahu'],
										null,
										['id'=>'vaksin_campak_sebelum_sakit',
										'placeholder'=>'Pilih',
										'class' => 'input-medium id_combobox']
										)
									}}
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Tanggal mulai demam</label>
								<div class="controls">
									{{Form::text(
										'dc[tanggal_timbul_demam]',
										null,
										array(
											'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
											'class'=>'input-medium tgl_mulai_sakit',
											'id'=>'tgl_mulai_sakit',
											'placeholder'=>'Tanggal mulai demam (DD-MM-YYYY)'
											)
										)
									}}
									<span class="help-inline" style="color:red"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Tanggal mulai rash</label>
								<div class="controls">
									{{Form::text(
										'dc[tanggal_timbul_rash]',
										null,
										array(
											'id'=>'tanggal_timbul_rash',
											'class'=>'input-medium tgl_mulai_rash',
											'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
											'onchange'=>"showEpidCampak('_pasien')",
											'placeholder'=>'Tanggal mulai rash (DD-MM-YYYY)'
											)
										)
									}}
									<span class="help-inline" style="color:red">(*)</span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Gejala / tanda lainnya</label>
								<div class="controls">
									<table>
										@foreach(Campak::DaftarGejala() as $row)
										<tr>
											<td>
												<input type="checkbox" value="{{$row->id}}" onclick="cekGejala({{$row->id}})" class="gejalaLain_{{$row->id}}" name="gejala_lain[]">
												{{$row->nama}}
											</td>
											<td>
												<input type="text" class="input-medium tglGejalaLain_{{$row->id}}" placeholder='(DD-MM-YYYY)' value="" disabled="disabled" name="tanggal_gejala_lain[]" data-uk-datepicker='{format:"YYYY-MM-DD"}'>
											</td>
										</tr>
										@endforeach
									</table>
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Komplikasi</label>
								<div class="controls">
									<table>
										<tr>
											<td>
												<input type="checkbox" class="komplikasi_0" name="komplikasi[]" value="0">&nbsp;Diare
											</td>
										</tr>
										<tr>
											<td>
												<input type="checkbox" class="komplikasi_1" name="komplikasi[]" value="1">&nbsp;Pneumonia
											</td>
										</tr>
										<tr>
											<td>
												<input type="checkbox" class="komplikasi_2" name="komplikasi[]" value="2">&nbsp;Bronchopneumonia
											</td>
										</tr>
										<tr>
											<td>
												<input type="checkbox" class="komplikasi_3" name="komplikasi[]" value="3">&nbsp;OMA
											</td>
										</tr>
										<tr>
											<td>
												<input type="checkbox" class="komplikasi_4" name="komplikasi[]" value="4">&nbsp;Ensefalitis
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<div class="span6">
							<div class="control-group">
								<label class="control-label">Status Vaksinasi</label>
								<div class="controls">
									{{Form::select(
										'dc[status_vaksinasi]',
										[
										null=>'Pilih',
										'Tidak Diketahui'=>'Tidak Diketahui',
										'Tidak Imunisasi'=>'Tidak Imunisasi',
										'Immunized, document'=>'Immunized, document',
										'Immunized, history'=>'Immunized, history',
										],
										null,
										['class' => 'input-medium',
										'id'=>'status_vaksinasi'
										])
									}}
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Dosis</label>
								<div class="controls">
									{{Form::select(
										'dc[dosis]',
										[
										''=>'Pilih',
										'1x'=>'1x',
										'2x'=>'2x',
										'3x'=>'3x',
										'4x'=>'4x',
										'5x'=>'5x',
										'6x'=>'6x',
										],
										null,
										['class' => 'input-medium',
										'id'=>'dosis'
										])
									}}
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Riwayat Berpergian</label>
								<div class="controls">
									{{Form::select(
										'dc[riwayat_pergi]',
										[
										null=>'Pilih',
										'Ya'=>'Ya',
										'Tidak'=>'Tidak',
										],
										null,
										['class' => 'input-medium',
										'id'=>'riwayat_pergi'
										])
									}}
									{{Form::text('dc[tujuan_riwayat_pergi]',null, ['class' => 'input-medium tujuan_riwayat_pergi','id'=>'tujuan_riwayat_pergi','disabled'=>'disabled'])}}
									<span class="help-inline"></span>
								</div>
							</div>
							<!-- awal input kolom tanggal laporan diterima -->
							<div class="control-group">
								<label class="control-label">Tanggal laporan diterima</label>
								<div class="controls">
									{{Form::text(
										null,
										Input::old('tanggal_laporan_diterima'),
										array(
											'id'=>'tanggal_laporan_diterima',
											'class'=>'input-medium',
											'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
											'placeholder'=>'(DD-MM-YYYY)'
											)
										)
									}}
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Tanggal pelacakan</label>
								<div class="controls">
									{{Form::text(
										'dc[tanggal_pelacakan]',
										null,
										array(
											'id'=>'tanggal_pelacakan',
											'class'=>'input-medium',
											'placeholder'=>'(DD-MM-YYYY)',
											'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}'))
									}}
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Diberi vitamin A</label>
								<div class="controls">
									{{Form::select(
										'dc[vitamin_A]',
										array(
											null => 'Pilih',
											'1' => 'Ya',
											'2'=>'Tidak'),
										null,
										array(
											'id'=>'vitamin_A',
											'class' => 'input-small id_combobox'
											)
										)
									}}
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Keadaan akhir</label>
								<div class="controls">
									{{Form::select(
										'dc[keadaan_akhir]',
										array(
											null => 'Pilih',
											'1' => 'Hidup/Sehat',
											'2'=>'Meninggal'),
										null,
										array(
											'id'=>'keadaan_akhir',
											'class' => 'input-medium id_combobox'
											)
										)
									}}
									<span class="help-inline"></span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Jenis kasus</label>
								<div class="controls">
									{{Form::select(
										'dc[jenis_kasus]',
										array(
											null => 'Pilih',
											'1' => 'KLB',
											'2'=>'Bukan KLB'),
											null,
											array(
												'class' => 'input-medium id_combobox',
												'id' => 'jenis_kasus',
												'onchange'=>'jenisKasus()',
												'data-validation'=>'[MIXED]',
												'data'=>'$ wajib di isi!',
												'data-validation-message'=>'Jenis kasus harus di isi',
											)
										)
									}}
								<span class="help-inline" style="color:red">(*)</span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">KLB Ke</label>
								<div class="controls">
									{{Form::select(
										'dc[klb_ke]',
										array(
											null => 'Pilih',
											'KI' => 'KI',
											'KII' => 'KII',
											'KIII' => 'KIII',
											'KIV' => 'KIV',
											'KV' => 'KV',
											'KVI' => 'KVI',
											'KVII' => 'KVII',
											'KVIII' => 'KVIII',
											'KIX' => 'KIX',
											'KX' => 'KX',
											'KXI' => 'KXI',
											'KXII' => 'KXII'
											),
											null,
											array(
												'class' => 'input-medium id_combobox',
												'id'=>'klb_ke',
												'disabled'=>'disabled',
												'data-validation'=>'[MIXED]',
												'data'=>'$ wajib di isi!',
												'data-validation-message'=>'KLB ke- harus di isi',
												'onchange'=>"showEpidKLB('_pasien')"
											)
										)
									}}
								<span class="help-inline" style="color:red">(*)</span>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Status kasus</label>
								<div class="controls">
									{{Form::select(
										'dc[status_kasus]',
										array(
											null => 'Pilih',
											'1' => 'Index',
											'2'=>'Bukan index'),
										null,
										array(
											'id'=>'status_kasus',
											'class' => 'input-medium id_combobox'
											)
										)
									}}
									<span class="help-inline"></span>
								</div>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
	<div class="form-actions">
		<a style="margin-left:20%;" class="btn btn-primary submitinput">Submit</a>
		{{Form::reset('reset',array('class'=>'btn btn-warning resetForm'))}}
		<a href="{{URL::to('labs/campak')}}" class="btn btn-danger">Kembali</a>
	</div>
</div>
{{Form::close()}}

<style>
	.ui-autocomplete {
		max-height: 220px;
		overflow-y: auto;
		overflow-x: hidden;
	}
</style>

<script type="text/javascript">
	$(function(){
		$('.submitinput').click(function(){
			$("html, body").animate({scrollTop: 0}, 500);
			$('.nav-tabs li:eq(1) a').tab('show');
		});
		$('#riwayat_pergi').change(function(){
			if ($(this).val()=='Ya') {
				$('.tujuan_riwayat_pergi').removeAttr('disabled');
			}else{
				$('.tujuan_riwayat_pergi').attr('disabled','disabled');
			};
		});

		$('#wilayah').autocomplete({
			source:"{{URL::to('getArea')}}",
			minLength:2,
			select:function(evt, ui)
			{
				$('#kelurahan_pasien').val(ui.item.kelurahan);
				$('#kecamatan_pasien').val(ui.item.kecamatan);
				$('#kabupaten_pasien').val(ui.item.kabupaten);
				$('#provinsi_pasien').val(ui.item.provinsi);
				$('#id_kelurahan_pasien').val(ui.item.id_kelurahan);
				$('#id_kecamatan_pasien').val(ui.item.id_kecamatan);
				$('#id_kabupaten_pasien').val(ui.item.id_kabupaten);
				$('#id_provinsi_pasien').val(ui.item.id_provinsi);
				$('#wilayah').val(null);
				return false;
			}
		})
		.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
			return $("<li>")
			.append("<a>"
				+item.kelurahan +"<br><small>Kelurahan: <i>"
				+item.kelurahan +"</i><br>Kecamatan: "
				+item.kecamatan+"<br>Kabupaten: "
				+item.kabupaten+"<br>Provinsi: "
				+item.provinsi+"</small></a>")
			.appendTo( ul );
		};

	});

function cekGejala(id) {
    var cek =$('.gejalaLain_'+id).is(':checked');
    if(cek){
        $('.tglGejalaLain_'+id).removeAttr('disabled');
    }
    else {
        $('.tglGejalaLain_'+id).attr('disabled','disabled');
    }
}
</script>
