<ul class="profile-tab nav nav-tabs">
	<li class="active"><a href="#inputDataIndividu" data-toggle="tab">Input data individual kasus</a></li>
	<li ><a href="#inputHasilLaboratorium" data-toggle="tab">Input Hasil Laboratorium</a></li>
</ul>
<div class="profile-tab-content tab-content">
	<div class="tab-pane fade active in" id="inputDataIndividu">
		@include('laboratorium.campak.inputkasus')
	</div>
	<div class="tab-pane fade  in" id="inputHasilLaboratorium">
		@include('laboratorium.campak.inputlab')
		@include('laboratorium.campak.inputserologi')
		@include('laboratorium.campak.inputvirologi')
	</div>
</div>

<script type="text/javascript">
	$(function(){
		getDetail();
		$('.resetForm').click(function(){
			$("select").select2('val','',true);
			$('input:checkbox').removeAttr('checked');
		});
	});

	function noSpesimen() {
		var tglTerimaSpesimen = $('#tglTerimaSpesimen').val();
		$.ajax({
			method: "POST",
			url: "{{URL::to('getNoSpesimen')}}",
			data: {tglPeriksa:tglTerimaSpesimen}
		})
		.done(function(response){
			$("#no_spesimen").val(response);
		});
	}

	function getDetail() {
		var id = "<?=$response['id']?>";
		$.ajax({
			method: "POST",
			url: "{{URL::to('getDataLabCampak')}}",
			data: {id_campak:id}
		})
		.done(function(response) {
			if (response!=null){
				$("#id_campak").val(response.data.campak_id_campak);
				$("#nama_anak").val(response.data.pasien_nama_anak);
				$("#nik").val(response.data.pasien_nik);
				$("#nama_ortu").val(response.data.pasien_nama_ortu);
				$("#jenis_kelamin").select2("val",response.data.pasien_jenis_kelamin);
				if(response.data.pasien_tanggal_lahir) {
					$("#tanggal_lahir").val(response.data.pasien_tanggal_lahir);
					$("#tanggal_lahir").removeAttr('disabled');
				};
				if(response.data.pasien_umur) {
					$("#tgl_tahun").val(response.data.pasien_umur);
					$("#tgl_tahun").removeAttr('disabled');
				};
				if(response.data.pasien_umur_bln) {
					$("#tgl_bulan").val(response.data.pasien_umur_bln);
					$("#tgl_bulan").removeAttr('disabled');
				};
				if(response.data.pasien_umur_hr) {
					$("#tgl_hari").val(response.data.pasien_umur_hr);
					$("#tgl_hari").removeAttr('disabled');
				};
				$('#kelurahan_pasien').val(response.data.pasien_kelurahan);
				$('#kecamatan_pasien').val(response.data.pasien_kecamatan);
				$('#kabupaten_pasien').val(response.data.pasien_kabupaten);
				$('#provinsi_pasien').val(response.data.pasien_provinsi);
				$('#id_kelurahan_pasien').val(response.data.pasien_id_kelurahan);
				$('#id_kecamatan_pasien').val(response.data.pasien_id_kecamatan);
				$('#id_kabupaten_pasien').val(response.data.pasien_id_kabupaten);
				$('#id_provinsi_pasien').val(response.data.pasien_id_provinsi);
				$("#alamat").val(response.data.pasien_alamat);
				$("#epid").val(response.data.campak_no_epid);
				$("#epid_lama").val(response.data.campak_no_epid_lama);
				$("#tanggal_imunisasi_terakhir").val(response.data.campak_tanggal_imunisasi_terakhir);
				$("#vaksin_campak_sebelum_sakit").select2('val',response.data.campak_vaksin_campak_sebelum_sakit);
				$("#tgl_mulai_sakit").val(response.data.campak_tanggal_timbul_demam);
				$("#tanggal_timbul_rash").val(response.data.campak_tanggal_timbul_rash);
				$.each(response.gejala,function(kg,vg){
					$('.gejalaLain_'+vg.id_daftar_gejala_campak).attr('checked','checked');
					$('.tglGejalaLain_'+vg.id_daftar_gejala_campak).removeAttr('disabled');
				});
				$.each(response.komplikasi,function(kk,vk){
					$('.komplikasi_'+vk.komplikasi).attr('checked','checked');
				});
				$("#status_vaksinasi").select2('val',response.data.campak_status_vaksinasi);
				$("#dosis").select2('val',response.data.campak_dosis);
				$("#riwayat_pergi").select2('val',response.data.campak_riwayat_pergi);
				if (response.data.campak_riwayat_pergi=='Ya'){
					$("#tujuan_riwayat_pergi").val(response.data.campak_tujuan_riwayat_pergi);
					$('#tujuan_riwayat_pergi').removeAttr('disabled');
				};
				$("#tanggal_laporan_diterima").val(response.data.campak_tanggal_laporan_diterima);
				$("#tanggal_pelacakan").val(response.data.campak_tanggal_pelacakan);
				$("#vitamin_A").select2('val',response.data.campak_vitamin_A);
				$("#keadaan_akhir").select2('val',response.data.campak_keadaan_akhir);
				$("#jenis_kasus").select2('val',response.data.campak_jenis_kasus);
				if (response.data.campak_klb_ke){
					$("#klb_ke").select2('val',response.data.campak_klb_ke);
					$("#klb_ke").removeAttr('disabled');
				};
				$("#status_kasus").select2('val',response.data.campak_status_kasus);
				$("#sumberSpesimen").select2('val',response.data.lab_sumber_spesimen);
				$("#id_outbreak").val(response.data.campak_id_outbreak);
				$("#dilaporkan_oleh").select2('val',response.data.lab_dilaporkan_oleh);
				$("#id_provinsi_outbreak").select2("val",response.data.lab_id_provinsi_outbreak);
				show_kabupaten('_outbreak',response.data.lab_id_provinsi_outbreak,response.data.lab_id_kabupaten_outbreak);
				$("#id_rs_outbreak").select2('val',response.data.lab_id_rs_outbreak);
				show_kecamatan('_outbreak',response.data.lab_id_kabupaten_outbreak,response.data.lab_id_kecamatan_outbreak);
				show_kelurahan('_outbreak',response.data.lab_id_kecamatan_outbreak,response.data.lab_id_kelurahan_outbreak);
				$("#id_puskesmas_outbreak").select2('val',response.data.lab_id_puskesmas_outbreak);
				if (response.data.lab_no_spesimen!=''){
					$("#no_spesimen").val(response.data.lab_no_spesimen);
				};
				$("#tglTerimaSpesimen").val(response.data.lab_tanggal_terima_spesimen);
				$("#kondisi_spesimen_dilab").select2('val',response.data.lab_kondisi_spesimen_dilab);
				$.each(response.sampel,function(ks,vs){
					var noSpesimen = vs.no_spesimen;
					var jenisPemeriksaan = vs.jenis_pemeriksaan;
					var typeSpesimen = vs.jenis_sampel;
					var ambilSampel = vs.ambil_sampel;
					var kirimSampel = vs.kirim_sampel;
					var terimaSampel = vs.terima_sampel;
					var sampel_ke = vs.sampel_ke;
					if (jenisPemeriksaan=='Serologi'){
						var igmCampak = (vs.hasil_igm_campak=='')?'':'Hasil IgM Campak : '+vs.hasil_igm_campak;
						var igmRubella = (vs.hasil_igm_rubella=='')?'':'Hasil IgM Rubella : '+vs.hasil_igm_rubella;
						var hasil = igmCampak+'<br>'+igmRubella;
					}else{
						var kultur = (vs.hasil_pemeriksaan_kultur=='')?'':'Hasil Pemeriksaan Kultur : '+vs.hasil_pemeriksaan_kultur;
						var pcr = (vs.hasil_periksa_pcr=='')?'':'Hasil Pemeriksaan PCR : '+vs.hasil_periksa_pcr;
						var sequencing = (vs.hasil_sequencing_virologi=='')?'':'Hasil Pemeriksaan Sequencing : '+vs.hasil_sequencing_virologi;
						var hasil = kultur+'<br>'+pcr+'<br>'+sequencing;
					};
					var idSampel = vs.id_sampel;
					var push = [noSpesimen,jenisPemeriksaan,typeSpesimen,ambilSampel,kirimSampel,terimaSampel,sampel_ke,hasil,idSampel];
					sampelCreate(push);
				});
		}
});
noSpesimen();
}
</script>
