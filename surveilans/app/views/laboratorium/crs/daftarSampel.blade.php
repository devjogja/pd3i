@include('laboratorium._include.filter_daftar_kasus')
<div class="module-body uk-overflow-container" id="data">
    <table class="display table table-bordered" id="example">
        <thead>
            <tr>
                <th>No.Lab</th>
                <th>Nama pasien</th>
                <th>Umur</th>
                <th>Jenis Kelamin</th>
                <th>Kabupaten</th>
                <th>Hasil</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($response['data'] as $row)
            <tr>
                <td>{{$row->no_epid}}</td>
                <td>{{$row->nama_pasien}}</td>
                <td>{{$row->umur.'Th '.$row->umur_bln.'Bln'.$row->umur_hr.'Hari'}}</td>
                <td>{{$row->jenis_kelamin}}</td>
                <td>{{$row->kabupaten}}</td>
                <td>{{$row->klasifikasi_final}}</td>
                <td>
                    <div class="btn-group" role="group" >
                        <a href="{{URL::to('crs_detail/'.$row->id_crs)}}" class="btn btn-xs btn-primary" data-uk-tooltip title="detail data" ><i class="icon icon-asterisk"></i></a>
                        <a href="{{URL::to('inputHasilLab/crs/'.$row->id_crs)}}" class="btn btn-xs btn-warning" data-uk-tooltip title="edit data"><i class="icon icon-pencil"></i></a>
                        <a href="{{URL::to('deleteLabCrs/'.$row->id_crs)}}" onclick="return confirm('apakah yakin ingin menghapus data ini?');return false;" class="btn btn-xs btn-danger" data-uk-tooltip title="hapus data"><i class="icon icon-remove"></i></a>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>