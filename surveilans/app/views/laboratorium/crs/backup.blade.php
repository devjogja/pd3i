<div class="alert alert-error">
	<p style="font-size:12px">Text input yang bertanda bintang <strong>(*)</strong> wajib di isi
</div>
{{ Form::open(
	array(
		'url' =>$response['action'],
		'class' =>'form-horizontal',
		'id'=>'formSave'
	))
}}

<?php
	$dt = $response['response'];
?>

<div class="module-body">
	<div class="row-fluid">
		<div class="span12">
			<div class="media">
				<fieldset>
					<legend>Data Pelapor</legend>
					<div class="control-group">
						<label class="control-label">Nama Faskes</label>
						<div class="controls">
							{{ Form::text(
									'dp[nama_rs]',
									(empty($dt->pelapor_nama_rs))?'':$dt->pelapor_nama_rs,
									array(
										  'class' => 'input-xlarge',
										  'placeholder' => 'Nama Faskes'
									))
							  }}
							<span class="help-inline" style="color:red">(*)</span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Provinsi</label>
						<div class="controls">
						{{ Form::select(
							'dp[id_provinsi]',
							array('0'=>'Pilih provinsi')+Provinsi::lists('provinsi','id_provinsi'),
							(empty($dt->pelapor_id_provinsi))?'':$dt->pelapor_id_provinsi,
							array(
								'id'=>'id_provinsi',
								'placeholder' => 'Pilih Provinsi',
								'onchange'=>'showKabupaten()',
								'class'=>'input-large id_combobox'
							))
						}}
						<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Kabupaten</label>
						<div class="controls">
						<?php
							$id_kabupaten_pelapor = (empty($dt->pelapor_id_kabupaten))?'':$dt->pelapor_id_kabupaten;
							$kp = (empty($dt->pelapor_id_kabupaten))?'':$dt->pelapor_id_kabupaten;
							$kbp = DB::table('kabupaten')->WHERE('id_kabupaten',$kp)->pluck('kabupaten');
							$kabupaten_pelapor = (empty($kbp))?'Pilih Kabupaten':$kbp;
						?>
						{{ Form::select(
							'dp[id_kabupaten]',
							 array(
								$id_kabupaten_pelapor => $kabupaten_pelapor
							),
							'null',
							array(
								'class' => 'input-large id_combobox',
								'id' => 'id_kabupaten',
							))
						}}
						<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Tanggal Laporan</label>
						<div class="controls">
						{{ Form::text(
							'dp[tanggal_laporan]',
							(empty($dt->pelapor_tanggal_laporan))?'':Helper::getDate($dt->pelapor_tanggal_laporan),
							array(
								'class' => 'input-medium', 
								'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
								'placeholder' => 'Tanggal Laporan',
							))
						}}
						<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Tanggal Investigasi</label>
						<div class="controls">
						{{ Form::text(
							'dp[tanggal_investigasi]',
							(empty($dt->pelapor_tanggal_investigasi))?'':Helper::getDate($dt->pelapor_tanggal_investigasi),
							array(
								'class' => 'input-medium', 
								'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
								'placeholder' => 'Tanggal Investigasi',
							))
						}}
						<span class="help-inline"></span>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
	</div>

	  <div class="row-fluid">
			<div class="span12">
				  <div class="media">
						<fieldset>
						<legend>Data Pasien</legend>
						<div class="control-group">
							  <label class="control-label">Nomor EPID</label>
							  <div class="controls">
							  {{ Form::text(
									'dk[no_epid]',
									(empty($dt->crs_no_epid))?'':$dt->crs_no_epid,
									array(
										  'class' => 'input-xlarge',
										  'placeholder' => 'Nomor EPID (Diisi oleh kabupaten)'
									))
							  }}
							  <span class="help-inline"></span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Nomor RM</label>
							  <div class="controls">
							  {{ Form::text(
									'dpa[no_rm]',
									(empty($dt->pasien_no_rm))?'':$dt->pasien_no_rm,
									Input::old('dpa[no_rm]'),
									array(
										  'class' => 'input-xlarge',
										  'placeholder' => 'Nomor RM'
									))
							  }}
							  <span class="help-inline"></span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Nama Bayi</label>
							  <div class="controls">
							  {{ Form::text(
									'dpa[nama_anak]',
									(empty($dt->pasien_nama_anak))?'':$dt->pasien_nama_anak,
									array(
										  'data-validation' => '[MIXED]',
										  'data' => '$ wajib di isi!',
										  'data-validation-message' => 'wajib di isi!',
										  'class' => 'input-xlarge',
										  'placeholder' => 'Nama Bayi'
									))
							  }}
							  <span class="help-inline" style="color:red">(*)</span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Jenis kelamin</label>
							  <div class="controls">
							  {{Form::select(
									'dpa[jenis_kelamin]', 
									array(
										  '2' => 'Pilih',
										  '0'=>'Laki-laki',
										  '1'=>'Perempuan',
									),
									(empty($dt->pasien_jenis_kelamin))?'':$dt->pasien_jenis_kelamin,
									array(
										  'data-validation'=>'[MIXED]',
										  'data'=>'$ wajib di isi!',
										  'data-validation-message'=>'jenis kelamin wajib di isi!',
										  'class' => 'input-medium id_combobox'
									))
							  }}
							  <span class="help-inline" style="color:red">(*)</span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Tanggal lahir</label>
							  <div class="controls">
							  {{ Form::text(
									'dpa[tanggal_lahir]',
									(empty($dt->pasien_tanggal_lahir))?'':Helper::getDate($dt->pasien_tanggal_lahir),
									array(
										'class' => 'input-medium tgl_lahir', 
										'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
										'placeholder' => 'Tanggal lahir',
										'onchange' => 'umur_pasien()',
									))
							  }}
							  <span class="help-inline" ></span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Umur</label>
							  <div class="controls">
									<input type="text" class="input-mini" value="{{(empty($dt->pasien_umur))?'':$dt->pasien_umur}}" name="dpa[umur]" id="tgl_tahun">Thn
									<input type="text" class="input-mini" value="{{(empty($dt->pasien_umur_bln))?'':$dt->pasien_umur_bln}}" name="dpa[umur_bln]" id="tgl_bln">Bln
									<input type="text" class="input-mini" value="{{(empty($dt->pasien_umur_hr))?'':$dt->pasien_umur_hr}}" name="dpa[umur_hr]" id="tgl_hari">Bln
							  </div>
						</div>
					<!-- awal input kolom provinsi -->
						<div class="control-group">
							<label class="control-label">Provinsi</label>
							<div class="controls">
								{{
									Form::select(
										'dpa[id_provinsi]',
										array('0' => 'Pilih provinsi')+ Provinsi::lists('provinsi','id_provinsi'),
										(empty($dt->pasien_id_provinsi))?'':$dt->pasien_id_provinsi,
										array(
											'id'       => 'id_provinsi_pasien',
											'onchange' => 'show_kabupaten("_pasien")',
											'class'    => 'input-large id_combobox'
										)
									)
								}}
								<span class="help-inline" id="error-id_provinsi_pasien"></span>
							</div>
						</div>
						<!-- awal input kolom kabupaten -->
						<div class="control-group">
							<label class="control-label">Kabupaten</label>
							<div class="controls">
								<?php
									$id_kabupaten = (empty($dt->pasien_id_kabupaten))?'':$dt->pasien_id_kabupaten;
									$kabupaten = (empty($dt->pasien_kabupaten))?'Pilih Kabupaten':$dt->pasien_kabupaten;
								?>
								{{
									Form::select(
										'dpa[id_kabupaten]',
										array($id_kabupaten => $kabupaten),
										null,
										array(
											'class'    => 'input-large id_combobox',
											'id'       => 'id_kabupaten_pasien',
											'onchange' => 'show_kecamatan("_pasien")'
										)
									)
								}}
								<span class="lodingKab"></span>
								<span class="help-inline" id="error-id_kabupaten_pasien"></span>
							</div>
						</div>
						<!-- awal input kolom kecamatan -->
						<div class="control-group">
							<label class="control-label">Kecamatan</label>
							<div class="controls">
								<?php
									$id_kecamatan = (empty($dt->pasien_id_kecamatan))?'':$dt->pasien_id_kecamatan;
									$kecamatan = (empty($dt->pasien_kecamatan))?'Pilih Kecamatan':$dt->pasien_kecamatan;
								?>
								{{
									Form::select(
										'dpa[id_kecamatan]',
										array($id_kecamatan => $kecamatan),
										null,
										array(
											'class'    => 'input-large id_combobox',
											'id'       => 'id_kecamatan_pasien',
											'onchange' => 'show_kelurahan("_pasien")'
										)
									)
								}}
								<span class="lodingKec"></span>
								<span class="help-inline" id="error-id_kecamatan_pasien"></span>
							</div>
						</div>
						<!-- awal input kolom kelurahan -->
						<div class="control-group">
							<label class="control-label">Kelurahan/Desa</label>
							<div class="controls">
								<?php
									$id_kelurahan = (empty($dt->pasien_id_kelurahan))?'':$dt->pasien_id_kelurahan;
									$kelurahan = (empty($dt->pasien_kelurahan))?'Pilih Kelurahan':$dt->pasien_kelurahan;
								?>
								{{
									Form::select(
										'dpa[id_kelurahan]',
										array($id_kelurahan => $kelurahan),
										null,
										array(
											'class' => 'input-large id_combobox',
											'id'    => 'id_kelurahan_pasien'
										)
									)
								}} 
								<span class="lodingKel"></span>
								<span class="help-inline" style="color:red">(*)</span>
							</div>
						</div>
						<div class="control-group">
							  <label class="control-label">Alamat</label>
							  <div class="controls">
							  {{ Form::textarea(
									'dpa[alamat]',
									(empty($dt->pasien_alamat))?'':$dt->pasien_alamat,
									array(
										  'rows' => '3',
										  'placeholder' => 'Hanya diisi nama jalan, no. rumah dan no. RT - RW'
									))
							  }}
							  <span class="help-inline"></span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Tempat bayi di lahirkan</label>
							  <div class="controls">
							  {{ Form::text(
									'dpa[tempat_lahir_bayi]',
									(empty($dt->pasien_tempat_lahir_bayi))?'':$dt->pasien_tempat_lahir_bayi,
									array(
										  'class' => 'input-xlarge',
										  'placeholder' => 'Tempat bayi di lahirkan'
									))
							  }}
							  <span class="help-inline"></span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Nama Ibu</label>
							  <div class="controls">
							  {{ Form::text(
									'dpa[nama_ortu]',
									(empty($dt->pasien_nama_ortu))?'':$dt->pasien_nama_ortu,
									array(
										  'class' => 'input-xlarge',
										  'placeholder' => 'Nama Ibu'
									))
							  }}
							  <span class="help-inline"></span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Nomer Telp</label>
							  <div class="controls">
							  {{ Form::text(
									'dpa[no_telp]',
									(empty($dt->pasien_no_telp))?'':$dt->pasien_no_telp,
									array(
										  'class' => 'input-xlarge',
										  'placeholder' => 'Nomer Telp'
									))
							  }}
							  <span class="help-inline"></span>
							  </div>
						</div>

						<div class="control-group">
							  <label class="control-label">Umur Kehamilan saat bayi di lahirkan</label>
							  <div class="controls">
							  	{{Form::selectRange(
									'dpa[umur_kehamilan_bayi]',
									20, 60,
									(empty($dt->pasien_umur_kehamilan_bayi))?'':$dt->pasien_umur_kehamilan_bayi,
									['class' => 'input-medium id_combobox'])
							 	}}
							  <span class="help-inline"></span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Berat badan bayi baru lahir</label>
							  <div class="controls">
							  {{ Form::text(
									'dpa[berat_badan_bayi]',
									(empty($dt->pasien_berat_badan_bayi))?'':$dt->pasien_berat_badan_bayi,
									array(
										  'class' => 'input-small',
										  'placeholder' => 'Berat badan bayi'
									))
							  }} Gram
							  <span class="help-inline"></span>
							  </div>
						</div>
				  </fieldset>
				  </div>
			</div>
	  </div>

	  <div class="row-fluid">
			<div class="span12">
				  <div class="media">
						<fieldset>
							  <legend>Data Klinis Pasien</legend>
							  <div class="row-fluid">
									<div class="span6">
										  <div class="media">
												<fieldset>
													  <legend>Group A (Lengkapi semua tanda dan gejala yang ada)</legend>
													  <div class="control-group">
															<label class="control-label">Congenital heart disease</label>
															<div class="controls">
															{{Form::select(
																  'dk[congenital_heart_disease]', 
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																  (empty($dt->crs_congenital_heart_disease))?'':$dt->crs_congenital_heart_disease,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Cataracts</label>
															<div class="controls">
															{{Form::select(
																  'dk[cataracts]', 
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																  (empty($dt->crs_cataracts))?'':$dt->crs_cataracts,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Congenital glaucoma</label>
															<div class="controls">
															{{Form::select(
																  'dk[congenital_glaucoma]', 
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																  (empty($dt->crs_congenital_glaucoma))?'':$dt->crs_congenital_glaucoma,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Pigmentary retinopathy</label>
															<div class="controls">
															{{Form::select(
																  'dk[pigmentary_retinopathy]', 
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																  (empty($dt->crs_pigmentary_retinopathy))?'':$dt->crs_pigmentary_retinopathy,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Hearing impairment</label>
															<div class="controls">
															{{Form::select(
																  'dk[hearing_impairment]', 
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																  (empty($dt->crs_hearing_impairment))?'':$dt->crs_hearing_impairment,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
												</fieldset>
										  </div>
										  <div class="control-group">
												<label class="control-label">Nama Dokter Pemeriksa</label>
												<div class="controls">
												{{ Form::text(
													  'dk[nama_dokter_pemeriksa]',
													  (empty($dt->crs_nama_dokter_pemeriksa))?'':$dt->crs_nama_dokter_pemeriksa,
													  array(
															'class' => 'input-xlarge',
															'placeholder' => 'Nama Dokter Pemeriksa'
													  ))
												}}
												<span class="help-inline"></span>
												</div>
										  </div>
										  <div class="control-group">
												<label class="control-label">Tanggal periksa</label>
												<div class="controls">
												{{ Form::text(
													  'dk[tgl_mulai_sakit]',
													  (empty($dt->crs_tgl_mulai_sakit))?'':Helper::getDate($dt->crs_tgl_mulai_sakit),
													  array(
															'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
															'class' => 'input-xlarge tgl_mulai_sakit',
															'id'=>'tgl_mulai_sakit',
															'onchange'=>'umur_pasien()',
															'placeholder' => 'Tanggal Periksa'
													  ))
												}}
												<span class="help-inline"></span>
												</div>
										  </div>
										  <div class="control-group">
												<label class="control-label">Keadaan Bayi saat ini</label>
												<div class="controls">
													<?php
														$keadaanAkhir = (empty($dt->crs_keadaan_akhir))?'':$dt->crs_keadaan_akhir;
													?>
													<table>
														<tr>
															<td>
																<input type="radio" name="dk[keadaan_akhir]" value="Hidup" id="hidup" <?php if($keadaanAkhir== "Hidup") { echo 'checked="checked"'; } ?> >
															</td>
															<td>Hidup</td>
														</tr>
														<tr>
															<td>
																<input type="radio" name="dk[keadaan_akhir]" value="Meninggal" id="meninggal" <?php if($keadaanAkhir== "Meninggal") { echo 'checked="checked"'; } ?> >
															</td>
															<td>Meninggal</td>
															<td>
																{{ Form::text(
																	'dk[penyebab_meninggal]',
																	(empty($dt->crs_penyebab_meninggal))?'':$dt->crs_penyebab_meninggal,
																	['class' => 'input-medium penyebab_meninggal',
																	'placeholder' => 'Penyebab Meninggal',
																	'readonly'=>'readonly']);
																}}
															</td>
														</tr>
													</table>
												<span class="help-inline"></span>
												</div>
										  </div>
									</div>
									<div class="span6">
										  <div class="media">
												<fieldset>
													  <legend>Group B (Lengkapi semua tanda dan gejala yang ada)</legend>
													  <div class="control-group">
															<label class="control-label">Purpura</label>
															<div class="controls">
															{{Form::select(
																'dk[purpura]', 
																array(
																	'' => 'Pilih',
																	'Ya'=>'Ya',
																	'Tidak'=>'Tidak',
																	'Tidak Tahu'=>'Tidak Tahu',
																),
																(empty($dt->crs_purpura))?'':$dt->crs_purpura,
																array(
																	'class' => 'input-medium id_combobox'
																))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Microcephaly</label>
															<div class="controls">
															{{Form::select(
																  'dk[microcephaly]', 
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																(empty($dt->crs_microcephaly))?'':$dt->crs_microcephaly,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Meningoencephalitis</label>
															<div class="controls">
															{{Form::select(
																  'dk[meningoencephalitis]', 
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																(empty($dt->crs_meningoencephalitis))?'':$dt->crs_meningoencephalitis,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Ikterik 24 jam post partum</label>
															<div class="controls">
															{{Form::select(
																  'dk[ikterik]', 
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																(empty($dt->crs_ikterik))?'':$dt->crs_ikterik,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Splenomegaly</label>
															<div class="controls">
															{{Form::select(
																  'dk[splenomegaly]', 
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																(empty($dt->crs_splenomegaly))?'':$dt->crs_splenomegaly,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Developmental delay</label>
															<div class="controls">
															{{Form::select(
																  'dk[developmental_delay]', 
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																(empty($dt->crs_developmental_delay))?'':$dt->crs_developmental_delay,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Radiolucent bone disease</label>
															<div class="controls">
															{{Form::select(
																  'dk[radiolucent]', 
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																		'Tidak Tahu'=>'Tidak Tahu',
																  ),
																(empty($dt->crs_radiolucent))?'':$dt->crs_radiolucent,
																  array(
																		'class' => 'input-medium id_combobox'
																  ))
															}}
															<span class="help-inline"></span>
															</div>
													  </div>
													  <div class="control-group">
															<label class="control-label">Other abnormalities</label>
															<div class="controls">
															{{Form::select(
																  'dk[other_abnormal]', 
																  array(
																		'' => 'Pilih',
																		'Ya'=>'Ya',
																		'Tidak'=>'Tidak',
																  ),
																(empty($dt->crs_other_abnormal))?'':$dt->crs_other_abnormal,
																  array(
																		'onchange'=>'showabnornal()',
																		'class' => 'input-medium id_combobox',
																		'id'=>'other_abnornalities'
																  ))
															}}
															{{ Form::text(
																  'dk[dt_other_abnormal]',
																(empty($dt->crs_dt_other_abnormal))?'':$dt->crs_dt_other_abnormal,
																  array(
																		'class'=>'input-medium other_ab',
																		'style'=>'padding:3px',
																		'readonly'=>'readonly',
																		'placeholder' => 'Other Abnormalities'
																  ))
															  }}
															<span class="help-inline"></span>
															</div>
													  </div>
												</fieldset>
										  </div>
									</div>
							  </div>
							  
						</fieldset>
				  </div>
			</div>
	  </div>
	  <div class="row-fluid">
			<div class="span12">
				  <div class="media">
					  <fieldset>
						<legend>Data Riwayat Kehamilan Ibu</legend>
						<div class="control-group">
							  <label class="control-label">Jumlah kehamilan sebelumnya</label>
							  <div class="controls">
							  {{ Form::text(
								  	'dpe[jumlah_kehamilan_sebelumnya]',
									(empty($dt->pe_jumlah_kehamilan_sebelumnya) || $dt->pe_jumlah_kehamilan_sebelumnya==0)?'':$dt->pe_jumlah_kehamilan_sebelumnya,
								  	array(
									  'placeholder' => 'Jumlah kehamilan sebelumnya',
									  'class'=>'input-medium'
								  	))
							  }}
							  &nbsp;(Dalam bentuk angka)
							  <span class="help-inline"></span>
							  </div>
						  </div>
						  <div class="control-group">
							  <label class="control-label">Umur Ibu (dalam tahun)</label>
							  <div class="controls">
							  {{ Form::text(
								  	'dpe[umur_ibu]',
									(empty($dt->pe_umur_ibu) || $dt->pe_umur_ibu==0)?'':$dt->pe_umur_ibu,
								  	array(
										'placeholder' => 'Umur Ibu (dalam tahun)'
								  	))
							  }}
							  <span class="help-inline"></span>
							  </div>
						  </div>
						  <div class="control-group">
							  <label class="control-label">Hari Pertama haid terakhir (HPHT)</label>
							  <div class="controls">
							  {{ Form::text(
									  'dpe[hari_pertama_haid]',
										(empty($dt->pe_hari_pertama_haid))?'':Helper::getDate($dt->pe_hari_pertama_haid),
									  array(
										  'placeholder' => 'Hari pertama haid terakhir (HPHT)',
									  ))
							  }}
							  <span class="help-inline"></span>
							  </div>
						  </div>
						  <div class="control-group">
							  Apakah selama kehamilan terakhir ini ibu pernah mengalami
						  </div>
						  <div class="control-group">
							  <table>
								  <tr>
									  <td>Conjunctivitis</td>
									  <td>
										  {{Form::select(
												'dpe[conjunctivitis]', 
												array(
													  '' => 'Pilih',
													  'Ya'=>'Ya',
													  'Tidak'=>'Tidak',
													  'Tidak Tahu'=>'Tidak Tahu',
												),
												(empty($dt->pe_conjunctivitis))?'':$dt->pe_conjunctivitis,
												array(
													'id'=>'conjunctivitis',
													'onchange'=>'actiondate(this.id, this.value)',
													'class' => 'input-medium id_combobox'
												))
										  }}
									  </td>
									  <td>
										  {{ Form::text(
											  'dpe[conjunctivitis_date]',
												(empty($dt->pe_conjunctivitis_date))?'':Helper::getDate($dt->pe_conjunctivitis_date),
											  array(
												'class'=>'conjunctivitis_date',
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												'readonly'=>'readonly',
												'placeholder'=>'Tanggal Kejadian'
											  ))
										  }}
									  </td>
								  </tr>
								  <tr>
									  <td>Pilek</td>
									  <td>
										  {{Form::select(
												'dpe[pilek]', 
												array(
													  '' => 'Pilih',
													  'Ya'=>'Ya',
													  'Tidak'=>'Tidak',
													  'Tidak Tahu'=>'Tidak Tahu',
												),
												(empty($dt->pe_pilek))?'':$dt->pe_pilek,
												array(
													'id'=>'pilek',
													'onchange'=>'actiondate(this.id, this.value)',
													'class' => 'input-medium id_combobox'
												))
										  }}
									  </td>
									  <td>
										  {{ Form::text(
											  'dpe[pilek_date]',
												(empty($dt->pe_pilek_date))?'':Helper::getDate($dt->pe_pilek_date),
											  array(
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												  'readonly'=>'readonly',
												  'class'=>'pilek_date',
												  'placeholder'=>'Tanggal Kejadian'
											  ))
										  }}
									  </td>
								  </tr>
								  <tr>
									  <td>Batuk</td>
									  <td>
										  {{Form::select(
												'dpe[batuk]', 
												array(
													  '' => 'Pilih',
													  'Ya'=>'Ya',
													  'Tidak'=>'Tidak',
													  'Tidak Tahu'=>'Tidak Tahu',
												),
												(empty($dt->pe_batuk))?'':$dt->pe_batuk,
												array(
													'id'=>'batuk',
													'onchange'=>'actiondate(this.id, this.value)',
													'class' => 'input-medium id_combobox'
												))
										  }}
									  </td>
									  <td>
										  {{ Form::text(
											  'dpe[batuk_date]',
												(empty($dt->pe_batuk_date))?'':Helper::getDate($dt->pe_batuk_date),
											  array(
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												  'readonly'=>'readonly',
												  'class'=>'batuk_date',
												  'placeholder'=>'Tanggal Kejadian'
											  ))
										  }}
									  </td>
								  </tr>
								  <tr>
									  <td>Ruam makulopapular</td>
									  <td>
										  {{Form::select(
												'dpe[ruam_makulopapular]', 
												array(
													  '' => 'Pilih',
													  'Ya'=>'Ya',
													  'Tidak'=>'Tidak',
													  'Tidak Tahu'=>'Tidak Tahu',
												),
												(empty($dt->pe_ruam_makulopapular))?'':$dt->pe_ruam_makulopapular,
												array(
													'id'=>'ruam_makulopapular',
													'onchange'=>'actiondate(this.id, this.value)',
													'class' => 'input-medium id_combobox'
												))
										  }}
									  </td>
									  <td>
										  {{ Form::text(
											  'dpe[ruam_makulopapular_date]',
												(empty($dt->pe_ruam_makulopapular_date))?'':Helper::getDate($dt->pe_ruam_makulopapular_date),
											  array(
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												  'readonly'=>'readonly',
												  'class'=>'ruam_makulopapular_date',
												  'placeholder'=>'Tanggal Kejadian'
											  ))
										  }}
									  </td>
								  </tr>
								  <tr>
									  <td>Pembengkakan kelenjar limfa</td>
									  <td>
										  {{Form::select(
												'dpe[pembengkakan_kelenjar_limfa]', 
												array(
													  '' => 'Pilih',
													  'Ya'=>'Ya',
													  'Tidak'=>'Tidak',
													  'Tidak Tahu'=>'Tidak Tahu',
												),
												(empty($dt->pe_pembengkakan_kelenjar_limfa))?'':$dt->pe_pembengkakan_kelenjar_limfa,
												array(
													'id'=>'pembengkakan_kelenjar_limfa',
													'onchange'=>'actiondate(this.id, this.value)',
													'class' => 'input-medium id_combobox'
												))
										  }}
									  </td>
									  <td>
										  {{ Form::text(
											  'dpe[pembengkakan_kelenjar_limfa_date]',
												(empty($dt->pe_pembengkakan_kelenjar_limfa_date))?'':Helper::getDate($dt->pe_pembengkakan_kelenjar_limfa_date),
											  array(
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												  'readonly'=>'readonly',
												  'class'=>'pembengkakan_kelenjar_limfa_date',
												  'placeholder'=>'Tanggal Kejadian'
											  ))
										  }}
									  </td>
								  </tr>
								  <tr>
									  <td>Demam</td>
									  <td>
										  {{Form::select(
												'dpe[demam]', 
												array(
													  '' => 'Pilih',
													  'Ya'=>'Ya',
													  'Tidak'=>'Tidak',
													  'Tidak Tahu'=>'Tidak Tahu',
												),
												(empty($dt->pe_demam))?'':$dt->pe_demam,
												array(
													'id'=>'demam',
													'onchange'=>'actiondate(this.id, this.value)',
													'class' => 'input-medium id_combobox'
												))
										  }}
									  </td>
									  <td>
										  {{ Form::text(
											  'dpe[demam_date]',
												(empty($dt->pe_demam_date))?'':Helper::getDate($dt->pe_demam_date),
											  array(
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												  'readonly'=>'readonly',
												  'class'=>'demam_date',
												  'placeholder'=>'Tanggal Kejadian'
											  ))
										  }}
									  </td>
								  </tr>
								  <tr>
									  <td>Arthralgia/arthritis</td>
									  <td>
										  {{Form::select(
												'dpe[arthralgia]', 
												array(
													  '' => 'Pilih',
													  'Ya'=>'Ya',
													  'Tidak'=>'Tidak',
													  'Tidak Tahu'=>'Tidak Tahu',
												),
												(empty($dt->pe_arthralgia))?'':$dt->pe_arthralgia,
												array(
													'id'=>'arthralgia',
													'onchange'=>'actiondate(this.id, this.value)',
													'class' => 'input-medium id_combobox'
												))
										  }}
									  </td>
									  <td>
										  {{ Form::text(
											  'dpe[arthralgia_date]',
												(empty($dt->pe_arthralgia_date))?'':Helper::getDate($dt->pe_arthralgia_date),
											  array(
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												  'readonly'=>'readonly',
												  'class'=>'arthralgia_date',
												  'placeholder'=>'Tanggal Kejadian'
											  ))
										  }}
									  </td>
								  </tr>
								  <tr>
									  <td>Komplikasi lain</td>
									  <td>
										  {{Form::select(
												'dpe[komplikasi_lain]', 
												array(
													  '' => 'Pilih',
													  'Ya'=>'Ya',
													  'Tidak'=>'Tidak',
													  'Tidak Tahu'=>'Tidak Tahu',
												),
												(empty($dt->pe_komplikasi_lain))?'':$dt->pe_komplikasi_lain,
												array(
													'id'=>'komplikasi_lain',
													'onchange'=>'actiondate(this.id, this.value)',
													'class' => 'input-medium id_combobox'
												))
										  }}
									  </td>
									  <td>
										  {{ Form::text(
											  'dpe[komplikasi_lain_date]',
												(empty($dt->pe_komplikasi_lain_date))?'':Helper::getDate($dt->pe_komplikasi_lain_date),
											  array(
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												  'readonly'=>'readonly',
												  'class'=>'komplikasi_lain_date',
												  'placeholder'=>'Tanggal Kejadian'
											  ))
										  }}
									  </td>
								  </tr>
								  <tr>
									  <td>Mendapat Vaksinasi rubella</td>
									  <td>
										  {{Form::select(
												'dpe[vaksinasi_rubella]', 
												array(
													  '' => 'Pilih',
													  'Ya'=>'Ya',
													  'Tidak'=>'Tidak',
													  'Tidak Tahu'=>'Tidak Tahu',
												),
												(empty($dt->pe_vaksinasi_rubella))?'':$dt->pe_vaksinasi_rubella,
												array(
													'id'=>'vaksinasi_rubella',
													'onchange'=>'actiondate(this.id, this.value)',
													'class' => 'input-medium id_combobox'
												))
										  }}
									  </td>
									  <td>
										  {{ Form::text(
											  'dpe[vaksinasi_rubella_date]',
												(empty($dt->pe_vaksinasi_rubella_date))?'':Helper::getDate($dt->pe_vaksinasi_rubella_date),
											  array(
												'data-uk-datepicker'=>'{format:"DD-MM-YYYY"}',
												  'readonly'=>'readonly',
												  'class'=>'vaksinasi_rubella_date',
												  'placeholder'=>'Tanggal Kejadian'
											  ))
										  }}
									  </td>
								  </tr>
							  </table>
						  </div>
						  <div class="control-group">
							  <label class="control-label">Apakah selama kehamilan ini ibu pernah didiagnosa rubella dengan konfirmasi lab</label>
							  <div class="controls">
							  {{Form::select(
									'dpe[diagnosa_rubella]', 
									array(
										  '' => 'Pilih',
										  'Ya'=>'Ya',
										  'Tidak'=>'Tidak',
										  'Tidak Tahu'=>'Tidak Tahu',
									),
									(empty($dt->pe_diagnosa_rubella))?'':$dt->pe_diagnosa_rubella,
									array(
										'id'=>'diagnosa_rubella',
										'onchange'=>'actiondate(this.id, this.value)',
										'class' => 'input-medium id_combobox'
									))
							  }}
							  {{ Form::text(
								  'dpe[tgl_diagnosa_rubella]',
									(empty($dt->pe_tgl_diagnosa_rubella))?'':Helper::getDate($dt->pe_tgl_diagnosa_rubella),
								  array(
										'readonly'=>'readonly',
									  'class' => 'input-large diagnosa_rubella_date', 
									  'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
									  'placeholder' => 'Kapan diagnosa dilakukan',
								  ))
							  }}
							  <span class="help-inline"></span>
							  </div>
						  </div>
						  <div class="control-group">
							  <label class="control-label">Apakah selama kehamilan ini ibu pernah terpapar atau berkontak dengan orang yang menderita ruam makulopapular ?</label>
							  <div class="controls">
							  {{Form::select(
									'dpe[kontak_ruam_makulopapular]', 
									array(
										  '' => 'Pilih',
										  'Ya'=>'Ya',
										  'Tidak'=>'Tidak',
										  'Tidak Tahu'=>'Tidak Tahu',
									),
									(empty($dt->pe_diagnosa_rubella))?'':$dt->pe_diagnosa_rubella,
									array(
										'id'=>'kontak_ruam_makulopapular',
										'onchange'=>'actiondate(this.id, this.value)',
										'class' => 'input-medium id_combobox'
									))
							  }}
							  <span class="help-inline"></span>
							  </div>
						  </div>
						  <div class="control-group">
							  <label class="control-label">Umur Kehamilan saat terpapar dengan penderita ruam makulopapular</label>
							  <div class="controls">
							 	{{Form::selectRange(
									'dpe[umur_hamil_kontak_ruam_makulopapular]',
									1, 60,
									(empty($dt->pe_umur_hamil_kontak_ruam_makulopapular))?'':$dt->pe_umur_hamil_kontak_ruam_makulopapular,
									['class' => 'input-medium id_combobox kontak_ruam_makulopapular_date'])
							  	}}
							  {{ Form::textarea(
								  'dpe[desc_kontak_ruam_makulopapular]',
								  	(empty($dt->pe_desc_kontak_ruam_makulopapular))?'':$dt->pe_desc_kontak_ruam_makulopapular,
								  array(
									'readonly'=>'readonly',
									'rows' => '3',
									  'class' => 'kontak_ruam_makulopapular_date', 
									  'placeholder' => 'Dimana terkena kontak',
								  ))
							  }}
							  <span class="help-inline"></span>
							  </div>
						  </div>
						  <div class="control-group">
							  <label class="control-label">Apakah ibu bepergian selama kehamilan terakhir ini</label>
							  <div class="controls">
							  {{Form::select(
									'dpe[pergi_waktu_hamil]', 
									array(
										  '' => 'Pilih',
										  'Ya'=>'Ya',
										  'Tidak'=>'Tidak',
										  'Tidak Tahu'=>'Tidak Tahu',
									),
									(empty($dt->pe_pergi_waktu_hamil))?'':$dt->pe_pergi_waktu_hamil,
									array(
										'id'=>'pergi_waktu_hamil',
										'onchange'=>'actiondate(this.id, this.value)',
										'class' => 'input-medium id_combobox'
									))
							  }}
							  <span class="help-inline"></span>
							  </div>
						  </div>
						  <div class="control-group">
							  <label class="control-label">Umur kehamilan ketika bepergian</label>
							  <div class="controls">
							  {{Form::selectRange(
									'dpe[umur_hamil_waktu_pergi]',
									1, 60,
									(empty($dt->pe_umur_hamil_waktu_pergi))?'':$dt->pe_umur_hamil_waktu_pergi,
									['class' => 'input-medium id_combobox pergi_waktu_hamil_date'])
							  }}
							  {{ Form::textarea(
								  'dpe[desc_pergi_waktu_hamil]',
								  (empty($dt->pe_desc_pergi_waktu_hamil))?'':$dt->pe_desc_pergi_waktu_hamil,
								  array(
									'readonly'=>'readonly',
									'rows'=>'3',
									  'class' => 'pergi_waktu_hamil_date', 
									  'placeholder' => 'Bepergian kemana',
								  ))
							  }}
							  <span class="help-inline"></span>
							  </div>
						  </div>
					  </fieldset>
				  </div>
			</div>
	  </div>
	  <div class="row-fluid">
		<div class="span12">
			<div class="media">
				<fieldset>
					<legend>Data Pemeriksaan dan Hasil Laboratorium pada bayi</legend>
					<div class="row-fluid">
						<div class="span12">
							<div class="media">
								<fieldset>
									<legend>Pemeriksaan Laboratorium</legend>
									<div class="control-group">
										<label class="control-label">Apakah spesimen diambil</label>
										<div class="controls">
										{{Form::select(
											'dlab[spesimen]', 
											array(
												'' => 'Pilih',
												'Ya'=>'Ya',
												'Tidak'=>'Tidak',
												'Tidak Tahu'=>'Tidak Tahu',
											),
											(empty($dt->lab_spesimen))?'':$dt->lab_spesimen,
											array(
												'class' => 'input-medium id_combobox'
											))
										}}
										<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Jenis Spesimen</label>
										<div class="controls">
											<table>
												<tr>
													<td>Serum 1</td>
													<td>
														{{ Form::text(
															'dlab[tgl_ambil_serum1]',
															(empty($dt->lab_tgl_ambil_serum1))?'':Helper::getDate($dt->lab_tgl_ambil_serum1),
															array(
																'class' => 'input-large', 
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Ambil',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_kirim_serum1]',
															(empty($dt->lab_tgl_kirim_serum1))?'':Helper::getDate($dt->lab_tgl_kirim_serum1),
															array(
																'class' => 'input-large akseskab', 
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal dikirim ke Lab (Di isi Kab/provinsi)',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_tiba_serum1]',
															(empty($dt->lab_tgl_tiba_serum1))?'':Helper::getDate($dt->lab_tgl_tiba_serum1),
															array(
																'class' => 'input-large akseskab', 
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal tiba di lab',
															))
														}}
													</td>
												</tr>
												<tr>
													<td>Serum 2</td>
													<td>
														{{ Form::text(
															'dlab[tgl_ambil_serum2]',
															(empty($dt->lab_tgl_ambil_serum2))?'':Helper::getDate($dt->lab_tgl_ambil_serum2),
															array(
																'class' => 'input-large', 
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Ambil',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_kirim_serum2]',
															(empty($dt->lab_tgl_kirim_serum2))?'':Helper::getDate($dt->lab_tgl_kirim_serum2),
															array(
																'class' => 'input-large akseskab', 
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal dikirim ke Lab (Di isi Kab/provinsi)',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_tiba_serum2]',
															(empty($dt->lab_tgl_tiba_serum2))?'':Helper::getDate($dt->lab_tgl_tiba_serum2),
															array(
																'class' => 'input-large akseskab', 
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal tiba di lab',
															))
														}}
													</td>
												</tr>
												<tr>
													<td>Throat Swab</td>
													<td>
														{{ Form::text(
															'dlab[tgl_ambil_throat_swab]',
															(empty($dt->lab_tgl_ambil_throat_swab))?'':Helper::getDate($dt->lab_tgl_ambil_throat_swab),
															array(
																'class' => 'input-large', 
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Ambil',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_kirim_throat_swab]',
															(empty($dt->lab_tgl_kirim_throat_swab))?'':Helper::getDate($dt->lab_tgl_kirim_throat_swab),
															array(
																'class' => 'input-large akseskab', 
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal dikirim ke Lab (Di isi Kab/provinsi)',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_tiba_throat_swab]',
															(empty($dt->lab_tgl_tiba_throat_swab))?'':Helper::getDate($dt->lab_tgl_tiba_throat_swab),
															array(
																'class' => 'input-large akseskab', 
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal tiba di lab',
															))
														}}
													</td>
												</tr>
												<tr>
													<td>Urine</td>
													<td>
														{{ Form::text(
															'dlab[tgl_ambil_urine]',
															(empty($dt->lab_tgl_ambil_urine))?'':Helper::getDate($dt->lab_tgl_ambil_urine),
															array(
																'class' => 'input-large', 
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Ambil',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_kirim_urine]',
															(empty($dt->lab_tgl_kirim_urine))?'':Helper::getDate($dt->lab_tgl_kirim_urine),
															array(
																'class' => 'input-large akseskab', 
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal dikirim ke Lab (Di isi Kab/provinsi)',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_tiba_urine]',
															(empty($dt->lab_tgl_tiba_urine))?'':Helper::getDate($dt->lab_tgl_tiba_urine),
															array(
																'class' => 'input-large akseskab', 
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal tiba di lab',
															))
														}}
													</td>
												</tr>
											</table>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<div class="media">
								<fieldset>
									<legend>Hasil Pemeriksaan Laboratorium</legend>
									<div class="control-group">
										<div class="controls">
											<table>
												<tr>
													<td><b>Jenis Pemeriksaan</b></td>
													<td colspan="3"><b>Hasil</b></td>
												</tr>
												<tr>
													<td>IgM serum ke 1</td>
													<td>
														{{Form::select(
															  'dlab[hasil_igm_serum1]', 
															  array(
																	'' => 'Pilih',
																	'Positif'=>'Positif',
																	'Negatif'=>'Negatif',
															  ),
																(empty($dt->lab_hasil_igm_serum1))?'':$dt->lab_hasil_igm_serum1,
															  array(
																	'class' => 'input-medium id_combobox'
															  ))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[virus_igm_serum1]',
																(empty($dt->lab_virus_igm_serum1))?'':$dt->lab_virus_igm_serum1,
															array(
																'class' => 'input-large', 
																'placeholder' => 'Jenis Virus',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_igm_serum1]',
															(empty($dt->lab_tgl_igm_serum1))?'':Helper::getDate($dt->lab_tgl_igm_serum1),
															array(
																'class' => 'input-large', 
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Hasil Lab',
															))
														}}
													</td>
												</tr>
												<tr>
													<td>IgM serum 2 (ulangan)</td>
													<td>
														{{Form::select(
															  'dlab[hasil_igm_serum2]', 
															  array(
																	'' => 'Pilih',
																	'Positif'=>'Positif',
																	'Negatif'=>'Negatif',
															  ),
																(empty($dt->lab_hasil_igm_serum2))?'':$dt->lab_hasil_igm_serum2,
															  array(
																	'class' => 'input-medium id_combobox'
															  ))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[virus_igm_serum2]',
																(empty($dt->lab_virus_igm_serum2))?'':$dt->lab_virus_igm_serum2,
															array(
																'class' => 'input-large', 
																'placeholder' => 'Jenis Virus',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_igm_serum2]',
															(empty($dt->lab_tgl_igm_serum2))?'':Helper::getDate($dt->lab_tgl_igm_serum2),
															array(
																'class' => 'input-large', 
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Hasil Lab',
															))
														}}
													</td>
												</tr>
												<tr>
													<td>IgG serum 1</td>
													<td>
														{{Form::select(
															  'dlab[hasil_igg_serum1]', 
															  array(
																	'' => 'Pilih',
																	'Positif'=>'Positif',
																	'Negatif'=>'Negatif',
															  ),
																(empty($dt->lab_hasil_igg_serum1))?'':$dt->lab_hasil_igg_serum1,
															  array(
																	'class' => 'input-medium id_combobox'
															  ))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[virus_igg_serum1]',
																(empty($dt->lab_virus_igg_serum1))?'':$dt->lab_virus_igg_serum1,
															array(
																'class' => 'input-large', 
																'placeholder' => 'Jenis Virus',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_igg_serum1]',
															(empty($dt->lab_tgl_igg_serum1))?'':Helper::getDate($dt->lab_tgl_igg_serum1),
															array(
																'class' => 'input-large', 
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Hasil Lab',
															))
														}}
													</td>
												</tr>
												<tr>
													<td>IgG serum 2 (ulangan)</td>
													<td>
														{{Form::select(
															  'dlab[hasil_igg_serum2]', 
															  array(
																	'' => 'Pilih',
																	'Positif'=>'Positif',
																	'Negatif'=>'Negatif',
															  ),
																(empty($dt->lab_hasil_igg_serum2))?'':$dt->lab_hasil_igg_serum2,
															  array(
																	'class' => 'input-medium id_combobox'
															  ))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[virus_igg_serum2]',
																(empty($dt->lab_virus_igg_serum2))?'':$dt->lab_virus_igg_serum2,
															array(
																'class' => 'input-large', 
																'placeholder' => 'Jenis Virus',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_igg_serum2]',
															(empty($dt->lab_tgl_igg_serum2))?'':Helper::getDate($dt->lab_tgl_igg_serum2),
															array(
																'class' => 'input-large', 
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Hasil Lab',
															))
														}}
													</td>
												</tr>
												<tr>
													<td>Isolasi</td>
													<td>
														{{Form::select(
															  'dlab[hasil_isolasi]', 
															  array(
																	'' => 'Pilih',
																	'Positif'=>'Positif',
																	'Negatif'=>'Negatif',
															  ),
																(empty($dt->lab_hasil_isolasi))?'':$dt->lab_hasil_isolasi,
															  array(
																	'class' => 'input-medium id_combobox'
															  ))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[virus_isolasi]',
																(empty($dt->lab_virus_isolasi))?'':$dt->lab_virus_isolasi,
															array(
																'class' => 'input-large', 
																'placeholder' => 'Jenis Virus',
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_isolasi]',
															(empty($dt->lab_tgl_isolasi))?'':Helper::getDate($dt->lab_tgl_isolasi),
															array(
																'class' => 'input-large', 
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Hasil Lab',
															))
														}}
													</td>
												</tr>
											</table>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
				</fieldset>
				<div class="control-group">
					<label class="control-label">Klasifikasi Final</label>
					<div class="controls">
					{{Form::select(
						  'dk[klasifikasi_final]', 
						  array(
								'' => 'Pilih',
								'CRS pasti(Lab Positif)'=>'CRS pasti(Lab Positif)',
								'CRS Klinis'=>'CRS Klinis',
								'CRI'=>'CRI',
								'Bukan CRS'=>'Bukan CRS',
								'Discarded'=>'Discarded',
						  ),
						  (empty($dt->crs_klasifikasi_final))?'':$dt->crs_klasifikasi_final,
						  array(
								'id'=>'klasifikasi_final',
								'onchange'=>'klasifikasifinal()',
								'class' => 'input-medium id_combobox'
						  ))
					}}
					{{ Form::textarea(
							'dk[klasifikasi_final_desc]',
							(empty($dt->crs_klasifikasi_final_desc))?'':$dt->crs_klasifikasi_final_desc,
							array(
								'id'=>'desc_klasifikasi_final',
								'readonly'=>'readonly',
								'rows' => '3',
								'placeholder' => 'Description'
							))
					  }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="form-actions">
	{{Form::submit('Simpan',array('class'=>'btn btn-primary'))}}
	{{Form::reset('Batal',array('class'=>'btn btn-warning'))}}
</div>
{{Form::close()}}
<input type="hidden" value="<?php echo Sentry::getUser()->hak_akses?>" id='hakakses'>

<script type="text/javascript" charset="utf-8">
	  $('#formSave').validate({
			submit: {
				  settings: {
						scrollToError: {
							  offset: -100,
							  duration: 500
						}
				  }
			}
	  });

	  $('#formSave input').keydown(function(e){
			if(e.keyCode==13){
				  if($(':input:eq(' + ($(':input').index(this) + 1) + ')').attr('type')=='submit'){// check for submit button and submit form on enter press
						return true;
				  }
				  $(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();
				  return false;
			}
	  });

	  $('#hidup').click(function(){
		$('.penyebab_meninggal').attr('readonly','readonly');
	  })

	  $('#meninggal').click(function(){
		$('.penyebab_meninggal').removeAttr('readonly');
	  })


	function showabnornal() {
		var te = $("#other_abnornalities").val();
		if (te == 'Ya') {
			$('.other_ab').removeAttr('readonly');
		} else{
			$('.other_ab').attr('readonly','readonly');
		};
	}
	function klasifikasifinal() {
		var te = $("#klasifikasi_final").val();
		if (te == 'Discarded' || te=='Bukan CRS') {
			$('#desc_klasifikasi_final').removeAttr('readonly');
		} else{
			$('#desc_klasifikasi_final').attr('readonly','readonly');
		};
	}
	function actiondate(dt, val) {
		if (val == 'Ya') {
			$('.'+dt+'_date').removeAttr('readonly');
		} else{
			$('.'+dt+'_date').attr('readonly','readonly');
		};
	}

	function pilihOptionTglLahir()
	{
		$('.tgl_lahir').removeAttr('readonly');
		$('#tgl_tahun').attr('readonly','readonly');
		$('#tgl_bulan').attr('readonly','readonly');
		$('#tgl_hari').attr('readonly','readonly');
		$('.tgl_lahir').val('');
		$('#tgl_tahun').val('');
		$('#tgl_bulan').val('');
		$('#tgl_hari').val('');
	}

	$(document).ready(function(){
		var hakakses = $('#hakakses').val();
		if (hakakses == 2 || hakakses == 7) {
			$('.akseskab').attr('readonly','readonly')
		};
	})
</script>