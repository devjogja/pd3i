<div class="alert alert-error">
	<p style="font-size:12px">Text input yang bertanda bintang <strong>(*)</strong> wajib di isi
</div>
{{ Form::open(
	array(
		'url' =>$response['action'],
		'class' =>'form-horizontal',
		'id'=>'formSave'
	))
}}

<input type="hidden" name="id_crs" id="id_crs" value="">
<input type="hidden" name="" id="tgl_mulai_sakit" value="" >
<div class="module-body">
	<div class="row-fluid">
		<div class="span12">
			<div class="media">
				<fieldset>
					<legend>Data Pelapor</legend>
					<div class="control-group">
						<label class="control-label">Faskes</label>
						<div class="controls">
						{{ Form::text(
							'dp[nama_rs]',
							null,
							array(
								  'class' => 'input-xlarge',
								  'placeholder' => 'Nama Faskes',
								  'id'=> 'nama_faskes',
							))
						}}
							<span class="help-inline" style="color:red">(*)</span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Provinsi</label>
						<div class="controls">
						{{ Form::select(
							'dp[id_provinsi]',
							array('0'=>'Pilih provinsi')+Provinsi::lists('provinsi','id_provinsi'),
							null,
							array(
								'id'=>'id_provinsi_pelapor',
								'placeholder' => 'Pilih Provinsi',
								'onchange'=>'show_kabupaten("_pelapor")',
								'class'=>'input-large id_combobox'
							))
						}}
						<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Kabupaten</label>
						<div class="controls">
						{{ Form::select(
							'dp[id_kabupaten]',
							array(null=>'Pilih Kabupaten'),
							null,
							array(
								'class' => 'input-large id_combobox',
								'id' => 'id_kabupaten_pelapor',
							))
						}}
						<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Tanggal Laporan</label>
						<div class="controls">
						{{ Form::text(
							'dp[tanggal_laporan]',
							null,
							array(
								'class' => 'input-medium',
								'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
								'placeholder' => 'Tanggal Laporan',
								'id'=>'pelapor_tanggal_laporan'
							))
						}}
						<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Tanggal Investigasi</label>
						<div class="controls">
						{{ Form::text(
							'dp[tanggal_investigasi]',
							(empty($dt->pelapor_tanggal_investigasi))?null:Helper::getDate($dt->pelapor_tanggal_investigasi),
							array(
								'class' => 'input-medium',
								'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
								'placeholder' => 'Tanggal Investigasi',
								'id'=>'pelapor_tanggal_investigasi'
							))
						}}
						<span class="help-inline"></span>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
	  <div class="row-fluid">
			<div class="span12">
					<div class="media">
						<fieldset>
						<legend>Data Pasien</legend>
						<div class="control-group">
							  <label class="control-label">Nomor EPID</label>
							  <div class="controls">
							  {{ Form::text(
									'dk[no_epid]',
									null,
									array(
										'id'=>'no_epid',
										'class' => 'input-xlarge',
										'placeholder' => 'Nomor EPID (Diisi oleh kabupaten)',
										'readonly'=>'readonly'
									))
							  }}
							  <span class="help-inline"></span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Nomor RM</label>
							  <div class="controls">
							  {{ Form::text(
									'dpa[no_rm]',
									null,
									Input::old('dpa[no_rm]'),
									array(
										  'class' => 'input-xlarge',
										  'placeholder' => 'Nomor RM',
										  'id'=>'no_rm'
									))
							  }}
							  <span class="help-inline"></span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Nama Bayi</label>
							  <div class="controls">
							  {{ Form::text(
									'dpa[nama_anak]',
									null,
									array(
										  'data-validation' => '[MIXED]',
										  'data' => '$ wajib di isi!',
										  'data-validation-message' => 'wajib di isi!',
										  'class' => 'input-xlarge',
										  'placeholder' => 'Nama Bayi',
										  'id'=>'nama_pasien'
									))
							  }}
							  <span class="help-inline" style="color:red">(*)</span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Jenis kelamin</label>
							  <div class="controls">
							  {{Form::select(
									'dpa[jenis_kelamin]',
									array(
										  '2' => 'Pilih',
										  '0'=>'Laki-laki',
										  '1'=>'Perempuan',
									),
									null,
									array(
										'data-validation'=>'[MIXED]',
										'data'=>'$ wajib di isi!',
										'data-validation-message'=>'jenis kelamin wajib di isi!',
										'class' => 'input-medium id_combobox',
										'id'=>'jenis_kelamin'
									))
							  }}
							  <span class="help-inline" style="color:red">(*)</span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Tanggal lahir</label>
							  <div class="controls">
							  {{ Form::text(
									'dpa[tanggal_lahir]',
									null,
									array(
										'class' => 'input-medium tgl_lahir',
										'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
										'placeholder' => 'Tanggal lahir',
										'onchange' => 'umur_pasien()',
										'id'=>'tanggal_lahir'
									))
							  }}
							  <span class="help-inline" ></span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Umur</label>
							  <div class="controls">
									<input type="text" class="input-mini" value="" name="dpa[umur]" id="tgl_tahun">Thn
									<input type="text" class="input-mini" value="" name="dpa[umur_bln]" id="tgl_bln">Bln
									<input type="text" class="input-mini" value="" name="dpa[umur_hr]" id="tgl_hari">Hari
							  </div>
						</div>

						<div class="control-group">
							<label class="control-label">Jalan/RT/RW/Dusun</label>
							<div class="controls">
								{{Form::textarea(
									'dpa[alamat]',
									null,
									array(
										'rows'=>'3',
										'id'=>'alamat',
										'style'=>'width:270px;',
										'placeholder' => 'Hanya diisi nama jalan, no. rumah dan no. RT - RW'
										)
									)
								}}
								<span class="help-inline" style="color:red"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Cari data wilayah</label>
							<div class="controls">
								{{Form::text('',null,['id'=>'wilayah','class' => 'input-xlarge','placeholder'=>'Ketik nama desa/kecamatan'])}}
								<span class="help-inline" style="color:red">(*)</span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Kelurahan/Desa</label>
							<div class="controls">
								{{Form::text('kelurahan_pasien',null,['id'=>'kelurahan_pasien','class' => 'input-xlarge','placeholder'=>'Kelurahan','readonly'=>'readonly'])}}
								{{Form::hidden('dpa[id_kelurahan]',null,['id'=>'id_kelurahan_pasien'])}}
								<span class="help-inline"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Kecamatan</label>
							<div class="controls">
								{{Form::text('kecamatan_pasien',null,['id'=>'kecamatan_pasien','class' => 'input-xlarge','placeholder'=>'Kecamatan','readonly'=>'readonly'])}}
								{{Form::hidden('dpa[id_kecamatan]',null,['id'=>'id_kecamatan_pasien'])}}
								<span class="help-inline"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Kabupaten</label>
							<div class="controls">
								{{Form::text('kabupaten_pasien',null,['id'=>'kabupaten_pasien','class' => 'input-xlarge','placeholder'=>'Kabupaten','readonly'=>'readonly'])}}
								{{Form::hidden('dpa[id_kabupaten]',null,['id'=>'id_kabupaten_pasien'])}}
								<span class="help-inline"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Provinsi</label>
							<div class="controls">
								{{Form::text('provinsi_pasien',null,['id'=>'provinsi_pasien','class' => 'input-xlarge','placeholder'=>'Provinsi','readonly'=>'readonly'])}}
								{{Form::hidden('dpa[id_provinsi]',null,['id'=>'id_provinsi_pasien'])}}
								<span class="help-inline"></span>
							</div>
						</div>

						<div class="control-group">
							  <label class="control-label">Tempat bayi di lahirkan</label>
							  <div class="controls">
							  {{ Form::text(
									'dpa[tempat_lahir_bayi]',
									null,
									array(
										  'class' => 'input-xlarge',
										  'placeholder' => 'Tempat bayi di lahirkan',
										  'id'=>'tempat_lahir_bayi'
									))
							  }}
							  <span class="help-inline"></span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Nama Ibu</label>
							  <div class="controls">
							  {{ Form::text(
									'dpa[nama_ortu]',
									null,
									array(
										  'class' => 'input-xlarge',
										  'placeholder' => 'Nama Ibu',
										  'id'=>'nama_ortu'
									))
							  }}
							  <span class="help-inline"></span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Nomer Telp</label>
							  <div class="controls">
							  {{ Form::text(
									'dpa[no_telp]',
									null,
									array(
										  'class' => 'input-xlarge',
										  'placeholder' => 'Nomer Telp',
										  'id'=>'no_telp'
									))
							  }}
							  <span class="help-inline"></span>
							  </div>
						</div>

						<div class="control-group">
							  <label class="control-label">Umur Kehamilan saat bayi di lahirkan</label>
							  <div class="controls">
								{{Form::selectRange(
									'dpa[umur_kehamilan_bayi]',
									20, 60,
									null,
									[
										'class' => 'input-medium id_combobox',
										'id'=>'umur_kehamilan_bayi'
									])
								}}
							  <span class="help-inline"></span>
							  </div>
						</div>
						<div class="control-group">
							  <label class="control-label">Berat badan bayi baru lahir</label>
							  <div class="controls">
							  {{ Form::text(
									'dpa[berat_badan_bayi]',
									null,
									array(
										  'class' => 'input-small',
										  'placeholder' => 'Berat badan bayi',
										  'id'=>'berat_badan_bayi'
									))
							  }} Gram
							  <span class="help-inline"></span>
							  </div>
						</div>
				  </fieldset>
				  </div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span12">
				<div class="media">
					<fieldset>
					<legend>Data Pemeriksaan dan Hasil Laboratorium pada bayi (Lab Rumah Sakit)</legend>
					<div class="row-fluid">
						<div class="span12">
							<div class="media">
								<fieldset>
									<legend>Pemeriksaan Laboratorium</legend>
									<div class="control-group">
										<label class="control-label">Apakah spesimen diambil</label>
										<div class="controls">
										{{Form::select(
											'dlab[spesimen]',
											array(
												null => 'Pilih',
												'Ya'=>'Ya',
												'Tidak'=>'Tidak',
												'Tidak Tahu'=>'Tidak Tahu',
											),
											null,
											array(
												'class' => 'input-medium id_combobox',
												'disabled'=>'disabled',
												'id'=>'rs_spesimen'
											))
										}}
										<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Jenis Spesimen</label>
										<div class="controls">
											<table>
												<tr>
													<td>Serum 1</td>
													<td>
														{{ Form::text(
															'dlab[tgl_ambil_serum1]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Ambil',
																'readonly'=>'readonly',
																'id'=>'rs_tgl_ambil_serum1'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_kirim_serum1]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal dikirim ke Lab (Di isi Kab/provinsi)',
																'readonly'=>'readonly',
																'id'=>'rs_tgl_kirim_serum1'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_tiba_serum1]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal tiba di lab',
																'readonly'=>'readonly',
																'id'=>'rs_tgl_tiba_serum1'
															))
														}}
													</td>
												</tr>
												<tr>
													<td>Serum 2</td>
													<td>
														{{ Form::text(
															'dlab[tgl_ambil_serum2]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Ambil',
																'readonly'=>'readonly',
																'id'=>'rs_tgl_ambil_serum2'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_kirim_serum2]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal dikirim ke Lab (Di isi Kab/provinsi)',
																'readonly'=>'readonly',
																'id'=>'rs_tgl_kirim_serum2'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_tiba_serum2]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal tiba di lab',
																'readonly'=>'readonly',
																'id'=>'rs_tgl_tiba_serum2'
															))
														}}
													</td>
												</tr>
												<tr>
													<td>Throat Swab</td>
													<td>
														{{ Form::text(
															'dlab[tgl_ambil_throat_swab]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Ambil',
																'readonly'=>'readonly',
																'id'=>'rs_tgl_ambil_throat_swab'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_kirim_throat_swab]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal dikirim ke Lab (Di isi Kab/provinsi)',
																'readonly'=>'readonly',
																'id'=>'rs_tgl_kirim_throat_swab'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_tiba_throat_swab]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal tiba di lab',
																'readonly'=>'readonly',
																'id'=>'rs_tgl_tiba_throat_swab'
															))
														}}
													</td>
												</tr>
												<tr>
													<td>Urine</td>
													<td>
														{{ Form::text(
															'dlab[tgl_ambil_urine]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Ambil',
																'readonly'=>'readonly',
																'id'=>'rs_tgl_ambil_urine'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_kirim_urine]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal dikirim ke Lab (Di isi Kab/provinsi)',
																'readonly'=>'readonly',
																'id'=>'rs_tgl_kirim_urine'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_tiba_urine]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal tiba di lab',
																'readonly'=>'readonly',
																'id'=>'rs_tgl_tiba_urine'
															))
														}}
													</td>
												</tr>
											</table>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<div class="media">
								<fieldset>
									<legend>Hasil Pemeriksaan Laboratorium</legend>
									<div class="control-group">
										<div class="controls">
											<table>
												<tr>
													<td><b>Jenis Pemeriksaan </b></td>
													<td colspan="3"><b>Hasil</b></td>
												</tr>
												<tr>
													<td>IgM serum ke 1</td>
													<td>
														{{Form::select(
															  'dlab[hasil_igm_serum1]',
															  array(
																	null => 'Pilih',
																	'Positif'=>'Positif',
																	'Negatif'=>'Negatif',
															  ),
																null,
															  array(
																	'class' => 'input-medium id_combobox',
																	'disabled'=>'disabled',
																	'id'=>'rs_hasil_igm_serum1'
															  ))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[virus_igm_serum1]',
																null,
															array(
																'class' => 'input-medium',
																'placeholder' => 'Jenis Virus',
																'readonly'=>'readonly',
																'id'=>'rs_virus_igm_serum1'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_igm_serum1]',
															null,
															array(
																'class' => 'input-medium',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Hasil Lab',
																'readonly'=>'readonly',
																'id'=>'rs_tgl_igm_serum1'
															))
														}}
													</td>
												</tr>
												<tr>
													<td>IgM serum 2 (ulangan)</td>
													<td>
														{{Form::select(
															  'dlab[hasil_igm_serum2]',
															  array(
																	null => 'Pilih',
																	'Positif'=>'Positif',
																	'Negatif'=>'Negatif',
															  ),
																null,
															  array(
																	'class' => 'input-medium id_combobox',
																	'disabled'=>'disabled',
																	'id'=>'rs_hasil_igm_serum2'
															  ))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[virus_igm_serum2]',
																null,
															array(
																'class' => 'input-medium',
																'placeholder' => 'Jenis Virus',
																'readonly'=>'readonly',
																'id'=>'rs_virus_igm_serum2'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_igm_serum2]',
															null,
															array(
																'class' => 'input-medium',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Hasil Lab',
																'readonly'=>'readonly',
																'id'=>'rs_tgl_igm_serum2'
															))
														}}
													</td>
												</tr>
												<tr>
													<td>IgG serum 1</td>
													<td>
														{{Form::select(
															  'dlab[hasil_igg_serum1]',
															  array(
																	null => 'Pilih',
																	'Positif'=>'Positif',
																	'Negatif'=>'Negatif',
															  ),
																null,
															  array(
																	'class' => 'input-medium id_combobox',
																	'disabled'=>'disabled',
																	'id'=>'rs_hasil_igg_serum1'
															  ))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[virus_igg_serum1]',
																null,
															array(
																'class' => 'input-medium',
																'placeholder' => 'Jenis Virus',
																'readonly'=>'readonly',
																'id'=>'rs_virus_igg_serum1'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_igg_serum1]',
															null,
															array(
																'class' => 'input-medium',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Hasil Lab',
																'readonly'=>'readonly',
																'id'=>'rs_tgl_igg_serum1'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_igg_serum1]',
															null,
															array(
																'class' => 'input-medium',
																'placeholder' => 'Kadar IgG',
																'readonly'=>'readonly',
																'id'=>'rs_kadar_igg_serum1'
															))
														}}
													</td>
												</tr>
												<tr>
													<td>IgG serum 2 (ulangan)</td>
													<td>
														{{Form::select(
															  'dlab[hasil_igg_serum2]',
															  array(
																	null => 'Pilih',
																	'Positif'=>'Positif',
																	'Negatif'=>'Negatif',
															  ),
																null,
															  array(
																	'class' => 'input-medium id_combobox',
																	'disabled'=>'disabled',
																	'id'=>'rs_hasil_igg_serum2'
															  ))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[virus_igg_serum2]',
																null,
															array(
																'class' => 'input-medium',
																'placeholder' => 'Jenis Virus',
																'readonly'=>'readonly',
																'id'=>'rs_virus_igg_serum2'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_igg_serum2]',
															null,
															array(
																'class' => 'input-medium',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Hasil Lab',
																'readonly'=>'readonly',
																'id'=>'rs_tgl_igg_serum2'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_igg_serum1]',
															null,
															array(
																'class' => 'input-medium',
																'placeholder' => 'Kadar IgG',
																'readonly'=>'readonly',
																'id'=>'rs_kadar_igg_serum2'
															))
														}}
													</td>
												</tr>
												<tr>
													<td>Isolasi</td>
													<td>
														{{Form::select(
															  'dlab[hasil_isolasi]',
															  array(
																	null => 'Pilih',
																	'Positif'=>'Positif',
																	'Negatif'=>'Negatif',
															  ),
																null,
															  array(
																	'class' => 'input-medium id_combobox',
																	'disabled'=>'disabled',
																	'id'=>'rs_hasil_isolasi'
															  ))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[virus_isolasi]',
																null,
															array(
																'class' => 'input-medium',
																'placeholder' => 'Jenis Virus',
																'readonly'=>'readonly',
																'id'=>'rs_virus_isolasi'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[tgl_isolasi]',
															null,
															array(
																'class' => 'input-medium',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Hasil Lab',
																'readonly'=>'readonly',
																'id'=>'rs_tgl_isolasi'
															))
														}}
													</td>
												</tr>
											</table>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
				</fieldset>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span12">
				<div class="media">
					<fieldset>
					<legend>Data Pemeriksaan dan Hasil Laboratorium pada bayi</legend>
					<div class="row-fluid">
						<div class="span12">
							<div class="media">
								<fieldset>
									<legend>Pemeriksaan Laboratorium</legend>
									<div class="control-group">
										<label class="control-label">No.Laboratorium</label>
										<div class="controls">
										{{ Form::text(
											'dlab[no_laboratorium]',
											null,
											array(
												'class' => 'input-large',
												'placeholder' => 'No.Laboratorium',
												'id'=>'lab_no_laboratorium'
											))
										}}
										<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Apakah spesimen diambil</label>
										<div class="controls">
										{{Form::select(
											'dlab[lab_spesimen]',
											array(
												null => 'Pilih',
												'Ya'=>'Ya',
												'Tidak'=>'Tidak',
												'Tidak Tahu'=>'Tidak Tahu',
											),
											null,
											array(
												'class' => 'input-medium id_combobox',
												'id'=>'ambilSpesimen'
											))
										}}
										<span class="help-inline"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Jenis Spesimen</label>
										<div class="controls">
											<table>
												<tr>
													<td>Serum 1</td>
													<td>
														{{ Form::text(
															'dlab[lab_tgl_ambil_serum1]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Ambil',
																'id'=>'lab_tgl_ambil_serum1'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[lab_tgl_kirim_serum1]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal dikirim ke Lab (Di isi Kab/provinsi)',
																'id'=>'lab_tgl_kirim_serum1'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[lab_tgl_tiba_serum1]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal tiba di lab',
																'id'=>'lab_tgl_tiba_serum1'
															))
														}}
													</td>
												</tr>
												<tr>
													<td>Serum 2</td>
													<td>
														{{ Form::text(
															'dlab[lab_tgl_ambil_serum2]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Ambil',
																'id'=>'lab_tgl_ambil_serum2'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[lab_tgl_kirim_serum2]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal dikirim ke Lab (Di isi Kab/provinsi)',
																'id'=>'lab_tgl_kirim_serum2'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[lab_tgl_tiba_serum2]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal tiba di lab',
																'id'=>'lab_tgl_tiba_serum2'
															))
														}}
													</td>
												</tr>
												<tr>
													<td>Throat Swab</td>
													<td>
														{{ Form::text(
															'dlab[lab_tgl_ambil_throat_swab]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Ambil',
																'id'=>'lab_tgl_ambil_throat_swab'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[lab_tgl_kirim_throat_swab]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal dikirim ke Lab (Di isi Kab/provinsi)',
																'id'=>'lab_tgl_kirim_throat_swab'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[lab_tgl_tiba_throat_swab]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal tiba di lab',
																'id'=>'lab_tgl_tiba_throat_swab'
															))
														}}
													</td>
												</tr>
												<tr>
													<td>Urine</td>
													<td>
														{{ Form::text(
															'dlab[lab_tgl_ambil_urine]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Ambil',
																'id'=>'lab_tgl_ambil_urine'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[lab_tgl_kirim_urine]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal dikirim ke Lab (Di isi Kab/provinsi)',
																'id'=>'lab_tgl_kirim_urine'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[lab_tgl_tiba_urine]',
															null,
															array(
																'class' => 'input-large',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal tiba di lab',
																'id'=>'lab_tgl_tiba_urine'
															))
														}}
													</td>
												</tr>
											</table>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<div class="media">
								<fieldset>
									<legend>Hasil Pemeriksaan Laboratorium</legend>
									<div class="control-group">
										<div class="controls">
											<table>
												<tr>
													<td><b>Jenis Pemeriksaan</b></td>
													<td colspan="3"><b>Hasil</b></td>
												</tr>
												<tr>
													<td>IgM serum ke 1</td>
													<td>
														{{Form::select(
															  'dlab[lab_hasil_igm_serum1]',
															  array(
																	null => 'Pilih',
																	'Positif'=>'Positif',
																	'Negatif'=>'Negatif',
															  ),
																null,
															  array(
																	'class' => 'input-medium id_combobox',
																	'onchange'=>'activeKlasifikasiFinal()',
																	'id'=>'lab_hasil_igm_serum1'
															  ))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[lab_virus_igm_serum1]',
																null,
															array(
																'class' => 'input-medium',
																'placeholder' => 'Jenis Virus',
																'id'=>'lab_virus_igm_serum1'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[lab_tgl_igm_serum1]',
															null,
															array(
																'class' => 'input-medium',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Hasil Lab',
																'id'=>'lab_tgl_igm_serum1'
															))
														}}
													</td>
												</tr>
												<tr>
													<td>IgM serum 2 (ulangan)</td>
													<td>
														{{Form::select(
															  'dlab[lab_hasil_igm_serum2]',
															  array(
																	null => 'Pilih',
																	'Positif'=>'Positif',
																	'Negatif'=>'Negatif',
															  ),
																null,
															  array(
																	'class' => 'input-medium id_combobox',
																	'onchange'=>'activeKlasifikasiFinal()',
																	'id'=>'lab_hasil_igm_serum2'
															  ))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[lab_virus_igm_serum2]',
																null,
															array(
																'class' => 'input-medium',
																'placeholder' => 'Jenis Virus',
																'id'=>'lab_virus_igm_serum2'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[lab_tgl_igm_serum2]',
															null,
															array(
																'class' => 'input-medium',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Hasil Lab',
																'id'=>'lab_tgl_igm_serum2'
															))
														}}
													</td>
												</tr>
												<tr>
													<td>IgG serum 1</td>
													<td>
														{{Form::select(
															  'dlab[lab_hasil_igg_serum1]',
															  array(
																	null => 'Pilih',
																	'Positif'=>'Positif',
																	'Negatif'=>'Negatif',
															  ),
																null,
															  array(
																	'class' => 'input-medium id_combobox',
																	'onchange'=>'activeKlasifikasiFinal()',
																	'id'=>'lab_hasil_igg_serum1'
															  ))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[lab_virus_igg_serum1]',
																(empty($dt->lab_virus_igg_serum1))?null:$dt->lab_virus_igg_serum1,
															array(
																'class' => 'input-medium',
																'placeholder' => 'Jenis Virus',
																'id'=>'lab_virus_igg_serum1'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[lab_tgl_igg_serum1]',
															null,
															array(
																'class' => 'input-medium',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Hasil Lab',
																'id'=>'lab_tgl_igg_serum1'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[lab_kadar_igg_serum1]',
															null,
															array(
																'class' => 'input-medium',
																'placeholder' => 'Kadar IgG',
																'id'=>'lab_kadar_igg_serum1'
															))
														}}
													</td>
												</tr>
												<tr>
													<td>IgG serum 2 (ulangan)</td>
													<td>
														{{Form::select(
															  'dlab[lab_hasil_igg_serum2]',
															  array(
																	null => 'Pilih',
																	'Positif'=>'Positif',
																	'Negatif'=>'Negatif',
															  ),
																null,
															  array(
																	'class' => 'input-medium id_combobox',
																	'onchange'=>'activeKlasifikasiFinal()',
																	'id'=>'lab_hasil_igg_serum2'
															  ))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[lab_virus_igg_serum2]',
																null,
															array(
																'class' => 'input-medium',
																'placeholder' => 'Jenis Virus',
																'id'=>'lab_virus_igg_serum2'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[lab_tgl_igg_serum2]',
															null,
															array(
																'class' => 'input-medium',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Hasil Lab',
																'id'=>'lab_tgl_igg_serum2'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[lab_kadar_igg_serum2]',
															null,
															array(
																'class' => 'input-medium',
																'placeholder' => 'Kadar IgG',
																'id'=>'lab_kadar_igg_serum2'
															))
														}}
													</td>
												</tr>
												<tr>
													<td>Isolasi</td>
													<td>
														{{Form::select(
															  'dlab[lab_hasil_isolasi]',
															  array(
																	null => 'Pilih',
																	'Positif'=>'Positif',
																	'Negatif'=>'Negatif',
															  ),
																null,
															  array(
																	'class' => 'input-medium id_combobox',
																	'onchange'=>'activeKlasifikasiFinal()',
																	'id'=>'lab_hasil_isolasi'
															  ))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[lab_virus_isolasi]',
																null,
															array(
																'class' => 'input-medium',
																'placeholder' => 'Jenis Virus',
																'id'=>'lab_virus_isolasi'
															))
														}}
													</td>
													<td>
														{{ Form::text(
															'dlab[lab_tgl_isolasi]',
															null,
															array(
																'class' => 'input-medium',
																'data-uk-datepicker' => '{format:"DD-MM-YYYY"}',
																'placeholder' => 'Tanggal Hasil Lab',
																'id'=>'lab_tgl_isolasi'
															))
														}}
													</td>
												</tr>
											</table>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
				</fieldset>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span12">
				<div class="media">
					<fieldset>
					<legend>Klasifikasi Final</legend>
						<div class="control-group">
							  <label class="control-label">Klasifikasi Final</label>
							  <div class="controls">
								{{Form::select(
									  'dk[klasifikasi_final]',
									  array(
											null => 'Pilih',
											'CRS pasti(Lab Positif)'=>'CRS pasti(Lab Positif)',
											'CRS Klinis'=>'CRS Klinis',
											'CRI'=>'CRI',
											'Bukan CRS'=>'Bukan CRS',
											'Discarded'=>'Discarded',
									  ),
									  null,
									  array(
											'id'=>'klasifikasi_final',
											'onchange'=>'klasifikasifinal()',
											'class' => 'input-medium id_combobox',
											'disabled'=>'disabled'
									  ))
								}}
								{{ Form::textarea(
									'dk[klasifikasi_final_desc]',
									null,
									array(
										'id'=>'desc_klasifikasi_final',
										'readonly'=>'readonly',
										'rows' => '3',
										'placeholder' => 'Description'
									))
								}}
								<span class="help-inline"></span>
								</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
	</div>
	<div class="form-actions">
		{{Form::submit('Simpan',array('class'=>'btn btn-primary','id'=>'submitForm'))}}
		<a href="{{URL::to('labs/crs')}}" class="btn btn-warning">Batal</a>
	</div>
</div>
{{Form::close()}}
<input type="hidden" value="<?php echo Sentry::getUser()->hak_akses?>" id='hakakses'>

<script type="text/javascript" charset="utf-8">
$(function(){
	setKlasifikasiFinal();
	getDetail();

	$('#wilayah').autocomplete({
		source:"{{URL::to('getArea')}}",
		minLength:2,
		select:function(evt, ui)
		{
			$('#kelurahan_pasien').val(ui.item.kelurahan);
			$('#kecamatan_pasien').val(ui.item.kecamatan);
			$('#kabupaten_pasien').val(ui.item.kabupaten);
			$('#provinsi_pasien').val(ui.item.provinsi);
			$('#id_kelurahan_pasien').val(ui.item.id_kelurahan);
			$('#id_kecamatan_pasien').val(ui.item.id_kecamatan);
			$('#id_kabupaten_pasien').val(ui.item.id_kabupaten);
			$('#id_provinsi_pasien').val(ui.item.id_provinsi);
			$('#wilayah').val(null);
			return false;
		}
	})
	.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		return $("<li>")
		.append("<a>"
			+item.kelurahan +"<br><small>Kelurahan: <i>"
			+item.kelurahan +"</i><br>Kecamatan: "
			+item.kecamatan+"<br>Kabupaten: "
			+item.kabupaten+"<br>Provinsi: "
			+item.provinsi+"</small></a>")
		.appendTo( ul );
	};
});

	function activeKlasifikasiFinal() {
		var spesimen = $('#ambilSpesimen').val();
		var hasil_igm_serum1 = $('#lab_hasil_igm_serum1').val();
		var hasil_igm_serum2 = $('#lab_hasil_igm_serum2').val();
		var hasil_igg_serum1 = $('#lab_hasil_igg_serum1').val();
		var hasil_igg_serum2 = $('#lab_hasil_igg_serum2').val();
		var hasil_isolasi = $('#lab_hasil_isolasi').val();
		if(spesimen=='Ya'&&(hasil_igm_serum1!=null||hasil_igm_serum2!=null||hasil_igg_serum1!=null||hasil_igg_serum2!=null||hasil_isolasi!=null)){
			setKlasifikasiFinal();
		}else{
			$('#klasifikasi_final').attr('disabled','disabled');
		}
	}

	function setKlasifikasiFinal() {
		var hasil_igm_serum1 = $('#lab_hasil_igm_serum1').val();
		var hasil_igm_serum2 = $('#lab_hasil_igm_serum2').val();
		var hasil_igg_serum1 = $('#lab_hasil_igg_serum1').val();
		var hasil_igg_serum2 = $('#lab_hasil_igg_serum2').val();
		if(hasil_igm_serum1=='Positif'){
			$('#klasifikasi_final').removeAttr('disabled');
			$('#klasifikasi_final').select2('val','CRS pasti(Lab Positif)');
		}else if(hasil_igm_serum1){
			$('#klasifikasi_final').select2('val','Bukan CRS');
		}
	}

	function showabnornal() {
		var te = $("#other_abnornalities").val();
		if (te == 'Ya') {
			$('.other_ab').removeAttr('readonly');
		} else{
			$('.other_ab').attr('readonly','readonly');
		};
	}
	function klasifikasifinal() {
		var te = $("#klasifikasi_final").val();
		if (te == 'Discarded' || te=='Bukan CRS') {
			$('#desc_klasifikasi_final').removeAttr('readonly');
		} else{
			$('#desc_klasifikasi_final').attr('readonly','readonly');
		};
	}

	function getDetail() {
		var id = "<?=$response['id']?>";
		$.ajax({
			method: "POST",
			url: "{{URL::to('getDataLabCrs')}}",
			data: {id:id}
		})
		.done(function(response) {
			if (response!=null){
				$('#id_crs').val(response.data.crs_id_crs);
				// Data pelapor
				$('#nama_faskes').val(response.data.pelapor_nama_rs);
				$('#id_provinsi_pelapor').select2('val',response.data.pelapor_id_provinsi);
				show_kabupaten('_pelapor',response.data.pelapor_id_kabupaten);
				$('#pelapor_tanggal_laporan').val(response.data.pelapor_tanggal_laporan);
				$('#pelapor_tanggal_investigasi').val(response.data.pelapor_tanggal_investigasi);
				// Data pasien
				$('#no_epid').val(response.data.crs_no_epid);
				$('#no_rm').val(response.data.crs_no_rm);
				$('#nama_pasien').val(response.data.pasien_nama_anak);
				$('#jenis_kelamin').select2('val',response.data.pasien_jenis_kelamin);
				$('#tanggal_lahir').val(response.data.pasien_tanggal_lahir);
				$('#tgl_tahun').val(response.data.pasien_umur);
				$('#tgl_bln').val(response.data.pasien_umur_bln);
				$('#tgl_hari').val(response.data.pasien_umur_hr);
				$('#alamat').val(response.data.pasien_alamat);
				$('#kelurahan_pasien').val(response.data.pasien_kelurahan);
				$('#kecamatan_pasien').val(response.data.pasien_kecamatan);
				$('#kabupaten_pasien').val(response.data.pasien_kabupaten);
				$('#provinsi_pasien').val(response.data.pasien_provinsi);
				$('#id_kelurahan_pasien').val(response.data.pasien_id_kelurahan);
				$('#id_kecamatan_pasien').val(response.data.pasien_id_kecamatan);
				$('#id_kabupaten_pasien').val(response.data.pasien_id_kabupaten);
				$('#id_provinsi_pasien').val(response.data.pasien_id_provinsi);
				$('#tempat_lahir_bayi').val(response.data.pasien_tempat_lahir_bayi);
				$('#nama_ortu').val(response.data.pasien_nama_ortu);
				$('#no_telp').val(response.data.pasien_no_telp);
				$('#umur_kehamilan_bayi').select2('val',response.data.pasien_umur_kehamilan_bayi);
				$('#berat_badan_bayi').val(response.data.pasien_berat_badan_bayi);
				// Data pemeriksaan lab rs
				$('#rs_spesimen').select2('val',response.data.rs_spesimen);
				$('#rs_tgl_ambil_serum1').val(response.data.rs_tgl_ambil_serum1);
				$('#rs_tgl_kirim_serum1').val(response.data.rs_tgl_kirim_serum1);
				$('#rs_tgl_tiba_serum1').val(response.data.rs_tgl_tiba_serum1);
				$('#rs_tgl_ambil_serum2').val(response.data.rs_tgl_ambil_serum2);
				$('#rs_tgl_kirim_serum2').val(response.data.rs_tgl_kirim_serum2);
				$('#rs_tgl_tiba_serum2').val(response.data.rs_tgl_tiba_serum2);
				$('#rs_tgl_ambil_throat_swab').val(response.data.rs_tgl_ambil_throat_swab);
				$('#rs_tgl_kirim_throat_swab').val(response.data.rs_tgl_kirim_throat_swab);
				$('#rs_tgl_tiba_throat_swab').val(response.data.rs_tgl_tiba_throat_swab);
				$('#rs_tgl_ambil_urine').val(response.data.rs_tgl_ambil_urine);
				$('#rs_tgl_kirim_urine').val(response.data.rs_tgl_kirim_urine);
				$('#rs_tgl_tiba_urine').val(response.data.rs_tgl_tiba_urine);
				$('#rs_hasil_igm_serum1').select2('val',response.data.rs_hasil_igm_serum1);
				$('#rs_virus_igm_serum1').val(response.data.rs_virus_igm_serum1);
				$('#rs_tgl_igm_serum1').val(response.data.rs_tgl_igm_serum1);
				$('#rs_hasil_igm_serum2').select2('val',response.data.rs_hasil_igm_serum2);
				$('#rs_virus_igm_serum2').val(response.data.rs_virus_igm_serum2);
				$('#rs_tgl_igm_serum2').val(response.data.rs_tgl_igm_serum2);
				$('#rs_hasil_igg_serum1').select2('val',response.data.rs_hasil_igg_serum1);
				$('#rs_virus_igg_serum1').val(response.data.rs_virus_igg_serum1);
				$('#rs_tgl_igg_serum1').val(response.data.rs_tgl_igg_serum1);
				$('#rs_kadar_igg_serum1').val(response.data.rs_kadar_igg_serum1);
				$('#rs_hasil_igg_serum2').select2('val',response.data.rs_hasil_igg_serum2);
				$('#rs_virus_igg_serum2').val(response.data.rs_virus_igg_serum2);
				$('#rs_tgl_igg_serum2').val(response.data.rs_tgl_igg_serum2);
				$('#rs_kadar_igg_serum2').val(response.data.rs_kadar_igg_serum2);
				$('#rs_hasil_isolasi').select2('val',response.data.rs_hasil_isolasi);
				$('#rs_virus_isolasi').val(response.data.rs_virus_isolasi);
				$('#rs_tgl_isolasi').val(response.data.rs_tgl_isolasi);

				$('#lab_no_laboratorium').val(response.data.lab_no_laboratorium);
				$('#lab_spesimen').select2('val',response.data.lab_spesimen);
				$('#lab_tgl_ambil_serum1').val(response.data.lab_tgl_ambil_serum1);
				$('#lab_tgl_kirim_serum1').val(response.data.lab_tgl_kirim_serum1);
				$('#lab_tgl_tiba_serum1').val(response.data.lab_tgl_tiba_serum1);
				$('#lab_tgl_ambil_serum2').val(response.data.lab_tgl_ambil_serum2);
				$('#lab_tgl_kirim_serum2').val(response.data.lab_tgl_kirim_serum2);
				$('#lab_tgl_tiba_serum2').val(response.data.lab_tgl_tiba_serum2);
				$('#lab_tgl_ambil_throat_swab').val(response.data.lab_tgl_ambil_throat_swab);
				$('#lab_tgl_kirim_throat_swab').val(response.data.lab_tgl_kirim_throat_swab);
				$('#lab_tgl_tiba_throat_swab').val(response.data.lab_tgl_tiba_throat_swab);
				$('#lab_tgl_ambil_urine').val(response.data.lab_tgl_ambil_urine);
				$('#lab_tgl_kirim_urine').val(response.data.lab_tgl_kirim_urine);
				$('#lab_tgl_tiba_urine').val(response.data.lab_tgl_tiba_urine);
				$('#lab_hasil_igm_serum1').select2('val',response.data.lab_hasil_igm_serum1);
				$('#lab_virus_igm_serum1').val(response.data.lab_virus_igm_serum1);
				$('#lab_tgl_igm_serum1').val(response.data.lab_tgl_igm_serum1);
				$('#lab_hasil_igm_serum2').select2('val',response.data.lab_hasil_igm_serum2);
				$('#lab_virus_igm_serum2').val(response.data.lab_virus_igm_serum2);
				$('#lab_tgl_igm_serum2').val(response.data.lab_tgl_igm_serum2);
				$('#lab_hasil_igg_serum1').select2('val',response.data.lab_hasil_igg_serum1);
				$('#lab_virus_igg_serum1').val(response.data.lab_virus_igg_serum1);
				$('#lab_tgl_igg_serum1').val(response.data.lab_tgl_igg_serum1);
				$('#lab_kadar_igg_serum1').val(response.data.lab_kadar_igg_serum1);
				$('#lab_hasil_igg_serum2').select2('val',response.data.lab_hasil_igg_serum2);
				$('#lab_virus_igg_serum2').val(response.data.lab_virus_igg_serum2);
				$('#lab_tgl_igg_serum2').val(response.data.lab_tgl_igg_serum2);
				$('#lab_kadar_igg_serum2').val(response.data.lab_kadar_igg_serum2);
				$('#lab_hasil_isolasi').select2('val',response.data.lab_hasil_isolasi);
				$('#lab_virus_isolasi').val(response.data.lab_virus_isolasi);
				$('#lab_tgl_isolasi').val(response.data.lab_tgl_isolasi);
				$('#tgl_mulai_sakit').val(response.data.crs_tgl_mulai_sakit);

				$('#klasifikasi_final').val(response.data.crs_klasifikasi_final);
				$('#desc_klasifikasi_final').val(response.data.crs_klasifikasi_final_desc);
			}
		});
	}
</script>
