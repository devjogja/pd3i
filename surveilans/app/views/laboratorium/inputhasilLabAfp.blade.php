<!-- js untuk post data hasil lab -->
<script>
$(document).ready(function(){
	$('#simpan_hasil_lab').submit(function() {
		loading_msg();
		$('#simpan_hasil_lab').ajaxSubmit({
			type: 'POST',
			success:function(data) {
				$('#msg').html('');
				$('#msg').html(data.msg);
				if(data.msg == 'tersimpan')location.href  = '{{URL::to("lab/afp")}}';
			}
		});
		return false;
	});
});

function loading_state(){
	$('#status').html('Loading...');
}

function loading_msg(){
	$('#msg').html('Loading...');
}	

</script>


<h4 style="text-align:center;">data kasus individual penyakit afp</h4>
<div class="tabbable-panel">
	<div class="tabbable-line">
		<ul class="nav nav-tabs ">
			<li class="active">
				<a href="#tab_default_1" data-toggle="tab">
				Input data individual kasus </a>
			</li>
			<li>
				<a href="#tab_default_2" data-toggle="tab">
				Input hasil laboratorium </a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_default_1">				
				{{Form::open(array('url'=>'afp_update','class'=>'form-horizontal'))}}
				<div class="module-body">
				  <div class="row-fluid">
				    <div class="span6">
				      <div class="media">
				      @foreach($afp as $row)
				        <fieldset>
				        <legend>Identitas pasien</legend>
				        <div class="control-group">
				            <label class="control-label">No Epidemologi</label>
				            <div class="controls">
				             {{Form::text('no_epid',$row->no_epid, array('class' => 'input-medium','placeholder'=>'Nomer epidemologi','id'=>'epid'))}}
				             {{Form::hidden('id_hasil_uji_lab_afp',$row->id_hasil_uji_lab_afp)}}
				            <span class="help-inline"></span>
				            </div>
				          </div>
				          <div class="control-group">
				            <label class="control-label">NIK</label>
				            <div class="controls">
				              {{Form::text('nik',$row->nik, array('class' => 'input-medium','placeholder'=>'Nomer induk kependudukan'))}}
				              <span class="help-inline"></span>
				            </div>
				          </div>
				          <div class="control-group">
				            <label class="control-label">Nama anak</label>
				            <div class="controls">
				              {{Form::text('nama_anak',$row->nama_anak, array('class' => 'input-xlarge','placeholder'=>'Nama anak'))}}
				              <span class="help-inline"></span>
				            </div>
				          </div>
				          <div class="control-group">
				            <label class="control-label">Nama orang tua</label>
				            <div class="controls">
				              {{Form::text('nama_ortu',$row->nama_ortu, array('class' => 'input-xlarge','placeholder'=>'Nama orang tua'))}}
				              <span class="help-inline"></span>
				            </div>
				          </div>
				          <div class="control-group">
				            <label class="control-label">Jenis kelamin</label>
				            <div class="controls">
				              {{Form::select('jenis_kelamin',['' => 'Pilih','0'=>'Laki-laki','1'=>'Perempuan','3' => 'Tidak diketahui','4'=>'Lainnya'],[$row->jenis_kelamin], array('class' => 'input-medium'))}}
				              <span class="help-inline"></span>
				            </div>
				          </div>
				          <div class="control-group">
				            <label class="control-label">Tanggal lahir</label>
				            <div class="controls">
				              {{Form::text('tanggal_lahir',$row->tanggal_lahir, array('class' => 'input-medium tgl_lahir','placeholder'=>'Tanggal lahir','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}','onchange'=>'umur()'))}}
				              <span class="help-inline"></span>
				            </div>
				          </div>
				          <div class="control-group">
				            <div class="controls">
				            <input type="text" class="input-mini"  value="{{$row->umur}}" name="umur" id="tgl_tahun">
				            Thn 
				            <input type="text" class="input-mini" value="{{$row->umur_bln}}" name="umur_bln" id="tgl_bulan">
				            Bln 
				            <input type="text" class="input-mini" value="{{$row->umur_hr}}" name="umur_hr" id="tgl_hari">
				            Hr
				            </div>
				          </div>
				          <div class="control-group">
				          <label class="control-label">Provinsi</label>
				          <div class="controls">
				          <select name="" class="input-medium id_combobox" id="id_provinsi" onchange="showKabupaten()">
				          <?php $pro=Provinsi::lists('provinsi','id_provinsi'); 
				           foreach ($pro as $id_provinsi => $provinsi) {
				              if($provinsi==$row->provinsi) {
				                ?>
				                <option value="{{$id_provinsi}}" selected="selected">{{$provinsi}}</option>
				                <?php
				              } else {
				              ?>
				                <option value="{{$id_provinsi}}">{{$provinsi}}</option>
				              <?php
				              }
				            } 
				          ?>
				          
				            </select>
				          <span class="help-inline"></span>
				            </div>
				          </div>
				          <div class="control-group">
				            <label class="control-label">Kabupaten/Kota</label>
				            <div class="controls">
				              <select name="" class="input-medium id_combobox" id="id_kabupaten" onchange="showKecamatan()">
				                <option value="">{{$row->kabupaten}}</option>
				              </select>
				              <span class="help-inline"></span>
				            </div>
				          </div>
				          <div class="control-group">
				            <label class="control-label">Kecamatan</label>
				            <div class="controls">
				              <select name="" class="input-medium id_combobox" id="id_kecamatan" onchange="showKelurahan()">
				                <option value="">{{$row->kecamatan}}</option>
				              </select>
				              <span class="help-inline"></span>
				            </div>
				          </div>
				          <div class="control-group">
				            <label class="control-label">Kelurahan/Desa</label>
				            <div class="controls">
				              <select name="id_kelurahan" class="input-medium id_combobox" id="id_kelurahan" onchange="showEpid()">
				                <option value="{{$row->id_kelurahan}}">{{$row->kelurahan}}</option>
				              </select>
				              <span class="help-inline"></span>
				            </div>
				          </div>
				          <div class="control-group">
				            <label class="control-label">Alamat</label>
				            <div class="controls">
				              {{Form::textarea('alamat',$row->alamat,array('rows'=>'7'))}}
				              <span class="help-inline"></span>
				            </div>
				          </div>
				          </fieldset>
				        </div>
				      </div>
				      <div class="span6">
				        <div class="media">
				            <fieldset>
				              <legend>Data surveilans AFP</legend>
				                  <div class="control-group">
				                      <label class="control-label" >Tanggal mulai lumpuh</label>
				                      <div class="controls">
				                      {{Form::text('tanggal_mulai_lumpuh',$row->tanggal_mulai_lumpuh,array('class'=>'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
				                      <span class="help-inline"></span>
				                      </div> 
				                  </div>
				                  <div class="control-group">
				                      <label class="control-label" >Demam sebelum lumpuh</label>
				                      <div class="controls">
				                      {{Form::select('demam_sebelum_lumpuh',[''=>'Pilih','0' => 'Ya','1'=>'Tidak'],[$row->demam_sebelum_lumpuh], array('class' => 'input-small'))}}
				                      <span class="help-inline"></span>
				                      </div>    
				                   </div>
				                  <div class="control-group">
				                      <label class="control-label" >Kelumpuhan anggota gerak kanan</label>
				                      <div class="controls">
				                      {{Form::select('kelumpuhan_anggota_gerak_kanan',[''=>'Pilih','0' => '1 anggota gerak','1'=>'2 anggota gerak','2'=>'Tidak'],[$row->kelumpuhan_anggota_gerak_kanan], array('class' => 'input-medium'))}}
				                      <span class="help-inline"></span>
				                      </div>
				                  </div>

				                  <div class="control-group">
				                      <label class="control-label" >Kelumpuhan anggota gerak kiri</label>
				                      <div class="controls">
				                      {{Form::select('kelumpuhan_anggota_gerak_kiri',[''=>'Pilih','0' => '1 anggota gerak','1'=>'2 anggota gerak','2'=>'Tidak'],[$row->kelumpuhan_anggota_gerak_kiri], array('class' => 'input-medium'))}}
				                      <span class="help-inline"></span>
				                      </div>
				                  </div>

				                  <div class="control-group">
				                      <label class="control-label" >Gangguan raba anggota gerak kanan</label>
				                      <div class="controls">
				                      {{Form::select('gangguan_raba_anggota_gerak_kanan',[''=>'Pilih','0' => '1 anggota gerak','1'=>'2 anggota gerak','2'=>'Tidak'],[$row->gangguan_raba_anggota_gerak_kanan], array('class' => 'input-medium'))}}
				                      <span class="help-inline"></span>
				                      </div>
				                  </div>
				                  <div class="control-group">
				                  <label class="control-label" >Gangguan raba anggota gerak kiri</label>
				                  <div class="controls">
				                      {{Form::select('gangguan_raba_anggota_gerak_kiri',[''=>'Pilih','0' => '1 anggota gerak','1'=>'2 anggota gerak','2'=>'Tidak'],[$row->gangguan_raba_anggota_gerak_kiri], array('class' => 'input-medium'))}}
				                      <span class="help-inline"></span>
				                      </div>
				                  </div>

				                  <div class="control-group">
				                      <label class="control-label" >Imunisasi polio sebelum sakit</label>
				                      <div class="controls">
				                      {{Form::select('imunisasi_polio_sebelum_sakit',[''=>'Pilih','1x' => '1X','2x'=>'2X','3x'=>'3X','4x'=>'4X','5x'=>'5X','6x'=>'6X','tidak'=>'Tidak','tidak tahu'=>'Tidak tahu'],[$row->imunisasi_polio_sebelum_sakit],array('class'=>'input-small'))}}
				                      <span class="help-inline"></span>
				                      </div>
				                  </div>
				                   <div class="control-group">
				                      <label class="control-label" >Tanggal vaksinasi polio terakhir</label>
				                      <div class="controls">
				                      {{Form::text('tanggal_vaksinasi_polio',$row->tanggal_vaksinasi_polio,array('class'=>'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
				                      <span class="help-inline"></span>
				                      </div>
				                  </div>
				                  <div class="control-group">
				                      <label class="control-label">Tanggal laporan diterima</label>
				                      <div class="controls">
				                        {{Form::text('tanggal_laporan_diterima',$row->tanggal_laporan_diterima,array('class'=>'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
				                        <span class="help-inline"></span>
				                      </div>
				                  </div>
				                  <div class="control-group">
				                     <label class="control-label">Tanggal pelacakan</label>
				                     <div class="controls">
				                     {{Form::text('tanggal_pelacakan',$row->tanggal_pelacakan,array('class'=>'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
				                     <span class="help-inline"></span>
				                     </div>
				                  </div>
				                  <div class="control-group">
				                      <label class="control-label" >Kontak dengan penyakit lain</label>
				                      <div class="controls">
				                      {{Form::select('kontak',[''=>'Pilih','Y' => 'Ya','T'=>'Tidak'],[$row->kontak], array('class' => 'input-mini'))}}
				                      <span class="help-inline"></span>
				                      </div>
				                  </div>
				                  <div class="control-group">
				                      <label class="control-label" >Keadaan akhir</label>
				                      <div class="controls">
				                      {{Form::select('keadaan_akhir',[''=>'Pilih','0' => 'Hidup','1'=>'Mati'],[$row->keadaan_akhir], array('class' => 'input-small'))}}
				                      <span class="help-inline"></span>
				                      </div>
				                  </div>
				              </fieldset>
				            </div>
				          </div>
				          <div class="span11">
				            <div class="media">
				            <fieldset>
				                <legend>Data spesimen dan laboratorium</legend>
				                <div class="control-group">
				                    <label class="control-label" >Tanggal pengambilan spesimen I</label>
				                    <div class="controls">
				                    {{Form::text('tanggal_pengambilan_spesimen1',$row->tanggal_pengambilan_spesimen1,array('class'=>'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
				                    <span class="help-inline"></span>
				                    </div>
				                </div>
				                <div class="control-group">
				                    <label class="control-label" >Tanggal pengambilan spesimen II</label>
				                    <div class="controls">
				                    {{Form::text('tanggal_pengambilan_spesimen2',$row->tanggal_pengambilan_spesimen2,array('class'=>'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
				                    <span class="help-inline"></span>
				                    </div>
				                </div>
				                <div class="control-group">
				                    <label class="control-label" >Jenis pemeriksaan laboratorium spesimen I</label>
				                    <div class="controls">
				                    {{Form::select('jenis_pemeriksaan_spesimen1',[''=>'Pilih','isolasi virus'=>'Isolasi virus','TTD'=>'TTD','sequencing'=>'Sequencing'],[$row->jenis_pemeriksaan_spesimen1],array('class'=>'input-medium'))}}
				                    <span class="help-inline"></span>
				                    </div>
				                </div>
				                <div class="control-group">
				                    <label class="control-label" >Hasil lab isolasi virus</label>
				                    <div class="controls">
				                    {{Form::text('hasil_lab_isolasi_virus',$row->hasil_lab_isolasi_virus,array('class'=>'input-large'))}}
				                    <span class="help-inline"></span>
				                    </div>
				                </div>
				                <div class="control-group">
				                    <label class="control-label" >Hasil lab ITD</label>
				                    <div class="controls">
				                    {{Form::text('hasil_lab_ITD',$row->hasil_lab_ITD,array('class'=>'input-large'))}}
				                    <span class="help-inline"></span>
				                    </div>
				                </div>
				                <div class="control-group">
				                    <label class="control-label" >hasil lab Sequencing</label>
				                    <div class="controls">
				                    {{Form::text('hasil_lab_sequencing',$row->hasil_lab_sequencing,array('class'=>'input-large'))}}
				                    <span class="help-inline"></span>
				                    </div>
				                </div>
				                <div class="control-group">
				                    <label class="control-label" >Jenis spesimen</label>
				                    <div class="controls">
				                    {{Form::select('jenis_spesimen',[''=>'Pilih','0' => 'Darah','1'=>'Urin'],[$row->jenis_spesimen], array('class' => 'input-small'))}}
				                    <span class="help-inline"></span>
				                    </div>
				                </div>
				                <div class="control-group">
				                  <label class="control-label">Klasifikasi final</label>
				                  <div class="controls">
				                  {{Form::select('klasifikasi_final',[''=>'Pilih','0' => 'Polio','1'=>'Bukan polio'],[$row->klasifikasi_final], array('class' => 'input-medium id_combobox'))}}
				                  <span class="help-inline"></span>
				                  </div>
				              </div>
				            </fieldset>
				            @endforeach
				          </div>
				        </div>
				      </div>
				    </div>
				    <div class="form-actions">
				        <button type="submit" class="btn btn-success">Simpan</button>
				        <button type="reset" class="btn">Batal</button>
				        <a href="{{URL::to('dashboard')}}" class="btn btn-warning">Kembali</a>
				    </div>
				{{Form::close()}}
			</div>
			<div class="tab-pane" id="tab_default_2">
				{{Form::open(array('url'=>'simpan_hasil_lab_afp','class'=>'form-horizontal','id'=>'simpan_hasil_lab'))}}
				@foreach($afp as $row)
				<input value="{{$row->id_pasien}}" name="id_pasien" type="hidden">
				<input value="{{$row->id_afp}}" name="id_afp" type="hidden">
				@endforeach
                    <div class="module-body">
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="media">
                                    <fieldset>
                                        <legend>Informasi spesimen</legend>
                                        <div class="control-group">
                                            <label class="control-label">No. Spesimen</label>
                                            <div class="controls">
                                             {{Form::text('no_spesimen',Input::old('no_spesimen'), array('class' => 'input-medium'))}}
                                            <span class="help-inline"></span>
                                            </div>
                                          </div>
                                          <div class="control-group">
                                            <label class="control-label">Tanggal sakit</label>
                                            <div class="controls">
                                              {{Form::text('tanggal_sakit',Input::old('tanggal_sakit'), array('class' => 'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                          <div class="control-group">
                                            <label class="control-label">Tanggal imunisasi polio terakhir</label>
                                            <div class="controls">
                                              {{Form::text('tanggal_imunisasi_polio_terakhir',Input::old('tanggal_imunisasi_polio_terakhir'), array('class' => 'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                          <div class="control-group">
                                            <label class="control-label">Tanggal spesimen</label>
                                            <div class="controls">
                                              {{Form::text('tanggal_spesimen',Input::old('tanggal_spesimen'), array('class' => 'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                          <div class="control-group">
                                            <label class="control-label">Tanggal pengiriman spesimen ke propinsi</label>
                                            <div class="controls">
                                              {{Form::text('tanggal_pengiriman_spesimen_propinsi',Input::old('tanggal_pengiriman_spesimen_propinsi'), array('class' => 'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                           <div class="control-group">
                                            <label class="control-label">Tanggal pengiriman spesimen ke laboratorium</label>
                                            <div class="controls">
                                              {{Form::text('tanggal_pengiriman_spesimen_laboratorium',Input::old('tanggal_pengiriman_spesimen_laboratorium'), array('class' => 'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                           <div class="control-group">
                                            <label class="control-label">Tanggal terima spesimen di laboratorium</label>
                                            <div class="controls">
                                              {{Form::text('tanggal_terima_spesimen_laboratorium',Input::old('tanggal_terima_spesimen_laboratorium'), array('class' => 'input-medium','data-uk-datepicker'=>'{format:"YYYY-MM-DD"}'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                          <div class="control-group">
                                            <label class="control-label">Kondisi spesimen waktu di terima di propinsi (diganti laboratorium)</label>
                                            <div class="controls">
                                              {{Form::select('kondisi_spesimen_dipropinsi', array('' => 'Pilih','0'=>'Baik','1'=>'Volum kurang','2' => 'Tidak dingin','3'=>'Kering(lisis)','4'=>'Tube bocor','5'=>'Tidak baik'),Input::old('kondisi_spesimen_dipropinsi'), array('class' => 'input-medium'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                          <div class="control-group">
                                            <label class="control-label">Kondisi spesimen waktu di terima di laboratorium</label>
                                            <div class="controls">
                                              {{Form::select('kondisi_spesimen_dilab', array('' => 'Pilih','0'=>'Adekuat(baik)','1'=>'Non adekuat(tidak baik)'),Input::old('kondisi_spesimen_dilab'), array('class' => 'input-medium'))}}
                                              <span class="help-inline"></span>
                                            </div>
                                          </div>
                                          	<div class="control-group">
				                                <label class="control-label" for="basicinput">Hasil Spesimen Tenggorokan</label>
				                                <div class="controls">
				                                    <input name="hasil_spesimen_tenggorokan" type="text" class="input-medium"/>  
				                                </div>
				                            </div>
				                            <div class="control-group">
				                                <label class="control-label" for="basicinput">Hasil Spesimen Hidung</label>
				                                <div class="controls">
				                                    <input name="hasil_spesimen_hidung" type="text" class="input-medium"/>  
				                                </div>
				                            </div>
				                            <div class="control-group">
				                                <label class="control-label" for="basicinput">Hasil pemeriksaan virus polio</label>
				                                <div class="controls">
				                                    <select name="hasil_pemeriksaan_virus_polio" type="text" class="input-small">
				                                        <option value="positif">Positif</option>
				                                        <option value="negatif">Negatif</option>
				                                    </select>
				                                </div>
				                            </div>
				                            <div class="control-group">
				                                <label class="control-label" for="basicinput">Hasil pemeriksaan non-polio enterovirus</label>
				                                <div class="controls">
				                                    <select name="hasil_pemeriksaan_nonpolio_enterovirus" type="text" class="input-small">
				                                        <option value="positif">Positif</option>
				                                        <option value="negatif">Negatif</option>
				                                    </select>
				                                </div>
				                            </div>
				                            <div class="control-group">
				                                <label class="control-label" for="basicinput">Klasifikasi Akhir</label>
				                                <div class="controls">
				                                    <input name="klasifikasi_akhir" type="text" class="input-medium"/>  
				                                </div>
				                            </div>
				                            <div class="control-group">
				                                <label class="control-label">Keterangan</label>
				                                <div class="controls">
				                                    <textarea rows="5" name="keterangan"></textarea>
				                                </div>
				                            </div>
				                            <div class="control-group">
				                                <label class="control-label">Tanggal Uji laboratorium</label>
				                                <div class="controls">
				                                    <input type="text" data-uk-datepicker="{format:'YYYY-MM-DD'}" class="input-mini" name="tanggal_uji_laboratorium">  
				                                </div>
				                            </div>
				                            <div class="control-group">
				                                <label class="control-label span3">Tanggal kirim hasil</label>
				                                <div class="controls span6">
				                                    <input type="text" data-uk-datepicker="{format:'YYYY-MM-DD'}" class="input-medium"  name="tanggal_kirim_hasil">  
				                                </div>
				                            </div>
                                    </fieldset>
                                </div>
                            </div>
                        <div class="form-actions">
                      		{{Form::submit('simpan',array('class'=>'btn btn-primary'))}}
                      		{{Form::reset('batal',array('class'=>'btn btn-warning'))}}
                          	<span id="msg"></span>
                        </div>
                          {{Form::close()}}
			</div>
		</div>
	</div>
</div>
