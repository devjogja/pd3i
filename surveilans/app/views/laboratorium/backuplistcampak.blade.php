@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <h4>Penyakit Campak </h4> 
                    <hr>
                </div>
                <ul class="profile-tab nav nav-tabs">
                    <li  class="active"><a href="#cari_pasien" data-toggle="tab">Cari pasien</a></li>
                    <!-- <li><a href="#friends" data-toggle="tab">Input data individual kasus</a></li>
                    <li><a href="#input-hasil" data-toggle="tab">Input hasil laboratorium</a></li> -->
                    <li><a href="#activity" data-toggle="tab">Daftar hasil laboratorium pasien campak</a></li>
                    <li><a href="#analisis" data-toggle="tab">Analisis kasus</a></li>
                </ul>
                <div class="profile-tab-content tab-content">
                <div class="tab-pane fade active in" id="cari_pasien">
                    <div class="module-body">
                        <div class="row-fluid">
                            <table class="table form-horizontal" style="width:100%;">
                            <!-- <caption>Silahkan mencari pasien terlebih dahulu untuk mengisi hasil data laboratorium</caption> -->
                                <tr>
                                    <td style="text-align:right;width:10%;">
                                        No. Epid
                                    </td>
                                    <td style="width:30%;">
                                        <input type="text" id="cari_no_epid" name="no_epid" required="required" placeholder="masukan no. epidemologi">
                                        <button id="cari" class="btn btn-success" onclick="cari_pasien_dilab()" >cari pasien</button>
                                    </td>
                                    <td style="text-align:left;">
                                        <!-- <button id="tampil" class="btn">advanced search</button> -->
                                    </td>
                                    <td></td>
                                </tr><!-- 
                                <div id="advan_seacrh" style="display:none">
                                <tr>
                                    <td style="text-align:right;">
                                        Nama pasien
                                    </td>
                                    <td>
                                    <input type="text" id="cari_nama_penderita" name="nama_penderita" placeholder="masukan nama pasien">
                                    </td>
                                    <td style="text-align:right;">
                                       
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:right;">
                                        Jenis kelamin
                                    </td>
                                    <td>
                                        <select type="text" id="cari_jenis_kelamin" name="jenis_kelamin" class="input-medium" >
                                            <option value="">Pilih</option>
                                            <option value="0">Laki-laki</option>
                                            <option value="1">Perempuan</option>
                                        </select>                                   
                                    </td>
                                    <td style="text-align:right;">
                                       
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                </div> -->
                                <!-- <tr>
                                    <td style="text-align:right;">
                                        
                                    </td>
                                    <td>
                                        <center>
                                            <button id="cari" class="btn btn-success">cari pasien</button>
                                        </center>
                                    </td>
                                    <td style="text-align:right;">
                                   
                                    </td>
                                    <td>
                                    </td>
                                </tr> -->
                                <tr>
                                    <td style="text-align:right;">
                                        
                                    </td>
                                    <td>
                                        
                                    </td>
                                    <td style="text-align:right;">
                                        
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="module message">
                    <div class="module-body table">                             
                        <table class="table table-message">
                            <tbody id="hasil_cari">
                                
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
                    <div class="tab-pane fade in" id="activity">
                        <div class="module-body uk-overflow-container" id="data">
                            <table class="display table table-bordered" id="example">
                            <thead>
                                <tr>
                                  <th>Kota/Kab</th>
                                  <th>No. Epid</th>
                                  <th>No. Lab</th>
                                  <th>Nama pasien</th>
                                  <th>Umur</th>
                                  <th>Tanggal Pengiriman spesimen</th>
                                  <th>Tanggal terima spesimen</th>
                                  <th>Kondisi spesimen</th>
                                  <th>Hasil pemeriksaan IgM Campak</th>
                                  <th>Hasil pemeriksaan IgM Rubella</th>
                                </tr>
                              </thead>
                              <tbody>
                              <?php $data = Campak::pasienLabcampak();?>
                              @foreach($data as $row)
                                <tr>
                                    <td>{{$row->kabupaten}}</td>
                                    <td>{{$row->no_epid}}</td>
                                    <td>{{$row->id_uji_lab}}</td>
                                    <td>{{$row->nama_anak}}</td>
                                    <td>@if($row->umur){{$row->umur}} tahun @endif</td>
                                    <td>{{$row->tanggal_pengiriman_spesimen_laboratorium}}</td>
                                    <td>{{$row->tanggal_terima_spesimen_laboratorium}}</td>
                                    <td><?php echo $retVal = ($row->kondisi_spesimen_dipropinsi==0) ? 'Adekuat(baik)' : 'Nonadekuat(tidak baik)' ; ?></td>
                                    <td><?php echo $retVal = ($row->hasil_igm_campak==0) ? 'Positif' : 'Negatif' ; ?></td>
                                    <td><?php echo $retVal = ($row->hasil_igm_rubella==0) ? 'Positif' : 'Negatif' ; ?></td>
                                </tr>
                            @endforeach
                              
                              </tbody>
                            </table>

                        </div>
                    </div>					
					@include('pemeriksaan.campak.analisis')
					@include('pemeriksaan.campak.analisis-js')
                </div>
            </div>
            <!--/.module-body-->
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('#example').dataTable();
});
</script>
<!--/.span9--><!-- 
@include('pemeriksaan.campak.analisis-js') -->
@stop
