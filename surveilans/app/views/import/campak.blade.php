@extends('layouts.master')
@section('content')
<div class="span12">
	<div class="content">
		<div class="module">
			<div class="module-body">
				<div class="profile-head media">
					<h4>Penyakit Campak</h4>
					<hr>
				</div>
				<div class="tab-pane fade active in" id="activity">
					<div class="module-body uk-overflow-container">
						{{
							Form::open(array(
								'url' 		=> 'campak.extract',
								'class' 	=> 'form-horizontal',
								'files' 	=> true,
								'method'	=> 'POST'
							))
						}}
						<fieldset>
							<legend>Import Campak</legend>
							<div class="control-group">
								<label class="control-label">Filename</label>
								<div class="controls">
									{{ Form::file('excel') }}
								</div>
							</div>
						</fieldset>
						<div class="form-actions">
							{{ Form::submit('Upload', array('class' => 'btn btn-default')) }}
						</div>
						{{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop