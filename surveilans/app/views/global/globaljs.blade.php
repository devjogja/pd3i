<script type="text/javascript">
var isset = function(variable){
    return typeof(variable) !== "undefined" && variable !== null && variable !== '';
}

	$(document).ready(function(){
		$(".unit").val("all");
		$('.day_start').attr('disabled', 'disabled');
		$('.day_end').attr('disabled', 'disabled');
		$('.month_start').attr('disabled', 'disabled');
		$('.month_end').attr('disabled', 'disabled');
		$('.year_start').attr('disabled', 'disabled');
		$('.year_end').attr('disabled', 'disabled');
		$('#example').dataTable();
		$("select").select2();
		var type = "<?php echo Session::get('type');?>";
		if (type=='kemenkes') {
			$('#export_dk').removeAttr('disabled');
			$('#export_analisis').removeAttr('disabled');
		};

		$('#search2').submit(function() {
			loading_states();
			$('#search2').ajaxSubmit({
				type: 'POST',
				dataType:'script',
				success:function(data) {
					//alert('asdasd');
					//$('#test_output').html(data);
				}
			});
			return false;
		});
	});

function loading_states(){
    $('#container_jk').html('<img src="{{URL::to("assets/img/loading1.gif")}}" style="width:40px;"><br>Loading...');
    $('#container_waktu').html('<img src="{{URL::to("assets/img/loading1.gif")}}" style="width:40px;"><br>Loading...');
    $('#container_umur').html('<img src="{{URL::to("assets/img/loading1.gif")}}" style="width:40px;"><br>Loading...');
    $('#container_imunisasi').html('<img src="{{URL::to("assets/img/loading1.gif")}}" style="width:40px;"><br>Loading...');
    $('#container_klasifikasi_final').html('<img src="{{URL::to("assets/img/loading1.gif")}}" style="width:40px;"><br>Loading...');
    //$('#map-canvas').html('<img src="{{URL::to("assets/img/loading1.gif")}}" style="width:40px;"><br>Loading...');
}

	$(document).on("shown", 'a[data-toggle="tab"]', function (e) {
		if(e.target=='{{URL::to("/")}}/lab/campak#analisis')
		{
			$('#tampilkan_analisa').click();
		}
	});

	function setDisable(obj) {
		if(obj.value == "all") {
			$('.day_start').attr('disabled', 'disabled');
			$('.day_end').attr('disabled', 'disabled');
			$('.month_start').attr('disabled', 'disabled');
			$('.month_end').attr('disabled', 'disabled');
			$('.year_start').attr('disabled', 'disabled');
			$('.year_end').attr('disabled', 'disabled');
		} else if(obj.value == "year") {
			$('.day_start').attr('disabled', 'disabled');
			$('.day_end').attr('disabled', 'disabled');
			$('.month_start').attr('disabled', 'disabled');
			$('.month_end').attr('disabled', 'disabled');
			$('.year_start').removeAttr('disabled');
			$('.year_end').removeAttr('disabled');
		} else if (obj.value == "month") {
			$('.day_start').attr('disabled', 'disabled');
			$('.day_end').attr('disabled', 'disabled');
			$('.month_start').removeAttr('disabled');
			$('.month_end').removeAttr('disabled');
			$('.year_start').removeAttr('disabled');
			$('.year_end').removeAttr('disabled');
		} else {
			$('.day_start').removeAttr('disabled');
			$('.day_end').removeAttr('disabled');
			$('.month_start').removeAttr('disabled');
			$('.month_end').removeAttr('disabled');
			$('.year_start').removeAttr('disabled');
			$('.year_end').removeAttr('disabled');
		}
	}

	function jenisKasus(){
		var id = $('#jenis_kasus').val();
		$('#klb_ke').attr('disabled','disabled');
		if (id=="1") {
			$('.noKLB').show();
			$("#sumberSpesimen").select2("val","KLB");
			$('#klb_ke').removeAttr('disabled');
		}else if(id=="2"){
			$('.noKLB').hide();
			$("#sumberSpesimen").select2("val","CBMS");
		}else{
			$('.noKLB').hide();
		};
	}

	function umur()
	{
		var tgl_lahir = $('.tgl_lahir').val();
		var tgl_sakit = $('.tgl_sakit').val();
		if (tgl_lahir != '') {
			$.ajax({
				data:{tanggal_lahir:tgl_lahir, tgl_sakit:tgl_sakit},
				url:'{{URL::to("hitung/umur")}}',
				success:function(data) {
					$('#tgl_tahun').val(data.tgl_tahun);
					$('#tgl_bulan').val(data.tgl_bulan);
					$('#tgl_hari').val(data.tgl_hari);
				}
			});
		}
	}

	function usia(){
		var tgl_tahun = $('#tgl_tahun').val();
		var tgl_bulan = $('#tgl_tahun').val();
		var tgl_hari = $('#tgl_tahun').val();
		if (tgl_tahun=='' && tgl_bulan=='' && tgl_hari=='') {
			var tgl_lahir = $('.tgl_lahir').val();
			var tgl_periksa = $('#tgl_mulai_sakit').val();
			var tgl_sakit = $('#tgl_sakit').val();
			$.ajax({
				data: {tgl_lahir:tgl_lahir,tgl_periksa:tgl_periksa,tgl_sakit:tgl_sakit},
				url:'{{URL::to("hitung/umur_pasien")}}',
				success:function(data){
					if (data!=='') {
						$('#tgl_tahun').val(data.years);
						$('#tgl_bulan').val(data.month);
						$('#tgl_hari').val(data.day);
						$('#tgl_tahun').removeAttr('readonly');
						$('#tgl_bulan').removeAttr('readonly');
						$('#tgl_hari').removeAttr('readonly');
					};
				}
			});
		};
	}

	function umur_pasien(){
		var tgl_lahir = $('.tgl_lahir').val();
		var tgl_periksa = $('#tgl_mulai_sakit').val();
		$.ajax({
			data:'tgl_lahir='+tgl_lahir+'&tgl_periksa='+tgl_periksa,
			url:'{{URL::to("hitung/umur_pasien")}}',
			success:function(data){
				if (data!=null) {
					$('#tgl_tahun').val(data.years);
					$('#tgl_bulan').val(data.month);
					$('#tgl_hari').val(data.day);
				};
			}
		});

		$.post('{{URL::to("getEpidCrs")}}', {tglLahir:tgl_lahir}, function(response){
			if (response!=null) {
				$('#no_epid').val(response);
			};
		});
	}

	function tgl_lahir()
	{
		var tgl_tahun = $('#tgl_tahun').val();
		var tgl_bulan = $('#tgl_bulan').val();
		var tgl_hari = $('#tgl_hari').val();
		var tgl_sakit = $('.tgl_mulai_sakit').val();
		var tgl_lahir = $('.tgl_lahir').val();
		$.ajax({
			data:'tgl_tahun='+tgl_tahun+'&tgl_bulan='+tgl_bulan+'&tgl_hari='+tgl_hari+'&tanggal_timbul_demam='+tgl_sakit+'&tanggal_lahir='+tgl_lahir,
			url:'{{URL::to("hitung/tgl_lahir")}}',
			success:function(data) {
				if(data.tgl==1) {
					$('.tgl_lahir').val('');
					$('.tgl_lahir').val(data.tgl1);
					$('#tgl_tahun').val(data.tgl_tahun);
					$('#tgl_bulan').val(data.tgl_bulan);
					$('#tgl_hari').val(data.tgl_hari);
				}
				$('.tgl_lahir').val('');
				$('.tgl_lahir').val(data);
			}
		});
	}

	function search_pasien() {
		var search = $('#search').val();
		var penyakit = "{{Request::segment(2)}}";
		$.post("{{URL::to('search_pasien')}}", {search:search, penyakit:penyakit}, function(response){
			$('#result').removeAttr('style');
			$('#result table tbody').html(response['content']);
			$('#url').attr('href',response['url']);
		});
	}

	function search_pasien_campak() {
		$.ajax({
			data: 'cari_no_epid='+$('#cari_no_epid').val(),
			url:'{{route("cari_pasien_campak")}}',
			success:function(data) {
				$('#result').removeAttr('style');
				$('#result table tbody').html('');
				$('#result table tbody').html(data);
			}
		});
	}

	function goBack() {
		window.history.back();
	}

	function showEpidCampak(id) {
		var id_kelurahan = $('#id_kelurahan'+id).val();
		var tgl_mulai_demam = $('#tgl_mulai_sakit').val();
		var tgl_mulai_rash = $('#tgl_sakit').val();
		var tgl_sakit = '';
		if(isset(tgl_mulai_demam)){
			tgl_sakit = tgl_mulai_demam;
		}
		if(isset(tgl_mulai_rash)){
			tgl_sakit = tgl_mulai_rash;
		}

		if (isset(id_kelurahan)) {
			$.ajax({
				data:'id_kelurahan='+id_kelurahan+'&date='+tgl_sakit,
				url:'{{URL::to("ambil_epid_campak")}}',
				success:function(data) {
					$('#epid').val(data);
				}
			});
		};
		return false;
	}

	function showEpidCrs(id) {
		var id_kelurahan = $('#id_kelurahan'+id).val();
		var tgl_lahir = $('#tgl_lahir').val();

		if (isset(id_kelurahan)) {
			$.ajax({
				data:'id_kelurahan='+id_kelurahan+'&date='+tgl_lahir,
				url:'{{URL::to("ambil_epid_crs")}}',
				success:function(data) {
					$('#no_epid').val(data);
				}
			});
		};
		return false;
	}

	function showEpidKLB(id){
		var id_kelurahan = $('#id_kelurahan'+id).val();
		var tgl_mulai_demam = $('#tgl_mulai_sakit').val();
		var tgl_mulai_rash = $('#tgl_sakit').val();
		var klb_ke= $('#klb_ke').val();
		var tgl_sakit = '';
		if(isset(tgl_mulai_demam)){
			tgl_sakit = tgl_mulai_demam;
		}
		if(isset(tgl_mulai_rash)){
			tgl_sakit = tgl_mulai_rash;
		}

		if (isset(id_kelurahan)) {
			$.ajax({
				data:'id_kelurahan='+id_kelurahan+'&date='+tgl_sakit+'&klb_ke='+klb_ke,
				url:'{{URL::to("ambil_epid_klb")}}',
				success:function(data) {
					$('#epidklb').val(data);
				}
			})
		}
	}

	function show_kabupaten(dt,kabupaten){
		var provinsi = $("#id_provinsi"+dt).val();
		var kabupaten = (kabupaten)?kabupaten:null;
		$('.lodingKab').html('<img src="{{URL::to("style/images/loding.gif")}}">');
		//get kabupaten
		$.ajax({
			method: "POST",
			url: "{{URL::to('provinsi/getkab')}}",
			data: {provinsi:provinsi,kabupaten:kabupaten}
		})
		.done(function(response) {
			$('#id_kabupaten'+dt).empty();
			$('#id_kecamatan'+dt).empty();
			$('#id_kelurahan'+dt).empty();
			if (response) {
				$("#id_kabupaten"+dt).append(response).select2();
				$('.lodingKab').empty();
			};
		});
		return false;
	}

	function show_kecamatan(dt, kecamatan){
		var kabupaten = $("#id_kabupaten"+dt).val();
		var kecamatan = (kecamatan)?kecamatan:null;
		if (kabupaten !== '' || kabupaten !== null || kabupaten !== 'undefined') {
			$('.lodingKec').html('<img src="{{URL::to("style/images/loding.gif")}}">');
			//get kecamatan
			$.ajax({
				method: "POST",
				url: "{{URL::to('provinsi/getkec')}}",
				data: {kabupaten:kabupaten,kecamatan:kecamatan}
			})
			.done(function(response) {
				$('#id_kecamatan'+dt).empty();
				$('#id_kelurahan'+dt).empty();
				if (response) {
					$("#id_kecamatan"+dt).append(response).select2();
					$('.lodingKec').empty();
				}
			});
		};
	}

	function show_kelurahan(dt, kelurahan){
		var kecamatan = $("#id_kecamatan"+dt).val();
		var kelurahan = (kelurahan)?kelurahan:null;
		if (kecamatan !== '' || kecamatan !== null || kecamatan !== 'undefined') {
			$('.lodingKel').html('<img src="{{URL::to("style/images/loding.gif")}}">');
			$.ajax({
				method: "POST",
				url: "{{URL::to('provinsi/getkel')}}",
				data: {kecamatan:kecamatan,kelurahan:kelurahan}
			})
			.done(function(response) {
				$('#id_kelurahan'+dt).empty();
				if (response) {
					$("#id_kelurahan"+dt).append(response).select2();
					$('.lodingKel').empty();
				}
			});
		}
	}

	function show_puskesmas(dt){
		var kecamatan = $("#id_kecamatan"+dt).val();
		$('.lodingPus').html('<img src="{{URL::to("style/images/loding.gif")}}">');
		$.ajax({
			method: "POST",
			url: "{{URL::to('provinsi/getpus')}}",
			data: {kecamatan:kecamatan}
		})
		.done(function(response) {
			$('#id_puskesmas'+dt).html('');
			if (response) {
				$('#id_rs'+dt).attr('disabled','disabled');
				$("#id_puskesmas"+dt).removeAttr('disabled');
				$("#id_puskesmas"+dt).html(response);
				$('.lodingPus').html('');
			};
		});
	}

	$('#export_dk').click(function(e){
		e.preventDefault();
		var params = JSON.stringify($('.filter_daftar_kasus').serializeArray());
		var penyakit = "{{Session::get('sess_penyakit')}}";
		var type = "{{Session::get('type')}}";
		if (type=='laboratorium') {
			var urls = "{{URL::to('exportall/campak/xls')}}";
		}else{
			var urls = "{{URL::to('exportall/'.strtolower(Session::get('sess_penyakit')).'/xls')}}";
		};
		var url = urls+'/'+params;
		window.open(url, '_blank');
	});

	function getKabupaten(id) {
		var provinsi = $('#id_provinsi'+id).val();
		var kabupaten = $('#id_kabupaten'+id).val();
		$('.lodingKab').html('<img src="{{URL::to("style/images/loding.gif")}}">');
		$.ajax({
			method: "POST",
			url: "{{URL::to('provinsi/getkab')}}",
			data: {provinsi:provinsi,kabupaten:kabupaten}
		})
		.done(function(data){
			$('#id_kabupaten'+id).select2('val','');
			$('#id_kecamatan'+id).select2('val','');
			$('#id_kelurahan'+id).select2('val','');
			if(data){
				$("#id_kabupaten"+id).html(data);
				$('.lodingKab').html('');
			};
		});
	}

	function getKecamatan(id) {
		var kabupaten = $('#id_kabupaten'+id).val();
		var kecamatan = $('#id_kecamatan'+id).val();
		$('.lodingKec').html('<img src="{{URL::to("style/images/loding.gif")}}">');
		$.ajax({
			method: "POST",
			url: "{{URL::to('provinsi/getkec')}}",
			data: {kabupaten:kabupaten,kecamatan:kecamatan}
		})
		.done(function(data){
			$('#id_kecamatan'+id).select2('val','');
			$('#id_kelurahan'+id).select2('val','');
			if(data){
				$("#id_kecamatan"+id).html(data);
				$('.lodingKec').html('');
			};
		});
	}

	function getKelurahan(id) {
		var kecamatan = $('#id_kecamatan'+id).val();
		var kelurahan = $('#id_kelurahan'+id).val();
		$('.lodingKel').html('<img src="{{URL::to("style/images/loding.gif")}}">');
		$.ajax({
			method: "POST",
			url: "{{URL::to('provinsi/getkel')}}",
			data: {kecamatan:kecamatan,kelurahan:kelurahan}
		})
		.done(function(data){
			$('#id_kelurahan'+id).select2('val','');
			if(data){
				$("#id_kelurahan"+id).html(data);
				$('.lodingKel').html('');
			};
		});
	}

	function gets_district(id) {
		$.ajax({
			data: 'province_id='+$('#province_id'+id).val(),
			url:'{{URL::to("region/get_district")}}',
			success:function(data) {
				$('#district_id'+id).attr('disabled',true);
				$('#sub_district_id'+id).attr('disabled',true);
				$('#puskesmas_id'+id).attr('disabled',true);
				if ($('#province_id'+id).val()!='') {
					$('#district_id'+id).html(data);
					$('#district_id'+id).removeAttr('disabled');
				};
			}
		});
		var type = "<?php echo Session::get('type');?>";
		var kd_faskes = "<?php echo Session::get('kd_faskes');?>";
		var prov = $('#province_id'+id).val();
		if ((type=='provinsi'&&kd_faskes==prov)||(type=='kemenkes')){
			$('#export'+id).removeAttr('disabled');
		}else{
			$('#export'+id).attr('disabled',true);
		};
	}

	function gets_kecamatan(id) {
		$.ajax({
			data: 'sub_district_id='+$('#sub_district_id'+id).val(),
			url:'{{URL::to("region/get_village")}}',
			success:function(data) {
				if ($('#province_id'+id).val()!='') {
					$('#district_id'+id).html(data);
					$('#district_id'+id).removeAttr('disabled');
				};
			}
		});
	}

	function gets_sub_district(id){
		$.ajax({
			data: 'district_id='+$('#district_id'+id).val(),
			url:'{{URL::to("region/get_sub_district")}}',
			success:function(data) {
				$('#sub_district_id'+id).attr('disabled',true);
				$('#puskesmas_id'+id).attr('disabled',true);
				if($('#district_id'+id).val()!='') {
					$('#sub_district_id'+id).html(data);
					$('#sub_district_id'+id).removeAttr('disabled');
				}
			}
		});
		$.ajax({
			data: 'district_id='+$('#district_id'+id).val(),
			url:'{{URL::to("region/get_rs")}}',
			success:function(data) {
				$('#rs_id'+id).attr('disabled',true);
				if($('#district_id'+id).val()!='') {
					$('#rs_id'+id).html(data);
					$('#rs_id'+id).removeAttr('disabled');
				}
			}
		});
		var type = "<?php echo Session::get('type');?>";
		var kd_faskes = "<?php echo Session::get('kd_faskes');?>";
		var kab = $('#district_id'+id).val();
		if ((type=='kemenkes')||(type=='provinsi')||(type=='kabupaten' && kd_faskes==kab)){
			$('#export'+id).removeAttr('disabled');
		}else{
			$('#export'+id).attr('disabled',true);
		};
	}

	function gets_puskesmas(id){
		$.ajax({
			data: 'sub_district_id='+$('#sub_district_id'+id).val(),
			url:'{{URL::to("region/get_puskesmas")}}',
			success:function(data) {
				$('#puskesmas_id'+id).attr('disabled',true);
				$('#rs_id'+id).attr('disabled',true);
				if($('#sub_district_id'+id).val()!=''){
					$('#puskesmas_id'+id).html(data);
					$('#puskesmas_id'+id).removeAttr('disabled');
				}
			}
		});
		var type = "<?php echo Session::get('type');?>";
		if ((type=='kemenkes')||(type=='provinsi')||(type=='kabupaten')){
			$('#export'+id).removeAttr('disabled');
		}else{
			$('#export'+id).attr('disabled',true);
		};
	}

	function gets_check_puskesmas(id){
		var type = "<?php echo Session::get('type')?>";
		var kd_faskes = "<?php echo Session::get('kd_faskes');?>";
		var puskesmas = $('#puskesmas_id'+id).val();
		if ((type=='kemenkes')||(type=='provinsi')||(type=='kabupaten')||(type=='puskesmas'&&kd_faskes==puskesmas)){
			$('#export'+id).removeAttr('disabled');
		}else{
			$('#export'+id).attr('disabled',true);
		};
	}

	function gets_rs(id){
		var val = $('#district_id'+id).val();
		$.ajax({
			data: 'district_id='+$('#district_id'+id).val(),
			url:'{{URL::to("region/get_rs")}}',
			success:function(data) {
				$('#rs_id'+id).attr('disabled',true);
				if($('#district_id'+id).val()!=''){
					$('#rs_id'+id).html(data);
					$('#rs_id'+id).removeAttr('disabled');
				}
			}
		});
		var type = "<?php echo Session::get('type');?>";
		if ((type=='kemenkes')||(type=='provinsi')||(type=='kabupaten')){
			$('#export'+id).removeAttr('disabled');
		}else{
			$('#export'+id).attr('disabled',true);
		};
	}

	function gets_check_rs(id){
		var type = "<?php echo Session::get('type')?>";
		var kd_faskes = "<?php echo Session::get('kd_faskes');?>";
		var rs = $('#rs_id'+id).val();
		if ((type=='kemenkes')||(type=='provinsi')||(type=='kabupaten')||(type=='rs'&&kd_faskes==rs)){
			$('#export'+id).removeAttr('disabled');
		}else{
			$('#export'+id).attr('disabled',true);
		};
	}

	function pilihOptionTglLahir()
	{
		$('.tgl_lahir').removeAttr('readonly');
		$('#tgl_mulai_sakit').removeAttr('class');
		$('#tgl_tahun').attr('readonly','readonly');
		$('#tgl_bulan').attr('readonly','readonly');
		$('#tgl_hari').attr('readonly','readonly');
		$('.tgl_lahir').val('');
		$('#tgl_tahun').val('');
		$('#tgl_bulan').val('');
		$('#tgl_hari').val('');
	}

	function pilihOptionUmur()
	{
		$('#tgl_tahun').removeAttr('readonly');
		$('#tgl_bulan').removeAttr('readonly');
		$('#tgl_hari').removeAttr('readonly');
		$('#tgl_mulai_sakit').attr('class','input-medium tgl_mulai_sakit');
		$('.tgl_lahir').attr('readonly','readonly');
		$('.tgl_lahir').val('');
		$('#tgl_tahun').val('');
		$('#tgl_bulan').val('');
		$('#tgl_hari').val('');
	}
</script>
