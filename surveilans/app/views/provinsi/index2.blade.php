@extends('layouts.master')
@section('content')
<section class="scrollable padder">  
  <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
    <li><a href="{{URL::to('beranda')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{URL::to('propinsi')}}">Wilayah</a></li> 
  </ul>     
  <h3>Informasi Wilayah</h3>
 <section class="panel panel-default">
    <header class="panel-heading">
      <a href="{{URL::to('provinsi/create')}}" class="btn btn-sm btn-default btn-rounded">Tambah wilayah</a>
    </header>
    <div class="table-responsive">
      <table class="table table-striped m-b-none ZsB5sTrf" data-ride="datatables">
        <thead>
          <tr>
            <th >Kode</th>
            <th >Nama Kelurahan</th>
            <th >Nama Kecamatan</th>
            <th >Nama Kabupaten</th>
            <th >Nama Provinsi</th>
            <th >Aksi</th>
          </tr>
        </thead>
      <tbody>
        @if($provinsi)
        @foreach($provinsi as $data)
        <tr>
          <td>{{$data->id_kelurahan}}</td>
          <td>{{$data->kelurahan}}</td>
          <td>{{$data->kecamatan}}</td>
          <td>{{$data->kabupaten}}</td>
          <td>{{$data->provinsi}}</td>
          <td>
            <a class="btn btn-primary btn-xs" href="{{URL::to('provinsi/edit/1')}}"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ubah"></i></a>
            <button class="btn btn-warning btn-xs"><i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus"></i></button>
       	  </td>
        </tr>
        @endforeach
        @endif
      </tbody>
      </table>
    </div>
  </section>
  <nav>
  <ul class="pagination pagination-sm">
    {{$provinsi->links()}}
  </ul>
  </nav>
</section>
@stop