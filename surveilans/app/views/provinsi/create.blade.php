@extends('layouts.master')
@section('content')
<section class="scrollable padder">
    <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
        <li><a href="{{URL::to('beranda')}}"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{URL::to('laboratorium')}}">Wilayah</a></li>
    	<li class="active">Tambah Wilayah</li>
    </ul>
<!-- 	<div class="m-b-md">
        <h3 class="m-b-none">Tambah Wilayah</h3>
    </div> -->
    <section class="panel panel-default">
        <header class="panel-heading font-bold">
            Silahkan lengkapi form berikut.
        </header>
    <div class="panel-body">
    <form method="POST" action="" accept-charset="UTF-8" role="form" class="form-horizontal" parsley-validate="parsley-validate" novalidate="novalidate"><input name="_token" type="hidden" value="whfbdRtWgBu5PFcdjTEhB04rXkeIUHtSPs7KiOMQ">        <div class="form-group">
        <label for="code" class="col-sm-2 control-label">Nama Propinsi</label>           
        <div class="col-sm-10">
            <input class="form-control" placeholder="" parsley-required="true" parsley-remote="http://localhost:8000/api/jobprefixes/validatecode" name="nama_laboratorium" type="text" id="code">            
        </div>
        </div>
        <div class="line line-dashed line-lg pull-in"></div>
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Nama Kabupaten</label>            
            <div class="col-sm-10">
            	<input class="form-control" placeholder="" parsley-required="true" parsley-remote="http://localhost:8000/api/jobprefixes/validatecode" name="nama_petugas" type="text" id="title">            
            </div>
    	</div>
        <div class="line line-dashed line-lg pull-in"></div>
        <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
                <input class="btn btn-primary" type="submit" value="Simpan">&nbsp;&nbsp;<a class="btn btn-default" href="{{URL::to('laboratorium')}}">Batal simpan</a>
            </div>
        </div>
    </form>    </div>
	</section>
</section>
@stop