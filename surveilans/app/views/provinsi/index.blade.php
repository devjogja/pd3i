@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <h4>
                        Data master wilayah
                    </h4>
                </div>
                <br>
                <div class="table-responsive">
                  <table class="uk-table uk-table-hover uk-table-striped uk-table-condensed table-bordered">
                    <thead>
                      <tr>
                        <th >Kode</th>
                        <th >Nama Kelurahan</th>
                        <th >Nama Kecamatan</th>
                        <th >Nama Kabupaten</th>
                        <th >Nama Provinsi</th>
                        <!-- <th >Aksi</th> -->
                      </tr>
                    </thead>
                  <tbody>
                    @if($provinsi)
                    @foreach($provinsi as $data)
                    <tr>
                      <td>{{$data->id_kelurahan}}</td>
                      <td>{{$data->kelurahan}}</td>
                      <td>{{$data->kecamatan}}</td>
                      <td>{{$data->kabupaten}}</td>
                      <td>{{$data->provinsi}}</td>
                      <!-- <td>
                        <a class="btn btn-primary btn-xs" href="{{URL::to('provinsi/edit/1')}}"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ubah"></i></a>
                        <button class="btn btn-warning btn-xs"><i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus"></i></button>
                      </td> -->
                    </tr>
                    @endforeach
                    @endif
                  </tbody>
                  </table>
                  <ul class="pagination pagination-sm">
                    {{$provinsi->links()}}
                  </ul>
                </div>
            </div>
            <!--/.module-body-->
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<!--/.span9-->
@stop