@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Tambah akun pengguna untuk level Provinsi</h3>
        </div>
        <div class="module-body">
    	{{Form::open(array('url'=>'provinsi/daftarakun','class'=>'form-horizontal row-fluid'))}}
        <div class="control-group">
            <label class="control-label span3" for="basicinput">Nama Provinsi</label>             
            <div class="controls span6">
                {{ Form::select('id_provinsi', array('placeholder'=>'Pilih')+Provinsi::lists('provinsi','id_provinsi'),null,array('id'=>'id_provinsi')) }}
            </div>
        </div>

        <div class="control-group">
        <label class="control-label span3" for="basicinput">Username</label>
            <div class="controls span6">
                <input name="username" type="text" class="form-control span8"/>
            </div>
        </div>

        <div class="control-group">
        <label class="control-label span3" for="basicinput">Password</label>
            <div class="controls span6">
                <input name="password" type="password" class="form-control span8"/>
            </div>
        </div>

        <div class="control-group">
            <div class="controls">
                <input name="id_lab" type="hidden" class="form-control span8"/>
                <input name="nik" type="hidden" class="form-control span8"/>
                <button type="submit" class="btn btn-success">Simpan</button>
                <a href="{{URL::to('#')}}" class="btn btn-warning">Kembali</a>
            </div>
        </div>
        {{Form::close()}}
        </div>
    </div>
    </div>
</div>
@stop