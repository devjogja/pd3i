@extends('layouts.master')
@section('content')
<section class="scrollable padder">  
  <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
    <li><a href=""><i class="fa fa-home"></i> Home</a></li> 
  </ul>     
  <h3>Informasi Rumah sakit</h3>
  <h4 class="inline text-muted m-t-n">Total <span class="m-l-xl m-r-sm">: </span></h4><h3 class="inline"> {{$jml}}</h3>
  <section class="panel panel-default">
    <header class="panel-heading">
      <a href="{{route('rumahsakit.create')}}" class="btn btn-sm btn-default btn-rounded">Tambah Rumah sakit</a>
    </header>
    <div class="table-responsive">
      <table class="table table-striped m-b-none ZsB5sTrf" data-ride="datatables">
        <thead>
          <tr>
            <th >No.</th>
            <th >Nama Rumah sakit</th>
            <th >Nama Petugas</th>
            <th >Telepon</th>
            <th >Email</th>
            <th >Alamat</th>
            <th >Aksi</th>
          </tr>
        </thead>
      <tbody>
      <?php $no=1; ?>
      @if($rumahsakit)
      @foreach($rumahsakit as $data)
      <tr>
        <td>{{$no}}</td>
        <td>{{$data->nama_rumahsakit}}</td>
        <td>{{$data->nama_petugas}}</td>
        <td>{{$data->telepon}}</td>
        <td>{{$data->email}}</td>
        <td>{{$data->alamat}}, {{ $data->kelurahan}}, {{$data->kecamatan}}, {{$data->kabupaten}}, {{$data->provinsi}}</td>
        <td>
          <a class="btn btn-primary btn-xs" href="{{URL::to('rumahsakit/'.$data->id_rumahsakit.'/edit')}}"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ubah"></i></a>
          <a class="btn btn-warning btn-xs" href="{{URL::to('rumahsakit/hapus/'.$data->id_rumahsakit)}}" onclick="alert('yakin ini menghapus data ini?')"><i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus"></i></a>
      </tr>
      <?php $no++?>
      @endforeach
      @endif
      </tbody>
      </table>
    </div>
  </section>
</section>
@stop