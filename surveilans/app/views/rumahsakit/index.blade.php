@extends('layouts.master')
@section('content')
<div class="span12">
    <div class="content">
        <div class="module">
            <div class="module-body">
                <div class="profile-head media">
                    <h4>
                        Data master Rumah sakit
                    </h4>
                </div>
                <br>
                <div class="table-responsive">
                  <table class="uk-table uk-table-hover uk-table-striped uk-table-condensed table-bordered">
                    <thead>
                      <tr>
                        <th >No.</th>
                        <th >Nama Rumah sakit</th>
                        <th >Alamat</th>
                        <th >jenis Rumah sakit</th>
                      </tr>
                    </thead>
                  <tbody>
                    <?php $no=1; ?>
                    @if($rumahsakit)
                    @foreach($rumahsakit as $data)
                    <tr>
                      <td>{{$no}}</td>
                      <td>{{$data->nama_rumah_sakit}}</td>
                      <td>{{$data->alamat}}</td>
                      <td>{{$data->jenis_rumah_sakit}}</td> 
                      <!-- <td>
                        <a class="btn btn-primary btn-xs" href="{{URL::to('rumahsakit/'.$data->id_rumah_sakit.'/edit')}}"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ubah"></i></a>
                        <a class="btn btn-warning btn-xs" href="{{URL::to('rumahsakit/hapus/'.$data->id_rumah_sakit)}}" onclick="alert('yakin ini menghapus data ini?')"><i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus"></i></a>
                      </td> -->
                    </tr>
                    <?php $no++?>
                    @endforeach
                    @endif
                  </tbody>
                  </table>
                  <ul class="pagination pagination-sm">
                    {{$rumahsakit->links()}}
                  </ul>
                </div>
            </div>
            <!--/.module-body-->
        </div>
        <!--/.module-->
    </div>
    <!--/.content-->
</div>
<!--/.span9-->
@stop