<?php
class AdminController extends BaseController {

		/**
		 * The layout that should be used for responses.
		 */
		protected $layout = 'layouts.master';

		// load index admin
		public function getIndex() {

			$this->layout->content = View::make('admin.index');

		}

		public function getLogin() {
			Session::put('sess_id', '');
			Session::put('sess_get_map_dashboard', "0");
			if (Sentry::check()) {
				return Redirect::to('dashboard');
			} else {
				return View::make('admin.login1');
			}
		}

		public function usermanuallab() {
			$file    = public_path() . "/file/manuallab.pdf";
			$headers = array(
				'Content-Type: application/pdf',
				);
			return Response::download($file, 'usermanuallab.pdf', $headers);
		}

		public function usermanual() {
			$file    = (public_path() . "/file/User Manual PD3I v.02.15.pdf");
			$headers = array(
				'Content-Type: application/pdf',
				);
			return Response::download($file, 'User Manual PD3I v.02.15.pdf', $headers);
		}

		/*
		combo_grafik_maps;
		combo_grafik_wilayah;
		combo_grafik_klasifikasi_final;
		combo_grafik_waktu;
		 */

		public function getJsGrafikMaps() {
			return View::make('admin.js_grafik_maps');
		}

		public function getJsGrafikWilayah() {
			return View::make('admin.js_grafik_wilayah');
		}

		public function getJsGrafikKlasifikasiFinal() {
			return View::make('admin.js_grafik_klasifikasi_final');
		}

		public function getJsGrafikWaktu() {
			return View::make('admin.js_grafik_waktu');
		}

		public function getJs() {

			return View::make('admin.js');
		}

		public function getFunction() {

			return View::make('admin.function');
		}

		public function getSignup() {

			return View::make('admin.signup');
		}

		public function getSettingLevel() {
			return View::make('admin.level');
		}

		public function getPilihProfil() {
				//update hak akses user
			if (!Sentry::check()) {
				return Redirect::to('/');
			}

			DB::table('users')
			->where('email', Sentry::getUser()->email)
			->update(array('hak_akses' => 0, 'id_user' => 0));
			return View::make('admin.pilih_profil');
		}

		public function getSettingProfil() {
			$level = Session::get('level');
			if ($level == 'puskesmas') {
				return View::make('admin.profil_puskesmas');
			} else if ($level == 'rs') {
				return View::make('admin.profil_rs');
			} else if ($level == 'kabupaten') {
				return View::make('admin.profil_kabupaten');
			} else if ($level == 'pusat') {
				return View::make('admin.profil_pusat');
			} else if ($level == 'provinsi') {
				return View::make('admin.profil_provinsi');
			} else if ($level == 'laboratorium') {
				return View::make('admin.profil_laboratorium');
			}

		}

		public function login() {
			$credentials = array(
				'email'    => Input::get('username'),
				'password' => Input::get('password'),
				);

			$message = 'Sukses Login';
			$status  = 'error';
			$address = '';
			try
			{
				if ($user = Sentry::authenticate($credentials, false)) {
					$q = "SELECT count(*) as jml FROM profil_puskesmas a
					WHERE a.created_by='" . Sentry::getUser()->id . "'";
					$data                 = DB::select($q);
					$jml_profil_puskesmas = $data[0]->jml;

					$q = "SELECT count(*) as jml FROM profil_dinkes_kabupaten a
					WHERE a.created_by='" . Sentry::getUser()->id . "'";
					$data                 = DB::select($q);
					$jml_profil_kabupaten = $data[0]->jml;

					$q = "SELECT count(*) as jml FROM profil_dinkes_provinsi a
					WHERE a.created_by='" . Sentry::getUser()->id . "'";
					$data                = DB::select($q);
					$jml_profil_provinsi = $data[0]->jml;

					$q = "SELECT count(*) as jml FROM profil_kemenkes a
					WHERE a.created_by='" . Sentry::getUser()->id . "'";
					$data                = DB::select($q);
					$jml_profil_kemenkes = $data[0]->jml;

					$q = "SELECT count(*) as jml FROM profil_laboratorium a
					WHERE a.created_by='" . Sentry::getUser()->id . "'";
					$data           = DB::select($q);
					$jml_profil_lab = $data[0]->jml;

					$q = "SELECT count(*) as jml FROM profil_rs a
					WHERE a.created_by='" . Sentry::getUser()->id . "'";
					$data          = DB::select($q);
					$jml_profil_rs = $data[0]->jml;

					$total = $jml_profil_puskesmas + $jml_profil_kabupaten + $jml_profil_provinsi + $jml_profil_kemenkes + $jml_profil_lab + $jml_profil_rs;

					$address = "";
					if (isset(Sentry::getUser()->id)) {
						if ($total == 0) {
							$address = URL::to('setting/level');
						} else {
							$address = URL::to('pilih_profil');
						}

					}
					$status = 'success';
				}
			} catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
				$message = 'Login field is required.';
			} catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
				$message = 'Password field is required.';
			} catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
				$message = 'Wrong password, try again.';
			} catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
				$message = 'User was not found.';
			} catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
				$message = 'User is not activated.';
			} catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e) {
				$message = 'User is suspended.';
			} catch (Cartalyst\Sentry\Throttling\UserBannedException $e) {
				$message = 'User is banned.';
			}

			$data['status']  = $status;
			$data['message'] = $message;
			$data['address'] = $address;
			return $data;
		}

		public function postSaveLevel() {
			Session::put('level', Input::get('level'));
			return Redirect::to('setting/profil');
		}

		public function generateKodeKonfirm()
		{
			$puskesmas = DB::table('puskesmas')->get();
			echo "Puskesmas";
			foreach ($puskesmas as $key => $val) {
				$random = str_random(5);
				echo"<pre>";print_r($val->puskesmas_id.'|'.$random);echo"</pre>";
				DB::table('puskesmas')->WHERE('puskesmas_id', $val->puskesmas_id)->update(['konfirmasi' => $random]);
			}
			echo "Rumasakit";
			$rs = DB::table('rumahsakit2')->get();
			foreach ($rs as $key => $val) {
				$random = str_random(5);
				echo"<pre>";print_r($val->id.'|'.$random);echo"</pre>";
				DB::table('rumahsakit2')->WHERE('id', $val->id)->update(['konfirmasi' => $random]);
			}
			echo "Kabupaten";
			$kabupaten = DB::table('kabupaten')->get();
			foreach ($kabupaten as $key => $val) {
				$random = str_random(5);
				echo"<pre>";print_r($val->id_kabupaten.'|'.$random);echo"</pre>";
				DB::table('kabupaten')->WHERE('id_kabupaten', $val->id_kabupaten)->update(['konfirmasi' => $random]);
			}
			echo "Provinsi";
			$provinsi = DB::table('provinsi')->get();
			foreach ($provinsi as $key => $val) {
				$random = str_random(5);
				echo"<pre>";print_r($val->id_provinsi.'|'.$random);echo"</pre>";
				DB::table('provinsi')->WHERE('id_provinsi', $val->id_provinsi)->update(['konfirmasi' => $random]);
			}
			echo "success";
		}

		public function konfirmpuskesmas($id) {
			$cek = DB::SELECT("
				SELECT
				a.puskesmas_id, a.puskesmas_code_faskes, a.puskesmas_name,a.konfirmasi,b.kabupaten, c.provinsi, d.kecamatan
				FROM puskesmas AS a
				LEFT JOIN kecamatan AS d ON a.kode_kec=d.id_kecamatan
				LEFT JOIN kabupaten AS b ON a.kode_kab=b.id_kabupaten
				LEFT JOIN provinsi AS c ON a.kode_prop=c.id_provinsi
				WHERE
				b.kabupaten LIKE '%$id%'
				");
			foreach ($cek as $key => $val) {
				if ($val->konfirmasi == '') {
					$random = str_random(5);
					$push   = ['konfirmasi' => $random];
					DB::table('puskesmas')->WHERE('puskesmas_id', $val->puskesmas_id)->update($push);
				}
			}
			if (count($cek) > 0) {
				$random = '';
				$sent   = "<table border=1>";
				$sent .= "<tr>
				<td>Provinsi</td>
				<td>Kabupaten</td>
				<td>Kecamatan</td>
				<td>Kode Faskes</td>
				<td>Nama Faskes</td>
				<td>Konfirmasi</td>
			</tr>";
			foreach ($cek as $key => $val) {
				$sent .= "<tr>";
				$sent .= "<td>$val->provinsi</td>";
				$sent .= "<td>$val->kabupaten</td>";
				$sent .= "<td>$val->kecamatan</td>";
				$sent .= "<td>$val->puskesmas_code_faskes</td>";
				$sent .= "<td>$val->puskesmas_name</td>";
				$sent .= "<td>$val->konfirmasi</td>";
				$sent .= "</tr>";
			}
			$sent .= "</table>";
			return $sent;
		}
	}

	public function konfirmrs($id) {
		$cek = DB::SELECT("
			SELECT
			a.id, a.kode_faskes, a.nama_faskes,a.konfirmasi,b.kabupaten, c.provinsi
			FROM rumahsakit2 AS a
			LEFT JOIN kabupaten AS b ON a.kode_kab=b.id_kabupaten
			LEFT JOIN provinsi AS c ON a.kode_prop=c.id_provinsi
			WHERE
			b.kabupaten LIKE '%$id%'
			");
		foreach ($cek as $key => $val) {
			if ($val->konfirmasi == '') {
				$random = str_random(5);
				$push   = ['konfirmasi' => $random];
				DB::table('rumahsakit2')->WHERE('id', $val->id)->update($push);
			}
		}

		if (count($cek) > 0) {
			$random = '';
			$sent   = "<table border=1>";
			$sent .= "<tr>
			<td>Provinsi</td>
			<td>Kabupaten</td>
			<td>Kode Faskes</td>
			<td>Nama Faskes</td>
			<td>Konfirmasi</td>
		</tr>";
		foreach ($cek as $key => $val) {
			$sent .= "<tr>";
			$sent .= "<td>$val->provinsi</td>";
			$sent .= "<td>$val->kabupaten</td>";
			$sent .= "<td>$val->kode_faskes</td>";
			$sent .= "<td>$val->nama_faskes</td>";
			$sent .= "<td>$val->konfirmasi</td>";
			$sent .= "</tr>";
		}
		$sent .= "</table>";
		return $sent;
	}
}

	public function konfirmkemenkes() {
		$random = '';
		$sent   = '<table border=1>';
		$sent .= "<tr>
					<td>Kemenkes</td>
					<td>Konfirmasi</td>
				</tr>";
		$sent .= "<tr>";
		$sent .= "<td>KEMENKES</td>";
		$sent .= "<td>kemenkes0k3</td>";
		$sent .= "</tr>";
		$sent .= "</table>";
		return $sent;
	}

public function konfirmkab($id) {
	$dt = DB::SELECT("
		SELECT * FROM kabupaten WHERE kabupaten LIKE '%$id%'
		");
	foreach ($dt as $key => $val) {
		if ($val->konfirmasi == '') {
			$random = str_random(5);
			$push   = ['konfirmasi' => $random];
			DB::table('kabupaten')->WHERE('id_kabupaten', $val->id_kabupaten)->update($push);
		}
	}
	if (count($dt) > 0) {
		$random = '';
		$sent   = '<table border=1>';
		$sent .= "<tr>
		<td>Kabupaten</td>
		<td>Konfirmasi</td>
	</tr>";
	foreach ($dt as $key => $val) {
		$sent .= "<tr>";
		$sent .= "<td>$val->kabupaten</td>";
		$sent .= "<td>$val->konfirmasi</td>";
		$sent .= "</tr>";
	}
	$sent .= "</table>";
	return $sent;
}
}

public function konfirmprov($id) {
	$dt = DB::SELECT("
		SELECT * FROM provinsi WHERE provinsi LIKE '%$id%'
		");
	foreach ($dt as $key => $val) {
		if ($val->konfirmasi == '') {
			$random = str_random(5);
			$push   = ['konfirmasi' => $random];
			DB::table('provinsi')->WHERE('id_provinsi', $val->id_provinsi)->update($push);
		}
	}
	if (count($dt) > 0) {
		$random = '';
		$sent   = '<table border=1>';
		$sent .= "<tr>
		<td>Provinsi</td>
		<td>Konfirmasi</td>
	</tr>";
	foreach ($dt as $key => $val) {
		$sent .= "<tr>";
		$sent .= "<td>$val->provinsi</td>";
		$sent .= "<td>$val->konfirmasi</td>";
		$sent .= "</tr>";
	}
	$sent .= "</table>";
	return $sent;
}
}

public function postSaveProfilPuskesmas() {
	$data            = array();
	$data['status']  = "?";
	$data['message'] = "?";
	$data['address'] = "?";

	if (!Input::get('puskesmas_code')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Puskesmas <br> Kode Puskesmas masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('alamat')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Puskesmas <br> Alamat masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('kode_konfirmasi')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Puskesmas <br> Kode Konfirmasi masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('penanggungjawab_1')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Puskesmas <br> Penanggungjawab 1 masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('penanggungjawab_2')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Puskesmas <br> Penanggungjawab 2 masih Kosong";
		$data['address'] = "";
	} else {

		$q = "
		SELECT count(*) jml
		FROM puskesmas
		WHERE
		puskesmas_code_faskes='" . Input::get('puskesmas_code') . "'
		AND konfirmasi='" . Input::get('kode_konfirmasi') . "'";
		$result   = DB::select($q);
		$jml_data = $result[0]->jml;
		if ($jml_data == 0) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Puskesmas <br> Salah kode konfirmasi !";
			$data['address'] = "";
		} else {
			$q = "
			SELECT count(*) jml
			FROM profil_puskesmas
			WHERE
			kode_faskes='" . Input::get('puskesmas_code') . "'
			AND created_by='" . Sentry::getUser()->id . "'";
			$result   = DB::select($q);
			$jml_data = $result[0]->jml;
			if ($jml_data != 0) {
				$data['status']  = "error";
				$data['message'] = "Gagal Register Profil Puskesmas <br> Profil Puskesmas Sudah ada !";
				$data['address'] = "";
			} else {

				try {
					$id = DB::table('profil_puskesmas')->insertGetId(
						array(
							'kode_faskes'       => Input::get('puskesmas_code'),
							'alamat'            => Input::get('alamat'),
							'konfirmasi'        => Input::get('kode_konfirmasi'),
							'penanggungjawab_1' => Input::get('penanggungjawab_1'),
							'penanggungjawab_2' => Input::get('penanggungjawab_2'),
							'created_dttm'      => date('Y-m-d H:i:s'),
							'created_by'        => Sentry::getUser()->id,
							)
						);
					$data['status']  = "success";
					$data['message'] = "Sukses Register Profil Puskesmas";
					$data['address'] = URL::to('pilih_profil');
				} catch (Exception $e) {
												//kembalikan ke halaman login
												//return Redirect::back();
					$data['status']  = "error";
					$data['message'] = "Gagal Register Profil Puskesmas !";
					$data['address'] = "";
				}
			}
		}
	}

	return $data;
}

public function postSaveProfilRS() {
	$data            = array();
	$data['status']  = "?";
	$data['message'] = "?";
	$data['address'] = "?";

	if (!Input::get('rs_code')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil RS <br> Kode RS masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('alamat')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil RS <br> Alamat masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('kode_konfirmasi')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil RS <br> Kode Konfirmasi masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('penanggungjawab_1')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil RS <br> Penanggungjawab 1 masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('penanggungjawab_2')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil RS <br> Penanggungjawab 2 masih Kosong";
		$data['address'] = "";
	} else {

		$q = "
		SELECT count(*) jml
		FROM rumahsakit2
		WHERE
		kode_faskes='" . Input::get('rs_code') . "'
		AND konfirmasi='" . Input::get('kode_konfirmasi') . "'";
		$result   = DB::select($q);
		$jml_data = $result[0]->jml;
		if ($jml_data == 0) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Rumah Sakit <br> Salah kode konfirmasi !";
			$data['address'] = "";
		} else {

			$q = "
			SELECT count(*) jml
			FROM profil_rs
			WHERE
			kode_faskes='" . Input::get('rs_code') . "'
			AND created_by='" . Sentry::getUser()->id . "'";
			$result   = DB::select($q);
			$jml_data = $result[0]->jml;
			if ($jml_data != 0) {
				$data['status']  = "error";
				$data['message'] = "Gagal Register Profil Rumah Sakit <br> Profil Rumah Sakit Sudah ada !";
				$data['address'] = "";
			} else {

				try {
					$id = DB::table('profil_rs')->insertGetId(
						array(
							'kode_faskes'       => Input::get('rs_code'),
							'alamat'            => Input::get('alamat'),
							'konfirmasi'        => Input::get('kode_konfirmasi'),
							'penanggungjawab_1' => Input::get('penanggungjawab_1'),
							'penanggungjawab_2' => Input::get('penanggungjawab_2'),
							'created_dttm'      => date('Y-m-d H:i:s'),
							'created_by'        => Sentry::getUser()->id,
							)
						);
					$data['status']  = "success";
					$data['message'] = "Sukses Register Profil Rumah Sakit";
					$data['address'] = URL::to('pilih_profil');
				} catch (Exception $e) {
												//kembalikan ke halaman login
												//return Redirect::back();
					$data['status']  = "error";
					$data['message'] = "Gagal Register Profil Rumah Sakit !";
					$data['address'] = "";
				}
			}
		}
	}

	return $data;
}

public function postSaveProfilLaboratorium() {
	$data            = array();
	$data['status']  = "?";
	$data['message'] = "?";
	$data['address'] = "?";

	if (!Input::get('laboratorium_code')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Laboratorium <br> Kode Laboratorium masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('alamat')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Laboratorium <br> Alamat masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('kode_konfirmasi')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Laboratorium <br> Kode Konfirmasi masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('penanggungjawab_1')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Laboratorium <br> Penanggungjawab 1 masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('penanggungjawab_2')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Laboratorium <br> Penanggungjawab 2 masih Kosong";
		$data['address'] = "";
	} else {

		$q = "
		SELECT count(*) jml
		FROM laboratorium
		WHERE
		lab_code='" . Input::get('laboratorium_code') . "'
		AND konfirmasi='" . Input::get('kode_konfirmasi') . "'";
		$result   = DB::select($q);
		$jml_data = $result[0]->jml;
		if ($jml_data == 0) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Laboratorium <br> Salah kode konfirmasi !";
			$data['address'] = "";
		} else {

			$q = "
			SELECT count(*) jml
			FROM profil_laboratorium
			WHERE
			kode_lab='" . Input::get('laboratorium_code') . "'
			AND created_by='" . Sentry::getUser()->id . "'";
			$result   = DB::select($q);
			$jml_data = $result[0]->jml;
			if ($jml_data != 0) {
				$data['status']  = "error";
				$data['message'] = "Gagal Register Profil Laboratorium <br> Profil Laboratorium Sudah ada !";
				$data['address'] = "";
			} else {

				try {
					$id = DB::table('profil_laboratorium')->insertGetId(
						array(
							'kode_lab'          => Input::get('laboratorium_code'),
							'alamat'            => Input::get('alamat'),
							'konfirmasi'        => Input::get('kode_konfirmasi'),
							'penanggungjawab_1' => Input::get('penanggungjawab_1'),
							'penanggungjawab_2' => Input::get('penanggungjawab_2'),
							'created_dttm'      => date('Y-m-d H:i:s'),
							'created_by'        => Sentry::getUser()->id,
							)
						);
					$data['status']  = "success";
					$data['message'] = "Sukses Register Profil Laboratorium";
					$data['address'] = URL::to('pilih_profil');
				} catch (Exception $e) {
												//kembalikan ke halaman login
												//return Redirect::back();
					$data['status']  = "error";
					$data['message'] = "Gagal Register Profil Laboratorium !";
					$data['address'] = "";
				}
			}
		}
	}

	return $data;
}

public function postSaveProfilKabupaten() {
	$data            = array();
	$data['status']  = "?";
	$data['message'] = "?";
	$data['address'] = "?";
	if (!Input::get('district_id')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Dinas Kesehatan Kabupaten <br> Kode Dinas Kesehatan Kabupaten masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('alamat')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Dinas Kesehatan Kabupaten <br> Alamat masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('kode_konfirmasi')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Dinas Kesehatan Kabupaten <br> Kode Konfirmasi masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('penanggungjawab_1')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Dinas Kesehatan Kabupaten <br> Penanggungjawab 1 masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('penanggungjawab_2')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Puskesmas <br> Penanggungjawab 2 masih Kosong";
		$data['address'] = "";
	} else {

		$q = "
		SELECT count(*) jml
		FROM kabupaten
		WHERE
		id_kabupaten='" . Input::get('district_id') . "'
		AND konfirmasi='" . Input::get('kode_konfirmasi') . "'";
		$result   = DB::select($q);
		$jml_data = $result[0]->jml;
		if ($jml_data == 0) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Dinas Kesehatan Kabupaten <br> Salah kode konfirmasi !";
			$data['address'] = "";
		} else {
			$q = "
			SELECT count(*) jml
			FROM profil_dinkes_kabupaten
			WHERE
			kode_kabupaten='" . Input::get('district_id') . "'
			AND created_by='" . Sentry::getUser()->id . "'";
			$result   = DB::select($q);
			$jml_data = $result[0]->jml;
			if ($jml_data != 0) {
				$data['status']  = "error";
				$data['message'] = "Gagal Register Profil Dinas Kesehatan Kabupaten <br> Profil Dinas Kesehatan Sudah ada !";
				$data['address'] = "";
			} else {

				try {
					$id = DB::table('profil_dinkes_kabupaten')->insertGetId(
						array(
							'kode_kabupaten'    => Input::get('district_id'),
							'alamat'            => Input::get('alamat'),
							'penanggungjawab_1' => Input::get('penanggungjawab_1'),
							'penanggungjawab_2' => Input::get('penanggungjawab_2'),
							'created_dttm'      => date('Y-m-d H:i:s'),
							'created_by'        => Sentry::getUser()->id,
							)
						);
					$data['status']  = "success";
					$data['message'] = "Sukses Register Profil Dinas Kesehatan Kabupaten";
					$data['address'] = URL::to('pilih_profil');
				} catch (Exception $e) {
												//kembalikan ke halaman login
												//return Redirect::back();
					$data['status']  = "error";
					$data['message'] = "Gagal Register Profil Dinas Kesehatan Kabupaten";
					$data['address'] = "";
				}
			}
		}
	}

	return $data;
}

public function postSaveProfilProvinsi() {
	$data            = array();
	$data['status']  = "?";
	$data['message'] = "?";
	$data['address'] = "?";

	if (!Input::get('province_id')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Dinas Kesehatan Provinsi <br> Kode Provinsi masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('alamat')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Dinas Kesehatan Provinsi <br> Alamat masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('kode_konfirmasi')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Dinas Kesehatan Provinsi <br> Kode Konfirmasi masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('penanggungjawab_1')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Dinas Kesehatan Provinsi <br> Penanggungjawab 1 masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('penanggungjawab_2')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Dinas Kesehatan Provinsi <br> Penanggungjawab 2 masih Kosong";
		$data['address'] = "";
	} else {

		$q = "
		SELECT count(*) jml
		FROM provinsi
		WHERE
		id_provinsi='" . Input::get('province_id') . "'
		AND konfirmasi='" . Input::get('kode_konfirmasi') . "'";
		$result   = DB::select($q);
		$jml_data = $result[0]->jml;
		if ($jml_data == 0) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Dinas Kesehatan Kabupaten <br> Salah kode konfirmasi !";
			$data['address'] = "";
		} else {

			$q = "
			SELECT count(*) jml
			FROM profil_dinkes_provinsi
			WHERE
			kode_provinsi='" . Input::get('province_id') . "'
			AND created_by='" . Sentry::getUser()->id . "'";
			$result   = DB::select($q);
			$jml_data = $result[0]->jml;
			if ($jml_data != 0) {
				$data['status']  = "error";
				$data['message'] = "Gagal Register Profil Dinas Kesehatan Provinsi <br> Profil Dinas Kesehatan Provinsi Sudah ada !";
				$data['address'] = "";
			} else {
				try {
					DB::table('profil_dinkes_provinsi')->insertGetId(
						array(
							'kode_provinsi'     => Input::get('province_id'),
							'alamat'            => Input::get('alamat'),
							'penanggungjawab_1' => Input::get('penanggungjawab_1'),
							'penanggungjawab_2' => Input::get('penanggungjawab_2'),
							'created_dttm'      => date('Y-m-d H:i:s'),
							'created_by'        => Sentry::getUser()->id,
							)
						);
					$data['status']  = "success";
					$data['message'] = "Sukses Register Profil Dinas Kesehatan Provinsi";
					$data['address'] = URL::to('pilih_profil');
				} catch (Exception $e) {
												//kembalikan ke halaman login
												//return Redirect::back();
					$data['status']  = "error";
					$data['message'] = "Gagal Register Profil Dinas Kesehatan Provinsi";
					$data['address'] = "";
				}
			}
		}
	}

	return $data;
}

public function postSaveProfilPusat() {
	$data            = array();
	$data['status']  = "?";
	$data['message'] = "?";
	$data['address'] = "?";

	if (!Input::get('kode_konfirmasi')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Kemenkes <br> Kode Konfirmasi masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('alamat')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Kemenkes <br> Alamat masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('penanggungjawab_1')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Kemenkes <br> Penanggungjawab 1 masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('penanggungjawab_2')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register Profil Kemenkes <br> Penanggungjawab 2 masih Kosong";
		$data['address'] = "";
	} else {
		$q = "
		SELECT count(*) jml
		FROM profil_kemenkes
		WHERE
		created_by='" . Sentry::getUser()->id . "'";
		$result   = DB::select($q);
		$jml_data = $result[0]->jml;
		if ($jml_data != 0) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Dinas Kesehatan Provinsi <br> Profil Dinas Kesehatan Provinsi Sudah ada !";
			$data['address'] = "";
		} else {
			if (Input::get('kode_konfirmasi') != 'kemenkes0k3') {
				$data['status']  = "error";
				$data['message'] = "Gagal Register Profil Kemenkes <br> Salah kode konfirmasi !";
				$data['address'] = "";
			} else {
				try {
					DB::table('profil_kemenkes')->insertGetId(
						array(
							'alamat'            => Input::get('alamat'),
							'penanggungjawab_1' => Input::get('penanggungjawab_1'),
							'penanggungjawab_2' => Input::get('penanggungjawab_2'),
							'created_dttm'      => date('Y-m-d H:i:s'),
							'created_by'        => Sentry::getUser()->id,
							)
						);
					$data['status']  = "success";
					$data['message'] = "Sukses Register Profil Kemenkes";
					$data['address'] = URL::to('pilih_profil');
				} catch (Exception $e) {
												//kembalikan ke halaman login
												//return Redirect::back();
					$data['status']  = "error";
					$data['message'] = "Gagal Register Profil Kemenkes";
					$data['address'] = "";
				}
			}
		}
	}

	return $data;
}

public function postSaveSignup() {
	$data            = array();
	$data['status']  = "?";
	$data['message'] = "?";
	$data['address'] = "?";

	if (!Input::get('first_name')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register User <br> Suku kata pertama pada nama anda masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('last_name')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register User <br> Suku terakhir pada nama anda masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('instansi')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register User <br> Nama Instansi masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('jabatan')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register User <br> Jabatan masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('email')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register User <br> Email masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('password')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register User <br> Password masih Kosong";
		$data['address'] = "";
	} else if (!Input::get('password2')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register User <br> Ulangi Password masih Kosong";
		$data['address'] = "";
	} else if (Input::get('password2') != Input::get('password')) {
		$data['status']  = "error";
		$data['message'] = "Gagal Register User <br> Kedua password harus sama";
		$data['address'] = "";
	} else {
		try {
			DB::table('users')->insertGetId(
				array(
					'first_name' => Input::get('first_name'),
					'last_name'  => Input::get('last_name'),
					'password'   => Hash::make(Input::get('password')),
					'email'      => Input::get('email'),
					'instansi'   => Input::get('instansi'),
					'jabatan'    => Input::get('jabatan'),
					'jk'         => Input::get('jk'),
					'no_telp'    => Input::get('no_telp'),
					'created_at' => date('Y-m-d H:i:s'),
					'activated'  => '1',
					)
				);
			$data['status']  = "success";
			$data['message'] = "Sukses Daftar User";
			$data['address'] = URL::to('/');
		} catch (Exception $e) {
								//kembalikan ke halaman login
								//return Redirect::back();
			$data['status']  = "error";
			$data['message'] = "Gagal Daftar User, Email Sudah Ada";
			$data['address'] = "";
		}
	}
	return $data;
}

public function getDeleteProfile() {
	$data       = array();
	$profile_id = Input::get('profile_id');
	$instansi   = Input::get('instansi');

	if ($instansi == 'puskesmas') {
		DB::table('profil_puskesmas')->where('id', '=', $profile_id)->delete();
		$data['status']  = "success";
		$data['message'] = "Sukses menghapus profil";
		$data['address'] = URL::to('pilih_profil');
	} else if ($instansi == 'kabupaten') {
		DB::table('profil_dinkes_kabupaten')->where('id', '=', $profile_id)->delete();
		$data['status']  = "success";
		$data['message'] = "Sukses menghapus profil";
		$data['address'] = URL::to('pilih_profil');
	} else if ($instansi == 'provinsi') {
		DB::table('profil_dinkes_provinsi')->where('id', '=', $profile_id)->delete();
		$data['status']  = "success";
		$data['message'] = "Sukses menghapus profil";
		$data['address'] = URL::to('pilih_profil');
	} else if ($instansi == 'kemenkes') {
		DB::table('profil_kemenkes')->where('id', '=', $profile_id)->delete();
		$data['status']  = "success";
		$data['message'] = "Sukses menghapus profil";
		$data['address'] = URL::to('pilih_profil');
	} else if ($instansi == 'laboratorium') {
		DB::table('profil_laboratorium')->where('id', '=', $profile_id)->delete();
		$data['status']  = "success";
		$data['message'] = "Sukses menghapus profil";
		$data['address'] = URL::to('pilih_profil');
	} else if ($instansi == 'rumahsakit2') {
		DB::table('profil_rs')->where('id', '=', $profile_id)->delete();
		$data['status']  = "success";
		$data['message'] = "Sukses menghapus profil";
		$data['address'] = URL::to('pilih_profil');
	}

	return $data;
}

public function getRegisterAdmin() {
	$data       = array();
	$profile_id = Input::get('profile_id');
	$instansi   = Input::get('instansi');

	DB::table('profil_puskesmas')
	->where('created_by', Sentry::getUser()->id)
	->update(array('is_admin' => 'n'));
	DB::table('profil_dinkes_kabupaten')
	->where('created_by', Sentry::getUser()->id)
	->update(array('is_admin' => 'n'));
	DB::table('profil_dinkes_provinsi')
	->where('created_by', Sentry::getUser()->id)
	->update(array('is_admin' => 'n'));
	DB::table('profil_kemenkes')
	->where('created_by', Sentry::getUser()->id)
	->update(array('is_admin' => 'n'));
	DB::table('profil_laboratorium')
	->where('created_by', Sentry::getUser()->id)
	->update(array('is_admin' => 'n'));
	DB::table('profil_rs')
	->where('created_by', Sentry::getUser()->id)
	->update(array('is_admin' => 'n'));

	if ($instansi == 'puskesmas') {
		$q = "
		SELECT COUNT(*) as jml
		FROM profil_puskesmas a
		WHERE a.is_admin='y' AND a.kode_faskes ='" . $profile_id . "'
		";
		$data_jml = DB::select($q);
		if ($data_jml[0]->jml >= 2) {
			$data['status']  = "error";
			$data['message'] = "Gagal, Admin Maksimal 2 !";
			$data['address'] = URL::to('pilih_profil');
		} else {
			DB::table('profil_puskesmas')
			->where('id', $profile_id)
			->update(array('is_admin' => 'y'));
			$data['status']  = "success";
			$data['message'] = "Sukses, Anda menjadi admin";
			$data['address'] = URL::to('pilih_profil');
		}
	} else if ($instansi == 'laboratorium') {
		$q = "
		SELECT COUNT(*) as jml
		FROM profil_laboratorium a
		WHERE a.is_admin='y' AND a.kode_lab ='" . $profile_id . "'
		";
		$data_jml = DB::select($q);
		if ($data_jml[0]->jml >= 2) {
			$data['status']  = "error";
			$data['message'] = "Gagal, Admin Maksimal 2 !";
			$data['address'] = URL::to('pilih_profil');
		} else {
			DB::table('profil_laboratorium')
			->where('id', $profile_id)
			->update(array('is_admin' => 'y'));
			$data['status']  = "success";
			$data['message'] = "Sukses, Anda menjadi admin";
			$data['address'] = URL::to('pilih_profil');
		}
	} else if ($instansi == 'kabupaten') {
		$q = "
		SELECT COUNT(*) as jml
		FROM profil_dinkes_kabupaten a
		WHERE a.is_admin='y' AND a.kode_kabupaten ='" . $profile_id . "'
		";
		$data_jml = DB::select($q);
		if ($data_jml[0]->jml >= 2) {
			$data['status']  = "error";
			$data['message'] = "Gagal, Admin Maksimal 2 !";
			$data['address'] = URL::to('pilih_profil');
		} else {
			DB::table('profil_dinkes_kabupaten')
			->where('id', $profile_id)
			->update(array('is_admin' => 'y'));
			$data['status']  = "success";
			$data['message'] = "Sukses, Anda menjadi admin";
			$data['address'] = URL::to('pilih_profil');
		}
	} else if ($instansi == 'provinsi') {
		$q = "
		SELECT COUNT(*) as jml
		FROM profil_dinkes_provinsi a
		WHERE a.is_admin='y' AND a.kode_provinsi ='" . $profile_id . "'
		";
		$data_jml = DB::select($q);
		if ($data_jml[0]->jml >= 2) {
			$data['status']  = "error";
			$data['message'] = "Gagal, Admin Maksimal 2 !";
			$data['address'] = URL::to('pilih_profil');
		} else {
			DB::table('profil_dinkes_provinsi')
			->where('id', $profile_id)
			->update(array('is_admin' => 'y'));
			$data['status']  = "success";
			$data['message'] = "Sukses, Anda menjadi admin";
			$data['address'] = URL::to('pilih_profil');
		}
	} else if ($instansi == 'kemenkes') {
		$q = "
		SELECT COUNT(*) as jml
		FROM profil_kemenkes a
		WHERE a.is_admin='y'
		";
		$data_jml = DB::select($q);
		if ($data_jml[0]->jml >= 2) {
			$data['status']  = "error";
			$data['message'] = "Gagal, Admin Maksimal 2 !";
			$data['address'] = URL::to('pilih_profil');
		} else {
			DB::table('profil_kemenkes')
			->where('id', $profile_id)
			->update(array('is_admin' => 'y'));
			$data['status']  = "success";
			$data['message'] = "Sukses, Anda menjadi admin";
			$data['address'] = URL::to('pilih_profil');
		}
	} else if ($instansi == 'rumahsakit2') {
		$q = "
		SELECT COUNT(*) as jml
		FROM profil_rs a
		WHERE a.is_admin='y'
		";
		$data_jml = DB::select($q);
		if ($data_jml[0]->jml >= 2) {
			$data['status']  = "error";
			$data['message'] = "Gagal, Admin Maksimal 2 !";
			$data['address'] = URL::to('pilih_profil');
		} else {
			DB::table('profil_rs')
			->where('id', $profile_id)
			->update(array('is_admin' => 'y'));
			$data['status']  = "success";
			$data['message'] = "Sukses, Anda menjadi admin";
			$data['address'] = URL::to('pilih_profil');
		}
	}

	return $data;
}

public function logout() {

				//logout
	Sentry::logout();
	Session::flush();
				//redirect ke halaman login
	return Redirect::to('/');
}

}
