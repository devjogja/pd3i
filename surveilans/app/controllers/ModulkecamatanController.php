<?php
class ModulkecamatanController extends BaseController {
	
	public function getIndex()
	{
		return View::make('master_data.modulkecamatan');
	}
	
	public function getList()
	{
		$modulkecamatan = ModulkecamatanModel::getListModulkecamatan();
		$current_page = Input::get('page');
		$current_search = Input::get('search_name');		
		return View::make('master_data.modulkecamatan_list',compact('modulkecamatan','current_page','current_search'));
	}
	
	public function getListGet()
	{
		$modulkecamatan = ModulkecamatanModel::getListModulkecamatan();
		$current_page = Input::get('page');
		$current_search = Input::get('search_name');		
		return View::make('master_data.modulkecamatan_list',compact('modulkecamatan','current_page','current_search'));
	}
	
	public function ModulkecamatanProcessForm(){
		$ret = array();
        if(Input::get('saved_id')) {
			$ret['command'] = "updating data";
			if(ModulkecamatanModel::DoUpdateData()) {
				$ret['msg'] = "Sukses Update Data";
				$ret['status'] = "success";
			} else {
				$ret['msg'] = "Gagal Update Data";
				$ret['status'] = "error";
			}
        } else {
			$ret['command'] = "adding data";
			$last_id = ModulkecamatanModel::DoAddData();
			if($last_id > 0) {
				$ret['msg'] = "Sukses Simpan Data";
				$ret['status'] = "success";
			} else {
				$ret['msg'] = "Gagal Simpan Data";
				$ret['status'] = "error";
			}
        }
		
		echo json_encode($ret);
		
	}
	
	public function ModulkecamatanGetDataById(){
		$provinsi = ModulkecamatanModel::getModulkecamatan();
		echo json_encode($provinsi);
	}
	
	public function ModulkecamatanDeleteList(){
		ModulkecamatanModel::DoDeleteModulkecamatan();
		$ret['msg'] = "Sukses Delete Data";
		$ret['status'] = "success";
		echo json_encode($ret);
	}
	
}
?>
