<?php
class ModulrumahsakitController extends BaseController
{

    public function getIndex()
    {
        return View::make('master_data.modulrumahsakit');
    }

    public function getList()
    {
        $modulrumahsakit = ModulrumahsakitModel::getListModulrumahsakit();
        $current_page    = Input::get('page');
        $current_search  = Input::get('search_name');
        return View::make('master_data.modulrumahsakit_list', compact('modulrumahsakit', 'current_page', 'current_search'));
    }

    public function getListGet()
    {
        $modulrumahsakit = ModulrumahsakitModel::getListModulrumahsakit();
        $current_page    = Input::get('page');
        $current_search  = Input::get('search_name');
        return View::make('master_data.modulrumahsakit_list', compact('modulrumahsakit', 'current_page', 'current_search'));
    }

    public function ModulrumahsakitProcessForm()
    {
        $ret = array();
        if (Input::get('saved_id')) {
            $ret['command'] = "updating data";
            if (ModulrumahsakitModel::DoUpdateData()) {
                $ret['msg']    = "Sukses Update Data";
                $ret['status'] = "success";
            } else {
                $ret['msg']    = "Gagal Update Data";
                $ret['status'] = "error";
            }
        } else {
            $ret['command'] = "adding data";
            $last_id        = ModulrumahsakitModel::DoAddData();
            if ($last_id > 0) {
                $ret['msg']    = "Sukses Simpan Data";
                $ret['status'] = "success";
            } else {
                $ret['msg']    = "Gagal Simpan Data";
                $ret['status'] = "error";
            }
        }

        echo json_encode($ret);

    }

    public function ModulrumahsakitGetDataById()
    {
        $provinsi = ModulrumahsakitModel::getModulrumahsakit();
        echo json_encode($provinsi);
    }

    public function ModulrumahsakitDeleteList()
    {
        ModulrumahsakitModel::DoDeleteModulrumahsakit();
        $ret['msg']    = "Sukses Delete Data";
        $ret['status'] = "success";
        echo json_encode($ret);
    }

}
