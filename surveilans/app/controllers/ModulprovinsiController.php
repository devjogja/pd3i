<?php
class ModulprovinsiController extends BaseController {
	
	public function getIndex()
	{
		return View::make('master_data.modulprovinsi');
	}
	
	public function getList()
	{
		$modulprovinsi = ModulprovinsiModel::getListModulprovinsi();
		$current_page = Input::get('page');
		$current_search = Input::get('search_name');		
		return View::make('master_data.modulprovinsi_list',compact('modulprovinsi','current_page','current_search'));
	}
	
	public function getListGet()
	{
		$modulprovinsi = ModulprovinsiModel::getListModulprovinsi();
		$current_page = Input::get('page');
		$current_search = Input::get('search_name');		
		return View::make('master_data.modulprovinsi_list',compact('modulprovinsi','current_page','current_search'));
	}
	
	public function ModulprovinsiProcessForm(){
		$ret = array();
        if(Input::get('saved_id')) {
			$ret['command'] = "updating data";
			if(ModulprovinsiModel::DoUpdateData()) {
				$ret['msg'] = "Sukses Update Data";
				$ret['status'] = "success";
			} else {
				$ret['msg'] = "Gagal Update Data";
				$ret['status'] = "error";
			}
        } else {
			$ret['command'] = "adding data";
			$last_id = ModulprovinsiModel::DoAddData();
			if($last_id > 0) {
				$ret['msg'] = "Sukses Simpan Data";
				$ret['status'] = "success";
			} else {
				$ret['msg'] = "Gagal Simpan Data";
				$ret['status'] = "error";
			}
        }
		
		echo json_encode($ret);
		
	}
	
	public function ModulprovinsiGetDataById(){
		$provinsi = ModulprovinsiModel::getModulprovinsi();
		echo json_encode($provinsi);
	}
	
	public function ModulprovinsiDeleteList(){
		ModulprovinsiModel::DoDeleteModulprovinsi();
		$ret['msg'] = "Sukses Delete Data";
		$ret['status'] = "success";
		echo json_encode($ret);
	}
	
}
?>
