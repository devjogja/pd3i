<?php

class AfpController extends \BaseController {

	public function __construct()
	{
		$this->beforeFilter('auth');
	}

	/**
	 * Display a listing of afp
	 *
	 * @return Response
	 */
	public function index()
	{
		Session::put('sess_id', '');
		Session::put('penyakit', 'AFP');
		Session::put('sess_penyakit','afp');
		$between = '';
		$district = '';
		if(!empty(Input::get())){
			$unit            = Input::get('unit');
			$day_start       = Input::get('day_start');
			$month_start     = Input::get('month_start');
			$year_start      = Input::get('year_start');
			$day_end         = Input::get('day_end');
			$month_end       = Input::get('month_end');
			$year_end        = Input::get('year_end');
			$province_id     = Input::get('province_id');
			$district_id     = Input::get('district_id');
			$sub_district_id = Input::get('sub_district_id');
			$rs_id			 = Input::get('rs_id');
			$puskesmas_id	 = Input::get('puskesmas_id');

			switch($unit) {
				case "all" :
					$between = "";
				break;
				case "year" :
					$start = $year_start;
					$end = $year_end;
					$between = " AND YEAR(afp_tanggal_mulai_lumpuh) BETWEEN '".$start."' AND '".$end."' ";
				break;
				case "month" :
					$start = $year_start . '-' . $month_start . '-1';
					$end = $year_end . '-' . $month_end . '-' . date('t', mktime(0,0,0,$month_end, 1, $year_end));
					$between = " AND DATE(afp_tanggal_mulai_lumpuh) BETWEEN STR_TO_DATE('".$start."', '%Y-%c-%e') AND STR_TO_DATE('".$end."', '%Y-%c-%e') ";
				break;
				default :
					$start = $year_start . '-' . $month_start . '-' . $day_start;
					$end = $year_end . '-' . $month_end . '-' . $day_end;
					$between = " AND DATE(afp_tanggal_mulai_lumpuh) BETWEEN STR_TO_DATE('".$start."', '%Y-%c-%e') AND STR_TO_DATE('".$end."', '%Y-%c-%e') ";
				break;
			}

			if($puskesmas_id){
				$district = "AND puskesmas_code_faskes='".$puskesmas_id."'";
			} else if($sub_district_id){
				$district = "AND pasien_id_kecamatan='".$sub_district_id."'";
			} else if($rs_id) {
				$district = "AND rs_kode_faskes='".$rs_id."' ";
			} else if($district_id) {
				$district = "AND pasien_id_kabupaten='".$district_id."' ";
			} else if($province_id) {
				$district = "AND pasien_id_provinsi='".$province_id."' ";
			} 
		}

		$data= Afp::pasienafp($between, $district);
		return View::make('pemeriksaan.afp.index',compact('data'));
	}

	/**
	 * Show the form for creating a new afp
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('pemeriksaan.afp.create');
	}

	/**
	 * Store a newly created afp in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//lakukan transaksi
		DB::transaction(function()
		{
			$type = Session::get('type');
			$kd_faskes = Session::get('kd_faskes');
			$faskes_id = Sentry::getUser()->id_user;
			//simpan data personal
			$id_pasien = DB::table('pasien')->insertGetId(array(
						'nik'           => Input::get('nik'),
						'nama_anak'     => Input::get('nama_anak'),
						'nama_ortu'     => Input::get('nama_ortu'),
						'alamat'        => Input::get('alamat'),
						'tanggal_lahir' => $this->encode_date(Input::get('tanggal_lahir')),
						'umur'          => Input::get('tgl_tahun'),
						'umur_bln'      => Input::get('tgl_bulan'),
						'umur_hr'       => Input::get('tgl_hari'),
						'jenis_kelamin' => Input::get('jenis_kelamin'),
						'id_kelurahan'  => Input::get('id_kelurahan')
					));

			//simpan data diagnosa penyakit afp
			$id_afp = DB::table('afp')->insertGetId(array(
						'no_epid'                           => Input::get('no_epid'),
						'no_epid_lama'                      => Input::get('no_epid_lama'),
						'tanggal_mulai_lumpuh'              => $this->encode_date(Input::get('tanggal_mulai_lumpuh')),
						'nama_puskesmas'					=> Input::get('nama_puskesmas'),
						'demam_sebelum_lumpuh'              => Input::get('demam_sebelum_lumpuh'),
						'kelumpuhan_anggota_gerak_kanan'    => Input::get('kelumpuhan_anggota_gerak_kanan'),
						'kelumpuhan_anggota_gerak_kiri'     => Input::get('kelumpuhan_anggota_gerak_kiri'),
						'gangguan_raba_anggota_gerak_kanan' => Input::get('gangguan_raba_anggota_gerak_kanan'),
						'gangguan_raba_anggota_gerak_kiri'  => Input::get('gangguan_raba_anggota_gerak_kiri'),
						'imunisasi_polio_sebelum_sakit'     => Input::get('imunisasi_polio_sebelum_sakit'),
						'tanggal_vaksinasi_polio'           => $this->encode_date(Input::get('tanggal_vaksinasi_polio')),
						'tanggal_laporan_diterima'          => $this->encode_date(Input::get('tanggal_laporan_diterima')),
						'tanggal_pelacakan'                 => $this->encode_date(Input::get('tanggal_pelacakan')),
						'tanggal_pengambilan_spesimen1'     => $this->encode_date(Input::get('tanggal_pengambilan_spesimen1')),
						'tanggal_pengambilan_spesimen2'     => $this->encode_date(Input::get('tanggal_pengambilan_spesimen2')),
						'jenis_spesimen'                    => Input::get('jenis_spesimen'),
						'kontak'                            => Input::get('kontak'),
						'keadaan_akhir'                     => Input::get('keadaan_akhir'),
						'klasifikasi_final'                 => Input::get('klasifikasi_final'),
						'tanggal_periksa'                   => date('Y-m-d'),
						'kode_faskes'						=>$type,
						'id_tempat_periksa'                 => $faskes_id,
						'created_at' => date('Y-m-d H:i:s'),
						'created_by' => Sentry::getUser()->id,
						'updated_at' => date('Y-m-d H:i:s'),
						'updated_by' => Sentry::getUser()->id,
					));

			//simpan ke uji laboratorium
			DB::table('hasil_uji_lab_afp')->insert(array(
					'id_afp'    => $id_afp,
					'id_pasien' => $id_pasien,
					'status'    => 0,
					'jp1' 		=> Input::get('jp1'),
					'isolasi_virus_jp1' 	=> Input::get('isolasi_virus_jp1'),
					'ket_isolasi_virus_jp1' => Input::get('ket_isolasi_virus_jp1'),
					'itd_jp1' 				=> Input::get('itd_jp1'),
					'ket_itd_jp1' 			=> Input::get('ket_itd_jp1'),
					'sequencing_jp1' 		=> Input::get('sequencing_jp1'),
					'ket_sequencing_jp1' 	=> Input::get('ket_sequencing_jp1'),
					'jp2' 					=> Input::get('jp2'),
					'isolasi_virus_jp2' 	=> Input::get('isolasi_virus_jp2'),
					'ket_isolasi_virus_jp2' => Input::get('ket_isolasi_virus_jp2'),
					'itd_jp2' 				=> Input::get('itd_jp2'),
					'ket_itd_jp2' 			=> Input::get('ket_itd_jp2'),
					'sequencing_jp2' 		=> Input::get('sequencing_jp2'),
					'ket_sequencing_jp2' 	=> Input::get('ket_sequencing_jp2'),
					'created_at' => date('Y-m-d H:i:s'),
					'created_by' => Sentry::getUser()->id,
					'updated_at' => date('Y-m-d H:i:s'),
					'updated_by' => Sentry::getUser()->id,
			));

			//simpan jenis pemeriksaan spesimen
			/*if(Input::get('jenis_pemeriksaan1')) {
				$nama_jenis_pemeriksaan1 = Input::get('nama_jenis_pemeriksaan1');
				$ket_jenis_pemeriksaan1 = Input::get('ket_jenis_pemeriksaan1');
				$count = count(Input::get('nama_jenis_pemeriksaan1'));
				for ($i=0; $i < $count; $i++) {
					DB::table('jenis_pemeriksaan')->insert(array(
						'id_afp' => $id_afp,
						'jenis_pemeriksaan' => Input::get('jenis_pemeriksaan1'),
						'nama_jenis_pemeriksaan' => $nama_jenis_pemeriksaan1[$i],
						'ket_jenis_pemeriksaan' => $ket_jenis_pemeriksaan1[$i]
				));
				}
			}*/
			//simpan jenis pemeriksaan spesimen
			/*if(Input::get('jenis_pemeriksaan2')) {
				$nama_jenis_pemeriksaan2 = Input::get('nama_jenis_pemeriksaan2');
				$ket_jenis_pemeriksaan2 = Input::get('ket_jenis_pemeriksaan2');
				$count = count(Input::get('nama_jenis_pemeriksaan2'));
				for ($i=0; $i < $count; $i++) {
					DB::table('jenis_pemeriksaan')->insert(array(
						'id_afp' => $id_afp,
						'jenis_pemeriksaan' => Input::get('jenis_pemeriksaan2'),
						'nama_jenis_pemeriksaan' => $nama_jenis_pemeriksaan2[$i],
						'ket_jenis_pemeriksaan' => $ket_jenis_pemeriksaan2[$i]
				));
				}
			}*/

			//get id kecamatan pasien dan id faskes
			if ($type=='rs') {
				$data = DB::SELECT("SELECT pasien_id_kecamatan AS id_dis_pasien, rs_kode_faskes AS id_dis_faskes FROM getdetailafp WHERE afp_id_afp=$id_afp");
			} else {
				$data = DB::SELECT("SELECT pasien_id_kecamatan AS id_dis_pasien, puskesmas_code_faskes AS id_dis_faskes FROM getdetailafp WHERE afp_id_afp=$id_afp");
			}
			$id_kec_pasien 	= (empty($data)?'':$data[0]->id_dis_pasien);
			$id_kec_faskes 	= (empty($data)?'':$data[0]->id_dis_faskes);
			//simpan notifikasi
			if($id_kec_pasien != $id_kec_faskes){
				DB::table('notification')->insert(array(
					'from_faskes_id'     => $faskes_id,
					'from_faskes_kec_id' => $id_kec_faskes,
					'to_faskes_kec_id'   => $id_kec_pasien,
					'type_id'            => 'afp',
					'global_id'          => $id_afp,
					'description'        => '',
					'submit_dttm'        => date('Y-m-d H:i:s')
				));
			}
		});

		//berhasil disimpan redirect to afp

		if(Sentry::getUser()->hak_akses==5){
			return Redirect::to('lab/afp');
		}
		else
		{
			return Redirect::to('afp');
		}
	}



	//simpan hasil laboratorium
	public function simpanUjiAfp() {
		DB::table('hasil_uji_lab_afp')->insert(array(
			'id_afp'                   => Input::get('id_afp'),
			'id_pasien'                => Input::get('id_pasien'),
			'hasil_spesimen'           => Input::get('hasil_spesimen'),
			'klasifikasi_akhir'        => Input::get('klasifikasi_akhir'),
			'tanggal_uji_laboratorium' => $this->encode_date(Input::get('tanggal_uji_laboratorium'))
		));
	}



	/**
	 * Display the specified afp.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$afp = afp::findOrFail($id);
		return View::make('pemeriksaan.afp.show', compact('afp'));
	}



	/**
	 * Show the form for editing the specified afp.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$afp = afp::find($id);
		return View::make('pemeriksaan.afp.edit', compact('afp'));
	}



	/**
	 * Update the specified afp in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$afp = afp::findOrFail($id);

		$validator = Validator::make($data = Input::all(), afp::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$afp->update($data);

		return Redirect::route('pemeriksaan.afp.index');
	}



	/**
	 * Remove the specified afp from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		afp::destroy($id);
		return Redirect::route('pemeriksaan.afp.index');
	}



	public function postForm()
	{
		$id = Input::get('id');
		if($id==1)
		{
			return View::make('diagnosa.afp');
		}
	}



	public function detailafp($id)
	{
		$afp = DB::select("SELECT
								pasien.nik,
								pasien.tanggal_lahir,
								pasien.nama_anak,
								pasien.nama_ortu,
								CONCAT_WS(',',pasien.alamat,lokasi.kelurahan,lokasi.kecamatan,lokasi.kabupaten,lokasi.provinsi) as alamat,
								pasien.umur,
								pasien.jenis_kelamin,
								afp.no_epid,
								afp.no_epid_lama,
								afp.keadaan_akhir,
								afp.kontak,
								afp.tanggal_mulai_lumpuh,
								afp.demam_sebelum_lumpuh,
								afp.kelumpuhan_anggota_gerak_kanan,
								afp.kelumpuhan_anggota_gerak_kiri,
								afp.gangguan_raba_anggota_gerak_kanan,
								afp.gangguan_raba_anggota_gerak_kiri,
								afp.tanggal_vaksinasi_polio,
								afp.tanggal_laporan_diterima,
								afp.tanggal_pelacakan,
								afp.imunisasi_polio_sebelum_sakit,
								afp.tanggal_pengambilan_spesimen,
								afp.tanggal_periksa,
								afp.tanggal_pengambilan_spesimen1,
								afp.tanggal_pengambilan_spesimen2,
								hasil_uji_lab_afp.no_spesimen,
								hasil_uji_lab_afp.hasil_spesimen,
								hasil_uji_lab_afp.klasifikasi_akhir,
								hasil_uji_lab_afp.tanggal_uji_laboratorium,
								hasil_uji_lab_afp.id_uji_lab
							FROM
								afp,
								pasien,
								lokasi,
								hasil_uji_lab_afp,
								users
							WHERE
								pasien.id_pasien=hasil_uji_lab_afp.id_pasien
							AND
								lokasi.id_kelurahan=pasien.id_kelurahan
							AND
								afp.id_afp=hasil_uji_lab_afp.id_afp
							AND
								users.email ='".Sentry::getUser()->email."'
							AND
								afp.id_afp ='".$id."'");

		return View::make('pemeriksaan.afp.detail',compact('afp'));
	}

	//get edit Afp
	public function getEditAfp($id)
	{
		$afp = DB::table('getdetailafp')->where('afp_id_afp',$id)->get();
		return View::make('pemeriksaan.afp.edit',compact('afp'));
	}

	public function delete($id)
	{
		Afp::delete($id);
		return Redirect::to('afp');
	}

	public function getHapus($id)
	{
		$dt['deleted_by'] = Sentry::getUser()->id;
		$dt['deleted_at'] = date('Y-m-d H:i:s');
		$update = DB::table('afp')
					->where('id_afp',$id)
					->update($dt);
		$delete_notif = DB::table('notification')
					->where('global_id',$id)
					->where('type_id','afp')
					->update($dt);
		return Redirect::to('afp');
	}

	//post edit Afp
	public function updateAfp()
	{
		$afp = DB::update(" update
								pasien,afp,hasil_uji_lab_afp
						    set
								afp.no_epid='".Input::get('no_epid')."',
								afp.no_epid_lama='".Input::get('no_epid_lama')."',
								pasien.nik='".Input::get('nik')."',
								pasien.tanggal_lahir=".$this->changeDate(Input::get('tanggal_lahir')).",
								pasien.nama_anak='".Input::get('nama_anak')."',
								pasien.nama_ortu='".Input::get('nama_ortu')."',
								pasien.id_kelurahan='".Input::get('id_kelurahan')."',
								pasien.umur='".Input::get('umur')."',
								pasien.umur_bln='".Input::get('umur_bln')."',
								pasien.umur_hr='".Input::get('umur_hr')."',
								pasien.jenis_kelamin='".Input::get('jenis_kelamin')."',
								pasien.alamat='".Input::get('alamat')."',
								afp.keadaan_akhir='".Input::get('keadaan_akhir')."',
								afp.kontak='".Input::get('kontak')."',
								afp.tanggal_mulai_lumpuh=".$this->changeDate(Input::get('tanggal_mulai_lumpuh')).",
								afp.demam_sebelum_lumpuh='".Input::get('demam_sebelum_lumpuh')."',
								afp.kelumpuhan_anggota_gerak_kanan='".Input::get('kelumpuhan_anggota_gerak_kanan')."',
								afp.kelumpuhan_anggota_gerak_kiri='".Input::get('kelumpuhan_anggota_gerak_kiri')."',
								afp.gangguan_raba_anggota_gerak_kanan='".Input::get('gangguan_raba_anggota_gerak_kanan')."',
								afp.gangguan_raba_anggota_gerak_kiri='".Input::get('gangguan_raba_anggota_gerak_kiri')."',
								afp.jenis_spesimen='".Input::get('jenis_spesimen')."',
								afp.imunisasi_polio_sebelum_sakit='".Input::get('imunisasi_polio_sebelum_sakit')."',
								afp.tanggal_pengambilan_spesimen=".$this->changeDate(Input::get('tanggal_pengambilan_spesimen')).",
								afp.tanggal_periksa=".$this->changeDate(Input::get('tanggal_periksa')).",
								afp.tanggal_vaksinasi_polio=".$this->changeDate(Input::get('tanggal_vaksinasi_polio')).",
								afp.tanggal_laporan_diterima=".$this->changeDate(Input::get('tanggal_laporan_diterima')).",
								afp.tanggal_pelacakan=".$this->changeDate(Input::get('tanggal_pelacakan')).",
								afp.tanggal_pengambilan_spesimen1=".$this->changeDate(Input::get('tanggal_pengambilan_spesimen1')).",
								afp.tanggal_pengambilan_spesimen2=".$this->changeDate(Input::get('tanggal_pengambilan_spesimen2')).",
								afp.hasil_lab_isolasi_virus='".Input::get('hasil_lab_isolasi_virus')."',
								afp.hasil_lab_ITD='".Input::get('hasil_lab_ITD')."',
								afp.hasil_lab_sequencing='".Input::get('hasil_lab_sequencing')."',
								afp.klasifikasi_final='".Input::get('klasifikasi_final')."',
								hasil_uji_lab_afp.jp1='".Input::get('jp1')."',
								hasil_uji_lab_afp.isolasi_virus_jp1='".Input::get('isolasi_virus_jp1')."',
								hasil_uji_lab_afp.ket_isolasi_virus_jp1='".Input::get('ket_isolasi_virus_jp1')."',
								hasil_uji_lab_afp.itd_jp1='".Input::get('itd_jp1')."',
								hasil_uji_lab_afp.ket_itd_jp1='".Input::get('ket_itd_jp1')."',
								hasil_uji_lab_afp.sequencing_jp1='".Input::get('sequencing_jp1')."',
								hasil_uji_lab_afp.ket_sequencing_jp1='".Input::get('ket_sequencing_jp1')."',
								hasil_uji_lab_afp.jp2='".Input::get('jp2')."',
								hasil_uji_lab_afp.isolasi_virus_jp2='".Input::get('isolasi_virus_jp2')."',
								hasil_uji_lab_afp.ket_isolasi_virus_jp2='".Input::get('ket_isolasi_virus_jp2')."',
								hasil_uji_lab_afp.itd_jp2='".Input::get('itd_jp2')."',
								hasil_uji_lab_afp.ket_itd_jp2='".Input::get('ket_itd_jp2')."',
								hasil_uji_lab_afp.sequencing_jp2='".Input::get('sequencing_jp2')."',
								hasil_uji_lab_afp.ket_sequencing_jp2='".Input::get('ket_sequencing_jp2')."'
							where
								pasien.id_pasien=hasil_uji_lab_afp.id_pasien
							and
								afp.id_afp=hasil_uji_lab_afp.id_afp
							and
								hasil_uji_lab_afp.id_hasil_uji_lab_afp='".Input::get('id_hasil_uji_lab_afp')."'");

			return Redirect::to('afp');
	}

	//route daftar afp
	public function getDaftarAfp()
	{
		$pe_afp = Afp::getPeAfp();
		return View::make('pemeriksaan.afp.daftar',compact('pe_afp'));
	}

	//fungsi untuk mengambil data pasien yang akan dilakukan penyelidikan epidemologi afp
	public function getEntri_afp($id)
	{
		$pe_afp = DB::table('getdetailafp')->where('afp_id_afp',$id)->get();
		return View::make('pemeriksaan.afp.pe_afp',compact('pe_afp'));
	}

	public function getEditPeAfp($id)
	{
		$pe_afp = DB::select("SELECT a.*,
				b.id_provinsi AS sumberinformasi_id_provinsi,
				b.id_kabupaten AS sumberinformasi_id_kabupaten,
				b.kabupaten AS sumberinformasi_kabupaten
				FROM getdetailafp AS a
				LEFT JOIN located AS b ON a.pe_kabupaten=b.id_kabupaten
				WHERE a.pe_id='$id'
				GROUP BY a.pe_id");

		return View::make('pemeriksaan.afp.edit_pe_afp',compact('row'));
	}

	//fungsi untuk menyimpan data kasus penyelidikan epidemologi afp
	public function postEpidAfp()
	{
		$no_epid = Input::get('no_epid');
		$ids_afp = Input::get('id_afp');

		$cek = DB::select("select no_epid from afp,pasien,hasil_uji_lab_afp where afp.id_afp=hasil_uji_lab_afp.id_afp and pasien.id_pasien=hasil_uji_lab_afp.id_pasien and afp.no_epid='".$no_epid."'");

		if(empty($cek)){
			//simpan data personal
				DB::table('afp')->insert(array('no_epid' => Input::get('no_epid')));
				$id_pasien=DB::table('pasien')->insert(array(
							'nama_anak'     => Input::get('nama_anak'),
							'alamat'        => Input::get('alamat'),
							'tanggal_lahir' => $this->encode_date(Input::get('tanggal_lahir')),
							'umur'          => Input::get('tgl_tahun'),
							'umur_bln'      => Input::get('tgl_bln'),
							'umur_hr'       => Input::get('tgl_hr'),
							'jenis_kelamin' => Input::get('jenis_kelamin'),
							'id_kelurahan'  => Input::get('id_kelurahan')
						));

				$no_epid                            = $no_epid;
				$id_pasien                          = $id_pasien;
				$propinsi                           = Input::get('propinsi');
				$kabupaten                          = Input::get('kabupaten');
				$laporan_dari                       = Input::get('laporan_dari');
				$ket_sumber_laporan                 = Input::get('ket_sumber_laporan');
				$tanggal_mulai_sakit                = $this->changeDate(Input::get('tanggal_mulai_sakit'));
				$tanggal_mulai_lumpuh               = $this->changeDate(Input::get('tanggal_mulai_lumpuh'));
				$tanggal_meninggal                  = $this->changeDate(Input::get('tanggal_meninggal'));
				$berobat_unit_pelayanan             = Input::get('berobat_unit_pelayanan');
				$nama_unit_pelayanan                = Input::get('nama_unit_pelayanan');
				$tanggal_berobat                    = $this->changeDate(Input::get('tanggal_berobat'));
				$diagnosis                          = Input::get('diagnosis');
				$no_rekam_medis                     = Input::get('no_rekam_medis');
				$kelumpuhan                         = Input::get('kelumpuhan')[0];
				$demam_sebelum_lumpuh               = Input::get('demam_sebelum_lumpuh');
				$lumpuh_tungkai_kanan               = Input::get('lumpuh_tungkai_kanan');
				$lumpuh_tungkai_kiri                = Input::get('lumpuh_tungkai_kiri');
				$lumpuh_lengan_kanan                = Input::get('lumpuh_lengan_kanan');
				$lumpuh_lengan_kiri                 = Input::get('lumpuh_lengan_kiri');
				$raba_tungkai_kanan                 = Input::get('raba_tungkai_kanan');
				$raba_tungkai_kiri                  = Input::get('raba_tungkai_kiri');
				$raba_lengan_kanan                  = Input::get('raba_lengan_kanan');
				$raba_lengan_kiri                   = Input::get('raba_lengan_kiri');
				$catatan_lain                       = Input::get('catatan_lain');
				$sebelum_sakit_berpergian           = Input::get('sebelum_sakit_berpergian');
				$lokasi                             = Input::get('lokasi');
				$tanggal_pergi                      = $this->changeDate(Input::get('tanggal_pergi'));
				$berkunjung                         = Input::get('berkunjung');
				$imunisasi_rutin                    = Input::get('imunisasi_rutin');
				$jumlah_dosis                       = Input::get('jumlah_dosis');
				$sumber_informasi                   = Input::get('sumber_informasi');
				$bias_polio                         = Input::get('bias_polio');
				$jumlah_dosis_bias_polio            = Input::get('jumlah_dosis_bias_polio');
				$sumber_informasi_bias_polio        = Input::get('sumber_informasi_bias_polio');
				$tanggal_imunisasi_polio            = $this->changeDate(Input::get('tanggal_imunisasi_polio'));
				$tanggal_ambil_spesimen_I           = $this->changeDate(Input::get('tanggal_ambil_spesimen_I'));
				$tanggal_ambil_spesimen_II          = $this->changeDate(Input::get('tanggal_ambil_spesimen_II'));
				$tanggal_kirim_spesimen_I           = $this->changeDate(Input::get('tanggal_kirim_spesimen_I'));
				$tanggal_kirim_spesimen_II          = $this->changeDate(Input::get('tanggal_kirim_spesimen_II'));
				$tanggal_kirim_spesimen_I_propinsi  = $this->changeDate(Input::get('tanggal_kirim_spesimen_I_propinsi'));
				$tanggal_kirim_spesimen_II_propinsi = $this->changeDate(Input::get('tanggal_kirim_spesimen_II_propinsi'));
				$catatan_tidak_diambil_spesimen     = Input::get('catatan_tidak_diambil_spesimen');
				$nama_petugas                       = Input::get('nama_petugas');
				$hasil_diagnosis                    = Input::get('hasil_diagnosis');
				$nama_DSA                           = Input::get('nama_DSA');

				$id_pe_afp = Afp::epid_afp($id_pasien,$propinsi,$kabupaten,$laporan_dari,$ket_sumber_laporan,$tanggal_mulai_sakit,$tanggal_mulai_lumpuh,$tanggal_meninggal,$berobat_unit_pelayanan,$nama_unit_pelayanan,$tanggal_berobat,$diagnosis,$no_rekam_medis,$kelumpuhan,$no_epid,$demam_sebelum_lumpuh,$lumpuh_tungkai_kanan,$lumpuh_tungkai_kiri,$lumpuh_lengan_kanan,$lumpuh_lengan_kiri,$raba_tungkai_kanan,$raba_tungkai_kiri,$raba_lengan_kanan,$raba_lengan_kiri,$catatan_lain,$sebelum_sakit_berpergian,$lokasi,$tanggal_pergi,$berkunjung,$imunisasi_rutin,$jumlah_dosis,$sumber_informasi,$bias_polio,$jumlah_dosis_bias_polio,$sumber_informasi_bias_polio,$tanggal_imunisasi_polio,$tanggal_ambil_spesimen_I,$tanggal_ambil_spesimen_II,$tanggal_kirim_spesimen_I,$tanggal_kirim_spesimen_II,$tanggal_kirim_spesimen_I_propinsi,$tanggal_kirim_spesimen_II_propinsi,$catatan_tidak_diambil_spesimen,$nama_petugas,$hasil_diagnosis,$nama_DSA);

				$id_afp = $_POST['id_afp'];
				$up = [
					'id_pe_afp'=>$id_pe_afp
				];
				//update id_pe
				$id_pe = DB::table('afp')
							->where('id_afp',$id_afp)
							->update($up);
				DB::table('hasil_uji_lab_afp')->insert(array(
					'id_afp'    => $id_afp,
					'id_pasien' => $id_pasien
				));

				return Redirect::to('afp');
		}
		else
		{
			$id_pasien                          = Input::get('id_pasien');
			$propinsi                           = Input::get('propinsi');
			$kabupaten                          = Input::get('kabupaten');
			$laporan_dari                       = Input::get('laporan_dari');
			$ket_sumber_laporan                 = Input::get('ket_sumber_laporan');
			$tanggal_mulai_sakit                = $this->changeDate(Input::get('tanggal_mulai_sakit'));
			$tanggal_mulai_lumpuh               = $this->changeDate(Input::get('tanggal_mulai_lumpuh'));
			$tanggal_meninggal                  = $this->changeDate(Input::get('tanggal_meninggal'));
			$berobat_unit_pelayanan             = Input::get('berobat_unit_pelayanan');
			$nama_unit_pelayanan                = Input::get('nama_unit_pelayanan');
			$tanggal_berobat                    = (Input::get('tanggal_berobat'));
			$diagnosis                          = Input::get('diagnosis');
			$no_rekam_medis                     = Input::get('no_rekam_medis');
			$kelumpuhan                         = Input::get('kelumpuhan')[0];
			$no_epid                            = Input::get('no_epid');
			$demam_sebelum_lumpuh               = Input::get('demam_sebelum_lumpuh');
			$lumpuh_tungkai_kanan               = Input::get('lumpuh_tungkai_kanan');
			$lumpuh_tungkai_kiri                = Input::get('lumpuh_tungkai_kiri');
			$lumpuh_lengan_kanan                = Input::get('lumpuh_lengan_kanan');
			$lumpuh_lengan_kiri                 = Input::get('lumpuh_lengan_kiri');
			$raba_tungkai_kanan                 = Input::get('raba_tungkai_kanan');
			$raba_tungkai_kiri                  = Input::get('raba_tungkai_kiri');
			$raba_lengan_kanan                  = Input::get('raba_lengan_kanan');
			$raba_lengan_kiri                   = Input::get('raba_lengan_kiri');
			$catatan_lain                       = Input::get('catatan_lain');
			$sebelum_sakit_berpergian           = Input::get('sebelum_sakit_berpergian');
			$lokasi                             = Input::get('lokasi');
			$tanggal_pergi                      = $this->changeDate(Input::get('tanggal_pergi'));
			$berkunjung                         = Input::get('berkunjung');
			$imunisasi_rutin                    = Input::get('imunisasi_rutin');
			$jumlah_dosis                       = Input::get('jumlah_dosis');
			$sumber_informasi                   = Input::get('sumber_informasi');
			$bias_polio                         = Input::get('bias_polio');
			$jumlah_dosis_bias_polio            = Input::get('jumlah_dosis_bias_polio');
			$sumber_informasi_bias_polio        = Input::get('sumber_informasi_bias_polio');
			$tanggal_imunisasi_polio            = $this->changeDate(Input::get('tanggal_imunisasi_polio'));
			$tanggal_ambil_spesimen_I           = $this->changeDate(Input::get('tanggal_ambil_spesimen_I'));
			$tanggal_ambil_spesimen_II          = $this->changeDate(Input::get('tanggal_ambil_spesimen_II'));
			$tanggal_kirim_spesimen_I           = $this->changeDate(Input::get('tanggal_kirim_spesimen_I'));
			$tanggal_kirim_spesimen_II          = $this->changeDate(Input::get('tanggal_kirim_spesimen_II'));
			$tanggal_kirim_spesimen_I_propinsi  = $this->changeDate(Input::get('tanggal_kirim_spesimen_I_propinsi'));
			$tanggal_kirim_spesimen_II_propinsi = $this->changeDate(Input::get('tanggal_kirim_spesimen_II_propinsi'));
			$catatan_tidak_diambil_spesimen     = Input::get('catatan_tidak_diambil_spesimen');
			$nama_petugas                       = Input::get('nama_petugas');
			$hasil_diagnosis                    = Input::get('hasil_diagnosis');
			$nama_DSA                           = Input::get('nama_DSA');

			$id_pe_afp = Afp::epid_afp($id_pasien,$propinsi,$kabupaten,$laporan_dari,$ket_sumber_laporan,$tanggal_mulai_sakit,$tanggal_mulai_lumpuh,$tanggal_meninggal,$berobat_unit_pelayanan,$nama_unit_pelayanan,$tanggal_berobat,$diagnosis,$no_rekam_medis,$kelumpuhan,$no_epid,$demam_sebelum_lumpuh,$lumpuh_tungkai_kanan,$lumpuh_tungkai_kiri,$lumpuh_lengan_kanan,$lumpuh_lengan_kiri,$raba_tungkai_kanan,$raba_tungkai_kiri,$raba_lengan_kanan,$raba_lengan_kiri,$catatan_lain,$sebelum_sakit_berpergian,$lokasi,$tanggal_pergi,$berkunjung,$imunisasi_rutin,$jumlah_dosis,$sumber_informasi,$bias_polio,$jumlah_dosis_bias_polio,$sumber_informasi_bias_polio,$tanggal_imunisasi_polio,$tanggal_ambil_spesimen_I,$tanggal_ambil_spesimen_II,$tanggal_kirim_spesimen_I,$tanggal_kirim_spesimen_II,$tanggal_kirim_spesimen_I_propinsi,$tanggal_kirim_spesimen_II_propinsi,$catatan_tidak_diambil_spesimen,$nama_petugas,$hasil_diagnosis,$nama_DSA);

			$up = [
				'id_pe_afp'=>$id_pe_afp
			];
			//update id_pe
			$id_pe = DB::table('afp')
						->where('id_afp',$ids_afp)
						->update($up);

			return Redirect::to('afp');
		}
	}

	public function getDetailPeAfp($id)
	{
		//CONCAT_WS(',',pasien.alamat,lokasi.kelurahan,lokasi.kecamatan,lokasi.kabupaten,lokasi.provinsi) as alamat,
		$data = DB::select("SELECT
					b.id_pasien,
					b.nik,
					b.nama_ortu,
					b.nama_anak,
					concat_ws(', ',b.alamat, c.kelurahan, c.kecamatan,c.kabupaten,c.provinsi) AS alamat,
					b.tanggal_lahir,
					b.umur,
					b.umur_bln,
					b.umur_hr,
					b.jenis_kelamin,
					b.id_kelurahan,
					a.id,
					a.propinsi,
					a.kabupaten,
					a.ket_sumber_laporan,
					a.laporan_dari,
					a.tanggal_mulai_sakit,
					a.tanggal_mulai_lumpuh,
					a.tanggal_meninggal,
					a.berobat_unit_pelayanan,
					a.nama_unit_pelayanan,
					a.tanggal_berobat,
					a.diagnosis,
					a.no_rekam_medis,
					a.kelumpuhan,
					a.no_epid,
					a.demam_sebelum_lumpuh,
					a.lumpuh_tungkai_kanan,
					a.lumpuh_tungkai_kiri,
					a.lumpuh_lengan_kiri,
					a.lumpuh_lengan_kanan,
					a.raba_tungkai_kanan,
					a.raba_tungkai_kiri,
					a.raba_lengan_kanan,
					a.catatan_lain,
					a.raba_lengan_kiri,
					a.sebelum_sakit_berpergian,
					a.lokasi,
					a.tanggal_pergi,
					a.berkunjung,
					a.imunisasi_rutin,
					a.jumlah_dosis,
					a.sumber_informasi,
					a.bias_polio,
					a.jumlah_dosis_bias_polio,
					a.sumber_informasi_bias_polio,
					a.tanggal_imunisasi_polio,
					a.tanggal_ambil_spesimen_I,
					a.tanggal_ambil_spesimen_II,
					a.tanggal_kirim_spesimen_I,
					a.tanggal_kirim_spesimen_II,
					a.tanggal_kirim_spesimen_I_propinsi,
					a.tanggal_kirim_spesimen_II_propinsi,
					a.catatan_tidak_diambil_spesimen,
					a.nama_petugas,
					a.hasil_diagnosis,
					a.nama_DSA
					FROM
					pe_afp AS a
					JOIN pasien AS b ON a.id_pasien=b.id_pasien
					JOIN located AS c ON b.id_kelurahan=c.id_kelurahan
					WHERE a.id ='".$id."'");
		return View::make('pemeriksaan.afp.detail_pe_afp',compact('data'));
	}

	public function postUpdatePeAfp()
	{
		$data = DB::update("update
								pe_afp
							set
								pe_afp.propinsi='".Input::get('propinsi')."',
								pe_afp.kabupaten='".Input::get('kabupaten')."',
								pe_afp.ket_sumber_laporan='".Input::get('ket_sumber_laporan')."',
								pe_afp.laporan_dari='".Input::get('laporan_dari')."',
								pe_afp.tanggal_mulai_sakit=".$this->changeDate(Input::get('tanggal_mulai_sakit')).",
								pe_afp.tanggal_mulai_lumpuh=".$this->changeDate(Input::get('tanggal_mulai_lumpuh')).",
								pe_afp.tanggal_meninggal=".$this->changeDate(Input::get('tanggal_meninggal')).",
								pe_afp.berobat_unit_pelayanan=".Input::get('berobat_unit_pelayanan').",
								pe_afp.nama_unit_pelayanan='".Input::get('nama_unit_pelayanan')."',
								pe_afp.tanggal_berobat=".$this->changeDate(Input::get('tanggal_berobat')).",
								pe_afp.diagnosis='".Input::get('diagnosis')."',
								pe_afp.no_rekam_medis='".Input::get('no_rekam_medis')."',
								pe_afp.kelumpuhan='".Input::get('kelumpuhan')."',
								pe_afp.no_epid='".Input::get('no_epid')."',
								pe_afp.demam_sebelum_lumpuh='".Input::get('demam_sebelum_lumpuh')."',
								pe_afp.lumpuh_tungkai_kanan='".Input::get('lumpuh_tungkai_kanan')."',
								pe_afp.lumpuh_tungkai_kiri='".Input::get('lumpuh_tungkai_kiri')."',
								pe_afp.lumpuh_lengan_kanan='".Input::get('lumpuh_lengan_kanan')."',
								pe_afp.lumpuh_lengan_kiri='".Input::get('lumpuh_lengan_kiri')."',
								pe_afp.raba_tungkai_kanan='".Input::get('raba_tungkai_kanan')."',
								pe_afp.raba_tungkai_kiri='".Input::get('raba_tungkai_kiri')."',
								pe_afp.raba_lengan_kanan='".Input::get('raba_lengan_kiri')."',
								pe_afp.raba_lengan_kiri='".Input::get('raba_lengan_kiri')."',
								pe_afp.catatan_lain='".Input::get('catatan_lain')."',
								pe_afp.sebelum_sakit_berpergian='".Input::get('sebelum_sakit_berpergian')."',
								pe_afp.lokasi='".Input::get('lokasi')."',
								pe_afp.tanggal_pergi=".$this->changeDate(Input::get('tanggal_pergi')).",
								pe_afp.berkunjung='".Input::get('berkunjung')."',
								pe_afp.imunisasi_rutin='".Input::get('imunisasi_rutin')."',
								pe_afp.jumlah_dosis='".Input::get('jumlah_dosis')."',
								pe_afp.sumber_informasi='".Input::get('sumber_informasi')."',
								pe_afp.bias_polio='".Input::get('bias_polio')."',
								pe_afp.jumlah_dosis_bias_polio='".Input::get('jumlah_dosis_bias_polio')."',
								pe_afp.sumber_informasi_bias_polio='".Input::get('sumber_informasi_bias_polio')."',
								pe_afp.tanggal_imunisasi_polio=".$this->changeDate(Input::get('tanggal_imunisasi_polio')).",
								pe_afp.tanggal_ambil_spesimen_I=".$this->changeDate(Input::get('tanggal_ambil_spesimen_I')).",
								pe_afp.tanggal_ambil_spesimen_II=".$this->changeDate(Input::get('tanggal_ambil_spesimen_II')).",
								pe_afp.tanggal_kirim_spesimen_I=".$this->changeDate(Input::get('tanggal_kirim_spesimen_I')).",
								pe_afp.tanggal_kirim_spesimen_II=".$this->changeDate(Input::get('tanggal_kirim_spesimen_II')).",
								pe_afp.tanggal_kirim_spesimen_I_propinsi=".$this->changeDate(Input::get('tanggal_kirim_spesimen_I_propinsi')).",
								pe_afp.tanggal_kirim_spesimen_II_propinsi=".$this->changeDate(Input::get('tanggal_kirim_spesimen_II_propinsi')).",
								pe_afp.catatan_tidak_diambil_spesimen='".Input::get('catatan_tidak_diambil_spesimen')."',
								pe_afp.nama_petugas='".Input::get('nama_petugas')."',
								pe_afp.hasil_diagnosis='".Input::get('hasil_diagnosis')."',
								pe_afp.nama_DSA='".Input::get('nama_DSA')."'
							where
								pe_afp.id='".Input::get('id')."'");

		return Redirect::to('afp');
	}



	//fungsi hapus data PE afp
	public function getHapusPeAfp($id)
	{
		 try {
			DB::beginTransaction();
			DB::delete("delete from pe_afp where pe_afp.id='".$id."'");
			DB::commit();
		} catch (Exception $e) {
			DB::rollback();
		}

		return Redirect::to('afp');
	}



	//fungsi ambil no epid untuk kasus afp
	function getEpid(){
		$id_kelurahan = Input::get('id_kelurahan');
		$tglsakit = (!empty(Input::get('date'))) ? date('Y-m-d', strtotime(Input::get('date'))) : '';
		$thn = (!empty($tglsakit)) ? date('Y', strtotime($tglsakit)) : '';
		$thnsakit = (!empty($tglsakit)) ? date('y', strtotime($tglsakit)) : '';
		$cek  = DB::select("SELECT
			max(a.no_epid) AS no_epid
			FROM
			afp a
			JOIN hasil_uji_lab_afp b ON a.id_afp = b.id_afp
			JOIN pasien c ON b.id_pasien = c.id_pasien
			WHERE
			YEAR(a.tanggal_mulai_lumpuh) = ".$thn."
			AND c.id_kelurahan = '".$id_kelurahan."'
			ORDER BY a.no_epid DESC
			LIMIT 1
			");

		$caracter = 'A';
		$code     = $id_kelurahan.$thnsakit;
		if (isset($cek[0]->no_epid)) {
			$nepid = substr($cek[0]->no_epid, -3);
			$epid   = sprintf("%03s", $nepid+1);
		}else{
			$epid     = '001';
		}
		$no_epid = $caracter.$code.$epid;

		return $no_epid;
	}

	/*public function getEpid()
	{
		echo "<pre>";print_r($_GET);echo "</pre>";die();
		$id_kel = Input::get('id_kelurahan');
		$tglsakit = $this->changeDate(Input::get('date'));
		$thn = substr($tglsakit,2,2);
		$data2 = DB::select("select
			max(no_epid) as no_epid
			from campak,
			pasien,
			hasil_uji_lab_campak
			where pasien.id_pasien=hasil_uji_lab_campak.id_pasien
			and campak.id_campak=hasil_uji_lab_campak.id_campak 
			and id_kelurahan='".$id_kel."' order by campak.id_campak limit 0,1");
        $cek = DB::select("select RIGHT(no_epid,3) from
        	campak,pasien,hasil_uji_lab_campak
        	where MID(no_epid,12,2) ='".$thn."' and
        	pasien.id_pasien=hasil_uji_lab_campak.id_pasien
        	and campak.id_campak=hasil_uji_lab_campak.id_campak and pasien.id_kelurahan='".$id_kel."' order by campak.id_campak limit 0,1");

        if(empty($cek))
        {
            $code     = $id_kel.$thn;
			$caracter = 'A';
			$epid     = '001';
			$no_epid     = $caracter.$code.$epid;
        }
        else
        {
        	$kode   = substr($data2[0]->no_epid,0,13);
			$noUrut = (int) substr($data2[0]->no_epid,13,3);
			$noUrut++;
			$no_epid   = $kode.sprintf("%03s", $noUrut);
        }

        return $no_epid;
	}*/



	public function getAll()
	{
		return Afp::has('patients')->with('patients')->get();
	}



	public function exportExcel($filetype, $dt=null)
	{
		$ds = json_decode($dt);
		$penyakit  = Session::get('penyakit');
		$excel_tmp = array(
			array(
				'No',
				'No Epid Kasus / KLB',
				'Nama Anak',
				'Nama Orangtua',
				'Alamat',
				'Puskesmas',
				'Kecamatan',
				'Kabupaten/Kota',
				'Provinsi',
				'Umur',
				'',
				'Jenis Kelamin',
				'Imunisasi Polio Sebelum Sakit',
				'',
				'',
				'Tanggal Imunisasi Polio Terakhir',
				'Demam Sebelum Lumpuh (Bukan Karena Ruda Paksa)',
				'Tanggal Mulai Lumpuh (Bukan Karena Ruda Paksa)',
				'Kelumpukan Anggota Gerak',
				'',
				'Gangguan Raba Anggota Gerak',
				'',
				'Tanggal Laporan Diterima',
				'Tanggal Pelacakan',
				'Tanggal Pengambilan Spesimen (Stool)',
				'',
				'Hasil Lab',
				'',
				'',
				'',
				'',
				'',
				'Keadaan Akhir',
				'Kontak',
				'Klasifikasi Final (Polio / Bukan Polio / Compatible)',

				// Tambahan
				'NIK',
				'Tanggal Lahir',
				// 'No Epid Lama'

			),
			array(
				'1',
				'2',
				'3',
				'4',
				'5',
				'6',
				'7',
				'8',
				'9',
				'Tahun',
				'Bulan',
				'12',
				'berapa kali',
				'tidak',
				'tidak tahu',
				'16',
				'17',
				'18',
				'Kanan',
				'Kiri',
				'Kanan',
				'Kiri',
				'23',
				'24',
				'Spesimen 1',
				'Spesimen 2',
				'Spesimen 1',
				'28',
				'29',
				'Spesimen 2',
				'31',
				'32',
				'33',
				'34',
				'35'
			),
			array(
				'b1',
				'b2',
				'b3',
				'b4',
				'b5',
				'b6',
				'b7',
				'b8',
				'b9',
				'b10',
				'b11',
				'b12',
				'b13',
				'b14',
				'b15',
				'b16',
				'b17',
				'b18',
				'b19',
				'b20',
				'b21',
				'b22',
				'b23',
				'b24',
				'b25',
				'b26',
				'Isolasi Virus',
				'ITD',
				'Sequencing',
				'Isolasi Virus',
				'ITD',
				'Sequencing',
				'b33',
				'b34',
				'b35'
			),
			array('')
		);


		foreach ($ds as $k => $v) {
			$post[$v->name] = $v->value;
		}
		$between = $wilayah = '';
		$unit			= (empty($post['unit'])?'':$post['unit']);
		$day_start      = (empty($post['day_start'])?'':$post['day_start']);
		$month_start    = (empty($post['month_start'])?'':$post['month_start']);
		$year_start     = (empty($post['year_start'])?'':$post['year_start']);
		$day_end        = (empty($post['day_end'])?'':$post['day_end']);
		$month_end      = (empty($post['month_end'])?'':$post['month_end']);
		$year_end       = (empty($post['year_end'])?'':$post['year_end']);
		$province_id    = (empty($post['province_id'])?'':$post['province_id']);
		$district_id    = (empty($post['district_id'])?'':$post['district_id']);
		$sub_district_id = (empty($post['sub_district_id'])?'':$post['sub_district_id']);
		$rs_id			 = (empty($post['rs_id'])?'':$post['rs_id']);
		$puskesmas_id	 = (empty($post['puskesmas_id'])?'':$post['puskesmas_id']);

			switch($unit) {
				case "all" :
					$between = " ";
				break;
				case "year" :
					$start   = $post['year_start'];
					$end     = $post['year_end'];
					$between = " AND YEAR(c.tanggal_periksa) BETWEEN '".$start."' AND '".$end."' ";
				break;
				case "month" :
					$start   = $post['year_start'] . '-' . $post['month_start'] . '-1';
					$end     = $post['year_end'] . '-' . $post['month_end'] . '-' . date('t', mktime(0,0,0,$post['month_end'], 1, $post['year_end']));
					$between = " AND DATE(c.tanggal_periksa) BETWEEN STR_TO_DATE('".$start."', '%Y-%c-%e') AND STR_TO_DATE('".$end."', '%Y-%c-%e') ";
				break;
				default :
					$start   = $post['year_start'] . '-' . $post['month_start'] . '-' . $post['day_start'];
					$end     = $post['year_end'] . '-' . $post['month_end'] . '-' . $post['day_end'];
					$between = " AND DATE(c.tanggal_periksa) BETWEEN STR_TO_DATE('".$start."', '%Y-%c-%e') AND STR_TO_DATE('".$end."', '%Y-%c-%e') ";
				break;
			}

			if($puskesmas_id){
				$wilayah = "AND puskesmas_code_faskes='".$puskesmas_id."'";
			} else if($sub_district_id){
				$wilayah = "AND e.id_kecamatan='".$sub_district_id."'";
			} else if($rs_id) {
				$wilayah = "AND j.kode_faskes='".$rs_id."' ";
			} else if($district_id) {
				$wilayah = "AND f.id_kabupaten='".$district_id."' ";
			} else if($province_id) {
				$wilayah = "AND g.id_provinsi='".$province_id."' ";
			} 

			$q = "
				SELECT
					c.no_epid,
					b.nama_anak,
					b.nama_ortu,
					b.alamat,
					h.puskesmas_name,
					e.kecamatan,
					f.kabupaten,
					g.provinsi,
					DATE_FORMAT(c.tanggal_mulai_lumpuh,'%d-%m-%Y') as tanggal_mulai_lumpuh,
					DATE_FORMAT(b.tanggal_lahir,'%d-%m-%Y') as tanggal_lahir,
					b.umur,
					b.umur_bln,

					#Tambahan
					b.nik,

					#JENIS KELAMIN
					IF(b.jenis_kelamin='0','Laki-laki',
					IF(b.jenis_kelamin='1','Perempuan',
					IF(b.jenis_kelamin='3','Tidak diketahui',
					IF(b.jenis_kelamin='4','Lainnya',
					'Tidak diisi')))) AS jenis_kelamin,

					IF(c.imunisasi_polio_sebelum_sakit='1x','1 Kali',
					IF(c.imunisasi_polio_sebelum_sakit='2x','2 Kali',
					IF(c.imunisasi_polio_sebelum_sakit='3x','3 Kali',
					IF(c.imunisasi_polio_sebelum_sakit='4x','4 Kali',
					IF(c.imunisasi_polio_sebelum_sakit='5x','5 Kali',
					IF(c.imunisasi_polio_sebelum_sakit='6x','6 Kali',
					'')))))) AS imunisasi_polio_sebelum_sakit,

					IF(c.imunisasi_polio_sebelum_sakit='tidak','Tidak',
					'') AS imunisasi_polio_sebelum_sakit_tidak,

					'????' AS imunisasi_polio_sebelum_sakit_tidak_tahu,

					DATE_FORMAT(c.tanggal_vaksinasi_polio,'%d-%m-%Y') as tanggal_vaksinasi_polio,
					'?????' AS demam_sebelum_lumpuh_bukan_karena_ruda_paksa,
					'?????' AS tanggal_demam_sebelum_lumpuh_bukan_karena_ruda_paksa,

					#KELUMPUHAN ANGGOTA GERAK KANAN
					IF(c.kelumpuhan_anggota_gerak_kanan='0','1 Anggota Gerak',
					IF(c.kelumpuhan_anggota_gerak_kanan='1','2 Anggota Gerak',
					IF(c.kelumpuhan_anggota_gerak_kanan='2','Tidak',
					''))) AS kelumpuhan_anggota_gerak_kanan,

					#KELUMPUHAN ANGGOTA GERAK KIRI
					IF(c.kelumpuhan_anggota_gerak_kiri='0','1 Anggota Gerak',
					IF(c.kelumpuhan_anggota_gerak_kiri='1','2 Anggota Gerak',
					IF(c.kelumpuhan_anggota_gerak_kiri='2','Tidak',
					''))) AS kelumpuhan_anggota_gerak_kiri,

					#GANGGUAN RABA ANGGOTA GERAK KANAN
					IF(c.gangguan_raba_anggota_gerak_kanan='0','1 Anggota Gerak',
					IF(c.gangguan_raba_anggota_gerak_kanan='1','2 Anggota Gerak',
					IF(c.gangguan_raba_anggota_gerak_kanan='2','Tidak',
					''))) AS gangguan_raba_anggota_gerak_kanan,

					#KELUMPUHAN ANGGOTA GERAK KIRI
					IF(c.gangguan_raba_anggota_gerak_kiri='0','1 Anggota Gerak',
					IF(c.gangguan_raba_anggota_gerak_kiri='1','2 Anggota Gerak',
					IF(c.gangguan_raba_anggota_gerak_kiri='2','Tidak',
					''))) AS gangguan_raba_anggota_gerak_kiri,

					DATE_FORMAT(c.tanggal_laporan_diterima,'%d-%m-%Y') as tanggal_laporan_diterima,
					DATE_FORMAT(c.tanggal_pelacakan,'%d-%m-%Y') as tanggal_pelacakan,
					DATE_FORMAT(c.tanggal_pengambilan_spesimen,'%d-%m-%Y') as tanggal_pengambilan_spesimen_1,

					c.hasil_lab_isolasi_virus AS hasil_lab_isolasi_virus_1,
					c.hasil_lab_ITD AS hasil_lab_ITD_1,
					c.hasil_lab_sequencing AS hasil_lab_sequencing_1,

					'?????????' AS tanggal_pengambilan_spesimen_2,
					'?????????' AS hasil_lab_isolasi_virus_2,
					'?????????' AS hasil_lab_ITD_2,
					'?????????' AS hasil_lab_sequencing_2,

					#KEADAAN AKHIR
					IF(c.keadaan_akhir='0','Hidup',
					IF(c.keadaan_akhir='1','Mati',
					''
					)) AS keadaan_akhir,

					#KONTAK
					IF(c.kontak='Y','Ya',
					IF(c.kontak='T','Tidak',
					'')) AS kontak,

					#KLASIFIKASI FINAL
					IF(c.klasifikasi_final='0','Polio',
					IF(c.klasifikasi_final='1','Bukan Polio',
					'')) AS klasifikasi_final

				FROM hasil_uji_lab_afp a
				JOIN pasien b ON b.id_pasien=a.id_pasien
				JOIN afp c ON c.id_afp=a.id_afp
				LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
				LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
				LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
				LEFT JOIN provinsi g ON g.id_provinsi=f.id_provinsi
				LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
				LEFT JOIN rumahsakit2 j  ON j.id=c.id_tempat_periksa
				WHERE
				1=1
				".$between."
				".$wilayah."
			";

			$data=DB::select($q);

			$ke_bawah = 3;
			for($i=0;$i<count($data);$i++){
				$excel_tmp[$ke_bawah+$i][0]  = 1+$i;
				$excel_tmp[$ke_bawah+$i][1]  = $data[$i]->no_epid;
				$excel_tmp[$ke_bawah+$i][2]  = $data[$i]->nama_anak;
				$excel_tmp[$ke_bawah+$i][3]  = $data[$i]->nama_ortu;
				$excel_tmp[$ke_bawah+$i][4]  = $data[$i]->alamat;
				$excel_tmp[$ke_bawah+$i][5]  = $data[$i]->puskesmas_name;
				$excel_tmp[$ke_bawah+$i][6]  = $data[$i]->kecamatan;
				$excel_tmp[$ke_bawah+$i][7]  = $data[$i]->kabupaten;
				$excel_tmp[$ke_bawah+$i][8]  = $data[$i]->provinsi;
				$excel_tmp[$ke_bawah+$i][9]  = $data[$i]->umur;
				$excel_tmp[$ke_bawah+$i][10] = $data[$i]->umur_bln;
				$excel_tmp[$ke_bawah+$i][11] = $data[$i]->jenis_kelamin;
				$excel_tmp[$ke_bawah+$i][12] = $data[$i]->imunisasi_polio_sebelum_sakit;
				$excel_tmp[$ke_bawah+$i][13] = $data[$i]->imunisasi_polio_sebelum_sakit_tidak;
				$excel_tmp[$ke_bawah+$i][14] = $data[$i]->imunisasi_polio_sebelum_sakit_tidak_tahu;
				$excel_tmp[$ke_bawah+$i][15] = $data[$i]->tanggal_vaksinasi_polio;
				$excel_tmp[$ke_bawah+$i][16] = $data[$i]->demam_sebelum_lumpuh_bukan_karena_ruda_paksa;
				$excel_tmp[$ke_bawah+$i][17] = $data[$i]->tanggal_demam_sebelum_lumpuh_bukan_karena_ruda_paksa;
				$excel_tmp[$ke_bawah+$i][18] = $data[$i]->kelumpuhan_anggota_gerak_kanan;
				$excel_tmp[$ke_bawah+$i][19] = $data[$i]->kelumpuhan_anggota_gerak_kiri;
				$excel_tmp[$ke_bawah+$i][20] = $data[$i]->gangguan_raba_anggota_gerak_kanan;
				$excel_tmp[$ke_bawah+$i][21] = $data[$i]->gangguan_raba_anggota_gerak_kiri;
				$excel_tmp[$ke_bawah+$i][22] = $data[$i]->tanggal_laporan_diterima;
				$excel_tmp[$ke_bawah+$i][23] = $data[$i]->tanggal_pelacakan;
				$excel_tmp[$ke_bawah+$i][24] = $data[$i]->tanggal_pengambilan_spesimen_1;
				$excel_tmp[$ke_bawah+$i][25] = $data[$i]->tanggal_pengambilan_spesimen_2;
				$excel_tmp[$ke_bawah+$i][26] = $data[$i]->hasil_lab_isolasi_virus_1;
				$excel_tmp[$ke_bawah+$i][27] = $data[$i]->hasil_lab_ITD_1;
				$excel_tmp[$ke_bawah+$i][28] = $data[$i]->hasil_lab_sequencing_1;
				$excel_tmp[$ke_bawah+$i][29] = $data[$i]->hasil_lab_isolasi_virus_2;
				$excel_tmp[$ke_bawah+$i][30] = $data[$i]->hasil_lab_ITD_2;
				$excel_tmp[$ke_bawah+$i][31] = $data[$i]->hasil_lab_sequencing_2;
				$excel_tmp[$ke_bawah+$i][32] = $data[$i]->keadaan_akhir;
				$excel_tmp[$ke_bawah+$i][33] = $data[$i]->kontak;
				$excel_tmp[$ke_bawah+$i][34] = $data[$i]->klasifikasi_final;
				$excel_tmp[$ke_bawah+$i][35] = $data[$i]->nik;
				$excel_tmp[$ke_bawah+$i][36] = $data[$i]->tanggal_lahir;

				// ditambahin yang belum ada / data selain C1

			}

		Excel::create('FORM_C1_AFP_'.date('Y_m_d_h_i_s'), function($excel) use($excel_tmp) {
			$excel->sheet('Sheetname', function($sheet) use($excel_tmp) {
			$sheet->fromArray($excel_tmp);
			//$sheet->mergeCells('A1:AF1');

			$sheet->mergeCells('A2:A4');
			$sheet->mergeCells('B2:B4');
			$sheet->mergeCells('C2:C4');
			$sheet->mergeCells('D2:D4');
			$sheet->mergeCells('E2:E4');
			$sheet->mergeCells('F2:F4');
			$sheet->mergeCells('G2:G4');
			$sheet->mergeCells('H2:H4');
			$sheet->mergeCells('I2:I4');
			$sheet->mergeCells('J3:J4');
			$sheet->mergeCells('K3:K4');
			$sheet->mergeCells('L2:L4');
			$sheet->mergeCells('M3:M4');
			$sheet->mergeCells('N3:N4');
			$sheet->mergeCells('O3:O4');
			$sheet->mergeCells('P2:P4');
			$sheet->mergeCells('Q2:Q4');
			$sheet->mergeCells('R2:R4');
			$sheet->mergeCells('S3:S4');
			$sheet->mergeCells('T3:T4');
			$sheet->mergeCells('U3:U4');
			$sheet->mergeCells('V3:V4');
			$sheet->mergeCells('W2:W4');
			$sheet->mergeCells('X2:X4');
			$sheet->mergeCells('Y3:Y4');
			$sheet->mergeCells('Z3:Z4');
			//$sheet->mergeCells('AA3:AA4');
			//$sheet->mergeCells('AB3:AB4');
			//$sheet->mergeCells('AC3:AC4');
			//$sheet->mergeCells('AD3:AD4');
			//$sheet->mergeCells('AE3:AE4');
			//$sheet->mergeCells('AF3:AF4');
			$sheet->mergeCells('AG2:AG4');
			$sheet->mergeCells('AH2:AH4');
			$sheet->mergeCells('AI2:AI4');

			// Tambahan
			$sheet->mergeCells('AJ2:AJ4');
			$sheet->mergeCells('AK2:AK4');

			$sheet->mergeCells('J2:K2');
			$sheet->mergeCells('M2:O2');
			$sheet->mergeCells('S2:T2');
			$sheet->mergeCells('U2:V2');
			$sheet->mergeCells('Y2:Z2');
			$sheet->mergeCells('AA2:AF2');
			$sheet->mergeCells('AA3:AC3');
			$sheet->mergeCells('AD3:AF3');

			$sheet->cells('A2:AK4', function($cells) {
				$cells->setAlignment('center');
				$cells->setValignment('center');
				$cells->setBackground('#FFFFAA');
				$cells->setFontSize(13);
				$cells->setFontWeight('bold');
			});

			//$sheet->cells('N5:AO1000', function($cells) {
			//	$cells->setAlignment('center');
			//	$cells->setValignment('center');
			//});

			$sheet->setWidth('A', 5);
			$sheet->setWidth('B', 25);
			$sheet->setWidth('C', 22);
			$sheet->setWidth('D', 22);
			$sheet->setWidth('E', 50);
			$sheet->setWidth('F', 21);
			$sheet->setWidth('G', 20);
			$sheet->setWidth('H', 20);
			$sheet->setWidth('I', 30);
			$sheet->setWidth('J', 15);
			$sheet->setWidth('K', 15);
			$sheet->setWidth('L', 26);
			$sheet->setWidth('M', 20);
			$sheet->setWidth('N', 17);
			$sheet->setWidth('O', 17);
			$sheet->setWidth('P', 45);
			$sheet->setWidth('Q', 74);
			$sheet->setWidth('R', 71);
			$sheet->setWidth('S', 20);
			$sheet->setWidth('T', 20);
			$sheet->setWidth('U', 20);
			$sheet->setWidth('V', 20);
			$sheet->setWidth('W', 36);
			$sheet->setWidth('X', 30);
			$sheet->setWidth('Y', 27);
			$sheet->setWidth('Z', 27);
			$sheet->setWidth('AA', 25);
			$sheet->setWidth('AB', 25);
			$sheet->setWidth('AC', 25);
			$sheet->setWidth('AD', 25);
			$sheet->setWidth('AE', 25);
			$sheet->setWidth('AF', 25);
			$sheet->setWidth('AG', 21);
			$sheet->setWidth('AH', 10);
			$sheet->setWidth('AI', 69);

			// Tambahan
			$sheet->setWidth('AJ', 20);
			$sheet->setWidth('AK', 20);

			$sheet->setBorder('A2:AK4', 'thin');

			$sheet->setFreeze('D5');
			$sheet->setAutoFilter('A4:AK4');
		});

		})->export('xls');
	}

	public function ExportAllExcel($filetype, $dt=null)
	{
		$ds = json_decode($dt);
		$penyakit  = Session::get('penyakit');
		$excel_tmp = array(
			['No','No Epid Kasus / KLB','No.Epid Lama','NIK','Nama Anak','Nama Orangtua','Jenis Kelamin','Tanggal Lahir','Umur','','','Alamat','Kelurahan','Kecamatan','Kabupaten/Kota','Provinsi','Imunisasi Polio Sebelum Sakit','Tanggal Mulai Lumpuh (Bukan Karena Ruda Paksa)','Demam Sebelum Lumpuh (Bukan Karena Ruda Paksa)','Tanggal Imunisasi Polio Terakhir','Kelumpukan Anggota Gerak','','Gangguan Raba Anggota Gerak','','Imunisasi rutin polio sebelum sakit','','PIN, Mop-up, ORI, BIAS Polio','','Tanggal imunisasi polio terakhir','Tanggal Laporan Diterima','Tanggal Pelacakan','Kontak','Keadaan Akhir','Tanggal Pengambilan Spesimen (Stool)','','Hasil Lab','','','','','','Klasifikasi Final (Polio / Bukan Polio / Compatible)'],
			['','','','','','','','','Tahun','Bulan','Hari','','','','','','berapa kali','','','','Kanan','Kiri','Kanan','Kiri','Jumlah dosis','Sumber informasi','Jumlah dosis','Sumber informasi','','','','','','Spesimen 1','Spesimen 2','Spesimen 1','','','Spesimen 2','','',''],
			['','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Isolasi Virus','ITD','Sequencing','Isolasi Virus','ITD','Sequencing',''],
			[]
		);

		$between = $wilayah = '';
		$unit			= (empty($post['unit'])?'':$post['unit']);
		$day_start      = (empty($post['day_start'])?'':$post['day_start']);
		$month_start    = (empty($post['month_start'])?'':$post['month_start']);
		$year_start     = (empty($post['year_start'])?'':$post['year_start']);
		$day_end        = (empty($post['day_end'])?'':$post['day_end']);
		$month_end      = (empty($post['month_end'])?'':$post['month_end']);
		$year_end       = (empty($post['year_end'])?'':$post['year_end']);
		$province_id    = (empty($post['province_id'])?'':$post['province_id']);
		$district_id    = (empty($post['district_id'])?'':$post['district_id']);
		$sub_district_id = (empty($post['sub_district_id'])?'':$post['sub_district_id']);
		$rs_id			 = (empty($post['rs_id'])?'':$post['rs_id']);
		$puskesmas_id	 = (empty($post['puskesmas_id'])?'':$post['puskesmas_id']);

			switch($unit) {
				case "all" :
					$between = " ";
				break;
				case "year" :
					$start   = $year_start;
					$end     = $year_end;
					$between = " AND YEAR(afp_tanggal_periksa) BETWEEN '".$start."' AND '".$end."' ";
				break;
				case "month" :
					$start   = $year_start . '-' . $month_start . '-1';
					$end     = $year_end . '-' . $month_end . '-' . date('t', mktime(0,0,0,$month_end, 1, $year_end));
					$between = " AND DATE(afp_tanggal_periksa) BETWEEN STR_TO_DATE('".$start."', '%Y-%c-%e') AND STR_TO_DATE('".$end."', '%Y-%c-%e') ";
				break;
				default :
					$start   = $year_start . '-' . $month_start . '-' . $day_start;
					$end     = $year_end . '-' . $month_end . '-' . $day_end;
					$between = " AND DATE(afp_tanggal_periksa) BETWEEN STR_TO_DATE('".$start."', '%Y-%c-%e') AND STR_TO_DATE('".$end."', '%Y-%c-%e') ";
				break;
			}

			if($puskesmas_id){
				$wilayah = "AND puskesmas_code_faskes='".$puskesmas_id."'";
			} else if($sub_district_id){
				$wilayah = "AND pasien_id_kecamatan='".$sub_district_id."'";
			} else if($rs_id) {
				$wilayah = "AND rs_kode_faskes='".$rs_id."' ";
			} else if($district_id) {
				$wilayah = "AND pasien_id_kabupaten='".$district_id."' ";
			} else if($province_id) {
				$wilayah = "AND pasien_id_provinsi='".$province_id."' ";
			} 

			$q = "
				SELECT
				*
				FROM getdetailafp
				WHERE
				1=1
				$between
				$wilayah
			";
			$data=DB::select($q);

			$ke_bawah = 3;
			for($i=0;$i<count($data);$i++){
				$excel_tmp[$ke_bawah+$i][]  = 1+$i;
				$excel_tmp[$ke_bawah+$i][]  = $data[$i]->afp_no_epid;
				$excel_tmp[$ke_bawah+$i][]  = $data[$i]->afp_no_epid_lama;
				$excel_tmp[$ke_bawah+$i][]  = $data[$i]->pasien_nik;
				$excel_tmp[$ke_bawah+$i][]  = $data[$i]->pasien_nama_anak;
				$excel_tmp[$ke_bawah+$i][]  = $data[$i]->pasien_nama_ortu;
				switch ($data[$i]->pasien_jenis_kelamin) {
					case '1':$jkl = 'Laki-laki';break;
					case '2':$jkl = 'Perempuan';break;
					case '3':$jkl = 'Tidak jelas';break;
					default:$jkl = '-';break;
				}
				$excel_tmp[$ke_bawah+$i][]  = $jkl;
				$excel_tmp[$ke_bawah+$i][]  = Helper::getDate($data[$i]->pasien_tanggal_lahir);
				$excel_tmp[$ke_bawah+$i][]  = $data[$i]->pasien_umur;
				$excel_tmp[$ke_bawah+$i][]  = $data[$i]->pasien_umur_bln;
				$excel_tmp[$ke_bawah+$i][]  = $data[$i]->pasien_umur_hr;
				$excel_tmp[$ke_bawah+$i][]  = $data[$i]->pasien_alamat;
				$excel_tmp[$ke_bawah+$i][]  = $data[$i]->pasien_kelurahan;
				$excel_tmp[$ke_bawah+$i][]  = $data[$i]->pasien_kecamatan;
				$excel_tmp[$ke_bawah+$i][]  = $data[$i]->pasien_kabupaten;
				$excel_tmp[$ke_bawah+$i][]  = $data[$i]->pasien_provinsi;
				$excel_tmp[$ke_bawah+$i][]  = $data[$i]->afp_imunisasi_polio_sebelum_sakit;
				$excel_tmp[$ke_bawah+$i][]  = Helper::getDate($data[$i]->afp_tanggal_mulai_lumpuh);
				$excel_tmp[$ke_bawah+$i][]  = $data[$i]->afp_no_epid;
				$excel_tmp[$ke_bawah+$i][]  = $data[$i]->afp_no_epid;
				$excel_tmp[$ke_bawah+$i][]  = $data[$i]->afp_no_epid;
				$excel_tmp[$ke_bawah+$i][]  = $data[$i]->afp_no_epid;
				$excel_tmp[$ke_bawah+$i][]  = $data[$i]->afp_no_epid;
				$excel_tmp[$ke_bawah+$i][]  = $data[$i]->afp_no_epid;
			}

		Excel::create('FORM_C1_AFP_'.date('Y_m_d_h_i_s'), function($excel) use($excel_tmp) {
			$excel->sheet('Sheetname', function($sheet) use($excel_tmp) {
			$sheet->fromArray($excel_tmp);
			//$sheet->mergeCells('A1:AF1');

			$sheet->mergeCells('A2:A4');
			$sheet->mergeCells('B2:B4');
			$sheet->mergeCells('C2:C4');
			$sheet->mergeCells('D2:D4');
			$sheet->mergeCells('E2:E4');
			$sheet->mergeCells('F2:F4');
			$sheet->mergeCells('G2:G4');
			$sheet->mergeCells('H2:H4');
			$sheet->mergeCells('I2:I4');
			$sheet->mergeCells('J3:J4');
			$sheet->mergeCells('K3:K4');
			$sheet->mergeCells('L2:L4');
			$sheet->mergeCells('M3:M4');
			$sheet->mergeCells('N3:N4');
			$sheet->mergeCells('O3:O4');
			$sheet->mergeCells('P2:P4');
			$sheet->mergeCells('Q2:Q4');
			$sheet->mergeCells('R2:R4');
			$sheet->mergeCells('S3:S4');
			$sheet->mergeCells('T3:T4');
			$sheet->mergeCells('U3:U4');
			$sheet->mergeCells('V3:V4');
			$sheet->mergeCells('W2:W4');
			$sheet->mergeCells('X2:X4');
			$sheet->mergeCells('Y3:Y4');
			$sheet->mergeCells('Z3:Z4');
			//$sheet->mergeCells('AA3:AA4');
			//$sheet->mergeCells('AB3:AB4');
			//$sheet->mergeCells('AC3:AC4');
			//$sheet->mergeCells('AD3:AD4');
			//$sheet->mergeCells('AE3:AE4');
			//$sheet->mergeCells('AF3:AF4');
			$sheet->mergeCells('AG2:AG4');
			$sheet->mergeCells('AH2:AH4');
			$sheet->mergeCells('AI2:AI4');

			// Tambahan
			$sheet->mergeCells('AJ2:AJ4');
			$sheet->mergeCells('AK2:AK4');

			$sheet->mergeCells('J2:K2');
			$sheet->mergeCells('M2:O2');
			$sheet->mergeCells('S2:T2');
			$sheet->mergeCells('U2:V2');
			$sheet->mergeCells('Y2:Z2');
			$sheet->mergeCells('AA2:AF2');
			$sheet->mergeCells('AA3:AC3');
			$sheet->mergeCells('AD3:AF3');

			$sheet->cells('A2:AK4', function($cells) {
				$cells->setAlignment('center');
				$cells->setValignment('center');
				$cells->setBackground('#FFFFAA');
				$cells->setFontSize(13);
				$cells->setFontWeight('bold');
			});

			//$sheet->cells('N5:AO1000', function($cells) {
			//	$cells->setAlignment('center');
			//	$cells->setValignment('center');
			//});

			$sheet->setWidth('A', 5);
			$sheet->setWidth('B', 25);
			$sheet->setWidth('C', 22);
			$sheet->setWidth('D', 22);
			$sheet->setWidth('E', 50);
			$sheet->setWidth('F', 21);
			$sheet->setWidth('G', 20);
			$sheet->setWidth('H', 20);
			$sheet->setWidth('I', 30);
			$sheet->setWidth('J', 15);
			$sheet->setWidth('K', 15);
			$sheet->setWidth('L', 26);
			$sheet->setWidth('M', 20);
			$sheet->setWidth('N', 17);
			$sheet->setWidth('O', 17);
			$sheet->setWidth('P', 45);
			$sheet->setWidth('Q', 74);
			$sheet->setWidth('R', 71);
			$sheet->setWidth('S', 20);
			$sheet->setWidth('T', 20);
			$sheet->setWidth('U', 20);
			$sheet->setWidth('V', 20);
			$sheet->setWidth('W', 36);
			$sheet->setWidth('X', 30);
			$sheet->setWidth('Y', 27);
			$sheet->setWidth('Z', 27);
			$sheet->setWidth('AA', 25);
			$sheet->setWidth('AB', 25);
			$sheet->setWidth('AC', 25);
			$sheet->setWidth('AD', 25);
			$sheet->setWidth('AE', 25);
			$sheet->setWidth('AF', 25);
			$sheet->setWidth('AG', 21);
			$sheet->setWidth('AH', 10);
			$sheet->setWidth('AI', 69);

			// Tambahan
			$sheet->setWidth('AJ', 20);
			$sheet->setWidth('AK', 20);

			$sheet->setBorder('A2:AK4', 'thin');

			$sheet->setFreeze('D5');
			$sheet->setAutoFilter('A4:AK4');
		});

		})->export('xls');
	}



	/**
	 * [filterDataKasus description]
	 *
	 * @return [type]
	 */
	public function filterDaftarKasus()
	{
		$type = Session::get('type');

		$kd_faskes = Session::get('kd_faskes');

		$wilayah = "";

		// mendapatkan tipe user yang sedang login
		if($type == "puskesmas") {
			$wilayah = "AND h.puskesmas_code_faskes='".$kd_faskes."' ";
		} else if($type == "kabupaten") {
			$wilayah = "AND h.kode_kab='".$kd_faskes."' ";
		} else if($type == "provinsi") {
			$wilayah = "AND h.kode_prop='".$kd_faskes."' ";
		} else if($type == "kemenkes") {
			$wilayah = "";
		}

		$unit            = Input::get('unit');
		$day_start       = Input::get('day_start');
		$month_start     = Input::get('month_start');
		$year_start      = Input::get('year_start');
		$day_end         = Input::get('day_end');
		$month_end       = Input::get('month_end');
		$year_end        = Input::get('year_end');
		$province_id     = Input::get('province_id');
		$district_id     = Input::get('district_id');
		$sub_district_id = Input::get('sub_district_id');
		$village_id      = Input::get('village_id');

		switch($unit) {
			case "all" :
				$between = " ";
			break;
			case "year" :
				$start = $post['year_start'];
				$end = $post['year_end'];
				$between = " AND YEAR(c.tanggal_periksa) BETWEEN '".$start."' AND '".$end."' ";
			break;
			case "month" :
				$start = $post['year_start'] . '-' . $post['month_start'] . '-1';
				$end = $post['year_end'] . '-' . $post['month_end'] . '-' . date('t', mktime(0,0,0,$post['month_end'], 1, $post['year_end']));
				$between = " AND DATE(c.tanggal_periksa) BETWEEN STR_TO_DATE('".$start."', '%Y-%c-%e') AND STR_TO_DATE('".$end."', '%Y-%c-%e') ";
			break;
			default :
				$start = $post['year_start'] . '-' . $post['month_start'] . '-' . $post['day_start'];
				$end = $post['year_end'] . '-' . $post['month_end'] . '-' . $post['day_end'];
				$between = " AND DATE(c.tanggal_periksa) BETWEEN STR_TO_DATE('".$start."', '%Y-%c-%e') AND STR_TO_DATE('".$end."', '%Y-%c-%e') ";
			break;
		}

		$wilayah="";
		if($village_id){
			$wilayah = "AND d.id_kelurahan='".$village_id."' ";
		} else if($sub_district_id) {
			$wilayah = "AND e.id_kecamatan='".$sub_district_id."' ";
		} else if($district_id) {
			$wilayah = "AND f.id_kabupaten='".$district_id."' ";
		} else if($province_id) {
			$wilayah = "AND g.id_provinsi='".$province_id."' ";
		}

		$q = "
			SELECT
				c.no_epid,
				c.id_afp,
				b.nama_anak,
				b.tanggal_lahir,
				CONCAT_WS(',',b.alamat,d.kelurahan,e.kecamatan,f.kabupaten,g.provinsi) as alamat,
				#c.keadaan_akhir,

				IF(c.keadaan_akhir='0','Hidup/sehat',
				IF(c.keadaan_akhir='1','Meninggal',
				''
				)) AS keadaan_akhir_nm,

				#c.klasifikasi_final,

				IF(c.klasifikasi_final='0','Polio',
				IF(c.klasifikasi_final='1','Bukan Polio',
				IF(c.klasifikasi_final='2','compatible',
				'Tidak diisi'))) AS klasifikasi_akhir_nm,

				a.id_hasil_uji_lab_afp

			FROM hasil_uji_lab_afp a
			JOIN pasien b ON b.id_pasien=a.id_pasien
			JOIN afp c ON c.id_afp=a.id_afp
			LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
			LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
			LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
			LEFT JOIN provinsi g ON g.id_provinsi=f.id_provinsi
			LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa

			#WHERE
			#1=1
			#".$between."
			#".$wilayah."
		";

		$data=DB::select($q);

		return View::make('pemeriksaan.afp.index')
			->with(compact('data'));

	}
}
