<?php

class CampakController extends \BaseController {
		//load view
	protected $layout = 'layouts.master';

	public function __construct() {
				//filter
		$this->beforeFilter('auth');
	}
	function sync(){
		$q = DB::select("
			SELECT
			a.*
			FROM bc_campak AS a
			LEFT OUTER JOIN campak AS b ON a.created_at=b.created_at AND a.created_at IS NOT NULL
			WHERE b.created_at IS NULL AND YEAR(a.created_at) = '2017'");
		foreach($q AS $key=>$val){
			$val = (array) $val;
			$id_campak_old = $val['id_campak'];
			$p1 = (array) DB::table('bc_campak')->where('id_campak',$id_campak_old)->first();
			unset($p1['id_campak']);
			$id_campak = DB::table('campak')->insertGetId($p1);
			$p2 = (array) DB::table('bc_hasil_uji_lab_campak')->where('id_campak',$id_campak_old)->first();
			unset($p2['id_hasil_uji_lab_campak']);
			$p2['id_campak'] = $id_campak;
			$id_hasil_uji_lab_campak = DB::table('hasil_uji_lab_campak')->insertGetId($p2);
			$p3 = (array) DB::table('bc_pasien')->where('id_pasien',$p2['id_pasien'])->first();
			unset($p3['id_pasien']);
			$id_pasien = DB::table('pasien')->insertGetId($p3);
			DB::table('hasil_uji_lab_campak')->where('id_hasil_uji_lab_campak',$id_hasil_uji_lab_campak)->update(['id_pasien'=>$id_pasien]);
			$p4 = (array) DB::table('bc_komplikasi')->where('id_campak',$id_campak_old)->get();
			foreach ($p4 as $k => $v) {
				$v = (array) $v;
				unset($v['id']);
				$v['id_campak'] = $id_campak;
				DB::table('komplikasi')->insert($v);
			}
			$p5 = (array) DB::table('bc_gejala_campak')->where('id_campak',$id_campak_old)->get();
			foreach ($p5 as $k => $v) {
				$v = (array) $v;
				unset($v['id']);
				$v['id_campak'] = $id_campak;
				DB::table('gejala_campak')->insert($v);
			}
			$p6 = (array) DB::table('bc_uji_spesimen')->where('id_campak',$id_campak_old)->get();
			foreach ($p6 as $k => $v) {
				$v = (array) $v;
				unset($v['id']);
				$v['id_campak'] = $id_campak;
				DB::table('uji_spesimen')->insert($v);
			}
			$p7 = (array) DB::table('bc_notification')->where(['global_id'=>$id_campak_old,'type_id'=>'campak'])->get();
			foreach ($p7 as $k => $v) {
				$v = (array) $v;
				unset($v['id']);
				$v['global_id'] = $id_campak;
				DB::table('notification')->insert($v);
			}
			$p8 = (array) DB::table('bc_pe_campak')->whereNull('deleted_at')->where(['id'=>$p1['id_pe_campak']])->first();
			unset($p8['id']);
			$id_pe_campak = DB::table('pe_campak')->insertGetId($p8);
			DB::table('campak')->where('id_campak',$id_campak)->update(['id_pe_campak'=>$id_pe_campak]);
			echo"<pre>";print_r($val);echo"</pre>";
		}
	}

		/**
		 * Display a listing of Campak.
		 *
		 * @return Response
		 */
		public function index() {
			Session::put('sess_id', '');
			Session::put('penyakit', 'Campak');
			Session::put('sess_penyakit', 'campak');
			$between = $district = '';
			if (!empty(Input::get())) {
				$unit            = Input::get('unit');
				$day_start       = Input::get('day_start');
				$month_start     = Input::get('month_start');
				$year_start      = Input::get('year_start');
				$day_end         = Input::get('day_end');
				$month_end       = Input::get('month_end');
				$year_end        = Input::get('year_end');
				$province_id     = Input::get('province_id');
				$district_id     = Input::get('district_id');
				$sub_district_id = Input::get('sub_district_id');
				$rs_id           = Input::get('rs_id');
				$puskesmas_id    = Input::get('puskesmas_id');

				switch ($unit) {
					case 'all':
					$between = '';
					break;
					case 'year':
					$start   = $year_start;
					$end     = $year_end;
					$between = "AND CASE
					WHEN campak_tanggal_timbul_rash NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(campak_tanggal_timbul_rash)
					WHEN campak_tanggal_timbul_demam NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(campak_tanggal_timbul_demam)
					END BETWEEN '"     . $start . "' AND '" . $end . "' ";
					break;
					case 'month':
					$start   = $year_start . '-' . $month_start . '-1';
					$end     = $year_end . '-' . $month_end . '-' . date('t', mktime(0, 0, 0, $month_end, 1, $year_end));
					$between = "AND CASE
					WHEN campak_tanggal_timbul_rash NOT IN ('1970-01-01' , '0000-00-00') THEN DATE(campak_tanggal_timbul_rash)
					WHEN campak_tanggal_timbul_demam NOT IN ('1970-01-01' , '0000-00-00') THEN DATE(campak_tanggal_timbul_demam)
					END BETWEEN STR_TO_DATE('"     . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
					break;
					default:
					$start   = $year_start . '-' . $month_start . '-' . $day_start;
					$end     = $year_end . '-' . $month_end . '-' . $day_end;
					$between = "AND CASE
					WHEN campak_tanggal_timbul_rash NOT IN ('1970-01-01' , '0000-00-00') THEN DATE(campak_tanggal_timbul_rash)
					WHEN campak_tanggal_timbul_demam NOT IN ('1970-01-01' , '0000-00-00') THEN DATE(campak_tanggal_timbul_demam)
					END BETWEEN STR_TO_DATE('"     . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
					break;
				}

				if ($puskesmas_id) {
					$district = "AND puskesmas_code_faskes='" . $puskesmas_id . "'";
				} elseif ($sub_district_id) {
					$district = "AND pasien_id_kecamatan='" . $sub_district_id . "'";
				} elseif ($rs_id) {
					$district = "AND rs_kode_faskes='" . $rs_id . "' ";
				} elseif ($district_id) {
					$district = "AND pasien_id_kabupaten='" . $district_id . "' ";
				} elseif ($province_id) {
					$district = "AND pasien_id_provinsi='" . $province_id . "' ";
				}
			}

			$data = Campak::pasiencampak($between, $district);

			return View::make('pemeriksaan.campak.index', compact('data'));
		}

		/**
		 * Show the form for creating a new Campak.
		 *
		 * @return Response
		 */
		public function create() {
			$this->layout->content = View::make('pemeriksaan.campak.index');
		}

		/**
		 * Store a newly created Campak in storage.
		 *
		 * @return Response
		 */
		public function store() {
			DB::transaction(function () {
				$type = Session::get('type');
				$faskes_id = Sentry::getUser()->id_user;

				$id_pasien = DB::table('pasien')->insertGetId(
					array(
						'nik'           => Input::get('nik'),
						'nama_anak'     => Input::get('nama_anak'),
						'nama_ortu'     => Input::get('nama_ortu'),
						'alamat'        => Input::get('alamat'),
						'tanggal_lahir' => $this->encode_date(Input::get('tanggal_lahir')),
						'umur'          => Input::get('tgl_tahun'),
						'umur_bln'      => Input::get('tgl_bulan'),
						'umur_hr'       => Input::get('tgl_hari'),
						'jenis_kelamin' => Input::get('jenis_kelamin'),
						'id_kelurahan'  => Input::get('id_kelurahan'),
						'created_by'    => Sentry::getUser()->id,
						'created_at'    => date('Y-m-d H:i:s'),
						)
					);

				$no_epid_klb = $klb_ke = '';
				if (Input::get('jenis_kasus') == 1) {
					$no_epid_klb = Input::get('no_epid_klb');
					$klb_ke = Input::get('klb_ke');
				}

						//simpan data campak
				$id_campak = DB::table('campak')->insertGetId(
					array(
						'no_epid'                       => Input::get('no_epid'),
						'no_epid_klb'                  => $no_epid_klb,
						'no_epid_lama'                  => Input::get('no_epid_lama'),
						'nama_puskesmas'                => Input::get('nama_puskesmas'),
						'id_outbreak'                   => Input::get('id_outbreak'),
						'tanggal_imunisasi_terakhir'    => $this->encode_date(Input::get('tanggal_imunisasi_terakhir')),
						'vaksin_campak_sebelum_sakit'   => Input::get('vaksin_campak_sebelum_sakit'),
						'tanggal_timbul_demam'          => $this->encode_date(Input::get('tanggal_timbul_demam')),
						'tanggal_timbul_rash'           => $this->encode_date(Input::get('tanggal_timbul_rash')),
						'klasifikasi_final'             => Input::get('klasifikasi_final'),
						'jenis_spesimen_serologi'       => Input::get('jenis_spesimen_serologi'),
						'tanggal_ambil_sampel_serologi' => $this->encode_date(Input::get('tanggal_ambil_sampel_serologi')),
						'jenis_spesimen_virologi'       => Input::get('jenis_spesimen_virologi'),
						'tanggal_ambil_sampel_virologi' => $this->encode_date(Input::get('tanggal_ambil_sampel_virologi')),
						'hasil_serologi_igm_campak'     => Input::get('hasil_serologi_igm_campak'),
						'hasil_serologi_igm_rubella'    => Input::get('hasil_serologi_igm_rubella'),
						'hasil_virologi_igm_campak'     => Input::get('hasil_virologi_igm_campak'),
						'hasil_virologi_igm_rubella'    => Input::get('hasil_virologi_igm_rubella'),
						'status_kasus'                  => Input::get('status_kasus'),
						'jenis_kasus'                   => Input::get('jenis_kasus'),
						'klb_ke'                        => $klb_ke,
						'vitamin_A'                     => Input::get('vitamin_A'),
						'keadaan_akhir'                 => Input::get('keadaan_akhir'),
						'tanggal_laporan_diterima'      => $this->encode_date(Input::get('tanggal_laporan_diterima')),
						'tanggal_pelacakan'             => $this->encode_date(Input::get('tanggal_pelacakan')),
						'tanggal_periksa'               => date('Y-m-d'),
						'id_tempat_periksa'             => $faskes_id,
						'kode_faskes'                   => $type,
						'created_by'                    => Sentry::getUser()->id,
						'created_at'                    => date('Y-m-d H:i:s'),
						)
					);

						//simpan ke uji laboratorium
				$id_hasil_campak = DB::table('hasil_uji_lab_campak')->insert(
					array(
						'id_campak'  => $id_campak,
						'id_pasien'  => $id_pasien,
						'created_by' => Sentry::getUser()->id,
						'created_at' => date('Y-m-d H:i:s'),
						)
					);

				$komplikasi = Input::get('komplikasi');
				if (isset($komplikasi)) {
					for ($i = 0; $i < count($komplikasi); ++$i) {
						DB::table('komplikasi')->insert(
							array(
								'id_campak'  => $id_campak,
								'komplikasi' => $komplikasi[$i],
								'created_by' => Sentry::getUser()->id,
								'created_at' => date('Y-m-d H:i:s'),
								)
							);
					}
				}

				$gejala_lain = Input::get('gejala_lain');
				$tanggal_gejala_lain = Input::get('tanggal_gejala_lain');
				if (isset($gejala_lain)) {
					for ($i = 0; $i < count($gejala_lain); ++$i) {
						DB::table('gejala_campak')->insert(
							array(
								'id_campak'               => $id_campak,
								'id_daftar_gejala_campak' => $gejala_lain[$i],
								'tgl_mulai'               => $this->encode_date($tanggal_gejala_lain[$i]),
								'created_by'              => Sentry::getUser()->id,
								'created_at'              => date('Y-m-d H:i:s'),
								)
							);
					}
				}

				$nama_sampel = Input::get('nama_sampel');
				$jenis_sampel = Input::get('jenis_sampel');
				$tanggal_ambil_sampel = Input::get('tanggal_ambil_sampel');
				for ($i = 0; $i < count($nama_sampel); ++$i) {
					DB::table('uji_spesimen')->insert(
						array(
							'id_campak'         => $id_campak,
							'jenis_pemeriksaan' => $nama_sampel[$i],
							'jenis_sampel'      => $jenis_sampel[$i],
							'tgl_ambil_sampel'  => $this->encode_date($tanggal_ambil_sampel[$i]),
							'created_by'        => Sentry::getUser()->id,
							'created_at'        => date('Y-m-d H:i:s'),
							)
						);
				}

				$kecFaskes = $kecPasien = '';
				if ($type == 'puskesmas') {
					$kecPasien = Input::get('id_kecamatan');
					$kecFaskes = DB::table('puskesmas')->WHERE('puskesmas_id', $faskes_id)->PLUCK('kode_kec AS kecFaskes');
				} elseif ($type == 'rs') {
					$kecPasien = Input::get('id_kabupaten');
					$kecFaskes = DB::table('rumahsakit2')->WHERE('id', $faskes_id)->PLUCK('kode_kab');
				}

				if ($kecPasien != $kecFaskes) {
					DB::table('notification')->insert(
						array(
							'from_faskes_id'     => $faskes_id,
							'from_faskes_kec_id' => $kecFaskes,
							'to_faskes_kec_id'   => $kecPasien,
							'type_id'            => 'campak',
							'global_id'          => $id_campak,
							'description'        => '',
							'submit_dttm'        => date('Y-m-d H:i:s'),
							)
						);
				};
			});

$msg           = 'tersimpan';
$no_epid       = Input::get('no_epid');
$nama_pasien   = Input::get('nama_anak');
$usia          = Input::get('tgl_tahun') . ' tahun';
$jenis_kelamin = Input::get('jenis_kelamin');
$keadaan_akhir = (Input::get('keadaan_akhir') == 1) ? 'Hidup/Sehat' : 'Meninggal';

return Redirect::to('campak');
}

		/**
		 * fungsi input individual kasus campak jika data dimasukan dari
		 * laboratorium.
		 */
		public function postInputbaru() {

				//simpan data personal
			$data = DB::table('pasien')->insert(array(
				'nik'           => Input::get('nik'),
				'nama_anak'     => Input::get('nama_anak'),
				'nama_ortu'     => Input::get('nama_ortu'),
				'alamat'        => Input::get('alamat'),
				'tanggal_lahir' => $this->encode_date(Input::get('tanggal_lahir')),
				'umur'          => Input::get('tgl_tahun'),
				'umur_bln'      => Input::get('tgl_bulan'),
				'umur_hr'       => Input::get('tgl_hari'),
				'jenis_kelamin' => Input::get('jenis_kelamin'),
				'id_kelurahan'  => Input::get('id_kelurahan'),
				));

			if ($data) {
						//simpan data campak
						//$cek = DB::select();
				DB::table('campak')->insert(array(
					'no_epid'                     => Input::get('no_epid'),
					'vaksin_campak_sebelum_sakit' => Input::get('vaksin_campak_sebelum_sakit'),
					'tanggal_timbul_demam'        => $this->encode_date(Input::get('tanggal_timbul_demam')),
					'nama_puskesmas'              => Input::get('nama_puskesmas'),
					'tanggal_timbul_rash'         => $this->encode_date(Input::get('tanggal_timbul_rash')),
					'jenis_pemeriksaan'           => Input::get('jenis_pemeriksaan'),
					'tipe_spesimen'               => Input::get('tipe_spesimen'),
					'klasifikasi_final'           => Input::get('klasifikasi_final'),
					'vitamin_A'                   => Input::get('vitamin_A'),
					'keadaan_akhir'               => Input::get('keadaan_akhir'),
					'tanggal_laporan_diterima'    => $this->encode_date(Input::get('tanggal_laporan_diterima')),
					'tanggal_pelacakan'           => $this->encode_date(Input::get('tanggal_pelacakan')),
					'tanggal_periksa'             => date('Y-m-d'),
					'created_at'                  => date('Y-m-d'),
					));

						//ambil id terakhir pada table pasien
						//$id_pasien = 0;
				$id_pasien = DB::table('pasien')->max('id_pasien');

						//ambil id terakhir pada table tetanus
				$id_campak = DB::table('campak')->max('id_campak');

						//simpan ke uji laboratorium
				DB::table('hasil_uji_lab_campak')->insert(array(
					'id_campak' => $id_campak,
					'id_pasien' => $id_pasien,
					));

				$no_epid     = Input::get('no_epid');
				$id_pasien   = $id_pasien;
				$id_campak   = $id_campak;
				$nama_pasien = Input::get('nama_anak');
				$usia        = Input::get('tgl_tahun');

						## kalo selo diganti pake switch case :-) <--hanya style coding bro #nobad
				if (Input::get('klasifikasi_final') == 1) {
					$klasifikasi_final = 'Campak(Lab)';
				} elseif (Input::get('klasifikasi_final') == 2) {
					$klasifikasi_final = 'Campak(Epid)';
				} elseif (Input::get('klasifikasi_final') == 3) {
					$klasifikasi_final = 'Campak(Klinis)';
				} elseif (Input::get('klasifikasi_final') == 4) {
					$klasifikasi_final = 'Rubella';
				} elseif (Input::get('klasifikasi_final') == 5) {
					$klasifikasi_final = 'Bukan campak/rubella';
				}

				$keadaan_akhir = (Input::get('keadaan_akhir') == 1) ? 'Hidup/Sehat' : 'Meninggal';

				$msg = 'tersimpan';

				return Response::json(compact(
					'msg', 'no_epid', 'nama_pasien',
					'id_pasien', 'usia', 'id_campak',
					'klasifikasi_final', 'keadaan_akhir'
					));
			}
		}

		/**
		 * [simpanUjiCampak description].
		 *
		 * @return [type] [description]
		 */
		public function simpanUjiCampak() {

				//simpan hasil laboratorium
			DB::table('hasil_uji_lab_campak')->insert(array(
				'id_hasil_uji_lab_campak'  => Input::get('id_hasil_uji_lab_campak'),
				'id_pasien'                => Input::get('id_pasien'),
				'id_campak'                => Input::get('id_campak'),
				'hasil_spesimen_darah'     => Input::get('hasil_spesimen_darah'),
				'hasil_spesimen_urin'      => Input::get('hasil_spesimen_urin'),
				'klasifikasi_final'        => Input::get('klasifikasi_final'),
				'tanggal_uji_laboratorium' => $this->encode_date(Input::get('tanggal_uji_laboratorium')),
				));
		}

		/**
		 * Display the specified Campak.
		 *
		 * @param int $id
		 *
		 * @return Response
		 */
		public function show($id) {
			$campak = Campak::findOrFail($id);

			return View::make('pemeriksaan.campak.show', compact('campak'));
		}

		/**
		 * Show the form for editing the specified Campak.
		 *
		 * @param int $id
		 *
		 * @return Response
		 */
		public function edit($id) {
			$campak = Campak::find($id);

			return View::make('pemeriksaan.campak.edit', compact('campak'));
		}

		/**
		 * Update the specified Campak in storage.
		 *
		 * @param int $id
		 *
		 * @return Response
		 */
		public function update() {
			$no_epid_klb = $klb_ke = '';
			if (Input::get('jenis_kasus') == 1) {
				$no_epid_klb = Input::get('no_epid_klb');
				$klb_ke = Input::get('klb_ke');
			}
			DB::update("
				update pasien,campak,hasil_uji_lab_campak
				set
				campak.no_epid='" . Input::get('no_epid') . "',
				campak.no_epid_klb='" . $no_epid_klb . "',
				campak.no_epid_lama='" . Input::get('no_epid_lama') . "',
				campak.tanggal_imunisasi_terakhir=CAST(" . $this->changeDate(Input::get('tanggal_imunisasi_terakhir')) . " as DATETIME),
				campak.vaksin_campak_sebelum_sakit='" . Input::get('vaksin_campak_sebelum_sakit') . "',
				campak.tanggal_timbul_demam=CAST(" . $this->changeDate(Input::get('tanggal_timbul_demam')) . " AS DATETIME),
				campak.tanggal_timbul_rash=CAST(" . $this->changeDate(Input::get('tanggal_timbul_rash')) . " AS DATETIME),
				campak.vitamin_A='" . Input::get('vitamin_A') . "',
				campak.id_outbreak='" . Input::get('id_outbreak') . "',
				campak.nama_puskesmas='" . Input::get('nama_puskesmas') . "',
				campak.keadaan_akhir='" . Input::get('keadaan_akhir') . "',
				campak.jenis_kasus='" . Input::get('jenis_kasus') . "',
				campak.klb_ke='" . $klb_ke . "',
				campak.klasifikasi_final='" . Input::get('klasifikasi_final') . "',
				campak.jenis_spesimen_serologi ='" . Input::get('jenis_spesimen_serologi') . "',
				campak.tanggal_ambil_sampel_serologi =CAST(" . $this->changeDate(Input::get('tanggal_ambil_sampel_serologi')) . " AS DATETIME),
				campak.jenis_spesimen_virologi ='" . Input::get('jenis_spesimen_virologi') . "',
				campak.tanggal_ambil_sampel_virologi =CAST(" . $this->changeDate(Input::get('tanggal_ambil_sampel_virologi')) . " AS DATETIME),
				campak.hasil_serologi_igm_campak = '" . Input::get('hasil_serologi_igm_campak') . "',
				campak.hasil_serologi_igm_rubella = '" . Input::get('hasil_serologi_igm_rubella') . "',
				campak.hasil_virologi_igm_campak ='" . Input::get('hasil_virologi_igm_campak') . "',
				campak.hasil_virologi_igm_rubella = '" . Input::get('hasil_virologi_igm_rubella') . "',
				campak.status_kasus              ='" . Input::get('status_kasus') . "',
				campak.tanggal_laporan_diterima=CAST(" . $this->changeDate(Input::get('tanggal_laporan_diterima')) . " AS DATETIME),
				campak.tanggal_pelacakan=CAST(" . $this->changeDate(Input::get('tanggal_pelacakan')) . " AS DATETIME),
				campak.updated_by                ='" . Sentry::getUser()->id . "',
				campak.updated_at                ='" . date('Y-m-d') . "',
				pasien.nik='" . Input::get('nik') . "',
				pasien.nama_anak='" . Input::get('nama_anak') . "',
				pasien.nama_ortu='" . Input::get('nama_ortu') . "',
				pasien.alamat='" . Input::get('alamat') . "',
				pasien.tanggal_lahir=CAST(" . $this->changeDate(Input::get('tanggal_lahir')) . " AS DATETIME),
				pasien.umur='" . Input::get('umur_tahun') . "',
				pasien.umur_bln='" . Input::get('umur_bln') . "',
				pasien.umur_hr='" . Input::get('umur_hr') . "',
				pasien.jenis_kelamin='" . Input::get('jenis_kelamin') . "',
				pasien.id_kelurahan='" . Input::get('id_kelurahan') . "',
				pasien.updated_by                ='" . Sentry::getUser()->id . "',
				pasien.updated_at                ='" . date('Y-m-d') . "'
				where pasien.id_pasien=hasil_uji_lab_campak.id_pasien and campak.id_campak=hasil_uji_lab_campak.id_campak and hasil_uji_lab_campak.id_hasil_uji_lab_campak='" . Input::get('id_hasil_uji_lab_campak') . "'");

			$komplikasi = Input::get('komplikasi');
			DB::table('komplikasi')->where('id_campak', Input::get('id_campak'))->delete();
			for ($i = 0; $i < count($komplikasi); ++$i) {
				DB::table('komplikasi')->insert(
					array(
						'id_campak'  => Input::get('id_campak'),
						'komplikasi' => $komplikasi[$i],
						)
					);
			}

			$nama_sampel          = Input::get('nama_sampel');
			$jenis_sampel         = Input::get('jenis_sampel');
			$tanggal_ambil_sampel = Input::get('tanggal_ambil_sampel');
			for ($i = 0; $i < count($nama_sampel); ++$i) {
				DB::table('uji_spesimen')->where('id_campak', '=', Input::get('id_campak'))->delete();
			}
			for ($i = 0; $i < count($nama_sampel); ++$i) {
				DB::table('uji_spesimen')->insert(
					array(
						'id_campak'         => Input::get('id_campak'),
						'jenis_pemeriksaan' => $nama_sampel[$i],
						'jenis_sampel'      => $jenis_sampel[$i],
						'tgl_ambil_sampel'  => $this->encode_date($tanggal_ambil_sampel[$i]),
						'updated_at'        => date('Y-m-d H:i:s'),
						));
			}

			$gejala_lain         = Input::get('gejala_lain');
			$tanggal_gejala_lain = Input::get('tanggal_gejala_lain');
			DB::table('gejala_campak')->where('id_campak', Input::get('id_campak'))->delete();
			if (isset($gejala_lain)) {
				$gc = array();
				for ($i = 0; $i < count($gejala_lain); ++$i) {
					DB::table('gejala_campak')->insert(
						array(
							'id_campak'               => Input::get('id_campak'),
							'id_daftar_gejala_campak' => $gejala_lain[$i],
							'tgl_mulai'               => $this->encode_date($tanggal_gejala_lain[$i]),
							'created_at'              => date('Y-m-d H:i:s'),
							'updated_at'              => date('Y-m-d H:i:s'),
							));
				}
			}

			return Redirect::to('campak');
		}

		/**
		 * Remove the specified Campak from storage.
		 *
		 * @param int $id
		 *
		 * @return Response
		 */
		public function destroy($id) {
			Campak::destroy($id);

			return Redirect::route('pemeriksaan.campak.index');
		}

		public function postForm() {
			$id = Input::get('id');
			if ($id == 1) {
				return View::make('diagnosa.campak');
			}
		}

		public function getEdit($id) {
			$campak = DB::table('getdetailcampak')->where('campak_id_campak', $id)->get();

			return View::make('pemeriksaan.campak.edit', compact('campak'));
		}

		public function getHapus($id) {
				//deleted :: change status_at to 0 for deleted
			$dt = [
			'deleted_by' => Sentry::getUser()->id,
			'deleted_at' => date('Y-m-d H:i:s'),
			];
			$delete = DB::table('campak')
			->where('id_campak', $id)
			->update($dt);
			$delete_notif = DB::table('notification')
			->where('global_id', $id)
			->where('type_id', 'campak')
			->update($dt);

			return Redirect::to('campak');
		}

		public function laporancampak() {
			$pdf = PDF::loadView('pemeriksaan.campak.cetak');

			return $pdf->stream();
		}

		//fungsi cetak laporan kasus individual campak
		public function getCetakCampak() {
			return View::make('pemeriksaan.campak.cetak1');
		}

		//fungsi untuk mengeload data campak,
		public function getDataCampak() {
			$data = Campak::pasiencampak();

			return View::make('pemeriksaan.campak.datacampak', compact('data'));
		}

		public function detailcampak($id) {
			$detail = DB::table('getdetailcampak')
			->WHERE('campak_id_campak', $id)
			->get();
			$dt     = (empty($detail[0]) ? '' : $detail[0]);
			$gejala = DB::table('gejala_campak')
			->WHERE('id_campak', $id)
			->get();
			$listgejala = array();
			if (count($gejala) >= 1) {
				foreach ($gejala as $key => $val) {
					$dg           = DB::table('daftar_gejala_campak')->where('id', $val->id_daftar_gejala_campak)->pluck('nama');
					$listgejala[] = array(
						'id_gejala'   => $val->id,
						'tgl_sakit'   => Helper::getDate($val->tgl_mulai),
						'nama_gejala' => $dg,
						);
				}
			}
			$komplikasi = DB::table('komplikasi')
			->WHERE('id_campak', $id)
			->get();
			$listkomplikasi = array();
			if (count($komplikasi) >= 1) {
				$ik = '';
				foreach ($komplikasi as $key => $val) {
					if ($val->komplikasi == '0') {
						$ik = 'Diare';
					} elseif ($val->komplikasi == '1') {
						$ik = 'Pneumonia';
					} elseif ($val->komplikasi == '2') {
						$ik = 'Bronchopneumonia';
					} elseif ($val->komplikasi == '3') {
						$ik = 'OMA';
					} elseif ($val->komplikasi == '4') {
						$ik = 'Ensefalitis';
					} else {
						$ik = $val->komplikasi;
					}
					$listkomplikasi[] = array(
						'id_komplikasi'   => $val->id,
						'name_komplikasi' => $ik,
						);
				}
			}
			$ujiSpesimen = DB::table('uji_spesimen')
			->WHERE('id_campak', $id)
			->get();
			$listSpesimen = [];
			if (count($ujiSpesimen) >= 1) {
				foreach ($ujiSpesimen as $key => $val) {
					if (in_array($val->jenis_pemeriksaan, ['0', '1'])) {
						$jenis_pemeriksaan = $jenis_sampel = '';
						if ($val->jenis_pemeriksaan == '0') {
							$jenis_pemeriksaan = 'Serologi';
							if ($val->jenis_sampel == '0') {
								$jenis_sampel = 'Darah';
							} else {
								$jenis_sampel = 'Urin';
							}
						} else {
							$jenis_pemeriksaan = 'Virologi';
							if ($val->jenis_sampel == '0') {
								$jenis_sampel = 'Urin';
							} else if ($val->jenis_sampel == '1') {
								$jenis_sampel = 'Usap tenggorokan';
							} else {
								$jenis_sampel = 'Cairan mulut';
							}
						}
						$listSpesimen[] = array(
							'jenis_pemeriksaan' => $jenis_pemeriksaan,
							'jenis_sampel'      => $jenis_sampel,
							'tgl_ambil_sampel'  => Helper::getDate($val->tgl_ambil_sampel),
							);
					}
				}
			}

			return View::make('pemeriksaan.campak.detail', compact('dt', 'listgejala', 'listkomplikasi', 'listSpesimen'));
		}

		public function getUmur() {
			$tgl_tahun = $tgl_bulan = $tgl_hari = '';
			if (Input::get('tanggal_lahir') && Input::get('tgl_sakit')) {
				$tgl_sakit = str_replace("'", '', $this->encode_date(Input::get('tgl_sakit')));
				$tanggal_lahir = str_replace("'", '', $this->encode_date(Input::get('tanggal_lahir')));
				if (strtotime($tgl_sakit) > strtotime($tanggal_lahir)) {
					list($y, $m, $d) = explode('-', $tgl_sakit);
					list($thn_skrg, $bln_skrg, $tgl_skrg) = explode('-', $tanggal_lahir);
						//$data = Pasien::umur($tanggal_lahir);

						// kelahiran dijadikan dalam format tanggal semua
					$lahir = $tgl_skrg + ($bln_skrg * 30) + ($thn_skrg * 365);

						// sekarang dijadikan dalam format tanggal semua
					$now = $d + ($m * 30) + ($y * 365);

						// dari format tanggal jadikan tahun, bulan, hari
					$tahun = ($now - $lahir) / 365;
					$bulan = (($now - $lahir) % 365) / 30;
					$hari  = (($now - $lahir) % 365) % 30;

					$tgl_tahun = floor($tahun);
					$tgl_bulan = floor($bulan);
					$tgl_hari  = floor($hari);
				}
			}

			return Response::json(compact('tgl_tahun', 'tgl_bulan', 'tgl_hari'));
		}

		public function generateEpid() {
				//clear noepid
			DB::UPDATE("UPDATE campak AS a SET a.no_epid=NULL WHERE a.deleted_at IS NULL");
			DB::UPDATE("UPDATE campak AS a SET a.tanggal_timbul_rash=NULL, a.tanggal_timbul_demam=NULL WHERE (a.tanggal_timbul_demam='0000-00-00' OR a.tanggal_timbul_rash='0000-00-00')");
			$query = DB::table('campak AS a');
			$query->select('a.id_campak',
				'c.id_kelurahan',
				'a.no_epid',
				DB::raw('CASE
					WHEN a.tanggal_timbul_demam IS NOT NULL THEN YEAR(a.tanggal_timbul_demam)
					WHEN a.tanggal_timbul_rash IS NOT NULL THEN YEAR(a.tanggal_timbul_rash)
					END AS thn_rash')
				);
			$query->join('hasil_uji_lab_campak AS b', 'a.id_campak', '=', 'b.id_campak');
			$query->join('pasien AS c', 'b.id_pasien', '=', 'c.id_pasien');
			$query->where('a.deleted_at', null);
			$dt = $query->get();
			foreach ($dt as $key => $val) {
				$thn                             = substr($val->thn_rash, -2);
				$row[$val->id_kelurahan][$thn][] = $val;
			}
			foreach ($row as $key => $val) {
				foreach ($val as $k => $v) {
					$i = 1;
					foreach ($v as $k1 => $v1) {
						$no   = str_pad($i++, 3, 0, STR_PAD_LEFT);
						$kode = 'C' . $v1->id_kelurahan . $k . $no;
						echo "<pre>";
						print_r($kode);
						echo "</pre>";
						$qu = DB::table('campak')->where('id_campak', $v1->id_campak)->update(array('no_epid' => $kode));
					}
				}
			}
			die();
		}

		public function generateEpidKLB() {
				//clear noepid
			DB::UPDATE("UPDATE campak AS a SET a.no_epid_klb=NULL WHERE a.jenis_kasus='1'");
			$query = DB::table('campak AS a');
			$query->select('a.id_campak',
				'c.id_kelurahan',
				'a.no_epid_klb',
				'a.klb_ke',
				DB::raw('CASE
					WHEN a.tanggal_timbul_demam IS NOT NULL THEN YEAR(a.tanggal_timbul_demam)
					WHEN a.tanggal_timbul_rash IS NOT NULL THEN YEAR(a.tanggal_timbul_rash)
					END AS thn_rash')
				);
			$query->join('hasil_uji_lab_campak AS b', 'a.id_campak', '=', 'b.id_campak');
			$query->join('pasien AS c', 'b.id_pasien', '=', 'c.id_pasien');
			$query->where('a.jenis_kasus', '1');
			$query->where('a.deleted_at', null);
			$dt = $query->get();

			foreach ($dt as $key => $val) {
				$thn                                           = substr($val->thn_rash, -2);
				$row[$val->id_kelurahan][$thn][$val->klb_ke][] = $val;
			}
			foreach ($row as $key => $val) {
				foreach ($val as $k => $v) {
					foreach ($v as $k1 => $v1) {
						$i = 1;
						foreach ($v1 as $k2 => $v2) {
							$no   = str_pad($i++, 3, 0, STR_PAD_LEFT);
							$kode = 'C' . $v2->id_kelurahan . $k . $no . '/' . $v2->klb_ke;
							echo "<pre>";
							print_r($kode);
							echo "</pre>";
							$qu = DB::table('campak')->where('id_campak', $v2->id_campak)->update(array('no_epid_klb' => $kode));
						}
					}
				}
			}
			die();
		}

		//fungsi untuk mendapatkan no epid campak secara otomatis
		public function getEpid($params = []) {
			$idKelurahan = Input::get('id_kelurahan');
			$date        = Input::get('date');
			$noepid      = Campak::getEpid(['id_kelurahan' => $idKelurahan, 'date' => $date]);

			return $noepid;
		}

		public function getEpidKLB() {
			$idKelurahan = Input::get('id_kelurahan');
			$klb_ke      = Input::get('klb_ke');
			$date        = substr($this->encode_date(Input::get('date')), 0, 4);
			$thn         = substr($date, 2, 2);
			$kode        = 'C' . $idKelurahan . $thn;

			$query = DB::table('campak AS a');
			$query->join('hasil_uji_lab_campak AS b', 'a.id_campak', '=', 'b.id_campak');
			$query->join('pasien AS c', 'b.id_pasien', '=', 'c.id_pasien');
			$query->where('a.deleted_at', null);
			$query->where('c.id_kelurahan', $idKelurahan);
			$query->where('a.klb_ke', $klb_ke);
			$query->where(DB::raw('CASE
				WHEN a.tanggal_timbul_demam IS NOT NULL THEN YEAR(a.tanggal_timbul_demam)
				WHEN a.tanggal_timbul_rash IS NOT NULL THEN YEAR(a.tanggal_timbul_rash)
				END'), $date);
			$count = $query->count();

			$no     = str_pad($count + 1, 3, 0, STR_PAD_LEFT);
			$noepid = 'C' . $idKelurahan . $thn . $no . '/' . $klb_ke;

			return $noepid;
		}

		public function pasien() {
			$no_epid = Input::get('no_epid');
			$data    = DB::select("select pasien.id_pasien as id_pasien,nama_anak,nama_ortu,alamat,jenis_kelamin,tanggal_lahir,umur,umur_bln,umur_hr,lokasi.provinsi,lokasi.kabupaten,lokasi.kecamatan,lokasi.kelurahan from pasien,campak,hasil_uji_lab_campak,lokasi
				where
				pasien.id_pasien=hasil_uji_lab_campak.id_pasien
				and campak.id_campak=hasil_uji_lab_campak.id_campak
				and pasien.id_kelurahan=lokasi.id_kelurahan
				and campak.no_epid='" . $no_epid . "'");

			foreach ($data as $value) {
				$id_pasien     = $value->id_pasien;
				$nama_anak     = $value->nama_anak;
				$nama_ortu     = $value->nama_ortu;
				$alamat        = $value->alamat;
				$jenis_kelamin = $value->jenis_kelamin;
				$tanggal_lahir = $value->tanggal_lahir;
				$umur          = $value->umur;
				$umur_bln      = $value->umur_bln;
				$umur_hr       = $value->umur_hr;
				$privinsi      = $value->provinsi;
				$kabupaten     = $value->kabupaten;
				$kecamatan     = $value->kecamatan;
				$kelurahan     = $value->kelurahan;
			}

			return Response::json(compact(
				'id_pasien', 'nama_anak', 'nama_ortu',
				'alamat', 'jenis_kelamin', 'tanggal_lahir',
				'umur', 'umur_bln', 'umur_hr',
				'provinsi', 'kabupaten', 'kecamatan',
				'kelurahan'
				));
		}

		//route daftar campak
		public function getDaftarPeCampak() {
			$pe_campak = Campak::getPeCampak();

			return View::make('pemeriksaan.campak.daftar', compact('pe_campak'));
		}

		//fungsi untuk mengambil data pasien yang akan dilakukan penyelidikan epidemologi campak
		public function getEntri_campak($id) {
			$pe_campak = DB::select("
				SELECT
				a.id_campak AS idCampak,
				a.no_epid,
				a.jenis_kasus,
				c.nik,
				c.id_pasien,
				c.tanggal_lahir,
				c.nama_anak,
				c.nama_ortu,
				c.alamat,
				d.kelurahan,
				d.kecamatan,
				d.kabupaten,
				d.provinsi,
				d.id_kelurahan,
				c.umur,
				c.umur_bln,
				c.umur_hr,
				c.jenis_kelamin,
				a.status_kasus
				FROM
				campak AS a
				JOIN hasil_uji_lab_campak AS b ON a.id_campak=b.id_campak
				JOIN pasien AS c ON b.id_pasien=c.id_pasien
				LEFT JOIN located AS d ON c.id_kelurahan=d.id_kelurahan
				WHERE a.id_campak='$id'");

			return View::make('pemeriksaan.campak.pe_campak', compact('pe_campak'));
		}

		//fungsi untuk menyimpan data kasus penyelidikan epidemologi campak
		public function postEpidCampak() {
			if ((
				Input::get('waktu_pengobatan') ||
				Input::get('tempat_pengobatan') ||
				Input::get('obat_yang_diberikan') ||
				Input::get('dirumah_orang_sakit_sama') ||
				Input::get('kapan_sakit_dirumah') ||
				Input::get('disekolah_orang_sakit_sama') ||
				Input::get('kapan_sakit_disekolah') ||
				Input::get('kekurangan_gizi') ||
				Input::get('tanggal_penyelidikan') ||
				Input::get('pelaksana') ||
				Input::get('alamat_tinggal_sementara') ||
				Input::get('tgl_pe_dialamat_sementara') ||
				Input::get('jumlah_kasus_dialamat_sementara') ||
				Input::get('alamat_tempat_tinggal') ||
				Input::get('tgl_pe_alamat_tempat_tinggal') ||
				Input::get('jumlah_kasus_dialamat_tempat_tinggal') ||
				Input::get('sekolah') ||
				Input::get('tgl_pe_sekolah') ||
				Input::get('jumlah_kasus_disekolah') ||
				Input::get('tempat_kerja') ||
				Input::get('tgl_pe_tempat_kerja') ||
				Input::get('jumlah_kasus_tempat_kerja') ||
				Input::get('lain_lain') ||
				Input::get('tgl_pe_lain_lain') ||
				Input::get('jumlah_kasus_lain_lain') ||
				Input::get('jumlah_total_kasus_tambahan')) == '') {
				return Redirect::to('campak');
			} else {
				$pel = '';
				if (!empty(Input::get('pelaksana')[0])) {
					foreach (Input::get('pelaksana') as $key => $val) {
						$pel .= $val.';';
					}
				}
				$postEpid = array(
					'id_pasien'                            => Input::get('id_pasien'),
					'faskes_id'                            => Sentry::getUser()->id_user,
					'created_by'                           => Sentry::getUser()->id,
					'no_epid'                              => $this->set_value(Input::get('no_epid')),
					'waktu_pengobatan'                     => $this->encode_date(Input::get('waktu_pengobatan')),
					'tempat_pengobatan'                    => Input::get('tempat_pengobatan'),
					'obat_yang_diberikan'                  => Input::get('obat_yang_diberikan'),
					'dirumah_orang_sakit_sama'             => Input::get('dirumah_orang_sakit_sama'),
					'kapan_sakit_dirumah'                  => $this->encode_date(Input::get('kapan_sakit_dirumah')),
					'disekolah_orang_sakit_sama'           => Input::get('disekolah_orang_sakit_sama'),
					'kapan_sakit_disekolah'                => $this->encode_date(Input::get('kapan_sakit_disekolah')),
					'kekurangan_gizi'                      => Input::get('kekurangan_gizi'),
					'tanggal_penyelidikan'                 => $this->encode_date(Input::get('tanggal_penyelidikan')),
					'pelaksana'                            => $pel,
					'alamat_tinggal_sementara'             => !empty(Input::get('tgl_pe_dialamat_sementara')) OR !empty(Input::get('jumlah_kasus_dialamat_sementara')) ? 1 : NULL,
					'tgl_pe_dialamat_sementara'            => $this->encode_date(Input::get('tgl_pe_dialamat_sementara')),
					'jumlah_kasus_dialamat_sementara'      => $this->set_value(Input::get('jumlah_kasus_dialamat_sementara')),
					'alamat_tempat_tinggal'                => !empty(Input::get('tgl_pe_alamat_tempat_tinggal')) OR !empty(Input::get('jumlah_kasus_dialamat_tempat_tinggal')) ? 1 : NULL,
					'tgl_pe_alamat_tempat_tinggal'         => $this->encode_date(Input::get('tgl_pe_alamat_tempat_tinggal')),
					'jumlah_kasus_dialamat_tempat_tinggal' => $this->set_value(Input::get('jumlah_kasus_dialamat_tempat_tinggal')),
					'sekolah'                              => !empty(Input::get('tgl_pe_sekolah')) OR !empty(Input::get('jumlah_kasus_disekolah')) ? 1 : NULL,
					'tgl_pe_sekolah'                       => $this->encode_date(Input::get('tgl_pe_sekolah')),
					'jumlah_kasus_disekolah'               => $this->set_value(Input::get('jumlah_kasus_disekolah')),
					'tempat_kerja'                         => !empty(Input::get('tgl_pe_tempat_kerja')) OR !empty(Input::get('jumlah_kasus_tempat_kerja')) ? 1 : NULL,
					'tgl_pe_tempat_kerja'                  => $this->encode_date(Input::get('tgl_pe_tempat_kerja')),
					'jumlah_kasus_tempat_kerja'            => $this->set_value(Input::get('jumlah_kasus_tempat_kerja')),
					'lain_lain'                            => Input::get('lain_lain'),
					'tgl_pe_lain_lain'                     => $this->encode_date(Input::get('tgl_pe_lain_lain')),
					'jumlah_kasus_lain_lain'               => $this->set_value(Input::get('jumlah_kasus_lain_lain')),
					'jumlah_total_kasus_tambahan'          => $this->set_value(Input::get('jumlah_total_kasus_tambahan')),
					'created_by'                           => Sentry::getUser()->id,
					'created_at'                           => date('Y-m-d H:i:s'),
					);
				$dt['id_pe_campak'] = DB::table('pe_campak')->insertGetId($postEpid);
				$update             = DB::table('campak')
				->where('id_campak', Input::get('id_campak'))
				->update($dt);

				return Redirect::to('campak');
			}
		}

		//fungsi untuk menampilkan detail PE campak
		public function getDetailPeCampak($id) {
			$data = DB::select("
				SELECT
				pasien.id_pasien,
				pasien.nik,
				pasien.nama_anak,
				pasien.nama_ortu,
				CONCAT_WS(',',pasien.alamat,located.kelurahan,located.kecamatan,located.kabupaten,located.provinsi) AS alamat,
				pasien.tanggal_lahir,
				pasien.umur,
				pasien.umur_bln,
				pasien.umur_hr,
				pasien.jenis_kelamin,
				pasien.id_kelurahan,
				pe_campak.*
				FROM
				pe_campak
				JOIN campak ON pe_campak.id = campak.id_pe_campak
				JOIN hasil_uji_lab_campak ON campak.id_campak = hasil_uji_lab_campak.id_campak
				JOIN pasien ON hasil_uji_lab_campak.id_pasien=pasien.id_pasien
				LEFT JOIN located ON pasien.id_kelurahan=located.id_kelurahan
				WHERE pe_campak.id='" . $id . "'");

			return View::make('pemeriksaan.campak.detail_pe_campak', compact('data'));
		}

		//fungsi untuk menampilkan edit data PE campak
		public function getEditPeCampak($id) {
			$data = Campak::getEditPeCampak($id);
			return View::make('pemeriksaan.campak.edit_pe_campak', compact('data'));
		}

		//fungsi untuk menampilkan nama puskesmas daerah tempat tinggal pasien
		public function getTampilFaskes() {
			$id_kelurahan = Input::get('id_kelurahan');
				/*$sql = DB::table('puskesmas')->where('kode_desa',$id_kelurahan);
				foreach ($sql as $key => $value) {
				$faskes = $value;
			}*/
			return $id_kelurahan;
		}

		//fungsi untuk menyimpan hasil update data PE campak
		public function postUpdatePeCampak() {
			$postEpid = array(
				'waktu_pengobatan'                     => $this->encode_date(Input::get('waktu_pengobatan')),
				'tempat_pengobatan'                    => Input::get('tempat_pengobatan'),
				'obat_yang_diberikan'                  => Input::get('obat_yang_diberikan'),
				'dirumah_orang_sakit_sama'             => Input::get('dirumah_orang_sakit_sama'),
				'kapan_sakit_dirumah'                  => $this->encode_date(Input::get('kapan_sakit_dirumah')),
				'disekolah_orang_sakit_sama'           => Input::get('disekolah_orang_sakit_sama'),
				'kapan_sakit_disekolah'                => $this->encode_date(Input::get('kapan_sakit_disekolah')),
				'kekurangan_gizi'                      => Input::get('kekurangan_gizi'),
				'tanggal_penyelidikan'                 => $this->encode_date(Input::get('tanggal_penyelidikan')),
				'pelaksana'                            => Input::get('pelaksana'),
				'alamat_tinggal_sementara'             => !empty(Input::get('tgl_pe_dialamat_sementara')) OR !empty(Input::get('jumlah_kasus_dialamat_sementara')) ? 1 : NULL,
				'tgl_pe_dialamat_sementara'            => $this->encode_date(Input::get('tgl_pe_dialamat_sementara')),
				'jumlah_kasus_dialamat_sementara'      => $this->set_value(Input::get('jumlah_kasus_dialamat_sementara')),
				'alamat_tempat_tinggal'                => !empty(Input::get('tgl_pe_alamat_tempat_tinggal')) OR !empty(Input::get('jumlah_kasus_dialamat_tempat_tinggal')) ? 1 : NULL,
				'tgl_pe_alamat_tempat_tinggal'         => $this->encode_date(Input::get('tgl_pe_alamat_tempat_tinggal')),
				'jumlah_kasus_dialamat_tempat_tinggal' => $this->set_value(Input::get('jumlah_kasus_dialamat_tempat_tinggal')),
				'sekolah'                              => !empty(Input::get('tgl_pe_sekolah')) OR !empty(Input::get('jumlah_kasus_disekolah')) ? 1 : NULL,
				'tgl_pe_sekolah'                       => $this->encode_date(Input::get('tgl_pe_sekolah')),
				'jumlah_kasus_disekolah'               => $this->set_value(Input::get('jumlah_kasus_disekolah')),
				'tempat_kerja'                         => !empty(Input::get('tgl_pe_tempat_kerja')) OR !empty(Input::get('jumlah_kasus_tempat_kerja')) ? 1 : NULL,
				'tgl_pe_tempat_kerja'                  => $this->encode_date(Input::get('tgl_pe_tempat_kerja')),
				'jumlah_kasus_tempat_kerja'            => $this->set_value(Input::get('jumlah_kasus_tempat_kerja')),
				'lain_lain'                            => Input::get('lain_lain'),
				'tgl_pe_lain_lain'                     => $this->encode_date(Input::get('tgl_pe_lain_lain')),
				'jumlah_kasus_lain_lain'               => $this->set_value(Input::get('jumlah_kasus_lain_lain')),
				'jumlah_total_kasus_tambahan'          => $this->set_value(Input::get('jumlah_total_kasus_tambahan')),
				'tanggal_penyelidikan'                     => $this->encode_date(Input::get('tanggal_penyelidikan')),
				'updated_by'                           => Sentry::getUser()->id,
				'updated_at'                           => date('Y-m-d H:i:s'),
				);
			DB::table('pe_campak')->where('id', Input::get('id'))->update($postEpid);

			return Redirect::to('campak');
		}

		public function getTampilDataCampak() {
		}

		public function getHapusPeCampak($id) {
				//deleted :: change status_at to 0 for deleted
			$dt = [
			'status_at'  => 0,
			'deleted_by' => Sentry::getUser()->id,
			'deleted_at' => date('Y-m-d H:i:s'),
			];
			$delete = DB::table('pe_campak')
			->where('id', $id)
			->update($dt);

			return Redirect::to('campak');
		}

		public function getAll() {
			return Campak::with('patients')->get();
		}

		public function ExportAllExcel($filetype, $dt=null) {

			$excel_tmp = [
				['NO','No.Epid','No.Epid Lama','No.Epid KLB','Nama Faskes','Kecamatan','Kabupaten/Kota','Provinsi','Nama Pasien','Identitas Pasien','','','','','','','','','','','','Data Klinis Campak','','','','','','','','','','','','','','','','','','','','','','','Data spesimen dan Laboratorium','','','','','','','Klasifikasi Final','Penyelidikan Epidomologi','','','','','','','','','','','','','','','','','','','','','',''],
				['','','','','','','','','','NIK','Nama Orang Tua','Jenis Kelamin','Tgl Lahir','Umur','','','Alamat','','','','','Tgl imunisasi campak terakhir','Jml imunisasi campak','Tgl mulai demam','Tgl mulai rash','Gejala Tanda Lain','','','','','','Komplikasi','','','','','','Tgl Laporan Diterima','Tgl pelacakan','Vitamin A','Keadaan akhir','Jenis Kasus','KLB ke-','Status kasus','Jenis Pemeriksaan','Jenis Sampel','Tgl ambil sampel','Hasil Lab','','','','','Riwayat Pengobatan','','','Riwayat kontak','','','Jumlah kasus tambahan di lokasi PE','','','','','','','','','','','','','','','Total kasus tambahan','Tgl penyelidikan','Pelaksana'],
				['','','','','','','','','','','','','','Thn','Bln','Hari','Jln','Kelurahan','Kecamatan','Kabupaten','Provinsi','','','','','Mata Merah','Tgl Kejadian','Batuk','Tgl Kejadian','Pilek','Tgl Kejadian','Diare','Pneumonia','Bronchopneumonia','OMA','Ensefalitis','Lain-lain','','','','','','','','','','','Serologi IgM Campak','Serologi IgM Rubella','Virologi IgM Campak','Virologi IgM Rubella','','Pengobatan pertama kali','Dimana pengobatan pertama kali','Obat yang diberikan','Apa dirumah ada sakit yang sama','Apa disekolah ada sakit yang sama','Apa penderita kurang gizi','Alamat tinggal sementara','Tgl PE','Jml kasus tambahan','Alamat tempat tinggal','Tgl PE','Jml kasus tambahan','Sekolah','Tgl PE','Jml kasus tambahan','Tempat Kerja','Tgl PE','Jml kasus tambahan','Lain-lain','Tgl PE','Jumlah kasus tambahan','','',''],
			];

			$ds = json_decode($dt);
			if (!empty($ds)) {
				foreach ($ds as $k => $v) {
					$post[$v->name] = $v->value;
				}
			}
			$between         = $district         = '';
			$unit            = (empty($post['unit']) ? '' : $post['unit']);
			$day_start       = (empty($post['day_start']) ? '' : $post['day_start']);
			$month_start     = (empty($post['month_start']) ? '' : $post['month_start']);
			$year_start      = (empty($post['year_start']) ? '' : $post['year_start']);
			$day_end         = (empty($post['day_end']) ? '' : $post['day_end']);
			$month_end       = (empty($post['month_end']) ? '' : $post['month_end']);
			$year_end        = (empty($post['year_end']) ? '' : $post['year_end']);
			$province_id     = (empty($post['province_id']) ? '' : $post['province_id']);
			$district_id     = (empty($post['district_id']) ? '' : $post['district_id']);
			$sub_district_id = (empty($post['sub_district_id']) ? '' : $post['sub_district_id']);
			$rs_id           = (empty($post['rs_id']) ? '' : $post['rs_id']);
			$puskesmas_id    = (empty($post['puskesmas_id']) ? '' : $post['puskesmas_id']);

			switch ($unit) {
				case 'all':
				$between = '';
				break;
				case 'year':
				$start   = $year_start;
				$end     = $year_end;
				$between = "AND CASE
				WHEN campak_tanggal_timbul_rash NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(campak_tanggal_timbul_rash)
				WHEN campak_tanggal_timbul_demam NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(campak_tanggal_timbul_demam)
				END BETWEEN '"     . $start . "' AND '" . $end . "' ";
				break;
				case 'month':
				$start   = $year_start . '-' . $month_start . '-1';
				$end     = $year_end . '-' . $month_end . '-' . date('t', mktime(0, 0, 0, $month_end, 1, $year_end));
				$between = "AND CASE
				WHEN campak_tanggal_timbul_rash NOT IN ('1970-01-01' , '0000-00-00') THEN DATE(campak_tanggal_timbul_rash)
				WHEN campak_tanggal_timbul_demam NOT IN ('1970-01-01' , '0000-00-00') THEN DATE(campak_tanggal_timbul_demam)
				END BETWEEN STR_TO_DATE('"     . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
				break;
				default:
				$start   = $year_start . '-' . $month_start . '-' . $day_start;
				$end     = $year_end . '-' . $month_end . '-' . $day_end;
				$between = "AND CASE
				WHEN campak_tanggal_timbul_rash NOT IN ('1970-01-01' , '0000-00-00') THEN DATE(campak_tanggal_timbul_rash)
				WHEN campak_tanggal_timbul_demam NOT IN ('1970-01-01' , '0000-00-00') THEN DATE(campak_tanggal_timbul_demam)
				END BETWEEN STR_TO_DATE('"     . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
				break;
			}

			if ($puskesmas_id) {
				$district = "AND code_faskes='" . $puskesmas_id . "'";
			} elseif ($sub_district_id) {
				$district = "AND pasien_id_kecamatan='" . $sub_district_id . "'";
			} elseif ($rs_id) {
				$district = "AND code_faskes='" . $rs_id . "' ";
			} elseif ($district_id) {
				$district = "AND pasien_id_kabupaten='" . $district_id . "' ";
			} elseif ($province_id) {
				$district = "AND pasien_id_provinsi='" . $province_id . "' ";
			}
			$data = Campak::exportpasien($between, $district);

			if (!empty($data)) {
				$rows = 3;
				$n    = 1;
				for ($i = 0; $i < sizeof($data); ++$i) {
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$n++:$n++;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->campak_no_epid:$data[$i]->campak_no_epid;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->campak_no_epid_lama:$data[$i]->campak_no_epid_lama;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->campak_no_epid_klb:$data[$i]->campak_no_epid_klb;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->campak_nama_puskesmas:$data[$i]->campak_nama_puskesmas;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->kecamatan_faskes:$data[$i]->kecamatan_faskes;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->kabupaten_faskes:$data[$i]->kabupaten_faskes;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->provinsi_faskes:$data[$i]->provinsi_faskes;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pasien_nama_anak:$data[$i]->pasien_nama_anak;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pasien_nik:$data[$i]->pasien_nik;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pasien_nama_ortu:$data[$i]->pasien_nama_ortu;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pasien_jenis_kelamin:$data[$i]->pasien_jenis_kelamin;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pasien_tgl_lahir:$data[$i]->pasien_tgl_lahir;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pasien_umur:$data[$i]->pasien_umur;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pasien_umur_bln:$data[$i]->pasien_umur_bln;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pasien_umur_hr:$data[$i]->pasien_umur_hr;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pasien_alamat:$data[$i]->pasien_alamat;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pasien_kelurahan:$data[$i]->pasien_kelurahan;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pasien_kecamatan:$data[$i]->pasien_kecamatan;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pasien_kabupaten:$data[$i]->pasien_kabupaten;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pasien_provinsi:$data[$i]->pasien_provinsi;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->campak_tanggal_imunisasi_terakhir:$data[$i]->campak_tanggal_imunisasi_terakhir;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->vaksin_campak_sebelum_sakit:$data[$i]->vaksin_campak_sebelum_sakit;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->campak_tanggal_timbul_demam:$data[$i]->campak_tanggal_timbul_demam;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->campak_tanggal_timbul_rash:$data[$i]->campak_tanggal_timbul_rash;
					$gejala = DB::table('gejala_campak')->WHERE('id_campak', $data[$i]->id_campak)->get();
					$mata_merah                  = $tgl_mata_merah                  = $batuk                  = $tgl_batuk                  = $pilek                  = $tgl_pilek                  = '';
					foreach ($gejala as $kg => $vg) {
						$mata_merah     = ($vg->id_daftar_gejala_campak == '1') ? 'Ya' : '-';
						$tgl_mata_merah = ($mata_merah != '-') ? Helper::formatDate($vg->tgl_mulai) : '';
						$batuk          = ($vg->id_daftar_gejala_campak == '2') ? 'Ya' : '-';
						$tgl_batuk      = ($batuk != '-') ? Helper::formatDate($vg->tgl_mulai) : '';
						$pilek          = ($vg->id_daftar_gejala_campak == '3') ? 'Ya' : '-';
						$tgl_pilek      = ($pilek != '-') ? Helper::formatDate($vg->tgl_mulai) : '';
					}
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$mata_merah:$mata_merah;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$tgl_mata_merah:$tgl_mata_merah;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$batuk:$batuk;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$tgl_batuk:$tgl_batuk;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$pilek:$pilek;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$tgl_pilek:$tgl_pilek;
					$komplikasi              = DB::table('komplikasi')->WHERE('id_campak', $data[$i]->id_campak)->get();
					$diare                   = $pneumonia                   = $bronchopneumonia                   = $oma                   = $ensefalitis                   = $lainlain                   = '';
					foreach ($komplikasi as $kk => $vk) {
						$diare            = ($vk->komplikasi == '0') ? 'Ya' : '-';
						$pneumonia        = ($vk->komplikasi == '1') ? 'Ya' : '-';
						$bronchopneumonia = ($vk->komplikasi == '2') ? 'Ya' : '-';
						$oma              = ($vk->komplikasi == '3') ? 'Ya' : '-';
						$ensefalitis      = ($vk->komplikasi == '4') ? 'Ya' : '-';
						$lainlain         = (!in_array($vk->komplikasi, array(0, 1, 2, 3, 4))) ? $vg->komplikasi : '-';
					}
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$diare:$diare;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$pneumonia:$pneumonia;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$bronchopneumonia:$bronchopneumonia;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$oma:$oma;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$ensefalitis:$ensefalitis;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$lainlain:$lainlain;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->campak_tanggal_laporan_diterima:$data[$i]->campak_tanggal_laporan_diterima;
					$excel_tmp[$rows + $i][] =($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null: $data[$i]->tanggal_pelacakan: $data[$i]->tanggal_pelacakan;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->vitamin_a_txt:$data[$i]->vitamin_a_txt;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->keadaan_akhir_txt:$data[$i]->keadaan_akhir_txt;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->jenis_kasus_txt:$data[$i]->jenis_kasus_txt;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->klb_ke:$data[$i]->klb_ke;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->status_kasus_txt:$data[$i]->status_kasus_txt;
					$jenis_pemeriksaan = '-';
					if ($data[$i]->jenis_pemeriksaan=='0') {
						$jenis_pemeriksaan = 'Serologi';
					}elseif($data[$i]->jenis_pemeriksaan=='1'){
						$jenis_pemeriksaan = 'Virologi';
					}
					$jenis_sampel = '-';
					if ($jenis_pemeriksaan=='Serologi' && $data[$i]->jenis_sampel=='0') {
						$jenis_sampel = 'Darah';
					}else if ($jenis_pemeriksaan=='Serologi' && $data[$i]->jenis_sampel=='1') {
						$jenis_sampel = 'Urin';
					}else if ($jenis_pemeriksaan=='Virologi' && $data[$i]->jenis_sampel=='0') {
						$jenis_sampel = 'Urin';
					}else if ($jenis_pemeriksaan=='Virologi' && $data[$i]->jenis_sampel=='1') {
						$jenis_sampel = 'Usap tenggorokan';
					}else if ($jenis_pemeriksaan=='Virologi' && $data[$i]->jenis_sampel=='2') {
						$jenis_sampel = 'Cairan mulut';
					}
					$excel_tmp[$rows + $i][] = $jenis_pemeriksaan;
					$excel_tmp[$rows + $i][] = $jenis_sampel; // jenis sampel
					$excel_tmp[$rows + $i][] = Helper::getDate($data[$i]->tgl_ambil_sampel); // tgl ambil sampel
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->hasil_serologi_igm_campak_txt:$data[$i]->hasil_serologi_igm_campak_txt;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->hasil_serologi_igm_rubella_txt:$data[$i]->hasil_serologi_igm_rubella_txt;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->hasil_virologi_igm_campak_txt:$data[$i]->hasil_virologi_igm_campak_txt;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->hasil_serologi_igm_rubella_txt:$data[$i]->hasil_serologi_igm_rubella_txt;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->campak_klasifikasi_final:$data[$i]->campak_klasifikasi_final;
					$excel_tmp[$rows + $i][] =($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null: $data[$i]->pe_waktu_pengobatan: $data[$i]->pe_waktu_pengobatan;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pe_tempat_pengobatan:$data[$i]->pe_tempat_pengobatan;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pe_obat_yang_diberikan:$data[$i]->pe_obat_yang_diberikan;

					$pe_dirumah_orang_sakit_sama = ($data[$i]->pe_dirumah_orang_sakit_sama == '1') ? 'Ya, Tanggal : ' . Helper::getDate($data[$i]->pe_kapan_sakit_dirumah) : '';
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$pe_dirumah_orang_sakit_sama:$pe_dirumah_orang_sakit_sama;
					$pe_disekolah_orang_sakit_sama = ($data[$i]->pe_disekolah_orang_sakit_sama == '1') ? 'Ya, Tanggal : ' . Helper::getDate($data[$i]->pe_kapan_sakit_disekolah) : '';
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$pe_disekolah_orang_sakit_sama:$pe_disekolah_orang_sakit_sama;
					$excel_tmp[$rows + $i][] =($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null: $data[$i]->pe_kekurangan_gizi: $data[$i]->pe_kekurangan_gizi;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pe_alamat_tinggal_sementara:$data[$i]->pe_alamat_tinggal_sementara;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pe_tgl_pe_dialamat_sementara:$data[$i]->pe_tgl_pe_dialamat_sementara;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pe_jumlah_kasus_dialamat_sementara:$data[$i]->pe_jumlah_kasus_dialamat_sementara;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pe_alamat_tempat_tinggal:$data[$i]->pe_alamat_tempat_tinggal;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pe_tgl_pe_alamat_tempat_tinggal:$data[$i]->pe_tgl_pe_alamat_tempat_tinggal;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pe_jumlah_kasus_dialamat_tempat_tinggal:$data[$i]->pe_jumlah_kasus_dialamat_tempat_tinggal;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pe_sekolah:$data[$i]->pe_sekolah;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pe_tgl_pe_sekolah:$data[$i]->pe_tgl_pe_sekolah;
					$excel_tmp[$rows + $i][] =($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null: $data[$i]->pe_jumlah_kasus_disekolah: $data[$i]->pe_jumlah_kasus_disekolah;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pe_tempat_kerja:$data[$i]->pe_tempat_kerja;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pe_tgl_pe_tempat_kerja:$data[$i]->pe_tgl_pe_tempat_kerja;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pe_jumlah_kasus_tempat_kerja:$data[$i]->pe_jumlah_kasus_tempat_kerja;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pe_lain_lain:$data[$i]->pe_lain_lain;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pe_tgl_pe_lain_lain:$data[$i]->pe_tgl_pe_lain_lain;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pe_jumlah_kasus_lain_lain:$data[$i]->pe_jumlah_kasus_lain_lain;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pe_jumlah_total_kasus_tambahan:$data[$i]->pe_jumlah_total_kasus_tambahan;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pe_tanggal_penyelidikan:$data[$i]->pe_tanggal_penyelidikan;
					$excel_tmp[$rows + $i][] = ($i!=0)?($data[$i]->id_campak==$data[$i-1]->id_campak)?null:$data[$i]->pe_pelaksana:$data[$i]->pe_pelaksana;
				}
			}
			Excel::create('FORM_EXPORT_CAMPAK_' . date('Y_m_d_h_i_s'), function ($excel) use ($excel_tmp) {
				$excel->sheet('Sheetname', function ($sheet) use ($excel_tmp) {
					$sheet->fromArray($excel_tmp);
					$sheet->cells('A2:BX4', function ($cells) {
						$cells->setAlignment('center');
						$cells->setValignment('center');
						$cells->setBackground('#FFFFAA');
						$cells->setFontSize(13);
						$cells->setFontWeight('bold');
					});
					$sheet->setBorder('A2:BX4', 'thin');
					$sheet->setFreeze('I5');
					$sheet->setAutoFilter('A4:BX4');
					$sheet->setAutoSize(true);
				});
			})->export('xls');
		}

		public function exportExcel($filetype, $dt = null) {
			$ds        = json_decode($dt);
			$penyakit  = Session::get('penyakit');
			$excel_tmp = array(
				array(
					'No',
					'No Epid Kasus / KLB',
					'Nama Penderita',
					'Nama Orangtua',
					'Alamat',
					'Puskesmas',
					'Kecamatan',
					'Kabupaten/Kota',
					'Provinsi',
					'Umur',
					'Umur/Bulan',
					'Jenis Kelamin',
					'Imunisasi Campak Sebelum Sakit',
					'Imunisasi Campak Sebelum Sakit',
					'Imunisasi Campak Sebelum Sakit',
					'Tanggal Imunisasi Campak Terakhir',
					'Tanggal Mulai',
					'Tanggal Mulai',
					'Tanggal Laporan Diterima',
					'Tanggal Pelacakan',
					'Tanggal Pengambilan Spesimen',
					'Tanggal Pengambilan Spesimen',
					'Hasil Lab',
					'Hasil Lab',
					'Hasil Lab',
					'Diberi Vitamin A',
					'Keadaan Akhir',
					'Klasifikasi Final / Campak (Lab)',
					'Klasifikasi Final / Campak (Epid)',
					'Klasifikasi Final / Campak (Klinis)',
					'Klasifikasi Final / Rubella',
					'Klasifikasi Final / Bukan Rubella / Campak (Discarded)',
				),
				array(
					'1',
					'2',
					'3',
					'4',
					'5',
					'6',
					'7',
					'8',
					'9',
					'Tahun',
					'Bulan',
					'Laki-laki/Perempuan',
					'Berapa Kali',
					'Tidak',
					'Tidak Tahu',
					'16',
					'Sakit',
					'Rash',
					'19',
					'20',
					'Darah',
					'Urin',
					'IgM Campak',
					'IgM Rubella',
					'Isolasi Virus',
					'26',
					'27',
					'Campak',
					'29',
					'30',
					'Rubella',
					'Bukan Campak / Rubella',
				),
				array(
					'1',
					'2',
					'3',
					'4',
					'5',
					'6',
					'7',
					'8',
					'9',
					'10',
					'11',
					'12',
					'13',
					'14',
					'15',
					'16',
					'17',
					'18',
					'19',
					'20',
					'21',
					'22',
					'23',
					'24',
					'25',
					'26',
					'27',
					'Lab',
					'Epid',
					'Klinis',
					'31',
					'32',
				),
				array(''),
			);

			foreach ($ds as $k => $v) {
				$post[$v->name] = $v->value;
			}
			$between         = $wilayah         = '';
			$unit            = (empty($post['unit']) ? '' : $post['unit']);
			$day_start       = (empty($post['day_start']) ? '' : $post['day_start']);
			$month_start     = (empty($post['month_start']) ? '' : $post['month_start']);
			$year_start      = (empty($post['year_start']) ? '' : $post['year_start']);
			$day_end         = (empty($post['day_end']) ? '' : $post['day_end']);
			$month_end       = (empty($post['month_end']) ? '' : $post['month_end']);
			$year_end        = (empty($post['year_end']) ? '' : $post['year_end']);
			$province_id     = (empty($post['province_id']) ? '' : $post['province_id']);
			$district_id     = (empty($post['district_id']) ? '' : $post['district_id']);
			$sub_district_id = (empty($post['sub_district_id']) ? '' : $post['sub_district_id']);
			$rs_id           = (empty($post['rs_id']) ? '' : $post['rs_id']);
			$puskesmas_id    = (empty($post['puskesmas_id']) ? '' : $post['puskesmas_id']);
			if (!empty($rs_id)) {
				$excel_tmp[0][5] = 'Rumah sakit';
			}else if(!empty($puskesmas_id)){
				$excel_tmp[0][5] = 'Puskesmas';
			}

			switch ($unit) {
				case 'all':
				$between = ' ';
				break;
				case 'year':
				$start   = $post['year_start'];
				$end     = $post['year_end'];
				$between = "AND CASE
				WHEN campak_tanggal_timbul_rash NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(campak_tanggal_timbul_rash)
				WHEN campak_tanggal_timbul_demam NOT IN ('1970-01-01' , '0000-00-00') THEN YEAR(campak_tanggal_timbul_demam)
				END BETWEEN '"     . $start . "' AND '" . $end . "' ";
				break;
				case 'month':
				$start   = $post['year_start'] . '-' . $post['month_start'] . '-1';
				$end     = $post['year_end'] . '-' . $post['month_end'] . '-' . date('t', mktime(0, 0, 0, $post['month_end'], 1, $post['year_end']));
				$between = "AND CASE
				WHEN campak_tanggal_timbul_rash NOT IN ('1970-01-01' , '0000-00-00') THEN DATE(campak_tanggal_timbul_rash)
				WHEN campak_tanggal_timbul_demam NOT IN ('1970-01-01' , '0000-00-00') THEN DATE(campak_tanggal_timbul_demam)
				END BETWEEN STR_TO_DATE('"     . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
				break;
				default:
				$start   = $post['year_start'] . '-' . $post['month_start'] . '-' . $post['day_start'];
				$end     = $post['year_end'] . '-' . $post['month_end'] . '-' . $post['day_end'];
				$between = "AND CASE
				WHEN campak_tanggal_timbul_rash NOT IN ('1970-01-01' , '0000-00-00') THEN DATE(campak_tanggal_timbul_rash)
				WHEN campak_tanggal_timbul_demam NOT IN ('1970-01-01' , '0000-00-00') THEN DATE(campak_tanggal_timbul_demam)
				END BETWEEN STR_TO_DATE('"     . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
				break;
			}

			if ($puskesmas_id) {
				$wilayah = "AND puskesmas_code_faskes='" . $puskesmas_id . "'";
			} elseif ($sub_district_id) {
				$wilayah = "AND pasien_id_kecamatan='" . $sub_district_id . "'";
			} elseif ($rs_id) {
				$wilayah = "AND rs_kode_faskes='" . $rs_id . "' ";
			} elseif ($district_id) {
				$wilayah = "AND pasien_id_kabupaten='" . $district_id . "' ";
			} elseif ($province_id) {
				$wilayah = "AND pasien_id_provinsi='" . $province_id . "' ";
			}

			$q = "
			SELECT
			a.campak_no_epid AS no_epid,
			a.pasien_nama_anak AS nama_anak,
			a.pasien_nama_ortu AS nama_ortu,
			a.alamat_new AS alamat,
			a.campak_nama_puskesmas AS puskesmas_name,
			a.pasien_kecamatan AS kecamatan,
			a.pasien_kabupaten AS kabupaten,
			a.pasien_provinsi AS provinsi,
			a.pasien_umur AS umur,
			a.pasien_umur_bln AS umur_bln,
			CASE a.pasien_jenis_kelamin
			WHEN '1' THEN 'Laki-laki'
			WHEN '2' THEN 'Perempuan'
			WHEN '3' THEN 'Tidak Jelas'
			ELSE 'Tidak di isi'
			END AS jenis_kelamin,
			CASE a.campak_vaksin_campak_sebelum_sakit
			WHEN '1' THEN '1x'
			WHEN '2' THEN '2x'
			WHEN '3' THEN '3x'
			WHEN '4' THEN '4x'
			WHEN '5' THEN '5x'
			WHEN '6' THEN '6x'
			ELSE ''
			END AS vaksin_campak_sebelum_sakit,
			CASE a.campak_vaksin_campak_sebelum_sakit
			WHEN '7' THEN 'V'
			END AS vaksin_campak_sebelum_sakit_tidak,
			CASE a.campak_vaksin_campak_sebelum_sakit
			WHEN '8' THEN 'V'
			END AS vaksin_campak_sebelum_sakit_tidak_tahu,
			a.campak_tanggal_imunisasi_terakhir AS tgl_imunisasi_campak_terakhir,
			a.campak_tanggal_timbul_demam AS tanggal_timbul_demam,
			a.campak_tanggal_timbul_rash AS tanggal_timbul_rash,
			a.campak_tanggal_laporan_diterima AS tanggal_laporan_diterima,
			a.campak_tanggal_pelacakan AS tanggal_pelacakan,
			a.hasil_serologi_igm_campak_txt AS hasil_igm_campak,
			a.hasil_serologi_igm_rubella_txt AS hasil_igm_rubella,
			a.hasil_virologi_igm_campak_txt AS hasil_isolasi_virus,
			CASE a.campak_vitamin_A
			WHEN '1' THEN 'Ya'
			WHEN '2' THEN 'Tidak'
			END AS vitamin_A,
			CASE a.campak_keadaan_akhir
			WHEN '1' THEN 'Hidup/sehat'
			WHEN '2' THEN 'Meninggal'
			END AS keadaan_akhir,
			IF(a.campak_klasifikasi_final='1','V', '') AS klasifikasi_final_lab,
			IF(a.campak_klasifikasi_final='2','V', '') AS klasifikasi_final_epid,
			IF(a.campak_klasifikasi_final='3','V', '') AS klasifikasi_final_klinis,
			IF(a.campak_klasifikasi_final='4','V', '') AS klasifikasi_final_rubella,
			IF(a.campak_klasifikasi_final='5','V', '') AS klasifikasi_final_bukan,
			b.jenis_pemeriksaan,
			b.jenis_sampel,
			b.tgl_ambil_sampel,
			b.hasil_igm_campak,
			b.hasil_igm_rubella,
			b.nama_penyakit,
			b.is_periksa
			FROM getdetailcampak AS a
			LEFT JOIN uji_spesimen AS b ON a.campak_id_campak=b.id_campak
			WHERE
			a.campak_deleted_at IS NULL
			" . $between . '
			' . $wilayah . '
			GROUP BY a.campak_id_campak
			';

			$data = DB::select($q);

			$ke_bawah = 3;
			for ($i = 0; $i < count($data); ++$i) {
				$excel_tmp[$ke_bawah + $i][0]  = 1 + $i;
				$excel_tmp[$ke_bawah + $i][1]  = $data[$i]->no_epid;
				$excel_tmp[$ke_bawah + $i][2]  = $data[$i]->nama_anak;
				$excel_tmp[$ke_bawah + $i][3]  = $data[$i]->nama_ortu;
				$excel_tmp[$ke_bawah + $i][4]  = $data[$i]->alamat;
				$excel_tmp[$ke_bawah + $i][5]  = $data[$i]->puskesmas_name;
				$excel_tmp[$ke_bawah + $i][6]  = $data[$i]->kecamatan;
				$excel_tmp[$ke_bawah + $i][7]  = $data[$i]->kabupaten;
				$excel_tmp[$ke_bawah + $i][8]  = $data[$i]->provinsi;
				$excel_tmp[$ke_bawah + $i][9]  = $data[$i]->umur;
				$excel_tmp[$ke_bawah + $i][10] = $data[$i]->umur_bln;
				$excel_tmp[$ke_bawah + $i][11] = $data[$i]->jenis_kelamin;
				$excel_tmp[$ke_bawah + $i][12] = $data[$i]->vaksin_campak_sebelum_sakit;
				$excel_tmp[$ke_bawah + $i][13] = $data[$i]->vaksin_campak_sebelum_sakit_tidak;
				$excel_tmp[$ke_bawah + $i][14] = $data[$i]->vaksin_campak_sebelum_sakit_tidak_tahu;
				$excel_tmp[$ke_bawah + $i][15] = Helper::getDate($data[$i]->tgl_imunisasi_campak_terakhir);
				$excel_tmp[$ke_bawah + $i][16] = Helper::getDate($data[$i]->tanggal_timbul_demam);
				$excel_tmp[$ke_bawah + $i][17] = Helper::getDate($data[$i]->tanggal_timbul_rash);
				$excel_tmp[$ke_bawah + $i][18] = Helper::getDate($data[$i]->tanggal_laporan_diterima);
				$excel_tmp[$ke_bawah + $i][19] = Helper::getDate($data[$i]->tanggal_pelacakan);
				$excel_tmp[$ke_bawah + $i][20] = ($data[$i]->jenis_sampel == '0') ? Helper::getDate($data[$i]->tgl_ambil_sampel) : '';
				$excel_tmp[$ke_bawah + $i][21] = ($data[$i]->jenis_sampel == '1') ? Helper::getDate($data[$i]->tgl_ambil_sampel) : '';
				$excel_tmp[$ke_bawah + $i][22] = $data[$i]->hasil_igm_campak;
				$excel_tmp[$ke_bawah + $i][23] = $data[$i]->hasil_igm_rubella;
				$excel_tmp[$ke_bawah + $i][24] = $data[$i]->hasil_isolasi_virus;
				$excel_tmp[$ke_bawah + $i][25] = $data[$i]->vitamin_A;
				$excel_tmp[$ke_bawah + $i][26] = $data[$i]->keadaan_akhir;
				$excel_tmp[$ke_bawah + $i][27] = $data[$i]->klasifikasi_final_lab;
				$excel_tmp[$ke_bawah + $i][28] = $data[$i]->klasifikasi_final_epid;
				$excel_tmp[$ke_bawah + $i][29] = $data[$i]->klasifikasi_final_klinis;
				$excel_tmp[$ke_bawah + $i][30] = $data[$i]->klasifikasi_final_rubella;
				$excel_tmp[$ke_bawah + $i][31] = $data[$i]->klasifikasi_final_bukan;
			}

			Excel::create('FORM_C1_CAMPAK_' . date('Y_m_d_h_i_s'), function ($excel) use ($excel_tmp) {
				$excel->sheet('Sheetname', function ($sheet) use ($excel_tmp) {
					$sheet->fromArray($excel_tmp);
								//$sheet->mergeCells('A1:AF1');

					$sheet->mergeCells('A2:A4');
					$sheet->mergeCells('B2:B4');
					$sheet->mergeCells('C2:C4');
					$sheet->mergeCells('D2:D4');
					$sheet->mergeCells('E2:E4');
					$sheet->mergeCells('F2:F4');
					$sheet->mergeCells('G2:G4');
					$sheet->mergeCells('H2:H4');
					$sheet->mergeCells('I2:I4');
					$sheet->mergeCells('J3:J4');
					$sheet->mergeCells('K3:K4');
					$sheet->mergeCells('L3:L4');
					$sheet->mergeCells('M3:M4');
					$sheet->mergeCells('N3:N4');
					$sheet->mergeCells('O3:O4');
					$sheet->mergeCells('P2:P4');
					$sheet->mergeCells('Q3:Q4');
					$sheet->mergeCells('R3:R4');
					$sheet->mergeCells('S2:S4');
					$sheet->mergeCells('T2:T4');
					$sheet->mergeCells('U3:U4');
					$sheet->mergeCells('V3:V4');
					$sheet->mergeCells('W3:W4');
					$sheet->mergeCells('X3:X4');
					$sheet->mergeCells('Y3:Y4');
					$sheet->mergeCells('Z2:Z4');
					$sheet->mergeCells('Aa2:AA4');
								//$sheet->mergeCells('AB2:AB4');
								//$sheet->mergeCells('AC2:AC4');
								//$sheet->mergeCells('AD2:AD4');
					$sheet->mergeCells('AE3:AE4');
					$sheet->mergeCells('AF3:AF4');

					$sheet->mergeCells('J2:K2');
					$sheet->mergeCells('M2:O2');
					$sheet->mergeCells('Q2:R2');
					$sheet->mergeCells('U2:V2');
					$sheet->mergeCells('W2:Y2');
					$sheet->mergeCells('AB2:AF2');
					$sheet->mergeCells('AB3:AD3');

					$sheet->cells('A2:AF4', function ($cells) {
						$cells->setAlignment('center');
						$cells->setValignment('center');
						$cells->setBackground('#FFFFAA');
						$cells->setFontSize(13);
						$cells->setFontWeight('bold');
					});

					$sheet->cells('N5:O1000', function ($cells) {
						$cells->setAlignment('center');
						$cells->setValignment('center');
					});

					$sheet->cells('AB5:AF1000', function ($cells) {
						$cells->setAlignment('center');
						$cells->setValignment('center');
					});

					$sheet->setWidth('A', 5);
					$sheet->setWidth('B', 25);
					$sheet->setWidth('C', 22);
					$sheet->setWidth('D', 22);
					$sheet->setWidth('E', 50);
					$sheet->setWidth('F', 25);
					$sheet->setWidth('G', 20);
					$sheet->setWidth('H', 20);
					$sheet->setWidth('I', 30);
					$sheet->setWidth('J', 10);
					$sheet->setWidth('K', 10);
					$sheet->setWidth('L', 22);
					$sheet->setWidth('M', 14);
					$sheet->setWidth('N', 14);
					$sheet->setWidth('O', 14);
					$sheet->setWidth('P', 37);
					$sheet->setWidth('Q', 11);
					$sheet->setWidth('R', 11);
					$sheet->setWidth('S', 27);
					$sheet->setWidth('T', 20);
					$sheet->setWidth('U', 18);
					$sheet->setWidth('V', 18);
					$sheet->setWidth('W', 13);
					$sheet->setWidth('X', 13);
					$sheet->setWidth('Y', 13);
					$sheet->setWidth('Z', 18);
					$sheet->setWidth('AA', 30);
					$sheet->setWidth('AB', 10);
					$sheet->setWidth('AC', 10);
					$sheet->setWidth('AD', 10);
					$sheet->setWidth('AE', 10);
					$sheet->setWidth('AF', 25);

					$sheet->setBorder('A2:AF4', 'thin');

					$sheet->setFreeze('D5');
					$sheet->setAutoFilter('A4:AF4');
				});

			})->export('xls');
		}

		public function filterDaftarKasus() {
			$unit            = Input::get('unit');
			$day_start       = Input::get('day_start');
			$month_start     = Input::get('month_start');
			$year_start      = Input::get('year_start');
			$day_end         = Input::get('day_end');
			$month_end       = Input::get('month_end');
			$year_end        = Input::get('year_end');
			$province_id     = Input::get('province_id');
			$district_id     = Input::get('district_id');
			$sub_district_id = Input::get('sub_district_id');
			$village_id      = Input::get('village_id');

			switch ($unit) {
				case 'all':
				$between = '';
				break;
				case 'year':
				$start   = $year_start;
				$end     = $year_end;
				$between = " AND YEAR(tanggal_periksa) BETWEEN '" . $start . "' AND '" . $end . "' ";
				break;
				case 'month':
				$start   = $year_start . '-' . $month_start . '-1';
				$end     = $year_end . '-' . $month_end . '-' . date('t', mktime(0, 0, 0, $month_end, 1, $year_end));
				$between = " AND DATE(tanggal_periksa) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
				break;
				default:
				$start   = $year_start . '-' . $month_start . '-' . $day_start;
				$end     = $year_end . '-' . $month_end . '-' . $day_end;
				$between = " AND DATE(tanggal_periksa) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
				break;
			}

			$district = '';
			if ($village_id) {
				$district = "AND id_kelurahan='" . $village_id . "' ";
			} elseif ($sub_district_id) {
				$district = "AND id_kecamatan='" . $sub_district_id . "' ";
			} elseif ($district_id) {
				$district = "AND id_kabupaten='" . $district_id . "' ";
			} elseif ($province_id) {
				$district = "AND id_provinsi='" . $province_id . "' ";
			}
			$data = Campak::pasiencampak($between, $district);

			return View::make('pemeriksaan.campak.index', compact('data'));
		}

		public function importFromExcel() {
			return View::make('pemeriksaan.campak.import_campak');
		}

		public function extractExcel() {
				// return $_GET;
			$file = Input::file('excel');

			return $file;
			$content = file_get_contents($file);
			echo '<pre>';
			print_r($content);
			echo '</pre>';
			die();

			return $content;
		}
	}
