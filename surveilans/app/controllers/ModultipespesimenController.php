<?php
class ModultipespesimenController extends BaseController {
	
	public function getIndex()
	{
		return View::make('master_data.modultipespesimen');
	}
	
	public function getList()
	{
		$modultipespesimen = ModultipespesimenModel::getListModultipespesimen();
		$current_page = Input::get('page');
		$current_search = Input::get('search_name');		
		return View::make('master_data.modultipespesimen_list',compact('modultipespesimen','current_page','current_search'));
	}
	
	public function getListGet()
	{
		$modultipespesimen = ModultipespesimenModel::getListModultipespesimen();
		$current_page = Input::get('page');
		$current_search = Input::get('search_name');		
		return View::make('master_data.modultipespesimen_list',compact('modultipespesimen','current_page','current_search'));
	}
	
	public function ModultipespesimenProcessForm(){
		$ret = array();
        if(Input::get('saved_id')) {
			$ret['command'] = "updating data";
			if(ModultipespesimenModel::DoUpdateData()) {
				$ret['msg'] = "Sukses Update Data";
				$ret['status'] = "success";
			} else {
				$ret['msg'] = "Gagal Update Data";
				$ret['status'] = "error";
			}
        } else {
			$ret['command'] = "adding data";
			$last_id = ModultipespesimenModel::DoAddData();
			if($last_id > 0) {
				$ret['msg'] = "Sukses Simpan Data";
				$ret['status'] = "success";
			} else {
				$ret['msg'] = "Gagal Simpan Data";
				$ret['status'] = "error";
			}
        }
		
		echo json_encode($ret);
		
	}
	
	public function ModultipespesimenGetDataById(){
		$provinsi = ModultipespesimenModel::getModultipespesimen();
		echo json_encode($provinsi);
	}
	
	public function ModultipespesimenDeleteList(){
		ModultipespesimenModel::DoDeleteModultipespesimen();
		$ret['msg'] = "Sukses Delete Data";
		$ret['status'] = "success";
		echo json_encode($ret);
	}
	
}
?>
