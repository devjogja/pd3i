<?php

class PasienController extends \BaseController {

	
	public function __construct()
	{
		//filter
		$this->beforeFilter('auth');
	}
	/**
	 * Display a listing of Pasien
	 *
	 * @return Response
	 */
	public function index()
	{
		$Pasien = Pasien::all();

		return View::make('Pasien.index', compact('Pasien'));
	}

	/**
	 * Show the form for creating a new pasien
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('Pasien.create');
	}

	/**
	 * Store a newly created pasien in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Pasien::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Pasien::create($data);

		return Redirect::route('Pasien.index');
	}

	/**
	 * Display the specified pasien.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$pasien = Pasien::findOrFail($id);

		return View::make('Pasien.show', compact('pasien'));
	}

	/**
	 * Show the form for editing the specified pasien.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$pasien = Pasien::find($id);

		return View::make('Pasien.edit', compact('pasien'));
	}

	/**
	 * Update the specified pasien in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$pasien = Pasien::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Pasien::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$pasien->update($data);

		return Redirect::route('Pasien.index');
	}

	function getUmur(){
		$tgl_lahir = strtotime(Input::get('tgl_lahir'));
		$tgl_periksa = strtotime(Input::get('tgl_periksa'));
		$tgl_sakit = strtotime(Input::get('tgl_sakit'));
		$convdate = ($tgl_periksa)?$tgl_periksa:$tgl_sakit;
		if(!empty($tgl_lahir) && !empty($convdate)){
			$diff = abs($convdate-$tgl_lahir);
			$years = floor($diff / (365*60*60*24));
			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

			$sent = [
				'years'=>$years,
				'month'=>$months,
				'day'=>$days
			];
			return $sent;
		}
	}

	function getTglLahir() {
		if (Input::get('tgl_periksa')) {
			$pecah = explode("-",Input::get('tgl_periksa'));
			if(Input::get('tanggal_lahir')=='' && checkdate($pecah[1], $pecah[0], $pecah[2])) {
				$thn = Input::get('tgl_tahun');
				$bln = Input::get('tgl_bulan');
				$hr  = Input::get('tgl_hari');
				$tgl_mulai_sakit = $this->changeDate(Input::get('tgl_periksa'));
				Difteri::get_tgl_lahir($thn,$bln,$hr,$tgl_mulai_sakit);
			} elseif (Input::get('tanggal_lahir') && checkdate($pecah[1], $pecah[0], $pecah[2])) {
				$thn = Input::get('tgl_tahun');
				$bln = Input::get('tgl_bulan');
				$hr  = Input::get('tgl_hari');
				$tgl_mulai_sakit = $this->changeDate(Input::get('tgl_periksa'));
				$tgl1  = Difteri::get_tgl_lahir($thn,$bln,$hr,$tgl_mulai_sakit);
				$pecah = explode("-",Input::get('tgl_periksa'));
				$m = $pecah[1];
				$d = $pecah[0];
				$y = $pecah[2];
				$tanggal_lahir = $this->changeDate($tgl1);
				list($thn_skrg, $bln_skrg, $tgl_skrg) = explode('-',$tanggal_lahir);
				// kelahiran dijadikan dalam format tanggal semua
				$lahir = $tgl_skrg+($bln_skrg*30)+($thn_skrg*365);
				// sekarang dijadikan dalam format tanggal semua
				$now = $d+($m*30)+($y*365);
				// dari format tanggal jadikan tahun, bulan, hari
				$tahun = ($now-$lahir)/365;
			   	$bulan = (($now-$lahir)%365)/30;
		   		$hari  = (($now-$lahir)%365)%30;
				$tgl_tahun = floor($tahun);
				$tgl_bulan = floor($bulan);
				$tgl_hari  = floor($hari);
				$tgl = 1;
				//return Response::json(compact('tgl','tgl1','tgl_tahun','tgl_bulan','tgl_hari'));
			}
		}
	}

	public function cekPasien()
	{
		$nama_anak = Input::get('nama_anak');
		$nik = Input::get('nik');
		$nama_ortu = Input::get('nama_ortu');
		$id_kelurahan = Input::get('id_kelurahan');
		$cekPasien = DB::SELECT("
				SELECT count(*) AS jml
				FROM pasien AS a
				JOIN hasil_uji_lab_campak AS b ON a.id_pasien=b.id_pasien
				JOIN campak AS c ON b.id_campak=c.id_campak
				WHERE
					c.status_at='1' AND a.nama_anak='$nama_anak' AND a.nama_ortu='$nama_ortu' AND a.id_kelurahan='$id_kelurahan'
			");
		$status=$message='';
		if ($cekPasien[0]->jml > 0) {
			$status = '1';
			$message = 'Data pasien sudah pernah di inputkan.';
		}
		$sent = [
			'status'=>$status,
			'message'=>$message
		];
		return Response::json(compact('sent'));
	}

	/**
	 * Remove the specified pasien from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Pasien::destroy($id);

		return Redirect::route('Pasien.index');
	}

	//cari pasien
	public function postCari() 
	{
		$nik = Input::get('nik');
		$hsl = Pasien::cari($nik);
		if($hsl) 
		{
			/*foreach ($hsl as $value) {
				$nik = $value->nik;
			
			}*/
			//return Redirect::to('laboratorium');
		}
		else 
		{
			echo "<div class='form-group'>
				<label class='col-sm-2 control-label'>Masukan NIK</label>         
                <div class='col-sm-8'>
                <input class='form-control' placeholder='search pasien' id='input-search' onchange='showPasien()'>
                <div class='searchable-container' id='hide'>
                <div class='info-block block-info clearfix'>
                    <h5>Pasien dengan NIK <b> ".$nik." </b> tidak ditemukan</h5>
                </div>
                </div>
            </div>
            </div>";
		}

		//return $html;
	}

}
