<?php
class BerandaController extends BaseController {

	/**
     * The layout that should be used for responses.
     */
    // protected $layout = 'layouts.master';



	/**
	 * [getIndex description]
	 *
	 * @return [type] [description]
	 */
	public function getIndex()
	{
		if(Sentry::check()){
			if(!Session::get('type')){
				return Redirect::to('pilih_profil');
			}else{
				return View::make('beranda.index1');
			}
		} else {
			return Redirect::to('/');
		}
	}

	/**
	 * [getJsInitMaps description]
	 *
	 * @return [type] [description]
	 */
	public function getJsInitMaps()
	{
		return View::make('beranda.js_init_maps');
	}


	/**
	 * [getJsGrafikMaps description]
	 *
	 * @return [type] [description]
	 */
	public function getJsGrafikMaps()
	{
		return View::make('beranda.js_grafik_maps');
	}


	/**
	 * [getJsGrafikMapsAlert description]
	 *
	 * @return [type] [description]
	 */
	public function getJsGrafikMapsAlert()
	{
		return View::make('beranda.js_grafik_maps_alert');
	}


	/**
	 * [getJsGrafikKlasifikasiFinal description]
	 *
	 * @return [type] [description]
	 */
	public function getJsGrafikKlasifikasiFinal()
	{
		return View::make('beranda.js_grafik_klasifikasi_final');
	}
	
	public function getEditProfilUser(){

		return View::make('beranda.edit_profil_user');
	}
	
	public function getEditProfilInstansi(){
		$type_profil = Session::get('type_profil');
		if($type_profil=='puskesmas')return View::make('beranda.edit_profil_puskesmas');
		else if($type_profil=='rs')return View::make('beranda.edit_profil_rs');
		else if($type_profil=='kabupaten')return View::make('beranda.edit_profil_kabupaten');
		else if($type_profil=='provinsi')return View::make('beranda.edit_profil_provinsi');
		else if($type_profil=='kemenkes')return View::make('beranda.edit_profil_kemenkes');
		else if($type_profil=='laboratorium')return View::make('beranda.edit_profil_laboratorium');
	}
	
	public function getDaftarPetugasSurveilans(){
		return View::make('beranda.daftar_petugas_surveilans');
	}
	
	public function postSaveEditProfilUser(){
		$data = array();
		$data['status']  = "?";
		$data['message'] = "?";
		$data['address'] = "?";

		if(!Input::get('first_name')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register User <br> Suku kata pertama pada nama anda masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('last_name')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register User <br> Suku terakhir pada nama anda masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('instansi')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register User <br> Nama Instansi masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('jabatan')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register User <br> Jabatan masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('email')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register User <br> Email masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('password')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register User <br> Password masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('password2')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register User <br> Ulangi Password masih Kosong";
			$data['address'] = "";
		} else if(Input::get('password2')!=Input::get('password')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register User <br> Kedua password harus sama";
			$data['address'] = "";
		} else	{
			try {
				
				/*
				DB::table('users')->insertGetId(
					array(
						'first_name'=>Input::get('first_name'),
						'last_name'=>Input::get('last_name'),
						'password'=>Hash::make(Input::get('password')),
						'email'=>Input::get('email'),
						'instansi'=>Input::get('instansi'),
						'jabatan'=>Input::get('jabatan'),
						'jk'=>Input::get('jk'),
						'created_at'=>date('Y-m-d H:i:s'),
						'activated'=>'1'
					)
				);
				*/
				
				DB::update("UPDATE users SET 
					first_name = '".Input::get('first_name')."', 
					last_name = '".Input::get('last_name')."', 
					password = '".Input::get('password')."',
					email = '".Input::get('email')."',
					instansi = '".Input::get('instansi')."',
					jabatan = '".Input::get('jabatan')."',
					password = '".Hash::make(Input::get('password'))."',
					no_telp = '".Input::get('no_telp')."',
					jk = '".Input::get('jk')."'
					where id = ?", 
					array(
						Sentry::getUser()->id
					)
				);
				
				$data['status']  = "success";
				$data['message'] = "Sukses Edit Profil User";
				$data['address'] = URL::to('/');
			}
			catch (Exception $e){
				//kembalikan ke halaman login
				//return Redirect::back();
				$data['status']  = "error";
				$data['message'] = "Gagal Edit User, Email Sudah Ada";
				$data['address'] = "";
			}
		}
		return $data;
	}
	
	public function postSaveEditProfilPuskesmas(){
		$data = array();
		$data['status']  = "?";
		$data['message'] = "?";
		$data['address'] = "?";

		if(!Input::get('puskesmas_code')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Puskesmas <br> Kode Puskesmas masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('alamat')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Puskesmas <br> Alamat masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('penanggungjawab_1')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Puskesmas <br> Penanggungjawab 1 masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('penanggungjawab_2')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Puskesmas <br> Penanggungjawab 2 masih Kosong";
			$data['address'] = "";
		} else {
			try {
				/*
				$id = DB::table('profil_puskesmas')->insertGetId(
				array(
					'kode_faskes'=>Input::get('puskesmas_code'),
					'alamat'=>Input::get('alamat'),
					'konfirmasi'=>Input::get('kode_konfirmasi'),
					'penanggungjawab_1'=>Input::get('penanggungjawab_1'),
					'penanggungjawab_2'=>Input::get('penanggungjawab_2'),
					'created_dttm'=>date('Y-m-d H:i:s'),
					'created_by'=>Sentry::getUser()->id
					)
				);
				*/
				DB::update("UPDATE profil_puskesmas SET 
					alamat = '".Input::get('alamat')."', 
					penanggungjawab_1 = '".Input::get('penanggungjawab_1')."', 
					penanggungjawab_2 = '".Input::get('penanggungjawab_2')."'
					where id = ?", 
					array(
						Session::get('id_profil')
					)
				);
				
				$data['status']  = "success";
				$data['message'] = "Sukses Update Profil Puskesmas";
				$data['address'] = URL::to('dashboard');
			} catch (Exception $e){
				//kembalikan ke halaman login
				//return Redirect::back();
				$data['status']  = "error";
				$data['message'] = "Gagal Update Profil Puskesmas !";
				$data['address'] = "";
			}
				
		}

		return $data;
	}
	
	public function postSaveEditProfilRS(){
		$data = array();
		$data['status']  = "?";
		$data['message'] = "?";
		$data['address'] = "?";

		if(!Input::get('puskesmas_code')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil RS <br> Kode RS masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('alamat')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil RS <br> Alamat masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('penanggungjawab_1')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil RS <br> Penanggungjawab 1 masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('penanggungjawab_2')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil RS <br> Penanggungjawab 2 masih Kosong";
			$data['address'] = "";
		} else {
			try {
				/*
				$id = DB::table('profil_puskesmas')->insertGetId(
				array(
					'kode_faskes'=>Input::get('puskesmas_code'),
					'alamat'=>Input::get('alamat'),
					'konfirmasi'=>Input::get('kode_konfirmasi'),
					'penanggungjawab_1'=>Input::get('penanggungjawab_1'),
					'penanggungjawab_2'=>Input::get('penanggungjawab_2'),
					'created_dttm'=>date('Y-m-d H:i:s'),
					'created_by'=>Sentry::getUser()->id
					)
				);
				*/
				DB::update("UPDATE profil_rs SET 
					alamat = '".Input::get('alamat')."', 
					penanggungjawab_1 = '".Input::get('penanggungjawab_1')."', 
					penanggungjawab_2 = '".Input::get('penanggungjawab_2')."'
					where id = ?", 
					array(
						Session::get('id_profil')
					)
				);
				
				$data['status']  = "success";
				$data['message'] = "Sukses Update Profil RS";
				$data['address'] = URL::to('dashboard');
			} catch (Exception $e){
				//kembalikan ke halaman login
				//return Redirect::back();
				$data['status']  = "error";
				$data['message'] = "Gagal Update Profil RS !";
				$data['address'] = "";
			}
				
		}

		return $data;
	}


	public function postSaveEditProfilLaboratorium(){
		$data = array();
		$data['status']  = "?";
		$data['message'] = "?";
		$data['address'] = "?";

		if(!Input::get('laboratorium_code')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Laboratorium <br> Kode Laboratorium masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('alamat')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Laboratorium <br> Alamat masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('penanggungjawab_1')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Laboratorium <br> Penanggungjawab 1 masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('penanggungjawab_2')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Laboratorium <br> Penanggungjawab 2 masih Kosong";
			$data['address'] = "";
		} else {				
			try {
				/*
				$id = DB::table('profil_laboratorium')->insertGetId(
				array(
					'kode_lab'=>Input::get('laboratorium_code'),
					'alamat'=>Input::get('alamat'),
					'konfirmasi'=>Input::get('kode_konfirmasi'),
					'penanggungjawab_1'=>Input::get('penanggungjawab_1'),
					'penanggungjawab_2'=>Input::get('penanggungjawab_2'),
					'created_dttm'=>date('Y-m-d H:i:s'),
					'created_by'=>Sentry::getUser()->id
					)
				);
				*/
				DB::update("UPDATE profil_laboratorium SET 
					alamat = '".Input::get('alamat')."', 
					penanggungjawab_1 = '".Input::get('penanggungjawab_1')."', 
					penanggungjawab_2 = '".Input::get('penanggungjawab_2')."'
					where id = ?", 
					array(
						Session::get('id_profil')
					)
				);
				$data['status']  = "success";
				$data['message'] = "Sukses Update Profil Laboratorium";
				$data['address'] = URL::to('dashboard');
			} catch (Exception $e){
				//kembalikan ke halaman login
				//return Redirect::back();
				$data['status']  = "error";
				$data['message'] = "Gagal Update Profil Laboratorium !";
				$data['address'] = "";
			}
		}
		return $data;
	}

	public function postSaveEditProfilKabupaten(){
		$data = array();
		$data['status']  = "?";
		$data['message'] = "?";
		$data['address'] = "?";
		if(!Input::get('district_id')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Dinas Kesehatan Kabupaten <br> Kode Dinas Kesehatan Kabupaten masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('alamat')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Dinas Kesehatan Kabupaten <br> Alamat masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('penanggungjawab_1')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Dinas Kesehatan Kabupaten <br> Penanggungjawab 1 masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('penanggungjawab_2')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Puskesmas <br> Penanggungjawab 2 masih Kosong";
			$data['address'] = "";
		} else {	
			try {
				/*
				$id = DB::table('profil_dinkes_kabupaten')->insertGetId(
				array(
					'kode_kabupaten'=>Input::get('district_id'),
					'alamat'=>Input::get('alamat'),
					'penanggungjawab_1'=>Input::get('penanggungjawab_1'),
					'penanggungjawab_2'=>Input::get('penanggungjawab_2'),
					'created_dttm'=>date('Y-m-d H:i:s'),
					'created_by'=>Sentry::getUser()->id
					)
				);
				*/
				
				DB::update("UPDATE profil_dinkes_kabupaten SET 
					alamat = '".Input::get('alamat')."', 
					penanggungjawab_1 = '".Input::get('penanggungjawab_1')."', 
					penanggungjawab_2 = '".Input::get('penanggungjawab_2')."'
					where id = ?", 
					array(
						Session::get('id_profil')
					)
				);
				
				$data['status']  = "success";
				$data['message'] = "Sukses Update Profil Dinas Kesehatan Kabupaten";
				$data['address'] = URL::to('dashboard');
			}
			catch (Exception $e){
				//kembalikan ke halaman login
				//return Redirect::back();
				$data['status']  = "error";
				$data['message'] = "Gagal Update Profil Dinas Kesehatan Kabupaten";
				$data['address'] = "";
			}
		}

		return $data;
	}

	public function postSaveEditProfilProvinsi(){
		$data = array();
		$data['status']  = "?";
		$data['message'] = "?";
		$data['address'] = "?";

		if(!Input::get('province_id')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Dinas Kesehatan Provinsi <br> Kode Provinsi masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('alamat')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Dinas Kesehatan Provinsi <br> Alamat masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('penanggungjawab_1')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Dinas Kesehatan Provinsi <br> Penanggungjawab 1 masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('penanggungjawab_2')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Dinas Kesehatan Provinsi <br> Penanggungjawab 2 masih Kosong";
			$data['address'] = "";
		} else {			
			try {
				/*
				DB::table('profil_dinkes_provinsi')->insertGetId(
				array(
					'kode_provinsi'=>Input::get('province_id'),
					'alamat'=>Input::get('alamat'),
					'penanggungjawab_1'=>Input::get('penanggungjawab_1'),
					'penanggungjawab_2'=>Input::get('penanggungjawab_2'),
					'created_dttm'=>date('Y-m-d H:i:s'),
					'created_by'=>Sentry::getUser()->id
					)
				);
				*/
				
				DB::update("UPDATE profil_dinkes_provinsi SET 
					alamat = '".Input::get('alamat')."', 
					penanggungjawab_1 = '".Input::get('penanggungjawab_1')."', 
					penanggungjawab_2 = '".Input::get('penanggungjawab_2')."'
					where id = ?", 
					array(
						Session::get('id_profil')
					)
				);
				
				
				$data['status']  = "success";
				$data['message'] = "Sukses Update Profil Dinas Kesehatan Provinsi";
				$data['address'] = URL::to('dashboard');
			}
			catch (Exception $e){
				//kembalikan ke halaman login
				//return Redirect::back();
				$data['status']  = "error";
				$data['message'] = "Gagal Update Profil Dinas Kesehatan Provinsi";
				$data['address'] = "";
			}
		}
		return $data;
	}

	public function postSaveEditProfilPusat(){
		$data = array();
		$data['status']  = "?";
		$data['message'] = "?";
		$data['address'] = "?";

		if(!Input::get('alamat')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Kemenkes <br> Alamat masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('penanggungjawab_1')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Kemenkes <br> Penanggungjawab 1 masih Kosong";
			$data['address'] = "";
		} else if(!Input::get('penanggungjawab_2')) {
			$data['status']  = "error";
			$data['message'] = "Gagal Register Profil Kemenkes <br> Penanggungjawab 2 masih Kosong";
			$data['address'] = "";
		} else {
			try {
				/*
				DB::table('profil_kemenkes')->insertGetId(
					array(
						'alamat'=>Input::get('alamat'),
						'penanggungjawab_1'=>Input::get('penanggungjawab_1'),
						'penanggungjawab_2'=>Input::get('penanggungjawab_2'),
						'created_dttm'=>date('Y-m-d H:i:s'),
						'created_by'=>Sentry::getUser()->id
					)
				);
				*/
				
				DB::update("UPDATE profil_kemenkes SET 
					alamat = '".Input::get('alamat')."', 
					penanggungjawab_1 = '".Input::get('penanggungjawab_1')."', 
					penanggungjawab_2 = '".Input::get('penanggungjawab_2')."'
					where id = ?", 
					array(
						Session::get('id_profil')
					)
				);
				
				$data['status']  = "success";
				$data['message'] = "Sukses Update Profil Kemenkes";
				$data['address'] = URL::to('dashboard');
			}
			catch (Exception $e){
				//kembalikan ke halaman login
				//return Redirect::back();
				$data['status']  = "error";
				$data['message'] = "Gagal Update Profil Kemenkes";
				$data['address'] = "";
			}	
		}

		return $data;
	}
	
	public function getResetPassword($id){
		$new_pass = substr(md5(time()),1,5);;
		$new_enc_pass = Hash::make($new_pass);
		DB::update("UPDATE users SET 
				password = '".$new_enc_pass."', 
				saved_pass = '".$new_pass."'
				where id = '".$id."'"
		);
		return Redirect::to('daftar_petugas_surveilans');
	}

	public function getDeleteUser($id)
	{
		DB::table('users')->WHERE('id',$id)->delete();
		return Redirect::to('daftar_petugas_surveilans');
	}

	public function getListUser()
	{
		$data=DB::table('users')->get();
	 	$sent = "<table border=1>";
	 	$sent .= "<tr>
	 				<td>Email</td>
	 				<td>Name</td>
	 				<td>Instansi</td>
	 				<td>Action</td>
	 			</tr>";
		foreach ($data as $key => $val) {
		 	$sent .= "<tr>";
		 	$sent .= "<td>$val->email</td>";
		 	$sent .= "<td>$val->first_name $val->last_name</td>";
		 	$sent .= "<td>$val->instansi</td>";
		 	$sent .= "<td><a href=''>Delete</a></td>";
		 	$sent .= "</tr>";
		}
	 	$sent .= "</table>";
		echo $sent;
	}
}