<?php

class WilayahController extends Controller
{

    public function __construct()
    {
        $this->beforeFilter('auth');
    }

    protected function getDistrict()
    {
        $province_id = Input::get('province_id');
        $q           = "
			SELECT
				id_kabupaten,
				id_provinsi,
				kabupaten,
				longitude,
				latitude
			FROM kabupaten
			WHERE id_provinsi='" . $province_id . "'
			order by kabupaten ASC
		";
        echo "<option value=''>--- Semua ---</option>";
        $data = DB::select($q);
        for ($i = 0; $i < count($data); $i++) {
            echo "<option value='" . $data[$i]->id_kabupaten . "'>" . $data[$i]->kabupaten . "</option>";
        }
    }

    protected function getSubDistrict()
    {
        $district_id = Input::get('district_id');
        $q           = "
			SELECT
				id_kecamatan,
				id_kabupaten,
				kecamatan
			FROM kecamatan
			WHERE id_kabupaten='" . $district_id . "'
			order by kecamatan ASC
		";
        echo "<option value=''>--- Semua ---</option>";
        $data = DB::select($q);
        for ($i = 0; $i < count($data); $i++) {
            echo "<option value='" . $data[$i]->id_kecamatan . "'>" . $data[$i]->kecamatan . "</option>";
        }
    }

    protected function getVillage()
    {
        $sub_district_id = Input::get('sub_district_id');
        $q               = "
			SELECT
				id_kelurahan,
				id_kecamatan,
				kelurahan
			FROM kelurahan
			WHERE id_kecamatan='" . $sub_district_id . "'
			order by kelurahan ASC
		";
        echo "<option value=''>--- Semua ---</option>";
        $data = DB::select($q);
        for ($i = 0; $i < count($data); $i++) {
            echo "<option value='" . $data[$i]->id_kelurahan . "'>" . $data[$i]->kelurahan . "</option>";
        }
    }

    protected function getPuskesmas()
    {
        $sub_district_id = Input::get('sub_district_id');
        $q               = "
			SELECT
				puskesmas_id,
				puskesmas_name,
				puskesmas_code_faskes,
				alamat,
				lokasi_puskesmas,
				longitude,
				latitude,
				kabupaten_name
			FROM puskesmas
			WHERE kode_kec='$sub_district_id'
			order by puskesmas_name ASC
		";
        echo "<option value=''>--- Semua ---</option>";
        $data = DB::select($q);
        for ($i = 0; $i < count($data); $i++) {
            echo "<option value='" . $data[$i]->puskesmas_code_faskes . "'>" . $data[$i]->puskesmas_name . "</option>";
        }
    }

    protected function getRS()
    {
        $district_id = Input::get('district_id');
        $q           = "
			SELECT
				`kode_faskes`,
				`nama_faskes`
			FROM rumahsakit2
			WHERE kode_kab='$district_id'
			order by nama_faskes ASC
		";
        echo "<option value=''>--- Semua ---</option>";
        $data = DB::select($q);
        for ($i = 0; $i < count($data); $i++) {
            echo "<option value='" . $data[$i]->kode_faskes . "'>" . $data[$i]->nama_faskes . "</option>";
        }
    }

    protected function getPuskesmasDetail()
    {
        $puskesmas_code = Input::get('puskesmas_code');
        $q              = "
			SELECT
				`puskesmas_id`,
				`puskesmas_name`,
				`puskesmas_code_faskes`,
				`alamat`,
				`lokasi_puskesmas`,
				`longitude`,
				`latitude`,
				`kabupaten_name`
			FROM puskesmas
			WHERE puskesmas_code_faskes LIKE '" . $puskesmas_code . "'
			order by puskesmas_name ASC
		";

        $data = DB::select($q);
        for ($i = 0; $i < count($data); $i++) {
            echo $data[$i]->alamat;
        }
    }

    protected function getRSDetail()
    {
        $rs_code = Input::get('rs_code');
        $q       = "
			SELECT
				`alamat`
			FROM rumahsakit2
			WHERE kode_faskes = '" . $rs_code . "'
		";

        $data = DB::select($q);
        for ($i = 0; $i < count($data); $i++) {
            echo $data[$i]->alamat;
        }
    }

    protected function getLaboratoriumDetail()
    {
        $lab_code = Input::get('lab_code');
        $q        = "
			SELECT
				*
			FROM laboratorium
			WHERE lab_code = '" . $lab_code . "'
		";

        $data = DB::select($q);
        for ($i = 0; $i < count($data); $i++) {
            echo $data[$i]->alamat;
        }
    }

    public function postChartJS()
    {
        return View::make('analisa.campak.js');
    }

    public function postChartMaps()
    {
        return View::make('analisa.campak.maps');
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    protected function getCampakJenisKelaminExcel()
    {
        $penyakit  = Session::get('penyakit');
        $excel_tmp = array(
            array('Jumlah Penderita ' . Session::get('penyakit') . ' Berdasar Jenis Kelamin'),
            array('No', 'Jenis Kelamin', 'Jumlah'),
        );

        $arr_nama_bulan[1]  = "Januari";
        $arr_nama_bulan[2]  = "Februari";
        $arr_nama_bulan[3]  = "Maret";
        $arr_nama_bulan[4]  = "April";
        $arr_nama_bulan[5]  = "Mei";
        $arr_nama_bulan[6]  = "Juni";
        $arr_nama_bulan[7]  = "Juli";
        $arr_nama_bulan[8]  = "Agustus";
        $arr_nama_bulan[9]  = "September";
        $arr_nama_bulan[10] = "Oktober";
        $arr_nama_bulan[11] = "November";
        $arr_nama_bulan[12] = "Desember";

        $post['province_id']     = "";
        $post['day_start']       = "";
        $post['month_start']     = "";
        $post['year_start']      = "2009";
        $post['district_id']     = "";
        $post['day_end']         = "";
        $post['month_end']       = "";
        $post['year_end']        = date('Y');
        $post['sub_district_id'] = "";
        $post['village_id']      = "";

        $post = Session::get('post_campak');

        $unit            = $post['unit'];
        $day_start       = $post['day_start'];
        $month_start     = $post['month_start'];
        $year_start      = $post['year_start'];
        $day_end         = $post['day_end'];
        $month_end       = $post['month_end'];
        $year_end        = $post['year_end'];
        $province_id     = $post['province_id'];
        $district_id     = $post['district_id'];
        $sub_district_id = $post['sub_district_id'];
        $village_id      = $post['village_id'];

        switch ($unit) {
            case "all":
                $between    = " ";
                $start_disp = $post['year_start'];
                $end_disp   = $post['year_end'];
                break;
            case "year":
                $start = $post['year_start'];
                $end   = $post['year_end'];
                if ($penyakit == "Campak") {
                    $between = " AND YEAR(c.tanggal_timbul_rash) BETWEEN '" . $start . "' AND '" . $end . "' ";
                } else if ($penyakit == "Difteri") {
                    $between = " AND YEAR(c.tanggal_timbul_demam) BETWEEN '" . $start . "' AND '" . $end . "' ";
                } else if ($penyakit == "AFP") {
                    $between = " AND YEAR(c.tanggal_mulai_lumpuh) BETWEEN '" . $start . "' AND '" . $end . "' ";
                } else if ($penyakit == "Tetanus") {
                    $between = " AND YEAR(c.tanggal_mulai_sakit) BETWEEN '" . $start . "' AND '" . $end . "' ";
                } else if ($penyakit == "crs") {
                    $between = " AND YEAR(c.tanggal_mulai_sakit) BETWEEN '" . $start . "' AND '" . $end . "' ";
                }

                $start_disp = $post['year_start'];
                $end_disp   = $post['year_end'];
                break;
            case "month":
                $start = $post['year_start'] . '-' . $post['month_start'] . '-1';
                $end   = $post['year_end'] . '-' . $post['month_end'] . '-' . date('t', mktime(0, 0, 0, $post['month_end'], 1, $post['year_end']));
                if ($penyakit == "Campak") {
                    $between = " AND DATE(c.tanggal_timbul_rash) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Difteri") {
                    $between = " AND DATE(c.tanggal_timbul_demam) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "AFP") {
                    $between = " AND DATE(c.tanggal_mulai_lumpuh) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Tetanus") {
                    $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Crs") {
                    $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                }

                $start_disp = $arr_nama_bulan[$post['month_start']] . " " . $post['year_start'];
                $end_disp   = $arr_nama_bulan[$post['month_end']] . " " . $post['year_end'];
                break;
            default:
                $start = $post['year_start'] . '-' . $post['month_start'] . '-' . $post['day_start'];
                $end   = $post['year_end'] . '-' . $post['month_end'] . '-' . $post['day_end'];
                if ($penyakit == "Campak") {
                    $between = " AND DATE(c.tanggal_timbul_rash) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Difteri") {
                    $between = " AND DATE(c.tanggal_timbul_demam) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "AFP") {
                    $between = " AND DATE(c.tanggal_mulai_lumpuh) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Tetanus") {
                    $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Crs") {
                    $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                }

                $start_disp = $post['day_start'] . " " . $arr_nama_bulan[$post['month_start']] . " " . $post['year_start'];
                $end_disp   = $post['day_end'] . " " . $arr_nama_bulan[$post['month_end']] . " " . $post['year_end'];
                break;
        }

        $wilayah = "";
        if ($village_id) {
            $wilayah = "AND d.id_kelurahan='" . $village_id . "' ";
        } else if ($sub_district_id) {
            $wilayah = "AND e.id_kecamatan='" . $sub_district_id . "' ";
        } else if ($district_id) {
            $wilayah = "AND f.id_kabupaten='" . $district_id . "' ";
        } else if ($province_id) {
            $wilayah = "AND g.id_provinsi='" . $province_id . "' ";
        }

        if ($penyakit == "Campak") {
            $q = "
			SELECT
					IF(b.jenis_kelamin='0','Laki-Laki',
					IF(b.jenis_kelamin='1','Perempuan',
					IF(b.jenis_kelamin='2','Tidak jelas',
					'Tidak Diisi'))) AS jenis_kelamin,
					COUNT(*) AS jml
			FROM hasil_uji_lab_campak a
			JOIN pasien b ON b.id_pasien=a.id_pasien
			JOIN campak c ON c.id_campak=a.id_campak
			LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
			LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
			LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
			LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
			LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
			WHERE
			1=1
			" . $between . "
			" . $wilayah . "
			AND c.klasifikasi_final IN ('0','1','2')
			GROUP BY b.jenis_kelamin
			";
        } else if ($penyakit == "AFP") {
            $q = "
			SELECT
					IF(b.jenis_kelamin='0','Laki-Laki',
					IF(b.jenis_kelamin='1','Perempuan',
					IF(b.jenis_kelamin='2','Tidak jelas',
					'Tidak Diisi'))) AS jenis_kelamin,
					COUNT(*) AS jml
			FROM hasil_uji_lab_afp a
			JOIN pasien b ON b.id_pasien=a.id_pasien
			JOIN afp c ON c.id_afp=a.id_afp
			LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
			LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
			LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
			LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
			LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
			WHERE
			1=1
			" . $between . "
			" . $wilayah . "
			AND c.klasifikasi_final IN ('0')
			GROUP BY b.jenis_kelamin
			";
        } else if ($penyakit == "Difteri") {
            $q = "
			SELECT
					IF(b.jenis_kelamin='0','Laki-Laki',
					IF(b.jenis_kelamin='1','Perempuan',
					IF(b.jenis_kelamin='2','Tidak jelas',
					'Tidak Diisi'))) AS jenis_kelamin,
					COUNT(*) AS jml
			FROM hasil_uji_lab_difteri a
			JOIN pasien b ON b.id_pasien=a.id_pasien
			JOIN difteri c ON c.id_difteri=a.id_difteri
			LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
			LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
			LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
			LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
			LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
			WHERE
			1=1
			" . $between . "
			" . $wilayah . "
			AND c.klasifikasi_final IN ('0','1','2')
			GROUP BY b.jenis_kelamin
			";
        } else if ($penyakit == "Tetanus") {
            $q = "
			SELECT
					IF(b.jenis_kelamin='0','Laki-Laki',
					IF(b.jenis_kelamin='1','Perempuan',
					IF(b.jenis_kelamin='2','Tidak jelas',
					'Tidak Diisi'))) AS jenis_kelamin,
					COUNT(*) AS jml
			FROM pasien_terserang_tetanus a
			JOIN pasien b ON b.id_pasien=a.id_pasien
			JOIN tetanus c ON c.id_tetanus=a.id_tetanus
			LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
			LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
			LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
			LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
			LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
			WHERE
			1=1
			" . $between . "
			" . $wilayah . "
			AND c.klasifikasi_akhir IN ('0')
			GROUP BY b.jenis_kelamin
			";
        } else if ($penyakit == 'Crs') {
            $q = "
			SELECT
				IF(b.jenis_kelamin='0','Laki-Laki', IF(b.jenis_kelamin='1','Perempuan',	IF(b.jenis_kelamin='2','Tidak jelas','Tidak Diisi'))) AS jenis_kelamin,
				COUNT(*) AS jml
				FROM
				crs AS a
				JOIN pasien AS b ON a.id_pasien=b.id_pasien
				LEFT JOIN puskesmas AS c ON a.id_tempat_periksa=c.puskesmas_id
				LEFT JOIN kelurahan AS d ON b.id_kelurahan=d.id_kelurahan
				LEFT JOIN kecamatan AS e ON d.id_kecamatan=e.id_kecamatan
				LEFT JOIN kabupaten AS f ON e.id_kabupaten=f.id_kabupaten
				LEFT JOIN provinsi AS g ON f.id_provinsi=g.id_provinsi
				WHERE 1=1
				" . $between . "
				" . $wilayah . "
				AND a.klasifikasi_final IN ('CRS pasti(Lab Positif)','CRS Klinis','CRI','Discarded')
				GROUP BY b.jenis_kelamin
				";
        }

        $data = DB::select($q);

        for ($i = 0; $i < count($data); $i++) {
            $excel_tmp[2 + $i][0] = $i + 1;
            $excel_tmp[2 + $i][1] = $data[$i]->jenis_kelamin;
            $excel_tmp[2 + $i][2] = $data[$i]->jml;
        }

        Excel::create('Jumlah_Penderita_' . Session::get('penyakit') . '_Berdasar_Jenis_Kelamin_' . date('Y_m_d_h_i_s'), function ($excel) use ($excel_tmp) {
            $excel->sheet('Sheetname', function ($sheet) use ($excel_tmp) {
                $sheet->fromArray($excel_tmp);
                $sheet->mergeCells('A1:E1');
                $sheet->mergeCells('A2:E2');
            });

        })->export('xls');
    }

    protected function getCampakWaktuExcel()
    {
        $penyakit  = Session::get('penyakit');
        $excel_tmp = array(
            array('Jumlah Penderita ' . Session::get('penyakit') . ' Berdasar Waktu'),
            array('No', 'Bulan & Tahun', 'Jumlah'),
        );

        $arr_nama_bulan[1]  = "Januari";
        $arr_nama_bulan[2]  = "Februari";
        $arr_nama_bulan[3]  = "Maret";
        $arr_nama_bulan[4]  = "April";
        $arr_nama_bulan[5]  = "Mei";
        $arr_nama_bulan[6]  = "Juni";
        $arr_nama_bulan[7]  = "Juli";
        $arr_nama_bulan[8]  = "Agustus";
        $arr_nama_bulan[9]  = "September";
        $arr_nama_bulan[10] = "Oktober";
        $arr_nama_bulan[11] = "November";
        $arr_nama_bulan[12] = "Desember";

        $post['province_id']     = "";
        $post['day_start']       = "";
        $post['month_start']     = "";
        $post['year_start']      = "2009";
        $post['district_id']     = "";
        $post['day_end']         = "";
        $post['month_end']       = "";
        $post['year_end']        = date('Y');
        $post['sub_district_id'] = "";
        $post['village_id']      = "";

        $post = Session::get('post_campak');

        $unit            = $post['unit'];
        $day_start       = $post['day_start'];
        $month_start     = $post['month_start'];
        $year_start      = $post['year_start'];
        $day_end         = $post['day_end'];
        $month_end       = $post['month_end'];
        $year_end        = $post['year_end'];
        $province_id     = $post['province_id'];
        $district_id     = $post['district_id'];
        $sub_district_id = $post['sub_district_id'];
        $village_id      = $post['village_id'];

        switch ($unit) {
            case "all":
                $between    = " ";
                $start_disp = $post['year_start'];
                $end_disp   = $post['year_end'];
                break;
            case "year":
                $start = $post['year_start'];
                $end   = $post['year_end'];
                if ($penyakit == "Campak") {
                    $between = " AND YEAR(c.tanggal_timbul_rash) BETWEEN '" . $start . "' AND '" . $end . "' ";
                } else if ($penyakit == "Difteri") {
                    $between = " AND YEAR(c.tanggal_timbul_demam) BETWEEN '" . $start . "' AND '" . $end . "' ";
                } else if ($penyakit == "AFP") {
                    $between = " AND YEAR(c.tanggal_mulai_lumpuh) BETWEEN '" . $start . "' AND '" . $end . "' ";
                } else if ($penyakit == "Tetanus") {
                    $between = " AND YEAR(c.tanggal_mulai_sakit) BETWEEN '" . $start . "' AND '" . $end . "' ";
                } else if ($penyakit == "crs") {
                    $between = " AND YEAR(c.tanggal_mulai_sakit) BETWEEN '" . $start . "' AND '" . $end . "' ";
                }

                $start_disp = $post['year_start'];
                $end_disp   = $post['year_end'];
                break;
            case "month":
                $start = $post['year_start'] . '-' . $post['month_start'] . '-1';
                $end   = $post['year_end'] . '-' . $post['month_end'] . '-' . date('t', mktime(0, 0, 0, $post['month_end'], 1, $post['year_end']));
                if ($penyakit == "Campak") {
                    $between = " AND DATE(c.tanggal_timbul_rash) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Difteri") {
                    $between = " AND DATE(c.tanggal_timbul_demam) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "AFP") {
                    $between = " AND DATE(c.tanggal_mulai_lumpuh) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Tetanus") {
                    $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Crs") {
                    $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                }

                $start_disp = $arr_nama_bulan[$post['month_start']] . " " . $post['year_start'];
                $end_disp   = $arr_nama_bulan[$post['month_end']] . " " . $post['year_end'];
                break;
            default:
                $start = $post['year_start'] . '-' . $post['month_start'] . '-' . $post['day_start'];
                $end   = $post['year_end'] . '-' . $post['month_end'] . '-' . $post['day_end'];
                if ($penyakit == "Campak") {
                    $between = " AND DATE(c.tanggal_timbul_rash) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Difteri") {
                    $between = " AND DATE(c.tanggal_timbul_demam) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "AFP") {
                    $between = " AND DATE(c.tanggal_mulai_lumpuh) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Tetanus") {
                    $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Crs") {
                    $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                }

                $start_disp = $post['day_start'] . " " . $arr_nama_bulan[$post['month_start']] . " " . $post['year_start'];
                $end_disp   = $post['day_end'] . " " . $arr_nama_bulan[$post['month_end']] . " " . $post['year_end'];
                break;
        }

        $wilayah = "";
        if ($village_id) {
            $wilayah = "AND d.id_kelurahan='" . $village_id . "' ";
        } else if ($sub_district_id) {
            $wilayah = "AND e.id_kecamatan='" . $sub_district_id . "' ";
        } else if ($district_id) {
            $wilayah = "AND f.id_kabupaten='" . $district_id . "' ";
        } else if ($province_id) {
            $wilayah = "AND g.id_provinsi='" . $province_id . "' ";
        }

        $no = 0;
        for ($i = $year_start; $i <= $year_end; $i++) {
            for ($j = 1; $j <= 12; $j++) {
                $no++;
                if ($j < 10) {
                    $bln_num = "0" . $j;
                } else {
                    $bln_num = $j;
                }

                $bln_name = $arr_nama_bulan[$j];
                if ($penyakit == "Campak") {
                    $q = "
						SELECT
							DATE_FORMAT(c.tanggal_timbul_demam,'%m') AS bln,
							DATE_FORMAT(c.tanggal_timbul_demam,'%Y') AS tahun,
							DATE_FORMAT(c.tanggal_timbul_demam,'%m %Y') AS bln_num,
							IFNULL(COUNT(*),0) AS jml
						FROM hasil_uji_lab_campak a
						JOIN pasien b ON b.id_pasien=a.id_pasien
						JOIN campak c ON c.id_campak=a.id_campak
						LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
						LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
						LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
						LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
						LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
						WHERE
						1=1
						AND c.klasifikasi_final IN ('0','1','2','3','4','5')
						AND DATE_FORMAT(c.tanggal_timbul_demam,'%m') = '" . $bln_num . "'
						AND DATE_FORMAT(c.tanggal_timbul_demam,'%Y') = '" . $i . "'
						" . $wilayah . "
						GROUP BY DATE_FORMAT(c.tanggal_timbul_demam,'%m %y')
					";
                } else if ($penyakit == "AFP") {
                    $q = "
						SELECT
							DATE_FORMAT(c.tanggal_mulai_lumpuh,'%m') AS bln,
							DATE_FORMAT(c.tanggal_mulai_lumpuh,'%Y') AS tahun,
							DATE_FORMAT(c.tanggal_mulai_lumpuh,'%m %Y') AS bln_num,
							IFNULL(COUNT(*),0) AS jml
						FROM hasil_uji_lab_afp a
						JOIN pasien b ON b.id_pasien=a.id_pasien
						JOIN afp c ON c.id_afp=a.id_afp
						LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
						LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
						LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
						LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
						LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
						WHERE
						1=1
						AND c.klasifikasi_final IN ('0')
						AND DATE_FORMAT(c.tanggal_mulai_lumpuh,'%m') = '" . $bln_num . "'
						AND DATE_FORMAT(c.tanggal_mulai_lumpuh,'%Y') = '" . $i . "'
						" . $wilayah . "
						GROUP BY DATE_FORMAT(c.tanggal_mulai_lumpuh,'%m %y')
					";
                } else if ($penyakit == "Difteri") {
                    $q = "
						SELECT
							DATE_FORMAT(c.tanggal_timbul_demam,'%m') AS bln,
							DATE_FORMAT(c.tanggal_timbul_demam,'%Y') AS tahun,
							DATE_FORMAT(c.tanggal_timbul_demam,'%m %Y') AS bln_num,
							IFNULL(COUNT(*),0) AS jml
						FROM hasil_uji_lab_difteri a
						JOIN pasien b ON b.id_pasien=a.id_pasien
						JOIN difteri c ON c.id_difteri=a.id_difteri
						LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
						LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
						LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
						LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
						LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
						WHERE
						1=1
						AND c.klasifikasi_final IN ('0','1','2')
						AND DATE_FORMAT(c.tanggal_timbul_demam,'%m') = '" . $bln_num . "'
						AND DATE_FORMAT(c.tanggal_timbul_demam,'%Y') = '" . $i . "'
						" . $wilayah . "
						GROUP BY DATE_FORMAT(c.tanggal_timbul_demam,'%m %y')
					";
                } else if ($penyakit == "Tetanus") {
                    $q = "
						SELECT
							DATE_FORMAT(c.tanggal_mulai_sakit,'%m') AS bln,
							DATE_FORMAT(c.tanggal_mulai_sakit,'%Y') AS tahun,
							DATE_FORMAT(c.tanggal_mulai_sakit,'%m %Y') AS bln_num,
							IFNULL(COUNT(*),0) AS jml
						FROM pasien_terserang_tetanus a
						JOIN pasien b ON b.id_pasien=a.id_pasien
						JOIN tetanus c ON c.id_tetanus=a.id_tetanus
						LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
						LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
						LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
						LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
						LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
						WHERE
						1=1
						AND c.klasifikasi_akhir IN ('Konfirm TN')
						AND DATE_FORMAT(c.tanggal_mulai_sakit,'%m') = '" . $bln_num . "'
						AND DATE_FORMAT(c.tanggal_mulai_sakit,'%Y') = '" . $i . "'
						" . $wilayah . "
						GROUP BY DATE_FORMAT(c.tanggal_mulai_sakit,'%m %y')
					";
                } else if ($penyakit == 'Crs') {
                    $q = "
					SELECT
						DATE_FORMAT(a.tgl_mulai_sakit,'%m') AS bln,
						DATE_FORMAT(a.tgl_mulai_sakit,'%Y') AS tahun,
						DATE_FORMAT(a.tgl_mulai_sakit,'%m %Y') AS bln_num,
						IFNULL(COUNT(*),0) AS jml
						FROM
						crs AS a
						JOIN pasien AS b ON a.id_pasien=b.id_pasien
						LEFT JOIN puskesmas AS c ON a.id_tempat_periksa=c.puskesmas_id
						LEFT JOIN kelurahan AS d ON b.id_kelurahan=d.id_kelurahan
						LEFT JOIN kecamatan AS e ON d.id_kecamatan=e.id_kecamatan
						LEFT JOIN kabupaten AS f ON e.id_kabupaten=f.id_kabupaten
						LEFT JOIN provinsi AS g ON f.id_provinsi=g.id_provinsi
						WHERE 1=1
						AND a.klasifikasi_final IN ('CRS pasti(Lab Positif)','CRS Klinis','CRI','Discarded')
						AND DATE_FORMAT(a.tgl_mulai_sakit,'%m')  = '" . $bln_num . "'
						AND DATE_FORMAT(a.tgl_mulai_sakit,'%Y')  = '" . $i . "'
						" . $wilayah . "
						GROUP BY DATE_FORMAT(a.tgl_mulai_sakit,'%m %y')
					";
                }

                $data = DB::select($q);

                $val_jml   = 0;
                $val_bln   = 0;
                $val_tahun = 0;
                if (count($data) > 0) {
                    $val_jml   = $data[0]->jml;
                    $val_bln   = $data[0]->bln;
                    $val_tahun = $data[0]->tahun;
                }
                if (!$val_jml) {
                    $val_jml = "-";
                }

                $excel_tmp[$no + 2][0] = $no;
                $excel_tmp[$no + 2][1] = $bln_name . " " . $i;
                $excel_tmp[$no + 2][2] = $val_jml;

            }
        }

        Excel::create('Jumlah_Penderita_' . Session::get('penyakit') . '_Berdasar_Waktu_' . date('Y_m_d_h_i_s'), function ($excel) use ($excel_tmp) {
            $excel->sheet('Sheetname', function ($sheet) use ($excel_tmp) {
                $sheet->fromArray($excel_tmp);
                $sheet->mergeCells('A1:E1');
                $sheet->mergeCells('A2:E2');
            });

        })->export('xls');
    }

    protected function getCampakUmurExcel()
    {
        $penyakit  = Session::get('penyakit');
        $excel_tmp = array(
            array('Jumlah Penderita ' . Session::get('penyakit') . ' Berdasar Umur'),
            array('No', 'Tahun', 'Kelompok Umur', 'Jumlah'),
        );

        $arr_nama_bulan[1]  = "Januari";
        $arr_nama_bulan[2]  = "Februari";
        $arr_nama_bulan[3]  = "Maret";
        $arr_nama_bulan[4]  = "April";
        $arr_nama_bulan[5]  = "Mei";
        $arr_nama_bulan[6]  = "Juni";
        $arr_nama_bulan[7]  = "Juli";
        $arr_nama_bulan[8]  = "Agustus";
        $arr_nama_bulan[9]  = "September";
        $arr_nama_bulan[10] = "Oktober";
        $arr_nama_bulan[11] = "November";
        $arr_nama_bulan[12] = "Desember";

        $post['province_id']     = "";
        $post['day_start']       = "";
        $post['month_start']     = "";
        $post['year_start']      = "2009";
        $post['district_id']     = "";
        $post['day_end']         = "";
        $post['month_end']       = "";
        $post['year_end']        = date('Y');
        $post['sub_district_id'] = "";
        $post['village_id']      = "";

        $post = Session::get('post_campak');

        $unit            = $post['unit'];
        $day_start       = $post['day_start'];
        $month_start     = $post['month_start'];
        $year_start      = $post['year_start'];
        $day_end         = $post['day_end'];
        $month_end       = $post['month_end'];
        $year_end        = $post['year_end'];
        $province_id     = $post['province_id'];
        $district_id     = $post['district_id'];
        $sub_district_id = $post['sub_district_id'];
        $village_id      = $post['village_id'];

        switch ($unit) {
            case "all":
                $between    = " ";
                $start_disp = $post['year_start'];
                $end_disp   = $post['year_end'];
                break;
            case "year":
                $start = $post['year_start'];
                $end   = $post['year_end'];
                if ($penyakit == "Campak") {
                    $between = " AND YEAR(c.tanggal_timbul_rash) BETWEEN '" . $start . "' AND '" . $end . "' ";
                } else if ($penyakit == "Difteri") {
                    $between = " AND YEAR(c.tanggal_timbul_demam) BETWEEN '" . $start . "' AND '" . $end . "' ";
                } else if ($penyakit == "AFP") {
                    $between = " AND YEAR(c.tanggal_mulai_lumpuh) BETWEEN '" . $start . "' AND '" . $end . "' ";
                } else if ($penyakit == "Tetanus") {
                    $between = " AND YEAR(c.tanggal_mulai_sakit) BETWEEN '" . $start . "' AND '" . $end . "' ";
                } else if ($penyakit == "crs") {
                    $between = " AND YEAR(c.tanggal_mulai_sakit) BETWEEN '" . $start . "' AND '" . $end . "' ";
                }

                $start_disp = $post['year_start'];
                $end_disp   = $post['year_end'];
                break;
            case "month":
                $start = $post['year_start'] . '-' . $post['month_start'] . '-1';
                $end   = $post['year_end'] . '-' . $post['month_end'] . '-' . date('t', mktime(0, 0, 0, $post['month_end'], 1, $post['year_end']));
                if ($penyakit == "Campak") {
                    $between = " AND DATE(c.tanggal_timbul_rash) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Difteri") {
                    $between = " AND DATE(c.tanggal_timbul_demam) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "AFP") {
                    $between = " AND DATE(c.tanggal_mulai_lumpuh) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Tetanus") {
                    $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Crs") {
                    $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                }

                $start_disp = $arr_nama_bulan[$post['month_start']] . " " . $post['year_start'];
                $end_disp   = $arr_nama_bulan[$post['month_end']] . " " . $post['year_end'];
                break;
            default:
                $start = $post['year_start'] . '-' . $post['month_start'] . '-' . $post['day_start'];
                $end   = $post['year_end'] . '-' . $post['month_end'] . '-' . $post['day_end'];
                if ($penyakit == "Campak") {
                    $between = " AND DATE(c.tanggal_timbul_rash) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Difteri") {
                    $between = " AND DATE(c.tanggal_timbul_demam) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "AFP") {
                    $between = " AND DATE(c.tanggal_mulai_lumpuh) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Tetanus") {
                    $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Crs") {
                    $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                }

                $start_disp = $post['day_start'] . " " . $arr_nama_bulan[$post['month_start']] . " " . $post['year_start'];
                $end_disp   = $post['day_end'] . " " . $arr_nama_bulan[$post['month_end']] . " " . $post['year_end'];
                break;
        }

        $wilayah = "";
        if ($village_id) {
            $wilayah = "AND d.id_kelurahan='" . $village_id . "' ";
        } else if ($sub_district_id) {
            $wilayah = "AND e.id_kecamatan='" . $sub_district_id . "' ";
        } else if ($district_id) {
            $wilayah = "AND f.id_kabupaten='" . $district_id . "' ";
        } else if ($province_id) {
            $wilayah = "AND g.id_provinsi='" . $province_id . "' ";
        }

        $arr_kelompok_umur = array(
            "",
            "Di bawah 1 tahun",
            "1-4 tahun",
            "4-9 tahun",
            "9-14tahun",
            "Di atas 15 tahun",
        );

        $no = 0;
        for ($k = 1; $k < count($arr_kelompok_umur); $k++) {
            $x_data = "";
            $comma  = "";
            for ($i = $year_start; $i <= $year_end; $i++) {
                $no++;
                if ($penyakit == "Campak") {
                    $q = "
					SELECT
					COUNT(*) AS jml
					FROM hasil_uji_lab_campak a
					JOIN pasien b ON b.id_pasien=a.id_pasien
					JOIN campak c ON c.id_campak=a.id_campak
					LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
					LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
					LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
					LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
					LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
					WHERE
					1=1
					AND YEAR(c.tanggal_timbul_rash) =  '" . $i . "'
					" . $wilayah . "
					AND getKelompokUmurCampak(b.tanggal_lahir,c.tanggal_timbul_rash)='" . $k . "'
					AND c.klasifikasi_final IN ('0','1','2')
					GROUP BY getKelompokUmurCampak(b.tanggal_lahir,c.tanggal_timbul_rash)
				";
                } else if ($penyakit == "AFP") {
                    $q = "
					SELECT
					COUNT(*) AS jml
					FROM hasil_uji_lab_afp a
					JOIN pasien b ON b.id_pasien=a.id_pasien
					JOIN afp c ON c.id_afp=a.id_afp
					LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
					LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
					LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
					LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
					LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
					WHERE
					1=1
					AND YEAR(c.tanggal_mulai_lumpuh) =  '" . $i . "'
					" . $wilayah . "
					AND getKelompokUmurCampak(b.tanggal_lahir,c.tanggal_mulai_lumpuh)='" . $k . "'
					AND c.klasifikasi_final IN ('0','1','2')
					GROUP BY getKelompokUmurCampak(b.tanggal_lahir,c.tanggal_mulai_lumpuh)
				";
                } else if ($penyakit == "Difteri") {
                    $q = "
					SELECT
					COUNT(*) AS jml
					FROM hasil_uji_lab_difteri a
					JOIN pasien b ON b.id_pasien=a.id_pasien
					JOIN difteri c ON c.id_difteri=a.id_difteri
					LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
					LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
					LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
					LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
					LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
					WHERE
					1=1
					AND YEAR(c.tanggal_timbul_demam) =  '" . $i . "'
					" . $wilayah . "
					AND getKelompokUmurCampak(b.tanggal_lahir,c.tanggal_timbul_demam)='" . $k . "'
					AND c.klasifikasi_final IN ('0','1','2')
					GROUP BY getKelompokUmurCampak(b.tanggal_lahir,c.tanggal_timbul_demam)
				";
                } else if ($penyakit == "Tetanus") {
                    $q = "
					SELECT
					COUNT(*) AS jml
					FROM pasien_terserang_tetanus a
					JOIN pasien b ON b.id_pasien=a.id_pasien
					JOIN tetanus c ON c.id_tetanus=a.id_tetanus
					LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
					LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
					LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
					LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
					LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
					WHERE
					1=1
					AND YEAR(c.tanggal_mulai_sakit) =  '" . $i . "'
					" . $wilayah . "
					AND getKelompokUmurCampak(b.tanggal_lahir,c.tanggal_mulai_sakit)='" . $k . "'
					AND c.klasifikasi_akhir IN ('Konfirm TN')
					GROUP BY getKelompokUmurCampak(b.tanggal_lahir,c.tanggal_mulai_sakit)
				";
                } else if ($penyakit == "Crs") {
                    $q = "
					SELECT
					COUNT(*) AS jml
					FROM
					crs AS a
					JOIN pasien AS b ON a.id_pasien=b.id_pasien
					LEFT JOIN puskesmas AS c ON a.id_tempat_periksa=c.puskesmas_id
					LEFT JOIN kelurahan AS d ON b.id_kelurahan=d.id_kelurahan
					LEFT JOIN kecamatan AS e ON d.id_kecamatan=e.id_kecamatan
					LEFT JOIN kabupaten AS f ON e.id_kabupaten=f.id_kabupaten
					LEFT JOIN provinsi AS g ON f.id_provinsi=g.id_provinsi
					WHERE 1=1
					AND YEAR(a.tgl_mulai_sakit) = '" . $i . "'
					" . $wilayah . "
					AND getKelompokUmurCampak(b.tanggal_lahir,a.tgl_mulai_sakit)='" . $k . "'
					AND a.klasifikasi_final IN ('CRS pasti(Lab Positif)','CRS Klinis','CRI','Discarded')
					GROUP BY getKelompokUmurCampak(b.tanggal_lahir,a.tgl_mulai_sakit)
				";
                }
                //echo $q;
                $data = DB::select($q);

                if (count($data) > 0) {
                    $val = $data[0]->jml;
                } else {
                    $val = 0;
                }

                $excel_tmp[$no + 2][0] = $no;
                $excel_tmp[$no + 2][1] = $i;
                $excel_tmp[$no + 2][2] = $arr_kelompok_umur[$k];
                $excel_tmp[$no + 2][3] = $val;

            }

        }

        Excel::create('Jumlah_Penderita_' . Session::get('penyakit') . '_Berdasar_Kelompok_Umur_' . date('Y_m_d_h_i_s'), function ($excel) use ($excel_tmp) {
            $excel->sheet('Sheetname', function ($sheet) use ($excel_tmp) {
                $sheet->fromArray($excel_tmp);
                $sheet->mergeCells('A1:E1');
                $sheet->mergeCells('A2:E2');
            });

        })->export('xls');
    }

    protected function getCampakImunisasiExcel()
    {
        $penyakit  = Session::get('penyakit');
        $excel_tmp = array(
            array('Jumlah Penderita ' . Session::get('penyakit') . ' Berdasar Status Imunisasi '),
            array('No', 'Status Imunisasi', 'Jumlah'),
        );

        $arr_nama_bulan[1]  = "Januari";
        $arr_nama_bulan[2]  = "Februari";
        $arr_nama_bulan[3]  = "Maret";
        $arr_nama_bulan[4]  = "April";
        $arr_nama_bulan[5]  = "Mei";
        $arr_nama_bulan[6]  = "Juni";
        $arr_nama_bulan[7]  = "Juli";
        $arr_nama_bulan[8]  = "Agustus";
        $arr_nama_bulan[9]  = "September";
        $arr_nama_bulan[10] = "Oktober";
        $arr_nama_bulan[11] = "November";
        $arr_nama_bulan[12] = "Desember";

        $post['province_id']     = "";
        $post['day_start']       = "";
        $post['month_start']     = "";
        $post['year_start']      = "2009";
        $post['district_id']     = "";
        $post['day_end']         = "";
        $post['month_end']       = "";
        $post['year_end']        = date('Y');
        $post['sub_district_id'] = "";
        $post['village_id']      = "";

        $post = Session::get('post_campak');

        $unit            = $post['unit'];
        $day_start       = $post['day_start'];
        $month_start     = $post['month_start'];
        $year_start      = $post['year_start'];
        $day_end         = $post['day_end'];
        $month_end       = $post['month_end'];
        $year_end        = $post['year_end'];
        $province_id     = $post['province_id'];
        $district_id     = $post['district_id'];
        $sub_district_id = $post['sub_district_id'];
        $village_id      = $post['village_id'];

        switch ($unit) {
            case "all":
                $between    = " ";
                $start_disp = $post['year_start'];
                $end_disp   = $post['year_end'];
                break;
            case "year":
                $start = $post['year_start'];
                $end   = $post['year_end'];
                if ($penyakit == "Campak") {
                    $between = " AND YEAR(c.tanggal_timbul_rash) BETWEEN '" . $start . "' AND '" . $end . "' ";
                } else if ($penyakit == "Difteri") {
                    $between = " AND YEAR(c.tanggal_timbul_demam) BETWEEN '" . $start . "' AND '" . $end . "' ";
                } else if ($penyakit == "AFP") {
                    $between = " AND YEAR(c.tanggal_mulai_lumpuh) BETWEEN '" . $start . "' AND '" . $end . "' ";
                } else if ($penyakit == "Tetanus") {
                    $between = " AND YEAR(c.tanggal_mulai_sakit) BETWEEN '" . $start . "' AND '" . $end . "' ";
                } else if ($penyakit == "crs") {
                    $between = " AND YEAR(c.tanggal_mulai_sakit) BETWEEN '" . $start . "' AND '" . $end . "' ";
                }

                $start_disp = $post['year_start'];
                $end_disp   = $post['year_end'];
                break;
            case "month":
                $start = $post['year_start'] . '-' . $post['month_start'] . '-1';
                $end   = $post['year_end'] . '-' . $post['month_end'] . '-' . date('t', mktime(0, 0, 0, $post['month_end'], 1, $post['year_end']));
                if ($penyakit == "Campak") {
                    $between = " AND DATE(c.tanggal_timbul_rash) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Difteri") {
                    $between = " AND DATE(c.tanggal_timbul_demam) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "AFP") {
                    $between = " AND DATE(c.tanggal_mulai_lumpuh) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Tetanus") {
                    $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Crs") {
                    $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                }

                $start_disp = $arr_nama_bulan[$post['month_start']] . " " . $post['year_start'];
                $end_disp   = $arr_nama_bulan[$post['month_end']] . " " . $post['year_end'];
                break;
            default:
                $start = $post['year_start'] . '-' . $post['month_start'] . '-' . $post['day_start'];
                $end   = $post['year_end'] . '-' . $post['month_end'] . '-' . $post['day_end'];
                if ($penyakit == "Campak") {
                    $between = " AND DATE(c.tanggal_timbul_rash) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Difteri") {
                    $between = " AND DATE(c.tanggal_timbul_demam) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "AFP") {
                    $between = " AND DATE(c.tanggal_mulai_lumpuh) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Tetanus") {
                    $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Crs") {
                    $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                }

                $start_disp = $post['day_start'] . " " . $arr_nama_bulan[$post['month_start']] . " " . $post['year_start'];
                $end_disp   = $post['day_end'] . " " . $arr_nama_bulan[$post['month_end']] . " " . $post['year_end'];
                break;
        }

        $wilayah = "";
        if ($village_id) {
            $wilayah = "AND d.id_kelurahan='" . $village_id . "' ";
        } else if ($sub_district_id) {
            $wilayah = "AND e.id_kecamatan='" . $sub_district_id . "' ";
        } else if ($district_id) {
            $wilayah = "AND f.id_kabupaten='" . $district_id . "' ";
        } else if ($province_id) {
            $wilayah = "AND g.id_provinsi='" . $province_id . "' ";
        }

        if ($penyakit == "Campak") {
            $q = "
					SELECT
						IF(c.vaksin_campak_sebelum_sakit='0',' 1 Kali ',
						IF(c.vaksin_campak_sebelum_sakit='1',' 2 Kali ',
						IF(c.vaksin_campak_sebelum_sakit='2',' 3 Kali ',
						IF(c.vaksin_campak_sebelum_sakit='3',' 4 Kali ',
						IF(c.vaksin_campak_sebelum_sakit='4',' 5 Kali ',
						IF(c.vaksin_campak_sebelum_sakit='5',' 6 Kali ',
						IF(c.vaksin_campak_sebelum_sakit='6',' Tidak ',
						IF(c.vaksin_campak_sebelum_sakit='7',' Tidak Tahu ',
						' Tidak Diisi '
						)))))))) AS name,
						COUNT(*) AS jml
					FROM hasil_uji_lab_campak a
					JOIN pasien b ON b.id_pasien=a.id_pasien
					JOIN campak c ON c.id_campak=a.id_campak
					LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
					LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
					LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
					LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
					LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
					WHERE
					1=1
					" . $between . "
					" . $wilayah . "
					AND c.klasifikasi_final IN ('0','1','2')
					GROUP BY c.vaksin_campak_sebelum_sakit
				";
        } else if ($penyakit == "AFP") {
            $q = "
					SELECT
						IF(c.imunisasi_polio_sebelum_sakit='0','1 Kali',
						IF(c.imunisasi_polio_sebelum_sakit='1','2 Kali',
						IF(c.imunisasi_polio_sebelum_sakit='2','3 Kali',
						IF(c.imunisasi_polio_sebelum_sakit='3','4 Kali',
						IF(c.imunisasi_polio_sebelum_sakit='4','5 Kali',
						IF(c.imunisasi_polio_sebelum_sakit='5','6 Kali',
						IF(c.imunisasi_polio_sebelum_sakit='6','Tidak',
						IF(c.imunisasi_polio_sebelum_sakit='7','Tidak tahu',
						'Tidak Diisi')))))))) AS name,
						COUNT(*) AS jml
					FROM hasil_uji_lab_afp a
					JOIN pasien b ON b.id_pasien=a.id_pasien
					JOIN afp c ON c.id_afp=a.id_afp
					LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
					LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
					LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
					LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
					LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
					WHERE
					1=1
					" . $between . "
					" . $wilayah . "
					AND c.klasifikasi_final IN ('0','1','2')
					GROUP BY c.imunisasi_polio_sebelum_sakit
				";
        } else if ($penyakit == "Difteri") {
            $q = "
					SELECT
						IF(c.vaksin_DPT_sebelum_sakit='0','1 Kali',
						IF(c.vaksin_DPT_sebelum_sakit='1','2 Kali',
						IF(c.vaksin_DPT_sebelum_sakit='2','3 Kali',
						IF(c.vaksin_DPT_sebelum_sakit='3','4 Kali',
						IF(c.vaksin_DPT_sebelum_sakit='4','5 Kali',
						IF(c.vaksin_DPT_sebelum_sakit='5','6 Kali',
						IF(c.vaksin_DPT_sebelum_sakit='6','Tidak',
						IF(c.vaksin_DPT_sebelum_sakit='7','Tidak tahu',
						'Tidak Diisi')))))))) AS name,
						COUNT(*) AS jml
					FROM hasil_uji_lab_difteri a
					JOIN pasien b ON b.id_pasien=a.id_pasien
					JOIN difteri c ON c.id_difteri=a.id_difteri
					LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
					LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
					LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
					LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
					LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
					WHERE
					1=1
					" . $between . "
					" . $wilayah . "
					AND c.klasifikasi_final IN ('0','1','2')
					GROUP BY c.vaksin_DPT_sebelum_sakit
				";
        } else if ($penyakit == "Tetanus") {
            $q = "
					SELECT
						IF(c.status_imunisasi='0','TT2+',
						IF(c.status_imunisasi='1','TT1',
						IF(c.status_imunisasi='2','Tidak Imunisasi',
						IF(c.status_imunisasi='3','Tidak Jelas',
						'Tidak Diisi')))) AS name,
						COUNT(*) AS jml
					FROM pasien_terserang_tetanus a
					JOIN pasien b ON b.id_pasien=a.id_pasien
					JOIN tetanus c ON c.id_tetanus=a.id_tetanus
					LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
					LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
					LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
					LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
					LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
					WHERE
					1=1
					" . $between . "
					" . $wilayah . "
					AND c.klasifikasi_akhir IN ('0')
					GROUP BY c.status_imunisasi
				";
        } else if ($penyakit == 'Crs') {
            $q = "SELECT
				COUNT(*) AS jml,
				k.vaksinasi_rubella AS name
				FROM
				crs AS a
				JOIN pasien AS b ON a.id_pasien=b.id_pasien
				JOIN pe_crs AS k ON a.id_pe_crs=k.id_pe_crs
				LEFT JOIN puskesmas AS c ON a.id_tempat_periksa=c.puskesmas_id
				LEFT JOIN kelurahan AS d ON b.id_kelurahan=d.id_kelurahan
				LEFT JOIN kecamatan AS e ON d.id_kecamatan=e.id_kecamatan
				LEFT JOIN kabupaten AS f ON e.id_kabupaten=f.id_kabupaten
				LEFT JOIN provinsi AS g ON f.id_provinsi=g.id_provinsi
				WHERE 1=1
				" . $between . "
				" . $wilayah . "
				AND a.klasifikasi_final IN ('CRS pasti(Lab Positif)','CRS Klinis','CRI','Discarded')
				GROUP BY k.vaksinasi_rubella
				";
        }

        $data = DB::select($q);

        for ($i = 0; $i < count($data); $i++) {
            $excel_tmp[2 + $i][0] = $i + 1;
            $excel_tmp[2 + $i][1] = $data[$i]->name;
            $excel_tmp[2 + $i][2] = $data[$i]->jml;

        }

        Excel::create('Jumlah_Penderita_' . Session::get('penyakit') . '_Berdasar_Status_Imunisasi_' . date('Y_m_d_h_i_s'), function ($excel) use ($excel_tmp) {
            $excel->sheet('Sheetname', function ($sheet) use ($excel_tmp) {
                $sheet->fromArray($excel_tmp);
                $sheet->mergeCells('A1:E1');
                $sheet->mergeCells('A2:E2');
            });

        })->export('xls');
    }

    protected function getCampakKlasifikasiFinalExcel()
    {
        $penyakit  = Session::get('penyakit');
        $excel_tmp = array(
            array('Jumlah Penderita ' . Session::get('penyakit') . ' Berdasar Klasifikasi Final'),
            array('No', 'Klasifikasi Akhir', 'Jumlah'),
        );

        $post               = Session::get('post_campak');
        $arr_nama_bulan[1]  = "Januari";
        $arr_nama_bulan[2]  = "Februari";
        $arr_nama_bulan[3]  = "Maret";
        $arr_nama_bulan[4]  = "April";
        $arr_nama_bulan[5]  = "Mei";
        $arr_nama_bulan[6]  = "Juni";
        $arr_nama_bulan[7]  = "Juli";
        $arr_nama_bulan[8]  = "Agustus";
        $arr_nama_bulan[9]  = "September";
        $arr_nama_bulan[10] = "Oktober";
        $arr_nama_bulan[11] = "November";
        $arr_nama_bulan[12] = "Desember";

        $post['province_id']     = "";
        $post['day_start']       = "";
        $post['month_start']     = "";
        $post['year_start']      = "2009";
        $post['district_id']     = "";
        $post['day_end']         = "";
        $post['month_end']       = "";
        $post['year_end']        = date('Y');
        $post['sub_district_id'] = "";
        $post['village_id']      = "";

        $unit            = $post['unit'];
        $day_start       = $post['day_start'];
        $month_start     = $post['month_start'];
        $year_start      = $post['year_start'];
        $day_end         = $post['day_end'];
        $month_end       = $post['month_end'];
        $year_end        = $post['year_end'];
        $province_id     = $post['province_id'];
        $district_id     = $post['district_id'];
        $sub_district_id = $post['sub_district_id'];
        $village_id      = $post['village_id'];

        switch ($unit) {
            case "all":
                $between    = " ";
                $start_disp = $post['year_start'];
                $end_disp   = $post['year_end'];
                break;
            case "year":
                $start = $post['year_start'];
                $end   = $post['year_end'];
                if ($penyakit == "Campak") {
                    $between = " AND YEAR(c.tanggal_timbul_rash) BETWEEN '" . $start . "' AND '" . $end . "' ";
                } else if ($penyakit == "Difteri") {
                    $between = " AND YEAR(c.tanggal_timbul_demam) BETWEEN '" . $start . "' AND '" . $end . "' ";
                } else if ($penyakit == "AFP") {
                    $between = " AND YEAR(c.tanggal_mulai_lumpuh) BETWEEN '" . $start . "' AND '" . $end . "' ";
                } else if ($penyakit == "Tetanus") {
                    $between = " AND YEAR(c.tanggal_mulai_sakit) BETWEEN '" . $start . "' AND '" . $end . "' ";
                } else if ($penyakit == "crs") {
                    $between = " AND YEAR(c.tanggal_mulai_sakit) BETWEEN '" . $start . "' AND '" . $end . "' ";
                }

                $start_disp = $post['year_start'];
                $end_disp   = $post['year_end'];
                break;
            case "month":
                $start = $post['year_start'] . '-' . $post['month_start'] . '-1';
                $end   = $post['year_end'] . '-' . $post['month_end'] . '-' . date('t', mktime(0, 0, 0, $post['month_end'], 1, $post['year_end']));
                if ($penyakit == "Campak") {
                    $between = " AND DATE(c.tanggal_timbul_rash) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Difteri") {
                    $between = " AND DATE(c.tanggal_timbul_demam) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "AFP") {
                    $between = " AND DATE(c.tanggal_mulai_lumpuh) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Tetanus") {
                    $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Crs") {
                    $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                }

                $start_disp = $arr_nama_bulan[$post['month_start']] . " " . $post['year_start'];
                $end_disp   = $arr_nama_bulan[$post['month_end']] . " " . $post['year_end'];
                break;
            default:
                $start = $post['year_start'] . '-' . $post['month_start'] . '-' . $post['day_start'];
                $end   = $post['year_end'] . '-' . $post['month_end'] . '-' . $post['day_end'];
                if ($penyakit == "Campak") {
                    $between = " AND DATE(c.tanggal_timbul_rash) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Difteri") {
                    $between = " AND DATE(c.tanggal_timbul_demam) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "AFP") {
                    $between = " AND DATE(c.tanggal_mulai_lumpuh) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Tetanus") {
                    $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                } else if ($penyakit == "Crs") {
                    $between = " AND DATE(c.tanggal_mulai_sakit) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                }

                $start_disp = $post['day_start'] . " " . $arr_nama_bulan[$post['month_start']] . " " . $post['year_start'];
                $end_disp   = $post['day_end'] . " " . $arr_nama_bulan[$post['month_end']] . " " . $post['year_end'];
                break;
        }

        $wilayah = "";
        if ($village_id) {
            $wilayah = "AND d.id_kelurahan='" . $village_id . "' ";
        } else if ($sub_district_id) {
            $wilayah = "AND e.id_kecamatan='" . $sub_district_id . "' ";
        } else if ($district_id) {
            $wilayah = "AND f.id_kabupaten='" . $district_id . "' ";
        } else if ($province_id) {
            $wilayah = "AND g.id_provinsi='" . $province_id . "' ";
        }

        if ($penyakit == "Campak") {
            $q = "
					SELECT
						IF(c.klasifikasi_final=NULL,'Pending',
						IF(c.klasifikasi_final='','Pending',
						IF(c.klasifikasi_final='0','Campak(Lab)',
						IF(c.klasifikasi_final='1','Campak (Epid)',
						IF(c.klasifikasi_final='2','Campak (Klinis)',
						IF(c.klasifikasi_final='3','Rubella',
						IF(c.klasifikasi_final='4','Bukan Campak/rubella',
						IF(c.klasifikasi_final='5','Pending',''
						)))))))) AS `name`,
						COUNT(*) AS jml
					FROM hasil_uji_lab_campak a
					JOIN pasien b ON b.id_pasien=a.id_pasien
					JOIN campak c ON c.id_campak=a.id_campak
					LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
					LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
					LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
					LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
					LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
					WHERE
					1=1
					" . $between . "
					" . $wilayah . "
					GROUP BY c.klasifikasi_final
				";
        } else if ($penyakit == "AFP") {
            $q = "
					SELECT
						IF(c.klasifikasi_final=NULL,'Undefined',
						IF(c.klasifikasi_final='','Undefined',
						IF(c.klasifikasi_final='0','Polio',
						IF(c.klasifikasi_final='1','Bukan polio',
						IF(c.klasifikasi_final='2','Compatible','Undefined'
						))))) AS `name`,
					COUNT(*) AS `jml`
					FROM hasil_uji_lab_afp a
					JOIN pasien b ON b.id_pasien=a.id_pasien
					JOIN afp c ON c.id_afp=a.id_afp
					LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
					LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
					LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
					LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
					LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
					WHERE
					1=1
					" . $between . "
					" . $wilayah . "
					GROUP BY c.klasifikasi_final
				";
        } else if ($penyakit == "Difteri") {
            $q = "
					SELECT
						IF(C.Klasifikasi_final=NULL,'Undefined',
						IF(c.klasifikasi_final='','Undefined',
						IF(c.klasifikasi_final='0','Probable',
						IF(c.klasifikasi_final='1','Konfirm',
						IF(c.klasifikasi_final='2','Negatif',''
						))))) AS `name`,
					COUNT(*) AS `jml`
					FROM hasil_uji_lab_difteri a
					JOIN pasien b ON b.id_pasien=a.id_pasien
					JOIN difteri c ON c.id_difteri=a.id_difteri
					LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
					LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
					LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
					LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
					LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
					WHERE
					1=1
					" . $between . "
					" . $wilayah . "
					GROUP BY c.klasifikasi_final
				";
        } else if ($penyakit == "Tetanus") {
            $q = "
					SELECT
						IFNULL(c.klasifikasi_akhir,'?') AS `name`,
						COUNT(*) AS `jml`
					FROM pasien_terserang_tetanus a
					JOIN pasien b ON b.id_pasien=a.id_pasien
					JOIN tetanus c ON c.id_tetanus=a.id_tetanus
					LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
					LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
					LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
					LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
					LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
					WHERE
					1=1
					" . $between . "
					" . $wilayah . "
					GROUP BY c.klasifikasi_akhir
				";
        } else if ($penyakit = 'Crs') {
            $q = "SELECT
				a.klasifikasi_final AS name,
				COUNT(*) AS jml
				FROM
				crs AS a
				JOIN pasien AS b ON a.id_pasien=b.id_pasien
				LEFT JOIN puskesmas AS c ON a.id_tempat_periksa=c.puskesmas_id
				LEFT JOIN kelurahan AS d ON b.id_kelurahan=d.id_kelurahan
				LEFT JOIN kecamatan AS e ON d.id_kecamatan=e.id_kecamatan
				LEFT JOIN kabupaten AS f ON e.id_kabupaten=f.id_kabupaten
				LEFT JOIN provinsi AS g ON f.id_provinsi=g.id_provinsi
				WHERE 1=1
				" . $between . "
				" . $wilayah . "
				GROUP BY a.klasifikasi_final";
        }

        $data = DB::select($q);

        for ($i = 0; $i < count($data); $i++) {
            $excel_tmp[2 + $i][0] = 1 + $i;
            $excel_tmp[2 + $i][1] = $data[$i]->name;
            $excel_tmp[2 + $i][2] = $data[$i]->jml;

        }

        Excel::create('Jumlah_Penderita_' . Session::get('penyakit') . '_Berdasar_Klasifikasi_Final_' . date('Y_m_d_h_i_s'), function ($excel) use ($excel_tmp) {
            $excel->sheet('Sheetname', function ($sheet) use ($excel_tmp) {
                $sheet->fromArray($excel_tmp);
                $sheet->mergeCells('A1:E1');
                $sheet->mergeCells('A2:E2');
            });

        })->export('xls');
    }

    protected function getCampakWilayahExcel()
    {
        $penyakit  = Session::get('penyakit');
        $excel_tmp = array(
            array('Jumlah Penderita ' . Session::get('penyakit') . ' Berdasar Wilayah'),
            array('No', 'Wilayah', 'Jumlah'),
        );

        $excel_tmp[2][0] = "-";
        $excel_tmp[2][1] = "-";
        $excel_tmp[2][2] = "-";

        Excel::create('Jumlah_Penderita_' . Session::get('penyakit') . '_Berdasar_Wilayah_' . date('Y_m_d_h_i_s'), function ($excel) use ($excel_tmp) {
            $excel->sheet('Sheetname', function ($sheet) use ($excel_tmp) {
                $sheet->fromArray($excel_tmp);
                $sheet->mergeCells('A1:E1');
                $sheet->mergeCells('A2:E2');
            });

        })->export('xls');
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    protected function getCampakJenisKelaminPrint()
    {
        return View::make('pemeriksaan.campak.print_jenis_kelamin');
    }

    protected function getCampakWaktuPrint()
    {
        return View::make('pemeriksaan.campak.print_waktu');
    }

    protected function getCampakUmurPrint()
    {
        return View::make('pemeriksaan.campak.print_umur');
    }

    protected function getCampakImunisasiPrint()
    {
        return View::make('pemeriksaan.campak.print_imunisasi');
    }

    protected function getCampakKlasifikasiFinalPrint()
    {
        return View::make('pemeriksaan.campak.print_klasifikasi_final');
    }

    protected function getCampakWilayahPrint()
    {
        return View::make('pemeriksaan.campak.print_wilayah');
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    protected function getProfilID()
    {
        Session::put('type', Input::get('type'));
        Session::put('kd_faskes', Input::get('kd_faskes'));
        Session::put('id_profil', Input::get('id_profil'));
        Session::put('type_profil', Input::get('type_profil'));
        // update hak_ses pada table users setiap user memilih profil akses
        $level = Session::get('type');
        if ($level == 'puskesmas') {
            $hak_akses = 2;
            $puskesmas = DB::select("select puskesmas_id from puskesmas where puskesmas.puskesmas_code_faskes='" . Input::get('kd_faskes') . "'");
            $id_user   = $puskesmas[0]->puskesmas_id;
        } else if ($level == 'rs') {
            $hak_akses = 7;
            $rs        = DB::select("select id from rumahsakit2 where kode_faskes='" . Input::get('kd_faskes') . "'");
            $id_user   = $rs[0]->id;
        } else if ($level == 'kabupaten') {
            $hak_akses = 4;
            $kabupaten = DB::select("select id_kabupaten from kabupaten where kabupaten.id_kabupaten='" . Input::get('kd_faskes') . "'");
            $id_user   = $kabupaten[0]->id_kabupaten;
        } else if ($level == 'kemenkes') {
            $hak_akses = 1;
            $id_user   = 1;
        } else if ($level == 'provinsi') {
            $hak_akses = 6;
            $provinsi  = DB::select("select id_provinsi from provinsi where provinsi.id_provinsi='" . Input::get('kd_faskes') . "'");
            $id_user   = $provinsi[0]->id_provinsi;
        } else if ($level == 'laboratorium') {
            $hak_akses    = 5;
            $laboratorium = DB::select("select id_laboratorium from laboratorium where laboratorium.lab_code='" . Input::get('kd_faskes') . "'");
            $id_user      = $laboratorium[0]->id_laboratorium;
        }

        //edit hak_akses tiap users login
        DB::table('users')
            ->where('email', Sentry::getUser()->email)
            ->update(array('hak_akses' => $hak_akses, 'id_user' => $id_user));

        //ROUTING HALAMAN DASHBOARD KE MASING2 USER
        if (Input::get('type') == 'puskesmas') {
            return Redirect::to('dashboard');
        } else if (Input::get('type') == 'rs') {
            return Redirect::to('dashboard');
        } else if (Input::get('type') == 'kabupaten') {
            return Redirect::to('dashboard');
        } else if (Input::get('type') == 'provinsi') {
            return Redirect::to('dashboard');
        } else if (Input::get('type') == 'kemenkes') {
            return Redirect::to('dashboard');
        } else if (Input::get('type') == 'laboratorium') {
            return Redirect::to('laboratorium');
        }

    }

    public function provinsi($id_provinsi = null)
    {
        return Provinsi::with('kabupatens')->paginate(3);
    }

    public function kabupaten()
    {
        return Kabupaten::with('provinsi', 'kecamatans')->paginate(3);
    }

    public function kecamatan($id_kabupaten = null)
    {
        return Kecamatan::with('kabupaten', 'kelurahans')->paginate(3);
    }

    public function kelurahan()
    {
        return Kelurahan::with('kecamatan')->paginate(3);
    }

    public function getListProvinsi()
    {
        $provinsi = Wilayah::getListProvinsi();
        return View::make('wilayah.provinsi.list', compact('provinsi'));
    }

    public function getListKabupaten($id_provinsi)
    {
        $kabupaten = Wilayah::getListKabupaten($id_provinsi);
        return View::make('wilayah.kabupaten.list', compact('kabupaten'));
    }

    public function getListKecamatan($id_kabupaten, $id_provinsi)
    {
        $kecamatan = Wilayah::getListKecamatan($id_kabupaten);
        return View::make('wilayah.kecamatan.list', compact('kecamatan', 'id_provinsi'));
    }

    public function getListKelurahan($id_kecamatan, $id_kabupaten, $id_provinsi)
    {
        $kelurahan = Wilayah::getListKelurahan($id_kecamatan);
        return View::make('wilayah.kelurahan.list', compact('kelurahan', 'id_kabupaten', 'id_provinsi'));
    }

    public function getArea()
    {
        $key   = Input::get("term");
        $query = Wilayah::getDataArea($key);
        print json_encode($query);
    }

}
