<?php
class ModulpuskesmasController extends BaseController {
	
	public function getIndex()
	{
		return View::make('master_data.modulpuskesmas');
	}
	
	public function getList()
	{
		$modulpuskesmas = ModulpuskesmasModel::getListModulpuskesmas();
		$current_page = Input::get('page');
		$current_search = Input::get('search_name');		
		return View::make('master_data.modulpuskesmas_list',compact('modulpuskesmas','current_page','current_search'));
	}

	public function getPuskesmas()
    {
        $where['a.kode_kec'] = Input::get('code_kecamatan');
        $query = ModulpuskesmasModel::getData($where);
        echo json_encode($query);
    }
	
	public function getListGet()
	{
		$modulpuskesmas = ModulpuskesmasModel::getListModulpuskesmas();
		$current_page = Input::get('page');
		$current_search = Input::get('search_name');		
		return View::make('master_data.modulpuskesmas_list',compact('modulpuskesmas','current_page','current_search'));
	}
	
	public function ModulpuskesmasProcessForm(){
		$ret = array();
        if(Input::get('puskesmas_id')) {
			$ret['command'] = "updating data";
			if(ModulpuskesmasModel::DoUpdateData(Input::all())) {
				$ret['msg'] = "Sukses Update Data";
				$ret['status'] = "success";
			} else {
				$ret['msg'] = "Gagal Update Data";
				$ret['status'] = "error";
			}
        } else {
			$ret['command'] = "adding data";
			$last_id = ModulpuskesmasModel::DoAddData();
			if($last_id > 0) {
				$ret['msg'] = "Sukses Simpan Data";
				$ret['status'] = "success";
			} else {
				$ret['msg'] = "Gagal Simpan Data";
				$ret['status'] = "error";
			}
        }
		
		echo json_encode($ret);
		
	}
	
	public function ModulpuskesmasGetDataById(){
		$provinsi = ModulpuskesmasModel::getModulpuskesmas();
		echo json_encode($provinsi);
	}
	
	public function ModulpuskesmasDeleteList(){
		ModulpuskesmasModel::DoDeleteModulpuskesmas();
		$ret['msg'] = "Sukses Delete Data";
		$ret['status'] = "success";
		echo json_encode($ret);
	}
	
}
?>
