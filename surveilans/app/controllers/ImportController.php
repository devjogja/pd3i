<?php
class ImportController extends BaseController {
	public function __construct()
	{
		$this->beforeFilter('auth');
	}

	public function ExtractCampak()
	{
		if (Input::file('excel')) {
			Excel::load(Input::file('excel'), function ($reader) {
				echo"<pre>";print_r($reader);echo"</pre>";die();
				$row=array();
				foreach ($reader->toArray() as $key => $val) {
					if (!in_array($key, array(0,1,2))) {
						$jenis_kelamin = null;
						if($val['11']=='Laki-laki'){
							$jenis_kelamin = '1';
						}elseif($val['11']=='Perempuan'){
							$jenis_kelamin = '2';
						}elseif($val['11']=='Tidak Jelas'){
							$jenis_kelamin = '3';
						}
						$kel = DB::table('located')->select('id_kelurahan')->WHERE(['kelurahan'=>$val['17'],'kecamatan'=>$val['18'],'kabupaten'=>$val['19'],'provinsi'=>$val['20']])->get();
						$id_kelurahan = (empty($kel))?null:$kel[0]->id_kelurahan;
						$dtPasien = [
						'nik'=>$val['9'],
						'nama_anak'=>$val['8'],
						'nama_ortu'=>$val['10'],
						'alamat'=>$val['16'],
						'tanggal_lahir'=>$val['12'],
						'umur'=>$val['13'],
						'umur_bln'=>$val['14'],
						'umur_hr'=>$val['15'],
						'jenis_kelamin'=>$jenis_kelamin,
						'id_kelurahan'=>$id_kelurahan,
						];
						$cekPasien = DB::table('pasien')->where(['nama_anak'=>$val['8']])->get();
						if(empty($cekPasien)){
							$id_pasien = DB::table('pasien')->insertGetId($dtPasien);
						}else{
							$id_pasien = $cekPasien[0]->id_pasien;
							$updatePasien = DB::table('pasien')->where(['id_pasien'=>$id_pasien])->update($dtPasien);
						}
						$tgl_sakit = null;
						if($val['23']){
							$tgl_sakit = $val['23'];
						}elseif($val['24']){
							$tgl_sakit = $val['24'];
						}
						$vaksin_campak_sebelum_sakit = null;
						if($val['22']=='Tidak'){
							$vaksin_campak_sebelum_sakit = '7';
						}elseif($val['22']=='Tidak tahu'){
							$vaksin_campak_sebelum_sakit = '8';
						}elseif($val['22']=='-'){
							$vaksin_campak_sebelum_sakit = null;
						}else{
							$vaksin_campak_sebelum_sakit = str_replace('X', '', $val['22']);
						}
						$no_epid = Campak::getEpid(['id_kelurahan'=>$id_kelurahan,'date'=>$tgl_sakit]);
						$klasifikasi_final = null;
						if($val['49']=='Campak (Lab)'){
							$klasifikasi_final = '1';
						}elseif($val['49']=='Campak (Epid)'){
							$klasifikasi_final = '2';
						}elseif($val['49']=='Campak (Klinis)'){
							$klasifikasi_final = '3';
						}elseif($val['49']=='Rubella'){
							$klasifikasi_final = '4';
						}elseif($val['49']=='Bukan campak/rubella'){
							$klasifikasi_final = '5';
						}elseif($val['49']=='Pending'){
							$klasifikasi_final = '6';
						}
						$status_kasus = null;
						if($val['43']=='Index'){
							$status_kasus = '1';
						}elseif($val['43']=='Bukan Index'){
							$status_kasus = '2';
						}
						$jenis_kasus = null;
						if($val['41']=='KLB'){
							$jenis_kasus = '1';
						}elseif($val['41']=='Bukan KLB'){
							$jenis_kasus = '2';
						}
						$vit_a = null;
						if($val['39']=='Ya'){
							$vit_a = '1';
						}elseif($val['39']=='Tidak'){
							$vit_a = '2';
						}
						$keadaan_akhir = null;
						if($val['40']=='Hidup'){
							$keadaan_akhir = '1';
						}elseif($val['40']=='Meninggal'){
							$keadaan_akhir = '2';
						}
						$type = 'puskesmas';
						$faskes_id = Sentry::getUser()->id_user;
						$dtCampak = [
						'no_epid' => $no_epid,
						'no_epid_lama' => $val[2],
						'nama_puskesmas' => $val[4],
						'id_outbreak' => null,
						'tanggal_imunisasi_terakhir' => Helper::changeDate($val['21']),
						'vaksin_campak_sebelum_sakit' => $vaksin_campak_sebelum_sakit,
						'tanggal_timbul_demam' => Helper::changeDate($val['23']),
						'tanggal_timbul_rash' => Helper::changeDate($val['24']),
						'klasifikasi_final' => $klasifikasi_final,
						'jenis_spesimen_serologi' => null,
						'tanggal_ambil_sampel_serologi' => null,
						'jenis_spesimen_virologi' => null,
						'tanggal_ambil_sampel_virologi' => null,
						'hasil_serologi_igm_campak' => null,
						'hasil_serologi_igm_rubella' => null,
						'hasil_virologi_igm_campak' => null,
						'hasil_virologi_igm_rubella' => null,
						'status_kasus' => $status_kasus,
						'jenis_kasus' => $jenis_kasus,
						'klb_ke' => $val['42'],
						'vitamin_A' => $vit_a,
						'keadaan_akhir' => $keadaan_akhir,
						'tanggal_laporan_diterima' => Helper::changeDate($val['37']),
						'tanggal_pelacakan' => Helper::changeDate($val['38']),
						'tanggal_periksa' => date('Y-m-d'),
						'id_tempat_periksa' => $faskes_id,
						'kode_faskes' => $type,
						'created_by' => Sentry::getUser()->id,
						'created_at' => date('Y-m-d H:i:s'),
						];
						$id_campak = DB::table('campak')->insertGetId($dtCampak);
						$id_hasil_campak = DB::table('hasil_uji_lab_campak')->insert(
							array(
								'id_campak' => $id_campak,
								'id_pasien' => $id_pasien,
								'created_by' => Sentry::getUser()->id,
								'created_at' => date('Y-m-d H:i:s'),
								)
							);
						$kecPasien = Input::get('id_kecamatan');
                		$kecFaskes = DB::table('puskesmas')->WHERE('puskesmas_id', $faskes_id)->PLUCK('kode_kec AS kecFaskes');
                		if ($kecPasien != $kecFaskes) {
                			DB::table('notification')->insert(
                				array(
                					'from_faskes_id' => $faskes_id,
                					'from_faskes_kec_id' => $kecFaskes,
                					'to_faskes_kec_id' => $kecPasien,
                					'type_id' => 'campak',
                					'global_id' => $id_campak,
                					'description' => '',
                					'submit_dttm' => date('Y-m-d H:i:s'),
                					)
                				);
                		};
					}
				}
			});
		}
		return Redirect::to('campak');
	}

	public function ExtractAfp()
	{
		if (Input::file('excel')) {
			Excel::load(Input::file('excel'), function ($reader) {
				$row=array();
				foreach ($reader->toArray() as $key => $val) {
					if (!in_array($key, array(0,1,2))) {
						$jenis_kelamin = null;
						if($val['11']=='Laki-laki'){
							$jenis_kelamin = '1';
						}elseif($val['11']=='Perempuan'){
							$jenis_kelamin = '2';
						}elseif($val['11']=='Tidak Jelas'){
							$jenis_kelamin = '3';
						}
						$kel = DB::table('located')->select('id_kelurahan')->WHERE(['kelurahan'=>$val['17'],'kecamatan'=>$val['18'],'kabupaten'=>$val['19'],'provinsi'=>$val['20']])->get();
						$id_kelurahan = (empty($kel))?null:$kel[0]->id_kelurahan;
						$dtPasien = [
						'nik'=>(isset($val['35']))?$val['35']:null,
						'nama_anak'=>$val['2'],
						'nama_ortu'=>$val['3'],
						'alamat'=>$val['4'],
						'tanggal_lahir'=>(isset($val['36']))?$val['36']:null,
						'umur'=>$val['9'],
						'umur_bln'=>$val['10'],
						'umur_hr'=>(isset($val['n']))?$val['n']:null,
						'jenis_kelamin'=>$jenis_kelamin,
						'id_kelurahan'=>$id_kelurahan,
						];
						$cekPasien = DB::table('pasien')->where(['nama_anak'=>$val['8']])->get();
						if(empty($cekPasien)){
							$id_pasien = DB::table('pasien')->insertGetId($dtPasien);
						}else{
							$id_pasien = $cekPasien[0]->id_pasien;
							$updatePasien = DB::table('pasien')->where(['id_pasien'=>$id_pasien])->update($dtPasien);
						}

						$dtCase = [
						
						];
					}
				}
			});
		}
		return Redirect::to('afp');
	}

	public function ExtractDifteri()
	{
		if (Input::file('excel')) {
			Excel::load(Input::file('excel'), function ($reader) {
				$row=array();
				foreach ($reader->toArray() as $key => $val) {
					if (!in_array($key, array(0,1,2))) {
						
					}
				}
			});
		}
		return Redirect::to('difteri');
	}

	public function ExtractTetanus()
	{
		if (Input::file('excel')) {
			Excel::load(Input::file('excel'), function ($reader) {
				$row=array();
				foreach ($reader->toArray() as $key => $val) {
					if (!in_array($key, array(0,1,2))) {
						
					}
				}
			});
		}
		return Redirect::to('tetanus');
	}

	public function ExtractCrs()
	{
		if (Input::file('excel')) {
			Excel::load(Input::file('excel'), function ($reader) {
				$row=array();
				foreach ($reader->toArray() as $key => $val) {
					if (!in_array($key, array(0,1,2))) {
						
					}
				}
			});
		}
		return Redirect::to('crs');
	}
}
?>