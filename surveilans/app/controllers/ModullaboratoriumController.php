<?php
class ModullaboratoriumController extends BaseController {
	
	public function getIndex()
	{
		return View::make('master_data.modullaboratorium');
	}
	
	public function getList()
	{
		$modullaboratorium = ModullaboratoriumModel::getListModullaboratorium();
		$current_page = Input::get('page');
		$current_search = Input::get('search_name');		
		return View::make('master_data.modullaboratorium_list',compact('modullaboratorium','current_page','current_search'));
	}
	
	public function getListGet()
	{
		$modullaboratorium = ModullaboratoriumModel::getListModullaboratorium();
		$current_page = Input::get('page');
		$current_search = Input::get('search_name');		
		return View::make('master_data.modullaboratorium_list',compact('modullaboratorium','current_page','current_search'));
	}
	
	public function ModullaboratoriumProcessForm(){
		$ret = array();
        if(Input::get('saved_id')) {
			$ret['command'] = "updating data";
			if(ModullaboratoriumModel::DoUpdateData()) {
				$ret['msg'] = "Sukses Update Data";
				$ret['status'] = "success";
			} else {
				$ret['msg'] = "Gagal Update Data";
				$ret['status'] = "error";
			}
        } else {
			$ret['command'] = "adding data";
			$last_id = ModullaboratoriumModel::DoAddData();
			if($last_id > 0) {
				$ret['msg'] = "Sukses Simpan Data";
				$ret['status'] = "success";
			} else {
				$ret['msg'] = "Gagal Simpan Data";
				$ret['status'] = "error";
			}
        }
		
		echo json_encode($ret);
		
	}
	
	public function ModullaboratoriumGetDataById(){
		$provinsi = ModullaboratoriumModel::getModullaboratorium();
		echo json_encode($provinsi);
	}
	
	public function ModullaboratoriumDeleteList(){
		ModullaboratoriumModel::DoDeleteModullaboratorium();
		$ret['msg'] = "Sukses Delete Data";
		$ret['status'] = "success";
		echo json_encode($ret);
	}
	
}
?>
