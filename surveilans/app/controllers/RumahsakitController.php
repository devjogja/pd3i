<?php

class RumahsakitController extends \BaseController {

	//load view
	protected $layout = "layouts.master"; 
	
	public function __construct()
	{
		//filter
		$this->beforeFilter('auth');
	}
	/**
	 * Display a listing of rumahsakit
	 *
	 * @return Response
	 */
	public function index()
	{
		//$rumahsakit = Rumahsakit::all();
		$rumahsakit = Rumahsakit::getRumahsakit();
		$jml = Rumahsakit::getSumRM();	
		$this->layout->content = View::make('rumahsakit.index',compact('rumahsakit'))
		->with('jml',$jml);
		
	}

	/**
	 * Show the form for creating a new rumahsakit
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('rumahsakit.create');
	}

	/**
	 * Store a newly created rumahsakit in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Rumahsakit::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Rumahsakit::create($data);

		return Redirect::route('rumahsakit.index');
	}

	/**
	 * Display the specified rumahsakit.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$rumahsakit = Rumahsakit::findOrFail($id);

		return View::make('rumahsakit.show', compact('rumahsakit'));
	}

	/**
	 * Show the form for editing the specified rumahsakit.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$rumahsakit = Rumahsakit::find($id);

		return View::make('rumahsakit.edit', compact('rumahsakit'));
	}

	public function updateWilayah(){
		$rs = DB::table('rumahsakit2')->get();
		foreach ($rs as $key => $val) {
			$id_kab = substr($val->kode_faskes, 0, 4);
			$id_provinsi = DB::table('kabupaten')->WHERE('id_kabupaten',$id_kab)->pluck('id_provinsi');
			$push = ['kode_kab'=>$id_kab,'kode_prop'=>$id_provinsi];
			DB::table('rumahsakit2')->WHERE('id',$val->id)->update($push);
		}
		return 'Success';
	}

	/**
	 * Update the specified rumahsakit in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rumahsakit = Rumahsakit::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Rumahsakit::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$rumahsakit->update($data);

		return Redirect::route('rumahsakit.index');
	}

	/**
	 * Remove the specified rumahsakit from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Rumahsakit::destroy($id);

		return Redirect::route('rumahsakit.index');
	}

}
