<?php

class KabupatenController extends \BaseController {

	public function __construct()
	{
		//filter
		$this->beforeFilter('auth');
	}
	
	/**
	 * Display a listing of kabupatens
	 *
	 * @return Response
	 */
	public function index()
	{
		/*$kabupaten = DB::table('users')
					->join('sub_kabupaten','users.id','=','sub_kabupaten.id_user')
					->join('users_groups','users.id','=','users_groups.user_id')
					->where('users_groups.group.id','=',2)
					->select('sub_kabupaten.id_kab.','users.email','sub_kabupaten.telepon','sub_kabupaten.nama_petugas');
		//$kabupatens = Subkabupaten::all();
		*/
		
		$kabupaten = DB::select('select users.email,kabupaten.kabupaten,sub_kabupaten.nama_petugas,sub_kabupaten.telepon
								from users,sub_kabupaten,users_groups,kabupaten
								where users.id=sub_kabupaten.id_user and users.id=users_groups.user_id and sub_kabupaten.id_kab=kabupaten.id_kabupaten and users_groups.group_id=2');
		return View::make('kabupaten.index', compact('kabupaten'));
	}
	
	/**
	 * Show the form for creating a new kabupaten
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('kabupaten.create');
	}

	/**
	 * Store a newly created kabupaten in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Subkabupaten::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		// Membuat user regular baru
		$user = Sentry::register(array(
		// silahkan ganti sesuai keinginan
		'email' => Input::get('username'),
		'password' => Input::get('password')
		), true); // langsung diaktivasi
		// Cari grup regular
		$regularGroup = Sentry::findGroupByName('regular');
		// Masukkan user ke grup regular
		$user->addGroup($regularGroup);
		//ambil id yang terakhir masuk
		//$sql = DB::statement('select max(id) from users');
		foreach(DB::select("select max(id) as id from users") as $id){
			$id=$id->id;
		}
		//simpan ke table sub_kabupaten
		DB::table('sub_kabupaten')->insert(array(
			'id_kab'=>Input::get('id_kab'),
			'id_user'=>$id));
		
		return Redirect::route('kabupaten.index');
	}

	//akun petugas untuk level kabupaten
	public function postDaftarAkun()
	{
		
		// Membuat user regular baru
		$user = Sentry::register(array(
		// silahkan ganti sesuai keinginan
		'email' => 'shidqi.abdullah@gmail.com',
		'password' => '#larapus2014',
		'first_name' => 'Shidqi',
		'last_name' => 'Abdullah Mubarak'
		), true); // langsung diaktivasi
		// Cari grup regular
		$regularGroup = Sentry::findGroupByName('regular');
		// Masukkan user ke grup regular
		$user->addGroup($regularGroup);
		
			
	}

	/**
	 * Display the specified kabupaten.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$kabupaten = Subkabupaten::findOrFail($id);

		return View::make('kabupaten.show', compact('kabupaten'));
	}

	/**
	 * Show the form for editing the specified kabupaten.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$kabupaten = Subkabupaten::find($id);

		return View::make('kabupaten.edit', compact('kabupaten'));
	}

	/**
	 * Update the specified kabupaten in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$kabupaten = Subkabupaten::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Subkabupaten::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$kabupaten->update($data);

		return Redirect::route('kabupaten.index');
	}

	/**
	 * Remove the specified kabupaten from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Subkabupaten::destroy($id);

		return Redirect::route('kabupaten.index');
	}

}
