<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}

	public function removeDataLatihan()
	{
		$campak = DB::update("UPDATE campak AS a
						JOIN hasil_uji_lab_campak AS b ON a.id_campak=b.id_campak
						JOIN pasien AS c ON b.id_pasien=c.id_pasien
						LEFT JOIN notification AS d ON a.id_campak=d.global_id
						LEFT JOIN pe_campak AS e ON a.no_epid=e.no_epid
						SET
						a.deleted_at=NOW(), a.deleted_by='0',
						b.deleted_at=NOW(), b.deleted_by='0',
						c.deleted_at=NOW(), c.deleted_by='0',
						d.deleted_at=NOW(), d.deleted_by='0',
						e.deleted_at=NOW(), e.deleted_by='0'
						WHERE c.nama_anak LIKE '%coba%'");
		$afp = DB::update("UPDATE afp AS a
						JOIN hasil_uji_lab_afp AS b ON a.id_afp=b.id_afp
						JOIN pasien AS c ON b.id_pasien=c.id_pasien
						LEFT JOIN notification AS d ON a.id_afp=d.global_id
						LEFT JOIN pe_afp AS e ON a.no_epid=e.no_epid
						SET
						a.deleted_at=NOW(), a.deleted_by='0',
						b.deleted_at=NOW(), b.deleted_by='0',
						c.deleted_at=NOW(), c.deleted_by='0',
						d.deleted_at=NOW(), d.deleted_by='0',
						e.deleted_at=NOW(), e.deleted_by='0'
						WHERE c.nama_anak LIKE '%coba%'");

		$difteri = DB::update("UPDATE difteri AS a
						JOIN hasil_uji_lab_difteri AS b ON a.id_difteri=b.id_difteri
						JOIN pasien AS c ON b.id_pasien=c.id_pasien
						LEFT JOIN notification AS d ON a.id_difteri=d.global_id
						LEFT JOIN pe_difteri AS e ON a.no_epid=e.no_epid
						SET
						a.deleted_at=NOW(), a.deleted_by='0',
						c.deleted_at=NOW(), c.deleted_by='0',
						d.deleted_at=NOW(), d.deleted_by='0'
						WHERE c.nama_anak LIKE '%coba%'");

		$crs = DB::update("UPDATE crs AS a
						JOIN hasil_uji_lab_crs AS b ON a.id_hasil_uji_lab_crs=b.id_hasil_uji_lab_crs
						JOIN pasien AS c ON a.id_pasien=c.id_pasien
						LEFT JOIN notification AS d ON a.id_crs=d.global_id
						LEFT JOIN pe_crs AS e ON a.id_pe_crs=e.id_pe_crs
						SET
						a.deleted_at=NOW(), a.deleted_by='0',
						b.deleted_at=NOW(), b.deleted_by='0',
						c.deleted_at=NOW(), c.deleted_by='0',
						d.deleted_at=NOW(), d.deleted_by='0',
						e.deleted_at=NOW(), e.deleted_by='0'
						WHERE c.nama_anak LIKE '%coba%'");
		$tetanus = DB::update("
						UPDATE tetanus AS a
						JOIN pasien_terserang_tetanus AS b ON a.id_tetanus=b.id_tetanus
						JOIN pasien AS c ON b.id_pasien_tetanus=c.id_pasien
						LEFT JOIN notification AS d ON a.id_tetanus=d.global_id
						LEFT JOIN pe_tetanus AS e ON a.no_epid=e.no_epid
						SET
						a.deleted_at=NOW(), a.deleted_by='0',
						c.deleted_at=NOW(), c.deleted_by='0',
						d.deleted_at=NOW(), d.deleted_by='0'
						WHERE c.nama_anak LIKE '%coba%'
			");
		return 'success';
	}
}
