<?php
class ModulkabupatenController extends BaseController {
	
	public function getIndex()
	{
		return View::make('master_data.modulkabupaten');
	}
	
	public function getList()
	{
		$modulkabupaten = ModulkabupatenModel::getListModulkabupaten();
		$current_page = Input::get('page');
		$current_search = Input::get('search_name');		
		return View::make('master_data.modulkabupaten_list',compact('modulkabupaten','current_page','current_search'));
	}
	
	public function getListGet()
	{
		$modulkabupaten = ModulkabupatenModel::getListModulkabupaten();
		$current_page = Input::get('page');
		$current_search = Input::get('search_name');		
		return View::make('master_data.modulkabupaten_list',compact('modulkabupaten','current_page','current_search'));
	}
	
	public function ModulkabupatenProcessForm(){
		$ret = array();
        if(Input::get('saved_id')) {
			$ret['command'] = "updating data";
			if(ModulkabupatenModel::DoUpdateData()) {
				$ret['msg'] = "Sukses Update Data";
				$ret['status'] = "success";
			} else {
				$ret['msg'] = "Gagal Update Data";
				$ret['status'] = "error";
			}
        } else {
			$ret['command'] = "adding data";
			$last_id = ModulkabupatenModel::DoAddData();
			if($last_id > 0) {
				$ret['msg'] = "Sukses Simpan Data";
				$ret['status'] = "success";
			} else {
				$ret['msg'] = "Gagal Simpan Data";
				$ret['status'] = "error";
			}
        }
		
		echo json_encode($ret);
		
	}
	
	public function ModulkabupatenGetDataById(){
		$provinsi = ModulkabupatenModel::getModulkabupaten();
		echo json_encode($provinsi);
	}
	
	public function ModulkabupatenDeleteList(){
		ModulkabupatenModel::DoDeleteModulkabupaten();
		$ret['msg'] = "Sukses Delete Data";
		$ret['status'] = "success";
		echo json_encode($ret);
	}
	
}
?>
