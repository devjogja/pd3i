<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	public function changeDate($date) {
		return !empty($date) ? "'".date('Y-m-d',strtotime($date))."'" : 'NULL';
	}

	function encode_date($date){
		return empty($date) ? NULL : date('Y-m-d', strtotime($date));
	}

	function set_value($val){
		return empty($val) ? NULL : $val;
	}

}
