<?php

class CobaController extends BaseController {
	
	public function getIndex()
	{
		return View::make('test.coba');
	}

	public function getMaps()
	{
		return View::make('test.testmaps');
	}

	public function getList()
	{
		$coba = Coba::getListCoba();
		$current_page = Input::get('page');
		$current_search = Input::get('search_name');		
		return View::make('test.coba_list',compact('coba','current_page','current_search'));
	}
	
	public function getListGet()
	{
		$coba = Coba::getListCoba();
		$current_page = Input::get('page');
		$current_search = Input::get('search_name');		
		return View::make('test.coba_list',compact('coba','current_page','current_search'));
	}

	public function execute()
	{
		$data = '21124';
		$secretKey = 'div7_12345';

		date_default_timezone_set('UTC');
		$timestamp = strval(time()-strtotime('1970-01-01 00:00:00'));

		$signature = hash_hmac('sha256', $data."&".$timestamp, $secretKey, true);
		$encodedSignature = base64_encode($signature);
		echo "<pre>";print_r($timestamp);echo "</pre>";die();
		
		$signature = $encodedSignature;
		$username = '12030802';
		$pass = 'askes';
		$kode = '095';
		$auth = 'Basic '.base64_encode($username.':'.$pass.':'.$kode);
		echo "<pre>";print_r($auth);echo "</pre>";die();
	}
	
	public function CobaProcessForm(){
		$ret = array();
        if(Input::get('saved_id')) {
			$ret['command'] = "updating data";
			if(Coba::DoUpdateData()) {
				$ret['msg'] = "Sukses Update Data";
				$ret['status'] = "success";
			} else {
				$ret['msg'] = "Gagal Update Data";
				$ret['status'] = "error";
			}
        } else {
			$ret['command'] = "adding data";
			$last_id = Coba::DoAddData();
			if($last_id > 0) {
				$ret['msg'] = "Sukses Simpan Data";
				$ret['status'] = "success";
			} else {
				$ret['msg'] = "Gagal Simpan Data";
				$ret['status'] = "error";
			}
        }
		
		echo json_encode($ret);
		
	}
	
	public function CobaGetDataById(){
		$provinsi = Coba::getCoba();
		echo json_encode($provinsi);
	}
	
	public function CobaDeleteList(){
		Coba::DoDeleteCoba();
		$ret['msg'] = "Sukses Delete Data";
		$ret['status'] = "success";
		echo json_encode($ret);
	}
	
}