<?php
class ModulkelurahanController extends BaseController {
	
	public function getIndex()
	{
		return View::make('master_data.modulkelurahan');
	}
	
	public function getList()
	{
		$modulkelurahan = ModulkelurahanModel::getListModulkelurahan();
		$current_page = Input::get('page');
		$current_search = Input::get('search_name');		
		return View::make('master_data.modulkelurahan_list',compact('modulkelurahan','current_page','current_search'));
	}
	
	public function getListGet()
	{
		$modulkelurahan = ModulkelurahanModel::getListModulkelurahan();
		$current_page = Input::get('page');
		$current_search = Input::get('search_name');		
		return View::make('master_data.modulkelurahan_list',compact('modulkelurahan','current_page','current_search'));
	}
	
	public function ModulkelurahanProcessForm(){
		$ret = array();
        if(Input::get('saved_id')) {
			$ret['command'] = "updating data";
			if(ModulkelurahanModel::DoUpdateData()) {
				$ret['msg'] = "Sukses Update Data";
				$ret['status'] = "success";
			} else {
				$ret['msg'] = "Gagal Update Data";
				$ret['status'] = "error";
			}
        } else {
			$ret['command'] = "adding data";
			$last_id = ModulkelurahanModel::DoAddData();
			if($last_id > 0) {
				$ret['msg'] = "Sukses Simpan Data";
				$ret['status'] = "success";
			} else {
				$ret['msg'] = "Gagal Simpan Data";
				$ret['status'] = "error";
			}
        }
		
		echo json_encode($ret);
		
	}
	
	public function ModulkelurahanGetDataById(){
		$provinsi = ModulkelurahanModel::getModulkelurahan();
		echo json_encode($provinsi);
	}
	
	public function ModulkelurahanDeleteList(){
		ModulkelurahanModel::DoDeleteModulkelurahan();
		$ret['msg'] = "Sukses Delete Data";
		$ret['status'] = "success";
		echo json_encode($ret);
	}
	
}
?>
