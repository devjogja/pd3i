<?php

class DifteriController extends \BaseController
{

    public function __construct()
    {
        $this->beforeFilter('auth');
    }

    /**
     * Display a listing of Campak
     *
     * @return Response
     */
    public function index()
    {
        Session::put('sess_id', '');
        Session::put('penyakit', 'Difteri');
        Session::put('sess_penyakit', 'difteri');
        $between = '';
        $district = '';
        if (!empty(Input::get())) {
            $unit = Input::get('unit');
            $day_start = Input::get('day_start');
            $month_start = Input::get('month_start');
            $year_start = Input::get('year_start');
            $day_end = Input::get('day_end');
            $month_end = Input::get('month_end');
            $year_end = Input::get('year_end');
            $province_id = Input::get('province_id');
            $district_id = Input::get('district_id');
            $sub_district_id = Input::get('sub_district_id');
            $rs_id = Input::get('rs_id');
            $puskesmas_id = Input::get('puskesmas_id');

            switch ($unit) {
                case "all":
                    $between = "";
                    break;
                case "year":
                    $start = $year_start;
                    $end = $year_end;
                    $between = " AND YEAR(difteri_tanggal_periksa) BETWEEN '" . $start . "' AND '" . $end . "' ";
                    break;
                case "month":
                    $start = $year_start . '-' . $month_start . '-1';
                    $end = $year_end . '-' . $month_end . '-' . date('t', mktime(0, 0, 0, $month_end, 1, $year_end));
                    $between = " AND DATE(difteri_tanggal_periksa) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                    break;
                default:
                    $start = $year_start . '-' . $month_start . '-' . $day_start;
                    $end = $year_end . '-' . $month_end . '-' . $day_end;
                    $between = " AND DATE(difteri_tanggal_periksa) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                    break;
            }

            if ($puskesmas_id) {
                $district = "AND puskesmas_code_faskes='" . $puskesmas_id . "'";
            } else if ($sub_district_id) {
                $district = "AND pasien_id_kecamatan='" . $sub_district_id . "'";
            } else if ($rs_id) {
                $district = "AND rs_kode_faskes='" . $rs_id . "' ";
            } else if ($district_id) {
                $district = "AND pasien_id_kabupaten='" . $district_id . "' ";
            } else if ($province_id) {
                $district = "AND pasien_id_provinsi='" . $province_id . "' ";
            }
        }
        $data = Difteri::pasiendifteri($between, $district);
        return View::make('pemeriksaan.difteri.index', compact('data'));
    }

    /**
     * Show the form for creating a new Campak
     *
     * @return Response
     */
    public function create()
    {
        return View::make('pemeriksaan.difteri.create');
    }

    /**
     * Store a newly created Campak in storage.
     *
     * @return Response
     */
    public function store()
    {
        //lakukan transaksi
        DB::transaction(function () {
            $type = Session::get('type');
            $faskes_id = Sentry::getUser()->id_user;

            //simpan data personal
            $id_pasien = DB::table('pasien')->insertGetId(array(
                'nik' => Input::get('nik'),
                'nama_anak' => Input::get('nama_anak'),
                'nama_ortu' => Input::get('nama_ortu'),
                'alamat' => Input::get('alamat'),
                'tanggal_lahir' => $this->encode_date(Input::get('tanggal_lahir')),
                'umur' => Input::get('tgl_tahun'),
                'umur_bln' => Input::get('tgl_bulan'),
                'umur_hr' => Input::get('tgl_hari'),
                'jenis_kelamin' => Input::get('jenis_kelamin'),
                'id_kelurahan' => Input::get('id_kelurahan'),
            ));

            //simpan data penyakit dfiteri
            $dd= array(
                'no_epid' => Input::get('no_epid'),
                'no_epid_lama' => Input::get('no_epid_lama'),
                'tanggal_timbul_demam' => $this->encode_date(Input::get('tanggal_timbul_demam')),
                'tanggal_vaksinasi_difteri_terakhir' => $this->encode_date(Input::get('tanggal_vaksinasi_difteri_terakhir')),
                'tanggal_pelacakan' => $this->encode_date(Input::get('tanggal_pelacakan')),
                'gejala_lain' => Input::get('gejala_lain'),
                'vaksin_DPT_sebelum_sakit' => Input::get('vaksin_DPT_sebelum_sakit'),
                'jumlah_kontak' => Input::get('jumlah_kontak'),
                'jumlah_swab_hidung' => Input::get('jumlah_swab_hidung'),
                'jumlah_positif' => Input::get('jumlah_positif'),
                'keadaan_akhir' => Input::get('keadaan_akhir'),
                'klasifikasi_final' => Input::get('klasifikasi_final'),
                'tanggal_periksa' => date('Y-m-d'),
                'id_tempat_periksa' => $faskes_id,
                'kode_faskes' => $type,
                'created_by' => Sentry::getUser()->id,
                'updated_by' => Sentry::getUser()->id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            );
            $dtd = [];
            foreach ($dd as $key=>$val) {
                if (!empty($val)) {
                    $dtd[$key]=$val;
                }
            }
            $id_difteri = DB::table('difteri')->insertGetId($dtd);

            if (isset($_POST['spesimen'])) {
                $dtSpesimen = $_POST['spesimen'];
                if (is_array($dtSpesimen['jenis_spesimen'])) {
                    foreach ($dtSpesimen['jenis_spesimen'] as $key => $val) {
                        $ds[$key]['jenis_spesimen'] = $val;
                        $ds[$key]['id_case'] = $id_difteri;
                        $ds[$key]['created_by'] = Sentry::getUser()->id;
                        $ds[$key]['updated_by'] = Sentry::getUser()->id;
                        $ds[$key]['created_at'] = date('Y-m-d H:i:s');
                        $ds[$key]['updated_at'] = date('Y-m-d H:i:s');
                    }
                }
                if (is_array($dtSpesimen['tgl_ambil_spesimen'])) {
                    foreach ($dtSpesimen['tgl_ambil_spesimen'] as $key => $val) {
                        $ds[$key]['tgl_ambil_spesimen'] = $this->changeDate($val);
                    }
                }
                if (is_array($dtSpesimen['jenis_pemeriksaan'])) {
                    foreach ($dtSpesimen['jenis_pemeriksaan'] as $key => $val) {
                        $ds[$key]['jenis_pemeriksaan'] = $val;
                    }
                }
                if (is_array($dtSpesimen['hasil_lab'])) {
                    foreach ($dtSpesimen['hasil_lab'] as $key => $val) {
                        $ds[$key]['hasil_lab'] = $val;
                    }
                }
                DB::table('trx_spesimen')->insert($ds);
            }

            //simpan id_pasien dan id_difteri ke table hasil_uji_lab_difteri
            DB::table('hasil_uji_lab_difteri')->insert(array(
                'id_pasien' => $id_pasien,
                'id_difteri' => $id_difteri,
            ));

            //simpan notifikasi
            if ($type == 'rs') {
                $data = DB::SELECT("SELECT pasien_id_kecamatan AS id_dis_pasien, rs_kode_faskes AS id_dis_faskes FROM getdetaildifteri WHERE difteri_id_difteri=$id_difteri");
            } else {
                $data = DB::SELECT("SELECT pasien_id_kecamatan AS id_dis_pasien, puskesmas_code_faskes AS id_dis_faskes FROM getdetaildifteri WHERE difteri_id_difteri=$id_difteri");
            }

            $id_kec_pasien = (empty($data) ? '' : $data[0]->id_dis_pasien);
            $id_kec_faskes = (empty($data) ? '' : $data[0]->id_dis_faskes);

            if ($id_kec_pasien != $id_kec_faskes) {
                DB::table('notification')->insert(array(
                    'from_faskes_id' => $faskes_id,
                    'from_faskes_kec_id' => $id_kec_faskes,
                    'to_faskes_kec_id' => $id_kec_pasien,
                    'type_id' => 'difteri',
                    'global_id' => $id_difteri,
                    'description' => '',
                    'submit_dttm' => date('Y-m-d H:i:s'),
                ));
            }
        });

        //berhasil disimpan
        return Redirect::to('difteri');
    }

    public function simpanUjiDifteri()
    {

        //simpan hasil laboratorium
        DB::table('hasil_uji_lab_difteri')->insert(array(
            'id_pasien' => Input::get('id_pasien'),
            'id_difteri' => Input::get('id_difteri'),
            'hasil_spesimen_tenggorokan' => Input::get('hasil_spesimen_tenggorokan'),
            'hasil_spesimen_hidung' => Input::get('hasil_spesimen_hidung'),
            'klasifikasi_akhir' => Input::get('klasifikasi_akhir'),
            'tanggal_uji_laboratorium' => $this->encode_date(Input::get('tanggal_uji_laboratorium')),
        ));
    }

    /**
     * Display the specified Campak.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $campak = Campak::findOrFail($id);
        return View::make('pemeriksaan.difteri.show', compact('campak'));
    }

    /**
     * Show the form for editing the specified Campak.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $campak = Campak::find($id);
        return View::make('pemeriksaan.difteri.edit', compact('campak'));
    }

    /**
     * Update the specified Campak in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $campak = Campak::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Campak::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $campak->update($data);

        return Redirect::route('pemeriksaan.difteri.index');
    }

    /**
     * Remove the specified Campak from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Campak::destroy($id);
        return Redirect::route('pemeriksaan.difteri.index');
    }

    public function postForm()
    {
        $id = Input::get('id');
        if ($id == 1) {
            return View::make('diagnosa.campak');
        }
    }

    public function detaildifteri($id)
    {
        $difteri = DB::select("SELECT
                                difteri.no_epid,
                                difteri.no_epid_lama,
                                pasien.nik,
                                pasien.tanggal_lahir,
                                pasien.nama_anak,
                                pasien.nama_ortu,
                                CONCAT_WS(',',pasien.alamat,lokasi.kelurahan,lokasi.kecamatan,lokasi.kabupaten,lokasi.provinsi) as alamat_pasien,
                                pasien.umur,
                                pasien.umur_bln,
                                pasien.umur_hr,
                                pasien.jenis_kelamin,
                                difteri.tanggal_timbul_demam,
                                difteri.tanggal_diambil_spesimen_tenggorokan,
                                difteri.tanggal_diambil_spesimen_hidung,
                                difteri.tanggal_vaksinasi_difteri_terakhir,
                                difteri.vaksin_DPT_sebelum_sakit,
                                difteri.keadaan_akhir,
                                difteri.tanggal_periksa,
                                difteri.tanggal_pelacakan,
                                difteri.hasil_lab_kultur_tenggorokan,
                                difteri.hasil_lab_kultur_hidung,
                                difteri.hasil_lab_mikroskop_tenggorokan,
                                difteri.hasil_lab_mikroskop_hidung,
                                difteri.jumlah_kontak,
                                difteri.jumlah_swab_hidung,
                                difteri.jumlah_positif,
                                hasil_uji_lab_difteri.id_hasil_uji_lab_difteri,
                                hasil_uji_lab_difteri.id_pasien,
                                hasil_uji_lab_difteri.id_difteri,
                                hasil_uji_lab_difteri.no_spesimen,
                                hasil_uji_lab_difteri.hasil_spesimen_tenggorokan,
                                hasil_uji_lab_difteri.hasil_spesimen_hidung,
                                hasil_uji_lab_difteri.klasifikasi_akhir,
                                hasil_uji_lab_difteri.tanggal_uji_laboratorium,
                                hasil_uji_lab_difteri.status,
                                hasil_uji_lab_difteri.id_uji_lab
                            FROM
                                difteri,
                                pasien,
                                lokasi,
                                hasil_uji_lab_difteri,
                                users
                            WHERE
                                pasien.id_pasien=hasil_uji_lab_difteri.id_pasien
                            AND
                                lokasi.id_kelurahan=pasien.id_kelurahan
                            AND
                                difteri.id_difteri=hasil_uji_lab_difteri.id_difteri
                            AND
                                users.email ='" . Sentry::getUser()->email . "'
                            AND
                                difteri.id_difteri ='" . $id . "'");

        return View::make('pemeriksaan.difteri.detail', compact('difteri'));
    }

    public function getHapus($id)
    {
        $dt['deleted_by'] = Sentry::getUser()->id;
        $dt['deleted_at'] = date('Y-m-d H:i:s');
        $update = DB::table('difteri')
            ->where('id_difteri', $id)
            ->update($dt);
        $delete_notif = DB::table('notification')
            ->where('global_id', $id)
            ->where('type_id', 'difteri')
            ->update($dt);
        return Redirect::to('difteri');
    }

    /*public function getHapus($id,$id_lab)
    {

    try {
    DB::beginTransaction();
    DB::delete("delete from hasil_uji_lab_difteri where hasil_uji_lab_difteri.id_hasil_uji_lab_difteri='".$id_lab."'");
    DB::delete("delete from difteri where difteri.id_difteri ='".$id."'");
    DB::delete("delete from gejala_difteri where id_difteri ='".$id."'");
    DB::commit();
    } catch (Exception $e) {
    DB::rollback();
    }

    return Redirect::to('difteri');
    }*/

    //get edit difteri
    public function getEditDifteri($id)
    {
        $difteri = DB::select("SELECT
                                difteri.no_epid,
                                difteri.no_epid_lama,
                                pasien.nik,
                                pasien.tanggal_lahir,
                                pasien.nama_anak,
                                pasien.nama_ortu,
                                pasien.alamat,
                                pasien.umur,
                                pasien.umur_bln,
                                pasien.umur_hr,
                                lokasi.provinsi,
                                lokasi.kabupaten,
                                lokasi.kecamatan,
                                lokasi.kelurahan,
                                lokasi.id_kelurahan,
                                lokasi.id_kecamatan,
                                lokasi.id_kabupaten,
                                pasien.jenis_kelamin,
                                difteri.tanggal_timbul_demam,
                                difteri.tanggal_diambil_spesimen_tenggorokan,
                                difteri.tanggal_diambil_spesimen_hidung,
                                difteri.tanggal_vaksinasi_difteri_terakhir,
                                difteri.tanggal_pelacakan,
                                difteri.jumlah_kontak,
                                difteri.jumlah_swab_hidung,
                                difteri.jumlah_positif,
                                difteri.jenis_pemeriksaan,
                                difteri.tipe_spesimen,
                                difteri.hasil_lab_kultur_tenggorokan,
                                difteri.hasil_lab_kultur_hidung,
                                difteri.hasil_lab_mikroskop_tenggorokan,
                                difteri.hasil_lab_mikroskop_hidung,
                                difteri.klasifikasi_final,
                                difteri.gejala_lain,
                                difteri.vaksin_DPT_sebelum_sakit,
                                difteri.keadaan_akhir,
                                hasil_uji_lab_difteri.id_hasil_uji_lab_difteri
                            FROM
                                difteri,
                                pasien,
                                lokasi,
                                hasil_uji_lab_difteri,
                                users
                            WHERE
                                pasien.id_pasien=hasil_uji_lab_difteri.id_pasien
                            AND
                                lokasi.id_kelurahan=pasien.id_kelurahan
                            AND
                                difteri.id_difteri=hasil_uji_lab_difteri.id_difteri
                            AND
                                users.email ='" . Sentry::getUser()->email . "'
                            AND
                                difteri.id_difteri ='" . $id . "'");

        return View::make('pemeriksaan.difteri.edit', compact('difteri'));
    }

    //post edit difteri
    public function updateDifteri()
    {
        DB::transaction(function () {
            DB::update("update
                            pasien,difteri,hasil_uji_lab_difteri
                        set
                            difteri.no_epid='" . Input::get('no_epid') . "',
                            difteri.no_epid_lama='" . Input::get('no_epid_lama') . "',
                            pasien.nik='" . Input::get('nik') . "',
                            pasien.tanggal_lahir=" . $this->changeDate(Input::get('tanggal_lahir')) . ",
                            pasien.nama_anak='" . Input::get('nama_anak') . "',
                            pasien.nama_ortu='" . Input::get('nama_ortu') . "',
                            pasien.alamat='" . Input::get('alamat') . "',
                            pasien.umur='" . Input::get('tgl_tahun') . "',
                            pasien.umur_bln='" . Input::get('tgl_bulan') . "',
                            pasien.umur_hr='" . Input::get('tgl_hari') . "',
                            pasien.id_kelurahan='" . Input::get('id_kelurahan') . "',
                            pasien.jenis_kelamin='" . Input::get('jenis_kelamin') . "',
                            difteri.tanggal_timbul_demam=" . $this->changeDate(Input::get('tanggal_timbul_demam')) . ",
                            difteri.tanggal_diambil_spesimen_tenggorokan=" . $this->changeDate(Input::get('tanggal_diambil_spesimen_tenggorokan')) . ",
                            difteri.tanggal_diambil_spesimen_hidung=" . $this->changeDate(Input::get('tanggal_diambil_spesimen_hidung')) . ",
                            difteri.tanggal_vaksinasi_difteri_terakhir=" . $this->changeDate(Input::get('tanggal_vaksinasi_difteri_terakhir')) . ",
                            difteri.tanggal_pelacakan=" . $this->changeDate(Input::get('tanggal_pelacakan')) . ",
                            difteri.jumlah_kontak='" . Input::get('jumlah_kontak') . "',
                            difteri.jumlah_swab_hidung='" . Input::get('jumlah_swab_hidung') . "',
                            difteri.jumlah_positif='" . Input::get('jumlah_positif') . "',
                            difteri.jenis_pemeriksaan='" . Input::get('jenis_pemeriksaan') . "',
                            difteri.tipe_spesimen='" . Input::get('tipe_spesimen') . "',
                            difteri.hasil_lab_kultur_tenggorokan='" . Input::get('hasil_lab_kultur_tenggorokan') . "',
                            difteri.hasil_lab_kultur_hidung='" . Input::get('hasil_lab_kultur_hidung') . "',
                            difteri.hasil_lab_mikroskop_tenggorokan='" . Input::get('hasil_lab_mikroskop_tenggorokan') . "',
                            difteri.hasil_lab_mikroskop_hidung='" . Input::get('hasil_lab_mikroskop_hidung') . "',
                            difteri.klasifikasi_final='" . Input::get('klasifikasi_final') . "',
                            difteri.gejala_lain='" . Input::get('gejala_lain') . "',
                            difteri.vaksin_DPT_sebelum_sakit='" . Input::get('vaksin_DPT_sebelum_sakit') . "',
                            difteri.keadaan_akhir='" . Input::get('keadaan_akhir') . "'
                        where
                            pasien.id_pasien=hasil_uji_lab_difteri.id_pasien
                        and
                            difteri.id_difteri=hasil_uji_lab_difteri.id_difteri
                        and
                            hasil_uji_lab_difteri.id_hasil_uji_lab_difteri='" . Input::get('id_hasil_uji_lab_difteri') . "'");
        });

        return Redirect::to('difteri');
    }

    //route daftar difteri
    public function getDaftarDifteri()
    {
        $pe_difteri = Difteri::getPeDifteri();
        return View::make('pemeriksaan.difteri.daftar', compact('pe_difteri'));
    }

    //fungsi untuk mengambil data pasien yang akan dilakukan penyelidikan epidemologi difteri
    public function getPEdifteri($id)
    {
        $pe_difteri = DB::select("SELECT
                                difteri.no_epid,
                                pasien.nik,
                                pasien.id_pasien,
                                pasien.tanggal_lahir,
                                pasien.nama_anak,
                                difteri.id_difteri,
                                difteri.tanggal_timbul_demam,
                                pasien.nama_ortu,
                                pasien.alamat,
                                lokasi.kelurahan,
                                lokasi.kecamatan,
                                lokasi.kabupaten,
                                lokasi.provinsi,
                                lokasi.id_kelurahan,
                                pasien.umur,
                                pasien.umur_bln,
                                pasien.umur_hr,
                                pasien.jenis_kelamin
                            FROM
                                difteri,
                                pasien,
                                lokasi,
                                hasil_uji_lab_difteri,
                                users
                            WHERE
                                pasien.id_pasien=hasil_uji_lab_difteri.id_pasien
                            AND
                                lokasi.id_kelurahan=pasien.id_kelurahan
                            AND
                                difteri.id_difteri=hasil_uji_lab_difteri.id_difteri
                            AND
                                users.email ='" . Sentry::getUser()->email . "'
                            AND
                                difteri.id_difteri ='" . $id . "'");

        return View::make('pemeriksaan.difteri.pe_difteri', compact('pe_difteri'));
    }

    //fungsi untuk menyimpan data kasus penyelidikan epidemologi difteri
    public function postEpidDifteri()
    {
        $tanggal_mulai_sakit = Input::get('tanggal_mulai_sakit');
        if (empty($tanggal_mulai_sakit) || $tanggal_mulai_sakit == '1970-01-01') {
            return Redirect::to('difteri');
        } else {
            $no_epid = Input::get('no_epid');
            $id_difteri = Input::get('id_difteri');

            $cek = DB::select("select
                        difteri.id_difteri,
                        no_epid
                    from
                        difteri,
                        pasien,
                        hasil_uji_lab_difteri
                    where
                        difteri.id_difteri=hasil_uji_lab_difteri.id_difteri
                    and
                        pasien.id_pasien=hasil_uji_lab_difteri.id_pasien
                    and
                        difteri.id_difteri='" . $id_difteri . "'");

            if (empty($cek)) {
                DB::table('difteri')->insert(array('no_epid' => Input::get('no_epid')));
                $id_pasien = DB::table('pasien')->insertGetId(array(
                    'nama_anak' => Input::get('nama_anak'),
                    'nama_ortu' => Input::get('nama_ortu'),
                    'alamat' => Input::get('alamat'),
                    'tanggal_lahir' => $this->encode_date(Input::get('tanggal_lahir')),
                    'umur' => Input::get('tgl_tahun'),
                    'umur_bln' => Input::get('tgl_bln'),
                    'umur_hr' => Input::get('tgl_hr'),
                    'jenis_kelamin' => Input::get('jenis_kelamin'),
                    'id_kelurahan' => Input::get('id_kelurahan'),
                ));
                //simpan edit epid difteri
                $dt['no_epid'] = Input::get('no_epid');
                $dt['id_pasien'] = $id_pasien;
                $dt['created_by'] = Sentry::getUser()->id;
                $dt['telepon'] = Input::get('telepon');
                $dt['pekerjaan'] = Input::get('pekerjaan');
                $dt['alamat_tempat_kerja'] = Input::get('alamat_tempat_kerja');
                $dt['nama_saudara_yang_bisa_dihubungi'] = Input::get('nama_saudara_yang_bisa_dihubungi');
                $dt['alamat_saudara_yang_bisa_dihubungi'] = Input::get('alamat_saudara_yang_bisa_dihubungi');
                $dt['tlp_saudara_yang_bisa_dihubungi'] = Input::get('tlp_saudara_yang_bisa_dihubungi');
                $dt['tempat_tinggal'] = Input::get('tempat_tinggal');
                $dt['puskesmas'] = Input::get('nama_puskesmas');
                $dt['tanggal_mulai_sakit'] = Helper::changeDate(Input::get('tanggal_mulai_sakit'));
                $dt['keluhan'] = Input::get('keluhan');
                $dt['gejala_sakit_demam'] = Input::get('gejala_sakit_demam');
                $dt['tanggal_gejala_sakit_demam'] = Helper::changeDate(Input::get('tanggal_gejala_sakit_demam'));
                $dt['gejala_sakit_kerongkongan'] = Input::get('gejala_sakit_kerongkongan');
                $dt['tanggal_gejala_sakit_kerongkongan'] = Helper::changeDate(Input::get('tanggal_gejala_sakit_kerongkongan'));
                $dt['gejala_leher_bengkak'] = Input::get('gejala_leher_bengkak');
                $dt['tanggal_gejala_leher_bengkak'] = Helper::changeDate(Input::get('tanggal_gejala_leher_bengkak'));
                $dt['gejala_sesak_nafas'] = Input::get('gejala_sesak_nafas');
                $dt['tanggal_gejala_sesak_nafas'] = Helper::changeDate(Input::get('tanggal_gejala_sesak_nafas'));
                $dt['gejala_pseudomembran'] = Input::get('gejala_pseudomembran');
                $dt['tanggal_gejala_pseudomembran'] = Helper::changeDate(Input::get('tanggal_gejala_pseudomembran'));
                $dt['gejala_lain'] = Input::get('gejala_lain');
                $dt['status_imunisasi_difteri'] = Input::get('status_imunisasi_difteri');
                $dt['jenis_spesimen'] = Input::get('jenis_spesimen')[0];
                $dt['tanggal_ambil_spesimen'] = Helper::changeDate(Input::get('tanggal_ambil_spesimen'));
                $dt['kode_spesimen'] = Input::get('kode_spesimen')[0];
                $dt['tempat_berobat'] = Input::get('tempat_berobat');
                $dt['dirawat'] = Input::get('dirawat');
                $dt['trakeostomi'] = Input::get('trakeostomi');
                $dt['antibiotik'] = Input::get('antibiotik');
                $dt['obat_lain'] = Input::get('obat_lain');
                $dt['ads'] = Input::get('ads');
                $dt['kondisi_kasus'] = Input::get('kondisi_kasus');
                $dt['penderita_berpergian'] = Input::get('penderita_berpergian');
                $dt['tempat_berpergian'] = Input::get('tempat_berpergian');
                $dt['penderita_berkunjung'] = Input::get('penderita_berkunjung');
                $dt['tempat_berkunjung'] = Input::get('tempat_berkunjung');
                $dt['penderita_menerima_tamu'] = Input::get('penderita_menerima_tamu');
                $dt['asal_tamu'] = Input::get('asal_tamu');
                $dt['nama'] = Input::get('nama');
                $dt['umur'] = Input::get('umur');
                $dt['hubungan_kasus'] = Input::get('hubungan_kasus');
                $dt['status_imunisasi'] = Input::get('status_imunisasi');
                $dt['hasil_lab'] = Input::get('hasil_lab');
                $dt['profilaksis'] = Input::get('profilaksis');

                $save = Difteri::epid_difteri($dt);

                $cari = DB::select("select id_difteri from difteri where no_epid='" . Input::get('no_epid') . "'");

                $up = [
                    'id_pe_difteri' => $save,
                    'no_epid' => Input::get('no_epid'),
                ];
                //update id_pe
                $id_pe = DB::table('difteri')
                    ->where('id_difteri', $id_difteri)
                    ->update($up);

                foreach ($cari as $value) {
                    $id_difteri = $value->id_difteri;
                }
                DB::table('hasil_uji_lab_difteri')->insert(array(
                    'id_difteri' => $id_difteri,
                    'id_pasien' => $id_pasien,
                ));

                return Redirect::to('difteri');
            } else {
                 $dt['no_epid'] = Input::get('no_epid');
                $dt['created_by'] = Sentry::getUser()->id;
                $dt['telepon'] = Input::get('telepon');
                $dt['pekerjaan'] = Input::get('pekerjaan');
                $dt['alamat_tempat_kerja'] = Input::get('alamat_tempat_kerja');
                $dt['nama_saudara_yang_bisa_dihubungi'] = Input::get('nama_saudara_yang_bisa_dihubungi');
                $dt['alamat_saudara_yang_bisa_dihubungi'] = Input::get('alamat_saudara_yang_bisa_dihubungi');
                $dt['tlp_saudara_yang_bisa_dihubungi'] = Input::get('tlp_saudara_yang_bisa_dihubungi');
                $dt['tempat_tinggal'] = Input::get('tempat_tinggal');
                $dt['puskesmas'] = Input::get('nama_puskesmas');
                $dt['tanggal_mulai_sakit'] = Helper::changeDate(Input::get('tanggal_mulai_sakit'));
                $dt['keluhan'] = Input::get('keluhan');
                $dt['gejala_sakit_demam'] = Input::get('gejala_sakit_demam');
                $dt['tanggal_gejala_sakit_demam'] = Helper::changeDate(Input::get('tanggal_gejala_sakit_demam'));
                $dt['gejala_sakit_kerongkongan'] = Input::get('gejala_sakit_kerongkongan');
                $dt['tanggal_gejala_sakit_kerongkongan'] = Helper::changeDate(Input::get('tanggal_gejala_sakit_kerongkongan'));
                $dt['gejala_leher_bengkak'] = Input::get('gejala_leher_bengkak');
                $dt['tanggal_gejala_leher_bengkak'] = Helper::changeDate(Input::get('tanggal_gejala_leher_bengkak'));
                $dt['gejala_sesak_nafas'] = Input::get('gejala_sesak_nafas');
                $dt['tanggal_gejala_sesak_nafas'] = Helper::changeDate(Input::get('tanggal_gejala_sesak_nafas'));
                $dt['gejala_pseudomembran'] = Input::get('gejala_pseudomembran');
                $dt['tanggal_gejala_pseudomembran'] = Helper::changeDate(Input::get('tanggal_gejala_pseudomembran'));
                $dt['gejala_lain'] = Input::get('gejala_lain');
                $dt['status_imunisasi_difteri'] = Input::get('status_imunisasi_difteri');
                $dt['jenis_spesimen'] = Input::get('jenis_spesimen')[0];
                $dt['tanggal_ambil_spesimen'] = Helper::changeDate(Input::get('tanggal_ambil_spesimen'));
                $dt['kode_spesimen'] = Input::get('kode_spesimen')[0];
                $dt['tempat_berobat'] = Input::get('tempat_berobat');
                $dt['dirawat'] = Input::get('dirawat');
                $dt['trakeostomi'] = Input::get('trakeostomi');
                $dt['antibiotik'] = Input::get('antibiotik');
                $dt['obat_lain'] = Input::get('obat_lain');
                $dt['ads'] = Input::get('ads');
                $dt['kondisi_kasus'] = Input::get('kondisi_kasus');
                $dt['penderita_berpergian'] = Input::get('penderita_berpergian');
                $dt['tempat_berpergian'] = Input::get('tempat_berpergian');
                $dt['penderita_berkunjung'] = Input::get('penderita_berkunjung');
                $dt['tempat_berkunjung'] = Input::get('tempat_berkunjung');
                $dt['penderita_menerima_tamu'] = Input::get('penderita_menerima_tamu');
                $dt['asal_tamu'] = Input::get('asal_tamu');
                $dt['nama'] = Input::get('nama');
                $dt['umur'] = Input::get('umur');
                $dt['hubungan_kasus'] = Input::get('hubungan_kasus');
                $dt['status_imunisasi'] = Input::get('status_imunisasi');
                $dt['hasil_lab'] = Input::get('hasil_lab');
                $dt['profilaksis'] = Input::get('profilaksis');

                $save = Difteri::epid_difteri($dt);
                $up = [
                    'id_pe_difteri' => $save,
                    'no_epid' => Input::get('no_epid'),
                ];
                //update id_pe
                $id_pe = DB::table('difteri')
                    ->where('id_difteri', $id_difteri)
                    ->update($up);

                return Redirect::to('difteri');
            }
        }

    }

    public function getDetailPeDifteri($id)
    {
        $data = DB::select("select
                                d.nik, a.no_epid, d.nama_anak, d.nama_ortu, CONCAT_WS(',',d.alamat,e.kelurahan,e.kecamatan,e.kabupaten,e.provinsi) as alamat, d.tanggal_lahir,
                                d.umur, d.umur_bln, d.umur_hr, d.jenis_kelamin, d.id_kelurahan,
                                a.telepon, a.pekerjaan, a.tempat_tinggal,
                                a.puskesmas, a.tanggal_mulai_sakit, a.keluhan, a.gejala_sakit_demam,
                                a.tanggal_gejala_sakit_demam, a.gejala_sakit_kerongkongan,
                                a.tanggal_gejala_sakit_kerongkongan, a.no_epid, a.gejala_leher_bengkak,
                                a.tanggal_gejala_leher_bengkak, a.gejala_sesak_nafas, a.tanggal_gejala_sesak_nafas,
                                a.gejala_pseudomembran, a.tanggal_gejala_pseudomembran, a.gejala_lain,
                                a.status_imunisasi_difteri, a.jenis_spesimen, a.tanggal_ambil_spesimen,
                                a.kode_spesimen, a.tempat_berobat, a.dirawat, a.trakeostomi,
                                a.antibiotik, a.obat_lain, a.ads, a.kondisi_kasus,
                                a.penderita_berpergian, a.tempat_berpergian, a.penderita_berkunjung,
                                a.tempat_berkunjung, a.penderita_menerima_tamu, a.asal_tamu, a.nama,
                                a.umur as umur_kontak, a.hubungan_kasus, a.status_imunisasi,
                                a.hasil_lab, a.profilaksis
                                from
                                pe_difteri a
                                JOIN difteri b ON a.id = b.id_pe_difteri
                                JOIN hasil_uji_lab_difteri c ON b.id_difteri = c.id_difteri
                                JOIN pasien d ON c.id_pasien = d.id_pasien
                                LEFT JOIN lokasi e ON d.id_kelurahan = e.id_kelurahan
                                WHERE
                                a.id='" . $id . "'
                            order by
                                a.created_at asc");

        return View::make('pemeriksaan.difteri.detail_pe_difteri', compact('data'));
    }

    public function getEditPeDifteri($id)
    {
        $difteri = Difteri::getEditPeDifteri($id);
        return View::make('pemeriksaan.difteri.edit_pe_difteri', compact('difteri'));
    }

    public function postUpdatePeDifteri()
    {
        DB::update("update
                        pe_difteri
                    set
                        pe_difteri.telepon='" . Input::get('telepon') . "',
                        pe_difteri.pekerjaan='" . Input::get('pekerjaan') . "',
                        pe_difteri.alamat_tempat_kerja ='" . Input::get('alamat_tempat_kerja') . "',
                        pe_difteri.nama_saudara_yang_bisa_dihubungi = '" . Input::get('nama_saudara_yang_bisa_dihubungi') . "',
                        pe_difteri.alamat_saudara_yang_bisa_dihubungi = '" . Input::get('alamat_saudara_yang_bisa_dihubungi') . "',
                        pe_difteri.tlp_saudara_yang_bisa_dihubungi = '" . Input::get('tlp_saudara_yang_bisa_dihubungi') . "',
                        pe_difteri.tempat_tinggal='" . Input::get('tempat_tinggal') . "',
                        pe_difteri.puskesmas='" . Input::get('puskesmas') . "',
                        pe_difteri.tanggal_mulai_sakit=" . $this->changeDate(Input::get('tanggal_mulai_sakit')) . ",
                        pe_difteri.keluhan='" . Input::get('keluhan') . "',
                        pe_difteri.gejala_sakit_demam='" . Input::get('gejala_sakit_demam') . "',
                        pe_difteri.tanggal_gejala_sakit_demam=" . $this->changeDate(Input::get('tanggal_gejala_sakit_demam')) . ",
                        pe_difteri.gejala_sakit_kerongkongan='" . Input::get('gejala_sakit_kerongkongan') . "',
                        pe_difteri.tanggal_gejala_sakit_kerongkongan=" . $this->changeDate(Input::get('tanggal_gejala_sakit_kerongkongan')) . ",
                        pe_difteri.no_epid='" . Input::get('no_epid') . "',
                        pe_difteri.gejala_leher_bengkak='" . Input::get('gejala_leher_bengkak') . "',
                        pe_difteri.tanggal_gejala_leher_bengkak=" . $this->changeDate(Input::get('tanggal_gejala_leher_bengkak')) . ",
                        pe_difteri.gejala_sesak_nafas='" . Input::get('gejala_sesak_nafas') . "',
                        pe_difteri.tanggal_gejala_sesak_nafas=" . $this->changeDate(Input::get('tanggal_gejala_sesak_nafas')) . ",
                        pe_difteri.gejala_pseudomembran='" . Input::get('gejala_pseudomembran') . "',
                        pe_difteri.tanggal_gejala_pseudomembran=" . $this->changeDate(Input::get('tanggal_gejala_pseudomembran')) . ",
                        pe_difteri.gejala_lain='" . Input::get('gejala_lain') . "',
                        pe_difteri.status_imunisasi_difteri='" . Input::get('status_imunisasi_difteri') . "',
                        pe_difteri.jenis_spesimen='" . Input::get('jenis_spesimen') . "',
                        pe_difteri.tanggal_ambil_spesimen=" . $this->changeDate(Input::get('tanggal_ambil_spesimen')) . ",
                        pe_difteri.kode_spesimen='" . Input::get('kode_spesimen') . "',
                        pe_difteri.tempat_berobat='" . Input::get('tempat_berobat') . "',
                        pe_difteri.dirawat='" . Input::get('dirawat') . "',
                        pe_difteri.trakeostomi='" . Input::get('trakeostomi') . "',
                        pe_difteri.antibiotik='" . Input::get('antibiotik') . "',
                        pe_difteri.obat_lain='" . Input::get('obat_lain') . "',
                        pe_difteri.ads='" . Input::get('ads') . "',
                        pe_difteri.kondisi_kasus='" . Input::get('kondisi_kasus') . "',
                        pe_difteri.penderita_berpergian='" . Input::get('penderita_berpergian') . "',
                        pe_difteri.tempat_berpergian='" . Input::get('tempat_berpergian') . "',
                        pe_difteri.penderita_berkunjung='" . Input::get('penderita_berkunjung') . "',
                        pe_difteri.tempat_berkunjung='" . Input::get('tempat_berkunjung') . "',
                        pe_difteri.penderita_menerima_tamu='" . Input::get('penderita_menerima_tamu') . "',
                        pe_difteri.asal_tamu='" . Input::get('asal_tamu') . "',
                        pe_difteri.nama='" . Input::get('nama') . "',
                        pe_difteri.umur='" . Input::get('umur') . "',
                        pe_difteri.hubungan_kasus='" . Input::get('hubungan_kasus') . "',
                        pe_difteri.status_imunisasi='" . Input::get('status_imunisasi') . "',
                        pe_difteri.hasil_lab='" . Input::get('hasil_lab') . "',
                        pe_difteri.profilaksis='" . Input::get('profilaksis') . "'
                    where
                        pe_difteri.id='" . Input::get('id') . "'");
        return Redirect::to('difteri');
    }

    //fungsi untuk mendapatkan no epid difteri secara otomatis
    public function getEpid()
    {

        $id_kel = Input::get('id_kelurahan');
        $tglsakit = $this->changeDate(Input::get('date'));
        $thn = substr($tglsakit, 2, 2);
        $data2 = DB::select("select max(no_epid) as no_epid from difteri,pasien,hasil_uji_lab_difteri where pasien.id_pasien=hasil_uji_lab_difteri.id_pasien and difteri.id_difteri=hasil_uji_lab_difteri.id_difteri and id_kelurahan='" . $id_kel . "' order by difteri.id_difteri limit 0,1");
        $cek = DB::select("select RIGHT(no_epid,3) from difteri,pasien,hasil_uji_lab_difteri where MID(no_epid,12,2) ='" . $thn . "' and pasien.id_pasien=hasil_uji_lab_difteri.id_pasien and difteri.id_difteri=hasil_uji_lab_difteri.id_difteri and pasien.id_kelurahan='" . $id_kel . "' order by difteri.id_difteri limit 0,1");

        if (empty($cek)) {
            $code = $id_kel . $thn;
            $caracter = 'D';
            $epid = '001';
            $no_epid = $caracter . $code . $epid;
        } else {
            $kode = substr($data2[0]->no_epid, 0, 13);
            $noUrut = (int) substr($data2[0]->no_epid, 13, 3);
            $noUrut++;
            $no_epid = $kode . sprintf("%03s", $noUrut);
        }

        return $no_epid;
    }

    //fungsi hapus pe difteri
    public function getHapusPeDifteri($id)
    {
        try {
            DB::beginTransaction();
            DB::delete("delete from pe_difteri where pe_difteri.id='" . $id . "'");
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return Redirect::to('difteri');
    }

    public function getAll()
    {
        return Difteri::has('patients')->with('patients')->get();
    }

    public function exportExcel($filetype, $dt = null)
    {
        $ds = json_decode($dt);
        $penyakit = Session::get('penyakit');
        $excel_tmp = array(
            array(
                'No',
                'No Epid Kasus / KLB',
                'Nama Penderita',
                'Nama Orangtua',
                'Alamat',
                'Puskesmas',
                'Kecamatan',
                'Kabupaten/Kota',
                'Provinsi',
                'Umur/Tahun',
                'Umur/Bulan',
                'Jenis Kelamin',
                'Imunisasi Difteri Sebelum Sakit',
                'Imunisasi Difteri Sebelum Sakit',
                'Imunisasi Difteri Sebelum Sakit',
                'Tanggal Imunisasi Difteri Terakhir',
                'Tanggal Mulai Demam',
                'Tanggal Pelacakan',
                'Tanggal Pengambilan Spesimen',
                'Tanggal Pengambilan Spesimen',
                'Hasil Lab Kultur Tenggorok',
                'Hasil Lab Kultur Hidung',
                'Hasil Lab Mikroskop Tenggorok',
                'Hasil Lab Mikroskop Hidung',
                'Keadaan Akhir (Hidup/Meninggal)',
                'Kontak',
                'Kontak',
                'Kontak',
                'Klasifikasi Final',
                'Klasifikasi Final',
                'Klasifikasi Final',
            ),
            array(
                '1',
                '2',
                '3',
                '4',
                '5',
                '6',
                '7',
                '8',
                '9',
                'Tahun',
                'Bulan',
                '12',
                'Berapa Kali',
                'Tidak',
                'Tidak Tahu',
                '16',
                '17',
                '18',
                'Tenggorok',
                'Hidung',
                'Kultur',
                '22',
                'Mikroskop',
                '24',
                '24',
                'Jumlah',
                'Diambil Spec(Swab Hidung)',
                'Positif',
                'Probable',
                'Konfirm',
                'Negatif',
            ),
            array(
                'B1',
                'B2',
                'B3',
                'B4',
                'B5',
                'B6',
                'B7',
                'B8',
                'B9',
                'B10',
                'B11',
                'B12',
                'B13',
                'B14',
                'B15',
                'B16',
                'B17',
                'B18',
                'B19',
                'B20',
                'Tenggorok',
                'Hidung',
                'Tenggorok',
                'Hidung',
                'B25',
                'B26',
                'B27',
                'B28',
                'B29',
                'B30',
            ),
            array(''),
        );

        foreach ($ds as $k => $v) {
            $post[$v->name] = $v->value;
        }
        $between = $wilayah = '';
        $unit = (empty($post['unit']) ? '' : $post['unit']);
        $day_start = (empty($post['day_start']) ? '' : $post['day_start']);
        $month_start = (empty($post['month_start']) ? '' : $post['month_start']);
        $year_start = (empty($post['year_start']) ? '' : $post['year_start']);
        $day_end = (empty($post['day_end']) ? '' : $post['day_end']);
        $month_end = (empty($post['month_end']) ? '' : $post['month_end']);
        $year_end = (empty($post['year_end']) ? '' : $post['year_end']);
        $province_id = (empty($post['province_id']) ? '' : $post['province_id']);
        $district_id = (empty($post['district_id']) ? '' : $post['district_id']);
        $sub_district_id = (empty($post['sub_district_id']) ? '' : $post['sub_district_id']);
        $rs_id = (empty($post['rs_id']) ? '' : $post['rs_id']);
        $puskesmas_id = (empty($post['puskesmas_id']) ? '' : $post['puskesmas_id']);

        switch ($unit) {
            case "all":
                $between = " ";
                break;
            case "year":
                $start = $post['year_start'];
                $end = $post['year_end'];
                $between = " AND YEAR(c.tanggal_periksa) BETWEEN '" . $start . "' AND '" . $end . "' ";
                break;
            case "month":
                $start = $post['year_start'] . '-' . $post['month_start'] . '-1';
                $end = $post['year_end'] . '-' . $post['month_end'] . '-' . date('t', mktime(0, 0, 0, $post['month_end'], 1, $post['year_end']));
                $between = " AND DATE(c.tanggal_periksa) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                break;
            default:
                $start = $post['year_start'] . '-' . $post['month_start'] . '-' . $post['day_start'];
                $end = $post['year_end'] . '-' . $post['month_end'] . '-' . $post['day_end'];
                $between = " AND DATE(c.tanggal_periksa) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                break;
        }

        if ($puskesmas_id) {
            $wilayah = "AND puskesmas_code_faskes='" . $puskesmas_id . "'";
        } else if ($sub_district_id) {
            $wilayah = "AND e.id_kecamatan='" . $sub_district_id . "'";
        } else if ($rs_id) {
            $wilayah = "AND j.kode_faskes='" . $rs_id . "' ";
        } else if ($district_id) {
            $wilayah = "AND f.id_kabupaten='" . $district_id . "' ";
        } else if ($province_id) {
            $wilayah = "AND g.id_provinsi='" . $province_id . "' ";
        }

        $q = "
                SELECT
                    c.no_epid,
                    b.nama_anak,
                    b.nama_ortu,
                    b.alamat,
                    CONCAT('PUSKESMAS ',h.puskesmas_name) AS puskesmas_name,
                    e.kecamatan,
                    f.kabupaten,
                    g.provinsi,
                    c.tanggal_timbul_demam,
                    b.tanggal_lahir,
                    IF(b.umur='','',CONCAT(b.umur,' Tahun')) as umur,
                    IF(b.umur_bln='','',CONCAT(b.umur_bln,' Bulan')) as umur_bln,

                    #JENIS KELAMIN
                    IF(b.jenis_kelamin='0','Tidak Diketahui',
                    IF(b.jenis_kelamin='1','Laki-laki',
                    IF(b.jenis_kelamin='2','Perempuan',
                    IF(b.jenis_kelamin='3','Lainnya',
                    'Tidak diisi')))) AS jenis_kelamin,

                    #HASIL LAB KULTUR TENGGOROKAN
                    IF(c.vaksin_DPT_sebelum_sakit='1x','1 Kali',
                    IF(c.vaksin_DPT_sebelum_sakit='2x','2 Kali',
                    IF(c.vaksin_DPT_sebelum_sakit='3x','3 Kali',
                    IF(c.vaksin_DPT_sebelum_sakit='4x','4 Kali',
                    IF(c.vaksin_DPT_sebelum_sakit='5x','5 Kali',
                    IF(c.vaksin_DPT_sebelum_sakit='6x','6 Kali',
                    '')))))) AS vaksin_DPT_sebelum_sakit,

                    #HASIL LAB KULTUR TENGGOROKAN TIDAK / TIDAK TAHU
                    IF(c.vaksin_DPT_sebelum_sakit='tidak','V','') AS vaksin_DPT_sebelum_sakit_tidak,
                    IF(c.vaksin_DPT_sebelum_sakit='tidak tahu','V','') AS vaksin_DPT_sebelum_sakit_tidak_tahu,

                    c.tanggal_vaksinasi_difteri_terakhir,
                    c.tanggal_pelacakan,
                    c.tanggal_diambil_spesimen_tenggorokan,
                    c.tanggal_diambil_spesimen_hidung,

                    #HASIL LAB KULTUR TENGGOROKAN
                    IF(c.hasil_lab_kultur_tenggorokan='1','Positif',
                    IF(c.hasil_lab_kultur_tenggorokan='2','Negatif',
                    'Tidak Diisi')) AS hasil_lab_kultur_tenggorokan,

                    #HASIL LAB KULTUR HIDUNG
                    IF(c.hasil_lab_kultur_hidung='1','Positif',
                    IF(c.hasil_lab_kultur_hidung='2','Negatif',
                    'Tidak Diisi')) AS hasil_lab_kultur_hidung,

                    #HASIL LAB MIKROSKOP TENGGOROKAN
                    IF(c.hasil_lab_mikroskop_tenggorokan='1','Positif',
                    IF(c.hasil_lab_mikroskop_tenggorokan='2','Negatif',
                    'Tidak Diisi')) AS hasil_lab_mikroskop_tenggorokan,

                    #HASIL LAB MIKROSKOP HIDUNG
                    IF(c.hasil_lab_mikroskop_hidung='1','Positif',
                    IF(c.hasil_lab_mikroskop_hidung='2','Negatif',
                    'Tidak Diisi')) AS hasil_lab_mikroskop_hidung,

                    #KEADAAN AKHIR
                    IF(c.keadaan_akhir='1','Hidup/Sehat',
                    IF(c.keadaan_akhir='2','Meninggal',
                    'Tidak Diisi')) AS keadaan_akhir,
                    c.kontak,
                    c.jumlah_kontak,
                    c.jumlah_swab_hidung,
                    c.jumlah_positif,
                    #KLASIFIKASI FINAL
                    IF(c.klasifikasi_final='probable','Probable',
                    IF(c.klasifikasi_final='konfirm','Konfirm',
                    IF(c.klasifikasi_final='negatif','Negatif',
                    'Tidak Diisi'))) AS klasifikasi_final,

                    IF(c.klasifikasi_final='probable','V','') AS klasifikasi_final_probable,
                    IF(c.klasifikasi_final='konfirm','V','') AS klasifikasi_final_konfirm,
                    IF(c.klasifikasi_final='negatif','V','') AS klasifikasi_final_negatif

                FROM hasil_uji_lab_difteri a
                JOIN pasien b ON b.id_pasien=a.id_pasien
                JOIN difteri c ON c.id_difteri=a.id_difteri
                LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
                LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
                LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
                LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
                LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
                LEFT JOIN rumahsakit2 j  ON j.id=c.id_tempat_periksa
                WHERE
                1=1
                " . $between . "
                " . $wilayah . "
            ";

        $data = DB::select($q);

        $ke_bawah = 3;
        for ($i = 0; $i < count($data); $i++) {
            $excel_tmp[$ke_bawah + $i][0] = 1 + $i;
            $excel_tmp[$ke_bawah + $i][1] = $data[$i]->no_epid;
            $excel_tmp[$ke_bawah + $i][2] = $data[$i]->nama_anak;
            $excel_tmp[$ke_bawah + $i][3] = $data[$i]->nama_ortu;
            $excel_tmp[$ke_bawah + $i][4] = $data[$i]->alamat;
            $excel_tmp[$ke_bawah + $i][5] = $data[$i]->puskesmas_name;
            $excel_tmp[$ke_bawah + $i][6] = $data[$i]->kecamatan;
            $excel_tmp[$ke_bawah + $i][7] = $data[$i]->kabupaten;
            $excel_tmp[$ke_bawah + $i][8] = $data[$i]->provinsi;
            $excel_tmp[$ke_bawah + $i][9] = $data[$i]->umur;
            $excel_tmp[$ke_bawah + $i][10] = $data[$i]->umur_bln;
            $excel_tmp[$ke_bawah + $i][11] = $data[$i]->jenis_kelamin;
            $excel_tmp[$ke_bawah + $i][12] = $data[$i]->vaksin_DPT_sebelum_sakit;
            $excel_tmp[$ke_bawah + $i][13] = $data[$i]->vaksin_DPT_sebelum_sakit_tidak;
            $excel_tmp[$ke_bawah + $i][14] = $data[$i]->vaksin_DPT_sebelum_sakit_tidak_tahu;
            $excel_tmp[$ke_bawah + $i][15] = Helper::getDate($data[$i]->tanggal_vaksinasi_difteri_terakhir);
            $excel_tmp[$ke_bawah + $i][16] = Helper::getDate($data[$i]->tanggal_timbul_demam);
            $excel_tmp[$ke_bawah + $i][17] = Helper::getDate($data[$i]->tanggal_pelacakan);
            $excel_tmp[$ke_bawah + $i][18] = Helper::getDate($data[$i]->tanggal_diambil_spesimen_tenggorokan);
            $excel_tmp[$ke_bawah + $i][19] = Helper::getDate($data[$i]->tanggal_diambil_spesimen_hidung);
            $excel_tmp[$ke_bawah + $i][20] = $data[$i]->hasil_lab_kultur_tenggorokan;
            $excel_tmp[$ke_bawah + $i][21] = $data[$i]->hasil_lab_kultur_hidung;
            $excel_tmp[$ke_bawah + $i][22] = $data[$i]->hasil_lab_mikroskop_tenggorokan;
            $excel_tmp[$ke_bawah + $i][23] = $data[$i]->hasil_lab_mikroskop_hidung;
            $excel_tmp[$ke_bawah + $i][24] = $data[$i]->keadaan_akhir;
            $excel_tmp[$ke_bawah + $i][25] = $data[$i]->jumlah_kontak;
            $excel_tmp[$ke_bawah + $i][26] = $data[$i]->jumlah_swab_hidung;
            $excel_tmp[$ke_bawah + $i][27] = $data[$i]->jumlah_positif;
            $excel_tmp[$ke_bawah + $i][28] = $data[$i]->klasifikasi_final_probable;
            $excel_tmp[$ke_bawah + $i][29] = $data[$i]->klasifikasi_final_konfirm;
            $excel_tmp[$ke_bawah + $i][30] = $data[$i]->klasifikasi_final_negatif;
        }

        Excel::create('FORM_C1_DIFTERI_' . date('Y_m_d_h_i_s'), function ($excel) use ($excel_tmp) {
            $excel->sheet('Sheetname', function ($sheet) use ($excel_tmp) {
                $sheet->fromArray($excel_tmp);
                //$sheet->mergeCells('A1:AF1');

                $sheet->mergeCells('A2:A4');
                $sheet->mergeCells('B2:B4');
                $sheet->mergeCells('C2:C4');
                $sheet->mergeCells('D2:D4');
                $sheet->mergeCells('E2:E4');
                $sheet->mergeCells('F2:F4');
                $sheet->mergeCells('G2:G4');
                $sheet->mergeCells('H2:H4');
                $sheet->mergeCells('I2:I4');
                $sheet->mergeCells('J3:J4');
                $sheet->mergeCells('K3:K4');
                $sheet->mergeCells('L2:L4');
                $sheet->mergeCells('M3:M4');
                $sheet->mergeCells('N3:N4');
                $sheet->mergeCells('O3:O4');
                $sheet->mergeCells('P2:P4');
                $sheet->mergeCells('Q2:Q4');
                $sheet->mergeCells('R2:R4');
                $sheet->mergeCells('S3:S4');
                $sheet->mergeCells('T3:T4');
                //$sheet->mergeCells('U3:U4');
                //$sheet->mergeCells('V3:V4');
                //$sheet->mergeCells('W3:W4');
                //$sheet->mergeCells('X3:X4');
                $sheet->mergeCells('Y2:Y4');
                $sheet->mergeCells('Z3:Z4');
                $sheet->mergeCells('AA3:AA4');
                $sheet->mergeCells('AB3:AB4');
                $sheet->mergeCells('AC3:AC4');
                $sheet->mergeCells('AD3:AD4');
                $sheet->mergeCells('AE3:AE4');

                $sheet->mergeCells('J2:K2');
                $sheet->mergeCells('M2:O2');
                $sheet->mergeCells('S2:T2');
                $sheet->mergeCells('U2:X2');
                $sheet->mergeCells('U3:V3');
                $sheet->mergeCells('W3:X3');
                $sheet->mergeCells('Z2:AB2');
                $sheet->mergeCells('AC2:AE2');

                $sheet->cells('A2:AE4', function ($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#FFFFAA');
                    $cells->setFontSize(13);
                    $cells->setFontWeight('bold');
                });

                $sheet->cells('O5:O1000', function ($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                });

                $sheet->cells('AC5:AE1000', function ($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                });

                $sheet->setWidth('A', 5);
                $sheet->setWidth('B', 25);
                $sheet->setWidth('C', 22);
                $sheet->setWidth('D', 22);
                $sheet->setWidth('E', 50);
                $sheet->setWidth('F', 122);
                $sheet->setWidth('G', 20);
                $sheet->setWidth('H', 20);
                $sheet->setWidth('I', 30);
                $sheet->setWidth('J', 10);
                $sheet->setWidth('K', 10);
                $sheet->setWidth('L', 22);
                $sheet->setWidth('M', 14);
                $sheet->setWidth('N', 14);
                $sheet->setWidth('O', 14);
                $sheet->setWidth('P', 37);
                $sheet->setWidth('Q', 24);
                $sheet->setWidth('R', 24);
                $sheet->setWidth('S', 27);
                $sheet->setWidth('T', 20);
                $sheet->setWidth('U', 18);
                $sheet->setWidth('V', 18);
                $sheet->setWidth('W', 13);
                $sheet->setWidth('X', 13);
                $sheet->setWidth('Y', 36);
                $sheet->setWidth('Z', 18);
                $sheet->setWidth('AA', 29);
                $sheet->setWidth('AB', 26);
                $sheet->setWidth('AC', 26);
                $sheet->setWidth('AD', 26);
                $sheet->setWidth('AE', 26);

                $sheet->setBorder('A2:AE4', 'thin');

                $sheet->setFreeze('D5');
                $sheet->setAutoFilter('A4:AE4');
            });

        })->export('xls');
    }

    public function getUmur($tgl_lahir, $tgl_demam)
    {

        $pecah = explode("-", $tgl_demam);
        $m = $pecah[1];
        $d = $pecah[0];
        $y = $pecah[2];
        $tanggal_lahir = $this->changeDate($tgl_lahir);
        list($thn_skrg, $bln_skrg, $tgl_skrg) = explode('-', $tanggal_lahir);
        //$data = Pasien::umur($tanggal_lahir);

        // kelahiran dijadikan dalam format tanggal semua
        $lahir = $tgl_skrg + ($bln_skrg * 30) + ($thn_skrg * 365);

        // sekarang dijadikan dalam format tanggal semua
        $now = $d + ($m * 30) + ($y * 365);

        // dari format tanggal jadikan tahun, bulan, hari
        $tahun = ($now - $lahir) / 365;
        $bulan = (($now - $lahir) % 365) / 30;
        $hari = (($now - $lahir) % 365) % 30;

        $tgl_tahun = floor($tahun);
        $tgl_bulan = floor($bulan);
        $tgl_hari = floor($hari);
        $tgl = '';

        return Response::json(compact('tgl_tahun', 'tgl_bulan', 'tgl_hari'));
    }
    //fungsi untuk mencari tanggal lahir
    public function getTgl_lahir()
    {
        if (Input::get('tanggal_timbul_demam')) {
            $pecah = explode("-", Input::get('tanggal_timbul_demam'));
            if (Input::get('tanggal_lahir') == '' && checkdate($pecah[1], $pecah[0], $pecah[2])) {
                $thn = Input::get('tgl_tahun');
                $bln = Input::get('tgl_bulan');
                $hr = Input::get('tgl_hari');
                $tgl_mulai_sakit = $this->changeDate(Input::get('tanggal_timbul_demam'));
                Difteri::get_tgl_lahir($thn, $bln, $hr, $tgl_mulai_sakit);
            } elseif (Input::get('tanggal_lahir') && checkdate($pecah[1], $pecah[0], $pecah[2])) {
                $thn = Input::get('tgl_tahun');
                $bln = Input::get('tgl_bulan');
                $hr = Input::get('tgl_hari');
                $tgl_mulai_sakit = $this->changeDate(Input::get('tanggal_timbul_demam'));
                $tgl1 = Difteri::get_tgl_lahir($thn, $bln, $hr, $tgl_mulai_sakit);
                $pecah = explode("-", Input::get('tanggal_timbul_demam'));
                $m = $pecah[1];
                $d = $pecah[0];
                $y = $pecah[2];
                $tanggal_lahir = $this->changeDate($tgl1);
                list($thn_skrg, $bln_skrg, $tgl_skrg) = explode('-', $tanggal_lahir);
                // kelahiran dijadikan dalam format tanggal semua
                $lahir = $tgl_skrg + ($bln_skrg * 30) + ($thn_skrg * 365);
                // sekarang dijadikan dalam format tanggal semua
                $now = $d + ($m * 30) + ($y * 365);
                // dari format tanggal jadikan tahun, bulan, hari
                $tahun = ($now - $lahir) / 365;
                $bulan = (($now - $lahir) % 365) / 30;
                $hari = (($now - $lahir) % 365) % 30;
                $tgl_tahun = floor($tahun);
                $tgl_bulan = floor($bulan);
                $tgl_hari = floor($hari);
                $tgl = 1;
                //return Response::json(compact('tgl','tgl1','tgl_tahun','tgl_bulan','tgl_hari'));
            }
        }
    }

    public function filterDaftarKasus($value = '')
    {
        $type = Session::get('type');

        $kd_faskes = Session::get('kd_faskes');

        $wilayah = "";

        // mendapatkan tipe user yang sedang login
        if ($type == "puskesmas") {
            $wilayah = "AND h.puskesmas_code_faskes='" . $kd_faskes . "' ";
        } else if ($type == "kabupaten") {
            $wilayah = "AND h.kode_kab='" . $kd_faskes . "' ";
        } else if ($type == "provinsi") {
            $wilayah = "AND h.kode_prop='" . $kd_faskes . "' ";
        } else if ($type == "kemenkes") {
            $wilayah = "";
        }

        $unit = Input::get('unit');
        $day_start = Input::get('day_start');
        $month_start = Input::get('month_start');
        $year_start = Input::get('year_start');
        $day_end = Input::get('day_end');
        $month_end = Input::get('month_end');
        $year_end = Input::get('year_end');
        $province_id = Input::get('province_id');
        $district_id = Input::get('district_id');
        $sub_district_id = Input::get('sub_district_id');
        $village_id = Input::get('village_id');

        switch ($unit) {
            case "all":
                $between = " ";
                break;
            case "year":
                $start = $post['year_start'];
                $end = $post['year_end'];
                $between = " AND YEAR(c.tanggal_periksa) BETWEEN '" . $start . "' AND '" . $end . "' ";
                break;
            case "month":
                $start = $post['year_start'] . '-' . $post['month_start'] . '-1';
                $end = $post['year_end'] . '-' . $post['month_end'] . '-' . date('t', mktime(0, 0, 0, $post['month_end'], 1, $post['year_end']));
                $between = " AND DATE(c.tanggal_periksa) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                break;
            default:
                $start = $post['year_start'] . '-' . $post['month_start'] . '-' . $post['day_start'];
                $end = $post['year_end'] . '-' . $post['month_end'] . '-' . $post['day_end'];
                $between = " AND DATE(c.tanggal_periksa) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                break;
        }

        $wilayah = "";
        if ($village_id) {
            $wilayah = "AND d.id_kelurahan='" . $village_id . "' ";
        } else if ($sub_district_id) {
            $wilayah = "AND e.id_kecamatan='" . $sub_district_id . "' ";
        } else if ($district_id) {
            $wilayah = "AND f.id_kabupaten='" . $district_id . "' ";
        } else if ($province_id) {
            $wilayah = "AND g.id_provinsi='" . $province_id . "' ";
        }

        $q = "
            SELECT
                c.no_epid,
                c.id_difteri,
                b.nama_anak,
                b.tanggal_lahir,
                CONCAT_WS(',',b.alamat,d.kelurahan,e.kecamatan,f.kabupaten,g.provinsi) as alamat,

                #KEADAAN AKHIR
                IF(c.keadaan_akhir='0','Hidup/Sehat',
                IF(c.keadaan_akhir='1','Meninggal',
                'Tidak Diisi')) AS keadaan_akhir_nm,

                #KLASIFIKASI FINAL
                IF(c.klasifikasi_final='0','Probable',
                IF(c.klasifikasi_final='1','Konfirm',
                IF(c.klasifikasi_final='2','Negatif',
                'Tidak Diisi'))) AS klasifikasi_final_nm,

                a.id_hasil_uji_lab_difteri

            FROM hasil_uji_lab_difteri a
            JOIN pasien b ON b.id_pasien=a.id_pasien
            JOIN difteri c ON c.id_difteri=a.id_difteri
            LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
            LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
            LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
            LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
            LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
            #WHERE
            #1=1
            #" . $between . "
            #" . $wilayah . "
        ";

        $data = DB::select($q);
        //return $data;
        return View::make('pemeriksaan.difteri.index', compact('data'));
    }
}
