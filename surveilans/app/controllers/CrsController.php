<?php

class CrsController extends \BaseController {

	public function __construct() {
		$this->beforeFilter('auth');
	}

	function sync()
	{
		$q = DB::SELECT("SELECT
		a.*
		FROM
		bc_crs AS a
		LEFT OUTER JOIN crs AS b ON a.created_at=b.created_at AND a.created_at IS NOT NULL
		WHERE b.created_at IS NULL AND YEAR(a.created_at) = '2017'");
		foreach ($q as $key => $val) {
			$val = (array) $val;
			$id_crs = $val['id_crs'];
			$p1 = (array)DB::table('bc_pelapor')->where(['id_pelapor'=>$val['id_pelapor']])->first();
			unset($p1['id_pelapor']);
			$id_pelapor = DB::table('pelapor')->insertGetId($p1);
			$p2 = (array)DB::table('bc_pasien')->where(['id_pasien'=>$val['id_pasien']])->first();
			unset($p2['id_pasien']);
			$id_pasien = DB::table('pasien')->insertGetId($p2);
			$p3 = (array)DB::table('bc_pe_crs')->where(['id_pe_crs'=>$val['id_pe_crs']])->first();
			unset($p3['id_pe_crs']);
			$id_pe_crs = DB::table('pe_crs')->insertGetId($p3);
			$p4 = (array)DB::table('bc_hasil_uji_lab_crs')->where(['id_hasil_uji_lab_crs'=>$val['id_hasil_uji_lab_crs']])->first();
			unset($p4['id_hasil_uji_lab_crs']);
			$id_hasil_uji_lab_crs = DB::table('hasil_uji_lab_crs')->insertGetId($p4);
			unset($val['id_crs']);
			$val['id_pelapor'] = $id_pelapor;
			$val['id_pasien'] = $id_pasien;
			$val['id_pe_crs'] = $id_pe_crs;
			$val['id_hasil_uji_lab_crs'] = $id_hasil_uji_lab_crs;
			$id = DB::table('crs')->insertGetId($val);
			$p5 = (array)DB::table('bc_notification')->where(['global_id'=>$id_crs])->first();
			unset($p5['id'],$p5['global_id']);
			$p5['global_id'] = $id;
			$id_notification = DB::table('notification')->insertGetId($p5);
			echo"<pre>";print_r($val);echo"</pre>";
			echo"<pre>";print_r($id);echo"</pre>";
		}
	}

	public function index() {
		Session::put('sess_id', '');
		Session::put('penyakit', 'CRS');
		Session::put('sess_penyakit', 'crs');
		$between = $district = '';
		if (!empty(Input::get())) {
			$unit            = Input::get('unit');
			$day_start       = Input::get('day_start');
			$month_start     = Input::get('month_start');
			$year_start      = Input::get('year_start');
			$day_end         = Input::get('day_end');
			$month_end       = Input::get('month_end');
			$year_end        = Input::get('year_end');
			$province_id     = Input::get('province_id');
			$district_id     = Input::get('district_id');
			$sub_district_id = Input::get('sub_district_id');
			$rs_id           = Input::get('rs_id');
			$puskesmas_id    = Input::get('puskesmas_id');

			switch ($unit) {
				case "all":
				$between = "";
				break;
				case "year":
				$start   = $year_start;
				$end     = $year_end;
				$between = " AND YEAR(crs_tanggal_periksa) BETWEEN '" . $start . "' AND '" . $end . "' ";
				break;
				case "month":
				$start   = $year_start . '-' . $month_start . '-1';
				$end     = $year_end . '-' . $month_end . '-' . date('t', mktime(0, 0, 0, $month_end, 1, $year_end));
				$between = " AND DATE(crs_tanggal_periksa) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
				break;
				default:
				$start   = $year_start . '-' . $month_start . '-' . $day_start;
				$end     = $year_end . '-' . $month_end . '-' . $day_end;
				$between = " AND DATE(crs_tanggal_periksa) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
				break;
			}

			if ($puskesmas_id) {
				$district = "AND puskesmas_code_faskes='" . $puskesmas_id . "'";
			} else if ($sub_district_id) {
				$district = "AND pasien_id_kecamatan='" . $sub_district_id . "'";
			} else if ($rs_id) {
				$district = "AND rs_kode_faskes='" . $rs_id . "'";
			} else if ($district_id) {
				$district = "AND pasien_id_kabupaten='" . $district_id . "' ";
			} else if ($province_id) {
				$district = "AND pasien_id_provinsi='" . $province_id . "' ";
			}
		}

		$data = Crs::pasiencrs($between, $district);
		return View::make('pemeriksaan.crs.index', compact('data'));
	}

	public function store() {
		DB::transaction(function () {
			$type = Session::get('type');
			$kd_faskes = Session::get('kd_faskes');
			$faskes_id = Sentry::getUser()->id_user;
						// insert data pelapor
			$dtpelapor = Input::get('dp');
			$dtpelapor['tanggal_laporan'] = $this->encode_date($dtpelapor['tanggal_laporan']);
			$dtpelapor['tanggal_investigasi'] = $this->encode_date($dtpelapor['tanggal_investigasi']);
			$dtpelapor['created_by'] = Sentry::getUser()->id;
			$dtpelapor['created_at'] = date('Y-m-d H:i:s');
			$d1 = [];
			foreach ($dtpelapor as $key=>$val) {
				if (!empty($val)) {
					$d1[$key]=$val;
				}
			}
			$id_pelapor = DB::table('pelapor')->insertGetId($d1);
						// insert data pasien
			$dtpasien = Input::get('dpa');
			$dtpasien['tanggal_lahir'] = $this->encode_date($dtpasien['tanggal_lahir']);
			$dtpasien['created_by'] = Sentry::getUser()->id;
			$dtpasien['created_at'] = date('Y-m-d H:i:s');
			$d2 = [];
			foreach ($dtpasien as $key=>$val) {
				if (!empty($val)) {
					$d2[$key]=$val;
				}
			}
			$id_pasien = DB::table('pasien')->insertGetId($d2);
						// input data pe crs
			$dtpe = Input::get('dpe');
			$dtpe['conjunctivitis_date'] = $this->encode_date($dtpe['conjunctivitis_date']);
			$dtpe['pilek_date'] = $this->encode_date($dtpe['pilek_date']);
			$dtpe['batuk_date'] = $this->encode_date($dtpe['batuk_date']);
			$dtpe['ruam_makulopapular_date'] = $this->encode_date($dtpe['ruam_makulopapular_date']);
			$dtpe['pembengkakan_kelenjar_limfa_date'] = $this->encode_date($dtpe['pembengkakan_kelenjar_limfa_date']);
			$dtpe['demam_date'] = $this->encode_date($dtpe['demam_date']);
			$dtpe['arthralgia_date'] = $this->encode_date($dtpe['arthralgia_date']);
			$dtpe['komplikasi_lain_date'] = $this->encode_date($dtpe['komplikasi_lain_date']);
			$dtpe['vaksinasi_rubella_date'] = $this->encode_date($dtpe['vaksinasi_rubella_date']);
			$dtpe['tgl_diagnosa_rubella'] = $this->encode_date($dtpe['tgl_diagnosa_rubella']);
			$dtpe['desc_kontak_ruam_makulopapular'] = strtoupper($dtpe['desc_kontak_ruam_makulopapular']);
			$dtpe['desc_pergi_waktu_hamil'] = strtoupper($dtpe['desc_pergi_waktu_hamil']);
			$dtpe['created_by'] = Sentry::getUser()->id;
			$dtpe['created_at'] = date('Y-m-d H:i:s');
			$d3 = [];
			foreach ($dtpe as $key=>$val) {
				if (!empty($val)) {
					$d3[$key]=$val;
				}
			}
			$id_pe_crs = DB::table('pe_crs')->insertGetId($d3);
						// input data lab
			$dtlab = Input::get('dlab');
			$dtlab['tgl_ambil_serum1'] = $this->encode_date($dtlab['tgl_ambil_serum1']);
			$dtlab['tgl_kirim_serum1'] = $this->encode_date($dtlab['tgl_kirim_serum1']);
			$dtlab['tgl_tiba_serum1'] = $this->encode_date($dtlab['tgl_tiba_serum1']);
			$dtlab['tgl_ambil_serum2'] = $this->encode_date($dtlab['tgl_ambil_serum2']);
			$dtlab['tgl_kirim_serum2'] = $this->encode_date($dtlab['tgl_kirim_serum2']);
			$dtlab['tgl_tiba_serum2'] = $this->encode_date($dtlab['tgl_tiba_serum2']);
			$dtlab['tgl_ambil_throat_swab'] = $this->encode_date($dtlab['tgl_ambil_throat_swab']);
			$dtlab['tgl_kirim_throat_swab'] = $this->encode_date($dtlab['tgl_kirim_throat_swab']);
			$dtlab['tgl_tiba_throat_swab'] = $this->encode_date($dtlab['tgl_tiba_throat_swab']);
			$dtlab['tgl_ambil_urine'] = $this->encode_date($dtlab['tgl_ambil_urine']);
			$dtlab['tgl_kirim_urine'] = $this->encode_date($dtlab['tgl_kirim_urine']);
			$dtlab['tgl_tiba_urine'] = $this->encode_date($dtlab['tgl_tiba_urine']);
			$dtlab['tgl_igm_serum1'] = $this->encode_date($dtlab['tgl_igm_serum1']);
			$dtlab['tgl_igm_serum2'] = $this->encode_date($dtlab['tgl_igm_serum2']);
			$dtlab['tgl_igg_serum1'] = $this->encode_date($dtlab['tgl_igg_serum1']);
			$dtlab['tgl_igg_serum2'] = $this->encode_date($dtlab['tgl_igg_serum2']);
			$dtlab['tgl_isolasi'] = $this->encode_date($dtlab['tgl_isolasi']);
			$dtlab['created_by'] = Sentry::getUser()->id;
			$dtlab['created_at'] = date('Y-m-d H:i:s');
			$d4 = [];
			foreach ($dtlab as $key=>$val) {
				if (!empty($val)) {
					$d4[$key]=$val;
				}
			}
			$id_hasil_uji_lab_crs = DB::table('hasil_uji_lab_crs')->insertGetId($d4);
						// insert data crs
			$dtcrs = Input::get('dk');
			$dtcrs['id_pelapor'] = $id_pelapor;
			$dtcrs['id_pasien'] = $id_pasien;
			$dtcrs['id_pe_crs'] = $id_pe_crs;
			$dtcrs['id_hasil_uji_lab_crs'] = $id_hasil_uji_lab_crs;
			$dtcrs['created_by'] = Sentry::getUser()->id;
			$dtcrs['created_at'] = date('Y-m-d H:i:s');
			$dtcrs['id_tempat_periksa'] = $faskes_id;
			$dtcrs['kode_faskes'] = $type;
			$dtcrs['tanggal_periksa'] = date('Y-m-d');
			$dtcrs['tgl_mulai_sakit'] = $this->encode_date($dtcrs['tgl_mulai_sakit']);
			$d5 = [];
			foreach ($dtcrs as $key=>$val) {
				if (!empty($val)) {
					$d5[$key]=$val;
				}
			}
			$id_crs = DB::table('crs')->insertGetId($d5);

			if ($type == 'rs') {
				$data = DB::SELECT("SELECT pasien_id_kecamatan AS id_dis_pasien, rs_kode_faskes AS id_dis_faskes FROM getdetailcrs WHERE crs_id_crs=$id_crs");
			} else {
				$data = DB::SELECT("SELECT pasien_id_kecamatan AS id_dis_pasien, puskesmas_code_faskes AS id_dis_faskes FROM getdetailcrs WHERE crs_id_crs=$id_crs");
			}
			$id_kec_pasien = (empty($data) ? '' : $data[0]->id_dis_pasien);
			$id_kec_faskes = (empty($data) ? '' : $data[0]->id_dis_faskes);
						//simpan notifikasi
			if ($id_kec_pasien != $id_kec_faskes) {
				DB::table('notification')->insert(array(
					'from_faskes_id'     => $faskes_id,
					'from_faskes_kec_id' => $id_kec_faskes,
					'to_faskes_kec_id'   => $id_kec_pasien,
					'type_id'            => 'crs',
					'global_id'          => $id_crs,
					'description'        => '',
					'submit_dttm'        => date('Y-m-d H:i:s'),
					));
			}
		});

return Redirect::to('crs');
}

public function getEpid() {
	$idKelurahan = Input::get('id_kelurahan');
	$date        = Input::get('date');
	$noepid      = Crs::getEpid(['id_kelurahan' => $idKelurahan, 'date' => $date]);

	return $noepid;
}

public function getEntri_crs($id) {
	$pe_crs = DB::table('getdetailcrs')
	->WHERE('crs_id_crs', $id)
	->SELECT('pe_diagnosa_rubella AS diagnosa_rubella',
		'pe_tgl_diagnosa_rubella AS tgl_diagnosa_rubella',
		'pe_kontak_ruam_makulopapular AS kontak_ruam_makulopapular',
		'pe_umur_hamil_kontak_ruam_makulopapular AS umur_hamil_kontak_ruam_makulopapular',
		'pe_desc_kontak_ruam_makulopapular AS desc_kontak_ruam_makulopapular',
		'pe_pergi_waktu_hamil AS pergi_waktu_hamil',
		'pe_umur_hamil_waktu_pergi AS umur_hamil_waktu_pergi',
		'pe_desc_pergi_waktu_hamil AS desc_pergi_waktu_hamil',
		'pe_id_pe_crs AS id_pe_crs'
		)
	->get();
	return View::make('pemeriksaan.crs.pe_crs', compact('pe_crs'));
}

public function postEpidCrs() {
	$dt                         = Input::get('dpe');
	$dt['created_by']           = Sentry::getUser()->id;
	$dt['created_at']           = date('Y-m-d H:i:s');
	$dt['faskes_id']            = Sentry::getUser()->id_user;
	$dt['tgl_diagnosa_rubella'] = $this->encode_date($dt['tgl_diagnosa_rubella']);
	$row=[];
	foreach ($dt as $key=>$val) {
		if ($val) {
			$row[$key] = $val;
		}
	}
	$update                     = DB::table('pe_crs')
	->where('id_pe_crs', Input::get('id_pe_crs'))
	->update($row);
	return Redirect::to('crs');
}

		//route daftar crs
public function getDaftarCrs() {
	$pe_crs = Crs::getPeCrs();
	return View::make('pemeriksaan.crs.daftar', compact('pe_crs'));
}

		/*public function getHapus($id)
		{
		$dt['status'] = 0;
		$dt['deleted_by'] = Sentry::getUser()->id;
		$dt['deleted_at'] = date('Y-m-d H:i:s');
		$update = DB::table('crs')
		->where('id_crs',$id)
		->update($dt);
		return Redirect::to('crs');
	}*/

	public function getHapus($id) {
		$dt['deleted_by'] = Sentry::getUser()->id;
		$dt['deleted_at'] = date('Y-m-d H:i:s');
		$update           = DB::table('crs')
		->where('id_crs', $id)
		->update($dt);
		$delete_notif = DB::table('notification')
		->where('global_id', $id)
		->where('type_id', 'crs')
		->update($dt);
		return Redirect::to('crs');
	}

		//fungsi hapus data PE crs
	public function getHapusPeCrs($id) {
		try {
			DB::beginTransaction();
			DB::table('pe_crs')->where('id_pe_crs', $id)->update(['faskes_id' => '0', 'created_by' => '0']);
			DB::commit();
		} catch (Exception $e) {
			DB::rollback();
		}
		return Redirect::to('crs');
	}

	public function detailcrs($id) {
		$crs = DB::table('getdetailcrs')
		->where('crs_id_crs', $id)
		->get();
		return View::make('pemeriksaan.crs.detail', compact('crs'));
	}

	public function getDetailPeCrs($id) {
		$crs = DB::table('getdetailcrs')
		->where('crs_id_crs', $id)
		->get();
		return View::make('pemeriksaan.crs.detail_pe', compact('crs'));
	}

	public function getEditCrs($id) {
		$crs = DB::table('getdetailcrs')
		->where('crs_id_crs', $id)
		->get();
		return View::make('pemeriksaan.crs.edit', compact('crs'));
	}

	public function updateCrs() {
		$type      = Session::get('type');
		$kd_faskes = Session::get('kd_faskes');
		$faskes_id = Sentry::getUser()->id_user;
				// insert data pelapor
		$dtpelapor                        = Input::get('dp');
		$dtpelapor['tanggal_laporan']     = $this->encode_date($dtpelapor['tanggal_laporan']);
		$dtpelapor['tanggal_investigasi'] = $this->encode_date($dtpelapor['tanggal_investigasi']);
		$dtpelapor['updated_by']          = Sentry::getUser()->id;
		$dtpelapor['updated_at']          = date('Y-m-d H:i:s');
		$id_pelapor                       = DB::table('pelapor')
		->where('id_pelapor', Input::get('id_pelapor'))
		->update($dtpelapor);
				// insert data pasien
		$dtpasien                  = Input::get('dpa');
		$dtpasien['tanggal_lahir'] = $this->encode_date($dtpasien['tanggal_lahir']);
		$dtpasien['updated_by']    = Sentry::getUser()->id;
		$dtpasien['updated_at']    = date('Y-m-d H:i:s');
		$id_pasien                 = DB::table('pasien')
		->where('id_pasien', Input::get('id_pasien'))
		->update($dtpasien);
				// input data pe crs
		$dtpe                                     = Input::get('dpe');
		$dtpe['conjunctivitis_date']              = $this->encode_date($dtpe['conjunctivitis_date']);
		$dtpe['pilek_date']                       = $this->encode_date($dtpe['pilek_date']);
		$dtpe['batuk_date']                       = $this->encode_date($dtpe['batuk_date']);
		$dtpe['ruam_makulopapular_date']          = $this->encode_date($dtpe['ruam_makulopapular_date']);
		$dtpe['pembengkakan_kelenjar_limfa_date'] = $this->encode_date($dtpe['pembengkakan_kelenjar_limfa_date']);
		$dtpe['demam_date']                       = $this->encode_date($dtpe['demam_date']);
		$dtpe['arthralgia_date']                  = $this->encode_date($dtpe['arthralgia_date']);
		$dtpe['komplikasi_lain_date']             = $this->encode_date($dtpe['komplikasi_lain_date']);
		$dtpe['vaksinasi_rubella_date']           = $this->encode_date($dtpe['vaksinasi_rubella_date']);
		$dtpe['tgl_diagnosa_rubella']             = $this->encode_date($dtpe['tgl_diagnosa_rubella']);
		$dtpe['desc_kontak_ruam_makulopapular']   = strtoupper($dtpe['desc_kontak_ruam_makulopapular']);
		$dtpe['desc_pergi_waktu_hamil']           = strtoupper($dtpe['desc_pergi_waktu_hamil']);
		$dtpe['updated_by']                       = Sentry::getUser()->id;
		$dtpe['updated_at']                       = date('Y-m-d H:i:s');
		$id_pe_crs                                = DB::table('pe_crs')
		->where('id_pe_crs', Input::get('id_pe_crs'))
		->update($dtpe);
				//  input data lab
		$dtlab                          = Input::get('dlab');
		$dtlab['tgl_ambil_serum1']      = $this->encode_date($dtlab['tgl_ambil_serum1']);
		$dtlab['tgl_kirim_serum1']      = $this->encode_date($dtlab['tgl_kirim_serum1']);
		$dtlab['tgl_tiba_serum1']       = $this->encode_date($dtlab['tgl_tiba_serum1']);
		$dtlab['tgl_ambil_serum2']      = $this->encode_date($dtlab['tgl_ambil_serum2']);
		$dtlab['tgl_kirim_serum2']      = $this->encode_date($dtlab['tgl_kirim_serum2']);
		$dtlab['tgl_tiba_serum2']       = $this->encode_date($dtlab['tgl_tiba_serum2']);
		$dtlab['tgl_ambil_throat_swab'] = $this->encode_date($dtlab['tgl_ambil_throat_swab']);
		$dtlab['tgl_kirim_throat_swab'] = $this->encode_date($dtlab['tgl_kirim_throat_swab']);
		$dtlab['tgl_tiba_throat_swab']  = $this->encode_date($dtlab['tgl_tiba_throat_swab']);
		$dtlab['tgl_ambil_urine']       = $this->encode_date($dtlab['tgl_ambil_urine']);
		$dtlab['tgl_kirim_urine']       = $this->encode_date($dtlab['tgl_kirim_urine']);
		$dtlab['tgl_tiba_urine']        = $this->encode_date($dtlab['tgl_tiba_urine']);
		$dtlab['tgl_igm_serum1']        = $this->encode_date($dtlab['tgl_igm_serum1']);
		$dtlab['tgl_igm_serum2']        = $this->encode_date($dtlab['tgl_igm_serum2']);
		$dtlab['tgl_igg_serum1']        = $this->encode_date($dtlab['tgl_igg_serum1']);
		$dtlab['tgl_igg_serum2']        = $this->encode_date($dtlab['tgl_igg_serum2']);
		$dtlab['tgl_isolasi']           = $this->encode_date($dtlab['tgl_isolasi']);
		$dtlab['updated_by']            = Sentry::getUser()->id;
		$dtlab['updated_at']            = date('Y-m-d H:i:s');
		$id_hasil_uji_lab_crs           = DB::table('hasil_uji_lab_crs')
		->where('id_hasil_uji_lab_crs', Input::get('id_hasil_uji_lab'))
		->update($dtlab);
				// insert data crs
		$dtcrs                      = Input::get('dk');
		$dtcrs['created_by']        = Sentry::getUser()->id;
		$dtcrs['id_tempat_periksa'] = $faskes_id;
		$dtcrs['kode_faskes']       = $type;
		$dtcrs['tanggal_periksa']   = date('Y-m-d');
		$dtcrs['tgl_mulai_sakit']   = $this->encode_date($dtcrs['tgl_mulai_sakit']);
		$dtcrs['updated_by']        = Sentry::getUser()->id;
		$dtcrs['updated_at']        = date('Y-m-d H:i:s');
		$id_crs                     = DB::table('crs')
		->where('id_crs', Input::get('id_crs'))
		->update($dtcrs);
		return Redirect::to('crs');
	}

	public function ExportExcelAll($filetype, $dt) {
		$ds        = json_decode($dt);
		$excel_tmp = [
		['No', 'No Epid', 'No.RM', 'Nama Penderita', 'Jenis kelamin', 'Tanggal lahir', 'Umur', '', '', 'Alamat', 'Kelurahan', 'Kecamatan', 'Kabupaten', 'Provinsi', 'Tempat bayi di lahirkan', 'Nama Ibu', 'No Telp', 'Umur Kehamilan saat bayi di lahirkan', 'Berat badan bayi baru lahir', 'Data Pelapor', '', '', '', '', 'Data Klinis Pasien', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Data Riwayat Kehamilan Ibu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Data Pemeriksaan dan Hasil Lab (Rumah Sakit)', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Data Pemeriksaan dan Hasil Lab (Lab Rujukan Nasional)', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Klasifikasi Final', 'Detail PE Penyakit CRS', '', '', '', '', '', '', ''],
		['', '', '', '', '', '', 'Tahun', 'Bulan', 'Hari', '', '', '', '', '', '', '', '', '', '', 'Nama Rumah Sakit', 'Provinsi', 'Kabupaten', 'Tanggal Laporan', 'Tanggal Investigasi', 'Nama Dokter Pemeriksa', 'Tanggal Periksa', 'Keadaan Bayi', 'Group A', '', '', '', '', 'Group B', '', '', '', '', '', '', '', 'Jumlah kehamilan sebelumnya', 'Umur Ibu (dalam tahun)', 'Hari Pertama haid terakhir (HPHT)', 'Activity penyakit', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Pemeriksaan Lab', '', '', '', '', '', '', '', '', '', '', '', '', 'Hasil Pemeriksaan Lab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Pemeriksaan Lab', '', '', '', '', '', '', '', '', '', '', '', '', 'Hasil Pemeriksaan Lab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Diagnosa Rubella', 'Tgl Diagnosa', 'Kontak Ruam makulopapular', 'Umur Kehamilan', 'Lokasi tertular', 'Perjalanan', 'Umur kehamilan', 'Tujuan perjalanan'],
		['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Congenital heart disease', 'Cataracts', 'Congenital glaucoma', 'Pigmentary retinopathy', 'Hearing impairment', 'Purpura', 'Microcephaly', 'Meningoencephalitis', 'Ikterik 24 jam post partum', 'Splenomegaly', 'Developmental delay', 'Radiolucent bone disease', 'Other abnormalities', '', '', '', 'Conjunctivitis', 'Tgl Kejadian', 'Pilek', 'Tgl Kejadian', 'Batuk', 'Tgl Kejadian', 'Ruam makulopapular', 'Tgl Kejadian', 'Pembengkakan kelenjar limfa', 'Tgl Kejadian', 'Demam', 'Tgl Kejadian', 'Arthralgia/arthritis', 'Tgl Kejadian', 'Komplikasi lain', 'Tgl Kejadian', 'Vaksinasi rubella', 'Tgl Kejadian', 'Diagnosa Rubella Lab', 'Tgl Diagnosa', 'Ruam Makulopapular', 'Umur kehamilan kejadian', 'Lokasi tertular', 'Bepergian selama kehamilan', 'Umur kehamilan', 'Tempat tujuan', 'Pengambilan Spesimen', 'Serum 1', '', '', 'Serum 2', '', '', 'Throat Swab', '', '', 'Urine', '', '', 'IgM serum ke 1', '', '', 'IgM serum 2 (ulangan)', '', '', 'IgG serum 1', '', '', '', 'IgG serum 2 (ulangan)', '', '', '', 'Isolasi', '', '', 'Pengambilan Spesimen', 'Serum 1', '', '', 'Serum 2', '', '', 'Throat Swab', '', '', 'Urine', '', '', 'IgM serum ke 1', '', '', 'IgM serum 2 (ulangan)', '', '', 'IgG serum 1', '', '', '', 'IgG serum 2 (ulangan)', '', '', '', 'Isolasi', '', '', '', '', '', '', '', '', '', '', ''],
		['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Tanggal Ambil', 'Tanggal Kirim', 'Tanggal Tiba', 'Tanggal Ambil', 'Tanggal Kirim', 'Tanggal Tiba', 'Tanggal Ambil', 'Tanggal Kirim', 'Tanggal Tiba', 'Tanggal Ambil', 'Tanggal Kirim', 'Tanggal Tiba', 'Hasil', 'Jenis Virus', 'Tgl Hasil Lab', 'Hasil', 'Jenis Virus', 'Tgl Hasil Lab', 'Hasil', 'Jenis Virus', 'Tgl Hasil Lab', 'Kadar IgG', 'Hasil', 'Jenis Virus', 'Tgl Hasil Lab', 'Kadar IgG', 'Hasil', 'Jenis Virus', 'Tgl Hasil Lab', '', 'Tanggal Ambil', 'Tanggal Kirim', 'Tanggal Tiba', 'Tanggal Ambil', 'Tanggal Kirim', 'Tanggal Tiba', 'Tanggal Ambil', 'Tanggal Kirim', 'Tanggal Tiba', 'Tanggal Ambil', 'Tanggal Kirim', 'Tanggal Tiba', 'Hasil', 'Jenis Virus', 'Tgl Hasil Lab', 'Hasil', 'Jenis Virus', 'Tgl Hasil Lab', 'Hasil', 'Jenis Virus', 'Tgl Hasil Lab', 'Kadar IgG', 'Hasil', 'Jenis Virus', 'Tgl Hasil Lab', 'Kadar IgG', 'Hasil', 'Jenis Virus', 'Tgl Hasil Lab', '', '', '', '', '', '', '', '', ''],
		];

		foreach ($ds as $k => $v) {
			$post[$v->name] = $v->value;
		}
		$between         = $district         = '';
		$unit            = (empty($post['unit']) ? '' : $post['unit']);
		$day_start       = (empty($post['day_start']) ? '' : $post['day_start']);
		$month_start     = (empty($post['month_start']) ? '' : $post['month_start']);
		$year_start      = (empty($post['year_start']) ? '' : $post['year_start']);
		$day_end         = (empty($post['day_end']) ? '' : $post['day_end']);
		$month_end       = (empty($post['month_end']) ? '' : $post['month_end']);
		$year_end        = (empty($post['year_end']) ? '' : $post['year_end']);
		$province_id     = (empty($post['province_id']) ? '' : $post['province_id']);
		$district_id     = (empty($post['district_id']) ? '' : $post['district_id']);
		$sub_district_id = (empty($post['sub_district_id']) ? '' : $post['sub_district_id']);
		$rs_id           = (empty($post['rs_id']) ? '' : $post['rs_id']);
		$puskesmas_id    = (empty($post['puskesmas_id']) ? '' : $post['puskesmas_id']);

		switch ($unit) {
			case "all":
			$between = "";
			break;
			case "year":
			$start   = $year_start;
			$end     = $year_end;
			$between = " AND YEAR(crs_tanggal_periksa) BETWEEN '" . $start . "' AND '" . $end . "' ";
			break;
			case "month":
			$start   = $year_start . '-' . $month_start . '-1';
			$end     = $year_end . '-' . $month_end . '-' . date('t', mktime(0, 0, 0, $month_end, 1, $year_end));
			$between = " AND DATE(crs_tanggal_periksa) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
			break;
			default:
			$start   = $year_start . '-' . $month_start . '-' . $day_start;
			$end     = $year_end . '-' . $month_end . '-' . $day_end;
			$between = " AND DATE(crs_tanggal_periksa) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
			break;
		}

		if ($puskesmas_id) {
			$district = "AND puskesmas_code_faskes='" . $puskesmas_id . "'";
		} else if ($sub_district_id) {
			$district = "AND pasien_id_kecamatan='" . $sub_district_id . "'";
		} else if ($rs_id) {
			$district = "AND rs_kode_faskes='" . $rs_id . "' ";
		} else if ($district_id) {
			$district = "AND pasien_id_kabupaten='" . $district_id . "' ";
		} else if ($province_id) {
			$district = "AND pasien_id_provinsi='" . $province_id . "' ";
		}

		$data = Crs::exportpasien($between, $district);

		if (!empty($data)) {
			$rows = 4;
			$i    = 0;
			foreach ($data as $key => $val) {
				$excel_tmp[$rows + $i][] = $i + 1;
				$excel_tmp[$rows + $i][] = $val->crs_no_epid;
				$excel_tmp[$rows + $i][] = $val->crs_no_rm;
				$excel_tmp[$rows + $i][] = $val->pasien_nama_anak;
				switch ($val->pasien_jenis_kelamin) {
					case '1':
					$jenis_kelamin = 'Laki-laki';
					break;
					case '2':
					$jenis_kelamin = 'Perempuan';
					break;
					case '3':
					$jenis_kelamin = 'Tidak Jelas';
					break;
					default:
					$jenis_kelamin = 'Tidak di isi';
					break;
				}
				$excel_tmp[$rows + $i][] = $jenis_kelamin;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->pasien_tanggal_lahir);
				$excel_tmp[$rows + $i][] = $val->pasien_umur;
				$excel_tmp[$rows + $i][] = $val->pasien_umur_bln;
				$excel_tmp[$rows + $i][] = $val->pasien_umur_hr;
				$excel_tmp[$rows + $i][] = $val->pasien_alamat;
				$excel_tmp[$rows + $i][] = $val->pasien_kelurahan;
				$excel_tmp[$rows + $i][] = $val->pasien_kecamatan;
				$excel_tmp[$rows + $i][] = $val->pasien_kabupaten;
				$excel_tmp[$rows + $i][] = $val->pasien_provinsi;
				$excel_tmp[$rows + $i][] = $val->pasien_tempat_lahir_bayi;
				$excel_tmp[$rows + $i][] = $val->pasien_nama_ortu;
				$excel_tmp[$rows + $i][] = $val->pasien_no_telp;
				$excel_tmp[$rows + $i][] = $val->pasien_umur_kehamilan_bayi;
				$excel_tmp[$rows + $i][] = $val->pasien_berat_badan_bayi;
				$excel_tmp[$rows + $i][] = $val->pelapor_nama_rs;
				$pelapor_provinsi        = DB::table('provinsi')->WHERE('id_provinsi', $val->pelapor_id_provinsi)->pluck('provinsi');
				$pelapor_kabupaten       = DB::table('kabupaten')->WHERE('id_kabupaten', $val->pelapor_id_kabupaten)->pluck('kabupaten');
				$excel_tmp[$rows + $i][] = $pelapor_provinsi;
				$excel_tmp[$rows + $i][] = $pelapor_kabupaten;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->pelapor_tanggal_laporan);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->pelapor_tanggal_investigasi);
				$excel_tmp[$rows + $i][] = $val->crs_nama_dokter_pemeriksa;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->crs_tanggal_periksa);
				switch ($val->crs_keadaan_akhir) {
					case 'Meniggal':
					$penyebabMeninggal = ', ' . $val->crs_penyebab_meninggal;
					break;
					default:
					$penyebabMeninggal = '';
					break;
				}
				$excel_tmp[$rows + $i][] = $val->crs_keadaan_akhir . '' . $penyebabMeninggal;
				$excel_tmp[$rows + $i][] = $val->crs_congenital_heart_disease;
				$excel_tmp[$rows + $i][] = $val->crs_cataracts;
				$excel_tmp[$rows + $i][] = $val->crs_congenital_glaucoma;
				$excel_tmp[$rows + $i][] = $val->crs_pigmentary_retinopathy;
				$excel_tmp[$rows + $i][] = $val->crs_hearing_impairment;
				$excel_tmp[$rows + $i][] = $val->crs_purpura;
				$excel_tmp[$rows + $i][] = $val->crs_microcephaly;
				$excel_tmp[$rows + $i][] = $val->crs_meningoencephalitis;
				$excel_tmp[$rows + $i][] = $val->crs_ikterik;
				$excel_tmp[$rows + $i][] = $val->crs_splenomegaly;
				$excel_tmp[$rows + $i][] = $val->crs_developmental_delay;
				$excel_tmp[$rows + $i][] = $val->crs_radiolucent;
				switch ($val->crs_other_abnormal) {
					case 'Ya':
					$dtOther = ', ' . $val->crs_dt_other_abnormal;
					break;
					default:
					$dtOther = '';
					break;
				}
				$excel_tmp[$rows + $i][] = $val->crs_other_abnormal . '' . $dtOther;
				$excel_tmp[$rows + $i][] = $val->pe_jumlah_kehamilan_sebelumnya;
				$excel_tmp[$rows + $i][] = $val->pe_umur_ibu;
				$excel_tmp[$rows + $i][] = $val->pe_hari_pertama_haid;
				$excel_tmp[$rows + $i][] = $val->pe_conjunctivitis;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->pe_conjunctivitis_date);
				$excel_tmp[$rows + $i][] = $val->pe_pilek;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->pe_pilek_date);
				$excel_tmp[$rows + $i][] = $val->pe_batuk;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->pe_batuk_date);
				$excel_tmp[$rows + $i][] = $val->pe_ruam_makulopapular;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->pe_ruam_makulopapular_date);
				$excel_tmp[$rows + $i][] = $val->pe_pembengkakan_kelenjar_limfa;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->pe_pembengkakan_kelenjar_limfa_date);
				$excel_tmp[$rows + $i][] = $val->pe_demam;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->pe_demam_date);
				$excel_tmp[$rows + $i][] = $val->pe_arthralgia;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->pe_arthralgia_date);
				$excel_tmp[$rows + $i][] = $val->pe_komplikasi_lain;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->pe_komplikasi_lain_date);
				$excel_tmp[$rows + $i][] = $val->pe_vaksinasi_rubella;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->pe_vaksinasi_rubella_date);
				$excel_tmp[$rows + $i][] = $val->pe_diagnosa_rubella;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->pe_tgl_diagnosa_rubella);
				$excel_tmp[$rows + $i][] = $val->pe_kontak_ruam_makulopapular;
				$excel_tmp[$rows + $i][] = $val->pe_umur_hamil_kontak_ruam_makulopapular;
				$excel_tmp[$rows + $i][] = $val->pe_desc_kontak_ruam_makulopapular;
				$excel_tmp[$rows + $i][] = $val->pe_pergi_waktu_hamil;
				$excel_tmp[$rows + $i][] = $val->pe_umur_hamil_waktu_pergi;
				$excel_tmp[$rows + $i][] = $val->pe_desc_pergi_waktu_hamil;
				$excel_tmp[$rows + $i][] = $val->rs_spesimen;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->rs_tgl_ambil_serum1);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->rs_tgl_kirim_serum1);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->rs_tgl_tiba_serum1);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->rs_tgl_ambil_serum2);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->rs_tgl_kirim_serum2);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->rs_tgl_tiba_serum2);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->rs_tgl_ambil_throat_swab);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->rs_tgl_kirim_throat_swab);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->rs_tgl_tiba_throat_swab);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->rs_tgl_ambil_urine);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->rs_tgl_kirim_urine);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->rs_tgl_tiba_urine);
				$excel_tmp[$rows + $i][] = $val->rs_hasil_igm_serum1;
				$excel_tmp[$rows + $i][] = $val->rs_virus_igm_serum1;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->rs_tgl_igm_serum1);
				$excel_tmp[$rows + $i][] = $val->rs_hasil_igm_serum2;
				$excel_tmp[$rows + $i][] = $val->rs_virus_igm_serum2;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->rs_tgl_igm_serum2);
				$excel_tmp[$rows + $i][] = $val->rs_hasil_igg_serum1;
				$excel_tmp[$rows + $i][] = $val->rs_virus_igg_serum1;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->rs_tgl_igg_serum1);
				$excel_tmp[$rows + $i][] = $val->kadar_igg_serum1;
				$excel_tmp[$rows + $i][] = $val->rs_hasil_igg_serum2;
				$excel_tmp[$rows + $i][] = $val->rs_virus_igg_serum2;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->rs_tgl_igg_serum2);
				$excel_tmp[$rows + $i][] = $val->kadar_igg_serum2;
				$excel_tmp[$rows + $i][] = $val->rs_hasil_isolasi;
				$excel_tmp[$rows + $i][] = $val->rs_virus_isolasi;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->rs_tgl_isolasi);
				$excel_tmp[$rows + $i][] = $val->lab_spesimen;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->lab_tgl_ambil_serum1);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->lab_tgl_kirim_serum1);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->lab_tgl_tiba_serum1);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->lab_tgl_ambil_serum2);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->lab_tgl_kirim_serum2);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->lab_tgl_tiba_serum2);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->lab_tgl_ambil_throat_swab);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->lab_tgl_kirim_throat_swab);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->lab_tgl_tiba_throat_swab);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->lab_tgl_ambil_urine);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->lab_tgl_kirim_urine);
				$excel_tmp[$rows + $i][] = Helper::getDate($val->lab_tgl_tiba_urine);
				$excel_tmp[$rows + $i][] = $val->lab_hasil_igm_serum1;
				$excel_tmp[$rows + $i][] = $val->lab_virus_igm_serum1;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->lab_tgl_igm_serum1);
				$excel_tmp[$rows + $i][] = $val->lab_hasil_igm_serum2;
				$excel_tmp[$rows + $i][] = $val->lab_virus_igm_serum2;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->lab_tgl_igm_serum2);
				$excel_tmp[$rows + $i][] = $val->lab_hasil_igg_serum1;
				$excel_tmp[$rows + $i][] = $val->lab_virus_igg_serum1;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->lab_tgl_igg_serum1);
				$excel_tmp[$rows + $i][] = $val->lab_kadar_igg_serum1;
				$excel_tmp[$rows + $i][] = $val->lab_hasil_igg_serum2;
				$excel_tmp[$rows + $i][] = $val->lab_virus_igg_serum2;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->lab_tgl_igg_serum2);
				$excel_tmp[$rows + $i][] = $val->lab_kadar_igg_serum2;
				$excel_tmp[$rows + $i][] = $val->lab_hasil_isolasi;
				$excel_tmp[$rows + $i][] = $val->lab_virus_isolasi;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->lab_tgl_isolasi);
				switch ($val->crs_klasifikasi_final) {
					case 'Discarded':
					$descKlasifikasi = ', Desc :' . $val->crs_klasifikasi_final_desc;
					break;
					case 'Bukan CRS':
					$descKlasifikasi = ', Desc :' . $val->crs_klasifikasi_final_desc;
					break;
					default:
					$descKlasifikasi = '';
					break;
				}
				$excel_tmp[$rows + $i][] = $val->crs_klasifikasi_final . '' . $descKlasifikasi;
				$excel_tmp[$rows + $i][] = $val->pe_diagnosa_rubella;
				$excel_tmp[$rows + $i][] = Helper::getDate($val->pe_tgl_diagnosa_rubella);
				$excel_tmp[$rows + $i][] = $val->pe_kontak_ruam_makulopapular;
				$excel_tmp[$rows + $i][] = $val->pe_umur_hamil_kontak_ruam_makulopapular;
				$excel_tmp[$rows + $i][] = $val->pe_desc_kontak_ruam_makulopapular;
				$excel_tmp[$rows + $i][] = $val->pe_pergi_waktu_hamil;
				$excel_tmp[$rows + $i][] = $val->pe_umur_hamil_waktu_pergi;
				$excel_tmp[$rows + $i][] = $val->pe_desc_pergi_waktu_hamil;
				$i++;
			}
		}

		Excel::create('FORM_EXPORT_CRS_' . date('Y_m_d_h_i_s'), function ($excel) use ($excel_tmp) {
			$excel->sheet('Sheetname', function ($sheet) use ($excel_tmp) {
				$sheet->fromArray($excel_tmp);
				$sheet->mergeCells('A2:A5');
				$sheet->mergeCells('B2:B5');
				$sheet->mergeCells('C2:C5');
				$sheet->mergeCells('D2:D5');
				$sheet->mergeCells('E2:E5');
				$sheet->mergeCells('F2:F5');
				$sheet->mergeCells('G2:I2');
				$sheet->mergeCells('G3:G5');
				$sheet->mergeCells('H3:H5');
				$sheet->mergeCells('I3:I5');
				$sheet->mergeCells('J2:J5');
				$sheet->mergeCells('K2:K5');
				$sheet->mergeCells('L2:L5');
				$sheet->mergeCells('M2:M5');
				$sheet->mergeCells('N2:N5');
				$sheet->mergeCells('O2:O5');
				$sheet->mergeCells('P2:P5');
				$sheet->mergeCells('Q2:Q5');
				$sheet->mergeCells('R2:R5');
				$sheet->mergeCells('S2:S5');
				$sheet->mergeCells('T2:X2');
				$sheet->mergeCells('T3:T5');
				$sheet->mergeCells('U3:U5');
				$sheet->mergeCells('V3:V5');
				$sheet->mergeCells('W3:W5');
				$sheet->mergeCells('X3:X5');
				$sheet->mergeCells('Y2:AN2');
				$sheet->mergeCells('Y3:Y5');
				$sheet->mergeCells('Z3:Z5');
				$sheet->mergeCells('AA3:AA5');
				$sheet->mergeCells('AB3:AF3');
				$sheet->mergeCells('AB4:AB5');
				$sheet->mergeCells('AC4:AC5');
				$sheet->mergeCells('AD4:AD5');
				$sheet->mergeCells('AE4:AE5');
				$sheet->mergeCells('AF4:AF5');
				$sheet->mergeCells('AG3:AN3');
				$sheet->mergeCells('AG4:AG5');
				$sheet->mergeCells('AH4:AH5');
				$sheet->mergeCells('AI4:AI5');
				$sheet->mergeCells('AJ4:AJ5');
				$sheet->mergeCells('AK4:AK5');
				$sheet->mergeCells('AL4:AL5');
				$sheet->mergeCells('AM4:AM5');
				$sheet->mergeCells('AN4:AN5');
				$sheet->mergeCells('AO2:BQ2');
				$sheet->mergeCells('AO3:AO5');
				$sheet->mergeCells('AP3:AP5');
				$sheet->mergeCells('AQ3:AQ5');
				$sheet->mergeCells('AR3:BQ3');
				$sheet->mergeCells('AR4:AR5');
				$sheet->mergeCells('AS4:AS5');
				$sheet->mergeCells('AT4:AT5');
				$sheet->mergeCells('AU4:AU5');
				$sheet->mergeCells('AV4:AV5');
				$sheet->mergeCells('AW4:AW5');
				$sheet->mergeCells('AX4:AX5');
				$sheet->mergeCells('AY4:AY5');
				$sheet->mergeCells('AZ4:AZ5');
				$sheet->mergeCells('BA4:BA5');
				$sheet->mergeCells('BB4:BB5');
				$sheet->mergeCells('BC4:BC5');
				$sheet->mergeCells('BD4:BD5');
				$sheet->mergeCells('BE4:BE5');
				$sheet->mergeCells('BF4:BF5');
				$sheet->mergeCells('BG4:BG5');
				$sheet->mergeCells('BH4:BH5');
				$sheet->mergeCells('BI4:BI5');
				$sheet->mergeCells('BJ4:BJ5');
				$sheet->mergeCells('BK4:BK5');
				$sheet->mergeCells('BL4:BL5');
				$sheet->mergeCells('BM4:BM5');
				$sheet->mergeCells('BN4:BN5');
				$sheet->mergeCells('BO4:BO5');
				$sheet->mergeCells('BP4:BP5');
				$sheet->mergeCells('BQ4:BQ5');
				$sheet->mergeCells('BR2:CU2');
				$sheet->mergeCells('BR3:CD3');
				$sheet->mergeCells('BR4:BR5');
				$sheet->mergeCells('BS4:BU4');
				$sheet->mergeCells('BV4:BX4');
				$sheet->mergeCells('BY4:CA4');
				$sheet->mergeCells('CB4:CD4');
				$sheet->mergeCells('CE3:CU3');
				$sheet->mergeCells('CE4:CG4');
				$sheet->mergeCells('CH4:CJ4');
				$sheet->mergeCells('CK4:CN4');
				$sheet->mergeCells('CO4:CR4');
				$sheet->mergeCells('CS4:CU4');
				$sheet->mergeCells('CV2:DY2');
				$sheet->mergeCells('CV3:DH3');
				$sheet->mergeCells('CV4:CV5');
				$sheet->mergeCells('CW4:CY4');
				$sheet->mergeCells('CZ4:DB4');
				$sheet->mergeCells('DC4:DE4');
				$sheet->mergeCells('DF4:DH4');
				$sheet->mergeCells('DI3:DY3');
				$sheet->mergeCells('DI4:DK4');
				$sheet->mergeCells('DL4:DN4');
				$sheet->mergeCells('DO4:DR4');
				$sheet->mergeCells('DS4:DV4');
				$sheet->mergeCells('DW4:DY4');
				$sheet->mergeCells('DZ2:DZ5');
				$sheet->mergeCells('EA2:EH2');
				$sheet->mergeCells('EA3:EA5');
				$sheet->mergeCells('EB3:EB5');
				$sheet->mergeCells('EC3:EC5');
				$sheet->mergeCells('ED3:ED5');
				$sheet->mergeCells('EE3:EE5');
				$sheet->mergeCells('EF3:EF5');
				$sheet->mergeCells('EG3:EG5');
				$sheet->mergeCells('EH3:EH5');

				$sheet->cells('A2:EH5', function ($cells) {
					$cells->setAlignment('center');
					$cells->setValignment('center');
					$cells->setBackground('#FFFFAA');
					$cells->setFontSize(13);
					$cells->setFontWeight('bold');
				});
				$sheet->setBorder('A2:EH5', 'thin');
				$sheet->setFreeze('D6');
				$sheet->setAutoFilter('A5:EH5');
				$sheet->setAutoSize(true);
			});
})->export('xls');
}

public function genExcel() {
	\Excel::create('file', function ($excel) {
		require_once ("/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
		require_once ("/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");

		$excel->sheet('New sheet', function ($sheet) {

			$sheet->SetCellValue("A1", "UK");
			$sheet->SetCellValue("A2", "USA");

			$sheet->_parent->addNamedRange(
				new \PHPExcel_NamedRange(
					'countries', $sheet, 'A1:A2'
					)
				);
			$sheet->SetCellValue("B1", "London");
			$sheet->SetCellValue("B2", "Birmingham");
			$sheet->SetCellValue("B3", "Leeds");
			$sheet->_parent->addNamedRange(
				new \PHPExcel_NamedRange(
					'UK', $sheet, 'B1:B3'
					)
				);
			$sheet->SetCellValue("C1", "Atlanta");
			$sheet->SetCellValue("C2", "New York");
			$sheet->SetCellValue("C3", "Los Angeles");
			$sheet->_parent->addNamedRange(
				new \PHPExcel_NamedRange(
					'USA', $sheet, 'C1:C3'
					)
				);
			$objValidation = $sheet->getCell('D1')->getDataValidation();
			$objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
			$objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
			$objValidation->setAllowBlank(false);
			$objValidation->setShowInputMessage(true);
			$objValidation->setShowErrorMessage(true);
			$objValidation->setShowDropDown(true);
			$objValidation->setErrorTitle('Input error');
			$objValidation->setError('Value is not in list.');
			$objValidation->setPromptTitle('Pick from list');
			$objValidation->setPrompt('Please pick a value from the drop-down list.');
								$objValidation->setFormula1('countries'); //note this!
							});
	})->download("xlsx");
	echo 'success';
}
}