<?php
	use Rah\Danpu\Dump;
	use Rah\Danpu\Export;
	use Rah\Danpu\Import;

class DatabaseController extends BaseController {
	
	public function __construct()
	{
		// filter
		$this->beforeFilter('auth');
		//$this->beforeFilter('csrf', array('only' => array('restore')));
		$this->dsn = 'mysql:dbname=' . Config::get('database.connections.mysql.database', 'surveilan') . ';host=127.0.0.1';
		$this->user = Config::get('database.connections.mysql.username', 'root');
		$this->pass = Config::get('database.connections.mysql.password', '');
	}
	
	public function backup()
	{
		// path file
		$file = storage_path() . '/backups/' . date('dmYHis') . '.sql';
		// dump database
		$dump = new Dump;
		$dump->file($file)->dsn($this->dsn)->user($this->user)->pass($this->pass);
		new Export($dump);
		// unduh file dump
		return Response::download($file, date('dmYHis') . '.sql');
	}

	public function restore()
	{
		return View::make('admin.restore');
	}

	public function postRestore()
	{
		// validasi
		$input = Input::get('sql');
		$rules = array('sql' => 'required');
		$validasi = Validator::make(Input::all(), $rules);
		// tidak valid
		if ($validasi->fails()) {
			// pesan
			//$sql = $validasi->messages()->first('sql') ?: '';
			return View::make('admin.restore');
			// valid
		} else {
			// ada sql
			if (Input::hasFile('sql')) {
			// nama sql
			$sql = date('dmYHis') . '.sql';
			// unggah sql ke dir "app/storage/restores"
			Input::file('sql')->move(storage_path() . '/restores/', $sql);
			// path file
			$file = storage_path() . '/restores/' . $sql;
			// dump database
			$dump = new Dump;
			$dump->file($file)->dsn($this->dsn)->user($this->user)->pass($this->pass);
			new Import($dump);
			// hapus file restore
			Delete::file($file);
			// tidak ada sql
		} else {
			// pesan
			$sql = 'Sql gagal diunggah.';
			return View::make('admin.restore',compact('sql'));
			}
		}

		return Redirect::to('dashboard');
	}


	public function Importexcel() {

		//di sini bisa ambil juga dari table
		$data = array(
        	array('Laki-laki', '54'),
        	array('Perempuan', '65')
    	);

    	Excel::create('nama_file_data_jenis_kelamin', function($excel) use($data) {

        	$excel->sheet('Sheetname', function($sheet) use($data) {

            	$sheet->fromArray($data);

        });

    	})->export('xls');
	}
}