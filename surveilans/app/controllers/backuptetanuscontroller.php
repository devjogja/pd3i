<?php

class BackupTetanusController extends \BaseController {

	//load view
	protected $layout = "layouts.master"; 
	
	public function __construct()
	{
		//filter
		$this->beforeFilter('auth');
	}
	/**
	 * Display a listing of tetanus
	 *
	 * @return Response
	 */
	public function index()
	{
		//$tetanus = Pasien::getPasientetanus();
		Session::put('sess_id', '');
		Session::put('penyakit', 'Tetanus');
		return View::make('pemeriksaan.tetanus.index',compact('tetanus'));
	}

	/**
	 * Show the form for creating a new tetanus
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('pemeriksaan.tetanus.index');
	}

	/**
	 * Store a newly created tetanus in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
		$pasien=DB::select("select nik from pasien where nik='".Input::get('nik')."'");
		if(!$pasien)
		{
		//simpan data personal
		DB::table('pasien')->insert(
			array(
				'nik'=>Input::get('nik'),
				'nama_anak'=>Input::get('nama_anak'),
				'nama_ortu'=>Input::get('nama_ortu'),
				'alamat'=>Input::get('alamat'),
				'tanggal_lahir'=>Input::get('tanggal_lahir'),
				'umur'=>Input::get('tgl_tahun'),
				'umur_bln'=>Input::get('tgl_bulan'),
				'umur_hr'=>Input::get('tgl_hari'),
				'jenis_kelamin'=>Input::get('jenis_kelamin'),
				'id_kelurahan'=>Input::get('id_kelurahan')
			));
		}
		//simpan data diagnosa penyakit tetanus
		DB::table('tetanus')->insert(
			array(
				'no_epid'=>Input::get('no_epid'),
				'tanggal_laporan_diterima'=>Input::get('tanggal_laporan_diterima'),
				'tanggal_pelacakan'=>Input::get('tanggal_pelacakan'),
				'ANC'=>Input::get('ANC'),
				'status_imunisasi'=>Input::get('status_imunisasi'),
				'pregnancy_helper'=>Input::get('pregnancy_helper'),
				'penolong_persalinan'=>Input::get('penolong_persalinan'),
				'perawatan_tali_pusar'=>Input::get('perawatan_tali_pusar'),
				'pemotongan_tali_pusar'=>Input::get('pemotongan_tali_pusar'),
				'rawat_rumah_sakit'=>Input::get('rawat_rumah_sakit'),
				'imunisasi_TT_ibu_saat_hamil'=>Input::get('imunisasi_TT_ibu_saat_hamil'),
				'status_TT_ibu_saat_hamil'=>Input::get('status_TT_ibu_saat_hamil'),
				'bayi_kejang'=>Input::get('bayi_kejang'),
				'riwayat_persalinan'=>Input::get('riwayat_persalinan'),
				'tanggal_mulai_sakit'=>Input::get('tanggal_mulai_sakit'),
				'bayi_lahir_menangis'=>Input::get('bayi_lahir_menangis'),
				'tanda_kehidupan_lain'=>Input::get('tanda_kehidupan_lain'),
				'mulut_mecucu'=>Input::get('mulut_mecucu'),
				'dirawat'=>Input::get('dirawat'),
				'tempat_dirawat'=>Input::get('tempat_dirawat'),
				'keadaan_akhir'=>Input::get('keadaan_akhir'),
				'klasifikasi_akhir'=>Input::get('klasifikasi_akhir'),
				'tanggal_periksa'=>date('Y-m-d')
			));
		//ambil id terakhir pada table pasien
		$pasien=DB::select("select id_pasien from pasien where nik='".Input::get('nik')."'");
		if($pasien)
		{
			foreach ($pasien as $value) {
				$id_pasien = $value->id_pasien;
			}
		}
		//ambil id terakhir pada table tetanus
		$id_tetanus = DB::table('tetanus')->max('id_tetanus');
		//simpan kedalam table pasien_terserang_penyakit
		DB::table('pasien_terserang_tetanus')->insert(
			array(
				'id_pasien'=>$id_pasien,
				'id_tetanus'=> $id_tetanus
				));

		return Redirect::to('tetanus');
	}

	/**
	 * Display the specified tetanus.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$tetanus = tetanus::findOrFail($id);

		return View::make('pemeriksaan.tetanus.show', compact('tetanus'));
	}

	/**
	 * Show the form for editing the specified tetanus.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$tetanus = tetanus::find($id);

		return View::make('pemeriksaan.tetanus.edit', compact('tetanus'));
	}

	/**
	 * Update the specified tetanus in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$tetanus = tetanus::findOrFail($id);

		$validator = Validator::make($data = Input::all(), tetanus::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$tetanus->update($data);

		return Redirect::route('pemeriksaan.tetanus.index');
	}

	/**
	 * Remove the specified tetanus from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		tetanus::destroy($id);

		return Redirect::route('pemeriksaan.tetanus.index');
	}

	public function postForm()
	{
		$id = Input::get('id');
		if($id==1) 
		{
			return View::make('diagnosa.tetanus');
		}
	} 

	public function detailtetanus($id){

		$tetanus = DB::select("SELECT 
								pasien.nik,
								pasien.tanggal_lahir, 
								pasien.nama_anak, 
								pasien.nama_ortu, 
								CONCAT_WS(',',pasien.alamat,lokasi.kelurahan,lokasi.kecamatan,lokasi.kabupaten,lokasi.provinsi) as alamat, 
								pasien.umur, 
								pasien.jenis_kelamin,
								tetanus.no_epid,  
								tetanus.keadaan_akhir,  
								tetanus.tanggal_mulai_sakit, 
								tetanus.klasifikasi_akhir, 
								tetanus.tanggal_periksa
							FROM
								tetanus,
								pasien,
								lokasi,
								pasien_terserang_tetanus,
								users
							WHERE
								pasien.id_pasien=pasien_terserang_tetanus.id_pasien
							AND 
								lokasi.id_kelurahan=pasien.id_kelurahan
							AND 
								tetanus.id_tetanus=pasien_terserang_tetanus.id_tetanus
							AND
								users.email ='".Sentry::getUser()->email."'
							AND
								tetanus.no_epid ='".$id."'");
		return View::make('pemeriksaan.tetanus.detail',compact('tetanus'));
	}

	//get edit tetanus
	public function getEditTetanus($id) {

		$tetanus = DB::select("SELECT 
								pasien.nik,
								pasien.tanggal_lahir, 
								pasien.nama_anak, 
								pasien.nama_ortu, 
								pasien.alamat, 
								pasien.umur,
								pasien.umur_bln,
								pasien.umur_hr,
								lokasi.provinsi,
								lokasi.kabupaten,
								lokasi.kecamatan,
								lokasi.kelurahan,
								pasien.id_kelurahan, 
								pasien.jenis_kelamin,
								tetanus.no_epid,  
								tetanus.keadaan_akhir,
								tetanus.tanggal_mulai_sakit,  
								tetanus.klasifikasi_akhir,
								pasien_terserang_tetanus.id_pasien_tetanus,
								tetanus.tanggal_laporan_diterima,
								tetanus.tanggal_pelacakan,
								tetanus.ANC,
								tetanus.status_imunisasi,
								tetanus.pregnancy_helper,
								tetanus.penolong_persalinan,
								tetanus.perawatan_tali_pusar,
								tetanus.pemotongan_tali_pusar,
								tetanus.rawat_rumah_sakit
							FROM
								tetanus,
								pasien,
								lokasi,
								pasien_terserang_tetanus,
								users
							WHERE
								pasien.id_pasien=pasien_terserang_tetanus.id_pasien
							AND 
								lokasi.id_kelurahan=pasien.id_kelurahan
							AND 
								tetanus.id_tetanus=pasien_terserang_tetanus.id_tetanus
							AND
								users.email ='".Sentry::getUser()->email."'
							AND
								tetanus.no_epid ='".$id."'");

		return View::make('pemeriksaan.tetanus.edit',compact('tetanus'));

	}

	public function getHapus($id,$id_lab) {

		try {

			DB::beginTransaction();
			DB::delete("delete from pasien_terserang_tetanus where pasien_terserang_tetanus.id_pasien_tetanus='".$id_lab."'");
			DB::delete("delete from tetanus where tetanus.no_epid ='".$id."'");
			DB::commit();
		} catch (Exception $e) {
			
			DB::rollback();

		}
		return Redirect::to('tetanus');
		
	}

	//post edit tetanus
	public function updateTetanus() {

		$tetanus = DB::update(" update
								pasien,tetanus,pasien_terserang_tetanus
						    set
								pasien.nik='".Input::get('nik')."',
								pasien.tanggal_lahir='".Input::get('tanggal_lahir')."', 
								pasien.nama_anak='".Input::get('nama_anak')."', 
								pasien.nama_ortu='".Input::get('nama_ortu')."', 
								pasien.alamat='".Input::get('alamat')."', 
								pasien.umur='".Input::get('tgl_tahun')."',
								pasien.umur_bln='".Input::get('tgl_bulan')."',
								pasien.umur_hr='".Input::get('tgl_hari')."',
								pasien.id_kelurahan='".Input::get('id_kelurahan')."', 
								pasien.jenis_kelamin='".Input::get('jenis_kelamin')."',
								tetanus.no_epid='".Input::get('no_epid')."',  
								tetanus.keadaan_akhir='".Input::get('keadaan_akhir')."', 
								tetanus.imunisasi_TT_ibu_saat_hamil='".Input::get('imunisasi_TT_ibu_saat_hamil')."', 
								tetanus.status_TT_ibu_saat_hamil='".Input::get('status_TT_ibu_saat_hamil')."', 
								tetanus.riwayat_persalinan='".Input::get('riwayat_persalinan')."', 
								tetanus.tanggal_mulai_sakit='".Input::get('tanggal_mulai_sakit')."', 
								tetanus.bayi_lahir_menangis='".Input::get('bayi_lahir_menangis')."', 
								tetanus.tanda_kehidupan_lain='".Input::get('tanda_kehidupan_lain')."', 
								tetanus.mulut_mecucu='".Input::get('mulut_mecucu')."', 
								tetanus.bayi_kejang='".Input::get('bayi_kejang')."', 
								tetanus.dirawat='".Input::get('dirawat')."', 
								tetanus.tempat_dirawat='".Input::get('tempat_dirawat')."', 
								tetanus.klasifikasi_akhir='".Input::get('klasifikasi_akhir')."',
								tetanus.tanggal_laporan_diterima='".Input::get('tanggal_laporan_diterima')."',
								tetanus.tanggal_pelacakan='".Input::get('tanggal_pelacakan')."',
								tetanus.ANC='".Input::get('ANC')."',
								tetanus.status_imunisasi='".Input::get('status_imunisasi')."',
								tetanus.pregnancy_helper='".Input::get('pregnancy_helper')."',
								tetanus.penolong_persalinan='".Input::get('penolong_persalinan')."',
								tetanus.perawatan_tali_pusar='".Input::get('perawatan_tali_pusar')."',
								tetanus.pemotongan_tali_pusar='".Input::get('pemotongan_tali_pusar')."',
								tetanus.rawat_rumah_sakit='".Input::get('rawat_rumah_sakit')."'
							where 
								pasien.id_pasien=pasien_terserang_tetanus.id_pasien
							and 
								tetanus.id_tetanus=pasien_terserang_tetanus.id_tetanus
							and 
								pasien_terserang_tetanus.id_pasien_tetanus='".Input::get('id_pasien_tetanus')."'");
			return Redirect::to('tetanus');

	}

	//route daftar tetanus
	public function getDaftarTetanus() {

		$pe_tetanus = Tetanus::getPeTetanus();
		return View::make('pemeriksaan.tetanus.daftar',compact('pe_tetanus'));
	}

	//fungsi untuk mengambil data pasien yang akan dilakukan penyelidikan epidemologi tetanus
	public function getEntri_tetanus($no_epid) {
		
		$pe_tetanus = DB::select("SELECT 
								tetanus.no_epid,
								pasien.nik,
								pasien.id_pasien,
								pasien.tanggal_lahir, 
								pasien.nama_anak, 
								pasien.nama_ortu, 
								pasien.alamat,
								lokasi.kelurahan,
								lokasi.kecamatan,
								lokasi.kabupaten,
								lokasi.provinsi,
								lokasi.id_kelurahan, 
								pasien.umur, 
								pasien.umur_bln,
								pasien.umur_hr,
								pasien.jenis_kelamin
							FROM
								tetanus,
								pasien,
								lokasi,
								pasien_terserang_tetanus,
								users
							WHERE
								pasien.id_pasien=pasien_terserang_tetanus.id_pasien
							AND 
								lokasi.id_kelurahan=pasien.id_kelurahan
							AND 
								tetanus.id_tetanus=pasien_terserang_tetanus.id_tetanus
							AND
								users.email ='".Sentry::getUser()->email."'
							AND
								tetanus.no_epid ='".$no_epid."'");

		return View::make('pemeriksaan.tetanus.pe_tetanus',compact('pe_tetanus'));
	}
	//fungsi untuk menyimpan data kasus penyelidikan epidemologi tetanus
	public function postEpidTetanus() {

		$no_epid = Input::get('no_epid');
		$cek=DB::select("select no_epid from tetanus,pasien,pasien_terserang_tetanus where tetanus.id_tetanus=pasien_terserang_tetanus.id_tetanus and pasien.id_pasien=pasien_terserang_tetanus.id_pasien and tetanus.no_epid='".$no_epid."'");
		if(empty($cek)) {

			//simpan data personal
			$data = 
			array(
			DB::table('tetanus')->insert(
				array('no_epid' => Input::get('no_epid')))
			,
			DB::table('pasien')->insert(
				array(
					'nama_anak'=>Input::get('nama_anak'),
					'alamat'=>Input::get('alamat'),
					'tanggal_lahir'=>Input::get('tanggal_lahir'),
					'umur'=>Input::get('tgl_tahun'),
					'umur_bln'=>Input::get('tgl_bulan'),
					'umur_hr'=>Input::get('tgl_hari'),
					'jenis_kelamin'=>Input::get('jenis_kelamin'),
					'id_kelurahan'=>Input::get('id_kelurahan')
				)));
			if($data) {
				$no_epid =$no_epid;
				$id_pasien=DB::table('pasien')->max('id_pasien');
				$sumber_laporan=Input::get('sumber_laporan');
				$tanggal_laporan_diterima=Input::get('tanggal_laporan_diterima');
				$tanggal_pelacakan=Input::get('tanggal_pelacakan');
				$anak_ke=Input::get('anak_ke');
				$nama_ayah=Input::get('nama_ayah');
				$umur_ayah=Input::get('umur_ayah');
				$th_pendidikan_ayah=Input::get('th_pendidikan_ayah');
				$pekerjaan_ayah=Input::get('pekerjaan_ayah');
				$nama_ibu=Input::get('nama_ibu');
				$th_pendidikan_ibu=Input::get('th_pendidikan_ibu');
				$pekerjaan_ibu=Input::get('pekerjaan_ibu');
				$nama_yang_diwawancarai=Input::get('nama_yang_diwawancarai');
				$hubungan_keluarga=Input::get('hubungan_keluarga');
				$bayi_hidup=Input::get('bayi_hidup');
				$tanggal_lahir=Input::get('tgl_lahir');
				$tanggal_mulai_sakit=Input::get('tanggal_mulai_sakit');
				$bayi_meninggal=Input::get('bayi_meninggal');
				$tanggal_meninggal=Input::get('tanggal_meninggal');
				$umur=Input::get('umur');
				$bayi_menangis=Input::get('bayi_menangis');
				$tanda_kehidupan_lain=Input::get('tanda_kehidupan_lain');
				$bayi_menetek=Input::get('bayi_menetek');
				$bayi_mencucu=Input::get('bayi_mencucu');
				$bayi_kejang=Input::get('bayi_kejang');
				$dirawat=Input::get('dirawat');
				$tempat_berobat=Input::get('tempat_berobat');
				$tanggal_dirawat=Input::get('tanggal_dirawat');
				$keadaan_setelah_dirawat=Input::get('keadaan_setelah_dirawat');
				$nama_pemeriksa=Input::get('nama_pemeriksa');
				$alamat=Input::get('alamat');
				$info_imunisasi=Input::get('info_imunisasi');
				$frekuensi=Input::get('frekuensi');
				$mendapat_imunisasi=Input::get('mendapat_imunisasi');
				$jumlah_imunisasi=Input::get('jumlah_imunisasi');
				$umur_kehamilan_pertama=Input::get('umur_kehamilan_pertama');
				$tanggal_imunisasi_pertama=Input::get('tanggal_imunisasi_pertama');
				$umur_kehamilan_kedua=Input::get('umur_kehamilan_kedua');
				$tanggal_imunisasi_kedua=Input::get('tanggal_imunisasi_kedua');
				$ibu_mendapat_imunisasi=Input::get('ibu_mendapat_imunisasi');
				$tahun_suntikan_pertama=Input::get('tahun_suntikan_pertama');
				$tahun_suntikan_kedua=Input::get('tahun_suntikan_kedua');
				$ibu_mendapat_suntikan_pengantin=Input::get('ibu_mendapat_suntikan_pengantin');
				$tahun_suntikan_pertama_pengantin=Input::get('tahun_suntikan_pertama_pengantin');
				$tahun_suntikan_kedua_pengantin=Input::get('tahun_suntikan_kedua_pengantin');
				$pemotong_tali_pusar=Input::get('pemotong_tali_pusar');
				$alat_lain=Input::get('alat_lain');
				$obat_penyembuh=Input::get('obat_penyembuh');
				$ramuan_lainnya=Input::get('ramuan_lainnya');
				$perawatan_tali_pusar=Input::get('perawatan_tali_pusar');
				$obat_merawat_tali_pusar=Input::get('obat_merawat_tali_pusar');
				$kesimpulan_diagnosis=Input::get('kesimpulan_diagnosis');
				$TT1=Input::get('TT1');
				$TT2=Input::get('TT2');
				$TT3=Input::get('TT3');
				$TT4=Input::get('TT4');
				$TT5=Input::get('TT5');
				$cakupan_tenaga_kesehatan=Input::get('cakupan_tenaga_kesehatan');
				$KN1=Input::get('KN1');
				$KN2=Input::get('KN2');
				
				$save= Tetanus::epid_tetanus($id_pasien,$sumber_laporan,$tanggal_laporan_diterima,$tanggal_pelacakan,$anak_ke,$nama_ayah,$umur_ayah,$th_pendidikan_ayah,$pekerjaan_ayah,$nama_ibu,$th_pendidikan_ibu,$pekerjaan_ibu,$nama_yang_diwawancarai,$hubungan_keluarga,$bayi_hidup,$tanggal_lahir,$tanggal_mulai_sakit,$bayi_meninggal,$tanggal_meninggal,$umur,$bayi_menangis,$tanda_kehidupan_lain,$bayi_menetek,$bayi_mencucu,$bayi_kejang,$dirawat,$tempat_berobat,$tanggal_dirawat,$keadaan_setelah_dirawat,$nama_pemeriksa,$alamat,$info_imunisasi,$frekuensi,$mendapat_imunisasi,$jumlah_imunisasi,$umur_kehamilan_pertama,$tanggal_imunisasi_pertama,$umur_kehamilan_kedua,$tanggal_imunisasi_kedua,$ibu_mendapat_imunisasi,$tahun_suntikan_pertama,$tahun_suntikan_kedua,$ibu_mendapat_suntikan_pengantin,$tahun_suntikan_pertama_pengantin,$tahun_suntikan_kedua_pengantin,$pemotong_tali_pusar,$alat_lain,$obat_penyembuh,$ramuan_lainnya,$perawatan_tali_pusar,$obat_merawat_tali_pusar,$kesimpulan_diagnosis,$TT1,$TT2,$TT3,$TT4,$TT5,$cakupan_tenaga_kesehatan,$KN1,$KN2); 
				
				$cari = DB::select("select id_tetanus from tetanus where no_epid='".Input::get('no_epid')."'");
				foreach ($cari as $value) {
					$id_tetanus =$value->id_tetanus;
				}
				DB::table('pasien_terserang_tetanus')->insert(
				array(
					'id_tetanus'=> $id_tetanus,
					'id_pasien'=>DB::table('pasien')->max('id_pasien')
				)); 
			
				return Redirect::to('tetanus');

			}
		}
		$id_pasien=Input::get('id_pasien');
		$no_epid=Input::get('no_epid');
		$sumber_laporan=Input::get('sumber_laporan');
		$tanggal_laporan_diterima=Input::get('tanggal_laporan_diterima');
		$tanggal_pelacakan=Input::get('tanggal_pelacakan');
		$anak_ke=Input::get('anak_ke');
		$nama_ayah=Input::get('nama_ayah');
		$umur_ayah=Input::get('umur_ayah');
		$th_pendidikan_ayah=Input::get('th_pendidikan_ayah');
		$pekerjaan_ayah=Input::get('pekerjaan_ayah');
		$nama_ibu=Input::get('nama_ibu');
		$th_pendidikan_ibu=Input::get('th_pendidikan_ibu');
		$pekerjaan_ibu=Input::get('pekerjaan_ibu');
		$nama_yang_diwawancarai=Input::get('nama_yang_diwawancarai');
		$hubungan_keluarga=Input::get('hubungan_keluarga');
		$bayi_hidup=Input::get('bayi_hidup');
		$tanggal_lahir=Input::get('tgl_lahir');
		$tanggal_mulai_sakit=Input::get('tanggal_mulai_sakit');
		$bayi_meninggal=Input::get('bayi_meninggal');
		$tanggal_meninggal=Input::get('tanggal_meninggal');
		$umur=Input::get('umur');
		$bayi_menangis=Input::get('bayi_menangis');
		$tanda_kehidupan_lain=Input::get('tanda_kehidupan_lain');
		$bayi_menetek=Input::get('bayi_menetek');
		$bayi_mencucu=Input::get('bayi_mencucu');
		$bayi_kejang=Input::get('bayi_kejang');
		$dirawat=Input::get('dirawat');
		$tempat_berobat=Input::get('tempat_berobat');
		$tanggal_dirawat=Input::get('tanggal_dirawat');
		$keadaan_setelah_dirawat=Input::get('keadaan_setelah_dirawat');
		$nama_pemeriksa=Input::get('nama_pemeriksa');
		$alamat=Input::get('alamat');
		$info_imunisasi=Input::get('info_imunisasi');
		$frekuensi=Input::get('frekuensi');
		$mendapat_imunisasi=Input::get('mendapat_imunisasi');
		$jumlah_imunisasi=Input::get('jumlah_imunisasi');
		$umur_kehamilan_pertama=Input::get('umur_kehamilan_pertama');
		$tanggal_imunisasi_pertama=Input::get('tanggal_imunisasi_pertama');
		$umur_kehamilan_kedua=Input::get('umur_kehamilan_kedua');
		$tanggal_imunisasi_kedua=Input::get('tanggal_imunisasi_kedua');
		$ibu_mendapat_imunisasi=Input::get('ibu_mendapat_imunisasi');
		$tahun_suntikan_pertama=Input::get('tahun_suntikan_pertama');
		$tahun_suntikan_kedua=Input::get('tahun_suntikan_kedua');
		$ibu_mendapat_suntikan_pengantin=Input::get('ibu_mendapat_suntikan_pengantin');
		$tahun_suntikan_pertama_pengantin=Input::get('tahun_suntikan_pertama_pengantin');
		$tahun_suntikan_kedua_pengantin=Input::get('tahun_suntikan_kedua_pengantin');
		$pemotong_tali_pusar=Input::get('pemotong_tali_pusar');
		$alat_lain=Input::get('alat_lain');
		$obat_penyembuh=Input::get('obat_penyembuh');
		$ramuan_lainnya=Input::get('ramuan_lainnya');
		$perawatan_tali_pusar=Input::get('perawatan_tali_pusar');
		$obat_merawat_tali_pusar=Input::get('obat_merawat_tali_pusar');
		$kesimpulan_diagnosis=Input::get('kesimpulan_diagnosis');
		$TT1=Input::get('TT1');
		$TT2=Input::get('TT2');
		$TT3=Input::get('TT3');
		$TT4=Input::get('TT4');
		$TT5=Input::get('TT5');
		$cakupan_tenaga_kesehatan=Input::get('cakupan_tenaga_kesehatan');
		$KN1=Input::get('KN1');
		$KN2=Input::get('KN2');
		
		$save= Tetanus::epid_tetanus($id_pasien,$no_epid,$sumber_laporan,$tanggal_laporan_diterima,$tanggal_pelacakan,$anak_ke,$nama_ayah,$umur_ayah,$th_pendidikan_ayah,$pekerjaan_ayah,$nama_ibu,$th_pendidikan_ibu,$pekerjaan_ibu,$nama_yang_diwawancarai,$hubungan_keluarga,$bayi_hidup,$tanggal_lahir,$tanggal_mulai_sakit,$bayi_meninggal,$tanggal_meninggal,$umur,$bayi_menangis,$tanda_kehidupan_lain,$bayi_menetek,$bayi_mencucu,$bayi_kejang,$dirawat,$tempat_berobat,$tanggal_dirawat,$keadaan_setelah_dirawat,$nama_pemeriksa,$alamat,$info_imunisasi,$frekuensi,$mendapat_imunisasi,$jumlah_imunisasi,$umur_kehamilan_pertama,$tanggal_imunisasi_pertama,$umur_kehamilan_kedua,$tanggal_imunisasi_kedua,$ibu_mendapat_imunisasi,$tahun_suntikan_pertama,$tahun_suntikan_kedua,$ibu_mendapat_suntikan_pengantin,$tahun_suntikan_pertama_pengantin,$tahun_suntikan_kedua_pengantin,$pemotong_tali_pusar,$alat_lain,$obat_penyembuh,$ramuan_lainnya,$perawatan_tali_pusar,$obat_merawat_tali_pusar,$kesimpulan_diagnosis,$TT1,$TT2,$TT3,$TT4,$TT5,$cakupan_tenaga_kesehatan,$KN1,$KN2); 
		

		return Redirect::to('tetanus');
	}

	public function getDetailPeTetanus($no_epid) {

		$data = DB::select("select 
								pasien.nama_anak, pasien.alamat, pasien.tanggal_lahir, 
								pasien.umur_hr, pasien.jenis_kelamin,
								pasien.id_kelurahan, 
								pe_tetanus.sumber_laporan, pe_tetanus.tanggal_laporan_diterima, pe_tetanus.tanggal_pelacakan, 
								pe_tetanus.anak_ke, pe_tetanus.nama_ayah, pe_tetanus.th_pendidikan_ayah, pe_tetanus.umur_ayah, 
								pe_tetanus.pekerjaan_ayah, pe_tetanus.nama_ibu, pe_tetanus.no_epid, pe_tetanus.th_pendidikan_ibu, 
								pe_tetanus.pekerjaan_ibu, pe_tetanus.nama_yang_diwawancarai, pe_tetanus.hubungan_keluarga, 
								pe_tetanus.bayi_hidup, pe_tetanus.tanggal_lahir as tgl_lahir_bayi, pe_tetanus.tanggal_mulai_sakit, 
								pe_tetanus.bayi_meninggal, pe_tetanus.tanggal_meninggal, pe_tetanus.umur as umur_mati, pe_tetanus.bayi_menangis, 
								pe_tetanus.tanda_kehidupan_lain, pe_tetanus.bayi_menetek, pe_tetanus.bayi_mencucu as bayi_mecucu, 
								pe_tetanus.dirawat, pe_tetanus.bayi_kejang, pe_tetanus.tempat_berobat, pe_tetanus.tanggal_dirawat, 
								pe_tetanus.keadaan_setelah_dirawat, pe_tetanus.nama_pemeriksa, pe_tetanus.alamat as alamat_periksa, 
								pe_tetanus.info_imunisasi, pe_tetanus.frekuensi, pe_tetanus.mendapat_imunisasi, 
								pe_tetanus.jumlah_imunisasi, pe_tetanus.umur_kehamilan_pertama, pe_tetanus.tanggal_imunisasi_pertama, 
								pe_tetanus.umur_kehamilan_kedua, pe_tetanus.tanggal_imunisasi_kedua, pe_tetanus.ibu_mendapat_imunisasi, 
								pe_tetanus.tahun_suntikan_pertama, pe_tetanus.tahun_suntikan_kedua, 
								pe_tetanus.ibu_mendapat_suntikan_pengantin, 
								pe_tetanus.tahun_suntikan_pertama_pengantin, pe_tetanus.tahun_suntikan_kedua_pengantin, 
								pe_tetanus.status_ibu_saat_hamil, pe_tetanus.pemotong_tali_pusar, 
								pe_tetanus.alat_lain, pe_tetanus.obat_penyembuh, pe_tetanus.ramuan_lainnya, 
								pe_tetanus.perawatan_tali_pusar, pe_tetanus.obat_merawat_tali_pusar, 
								pe_tetanus.kesimpulan_diagnosis, pe_tetanus.TT1, pe_tetanus.TT2, pe_tetanus.TT3, pe_tetanus.TT4, 
								pe_tetanus.TT5, pe_tetanus.cakupan_tenaga_kesehatan, pe_tetanus.KN1, pe_tetanus.KN2 
							from 
								pasien,pe_tetanus,lokasi 
							where 
								pasien.id_pasien=pe_tetanus.id_pasien 
							and 
								lokasi.id_kelurahan=pasien.id_kelurahan 
							and 
								pe_tetanus.no_epid='".$no_epid."' 
							order by 
								created_at 
							asc");
		return View::make('pemeriksaan.tetanus.detail_pe_tetanus',compact('data'));
	}

	public function getEditPeTetanus($id) {

		$tetanus = Tetanus::getEditPeTetanus($id);
		return View::make('pemeriksaan.tetanus.edit_pe_tetanus',compact('tetanus'));
	}

	public function postEditPeTetanus() {

		DB::update("update
						pasien,
						pe_tetanus
					set
					 	pasien.nik='".Input::get('nik')."', 
					 	pasien.nama_anak='".Input::get('nama_anak')."', 
					 	pasien.nama_ortu='".Input::get('nama_ortu')."', 
					 	pasien.alamat='".Input::get('alamat')."', 
					 	pasien.tanggal_lahir='".Input::get('tanggal_lahir')."', 
					 	pasien.umur_hr='".Input::get('tgl_hari')."', 
					 	pasien.jenis_kelamin='".Input::get('jenis_kelamin')."', 
					 	pasien.id_kelurahan='".Input::get('id_kelurahan')."', 
					 	pe_tetanus.sumber_laporan='".Input::get('sumber_laporan')."', 
					 	pe_tetanus.tanggal_laporan_diterima='".Input::get('tanggal_laporan_diterima')."', 
					 	pe_tetanus.tanggal_pelacakan='".Input::get('tanggal_pelacakan')."', 
					 	pe_tetanus.anak_ke='".Input::get('anak_ke')."', 
					 	pe_tetanus.nama_ayah='".Input::get('nama_ayah')."', 
					 	pe_tetanus.th_pendidikan_ayah='".Input::get('th_pendidikan_ayah')."', 
					 	pe_tetanus.umur_ayah='".Input::get('umur_ayah')."', 
					 	pe_tetanus.pekerjaan_ayah='".Input::get('pekerjaan_ayah')."', 
					 	pe_tetanus.nama_ibu='".Input::get('nama_ibu')."', 
					 	pe_tetanus.no_epid='".Input::get('no_epid')."', 
					 	pe_tetanus.th_pendidikan_ibu='".Input::get('th_pendidikan_ibu')."', 
					 	pe_tetanus.pekerjaan_ibu='".Input::get('pekerjaan_ibu')."', 
					 	pe_tetanus.nama_yang_diwawancarai='".Input::get('nama_yang_diwawancarai')."', 
					 	pe_tetanus.hubungan_keluarga='".Input::get('hubungan_keluarga')."', 
					 	pe_tetanus.bayi_hidup='".Input::get('bayi_hidup')."', 
					 	pe_tetanus.tanggal_lahir='".Input::get('tgl_lahir')."', 
					 	pe_tetanus.tanggal_mulai_sakit='".Input::get('tanggal_mulai_sakit')."', 
					 	pe_tetanus.bayi_meninggal='".Input::get('bayi_meninggal')."', 
					 	pe_tetanus.tanggal_meninggal='".Input::get('tanggal_meninggal')."', 
					 	pe_tetanus.umur='".Input::get('umur')."', 
					 	pe_tetanus.bayi_menangis='".Input::get('bayi_menangis')."', 
					 	pe_tetanus.tanda_kehidupan_lain='".Input::get('tanda_kehidupan_lain')."', 
					 	pe_tetanus.bayi_menetek='".Input::get('bayi_menetek')."', 
					 	pe_tetanus.bayi_mencucu='".Input::get('bayi_mencucu')."', 
					 	pe_tetanus.dirawat='".Input::get('dirawat')."', 
					 	pe_tetanus.bayi_kejang='".Input::get('bayi_kejang')."', 
					 	pe_tetanus.tempat_berobat='".Input::get('tempat_berobat')."', 
					 	pe_tetanus.tanggal_dirawat='".Input::get('tanggal_dirawat')."', 
					 	pe_tetanus.keadaan_setelah_dirawat='".Input::get('keadaan_setelah_dirawat')."', 
					 	pe_tetanus.nama_pemeriksa='".Input::get('nama_pemeriksa')."', 
					 	pe_tetanus.alamat='".Input::get('alamat_pemeriksa')."', 
					 	pe_tetanus.info_imunisasi='".Input::get('info_imunisasi')."', 
					 	pe_tetanus.frekuensi='".Input::get('frekuensi')."', 
					 	pe_tetanus.mendapat_imunisasi='".Input::get('mendapat_imunisasi')."', 
					 	pe_tetanus.jumlah_imunisasi='".Input::get('jumlah_imunisasi')."', 
					 	pe_tetanus.umur_kehamilan_pertama='".Input::get('umur_kehamilan_pertama')."', 
					 	pe_tetanus.tanggal_imunisasi_pertama='".Input::get('tanggal_imunisasi_pertama')."', 
					 	pe_tetanus.umur_kehamilan_kedua='".Input::get('umur_kehamilan_kedua')."', 
					 	pe_tetanus.tanggal_imunisasi_kedua='".Input::get('tanggal_imunisasi_kedua')."', 
					 	pe_tetanus.ibu_mendapat_imunisasi='".Input::get('ibu_mendapat_imunisasi')."', 
					 	pe_tetanus.tahun_suntikan_pertama='".Input::get('tahun_suntikan_pertama')."', 
					 	pe_tetanus.tahun_suntikan_kedua='".Input::get('tahun_suntikan_kedua')."', 
					 	pe_tetanus.ibu_mendapat_suntikan_pengantin='".Input::get('ibu_mendapat_suntikan_pengantin')."', 
					 	pe_tetanus.tahun_suntikan_pertama_pengantin='".Input::get('tahun_suntikan_pertama_pengantin')."', 
					 	pe_tetanus.tahun_suntikan_kedua_pengantin='".Input::get('tahun_suntikan_kedua_pengantin')."', 
					 	pe_tetanus.pemotong_tali_pusar='".Input::get('pemotong_tali_pusar')."', 
					 	pe_tetanus.alat_lain='".Input::get('alat_lain')."', 
					 	pe_tetanus.obat_penyembuh='".Input::get('obat_penyembuh')."', 
					 	pe_tetanus.ramuan_lainnya='".Input::get('ramuan_lainnya')."', 
					 	pe_tetanus.perawatan_tali_pusar='".Input::get('perawatan_tali_pusar')."', 
					 	pe_tetanus.obat_merawat_tali_pusar='".Input::get('obat_merawat_tali_pusar')."', 
					 	pe_tetanus.kesimpulan_diagnosis='".Input::get('kesimpulan_diagnosis')."', 
					 	pe_tetanus.TT1='".Input::get('TT1')."', 
					 	pe_tetanus.TT2='".Input::get('TT2')."', 
					 	pe_tetanus.TT3='".Input::get('TT3')."', 
					 	pe_tetanus.TT4='".Input::get('TT4')."', 
					 	pe_tetanus.TT5='".Input::get('TT5')."', 
					 	pe_tetanus.cakupan_tenaga_kesehatan='".Input::get('cakupan_tenaga_kesehatan')."', 
					 	pe_tetanus.KN1='".Input::get('KN1')."', 
					 	pe_tetanus.KN2='".Input::get('KN2')."',
					 	pe_tetanus.status_ibu_saat_hamil='".Input::get('status_ibu_saat_hamil')."' 
					where 
						pasien.id_pasien=pe_tetanus.id_pasien 
					and 
						pe_tetanus.id='".Input::get('id')."'");

		return Redirect::to('tetanus');
	}

	//fungsi untuk mendapatkan no epid tetanus secara otomatis
	public function getEpid() {
		$id_kel = Input::get('id_kelurahan');
		$data2=DB::select("select no_epid from tetanus,pasien,pasien_terserang_tetanus where pasien.id_pasien=pasien_terserang_tetanus.id_pasien and tetanus.id_tetanus=pasien_terserang_tetanus.id_tetanus and id_kelurahan='".$id_kel."' order by tetanus.id_tetanus limit 0,1");
        $cek = DB::select("select RIGHT(no_epid,3) from tetanus,pasien,pasien_terserang_tetanus where MID(no_epid,10,2) ='".date('y')."' and pasien.id_pasien=pasien_terserang_tetanus.id_pasien and tetanus.id_tetanus=pasien_terserang_tetanus.id_tetanus and pasien.id_kelurahan='".$id_kel."' order by tetanus.id_tetanus limit 0,1");
        if(empty($cek)) {
            $code=substr($id_kel,0,7).date('y');
            $caracter = 'TN';
            $epid = '001';
           	$epid =$caracter.$code.$epid;
        }
        else {
            foreach ($data2 as $result) {
                $kode = substr($result->no_epid,0,9).date('y');
                $noUrut = (int) substr($result->no_epid,11,3);
                $noUrut++;
                $epid = sprintf("%03s", $noUrut);
            }
            $epid=$kode.$epid;  
        }

        return $epid;
	}

	//fungsi menghapus data pe tetanus
	public function getHapusPeTetanus($no_epid) {

		try {

			DB::beginTransaction();
			DB::delete("delete from pe_tetanus where pe_tetanus.no_epid='".$no_epid."'");
			DB::commit();
		} catch (Exception $e) {
			
			DB::rollback();

		}
		return Redirect::to('tetanus');
	}

}
