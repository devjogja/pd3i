<?php

class TetanusController extends \BaseController {

    public function __construct() {
        $this->beforeFilter('auth');
    }

    /**
     * Display a listing of tetanus
     *
     * @return Response
     */
    public function index() {
        Session::put('sess_id', '');
        Session::put('penyakit', 'Tetanus');
        Session::put('sess_penyakit', 'tetanus');
        return View::make('pemeriksaan.tetanus.index', compact('tetanus'));
    }

    /**
     * Show the form for creating a new tetanus
     *
     * @return Response
     */
    public function create() {
        return View::make('pemeriksaan.tetanus.index');
    }

    /**
     * Store a newly created tetanus in storage.
     *
     * @return Response
     */
    public function store() {
        //lakukan transaksi
        DB::transaction(function () {
            $type = Session::get('type');
            $kd_faskes = Session::get('kd_faskes');
            $faskes_id = '';
            if (Sentry::getUser()->hak_akses == 2) {
                $q = "
                        SELECT puskesmas_id
                        FROM puskesmas
                        WHERE puskesmas_code_faskes='" . $kd_faskes . "'
                     ";
                $data = DB::select($q);
                $faskes_id = $data[0]->puskesmas_id;
            } else if (Sentry::getUser()->hak_akses == 7) {
                $faskes_id = Sentry::getUser()->id_user;
            }

            $id_pasien = DB::table('pasien')->insertGetId(array(
                'nik'           => Input::get('nik'),
                'nama_anak'     => Input::get('nama_anak'),
                'nama_ortu'     => Input::get('nama_ortu'),
                'alamat'        => Input::get('alamat'),
                'tanggal_lahir' => $this->encode_date(Input::get('tanggal_lahir')),
                'umur'          => Input::get('tgl_tahun'),
                'umur_bln'      => Input::get('tgl_bulan'),
                'umur_hr'       => Input::get('tgl_hari'),
                'jenis_kelamin' => Input::get('jenis_kelamin'),
                'id_kelurahan'  => Input::get('id_kelurahan'),
            ));

            //simpan data diagnosa penyakit tetanus
            $id_tetanus = DB::table('tetanus')->insertGetId(array(
                'no_epid'                  => Input::get('no_epid'),
                'no_epid_lama'             => Input::get('no_epid_lama'),
                'created_by'               => Sentry::getUser()->id,
                'nama_puskesmas'           => Input::get('nama_puskesmas'),
                'tanggal_laporan_diterima' => $this->encode_date(Input::get('tanggal_laporan_diterima')),
                'tanggal_pelacakan'        => $this->encode_date(Input::get('tanggal_pelacakan')),
                'ANC'                      => Input::get('ANC'),
                'status_imunisasi'         => Input::get('status_imunisasi'),
                'penolong_persalinan'      => Input::get('penolong_persalinan'),
                'pemotongan_tali_pusat'    => Input::get('pemotongan_tali_pusat'),
                'rawat_rumah_sakit'        => Input::get('rawat_rumah_sakit'),
                'tanggal_mulai_sakit'      => $this->encode_date(Input::get('tanggal_mulai_sakit')),
                'keadaan_akhir'            => Input::get('keadaan_akhir'),
                'klasifikasi_akhir'        => Input::get('klasifikasi_akhir'),
                'tanggal_periksa'          => date('Y-m-d'),
                'id_tempat_periksa'        => $faskes_id,
                'kode_faskes'              => $type,
                'created_at'               => date('Y-m-d H:i:s'),
                'updated_at'               => date('Y-m-d H:i:s'),
            ));

            //simpan kedalam table pasien_terserang_penyakit
            DB::table('pasien_terserang_tetanus')->insert(array(
                'id_pasien'  => $id_pasien,
                'id_tetanus' => $id_tetanus,
            ));

            //notifikasi

            if ($type == 'rs') {
                $data = DB::SELECT("
                        SELECT
                        d.id_kecamatan AS id_dis_pasien,
                        e.kode_faskes AS id_dis_faskes
                        FROM pasien_terserang_tetanus AS a
                        JOIN pasien AS b ON a.id_pasien=b.id_pasien
                        JOIN tetanus AS c ON a.id_tetanus=c.id_tetanus
                        LEFT JOIN located AS d ON b.id_kelurahan=d.id_kelurahan
                        LEFT JOIN rumahsakit2 AS e ON c.id_tempat_periksa=e.id
                        LEFT JOIN puskesmas AS f ON c.id_tempat_periksa=f.puskesmas_id
                        WHERE c.id_tetanus=$id_tetanus
                    ");
            } else {
                $data = DB::SELECT("
                        SELECT
                        d.id_kecamatan AS id_dis_pasien,
                        f.puskesmas_code_faskes AS id_dis_faskes
                        FROM pasien_terserang_tetanus AS a
                        JOIN pasien AS b ON a.id_pasien=b.id_pasien
                        JOIN tetanus AS c ON a.id_tetanus=c.id_tetanus
                        LEFT JOIN located AS d ON b.id_kelurahan=d.id_kelurahan
                        LEFT JOIN rumahsakit2 AS e ON c.id_tempat_periksa=e.id
                        LEFT JOIN puskesmas AS f ON c.id_tempat_periksa=f.puskesmas_id
                        WHERE c.id_tetanus=$id_tetanus");
            }
            $id_kec_pasien = (empty($data) ? '' : $data[0]->id_dis_pasien);
            $id_kec_faskes = (empty($data) ? '' : $data[0]->id_dis_faskes);
            if ($id_kec_pasien != $id_kec_faskes) {
                DB::table('notification')->insert(array(
                    'from_faskes_id'     => $faskes_id,
                    'from_faskes_kec_id' => $id_kec_faskes,
                    'to_faskes_kec_id'   => $id_kec_pasien,
                    'type_id'            => 'tetanus',
                    'global_id'          => $id_tetanus,
                    'description'        => '',
                    'submit_dttm'        => date('Y-m-d H:i:s'),
                ));
            }
        });

        return Redirect::to('tetanus');
    }

    /**
     * Display the specified tetanus.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $tetanus = tetanus::findOrFail($id);
        return View::make('pemeriksaan.tetanus.show', compact('tetanus'));
    }

    /**
     * Show the form for editing the specified tetanus.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $tetanus = tetanus::find($id);
        return View::make('pemeriksaan.tetanus.edit', compact('tetanus'));
    }

    /**
     * Update the specified tetanus in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $tetanus = tetanus::findOrFail($id);

        $validator = Validator::make($data = Input::all(), tetanus::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $tetanus->update($data);

        return Redirect::route('pemeriksaan.tetanus.index');
    }

    /**
     * Remove the specified tetanus from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        tetanus::destroy($id);
        return Redirect::route('pemeriksaan.tetanus.index');
    }

    public function postForm() {
        $id = Input::get('id');

        if ($id == 1) {
            return View::make('diagnosa.tetanus');
        }
    }

    public function detailtetanus($id) {

        $tetanus = DB::select("SELECT
                                pasien.nik,
                                pasien.tanggal_lahir,
                                pasien.nama_anak,
                                pasien.nama_ortu,
                                CONCAT_WS(',',pasien.alamat,lokasi.kelurahan,lokasi.kecamatan,lokasi.kabupaten,lokasi.provinsi) as alamat,
                                pasien.umur,
                                pasien.jenis_kelamin,
                                tetanus.no_epid,
                                tetanus.no_epid_lama,
                                tetanus.keadaan_akhir,
                                tetanus.tanggal_mulai_sakit,
                                tetanus.klasifikasi_akhir,
                                tetanus.tanggal_laporan_diterima,
                                tetanus.tanggal_pelacakan,
                                tetanus.ANC,
                                tetanus.status_imunisasi,
                                tetanus.penolong_persalinan,
                                tetanus.pemotongan_tali_pusat,
                                tetanus.rawat_rumah_sakit
                            FROM
                                tetanus,
                                pasien,
                                lokasi,
                                pasien_terserang_tetanus,
                                users
                            WHERE
                                pasien.id_pasien=pasien_terserang_tetanus.id_pasien
                            AND
                                lokasi.id_kelurahan=pasien.id_kelurahan
                            AND
                                tetanus.id_tetanus=pasien_terserang_tetanus.id_tetanus
                            AND
                                users.email ='" . Sentry::getUser()->email . "'
                            AND
                                tetanus.id_tetanus='" . $id . "'");
        return View::make('pemeriksaan.tetanus.detail', compact('tetanus'));
    }

    //get edit tetanus
    public function getEditTetanus($id) {
        $tetanus = DB::select("SELECT
                                pasien.nik,
                                pasien.tanggal_lahir,
                                pasien.nama_anak,
                                pasien.nama_ortu,
                                pasien.alamat,
                                pasien.umur,
                                pasien.umur_bln,
                                pasien.umur_hr,
                                lokasi.provinsi,
                                lokasi.kabupaten,
                                lokasi.kecamatan,
                                lokasi.kelurahan,
                                lokasi.id_kelurahan,
                                lokasi.id_kecamatan,
                                lokasi.id_kabupaten,
                                pasien.id_kelurahan,
                                pasien.jenis_kelamin,
                                tetanus.no_epid,
                                tetanus.no_epid_lama,
                                tetanus.keadaan_akhir,
                                tetanus.tanggal_mulai_sakit,
                                tetanus.klasifikasi_akhir,
                                pasien_terserang_tetanus.id_pasien_tetanus,
                                tetanus.tanggal_laporan_diterima,
                                tetanus.tanggal_pelacakan,
                                tetanus.ANC,
                                tetanus.status_imunisasi,
                                tetanus.penolong_persalinan,
                                tetanus.pemotongan_tali_pusat,
                                tetanus.rawat_rumah_sakit
                            FROM
                                tetanus,
                                pasien,
                                lokasi,
                                pasien_terserang_tetanus,
                                users
                            WHERE
                                pasien.id_pasien=pasien_terserang_tetanus.id_pasien
                            AND
                                lokasi.id_kelurahan=pasien.id_kelurahan
                            AND
                                tetanus.id_tetanus=pasien_terserang_tetanus.id_tetanus
                            AND
                                users.email ='" . Sentry::getUser()->email . "'
                            AND
                                tetanus.id_tetanus ='" . $id . "'");

        return View::make('pemeriksaan.tetanus.edit', compact('tetanus'));
    }

    public function getHapus($id) {
        $dt['deleted_by'] = Sentry::getUser()->id;
        $dt['deleted_at'] = date('Y-m-d H:i:s');
        $update           = DB::table('tetanus')
            ->where('id_tetanus', $id)
            ->update($dt);
        $delete_notif = DB::table('notification')
            ->where('global_id', $id)
            ->where('type_id', 'tetanus')
            ->update($dt);
        return Redirect::to('tetanus');
    }

    /*public function getHapus($id,$id_lab)
    {
    try
    {
    DB::beginTransaction();
    DB::delete("delete from pasien_terserang_tetanus where pasien_terserang_tetanus.id_pasien_tetanus='".$id_lab."'");
    DB::delete("delete from tetanus where tetanus.id_tetanus ='".$id."'");
    DB::commit();
    }
    catch (Exception $e)
    {
    DB::rollback();
    }

    return Redirect::to('tetanus');
    }*/

    //post edit tetanus
    public function updateTetanus() {
        DB::transaction(function () {
            DB::update("update
                            pasien,tetanus,pasien_terserang_tetanus
                        set
                            pasien.nik='" . Input::get('nik') . "',
                            pasien.tanggal_lahir=" . $this->changeDate(Input::get('tanggal_lahir')) . ",
                            pasien.nama_anak='" . Input::get('nama_anak') . "',
                            pasien.nama_ortu='" . Input::get('nama_ortu') . "',
                            pasien.alamat='" . Input::get('alamat') . "',
                            pasien.umur='" . Input::get('tgl_tahun') . "',
                            pasien.umur_bln='" . Input::get('tgl_bulan') . "',
                            pasien.umur_hr='" . Input::get('tgl_hari') . "',
                            pasien.id_kelurahan='" . Input::get('id_kelurahan') . "',
                            pasien.jenis_kelamin='" . Input::get('jenis_kelamin') . "',
                            tetanus.no_epid='" . Input::get('no_epid') . "',
                            tetanus.no_epid_lama='" . Input::get('no_epid_lama') . "',
                            tetanus.keadaan_akhir='" . Input::get('keadaan_akhir') . "',
                            tetanus.tanggal_mulai_sakit=" . $this->changeDate(Input::get('tanggal_mulai_sakit')) . ",
                            tetanus.klasifikasi_akhir='" . Input::get('klasifikasi_akhir') . "',
                            tetanus.tanggal_laporan_diterima=" . $this->changeDate(Input::get('tanggal_laporan_diterima')) . ",
                            tetanus.tanggal_pelacakan=" . $this->changeDate(Input::get('tanggal_pelacakan')) . ",
                            tetanus.ANC='" . Input::get('ANC') . "',
                            tetanus.status_imunisasi='" . Input::get('status_imunisasi') . "',
                            tetanus.penolong_persalinan='" . Input::get('penolong_persalinan') . "',
                            tetanus.pemotongan_tali_pusat='" . Input::get('pemotongan_tali_pusat') . "',
                            tetanus.rawat_rumah_sakit='" . Input::get('rawat_rumah_sakit') . "'
                        where
                            pasien.id_pasien=pasien_terserang_tetanus.id_pasien
                        and
                            tetanus.id_tetanus=pasien_terserang_tetanus.id_tetanus
                        and
                            pasien_terserang_tetanus.id_pasien_tetanus='" . Input::get('id_pasien_tetanus') . "'");
        });

        //ketika tersimpan akan diarahkan kehalaman index tetanus
        return Redirect::to('tetanus');
    }

    //route daftar tetanus
    public function getDaftarTetanus() {
        $pe_tetanus = Tetanus::getPeTetanus();
        return View::make('pemeriksaan.tetanus.daftar', compact('pe_tetanus'));
    }

    //fungsi untuk mengambil data pasien yang akan dilakukan penyelidikan epidemologi tetanus
    public function getEntri_tetanus($id) {
        $pe_tetanus = DB::table('getpetetanus')
            ->where('id_tetanus', $id)->get();

        return View::make('pemeriksaan.tetanus.pe_tetanus', compact('pe_tetanus'));
    }

    //fungsi untuk mengedit data petugas persalinan pada modul PE tetanus
    public function getEditPetugasKehamilan() {
        $id   = Input::get('id');
        $data = DB::select("select id,nama_pemeriksa,profesi,alamat,frekuensi from petugas_pemeriksa where id='" . $id . "'");

        foreach ($data as $value) {
            $id             = $value->id;
            $nama_pemeriksa = $value->nama_pemeriksa;
            $profesi        = $value->profesi;
            $alamat         = $value->alamat;
            $frekuensi      = $value->frekuensi;
        }

        return Response::json(compact('id', 'nama_pemeriksa', 'profesi', 'alamat', 'frekuensi'));
    }

    //fungsi untuk mengedit data petugas pelacak pada modul PE tetanus
    public function getEditPetugasPelacak() {
        $id   = Input::get('id');
        $data = DB::select("select id,nama_pemeriksa,profesi from petugas_pemeriksa where id='" . $id . "'");

        foreach ($data as $value) {
            $id             = $value->id;
            $nama_pemeriksa = $value->nama_pemeriksa;
            $profesi        = $value->profesi;
        }

        return Response::json(compact('id', 'nama_pemeriksa', 'profesi'));
    }

    //fungsi untuk menupdate data petugas kehamilan pada modul PE tetanus
    public function getHapusPetugasKehamilan() {
        DB::update("delete from petugas_pemeriksa where id='" . Input::get('id') . "'");
        echo 'sukses';
    }

    //fungsi untuk menupdate data petugas kehamilan pada modul PE tetanus
    public function getUpdatePetugasKehamilan() {
        DB::update("update
                            petugas_pemeriksa
                        set
                            nama_pemeriksa='" . Input::get('nama_petugas_kehamilan') . "',
                            profesi='" . Input::get('profesi_petugas_kehamilan') . "',
                            alamat='" . Input::get('alamat_petugas_kehamilan') . "',
                            frekuensi='" . Input::get('frekuensi') . "'
                        where
                            id='" . Input::get('id') . "'");
        echo 'sukses';
    }

    //fungsi untuk menupdate data petugas pelacak pada modul PE tetanus
    public function getUpdatePetugasPelacak() {
        DB::update("update
                            petugas_pemeriksa
                        set
                            nama_pemeriksa='" . Input::get('nama') . "',
                            profesi='" . Input::get('profesi') . "'
                        where
                            id='" . Input::get('id') . "'");
        echo 'sukses';
    }

    //fungsi untuk menupdate data petugas pelacak pada modul PE tetanus
    public function getHapusPetugasPelacak() {
        DB::update("delete from petugas_pemeriksa where id='" . Input::get('id') . "'");
        echo 'sukses';
    }

    //fungsi untuk menambah petugas kehamilan pada modul input PE tetanus
    public function getTambahPetugaskehamilan() {
        DB::table('petugas_pemeriksa')->insert(
            array(
                'no_epid'              => Input::get('no_epid'),
                'nama_pemeriksa'       => Input::get('nama_petugas_kehamilan'),
                'profesi'              => Input::get('profesi_petugas_kehamilan'),
                'alamat'               => Input::get('alamat_petugas_kehamilan'),
                'frekuensi'            => Input::get('frekuensi'),
                'identifikasi_petugas' => 'K',
            ));

        echo 'sukses';

    }

    //fungsi untuk menambah petugas pelacak pada modul input PE tetanus
    public function getTambahPetugasPelacak() {
        DB::table('petugas_pemeriksa')->insert(
            array(
                'no_epid'              => Input::get('no_epid'),
                'nama_pemeriksa'       => Input::get('nama'),
                'profesi'              => Input::get('profesi'),
                'identifikasi_petugas' => 'T',
            ));
        echo 'sukses';

    }

    //fungsi untuk menampilkan petugas kehamilan pada modul input PE tetanus
    public function getPetugasKehamilan($no_epid) {
        $table = '<tr>
                    <td>Nama</td>
                    <td>Profesi</td>
                    <td>Alamat</td>
                    <td>Frekuensi</td>
                    <td>aksi</td>
                  </tr>';
        $hasil = DB::SELECT("SELECT
                    a.id,
                    a.nama_pemeriksa,
                    a.alamat,
                    a.profesi,
                    a.frekuensi
                    FROM
                    petugas_pemeriksa AS a
                    JOIN tetanus AS b ON a.no_epid=b.no_epid
                    WHERE a.identifikasi_petugas='K'
                    AND a.no_epid='$no_epid'");
        foreach ($hasil as $value) {
            $table .= '<tr>
                            <td>' . $value->nama_pemeriksa . '</td>
                            <td>' . $value->profesi . '</td>
                            <td>' . $value->alamat . '</td>
                            <td>' . $value->frekuensi . '</td>
                            <td><a onclick="edit_petugas_kehamilan(' . $value->id . ')" class="btn btn-warning">edit</a>
                            <a onclick="hapus_petugas_kehamilan(' . $value->id . ')" class="btn btn-danger">hapus</a></td>
                        </tr>';
        }

        return $table;
    }

    //fungsi untuk menampilkan petugas pelacak pada modul input PE tetanus
    public function getPetugasPelacak($no_epid) {
        $table = '<tr>
                    <td>Nama</td>
                    <td>Jabatan</td>
                    <td>aksi</td>
                  </tr>';
        $hasil = DB::select("select petugas_pemeriksa.id, petugas_pemeriksa.nama_pemeriksa,petugas_pemeriksa.profesi from petugas_pemeriksa,tetanus where tetanus.no_epid=petugas_pemeriksa.no_epid and petugas_pemeriksa.identifikasi_petugas='T' and petugas_pemeriksa.no_epid='" . $no_epid . "'");
        foreach ($hasil as $value) {
            $table .= '<tr>
                            <td>' . $value->nama_pemeriksa . '</td>
                            <td>' . $value->profesi . '</td>
                            <td><a onclick="edit_pelacak(' . $value->id . ')" class="btn btn-warning">edit</a>
                            <a onclick="hapus_pelacak(' . $value->id . ')" class="btn btn-danger">hapus</a></td>
                        </tr>';
        }

        return $table;
    }

    //fungsi untuk menyimpan data kasus penyelidikan epidemologi tetanus
    public function postEpidTetanus() {
        if ((
            Input::get('sumber_laporan') ||
            Input::get('nama_sumber_laporan') ||
            Input::get('tanggal_laporan_diterima') ||
            Input::get('tanggal_pelacakan') ||
            Input::get('anak_ke') ||
            Input::get('nama_ayah') ||
            Input::get('umur_ayah') ||
            Input::get('th_pendidikan_ayah') ||
            Input::get('pekerjaan_ayah') ||
            Input::get('nama_ibu') ||
            Input::get('th_pendidikan_ibu') ||
            Input::get('pekerjaan_ibu') ||
            Input::get('nama_yang_diwawancarai') ||
            Input::get('hubungan_keluarga') ||
            Input::get('bayi_hidup') ||
            Input::get('tgl_lahir') ||
            Input::get('tanggal_mulai_sakit') ||
            Input::get('bayi_meninggal') ||
            Input::get('tanggal_meninggal') ||
            Input::get('umur') ||
            Input::get('bayi_menangis') ||
            Input::get('tanda_kehidupan_lain') ||
            Input::get('bayi_menetek') ||
            Input::get('bayi_mencucu') ||
            Input::get('bayi_kejang') ||
            Input::get('dirawat') ||
            Input::get('tempat_berobat') ||
            Input::get('nama_faskes_bayi_dirawat') ||
            Input::get('tanggal_dirawat') ||
            Input::get('keadaan_setelah_dirawat') ||
            Input::get('info_imunisasi') ||
            Input::get('mendapat_imunisasi') ||
            Input::get('jumlah_imunisasi') ||
            Input::get('umur_kehamilan_pertama') ||
            Input::get('umur_kehamilan_kedua') ||
            Input::get('status_ibu_saat_hamil') ||
            Input::get('tanggal_imunisasi_kedua') ||
            Input::get('ibu_mendapat_imunisasi_1') ||
            Input::get('ibu_mendapat_imunisasi_2') ||
            Input::get('tahun_kehamilan_1') ||
            Input::get('tahun_kehamilan_2') ||
            Input::get('ibu_mendapat_suntikan_pengantin') ||
            Input::get('obat_penyembuh') ||
            Input::get('ramuan_lainnya') ||
            Input::get('perawatan_tali_pusat') ||
            Input::get('obat_merawat_tali_pusat') ||
            Input::get('kesimpulan_diagnosis') ||
            Input::get('TT1') ||
            Input::get('TT2') ||
            Input::get('TT3') ||
            Input::get('TT4') ||
            Input::get('TT5') ||
            Input::get('cakupan_tenaga_kesehatan') ||
            Input::get('KN1') ||
            Input::get('KN2') ||
            Input::get('gpa') ||
            Input::get('jml_periksa_kehamilan_dengan_dokter') ||
            Input::get('jml_periksa_kehamilan_dengan_bidan_perawat') ||
            Input::get('jml_periksa_kehamilan_dengan_dukun') ||
            Input::get('jml_Tidak_ANC') ||
            Input::get('jml_tidak_jelas')) == '') {
            return Redirect::to('tetanus');
        }

        $id_pasien                                  = Input::get('id_pasien');
        $no_epid                                    = Input::get('no_epid');
        $sumber_laporan                             = Input::get('sumber_laporan');
        $nama_sumber_laporan                        = Input::get('nama_sumber_laporan');
        $tanggal_laporan_diterima                   = $this->changeDate(Input::get('tanggal_laporan_diterima'));
        $tanggal_pelacakan                          = $this->changeDate(Input::get('tanggal_pelacakan'));
        $anak_ke                                    = Input::get('anak_ke');
        $nama_ayah                                  = Input::get('nama_ayah');
        $umur_ayah                                  = Input::get('umur_ayah');
        $th_pendidikan_ayah                         = Input::get('th_pendidikan_ayah');
        $pekerjaan_ayah                             = Input::get('pekerjaan_ayah');
        $nama_ibu                                   = Input::get('nama_ibu');
        $th_pendidikan_ibu                          = Input::get('th_pendidikan_ibu');
        $pekerjaan_ibu                              = Input::get('pekerjaan_ibu');
        $nama_yang_diwawancarai                     = Input::get('nama_yang_diwawancarai');
        $hubungan_keluarga                          = Input::get('hubungan_keluarga');
        $bayi_hidup                                 = Input::get('bayi_hidup');
        $tanggal_lahir                              = $this->changeDate(Input::get('tgl_lahir'));
        $tanggal_mulai_sakit                        = $this->changeDate(Input::get('tanggal_mulai_sakit'));
        $bayi_meninggal                             = Input::get('bayi_meninggal');
        $tanggal_meninggal                          = $this->changeDate(Input::get('tanggal_meninggal'));
        $umur                                       = Input::get('umur');
        $bayi_menangis                              = Input::get('bayi_menangis');
        $tanda_kehidupan_lain                       = Input::get('tanda_kehidupan_lain');
        $bayi_menetek                               = Input::get('bayi_menetek');
        $bayi_mencucu                               = Input::get('bayi_mencucu');
        $bayi_kejang                                = Input::get('bayi_kejang');
        $dirawat                                    = Input::get('dirawat');
        $tempat_berobat                             = Input::get('tempat_berobat');
        $nama_faskes_bayi_dirawat                   = Input::get('nama_faskes_bayi_dirawat');
        $tanggal_dirawat                            = $this->changeDate(Input::get('tanggal_dirawat'));
        $keadaan_setelah_dirawat                    = Input::get('keadaan_setelah_dirawat');
        $info_imunisasi                             = Input::get('info_imunisasi');
        $mendapat_imunisasi                         = Input::get('mendapat_imunisasi');
        $jumlah_imunisasi                           = Input::get('jumlah_imunisasi');
        $umur_kehamilan_pertama                     = Input::get('umur_kehamilan_pertama');
        $umur_kehamilan_kedua                       = Input::get('umur_kehamilan_kedua');
        $status_ibu_saat_hamil                      = Input::get('status_ibu_saat_hamil');
        $tanggal_imunisasi_kedua                    = $this->changeDate(Input::get('tanggal_imunisasi_kedua'));
        $ibu_mendapat_imunisasi_1                   = Input::get('ibu_mendapat_imunisasi_1');
        $ibu_mendapat_imunisasi_2                   = Input::get('ibu_mendapat_imunisasi_2');
        $tahun_kehamilan_1                          = Input::get('tahun_kehamilan_1');
        $tahun_kehamilan_2                          = Input::get('tahun_kehamilan_2');
        $ibu_mendapat_suntikan_pengantin            = Input::get('ibu_mendapat_suntikan_pengantin');
        $obat_penyembuh                             = Input::get('obat_penyembuh');
        $ramuan_lainnya                             = Input::get('ramuan_lainnya');
        $perawatan_tali_pusat                       = Input::get('perawatan_tali_pusat');
        $obat_merawat_tali_pusat                    = Input::get('obat_merawat_tali_pusat');
        $kesimpulan_diagnosis                       = Input::get('kesimpulan_diagnosis');
        $TT1                                        = Input::get('TT1');
        $TT2                                        = Input::get('TT2');
        $TT3                                        = Input::get('TT3');
        $TT4                                        = Input::get('TT4');
        $TT5                                        = Input::get('TT5');
        $cakupan_tenaga_kesehatan                   = Input::get('cakupan_tenaga_kesehatan');
        $KN1                                        = Input::get('KN1');
        $KN2                                        = Input::get('KN2');
        $gpa                                        = Input::get('gpa');
        $jml_periksa_kehamilan_dengan_dokter        = Input::get('jml_periksa_kehamilan_dengan_dokter');
        $jml_periksa_kehamilan_dengan_bidan_perawat = Input::get('jml_periksa_kehamilan_dengan_bidan_perawat');
        $jml_periksa_kehamilan_dengan_dukun         = Input::get('jml_periksa_kehamilan_dengan_dukun');
        $jml_Tidak_ANC                              = Input::get('jml_Tidak_ANC');
        $jml_tidak_jelas                            = Input::get('jml_tidak_jelas');

        DB::table('pe_tetanus')->insertGetId(array(
            'id_pasien'                                  => $id_pasien,
            'no_epid'                                    => $no_epid,
            'created_by'                                 => Sentry::getUser()->id,
            'sumber_laporan'                             => $sumber_laporan,
            'nama_sumber_laporan'                        => $nama_sumber_laporan,
            'tanggal_laporan_diterima'                   => $tanggal_laporan_diterima,
            'tanggal_pelacakan'                          => $tanggal_pelacakan,
            'anak_ke'                                    => $anak_ke,
            'nama_ayah'                                  => $nama_ayah,
            'umur_ayah'                                  => $umur_ayah,
            'th_pendidikan_ayah'                         => $th_pendidikan_ayah,
            'pekerjaan_ayah'                             => $pekerjaan_ayah,
            'nama_ibu'                                   => $nama_ibu,
            'th_pendidikan_ibu'                          => $th_pendidikan_ibu,
            'pekerjaan_ibu'                              => $pekerjaan_ibu,
            'nama_yang_diwawancarai'                     => $nama_yang_diwawancarai,
            'hubungan_keluarga'                          => $hubungan_keluarga,
            'bayi_hidup'                                 => $bayi_hidup,
            'tanggal_lahir'                              => $tanggal_lahir,
            'tanggal_mulai_sakit'                        => $tanggal_mulai_sakit,
            'bayi_meninggal'                             => $bayi_meninggal,
            'tanggal_meninggal'                          => $tanggal_meninggal,
            'umur'                                       => $umur,
            'bayi_menangis'                              => $bayi_menangis,
            'tanda_kehidupan_lain'                       => $tanda_kehidupan_lain,
            'bayi_menetek'                               => $bayi_menetek,
            'bayi_mencucu'                               => $bayi_mencucu,
            'bayi_kejang'                                => $bayi_kejang,
            'dirawat'                                    => $dirawat,
            'tempat_berobat'                             => $tempat_berobat,
            'nama_faskes_bayi_dirawat'                   => $nama_faskes_bayi_dirawat,
            'tanggal_dirawat'                            => $tanggal_dirawat,
            'keadaan_setelah_dirawat'                    => $keadaan_setelah_dirawat,
            'info_imunisasi'                             => $info_imunisasi,
            'mendapat_imunisasi'                         => $mendapat_imunisasi,
            'jumlah_imunisasi'                           => $jumlah_imunisasi,
            'umur_kehamilan_pertama'                     => $umur_kehamilan_pertama,
            'umur_kehamilan_kedua'                       => $umur_kehamilan_kedua,
            'status_ibu_saat_hamil'                      => $status_ibu_saat_hamil,
            'tanggal_imunisasi_kedua'                    => $tanggal_imunisasi_kedua,
            'ibu_mendapat_imunisasi_1'                   => $ibu_mendapat_imunisasi_1,
            'ibu_mendapat_imunisasi_2'                   => $ibu_mendapat_imunisasi_2,
            'tahun_kehamilan_1'                          => $tahun_kehamilan_1,
            'tahun_kehamilan_2'                          => $tahun_kehamilan_2,
            'ibu_mendapat_suntikan_pengantin'            => $ibu_mendapat_suntikan_pengantin,
            'obat_penyembuh'                             => $obat_penyembuh,
            'ramuan_lainnya'                             => $ramuan_lainnya,
            'perawatan_tali_pusat'                       => $perawatan_tali_pusat,
            'obat_merawat_tali_pusat'                    => $obat_merawat_tali_pusat,
            'kesimpulan_diagnosis'                       => $kesimpulan_diagnosis,
            'TT1'                                        => $TT1,
            'TT2'                                        => $TT2,
            'TT3'                                        => $TT3,
            'TT4'                                        => $TT4,
            'TT5'                                        => $TT5,
            'cakupan_tenaga_kesehatan'                   => $cakupan_tenaga_kesehatan,
            'KN1'                                        => $KN1,
            'KN2'                                        => $KN2,
            'gpa'                                        => $gpa,
            'jml_periksa_kehamilan_dengan_dokter'        => $jml_periksa_kehamilan_dengan_dokter,
            'jml_periksa_kehamilan_dengan_bidan_perawat' => $jml_periksa_kehamilan_dengan_bidan_perawat,
            'jml_periksa_kehamilan_dengan_dukun'         => $jml_periksa_kehamilan_dengan_dukun,
            'jml_Tidak_ANC'                              => $jml_Tidak_ANC,
            'jml_tidak_jelas'                            => $jml_tidak_jelas,
            'created_at'                                 => date("Y-m-d H:i:s"),
            'updated_at'                                 => date("Y-m-d H:i:s")));

        return Redirect::to('tetanus');

    }

    public function getDetailPeTetanus($id) {
        $data = DB::select("select
                                pasien.id_pasien,pasien.nama_anak, pasien.alamat, pasien.tanggal_lahir,
                                    pasien.umur_hr, pasien.jenis_kelamin,
                                pasien.id_kelurahan,
                                pe_tetanus.id,
                                pe_tetanus.sumber_laporan, pe_tetanus.nama_sumber_laporan,pe_tetanus.tanggal_laporan_diterima, pe_tetanus.tanggal_pelacakan,
                                pe_tetanus.anak_ke, pe_tetanus.nama_ayah, pe_tetanus.th_pendidikan_ayah, pe_tetanus.umur_ayah,
                                pe_tetanus.pekerjaan_ayah, pe_tetanus.nama_ibu, pe_tetanus.no_epid, pe_tetanus.th_pendidikan_ibu,
                                pe_tetanus.pekerjaan_ibu, pe_tetanus.nama_yang_diwawancarai, pe_tetanus.hubungan_keluarga,
                                pe_tetanus.bayi_hidup, pe_tetanus.tanggal_lahir as tgl_lahir_bayi, pe_tetanus.tanggal_mulai_sakit,
                                pe_tetanus.bayi_meninggal, pe_tetanus.tanggal_meninggal, pe_tetanus.umur as umur_mati, pe_tetanus.bayi_menangis,
                                pe_tetanus.tanda_kehidupan_lain, pe_tetanus.bayi_menetek, pe_tetanus.bayi_mencucu as bayi_mecucu,
                                pe_tetanus.dirawat, pe_tetanus.bayi_kejang, pe_tetanus.tempat_berobat, pe_tetanus.tanggal_dirawat,
                                pe_tetanus.keadaan_setelah_dirawat,pe_tetanus.info_imunisasi, pe_tetanus.mendapat_imunisasi,
                                pe_tetanus.jumlah_imunisasi, pe_tetanus.umur_kehamilan_pertama,
                                pe_tetanus.umur_kehamilan_kedua, pe_tetanus.tanggal_imunisasi_kedua,
                                pe_tetanus.ibu_mendapat_suntikan_pengantin,
                                pe_tetanus.status_ibu_saat_hamil, pe_tetanus.obat_penyembuh, pe_tetanus.ramuan_lainnya,
                                pe_tetanus.perawatan_tali_pusat, pe_tetanus.obat_merawat_tali_pusat,
                                pe_tetanus.kesimpulan_diagnosis, pe_tetanus.TT1, pe_tetanus.TT2, pe_tetanus.TT3, pe_tetanus.TT4,
                                pe_tetanus.TT5, pe_tetanus.cakupan_tenaga_kesehatan, pe_tetanus.KN1, pe_tetanus.KN2
                            from
                                pasien,pe_tetanus,lokasi
                            where
                                pasien.id_pasien=pe_tetanus.id_pasien
                            and
                                lokasi.id_kelurahan=pasien.id_kelurahan
                            and
                                pe_tetanus.id='" . $id . "'
                            ");
        return View::make('pemeriksaan.tetanus.detail_pe_tetanus', compact('data'));
    }

    public function getEditPeTetanus($id) {

        $tetanus = Tetanus::getEditPeTetanus($id);
        return View::make('pemeriksaan.tetanus.edit_pe_tetanus', compact('tetanus'));
    }

    public function postEditPeTetanus() {
        DB::update("update
                        pe_tetanus
                    set
                        pe_tetanus.sumber_laporan='" . Input::get('sumber_laporan') . "',
                        pe_tetanus.nama_sumber_laporan='" . Input::get('nama_sumber_laporan') . "',
                        pe_tetanus.tanggal_laporan_diterima=" . $this->changeDate(Input::get('tanggal_laporan_diterima')) . ",
                        pe_tetanus.tanggal_pelacakan=" . $this->changeDate(Input::get('tanggal_pelacakan')) . ",
                        pe_tetanus.anak_ke='" . Input::get('anak_ke') . "',
                        pe_tetanus.nama_ayah='" . Input::get('nama_ayah') . "',
                        pe_tetanus.th_pendidikan_ayah='" . Input::get('th_pendidikan_ayah') . "',
                        pe_tetanus.umur_ayah='" . Input::get('umur_ayah') . "',
                        pe_tetanus.pekerjaan_ayah='" . Input::get('pekerjaan_ayah') . "',
                        pe_tetanus.nama_ibu='" . Input::get('nama_ibu') . "',
                        pe_tetanus.no_epid='" . Input::get('no_epid') . "',
                        pe_tetanus.th_pendidikan_ibu='" . Input::get('th_pendidikan_ibu') . "',
                        pe_tetanus.pekerjaan_ibu='" . Input::get('pekerjaan_ibu') . "',
                        pe_tetanus.nama_yang_diwawancarai='" . Input::get('nama_yang_diwawancarai') . "',
                        pe_tetanus.hubungan_keluarga='" . Input::get('hubungan_keluarga') . "',
                        pe_tetanus.bayi_hidup='" . Input::get('bayi_hidup') . "',
                        pe_tetanus.tanggal_lahir=" . $this->changeDate(Input::get('tgl_lahir')) . ",
                        pe_tetanus.tanggal_mulai_sakit=" . $this->changeDate(Input::get('tanggal_mulai_sakit')) . ",
                        pe_tetanus.bayi_meninggal='" . Input::get('bayi_meninggal') . "',
                        pe_tetanus.tanggal_meninggal=" . $this->changeDate(Input::get('tanggal_meninggal')) . ",
                        pe_tetanus.umur='" . Input::get('umur') . "',
                        pe_tetanus.bayi_menangis='" . Input::get('bayi_menangis') . "',
                        pe_tetanus.tanda_kehidupan_lain='" . Input::get('tanda_kehidupan_lain') . "',
                        pe_tetanus.bayi_menetek='" . Input::get('bayi_menetek') . "',
                        pe_tetanus.bayi_mencucu='" . Input::get('bayi_mencucu') . "',
                        pe_tetanus.dirawat='" . Input::get('dirawat') . "',
                        pe_tetanus.bayi_kejang='" . Input::get('bayi_kejang') . "',
                        pe_tetanus.tempat_berobat='" . Input::get('tempat_berobat') . "',
                        pe_tetanus.tanggal_dirawat='" . Input::get('tanggal_dirawat') . "',
                        pe_tetanus.keadaan_setelah_dirawat='" . Input::get('keadaan_setelah_dirawat') . "',
                        pe_tetanus.info_imunisasi='" . Input::get('info_imunisasi') . "',
                        pe_tetanus.mendapat_imunisasi='" . Input::get('mendapat_imunisasi') . "',
                        pe_tetanus.jumlah_imunisasi='" . Input::get('jumlah_imunisasi') . "',
                        pe_tetanus.umur_kehamilan_pertama='" . Input::get('umur_kehamilan_pertama') . "',
                        pe_tetanus.umur_kehamilan_kedua='" . Input::get('umur_kehamilan_kedua') . "',
                        pe_tetanus.tanggal_imunisasi_kedua=" . $this->changeDate(Input::get('tanggal_imunisasi_kedua')) . ",
                        pe_tetanus.ibu_mendapat_suntikan_pengantin='" . Input::get('ibu_mendapat_suntikan_pengantin') . "',
                        pe_tetanus.obat_penyembuh='" . Input::get('obat_penyembuh') . "',
                        pe_tetanus.ramuan_lainnya='" . Input::get('ramuan_lainnya') . "',
                        pe_tetanus.perawatan_tali_pusat='" . Input::get('perawatan_tali_pusat') . "',
                        pe_tetanus.obat_merawat_tali_pusat='" . Input::get('obat_merawat_tali_pusat') . "',
                        pe_tetanus.kesimpulan_diagnosis='" . Input::get('kesimpulan_diagnosis') . "',
                        pe_tetanus.TT1='" . Input::get('TT1') . "',
                        pe_tetanus.TT2='" . Input::get('TT2') . "',
                        pe_tetanus.TT3='" . Input::get('TT3') . "',
                        pe_tetanus.TT4='" . Input::get('TT4') . "',
                        pe_tetanus.TT5='" . Input::get('TT5') . "',
                        pe_tetanus.cakupan_tenaga_kesehatan='" . Input::get('cakupan_tenaga_kesehatan') . "',
                        pe_tetanus.KN1='" . Input::get('KN1') . "',
                        pe_tetanus.KN2='" . Input::get('KN2') . "',
                        pe_tetanus.status_ibu_saat_hamil='" . Input::get('status_ibu_saat_hamil') . "'
                    where
                        pe_tetanus.id='" . Input::get('id') . "'");

        return Redirect::to('tetanus');
    }

    //fungsi untuk mendapatkan no epid tetanus secara otomatis
    public function getEpid() {
        $id_kel   = Input::get('id_kelurahan');
        $tglsakit = $this->changeDate(Input::get('date'));
        $thn      = substr($tglsakit, 2, 2);
        $data2    = DB::select("select max(no_epid) as no_epid from tetanus,pasien,pasien_terserang_tetanus where pasien.id_pasien=pasien_terserang_tetanus.id_pasien and tetanus.id_tetanus=pasien_terserang_tetanus.id_tetanus and id_kelurahan='" . $id_kel . "' order by tetanus.id_tetanus limit 0,1");
        $cek      = DB::select("select RIGHT(no_epid,3) from tetanus,pasien,pasien_terserang_tetanus where MID(no_epid,13,2) ='" . $thn . "' and pasien.id_pasien=pasien_terserang_tetanus.id_pasien and tetanus.id_tetanus=pasien_terserang_tetanus.id_tetanus and pasien.id_kelurahan='" . $id_kel . "' order by tetanus.id_tetanus limit 0,1");

        if (empty($cek)) {
            $code     = $id_kel . $thn;
            $caracter = 'TN';
            $epid     = '001';
            $no_epid  = $caracter . $code . $epid;
        } else {
            $kode   = substr($data2[0]->no_epid, 0, 14);
            $noUrut = (int) substr($data2[0]->no_epid, 14, 3);
            $noUrut++;
            $no_epid = $kode . sprintf("%03s", $noUrut);
        }

        return $no_epid;
    }

    //fungsi menghapus data pe tetanus
    public function getHapusPeTetanus($id) {
        try {
            DB::beginTransaction();
            DB::delete("delete from pe_tetanus where pe_tetanus.id='" . $id . "'");
            DB::commit();
        } catch (Exception $e) {

            DB::rollback();

        }
        return Redirect::to('tetanus');
    }

    public function getAll() {
        $all_tenanus = Tetanus::with('patients')->get();
        return $all_tenanus;
    }

    public function exportExcel($filetype, $dt = null) {
        $ds        = json_decode($dt);
        $penyakit  = Session::get('penyakit');
        $excel_tmp = array(
            array(
                'No',
                'No Epid Kasus / KLB',
                'Nama Bayi',
                'Nama Orangtua',
                'Alamat',
                'Puskesmas',
                'Kecamatan',
                'Kabupaten/Kota',
                'Provinsi',
                'Umur',
                'Jenis Kelamin',
                'Tanggal Laporan Diterima',
                'Tanggal Pelacakan',
                'Antenatal Care (ANC)',
                'Antenatal Care (ANC)',
                'Antenatal Care (ANC)',
                'Antenatal Care (ANC)',
                'Antenatal Care (ANC)',
                'Status Imunisasi Ibu',
                'Status Imunisasi Ibu',
                'Status Imunisasi Ibu',
                'Status Imunisasi Ibu',
                'Penolong persalinan',
                'Penolong persalinan',
                'Penolong persalinan',
                'Penolong persalinan',
                'Perawatan Tali Pusat',
                'Perawatan Tali Pusat',
                'Perawatan Tali Pusat',
                'Perawatan Tali Pusat',
                'Pemotongan Tali Pusat',
                'Pemotongan Tali Pusat',
                'Pemotongan Tali Pusat',
                'Pemotongan Tali Pusat',
                'Rawat Rumah Sakit',
                'Rawat Rumah Sakit',
                'Rawat Rumah Sakit',
                'Keadaan akhir',
                'Klasifikasi final',
                'Klasifikasi final',
                'Klasifikasi final',
            ),
            array(
                '1',
                '2',
                '3',
                '4',
                '5',
                '6',
                '7',
                '8',
                '9',
                'Hari',
                '11',
                '12',
                '13',
                'Dokter',
                'Bidan/Perawat',
                'Dukun',
                'Tidak ANC',
                'Tidak Jelas',
                'TT2+',
                'TT1',
                'Tidak Imunisasi',
                'Tidak Jelas',
                'Dokter',
                'Bidan/Perawat',
                'Dukun',
                'Tidak Jelas',
                'Alc/Iod',
                'Ramuan',
                'Lain-Lain',
                'Tidak Jelas',
                'Gunting',
                'Bambu',
                'Lain-lain',
                'Tidak Jelas',
                'Ya',
                'Tidak',
                'Tidak Jelas',
                'Hidup/Meninggal',
                'Konfirm TN',
                'Tersangka TN',
                'Bukan TN',
            ),
            array(''),
            array(''),
        );

        foreach ($ds as $k => $v) {
            $post[$v->name] = $v->value;
        }
        $between         = $wilayah         = '';
        $unit            = (empty($post['unit']) ? '' : $post['unit']);
        $day_start       = (empty($post['day_start']) ? '' : $post['day_start']);
        $month_start     = (empty($post['month_start']) ? '' : $post['month_start']);
        $year_start      = (empty($post['year_start']) ? '' : $post['year_start']);
        $day_end         = (empty($post['day_end']) ? '' : $post['day_end']);
        $month_end       = (empty($post['month_end']) ? '' : $post['month_end']);
        $year_end        = (empty($post['year_end']) ? '' : $post['year_end']);
        $province_id     = (empty($post['province_id']) ? '' : $post['province_id']);
        $district_id     = (empty($post['district_id']) ? '' : $post['district_id']);
        $sub_district_id = (empty($post['sub_district_id']) ? '' : $post['sub_district_id']);
        $rs_id           = (empty($post['rs_id']) ? '' : $post['rs_id']);
        $puskesmas_id    = (empty($post['puskesmas_id']) ? '' : $post['puskesmas_id']);

        switch ($unit) {
            case "all":
                $between = " ";
                break;
            case "year":
                $start   = $post['year_start'];
                $end     = $post['year_end'];
                $between = " AND YEAR(c.tanggal_periksa) BETWEEN '" . $start . "' AND '" . $end . "' ";
                break;
            case "month":
                $start   = $post['year_start'] . '-' . $post['month_start'] . '-1';
                $end     = $post['year_end'] . '-' . $post['month_end'] . '-' . date('t', mktime(0, 0, 0, $post['month_end'], 1, $post['year_end']));
                $between = " AND DATE(c.tanggal_periksa) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                break;
            default:
                $start   = $post['year_start'] . '-' . $post['month_start'] . '-' . $post['day_start'];
                $end     = $post['year_end'] . '-' . $post['month_end'] . '-' . $post['day_end'];
                $between = " AND DATE(c.tanggal_periksa) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                break;
        }

        if ($puskesmas_id) {
            $wilayah = "AND puskesmas_code_faskes='" . $puskesmas_id . "'";
        } else if ($sub_district_id) {
            $wilayah = "AND e.id_kecamatan='" . $sub_district_id . "'";
        } else if ($rs_id) {
            $wilayah = "AND j.kode_faskes='" . $rs_id . "' ";
        } else if ($district_id) {
            $wilayah = "AND f.id_kabupaten='" . $district_id . "' ";
        } else if ($province_id) {
            $wilayah = "AND g.id_provinsi='" . $province_id . "' ";
        }

        $q = "
                SELECT
                    c.no_epid,
                    b.nama_anak,
                    b.nama_ortu,
                    b.alamat,
                    h.puskesmas_name,
                    e.kecamatan,
                    f.kabupaten,
                    g.provinsi,
                    IF(b.umur_hr='','',CONCAT(b.umur_hr,' Hari')) as umur_hr,

                    #JENIS KELAMIN
                    IF(b.jenis_kelamin=null,'Tidak Diketahui',
                    IF(b.jenis_kelamin='1','Laki-laki',
                    IF(b.jenis_kelamin='2','Perempuan',
                    IF(b.jenis_kelamin='3','Tidak Jelas',
                    'Tidak diisi')))) AS jenis_kelamin,

                    DATE_FORMAT(c.tanggal_laporan_diterima,'%d-%m-%Y') as tanggal_laporan_diterima,
                    DATE_FORMAT(c.tanggal_pelacakan,'%d-%m-%Y') as tanggal_pelacakan,

                    #ANC
                    IF(c.ANC='0','Dokter',
                    IF(c.ANC='1','Bidan/Perawat',
                    IF(c.ANC='2','Dukun',
                    IF(c.ANC='3','Tidak ANC',
                    IF(c.ANC='4','Tidak Jelas',
                    'Tidak Diisi'))))) AS anc,

                    IF(c.ANC='0','V','') AS anc_dokter,
                    IF(c.ANC='1','V','') AS anc_bidan,
                    IF(c.ANC='2','V','') AS anc_dukun,
                    IF(c.ANC='3','V','') AS anc_tidak_anc,
                    IF(c.ANC='4','V','') AS anc_tidak_jelas,


                    #STATUS IMUNISASI
                    IF(c.status_imunisasi='0','TT2+',
                    IF(c.status_imunisasi='1','TT1',
                    IF(c.status_imunisasi='2','Tidak Imunisasi',
                    IF(c.status_imunisasi='Tidak Jelas','Tidak Jelas',
                    'Tidak Diisi')))) AS status_imunisasi,

                    IF(c.status_imunisasi='0','V','') AS status_imunisasi_tt2,
                    IF(c.status_imunisasi='1','V','') AS status_imunisasi_tt1,
                    IF(c.status_imunisasi='2','V','') AS status_imunisasi_tidak_imunisasi,
                    IF(c.status_imunisasi='Tidak Jelas','V','') AS status_imunisasi_tidak_jelas,

                    #PENOLONG PERSALINAN
                    IF(c.penolong_persalinan='0','Dokter',
                    IF(c.penolong_persalinan='1','Bidan/Perawat',
                    IF(c.penolong_persalinan='2','Dukun',
                    IF(c.penolong_persalinan='3','Tidak Jelas',
                    'Tidak diisi')))) AS penolong_persalinan,

                    IF(c.penolong_persalinan='0','V','') AS penolong_persalinan_dokter,
                    IF(c.penolong_persalinan='1','V','') AS penolong_persalinan_bidan,
                    IF(c.penolong_persalinan='2','V','') AS penolong_persalinan_dukun,
                    IF(c.penolong_persalinan='3','V','') AS penolong_persalinan_tidak_jelas,


                    #PEMOTONG TALI PUSAT
                    IF(c.pemotongan_tali_pusat='0','Gunting',
                    IF(c.pemotongan_tali_pusat='1','Bambu',
                    IF(c.pemotongan_tali_pusat='2','Lain-lain',
                    IF(c.pemotongan_tali_pusat='3','Tidak Jelas',
                    'Tidak diisi')))) AS pemotongan_tali_pusat,

                    IF(c.pemotongan_tali_pusat='0','V','') AS pemotongan_tali_pusat_gunting,
                    IF(c.pemotongan_tali_pusat='1','V','') AS pemotongan_tali_pusat_bambu,
                    IF(c.pemotongan_tali_pusat='2','V','') AS pemotongan_tali_pusat_lain,
                    IF(c.pemotongan_tali_pusat='3','V','') AS pemotongan_tali_pusat_tidak_jelas,

                    #KEADAAN AKHIR
                    IF(c.keadaan_akhir='H','Hidup',
                    IF(c.keadaan_akhir='M','Mati',
                    'Tidak diisi')) AS keadaan_akhir,

                    c.klasifikasi_akhir,

                    IF(c.klasifikasi_akhir='Konfirm TN','V','') AS klasifikasi_akhir_konfirm_tn,
                    IF(c.klasifikasi_akhir='Tersangka TN','V','') AS klasifikasi_akhir_tersangka_tn,
                    IF(c.klasifikasi_akhir='Bukan TN','V','') AS klasifikasi_akhir_bukan_tn

                FROM pasien_terserang_tetanus a
                JOIN pasien b ON b.id_pasien=a.id_pasien
                JOIN tetanus c ON c.id_tetanus=a.id_tetanus
                LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
                LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
                LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
                LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
                LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
                LEFT JOIN rumahsakit2 j  ON j.id=c.id_tempat_periksa
                WHERE
                1=1
                " . $between . "
                " . $wilayah . "
            ";

        $data = DB::select($q);

        $ke_bawah = 3;
        for ($i = 0; $i < count($data); $i++) {
            $excel_tmp[$ke_bawah + $i][0]  = 1 + $i;
            $excel_tmp[$ke_bawah + $i][1]  = $data[$i]->no_epid;
            $excel_tmp[$ke_bawah + $i][2]  = $data[$i]->nama_anak;
            $excel_tmp[$ke_bawah + $i][3]  = $data[$i]->nama_ortu;
            $excel_tmp[$ke_bawah + $i][4]  = $data[$i]->alamat;
            $excel_tmp[$ke_bawah + $i][5]  = $data[$i]->puskesmas_name;
            $excel_tmp[$ke_bawah + $i][6]  = $data[$i]->kecamatan;
            $excel_tmp[$ke_bawah + $i][7]  = $data[$i]->kabupaten;
            $excel_tmp[$ke_bawah + $i][8]  = $data[$i]->provinsi;
            $excel_tmp[$ke_bawah + $i][9]  = $data[$i]->umur_hr;
            $excel_tmp[$ke_bawah + $i][10] = $data[$i]->jenis_kelamin;
            $excel_tmp[$ke_bawah + $i][11] = $data[$i]->tanggal_laporan_diterima;
            $excel_tmp[$ke_bawah + $i][12] = $data[$i]->tanggal_pelacakan;

            $excel_tmp[$ke_bawah + $i][13] = $data[$i]->anc_dokter;
            $excel_tmp[$ke_bawah + $i][14] = $data[$i]->anc_bidan;
            $excel_tmp[$ke_bawah + $i][15] = $data[$i]->anc_dukun;
            $excel_tmp[$ke_bawah + $i][16] = $data[$i]->anc_tidak_anc;
            $excel_tmp[$ke_bawah + $i][17] = $data[$i]->anc_tidak_jelas;

            $excel_tmp[$ke_bawah + $i][18] = $data[$i]->status_imunisasi_tt2;
            $excel_tmp[$ke_bawah + $i][19] = $data[$i]->status_imunisasi_tt1;
            $excel_tmp[$ke_bawah + $i][20] = $data[$i]->status_imunisasi_tidak_imunisasi;
            $excel_tmp[$ke_bawah + $i][21] = $data[$i]->status_imunisasi_tidak_jelas;

            $excel_tmp[$ke_bawah + $i][22] = $data[$i]->penolong_persalinan_dokter;
            $excel_tmp[$ke_bawah + $i][23] = $data[$i]->penolong_persalinan_bidan;
            $excel_tmp[$ke_bawah + $i][24] = $data[$i]->penolong_persalinan_dukun;
            $excel_tmp[$ke_bawah + $i][25] = $data[$i]->penolong_persalinan_tidak_jelas;

            $excel_tmp[$ke_bawah + $i][30] = $data[$i]->pemotongan_tali_pusat_gunting;
            $excel_tmp[$ke_bawah + $i][31] = $data[$i]->pemotongan_tali_pusat_bambu;
            $excel_tmp[$ke_bawah + $i][32] = $data[$i]->pemotongan_tali_pusat_lain;
            $excel_tmp[$ke_bawah + $i][33] = $data[$i]->pemotongan_tali_pusat_tidak_jelas;

            $excel_tmp[$ke_bawah + $i][37] = $data[$i]->keadaan_akhir;

            $excel_tmp[$ke_bawah + $i][38] = $data[$i]->klasifikasi_akhir_konfirm_tn;
            $excel_tmp[$ke_bawah + $i][39] = $data[$i]->klasifikasi_akhir_tersangka_tn;
            $excel_tmp[$ke_bawah + $i][40] = $data[$i]->klasifikasi_akhir_bukan_tn;
        }

        Excel::create('FORM_C1_TETANUS_' . date('Y_m_d_h_i_s'), function ($excel) use ($excel_tmp) {
            $excel->sheet('Sheetname', function ($sheet) use ($excel_tmp) {
                $sheet->fromArray($excel_tmp);
                //$sheet->mergeCells('A1:AF1');

                $sheet->mergeCells('A2:A4');
                $sheet->mergeCells('B2:B4');
                $sheet->mergeCells('C2:C4');
                $sheet->mergeCells('D2:D4');
                $sheet->mergeCells('E2:E4');
                $sheet->mergeCells('F2:F4');
                $sheet->mergeCells('G2:G4');
                $sheet->mergeCells('H2:H4');
                $sheet->mergeCells('I2:I4');
                $sheet->mergeCells('J3:J4');
                $sheet->mergeCells('K2:K4');
                $sheet->mergeCells('L2:L4');
                $sheet->mergeCells('M2:M4');
                $sheet->mergeCells('N3:N4');
                $sheet->mergeCells('O3:O4');
                $sheet->mergeCells('P3:P4');
                $sheet->mergeCells('Q3:Q4');
                $sheet->mergeCells('R3:R4');
                $sheet->mergeCells('S3:S4');
                $sheet->mergeCells('T3:T4');
                $sheet->mergeCells('U3:U4');
                $sheet->mergeCells('V3:V4');
                $sheet->mergeCells('W3:W4');
                $sheet->mergeCells('X3:X4');
                $sheet->mergeCells('Y3:Y4');
                $sheet->mergeCells('Z3:Z4');
                $sheet->mergeCells('AA3:AA4');
                $sheet->mergeCells('AB3:AB4');
                $sheet->mergeCells('AC3:AC4');
                $sheet->mergeCells('AD3:AD4');
                $sheet->mergeCells('AE3:AE4');
                $sheet->mergeCells('AF3:AF4');
                $sheet->mergeCells('AG3:AG4');
                $sheet->mergeCells('AH3:AH4');
                $sheet->mergeCells('AI3:AI4');
                $sheet->mergeCells('AJ3:AJ4');
                $sheet->mergeCells('AK3:AK4');
                $sheet->mergeCells('AL3:AL4');
                $sheet->mergeCells('AM3:AM4');
                $sheet->mergeCells('AN3:AN4');
                $sheet->mergeCells('AO3:AO4');

                $sheet->mergeCells('N2:R2');
                $sheet->mergeCells('S2:V2');
                $sheet->mergeCells('W2:Z2');
                $sheet->mergeCells('AA2:AD2');
                $sheet->mergeCells('AE2:AH2');
                $sheet->mergeCells('AI2:AK2');
                $sheet->mergeCells('AM2:AO2');

                $sheet->cells('A2:AO4', function ($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#FFFFAA');
                    $cells->setFontSize(13);
                    $cells->setFontWeight('bold');
                });

                $sheet->cells('N5:AO1000', function ($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                });

                $sheet->setWidth('A', 5);
                $sheet->setWidth('B', 25);
                $sheet->setWidth('C', 22);
                $sheet->setWidth('D', 22);
                $sheet->setWidth('E', 50);
                $sheet->setWidth('F', 122);
                $sheet->setWidth('G', 20);
                $sheet->setWidth('H', 20);
                $sheet->setWidth('I', 30);
                $sheet->setWidth('J', 10);
                $sheet->setWidth('K', 15);
                $sheet->setWidth('L', 26);
                $sheet->setWidth('M', 20);
                $sheet->setWidth('N', 17);
                $sheet->setWidth('O', 17);
                $sheet->setWidth('P', 17);
                $sheet->setWidth('Q', 17);
                $sheet->setWidth('R', 17);
                $sheet->setWidth('S', 17);
                $sheet->setWidth('T', 17);
                $sheet->setWidth('U', 17);
                $sheet->setWidth('V', 17);
                $sheet->setWidth('W', 17);
                $sheet->setWidth('X', 17);
                $sheet->setWidth('Y', 17);
                $sheet->setWidth('Z', 17);
                $sheet->setWidth('AA', 17);
                $sheet->setWidth('AB', 17);
                $sheet->setWidth('AC', 17);
                $sheet->setWidth('AD', 17);
                $sheet->setWidth('AE', 17);
                $sheet->setWidth('AF', 17);
                $sheet->setWidth('AG', 17);
                $sheet->setWidth('AH', 17);
                $sheet->setWidth('AI', 17);
                $sheet->setWidth('AJ', 17);
                $sheet->setWidth('AK', 17);
                $sheet->setWidth('AL', 19);
                $sheet->setWidth('AM', 17);
                $sheet->setWidth('AN', 17);
                $sheet->setWidth('AO', 17);

                $sheet->setBorder('A2:AO4', 'thin');

                $sheet->setFreeze('D5');
                $sheet->setAutoFilter('A4:AO4');
            });

        })->export('xls');
    }

    public function filterDaftarKasus() {
        $type = Session::get('type');

        $kd_faskes = Session::get('kd_faskes');

        $wilayah = "";

        // mendapatkan tipe user yang sedang login
        if ($type == "puskesmas") {
            $wilayah = "AND h.puskesmas_code_faskes='" . $kd_faskes . "' ";
        } else if ($type == "kabupaten") {
            $wilayah = "AND h.kode_kab='" . $kd_faskes . "' ";
        } else if ($type == "provinsi") {
            $wilayah = "AND h.kode_prop='" . $kd_faskes . "' ";
        } else if ($type == "kemenkes") {
            $wilayah = "";
        }

        $unit            = Input::get('unit');
        $day_start       = Input::get('day_start');
        $month_start     = Input::get('month_start');
        $year_start      = Input::get('year_start');
        $day_end         = Input::get('day_end');
        $month_end       = Input::get('month_end');
        $year_end        = Input::get('year_end');
        $province_id     = Input::get('province_id');
        $district_id     = Input::get('district_id');
        $sub_district_id = Input::get('sub_district_id');
        $village_id      = Input::get('village_id');

        switch ($unit) {
            case "all":
                $between = " ";
                break;
            case "year":
                $start   = $post['year_start'];
                $end     = $post['year_end'];
                $between = " AND YEAR(c.tanggal_periksa) BETWEEN '" . $start . "' AND '" . $end . "' ";
                break;
            case "month":
                $start   = $post['year_start'] . '-' . $post['month_start'] . '-1';
                $end     = $post['year_end'] . '-' . $post['month_end'] . '-' . date('t', mktime(0, 0, 0, $post['month_end'], 1, $post['year_end']));
                $between = " AND DATE(c.tanggal_periksa) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                break;
            default:
                $start   = $post['year_start'] . '-' . $post['month_start'] . '-' . $post['day_start'];
                $end     = $post['year_end'] . '-' . $post['month_end'] . '-' . $post['day_end'];
                $between = " AND DATE(c.tanggal_periksa) BETWEEN STR_TO_DATE('" . $start . "', '%Y-%c-%e') AND STR_TO_DATE('" . $end . "', '%Y-%c-%e') ";
                break;
        }

        $wilayah = "";
        if ($village_id) {
            $wilayah = "AND d.id_kelurahan='" . $village_id . "' ";
        } else if ($sub_district_id) {
            $wilayah = "AND e.id_kecamatan='" . $sub_district_id . "' ";
        } else if ($district_id) {
            $wilayah = "AND f.id_kabupaten='" . $district_id . "' ";
        } else if ($province_id) {
            $wilayah = "AND g.id_provinsi='" . $province_id . "' ";
        }

        $q = "
            SELECT
                c.no_epid,
                c.id_tetanus,
                b.nama_anak,
                b.tanggal_lahir,
                CONCAT_WS(',',b.alamat,d.kelurahan,e.kecamatan,f.kabupaten,g.provinsi) as alamat,


                #KEADAAN AKHIR
                IF(c.keadaan_akhir='0','Hidup',
                IF(c.keadaan_akhir='1','Meninggal',
                'Tidak diisi')) AS keadaan_akhir,

                c.klasifikasi_akhir

            FROM pasien_terserang_tetanus a
            JOIN pasien b ON b.id_pasien=a.id_pasien
            JOIN tetanus c ON c.id_tetanus=a.id_tetanus
            LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
            LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
            LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
            LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
            LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
            #WHERE
            #1=1
            #" . $between . "
            #" . $wilayah . "
        ";

        $data = DB::select($q);

        return View::make('pemeriksaan.tetanus.index', compact('data'));
    }
}