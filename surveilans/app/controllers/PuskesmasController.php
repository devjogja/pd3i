<?php

class PuskesmasController extends \BaseController {


	public function __construct()
	{
		//filter
		$this->beforeFilter('auth');
	}
	/**
	 * Display a listing of puskesmas
	 *
	 * @return Response
	 */
	public function index()
	{
		//$puskesmas = Puskesmas::all();
		$puskesmas = Puskesmas::getPKM();
		$jml = Puskesmas::getSumPKM();
		return View::make('puskesmas.index', compact('puskesmas'))
		->with('jml',$jml);
	}

	/**
	 * Show the form for creating a new Puskesmas
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('puskesmas.create');
	}

	/**
	 * Store a newly created Puskesmas in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Puskesmas::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Puskesmas::create($data);

		return Redirect::route('puskesmas.index');
	}

	/**
	 * Display the specified Puskesmas.
	 *
	 * @param  in t  $id
	 * @return Response
	 */
	public function show($id)
	{
		$puskesmas = Puskesmas::findOrFail($id);

		return View::make('puskesmas.show', compact('puskesmas'));
	}

	/**
	 * Show the form for editing the specified Puskesmas.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$puskesmas = Puskesmas::find($id);

		return View::make('puskesmas.edit', compact('puskesmas'));
	}

	/**
	 * Update the specified Puskesmas in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$puskesmas = Puskesmas::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Puskesmas::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$puskesmas->update($data);

		return Redirect::route('puskesmas.index');
	}

	/**
	 * Remove the specified Puskesmas from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Puskesmas::destroy($id);

		return Redirect::route('puskesmas.index');
	}

	// load daftar akun di level puskesmas
	public function listakun()
	{
		$puskesmas = DB::select('select users.email,puskesmas.nama_puskesmas,puskesmas.telepon
								from users,puskesmas,users_groups
								where users.id=puskesmas.id_user and users.id=users_groups.user_id and users_groups.group_id=2');
		return View::make('puskesmas.listakun',compact('puskesmas'));
	}

	//get form untuk menambah akun baru
	public function createakun()
	{
		return View::make('puskesmas.daftarakun');
	}


	public function surveilansOfficer()
	{
		$column = array(
			'first_name',
			'last_name',
			'instansi',
			'email',
			'no_telp'
		);

		$users = User::get($column);
		return View::make('puskesmas.list-surveilans', compact('users'));
		// return User::get($column);
	}
}
