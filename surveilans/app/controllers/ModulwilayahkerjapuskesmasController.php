<?php
class ModulwilayahkerjapuskesmasController extends BaseController {
	
	public function getIndex()
	{
		return View::make('master_data.modulwilayahkerjapuskesmas');
	}
	
	public function getList()
	{
		$modulwilayahkerjapuskesmas = ModulwilayahkerjapuskesmasModel::getListModulwilayahkerjapuskesmas();
		$current_page = Input::get('page');
		$current_search = Input::get('search_name');		
		return View::make('master_data.modulwilayahkerjapuskesmas_list',compact('modulwilayahkerjapuskesmas','current_page','current_search'));
	}
	
	public function getListGet()
	{
		$modulwilayahkerjapuskesmas = ModulwilayahkerjapuskesmasModel::getListModulwilayahkerjapuskesmas();
		$current_page = Input::get('page');
		$current_search = Input::get('search_name');		
		return View::make('master_data.modulwilayahkerjapuskesmas_list',compact('modulwilayahkerjapuskesmas','current_page','current_search'));
	}
	
	public function ModulwilayahkerjapuskesmasProcessForm(){
		$ret = array();
        if(Input::get('saved_id')) {
			$ret['command'] = "updating data";
			if(ModulwilayahkerjapuskesmasModel::DoUpdateData()) {
				$ret['msg'] = "Sukses Update Data";
				$ret['status'] = "success";
			} else {
				$ret['msg'] = "Gagal Update Data";
				$ret['status'] = "error";
			}
        } else {
			$ret['command'] = "adding data";
			$last_id = ModulwilayahkerjapuskesmasModel::DoAddData();
			if($last_id > 0) {
				$ret['msg'] = "Sukses Simpan Data";
				$ret['status'] = "success";
			} else {
				$ret['msg'] = "Gagal Simpan Data";
				$ret['status'] = "error";
			}
        }
		
		echo json_encode($ret);
		
	}
	
	public function ModulwilayahkerjapuskesmasGetDataById(){
		$provinsi = ModulwilayahkerjapuskesmasModel::getModulwilayahkerjapuskesmas();
		echo json_encode($provinsi);
	}
	
	public function ModulwilayahkerjapuskesmasDeleteList(){
		ModulwilayahkerjapuskesmasModel::DoDeleteModulwilayahkerjapuskesmas();
		$ret['msg'] = "Sukses Delete Data";
		$ret['status'] = "success";
		echo json_encode($ret);
	}
	
}
?>
