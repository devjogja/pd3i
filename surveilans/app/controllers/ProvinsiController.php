<?php

class ProvinsiController extends \BaseController {

    public function __construct() {
        //filter
        $this->beforeFilter('auth');
    }
    /**
     * Display a listing of provinsi
     *
     * @return Response
     */
    public function index() {
        //$provinsi = Provinsi::getWilayah();

        //return View::make('provinsi.index', compact('provinsi'));
    }

    /**
     * Show the form for creating a new provinsi
     *
     * @return Response
     */
    public function create() {
        return View::make('provinsi.create');
    }

    /**
     * Store a newly created provinsi in storage.
     *
     * @return Response
     */
    public function store() {
        $validator = Validator::make($data = Input::all(), Provinsi::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        Provinsi::create($data);

        return Redirect::route('provinsi.index');
    }

    /**
     * Display the specified provinsi.
     *
     * @param  int  $id
     * @return Response
     */
    /*public function show($id)
    {
    //$provinsi = Provinsi::findOrFail($id);

    return View::make('provinsi.show', compact('provinsi'));
    }*/

    /**
     * Show the form for editing the specified provinsi.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $provinsi = Provinsi::find($id);

        return View::make('provinsi.edit', compact('provinsi'));
    }

    /**
     * Update the specified provinsi in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $provinsi = Provinsi::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Provinsi::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $provinsi->update($data);

        return Redirect::route('provinsi.index');
    }

    /**
     * Remove the specified provinsi from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        Provinsi::destroy($id);

        return Redirect::route('provinsi.index');
    }

    //fungsi untuk load data provinsi sesuai ID provinsi yang dipilih
    public function Kabupaten() {
        $pro          = Input::get('provinsi');
        $id_kabupaten = Input::get('kabupaten');
        $kabupaten    = DB::table('kabupaten')->select('id_kabupaten', 'kabupaten')->where('id_provinsi', $pro)->get();

        $response = '<option value="">Pilih Kabupaten</option>';
        foreach ($kabupaten as $key) {
            if ($id_kabupaten == $key->id_kabupaten) {
                $response .= "<option value=" . $key->id_kabupaten . " selected='selected'>" . $key->kabupaten . "</option>";
            } else {
                $response .= "<option value=" . $key->id_kabupaten . ">" . $key->kabupaten . "</option>";
            }
        }
        echo $response;
    }

    //fungsi untuk load data kecamatan sesuai ID kabupaten yang dipilih
    public function Kecamatan() {
        $kab          = Input::get('kabupaten');
        $id_kecamatan = Input::get('kecamatan');
        $kecamatan    = DB::table('kecamatan')->select('id_kecamatan', 'kecamatan')->where('id_kabupaten', $kab)->get();

        $response = '<option value="">Pilih Kecamatan</option>';
        foreach ($kecamatan as $key) {
            if ($id_kecamatan == $key->id_kecamatan) {
                $response .= "<option value=" . $key->id_kecamatan . " selected='selected'>" . $key->kecamatan . "</option>\n";
            } else {
                $response .= "<option value=" . $key->id_kecamatan . ">" . $key->kecamatan . "</option>\n";
            }
        }
        echo $response;
    }

    public function Rumahsakit() {
        $kab = Input::get('kabupaten');
        $rs  = DB::table('rumahsakit2')->select('kode_kab', 'nama_faskes', 'id', 'kode_faskes')->where('kode_kab', $kab)->get();
        foreach ($rs as $key) {
            echo "<option value=" . $key->id . ">" . $key->nama_faskes . "</option>\n";
        }
    }

    //fungsi untuk load data kecamatan sesuai ID kabupaten yang dipilih
    public function Kelurahan() {
        $kec          = Input::get('kecamatan');
        $id_kelurahan = Input::get('kelurahan');
        $kelurahan    = DB::table('kelurahan')->select('id_kelurahan', 'kelurahan')->where('id_kecamatan', $kec)->get();

        $response = '<option value="">Pilih Kelurahan</option>';
        foreach ($kelurahan as $key) {
            if ($id_kelurahan == $key->id_kelurahan) {
                $response .= "<option value=" . $key->id_kelurahan . " selected='selected'>" . $key->kelurahan . "</option>\n";
            } else {
                $response .= "<option value=" . $key->id_kelurahan . ">" . $key->kelurahan . "</option>\n";
            }
        }
        echo $response;
    }

    public function Puskesmas() {
        $kec          = Input::get('kecamatan');
        $id_puskesmas = Input::get('puskesmas');
        $puskesmas    = DB::SELECT("
			SELECT * FROM puskesmas
			WHERE puskesmas_code_faskes LIKE 'P" . $kec . "%'
			order by puskesmas_name ASC
		");
        foreach ($puskesmas as $key) {
            if ($id_puskesmas == $key->puskesmas_id) {
                echo "<option value=" . $key->puskesmas_id . " selected='selected'>" . $key->puskesmas_name . "</option>\n";
            } else {
                echo "<option value=" . $key->puskesmas_id . ">" . $key->puskesmas_name . "</option>\n";
            }
        }
    }

    //fungsi untuk mengload daftar akun petugas
    public function show() {
        //select table
        $provinsi = DB::select('select
									users.email,provinsi.provinsi,sub_provinsi.telepon
								from
									users,sub_provinsi,users_groups,provinsi
								where
									users.id=sub_provinsi.id_user
								and
									users.id=users_groups.user_id
								and
									sub_provinsi.id_provinsi=provinsi.id_provinsi
								and
									users_groups.group_id=2');

        return View::make('provinsi.listakun', compact('provinsi'));
    }

    //load form untuk menambah akun pada level provinsi
    public function Form() {

        return View::make('provinsi.daftarakun');
    }

    //fungsi untuk menyimpan pendaftaran akun pada level provinsi
    public function DaftarAkun() {
        /*$validator = Validator::make($data = Input::all(), Subk::$rules);

        if ($validator->fails())
        {
        return Redirect::back()->withErrors($validator)->withInput();
        }*/

        // Membuat user regular baru
        $user = Sentry::register(array(
            // silahkan ganti sesuai keinginan
            'email'    => Input::get('username'),
            'password' => Input::get('password'),
        ), true); // langsung diaktivasi
        // Cari grup regular
        $regularGroup = Sentry::findGroupByName('regular');
        // Masukkan user ke grup regular
        $user->addGroup($regularGroup);
        //ambil id yang terakhir masuk
        //$sql = DB::statement('select max(id) from users');
        foreach (DB::select("select max(id) as id from users") as $id) {
            $id = $id->id;
        }
        //simpan ke table sub_kabupaten
        DB::table('sub_provinsi')->insert(array(
            'id_provinsi' => Input::get('id_provinsi'),
            'id_user'     => $id));

        return View::make('provinsi.listakun');
    }

}
