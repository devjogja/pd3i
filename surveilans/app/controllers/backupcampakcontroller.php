<?php

class BackupCampakController extends \BaseController {

	

	//load view
	protected $layout = "layouts.master"; 
	
	public function __construct()
	{
		//filter
		$this->beforeFilter('auth');
	}

	/**
	 * Display a listing of Campak
	 *
	 * @return Response
	 */
	public function index()
	{
		
		if(!Sentry::check())
		{
			return Redirect::to('/');
		}

		Session::put('sess_id', '');
		Session::put('penyakit', 'Campak');
		return View::make('pemeriksaan.campak.index');
	}
	
	/**
	 * Show the form for creating a new Campak
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('pemeriksaan.campak.index');
	}

	/**
	 * Store a newly created Campak in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
		
		$kd_faskes = Session::get('kd_faskes');
		
		$q="
		SELECT puskesmas_id
		FROM puskesmas
		WHERE puskesmas_code_faskes='".$kd_faskes."'
		";
		$data=DB::select($q);
		$puskesmas_id 	= $data[0]->puskesmas_id;
		
		//simpan data personal
		$id_pasien=DB::table('pasien')->insertGetId(
				array(
					'nik'=>Input::get('nik'),
					'nama_anak'=>Input::get('nama_anak'),
					'nama_ortu'=>Input::get('nama_ortu'),
					'alamat'=>Input::get('alamat'),
					'tanggal_lahir'=>Input::get('tanggal_lahir'),
					'umur'=>Input::get('tgl_tahun'),
					'umur_bln'=>Input::get('tgl_bulan'),
					'umur_hr'=>Input::get('tgl_hari'),
					'jenis_kelamin'=>Input::get('jenis_kelamin'),
					'id_kelurahan'=>Input::get('id_kelurahan')
				));
		//cek jika table pasien sukses di isi maka simpan data penyakit campak ke table campak
		if($id_pasien) {
			//simpan data campak
			//$cek = DB::select();
			$id_campak=DB::table('campak')->insertGetId(
			array(
				'no_epid'=>Input::get('no_epid'),
				'no_epid_lama'=>Input::get('no_epid_lama'),
				'nama_puskesmas'=>Input::get('nama_puskesmas'),
				'created_by'=>Sentry::getUser()->id,
				'vaksin_campak_sebelum_sakit'=>Input::get('vaksin_campak_sebelum_sakit'),
				'tanggal_timbul_demam'=>Input::get('tanggal_timbul_demam'),
				'tanggal_timbul_rash'=>Input::get('tanggal_timbul_rash'),
				'tanggal_diambil_spesimen_oral_fluid'=>Input::get('tanggal_diambil_spesimen_oral_fluid'),
				'jenis_pemeriksaan'=>Input::get('jenis_pemeriksaan'),
				'hasil_igm_campak'=>Input::get('hasil_igm_campak'),
				'hasil_igm_rubella'=>Input::get('hasil_igm_rubella'),
				'hasil_isolasi_virus'=>Input::get('hasil_isolasi_virus'),
				'klasifikasi_final'=>Input::get('klasifikasi_final'),
				'tanggal_diambil_spesimen_darah'=>Input::get('tanggal_diambil_spesimen_darah'),
				'tanggal_diambil_spesimen_urin'=>Input::get('tanggal_diambil_spesimen_urin'),
				'vitamin_A'=>Input::get('vitamin_A'),
				'keadaan_akhir'=>Input::get('keadaan_akhir'),
				'tanggal_laporan_diterima'=>Input::get('tanggal_laporan_diterima'),
				'tanggal_pelacakan'=>Input::get('tanggal_pelacakan'),
				'tanggal_periksa' => date('Y-m-d'),
				'id_tempat_periksa' => $puskesmas_id,
				'created_at' =>date('Y-m-d H:i:s'),
				'updated_at' =>date('Y-m-d H:i:s')
				));

			//simpan ke uji laboratorium
			DB::table('hasil_uji_lab_campak')->insert(
				array(
					'id_campak'=> $id_campak,
					'id_pasien'=> $id_pasien
				)); 
			}

			$gejala_lain = Input::get('gejala_lain');
			$tipe_spesimen = Input::get('tipe_spesimen');
			$tanggal_gejala_lain = Input::get('tanggal_gejala_lain');
			$jml_tipe_spesimen = Input::get('jml_tipe_spesimen');
			
			if(isset($gejala_lain)) {
			for ($i=0; $i < count($gejala_lain); $i++) { 
				$id_daftar_gejala_campak=DB::table('daftar_gejala_campak')->insertGetId(
					array(
						'nama'=>$gejala_lain[$i],
						'created_at' =>date('Y-m-d H:i:s'),
						'updated_at' =>date('Y-m-d H:i:s')
					));

				DB::table('gejala_campak')->insert(
					array(
						'id_campak'=>$id_campak,
						'id_daftar_gejala_campak'=>$id_daftar_gejala_campak,
						'tgl_mulai' => $tanggal_gejala_lain[$i] ,
						'created_at' =>date('Y-m-d H:i:s'),
						'updated_at' =>date('Y-m-d H:i:s')
					));
			}
			}

			if(isset($tipe_spesimen)) {
			for ($i=0; $i < count($tipe_spesimen); $i++) { 
				DB::table('tipe_spesimen')->insert(
					array(
						'id_campak'=>$id_campak,
						'tipe_spesimen'=>$tipe_spesimen[$i],
						'jml_tipe_spesimen'=>$jml_tipe_spesimen[$i],
						'created_at' =>date('Y-m-d H:i:s'),
						'updated_at' =>date('Y-m-d H:i:s')
					));
			}
			}

			//post notifikasi
			
			$q="
			SELECT 
				b.id_pasien,	
				b.nama_anak,
				e.id_kecamatan AS id_kec_pasien,
				e.kecamatan AS nm_kec_pasien,
				h.kode_kec AS id_kec_faskes,
				i.kecamatan AS nm_kec_faskes
			FROM hasil_uji_lab_campak a 
			JOIN pasien b ON b.id_pasien=a.id_pasien
			JOIN campak c ON c.id_campak=a.id_campak
			LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
			LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
			LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
			LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
			LEFT JOIN puskesmas h  ON h.puskesmas_id=c.id_tempat_periksa
			LEFT JOIN kecamatan i ON i.id_kecamatan=h.kode_kec
			WHERE a.id_campak='".$id_campak."'
			";
			$data=DB::select($q);
			$id_kec_pasien 	= $data[0]->id_kec_pasien;
			$id_kec_faskes 	= $data[0]->id_kec_faskes;
			
			if($id_kec_pasien != $id_kec_faskes){
				DB::table('notification')->insert(
				array(
					'from_faskes_id'=>$puskesmas_id,
					'from_faskes_kec_id'=>$id_kec_faskes,
					'to_faskes_kec_id'=>$id_kec_pasien,
					'type_id'=>'campak',
					'global_id'=>$id_campak,
					'description'=>'',
					'submit_dttm'=>date('Y-m-d H:i:s')
				));
			}
		return Redirect::to('campak');
	}

	//fungsi input individual kasus campak
	//jika data dimasukan dari laboratorium
	public function postInputbaru() {

		//simpan data personal
		$data=DB::table('pasien')->insert(
				array(
					'nik'=>Input::get('nik'),
					'nama_anak'=>Input::get('nama_anak'),
					'nama_ortu'=>Input::get('nama_ortu'),
					'alamat'=>Input::get('alamat'),
					'tanggal_lahir'=>Input::get('tanggal_lahir'),
					'umur'=>Input::get('tgl_tahun'),
					'umur_bln'=>Input::get('tgl_bulan'),
					'umur_hr'=>Input::get('tgl_hari'),
					'jenis_kelamin'=>Input::get('jenis_kelamin'),
					'id_kelurahan'=>Input::get('id_kelurahan')
				));
		if($data) {
			//simpan data campak
			//$cek = DB::select();
			DB::table('campak')->insert(
			array(
				'no_epid'=>Input::get('no_epid'),
				'vaksin_campak_sebelum_sakit'=>Input::get('vaksin_campak_sebelum_sakit'),
				'tanggal_timbul_demam'=>Input::get('tanggal_timbul_demam'),
				'nama_puskesmas'=>Input::get('nama_puskesmas'),
				'tanggal_timbul_rash'=>Input::get('tanggal_timbul_rash'),
				'tanggal_diambil_spesimen_oral_fluid'=>Input::get('tanggal_diambil_spesimen_oral_fluid'),
				'jenis_pemeriksaan'=>Input::get('jenis_pemeriksaan'),
				'tipe_spesimen'=>Input::get('tipe_spesimen'),
				'hasil_igm_campak'=>Input::get('hasil_igm_campak'),
				'hasil_igm_rubella'=>Input::get('hasil_igm_rubella'),
				'hasil_isolasi_virus'=>Input::get('hasil_isolasi_virus'),
				'klasifikasi_final'=>Input::get('klasifikasi_final'),
				'tanggal_diambil_spesimen_darah'=>Input::get('tanggal_diambil_spesimen_darah'),
				'tanggal_diambil_spesimen_urin'=>Input::get('tanggal_diambil_spesimen_urin'),
				'vitamin_A'=>Input::get('vitamin_A'),
				'keadaan_akhir'=>Input::get('keadaan_akhir'),
				'tanggal_laporan_diterima'=>Input::get('tanggal_laporan_diterima'),
				'tanggal_pelacakan'=>Input::get('tanggal_pelacakan'),
				'tanggal_periksa' => date('Y-m-d'),
				'created_at'=> date('Y-m-d')
				));
			//ambil id terakhir pada table pasien
			//$id_pasien = 0;
			$id_pasien = DB::table('pasien')->max('id_pasien');
			//ambil id terakhir pada table tetanus
			$id_campak = DB::table('campak')->max('id_campak');
			//simpan ke uji laboratorium
			DB::table('hasil_uji_lab_campak')->insert(
				array(
					'id_campak'=> $id_campak,
					'id_pasien'=> $id_pasien
				));

			$no_epid = Input::get('no_epid');
			$id_pasien = $id_pasien;
			$id_campak = $id_campak;
			$nama_pasien = Input::get('nama_anak');
			$usia = Input::get('tgl_tahun');
			 
			if(Input::get('klasifikasi_final')==0)
			{
				$klasifikasi_final = 'Campak(Lab)';
			}
			elseif (Input::get('klasifikasi_final')==1) 
			{
				$klasifikasi_final = 'Campak(Epid)';
			} 
			elseif (Input::get('klasifikasi_final')==2) 
			{
				$klasifikasi_final = 'Campak(Klinis)';
			}
			elseif (Input::get('klasifikasi_final')==3) 
			{
				$klasifikasi_final = 'Rubella';
			}
			elseif (Input::get('klasifikasi_final')==4) 
			{
				$klasifikasi_final = 'Bukan campak/rubella';
			}
			
			$keadaan_akhir = (Input::get('keadaan_akhir')==0) ? 'Hidup/Sehat' : 'Meninggal' ;
			$msg = "tersimpan";

				return Response::json(compact('msg','no_epid','nama_pasien','id_pasien','usia','id_campak','klasifikasi_final','keadaan_akhir'));
			}
		
		
	}

	public function simpanUjiCampak() {
		
		//simpan hasil laboratorium
		DB::table('hasil_uji_lab_campak')->insert(
			array(
				'id_hasil_uji_lab_campak'=>Input::get('id_hasil_uji_lab_campak'),
				'id_pasien'=>Input::get('id_pasien'),
				'id_campak'=>Input::get('id_campak'),
				'hasil_spesimen_darah'=>Input::get('hasil_spesimen_darah'),
				'hasil_spesimen_urin'=>Input::get('hasil_spesimen_urin'),
				'klasifikasi_final'=>Input::get('klasifikasi_final'),
				'tanggal_uji_laboratorium'=>Input::get('tanggal_uji_laboratorium')
			));
	}

	

	/**
	 * Display the specified Campak.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$campak = Campak::findOrFail($id);

		return View::make('pemeriksaan.campak.show', compact('campak'));
	}

	/**
	 * Show the form for editing the specified Campak.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$campak = Campak::find($id);

		return View::make('pemeriksaan.campak.edit', compact('campak'));
	}

	/**
	 * Update the specified Campak in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
		
		DB::update(" 

			update pasien,campak,hasil_uji_lab_campak
					set 
					campak.no_epid='".Input::get('no_epid')."',
					campak.vaksin_campak_sebelum_sakit='".Input::get('vaksin_campak_sebelum_sakit')."',
					campak.tanggal_timbul_demam=CAST('".Input::get('tanggal_timbul_demam')."' AS DATETIME),
					campak.tanggal_timbul_rash=CAST('".Input::get('tanggal_timbul_rash')."' AS DATETIME),
					campak.tanggal_diambil_spesimen_darah=CAST('".Input::get('tanggal_diambil_spesimen_darah')."' AS DATETIME),
					campak.tanggal_diambil_spesimen_urin=CAST('".Input::get('tanggal_diambil_spesimen_urin')."' AS DATETIME),
					campak.tanggal_diambil_spesimen_oral_fluid=CAST('".Input::get('tanggal_diambil_spesimen_oral_fluid')."' AS DATETIME),
					campak.vitamin_A='".Input::get('vitamin_A')."',
					campak.nama_puskesmas='".Input::get('nama_puskesmas')."',
					campak.jenis_pemeriksaan='".Input::get('jenis_pemeriksaan')."',
					campak.tipe_spesimen='".Input::get('tipe_spesimen')."',
					campak.keadaan_akhir='".Input::get('keadaan_akhir')."',
					campak.hasil_igm_campak='".Input::get('hasil_igm_campak')."',
					campak.hasil_igm_rubella='".Input::get('hasil_igm_rubella')."',
					campak.hasil_isolasi_virus='".Input::get('hasil_isolasi_virus')."',
					campak.klasifikasi_final='".Input::get('klasifikasi_final')."',
					campak.tanggal_laporan_diterima=CAST('".Input::get('tanggal_laporan_diterima')."' AS DATETIME),
					campak.tanggal_pelacakan=CAST('".Input::get('tanggal_pelacakan')."' AS DATETIME),
					pasien.nik='".Input::get('nik')."',
					pasien.nama_anak='".Input::get('nama_anak')."',
					pasien.nama_ortu='".Input::get('nama_ortu')."',
					pasien.alamat='".Input::get('alamat')."',
					pasien.tanggal_lahir=CAST('".Input::get('tanggal_lahir')."' AS DATETIME),
					pasien.umur='".Input::get('umur_tahun')."',
					pasien.umur_bln='".Input::get('umur_bln')."',
					pasien.umur_hr='".Input::get('umur_hr')."',
					pasien.jenis_kelamin='".Input::get('jenis_kelamin')."',
					pasien.id_kelurahan='".Input::get('id_kelurahan')."' 
					where pasien.id_pasien=hasil_uji_lab_campak.id_pasien and campak.id_campak=hasil_uji_lab_campak.id_campak and hasil_uji_lab_campak.id_hasil_uji_lab_campak='".Input::get('id_hasil_uji_lab_campak')."'");

		return Redirect::to('campak');
	}

	/**
	 * Remove the specified Campak from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Campak::destroy($id);

		return Redirect::route('pemeriksaan.campak.index');
	}

	public function postForm()
	{
		$id = Input::get('id');
		if($id==1) 
		{
			return View::make('diagnosa.campak');
		}
	}

	public function getEdit($id) {

		$campak = DB::select("SELECT 
								campak.no_epid as no_epid,
								pasien.nik,
								pasien.tanggal_lahir, 
								pasien.nama_anak, 
								pasien.nama_ortu, 
								pasien.umur_bln,
								pasien.umur_hr,
								pasien.alamat,
								pasien.id_kelurahan,
								lokasi.kelurahan,
								lokasi.kecamatan,
								lokasi.kabupaten,
								lokasi.provinsi,
								CONCAT_WS(',',pasien.alamat,lokasi.kelurahan,lokasi.kecamatan,lokasi.kabupaten,lokasi.provinsi) as alamat_pasien, 
								pasien.umur, 
								pasien.jenis_kelamin, 
								campak.vaksin_campak_sebelum_sakit, 
								campak.tanggal_timbul_demam, 
								campak.tanggal_timbul_rash, 
								campak.tanggal_diambil_spesimen_darah, 
								campak.keadaan_akhir, 
								campak.vitamin_A, 
								campak.tanggal_diambil_spesimen_urin, 
								campak.tanggal_periksa, 
								campak.tanggal_laporan_diterima, 
								campak.tanggal_pelacakan,
								campak.tanggal_diambil_spesimen_oral_fluid,
								hasil_uji_lab_campak.id_hasil_uji_lab_campak,
								campak.tipe_spesimen,
								hasil_uji_lab_campak.no_spesimen,
								hasil_uji_lab_campak.hasil_spesimen_darah, 
								hasil_uji_lab_campak.hasil_spesimen_urin, 
								campak.nama_puskesmas, 
								campak.klasifikasi_final,
								hasil_uji_lab_campak.tanggal_uji_laboratorium,
								campak.jenis_pemeriksaan,
								campak.hasil_igm_campak,
								campak.hasil_igm_rubella,
								campak.hasil_isolasi_virus,
								hasil_uji_lab_campak.id_uji_lab
							FROM
								campak,
								pasien,
								lokasi,
								hasil_uji_lab_campak,
								users
							WHERE
								pasien.id_pasien=hasil_uji_lab_campak.id_pasien
							AND 
								lokasi.id_kelurahan=pasien.id_kelurahan
							AND 
								campak.id_campak=hasil_uji_lab_campak.id_campak
							AND
								users.email ='".Sentry::getUser()->email."'
							AND
								campak.no_epid ='".$id."'");
		return View::make('pemeriksaan.campak.edit',compact('campak'));
	}

	public function getHapus($id,$id_lab) {

		try {

			DB::beginTransaction();
			DB::delete("delete from hasil_uji_lab_campak where hasil_uji_lab_campak.id_hasil_uji_lab_campak='".$id_lab."'");
			DB::delete("delete from campak where campak.no_epid ='".$id."'");
			DB::commit();
		} catch (Exception $e) {
			
			DB::rollback();

		}
		return Redirect::to('campak');
		
	}

	public function laporancampak() 
	{
		$pdf = PDF::loadView('pemeriksaan.campak.cetak');
		return $pdf->stream();
	}

	//fungsi cetak laporan kasus individual campak
	public function getCetakCampak() {

		return View::make('pemeriksaan.campak.cetak1');
	}

	//fungsi untuk mengeload data campak,
	public function getDataCampak() {
		
		$data = Campak::pasiencampak();
		return View::make('pemeriksaan.campak.datacampak',compact('data'));
	}
	public function detailcampak($id) {

		//$id = Input::get('no_epid');
		$detail = DB::select("SELECT 
								campak.no_epid,
								pasien.nik,
								pasien.tanggal_lahir, 
								pasien.nama_anak, 
								pasien.nama_ortu, 
								CONCAT_WS(',',pasien.alamat,lokasi.kelurahan,lokasi.kecamatan,lokasi.kabupaten,lokasi.provinsi) as alamat_pasien, 
								pasien.umur, 
								pasien.jenis_kelamin, 
								campak.vaksin_campak_sebelum_sakit, 
								campak.tanggal_timbul_demam, 
								campak.tanggal_timbul_rash, 
								campak.tanggal_diambil_spesimen_darah, 
								campak.keadaan_akhir, 
								campak.vitamin_A, 
								campak.tanggal_diambil_spesimen_urin, 
								campak.tanggal_periksa, 
								campak.tanggal_laporan_diterima, 
								campak.tanggal_pelacakan,
								hasil_uji_lab_campak.no_spesimen,
								hasil_uji_lab_campak.hasil_spesimen_darah, 
								hasil_uji_lab_campak.hasil_spesimen_urin,  
								hasil_uji_lab_campak.klasifikasi_final, 								
								hasil_uji_lab_campak.tanggal_uji_laboratorium,
								hasil_uji_lab_campak.id_uji_lab
							FROM
								campak,
								pasien,
								lokasi,
								hasil_uji_lab_campak,
								users
							WHERE
								pasien.id_pasien=hasil_uji_lab_campak.id_pasien
							AND 
								lokasi.id_kelurahan=pasien.id_kelurahan
							AND 
								campak.id_campak=hasil_uji_lab_campak.id_campak
							AND
								users.email ='".Sentry::getUser()->email."'
							AND
								campak.no_epid ='".$id."'");

		return View::make('pemeriksaan.campak.detail',compact('detail'));
	}



	public function getUmur() {

		$y = date('Y');
		$m = date('m');
		$d = date('d');

		$tanggal_lahir = Input::get('tanggal_lahir');
		list($thn_skrg, $bln_skrg, $tgl_skrg) = explode('-',$tanggal_lahir);
		//$data = Pasien::umur($tanggal_lahir);
		// kelahiran dijadikan dalam format tanggal semua
		$lahir = $tgl_skrg+($bln_skrg*30)+($thn_skrg*365);
		// sekarang dijadikan dalam format tanggal semua
		$now = $d+($m*30)+($y*365);
 
		// dari format tanggal jadikan tahun, bulan, hari
		$tahun =($now-$lahir)/365;
	   	$bulan =(($now-$lahir)%365)/30;
   		$hari  =(($now-$lahir)%365)%30;	
 
		$tgl_tahun=floor($tahun);
		$tgl_bulan=floor($bulan);
		$tgl_hari=floor($hari);
		
		return Response::json(compact('tgl_tahun','tgl_bulan','tgl_hari'));
	}

	//fungsi untuk mendapatkan no epid campak secara otomatis
	public function getEpid() {
		$id_kel = Input::get('id_kelurahan');
		$data2=DB::select("select no_epid from campak,pasien,hasil_uji_lab_campak where pasien.id_pasien=hasil_uji_lab_campak.id_pasien and campak.id_campak=hasil_uji_lab_campak.id_campak and id_kelurahan='".$id_kel."' order by campak.id_campak limit 0,1");
        $cek = DB::select("select RIGHT(no_epid,3) from campak,pasien,hasil_uji_lab_campak where MID(no_epid,9,2) ='".date('y')."' and pasien.id_pasien=hasil_uji_lab_campak.id_pasien and campak.id_campak=hasil_uji_lab_campak.id_campak and pasien.id_kelurahan='".$id_kel."' order by campak.id_campak limit 0,1");
        if(empty($cek)) {
            $code=substr($id_kel,0,7).date('y');
            $caracter = 'C';
            $epid = '001';
           	$epid =$caracter.$code.$epid;
        }
        else {
            foreach ($data2 as $result) {
                $kode = substr($result->no_epid,0,8).date('y');
                $noUrut = (int) substr($result->no_epid,10,4);
                $noUrut++;
                $epid = sprintf("%03s", $noUrut);
            }
            $epid=$kode.$epid;  
        }

        return $epid;
	}

	public function pasien() {

		$no_epid = Input::get('no_epid');
		$data = DB::select("select pasien.id_pasien as id_pasien,nama_anak,nama_ortu,alamat,jenis_kelamin,tanggal_lahir,umur,umur_bln,umur_hr,lokasi.provinsi,lokasi.kabupaten,lokasi.kecamatan,lokasi.kelurahan from pasien,campak,hasil_uji_lab_campak,lokasi
							where 
							pasien.id_pasien=hasil_uji_lab_campak.id_pasien
							and campak.id_campak=hasil_uji_lab_campak.id_campak
							and pasien.id_kelurahan=lokasi.id_kelurahan
							and campak.no_epid='".$no_epid."'");
		foreach ($data as $value) {
			$id_pasien = $value->id_pasien;
			$nama_anak = $value->nama_anak;
			$nama_ortu = $value->nama_ortu;
			$alamat = $value->alamat;
			$jenis_kelamin = $value->jenis_kelamin;
			$tanggal_lahir = $value->tanggal_lahir;
			$umur = $value->umur;
			$umur_bln = $value->umur_bln;
			$umur_hr = $value->umur_hr;
			$privinsi = $value->provinsi;
			$kabupaten = $value->kabupaten;
			$kecamatan = $value->kecamatan;
			$kelurahan = $value->kelurahan;
		}
		return Response::json(compact('id_pasien','nama_anak','nama_ortu','alamat','jenis_kelamin','tanggal_lahir','umur','umur_bln','umur_hr','provinsi','kabupaten','kecamatan','kelurahan'));
	}

	//route daftar campak
	public function getDaftarPeCampak() {

		$pe_campak = Campak::getPeCampak();
		return View::make('pemeriksaan.campak.daftar',compact('pe_campak'));
	}

	//fungsi untuk mengambil data pasien yang akan dilakukan penyelidikan epidemologi campak
	public function getEntri_campak($no_epid) {
		
		$pe_campak = DB::select("SELECT 
								campak.no_epid,
								pasien.nik,
								pasien.id_pasien,
								pasien.tanggal_lahir, 
								pasien.nama_anak, 
								pasien.nama_ortu, 
								pasien.alamat,
								lokasi.kelurahan,
								lokasi.kecamatan,
								lokasi.kabupaten,
								lokasi.provinsi,
								lokasi.id_kelurahan, 
								pasien.umur, 
								pasien.umur_bln,
								pasien.umur_hr,
								pasien.jenis_kelamin
							FROM
								campak,
								pasien,
								lokasi,
								hasil_uji_lab_campak,
								users
							WHERE
								pasien.id_pasien=hasil_uji_lab_campak.id_pasien
							AND 
								lokasi.id_kelurahan=pasien.id_kelurahan
							AND 
								campak.id_campak=hasil_uji_lab_campak.id_campak
							AND
								users.email ='".Sentry::getUser()->email."'
							AND
								campak.no_epid ='".$no_epid."'");

		return View::make('pemeriksaan.campak.pe_campak',compact('pe_campak'));
	}

	//fungsi untuk menyimpan data kasus penyelidikan epidemologi campak
	public function postEpidCampak() {

		$no_epid = Input::get('no_epid');
		$cek=DB::select("select no_epid from campak,pasien,hasil_uji_lab_campak where campak.id_campak=hasil_uji_lab_campak.id_campak and pasien.id_pasien=hasil_uji_lab_campak.id_pasien and campak.no_epid='".$no_epid."'");
		if(empty($cek)){
			//simpan data personal
			$data = 
			array(
			DB::table('campak')->insert(
				array('no_epid' => Input::get('no_epid')))
			,
			DB::table('pasien')->insert(
				array(
					'nama_anak'=>Input::get('nama_anak'),
					'alamat'=>Input::get('alamat'),
					'tanggal_lahir'=>Input::get('tanggal_lahir'),
					'umur'=>Input::get('tgl_tahun'),
					'umur_bln'=>Input::get('tgl_bulan'),
					'umur_hr'=>Input::get('tgl_hari'),
					'jenis_kelamin'=>Input::get('jenis_kelamin'),
					'id_kelurahan'=>Input::get('id_kelurahan')
				)));
			if($data) {
				$no_epid =$no_epid;
				$id_pasien				=DB::table('pasien')->max('id_pasien');
				$gejala_umum			=Input::get('gejala_umum');
				$tanggal_timbul_gejala	=Input::get('tanggal_timbul_gejala');
				$komplikasi				=Input::get('komplikasi');
				$catatan_komplikasi		=Input::get('catatan_komplikasi');
				$waktu_pengobatan		=Input::get('waktu_pengobatan');
				$tempat_pengobatan		=Input::get('tempat_pengobatan');
				$obat_yang_diberikan		=Input::get('obat_yang_diberikan');
				$dirumah_orang_sakit_sama	=Input::get('dirumah_orang_sakit_sama');
				$kapan_sakit_dirumah		=Input::get('kapan_sakit_dirumah');
				$disekolah_orang_sakit_sama	=Input::get('disekolah_orang_sakit_sama');
				$kapan_sakit_disekolah		=Input::get('kapan_sakit_disekolah');
				$kekurangan_gizi					=Input::get('kekurangan_gizi');
				$imunisasi_campak_sudah_diberikan	=Input::get('imunisasi_campak_sudah_diberikan');
				$usia_terakhir_imunisasi			=Input::get('usia_terakhir_imunisasi');
				$sediaan							=Input::get('sediaan');
				$hasil_lab							=Input::get('hasil_lab');
				$tanggal_penyelidikan				=Input::get('tanggal_penyelidikan');
				$pelaksana							=Input::get('pelaksana');

				$save= Campak::epid_campak($id_pasien,$gejala_umum,$tanggal_timbul_gejala,$komplikasi,$catatan_komplikasi,$waktu_pengobatan,$tempat_pengobatan,$obat_yang_diberikan,$dirumah_orang_sakit_sama,$kapan_sakit_dirumah,$disekolah_orang_sakit_sama,$kapan_sakit_disekolah,$kekurangan_gizi,$imunisasi_campak_sudah_diberikan,$usia_terakhir_imunisasi,$sediaan,$hasil_lab,$tanggal_penyelidikan,$pelaksana); 
				
				$cari = DB::select("select id_campak from campak where no_epid='".Input::get('no_epid')."'");
				foreach ($cari as $value) {
					$id_campak =$value->id_campak;
				}
				DB::table('hasil_uji_lab_campak')->insert(
				array(
					'id_campak'=> $id_campak,
					'id_pasien'=>DB::table('pasien')->max('id_pasien')
				)); 
			
				return Redirect::to('campak');
			}
		} else {
			$id_pasien				=Input::get('id_pasien');
			$gejala_umum			=Input::get('gejala_umum');
			$tanggal_timbul_gejala	=Input::get('tanggal_timbul_gejala');
			$komplikasi				=Input::get('komplikasi');
			$no_epid				=Input::get('no_epid');
			$catatan_komplikasi		=Input::get('catatan_komplikasi');
			$waktu_pengobatan		=Input::get('waktu_pengobatan');
			$tempat_pengobatan		=Input::get('tempat_pengobatan');
			$obat_yang_diberikan		=Input::get('obat_yang_diberikan');
			$dirumah_orang_sakit_sama	=Input::get('dirumah_orang_sakit_sama');
			$kapan_sakit_dirumah		=Input::get('kapan_sakit_dirumah');
			$disekolah_orang_sakit_sama	=Input::get('disekolah_orang_sakit_sama');
			$kapan_sakit_disekolah		=Input::get('kapan_sakit_disekolah');
			$kekurangan_gizi					=Input::get('kekurangan_gizi');
			$imunisasi_campak_sudah_diberikan	=Input::get('imunisasi_campak_sudah_diberikan');
			$usia_terakhir_imunisasi			=Input::get('usia_terakhir_imunisasi');
			$sediaan							=Input::get('sediaan');
			$hasil_lab							=Input::get('hasil_lab');
			$tanggal_penyelidikan				=Input::get('tanggal_penyelidikan');
			$pelaksana							=Input::get('pelaksana');

			$save= Campak::epid_campak($id_pasien,$gejala_umum,$tanggal_timbul_gejala,$komplikasi,$no_epid,$catatan_komplikasi,$waktu_pengobatan,$tempat_pengobatan,$obat_yang_diberikan,$dirumah_orang_sakit_sama,$kapan_sakit_dirumah,$disekolah_orang_sakit_sama,$kapan_sakit_disekolah,$kekurangan_gizi,$imunisasi_campak_sudah_diberikan,$usia_terakhir_imunisasi,$sediaan,$hasil_lab,$tanggal_penyelidikan,$pelaksana); 

			return Redirect::to('campak');
		}

		
	}


	public function getDetailPeCampak($no_epid) {

		$data = DB::select("select pasien.id_pasien, pasien.nik, pasien.nama_anak, pasien.nama_ortu, CONCAT_WS(',',pasien.alamat,lokasi.kelurahan,lokasi.kecamatan,lokasi.kabupaten,lokasi.provinsi) as alamat,  pasien.tanggal_lahir, pasien.umur, pasien.umur_bln, pasien.umur_hr, pasien.jenis_kelamin, pasien.id_kelurahan, pe_campak.gejala_umum, pe_campak.tanggal_timbul_gejala, pe_campak.komplikasi, pe_campak.catatan_komplikasi, pe_campak.waktu_pengobatan, pe_campak.tempat_pengobatan, pe_campak.obat_yang_diberikan, pe_campak.no_epid, pe_campak.dirumah_orang_sakit_sama, pe_campak.kapan_sakit_dirumah, pe_campak.disekolah_orang_sakit_sama, pe_campak.kapan_sakit_disekolah, pe_campak.kekurangan_gizi, pe_campak.imunisasi_campak_sudah_diberikan, pe_campak.usia_terakhir_imunisasi, pe_campak.sediaan, pe_campak.hasil_lab, pe_campak.tanggal_penyelidikan, pe_campak.pelaksana 
							from pasien,lokasi,pe_campak where pasien.id_pasien=pe_campak.id_pasien and pasien.id_kelurahan=lokasi.id_kelurahan and pe_campak.no_epid='".$no_epid."' order by created_at asc");
		return View::make('pemeriksaan.campak.detail_pe_campak',compact('data'));
	}

	public function getEditPeCampak($no_epid) {

		$data = Campak::getEditPeCampak($no_epid);
		return View::make('pemeriksaan.campak.edit_pe_campak',compact('data'));

	}

	public function postUpdatePeCampak() {

		DB::update("update
						pasien,
						pe_campak
					set 
						pasien.nama_anak='".Input::get('nama_anak')."', 
						pasien.nama_ortu='".Input::get('nama_ortu')."', 
						pasien.alamat='".Input::get('alamat')."', 
						pasien.tanggal_lahir='".Input::get('tanggal_lahir')."',
						pasien.umur='".Input::get('tgl_tahun')."', 
						pasien.umur_bln='".Input::get('tgl_bulan')."', 
						pasien.umur_hr='".Input::get('tgl_hari')."', 
						pasien.jenis_kelamin='".Input::get('jenis_kelamin')."', 
						pasien.id_kelurahan='".Input::get('id_kelurahan')."', 
						pe_campak.gejala_umum='".Input::get('gejala_umum')."', 
						pe_campak.tanggal_timbul_gejala='".Input::get('tanggal_timbul_gejala')."', 
						pe_campak.komplikasi='".Input::get('komplikasi')."', 
						pe_campak.catatan_komplikasi='".Input::get('catatan_komplikasi')."', 
						pe_campak.waktu_pengobatan='".Input::get('waktu_pengobatan')."', 
						pe_campak.tempat_pengobatan='".Input::get('tempat_pengobatan')."', 
						pe_campak.obat_yang_diberikan='".Input::get('obat_yang_diberikan')."', 
						pe_campak.no_epid='".Input::get('no_epid')."', 
						pe_campak.dirumah_orang_sakit_sama='".Input::get('dirumah_orang_sakit_sama')."', 
						pe_campak.kapan_sakit_dirumah='".Input::get('kapan_sakit_dirumah')."', 
						pe_campak.disekolah_orang_sakit_sama='".Input::get('disekolah_orang_sakit_sama')."', 
						pe_campak.kapan_sakit_disekolah='".Input::get('kapan_sakit_disekolah')."', 
						pe_campak.kekurangan_gizi='".Input::get('kekurangan_gizi')."', 
						pe_campak.imunisasi_campak_sudah_diberikan='".Input::get('imunisasi_campak_sudah_diberikan')."', 
						pe_campak.usia_terakhir_imunisasi='".Input::get('usia_terakhir_imunisasi')."', 
						pe_campak.sediaan='".Input::get('sediaan')."', 
						pe_campak.hasil_lab='".Input::get('hasil_lab')."', 
						pe_campak.tanggal_penyelidikan='".Input::get('tanggal_penyelidikan')."', 
						pe_campak.pelaksana='".Input::get('pelaksana')."'
					where 
						pasien.id_pasien=pe_campak.id_pasien  
					and 
						pe_campak.id='".Input::get('id')."'");
		return Redirect::to('campak');
	}
 	
 	public function getTampilDataCampak() {

 		
 	}
	//fungsi untuk menghapus pe campak
	public function getHapusPeCampak($no_epid) {

		try {

			DB::beginTransaction();
			DB::delete("delete from pe_campak where pe_campak.no_epid='".$no_epid."'");
			DB::commit();
		} catch (Exception $e) {
			
			DB::rollback();

		}
		return Redirect::to('campak');
	}

	
	
}
