<?php

//controller untuk laboratorium

class LaboratoriumController extends BaseController
{
    //load view
    protected $layout = 'layouts.master';

    public function __construct()
    {
        //filter
        $this->beforeFilter('auth');
    }

    //fungsi untuk index
    public function getIndex()
    {
        /*if(Datatable::shouldHandle())
        {
            return Datatable::collection(Author::all(array('id','name')))
            ->showColumns('nama_laboratorium', 'nama_petugas', 'telepon', 'email', 'alamat')
            ->addColumn('', function ($model) {
                return 'edit | hapus';
            })
            ->searchColumns('nama_laboratorium')
            ->orderColumns('nama_laboratorium')
            ->make();
        }*/
        /*$laboratorium = Laboratorium::getLab();
        $jml = Laboratorium::getSumLab();
        $this->layout->content = View::make('laboratorium.index',compact('laboratorium'))
        ->with('jml',$jml);*/
        /*$ujilab = DB::select('select
                                no_epid,p_epid.nik,tgl_rash,tgl_sakit,tgl_ambil_serum,tgl_ambil_urin,
                                tgl_pelacakan,tgl_vaksinasi,vitamin_A,dosis_vaksinasi,keadaan_akhir,p_epid.klasifikasi_final
                                from p_epid,pasien,users,laboratorium
                                where p_epid.nik=pasien.nik and p_epid.id_lab=laboratorium.id_lab and laboratorium.id_user="'.Sentry::getUser()->id.'"
                                and users.id = laboratorium.id_user');*/

        $this->layout->content = View::make('beranda.index1');
    }

    public function getListlab()
    {
        $laboratorium = Laboratorium::getLab();

        return View::make('laboratorium.list', compact('laboratorium'));
    }
    //fungsi untuk create
    public function getCreate()
    {
        $this->layout->contet = View::make('laboratorium.create');
    }

    //fungsi load data sesuai id untuk di edit
    public function getEdit($id)
    {
        $laboratorium = Laboratorium::find($id);

        $this->layout->contet = View::make('laboratorium.edit')->with('laboratorium', $laboratorium);
    }

    //fungsi simpan data
    public function postSimpan()
    {
        $validator = Validator::make($data = Input::all(), Laboratorium::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        Laboratorium::create($data);

        return Redirect::to('laboratorium');
    }

    /**
     * Update the specified kabupaten in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function postUpdate($id)
    {
        $laboratorium = Laboratorium::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Laboratorium::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $laboratorium->update($data);

        return Redirect::to('laboratorium');
    }

    /**
     * Remove the specified kabupaten from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function getDestroy($id)
    {
        Laboratorium::destroy($id);

        return Redirect::to('laboratorium');
    }

    //load halaman untuk memasukan data hasil laboratorium
    public function getUjilab()
    {
        return View::make('laboratorium.editspesimen');
    }

    //list akun pengguna lab
    public function getListAkun()
    {
        $lab = DB::select('select users.email,laboratorium.nama_laboratorium,laboratorium.nama_petugas,laboratorium.telepon
						from users,laboratorium,users_groups
						where users.id=laboratorium.id_user and users.id=users_groups.user_id and users_groups.group_id=2');

        return view::make('laboratorium.daftarpetugas', compact('lab'));
    }

    //load form tambah akun petugas
    public function getForm()
    {
        return View::make('laboratorium.daftarakun');
    }

    public function getDeleteLab($penyakit, $id)
    {
        if ($penyakit == 'crs') {
        }
    }

    public function getLabHapus($id)
    {
        //deleted :: change status_at to 0 for deleted
        $dt = [
            'status_lab' => 0,
            'deleted_by' => Sentry::getUser()->id,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];
        $delete = DB::table('hasil_uji_lab_campak')
                ->where('id_campak', $id)
                ->update($dt);
        DB::table('uji_sampel')->where('id_campak', $id)->delete();

        return Redirect::to('labs/campak');
    }

    public function getLabCrsHapus($id)
    {
        //deleted :: change status_at to 0 for deleted
        $dt = [
            'validity_lab' => null,
            'deleted_by' => Sentry::getUser()->id,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];
        $delete = DB::table('hasil_uji_lab_crs')
                ->where('id_crs', $id)
                ->update($dt);
        DB::table('uji_sampel')->where('id', $id)->delete();

        return Redirect::to('labs/crs');
    }

    public function getLabAfpHapus($id)
    {
        //deleted :: change status_at to 0 for deleted
        $dt = [
            'status_lab' => 0,
            'deleted_by' => Sentry::getUser()->id,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];
        $delete = DB::table('hasil_uji_lab_afp')
                ->where('id_afp', $id)
                ->update($dt);
        DB::table('uji_sampel')->where('id', $id)->delete();

        return Redirect::to('labs/afp');
    }

    public function getLabDetail($id)
    {
        $data = DB::table('getdetailcampak')
                ->where('campak_id_campak', $id)
                ->get();
        $dt = (empty($data[0])) ? '' : $data[0];

        return View::make('laboratorium.campak.detail', compact('dt'));
    }

    public function postDataLabCampak()
    {
        $id = Input::get('id_campak');
        $dtCampak = DB::table('getdetailcampak')
                ->where('campak_id_campak', $id)->get();
        $data = $dtCampak[0];
        $data->pasien_tanggal_lahir = Helper::getDate($data->pasien_tanggal_lahir);
        $data->campak_tanggal_imunisasi_terakhir = Helper::getDate($data->campak_tanggal_imunisasi_terakhir);
        $data->campak_tanggal_timbul_demam = Helper::getDate($data->campak_tanggal_timbul_demam);
        $data->campak_tanggal_timbul_rash = Helper::getDate($data->campak_tanggal_timbul_rash);
        $data->campak_tanggal_laporan_diterima = Helper::getDate($data->campak_tanggal_laporan_diterima);
        $data->campak_tanggal_pelacakan = Helper::getDate($data->campak_tanggal_pelacakan);
        $data->lab_tanggal_terima_spesimen = Helper::getDate($data->lab_tanggal_terima_spesimen);
        $dtGejala = DB::table('gejala_campak')
                    ->WHERE('id_campak', $id)->get();
        $gejala = $dtGejala;
        $dtKomplikasi = DB::table('komplikasi')
                    ->WHERE('id_campak', $id)->get();
        $komplikasi = $dtKomplikasi;
        $sampel = DB::table('uji_sampel')
                    ->WHERE('id_campak', $id)->get();

        return Response::json(compact('data', 'gejala', 'komplikasi', 'sampel'));
    }

    public function getDataLabCrs()
    {
        $id = Input::get('id');
        $dtCrs = DB::table('getdetailcrs')->where('crs_id_crs',$id)->get();
        $data = $dtCrs[0];
        $data->pelapor_tanggal_laporan = Helper::getDate($data->pelapor_tanggal_laporan);
        $data->pelapor_tanggal_investigasi = Helper::getDate($data->pelapor_tanggal_investigasi);
        $data->pasien_tanggal_lahir = Helper::getDate($data->pasien_tanggal_lahir);
        $data->rs_tgl_ambil_serum1 = Helper::getDate($data->rs_tgl_ambil_serum1);
        $data->rs_tgl_kirim_serum1 = Helper::getDate($data->rs_tgl_kirim_serum1);
        $data->rs_tgl_tiba_serum1 = Helper::getDate($data->rs_tgl_tiba_serum1);
        $data->rs_tgl_ambil_serum2 = Helper::getDate($data->rs_tgl_ambil_serum2);
        $data->rs_tgl_kirim_serum2 = Helper::getDate($data->rs_tgl_kirim_serum2);
        $data->rs_tgl_tiba_serum2 = Helper::getDate($data->rs_tgl_tiba_serum2);
        $data->rs_tgl_ambil_throat_swab = Helper::getDate($data->rs_tgl_ambil_throat_swab);
        $data->rs_tgl_kirim_throat_swab = Helper::getDate($data->rs_tgl_kirim_throat_swab);
        $data->rs_tgl_tiba_throat_swab = Helper::getDate($data->rs_tgl_tiba_throat_swab);
        $data->rs_tgl_ambil_urine = Helper::getDate($data->rs_tgl_ambil_urine);
        $data->rs_tgl_kirim_urine = Helper::getDate($data->rs_tgl_kirim_urine);
        $data->rs_tgl_tiba_urine = Helper::getDate($data->rs_tgl_tiba_urine);
        $data->rs_tgl_igm_serum1 = Helper::getDate($data->rs_tgl_igm_serum1);
        $data->rs_tgl_igm_serum2 = Helper::getDate($data->rs_tgl_igm_serum2);
        $data->rs_tgl_igg_serum1 = Helper::getDate($data->rs_tgl_igg_serum1);
        $data->rs_tgl_igg_serum2 = Helper::getDate($data->rs_tgl_igg_serum2);
        $data->rs_tgl_isolasi = Helper::getDate($data->rs_tgl_isolasi);

        $data->lab_tgl_ambil_serum1 = Helper::getDate($data->lab_tgl_ambil_serum1);
        $data->lab_tgl_kirim_serum1 = Helper::getDate($data->lab_tgl_kirim_serum1);
        $data->lab_tgl_tiba_serum1 = Helper::getDate($data->lab_tgl_tiba_serum1);
        $data->lab_tgl_ambil_serum2 = Helper::getDate($data->lab_tgl_ambil_serum2);
        $data->lab_tgl_kirim_serum2 = Helper::getDate($data->lab_tgl_kirim_serum2);
        $data->lab_tgl_tiba_serum2 = Helper::getDate($data->lab_tgl_tiba_serum2);
        $data->lab_tgl_ambil_throat_swab = Helper::getDate($data->lab_tgl_ambil_throat_swab);
        $data->lab_tgl_kirim_throat_swab = Helper::getDate($data->lab_tgl_kirim_throat_swab);
        $data->lab_tgl_tiba_throat_swab = Helper::getDate($data->lab_tgl_tiba_throat_swab);
        $data->lab_tgl_ambil_urine = Helper::getDate($data->lab_tgl_ambil_urine);
        $data->lab_tgl_kirim_urine = Helper::getDate($data->lab_tgl_kirim_urine);
        $data->lab_tgl_tiba_urine = Helper::getDate($data->lab_tgl_tiba_urine);
        $data->lab_tgl_igm_serum1 = Helper::getDate($data->lab_tgl_igm_serum1);
        $data->lab_tgl_igm_serum2 = Helper::getDate($data->lab_tgl_igm_serum2);
        $data->lab_tgl_igg_serum1 = Helper::getDate($data->lab_tgl_igg_serum1);
        $data->lab_tgl_igg_serum2 = Helper::getDate($data->lab_tgl_igg_serum2);
        $data->lab_tgl_isolasi = Helper::getDate($data->lab_tgl_isolasi);
        $data->crs_tgl_mulai_sakit = Helper::getDate($data->crs_tgl_mulai_sakit);

        return Response::json(compact('data'));
    }

    //simpan akun lab
    public function postDaftarAkun()
    {
        /*$validator = Validator::make($data = Input::all(), Subkabupaten::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }*/

        // Membuat user regular baru
        $user = Sentry::register(array(
        // silahkan ganti sesuai keinginan
        'email' => Input::get('username'),
        'password' => Input::get('password'),
        ), true); // langsung diaktivasi
        // Cari grup regular
        $regularGroup = Sentry::findGroupByName('regular');
        // Masukkan user ke grup regular
        $user->addGroup($regularGroup);
        //ambil id yang terakhir masuk
        //$sql = DB::statement('select max(id) from users');
        /*foreach(DB::select("select max(id) as id from users") as $id){
            $id=$id->id;
        }*/
        /*//simpan ke table sub_kabupaten
        DB::table('laboratorium')->insert(array(
            'id_kab'=>Input::get('id_kab'),
            'id_user'=>$id));*/

        return Redirect::route('kabupaten.ListAkun');
    }

    public function labCampak()
    {
        $data = Laboratorium::getDaftarSampel();

        return View::make('laboratorium.listcampak', compact('data'));
    }

    public function getDetailSampel()
    {
        $id_sampel = Input::get('id_sampel');
        $dt = DB::table('uji_sampel AS a')->WHERE('a.id_sampel', $id_sampel)->get();
        $data = (empty($dt)) ? '' : $dt[0];

        return Response::json(compact('data'));
    }

    public function labs($case)
    {
        switch ($case) {
            case 'crs':
                $response['title'] = 'CRS';
                $response['daftarSampel'] = 'laboratorium.crs.daftarSampel';
                break;
            case 'campak':
                $response['title'] = 'Campak';
                $response['daftarSampel'] = 'laboratorium.campak.daftarSampel';
                break;
            case 'afp':
                $response['title'] = 'AFP';
                $response['daftarSampel'] = 'laboratorium.afp.daftarSampel';
                break;
            default:
                $response['title'] = '';break;
        }
        $response['data'] = Laboratorium::getListSampel($case);

        return View::make('laboratorium.index', compact('response'));
    }

    public function labDifteri()
    {
        Session::put('sess_id', '');
        $tersangka = DB::select("
								SELECT
									difteri.no_epid,pasien.nama_anak,pasien.tanggal_lahir,
									pasien.jenis_kelamin,difteri.tanggal_diambil_spesimen_tenggorokan,difteri.tanggal_diambil_spesimen_hidung
								FROM
									pasien,difteri,users,laboratorium,hasil_uji_lab_difteri
								WHERE
									users.id_user=laboratorium.id_laboratorium
								AND
									pasien.id_pasien=hasil_uji_lab_difteri.id_pasien
								AND
									difteri.id_difteri=hasil_uji_lab_difteri.id_difteri
								AND
									difteri.id_laboratorium = laboratorium.id_laboratorium
								AND
									users.email='".Sentry::getUser()->email."'
								AND
										hasil_uji_lab_difteri.status=0
								GROUP BY
									pasien.id_pasien");

        return View::make('laboratorium.listdifteri', compact('tersangka'));
    }

    public function labAFP()
    {
        $data = '';

        return View::make('laboratorium.listafp', compact('data'));
    }

    public function labCRS()
    {
        $data = '';

        return View::make('laboratorium.listcrs', compact('data'));
    }

    public function editSpesimenCampak($id)
    {
        $tersangka = DB::select("
								SELECT
									campak.no_epid,hasil_uji_lab_campak.id_pasien,hasil_uji_lab_campak.id_campak,pasien.nama_anak,pasien.umur,pasien.tanggal_lahir,
									pasien.jenis_kelamin,campak.tanggal_diambil_spesimen_darah,campak.tanggal_diambil_spesimen_urin
								FROM
									pasien,campak,users,laboratorium,hasil_uji_lab_campak
								WHERE
									users.id_user=laboratorium.id_laboratorium
								AND
									pasien.id_pasien=hasil_uji_lab_campak.id_pasien
								AND
									campak.id_campak=hasil_uji_lab_campak.id_campak
								AND
									campak.id_laboratorium = laboratorium.id_laboratorium
								AND
									users.email='".Sentry::getUser()->email."'
								AND
									campak.no_epid = '".$id."'
								");

        return View::make('laboratorium.editspesimencampak', compact('tersangka'));
    }

    public function editSpesimenDifteri($id)
    {
        $tersangka = DB::select("
								SELECT
									difteri.no_epid,hasil_uji_lab_difteri.id_pasien,hasil_uji_lab_difteri.id_difteri,pasien.nama_anak,pasien.tanggal_lahir,
									pasien.jenis_kelamin,pasien.umur,difteri.tanggal_diambil_spesimen_tenggorokan,difteri.tanggal_diambil_spesimen_hidung
								FROM
									pasien,difteri,users,laboratorium,hasil_uji_lab_difteri
								WHERE
									users.id_user=laboratorium.id_laboratorium
								AND
									pasien.id_pasien=hasil_uji_lab_difteri.id_pasien
								AND
									difteri.id_difteri=hasil_uji_lab_difteri.id_difteri
								AND
									difteri.id_laboratorium = laboratorium.id_laboratorium
								AND
									users.email='".Sentry::getUser()->email."'
								AND
									difteri.no_epid='".$id."'");

        return View::make('laboratorium.editspesimendifteri', compact('tersangka'));
    }

    public function editSpesimenAfp($id)
    {
        $tersangka = DB::select("
								SELECT
									afp.no_epid,hasil_uji_lab_afp.id_pasien,hasil_uji_lab_afp.id_afp,pasien.nama_anak,pasien.tanggal_lahir,
									pasien.jenis_kelamin,pasien.umur,afp.jenis_spesimen,afp.tanggal_pengambilan_spesimen
								FROM
									pasien,afp,users,laboratorium,hasil_uji_lab_afp
								WHERE
									users.id_user=laboratorium.id_laboratorium
								AND
									pasien.id_pasien=hasil_uji_lab_afp.id_pasien
								AND
									afp.id_afp=hasil_uji_lab_afp.id_afp
								AND
									afp.id_laboratorium = laboratorium.id_laboratorium
								AND
									users.email='".Sentry::getUser()->email."'
								AND
									afp.no_epid='".$id."'");

        return View::make('laboratorium.editspesimenafp', compact('tersangka'));
    }

    public function postSimpanHasilLabCampak()
    {
        echo "<pre>";print_r($_POST);echo "</pre>";die();
        $q = DB::select("select kode_lab from profil_laboratorium,laboratorium where profil_laboratorium.kode_lab=laboratorium.lab_code and profil_laboratorium.created_by='".Sentry::getUser()->id."'");
        $id_uji_lab = $q[0]->kode_lab;
        $id_pasien = Input::get('id_pasien');
        $id_campak = Input::get('id_campak');
        $no_spesimen = Input::get('no_spesimen');
        $hasil_darah = Input::get('hasil_spesimen_darah');
        $hasil_urin = Input::get('hasil_spesimen_urin');
        $kondisi_spesimen = Input::get('kondisi_spesimen');
        $hasil_pemeriksaan_campak = Input::get('hasil_pemeriksaan_campak');
        $hasil_pemeriksaan_rubella = Input::get('hasil_pemeriksaan_rubella');
        $hasil_pemeriksaan_bukan_campak_rubella = Input::get('hasil_pemeriksaan_bukan_campak_rubella');
        $keterangan = Input::get('keterangan');
        $tanggal_terima_spesimen = Input::get('tanggal_terima_spesimen');
        $tanggal_kirim_hasil = Input::get('tanggal_kirim_hasil');
        DB::table('hasil_uji_lab_campak')
        ->where('id_pasien', $id_pasien)
        ->where('id_campak', $id_campak)
        ->update(
            array(
                'no_spesimen' => $no_spesimen,
                'hasil_spesimen_darah' => $hasil_darah,
                'id_uji_lab' => $id_uji_lab,
                'created_by' => Sentry::getUser()->email,
                'hasil_igm_campak' => Input::get('hasil_igm_campak'),
                'hasil_igm_rubella' => Input::get('hasil_igm_rubella'),
                'hasil_isolasi_virus' => Input::get('hasil_isolasi_virus'),
                'tipe_spesimen' => Input::get('tipe_spesimen'),
                'sampel_lainnya' => Input::get('sampel_lainnya'),
                'tanggal_pengiriman_spesimen_propinsi' => Input::get('tanggal_pengiriman_spesimen_propinsi'),
                'tanggal_pengiriman_spesimen_laboratorium' => Input::get('tanggal_pengiriman_spesimen_laboratorium'),
                'tanggal_terima_spesimen_laboratorium' => Input::get('tanggal_terima_spesimen_laboratorium'),
                'permintaan_tes_serologi' => Input::get('permintaan_tes_serologi'),
                'kondisi_spesimen_dipropinsi' => Input::get('kondisi_spesimen_dipropinsi'),
                'kondisi_spesimen_dilab' => Input::get('kondisi_spesimen_dilab'),
                'tanggal_pemeriksaan_sampel' => Input::get('tanggal_pemeriksaan_sampel'),
                'kit_elisa' => Input::get('kit_elisa'),
                'lainnya' => Input::get('lainnya'),
                'campak_antigen_OD' => Input::get('campak_antigen_OD'),
                'rubella_antigen_OD' => Input::get('rubella_antigen_OD'),
                'control_antigen_OD' => Input::get('control_antigen_OD'),
                'tanggal_hasil_tersedia' => Input::get('tanggal_hasil_tersedia'),
                'tanggal_hasil_dilaporkan' => Input::get('tanggal_hasil_dilaporkan'),
                'hasil_pemeriksaan_akhir' => Input::get('hasil_pemeriksaan_akhir'),
                'regional_reference_lab' => Input::get('regional_reference_lab'),
                'tanggal_RRL' => Input::get('tanggal_RRL'),
                'hasil_RRL' => Input::get('hasil_RRL'),
                'tanggal_terima_hasil_RRL' => Input::get('tanggal_terima_hasil_RRL'),
                'hasil_spesimen_urin' => $hasil_urin,
                'kondisi_spesimen' => $kondisi_spesimen,
                'hasil_pemeriksaan_campak' => $hasil_pemeriksaan_campak,
                'hasil_pemeriksaan_rubella' => $hasil_pemeriksaan_rubella,
                'hasil_pemeriksaan_bukan_campak_rubella' => $hasil_pemeriksaan_bukan_campak_rubella,
                'keterangan' => $keterangan,
                'tanggal_terima_spesimen' => $tanggal_terima_spesimen,
                'tanggal_kirim_hasil' => $tanggal_kirim_hasil,
                'tanggal_uji_laboratorium' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'status' => 1, )
            );
        $msg = 'tersimpan';

        return Response::json(compact('msg'));
    }

    public function postSimpanHasilLabDifteri()
    {
        $id_pasien = Input::get('id_pasien');
        $id_difteri = Input::get('id_difteri');
        $no_spesimen = Input::get('no_spesimen');
        $hasil_tenggorokan = Input::get('hasil_spesimen_tenggorokan');
        $hasil_hidung = Input::get('hasil_spesimen_hidung');
        $klasifikasi_akhir = Input::get('klasifikasi_akhir');
        $tgl = Input::get('tanggal_uji_laboratorium');

        DB::table('hasil_uji_lab_difteri')
        ->where('id_pasien', $id_pasien)
        ->where('id_difteri', $id_difteri)
        ->update(
            array(
                'no_spesimen' => $no_spesimen,
                'hasil_spesimen_tenggorokan' => $hasil_tenggorokan,
                'hasil_spesimen_hidung' => $hasil_hidung,
                'klasifikasi_akhir' => $klasifikasi_akhir,
                'tanggal_uji_laboratorium' => $tgl,
                'kondisi_spesimen' => Input::get('kondisi_spesimen'),
                'hasil_pemeriksaan' => Input::get('hasil_pemeriksaan'),
                'keterangan' => Input::get('keterangan'),
                'tanggal_terima_spesimen' => Input::get('tanggal_terima_spesimen'),
                'tanggal_kirim_hasil' => Input::get('tanggal_kirim_hasil'),
                'status' => 1, )
            );

        return Redirect::to('lab/difteri');
    }

    public function postSimpanHasilLabAfp()
    {
        $id_pasien = Input::get('id_pasien');
        $id_difteri = Input::get('id_difteri');
        $no_spesimen = Input::get('no_spesimen');
        $hasil_tenggorokan = Input::get('hasil_spesimen_tenggorokan');
        $hasil_hidung = Input::get('hasil_spesimen_hidung');
        $klasifikasi_akhir = Input::get('klasifikasi_akhir');
        $tgl = Input::get('tanggal_uji_laboratorium');

        DB::table('hasil_uji_lab_difteri')
        ->where('id_pasien', $id_pasien)
        ->where('id_difteri', $id_difteri)
        ->update(
            array(
                'no_spesimen' => $no_spesimen,
                'hasil_spesimen_tenggorokan' => $hasil_tenggorokan,
                'hasil_spesimen_hidung' => $hasil_hidung,
                'klasifikasi_akhir' => $klasifikasi_akhir,
                'tanggal_uji_laboratorium' => $tgl,
                'kondisi_spesimen' => Input::get('kondisi_spesimen'),
                'hasil_pemeriksaan' => Input::get('hasil_pemeriksaan'),
                'keterangan' => Input::get('keterangan'),
                'tanggal_terima_spesimen' => Input::get('tanggal_terima_spesimen'),
                'tanggal_kirim_hasil' => Input::get('tanggal_kirim_hasil'),
                'status' => 1, )
            );

        return Redirect::to('lab/difteri');
    }

    public function findPasien()
    {
        $search = Input::get('search');
        $case = Input::get('penyakit');
        $query = '';
        if ($case == 'crs') {
            $query = DB::select("
				SELECT
					a.id_crs AS id, a.no_epid, b.nama_anak, b.nama_ortu, '' AS rujukan, '' AS from_lab, '' AS to_lab, b.umur, b.umur_bln, b.umur_hr, CONCAT_WS(', ',b.alamat,c.kelurahan,c.kecamatan,c.kabupaten,c.provinsi) AS alamat
				FROM
				crs AS a
				JOIN pasien AS b ON a.id_pasien=b.id_pasien
				LEFT JOIN located AS c ON b.id_kelurahan=c.id_kelurahan
                LEFT JOIN hasil_uji_lab_crs AS d ON a.id_hasil_uji_lab_crs=d.id_hasil_uji_lab_crs
				WHERE
				a.deleted_at IS NULL AND d.validity_lab IS NULL
				AND (b.nama_anak LIKE '%".$search."%' OR a.no_rm LIKE '%.$search.%')
                GROUP BY a.id_crs
			");
        } elseif ($case == 'campak') {
            $query = DB::select("
				SELECT
					a.id_campak AS id, a.no_epid, c.nama_anak, b.rujukan, e.nama_laboratorium AS from_lab, f.nama_laboratorium AS to_lab, c.nama_ortu, c.umur, c.umur_bln, c.umur_hr, CONCAT_WS(', ',c.alamat,d.kelurahan,d.kecamatan,d.kabupaten,d.provinsi) AS alamat
				FROM
					campak AS a
				JOIN hasil_uji_lab_campak AS b ON a.id_campak=b.id_campak
				JOIN pasien AS c ON b.id_pasien=c.id_pasien
                -- JOIN uji_spesimen AS h ON a.id_campak=h.id_campak
				LEFT JOIN located AS d ON c.id_kelurahan=d.id_kelurahan
				LEFT JOIN laboratorium AS e ON b.from_lab_rujukan=e.id_laboratorium
				LEFT JOIN laboratorium AS f ON b.to_lab_rujukan=f.id_laboratorium
				WHERE
				a.deleted_at IS NULL
				AND (c.nama_anak LIKE '%$search%' OR a.no_epid LIKE '%$search%' OR a.no_epid_lama LIKE '%$search%')
                GROUP BY a.id_campak
			");
        } elseif($case == 'afp'){
            $query = DB::SELECT("
                SELECT
                a.id_afp AS id, a.no_epid, c.nama_anak, c.nama_ortu, '' AS rujukan, '' AS from_lab, '' AS to_lab, c.umur, c.umur_bln, c.umur_hr, CONCAT_WS(', ',c.alamat,d.kelurahan,d.kecamatan,d.kabupaten,d.provinsi) AS alamat
                FROM afp AS a
                JOIN hasil_uji_lab_afp AS b ON a.id_afp=b.id_afp
                JOIN pasien AS c ON b.id_pasien=c.id_pasien
                LEFT JOIN located AS d ON c.id_kelurahan=d.id_kelurahan
                WHERE a.deleted_at IS NULL
                AND (c.nama_anak LIKE '%$search%' OR a.no_epid LIKE '%$search%')
                GROUP BY a.id_afp
            ");
        }

        if ($query) {
            $response = '';
            foreach ($query as $key => $val) {
                $rujukan = ($val->rujukan == 'Ya') ? 'Ya, Dari Lab '.$val->from_lab.' dirujuk ke Lab '.$val->to_lab : '-';
                $response .= "<tr>
							<td>$val->no_epid</td>
							<td>$val->nama_anak</td>
							<td>$val->nama_ortu</td>
							<td>$val->alamat</td>
							<td>$rujukan</td>
							<td>
								<a class='btn btn-success' href='".URL::to('inputHasilLab/'.$case.'/'.$val->id)."'>Input Baru</a>
							</td>
						</tr>";
            }
        } else {
            $response = "<tr>
							<td colspan='6'>Maaf, pasien yang anda cari tidak ditemukan, apakah anda ingin membuat data baru ?</td>
						</tr>";
        }
        $sent['content'] = $response;
        $sent['url'] = URL::to('inputHasilLab').'/'.$case.'/-';

        return $sent;
    }

    public function inputHasilLab($case, $id)
    {
        if ($case == 'crs') {
            $query = DB::table('getdetailcrs')
                ->where('crs_id_crs', $id)->get();
            $response['response'] = ($query) ? $query[0] : null;
            $response['action'] = 'storeLabCrs';
            $response['content'] = 'laboratorium.crs.input';
            $response['title'] = 'CRS';
        } elseif ($case == 'campak') {
            $response['action'] = 'storeLabCampak';
            $response['content'] = 'laboratorium.campak.input';
            $response['title'] = 'Campak';
        } elseif ($case == 'afp'){
            $response['action'] = 'storeLabAfp';
            $response['content'] = 'laboratorium.afp.input';
            $response['title'] = 'AFP';
        }
        $response['id'] = $id;

        return View::make('laboratorium.indexLab', compact('response'));
    }

    public function getFindPasienCampak()
    {
        $data = DB::select("
						SELECT
							a.id_campak, a.no_epid, c.nama_anak, c.nama_ortu, c.umur, CONCAT_WS(', ',c.alamat,d.kelurahan,d.kecamatan,d.kabupaten,d.provinsi) AS alamat
						FROM
						campak AS a
						JOIN hasil_uji_lab_campak AS b ON a.id_campak=b.id_campak
						JOIN pasien AS c ON b.id_pasien=c.id_pasien
						JOIN located AS d ON c.id_kelurahan=d.id_kelurahan
						WHERE
						(a.no_epid LIKE '%".Input::get('cari_no_epid')."%' OR c.nama_anak LIKE '%".Input::get('cari_no_epid')."%')
						AND a.status_at='1'
						");
        $html = '';
        if ($data) {
            foreach ($data as $value) {
                $html .= "<tr>
							<td>$value->no_epid</td>
							<td>$value->nama_anak</td>
							<td>$value->nama_ortu</td>
							<td>$value->alamat</td>
							<td>
								<a class='btn btn-success' href='".URL::to('input_hasil_lab/'.$value->id_campak)."'>Input Baru</a>
							</td>
						</tr>";
            }
        } else {
            $html .= "<tr>
							<td colspan='5'>Maaf, pasien yang anda cari tidak ditemukan, apakah anda ingin membuat data baru ?</td>
						</tr>";
        }

        return $html;
    }

    public function getFindPasienAfp()
    {
        $data = DB::select("select afp.no_epid,nama_anak,umur,CONCAT_WS(',',pasien.alamat,lokasi.kelurahan,lokasi.kecamatan,lokasi.kabupaten,lokasi.provinsi) as alamat from afp,pasien,hasil_uji_lab_afp,lokasi where lokasi.id_kelurahan=pasien.id_kelurahan and afp.id_afp=hasil_uji_lab_afp.id_afp and pasien.id_pasien=hasil_uji_lab_afp.id_pasien and afp.no_epid='".Input::get('cari_no_epid')."'");
        if ($data) {
            foreach ($data as $value) {
                $no_epid = $value->no_epid;
                $hasil = 'No Epid ='.$value->no_epid.' Nama penderita='.$value->nama_anak.' Umur='.$value->umur.' tahun Alamat='.$value->alamat;
            }
            $html = '<tr class="task">
					<td class="cell-icon"><i class="icon-checker high"></i></td>
					<td class="cell-title"><div>"'.$hasil.'"</div></td>
					<td class="cell-time align-right"><a data-toggle="modal" data-target="#myModal" href="'.URL::to('input_hasil_lab/'.$no_epid).'">Input data hasil laboratorium</a></td>
				</tr>';
        } else {
            $html = '<tr class="task">
					<td class="cell-icon"><i class="icon-checker high"></i></td>
					<td class="cell-title"><div>data tidak ditemukan, silahkan mengisi form input data individual kasus</div></td>
					<td class="cell-time align-right"><a href="'.URL::to('input_baru_afp').'">input baru</a></td>
				</tr>';
        }

        return $html;
    }

    public function getInputhasilLab($id)
    {
        $data = DB::SELECT("
						SELECT *
						FROM getdetailcampak
						WHERE campak_id_campak='$id'"
                );
        $dt = empty($data) ? '' : $data[0];

        return View::make('laboratorium.inputhasilLabCampak', compact('dt'));
    }

    public function getInputhasilLabCampak($id)
    {
        return View::make('laboratorium.inputLabCampak');
    }

    public function getNoSpesimen()
    {
        $kodeFaskes = Session::get('kd_faskes');
        $thn = substr($this->changeDate(Input::get('tglPeriksa')), 2, 2);
        $kode = $kodeFaskes.'/'.$thn.'/';
        $cek = DB::SELECT("
				SELECT
				RIGHT(no_spesimen,3) AS no_spesimen
				FROM hasil_uji_lab_campak AS a
				WHERE no_spesimen LIKE '$kode%'
				ORDER BY no_spesimen DESC
				limit 0,1
			");
        if (empty($cek[0]->no_spesimen)) {
            $noSampel = '001';
        } else {
            $noUrut = (int) substr($cek[0]->no_spesimen, -3);
            $noSampel = sprintf('%03s', $noUrut + 1);
        }
        $noLaboratorium = $kode.$noSampel;

        return $noLaboratorium;
    }

    public function getInputbaru()
    {
        return View::make('laboratorium.inputbaruCampak');
    }

    public function getInputbaruafp()
    {
        return View::make('laboratorium.inputbaruAfp');
    }

    public function getAnalisa()
    {
        return View::make('laboratorium._include.js');
    }

    //fungsi untuk memilih jenis sampel pemeriksaan laboratorium
    public function getPilihJenisSampel()
    {
        $jenis_pemeriksaan = Input::get('jenis_pemeriksaan');
        if ($jenis_pemeriksaan == 0) {
            echo'
				<tr>
					<td>Jenis sampel</td>
					<td>
					<select name="jenis_sampel" class="id_combobox" id="jenis_sampel">
					  <option value="">Pilih</option>
					  <option value="0">Darah</option>
					  <option value="1">Urin</option>
					</select>
					</td>
				</tr>
				<tr>
					<td>Hasil IGM campak</td>
					<td>
					<select name="hasil_igm_campak" class="id_combobox" id="hasil_igm_campak">
					  <option value="">Pilih</option>
					  <option value="0">Positif</option>
					  <option value="1">Negatif</option>
					  <option value="2">Equivocal</option>
					  <option value="3">Pending</option>
					</select>
					</td>
				</tr>
				<tr>
					<td>Hasil IGM Rubella</td> <td>
					<select name="hasil_igm_rubella" class="id_combobox" id="hasil_igm_rubella">
					  <option value="">Pilih</option>
					  <option value="0">Positif</option>
					  <option value="1">Negatif</option>
					  <option value="2">Equivocal</option>
					  <option value="3">Pending</option>
					</select>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><button class="btn btn-success" onclick="simpanhasillab()">simpan</button></td>
				</tr>';
        } elseif ($jenis_pemeriksaan == 1) {
            echo'
				<tr>
					<td>Jenis sampel</td>
					<td>
					<select name="jenis_sampel" class="id_combobox" id="jenis_sampel">
					  <option value="">Pilih</option>
					  <option value="0">Urin</option>
					  <option value="1">Usap tenggorokan</option>
					  <option value="2">Cairan mulut</option>
					</select>
					</td>
				</tr>
				<tr>
					<td>Hasil IGM campak</td>
					<td>
					<select name="hasil_igm_campak" class="id_combobox" id="hasil_igm_campak" onchange="cek()">
					  <option value="">Pilih</option>
					  <option value="0">Positif</option>
					  <option value="1">Negatif</option>
					  <option value="2">Pending</option>
					</select>
					<input name="nama_penyakit" id="nama_penyakit" disabled>
					</td>
				</tr>
				<tr>
					<td>Hasil IGM Rubella</td> <td>
					<select name="hasil_igm_rubella" class="id_combobox" id="hasil_igm_rubella">
					  <option value="">Pilih</option>
					  <option value="0">Positif</option>
					  <option value="1">Negatif</option>
					  <option value="2">Pending</option>
					</select>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><button class="btn btn-success" onclick="simpanhasillab()">simpan</button></td>
				</tr>';
        } elseif ($jenis_pemeriksaan == 3) {
            echo '<tr>
					<td>Jenis sampel</td>
					<td>
					<select name="jenis_sampel" class="id_combobox" id="jenis_sampel">
					  <option value="">Pilih</option>
					</select>
					</td>
				</tr>
				<tr>
					<td>Hasil IGM campak</td>
					<td>
					<select name="hasil_igm_campak" class="id_combobox" id="hasil_igm_campak">
					  <option value="">Pilih</option>
					</select>
					</td>
				</tr>
				<tr>
					<td>Hasil IGM Rubella</td> <td>
					<select name="hasil_igm_rubella" class="id_combobox" id="hasil_igm_rubella">
					  <option value="">Pilih</option>
					</select>
					</td>
				</tr>';
        }
    }

    //simpan uji sampel
    public function postUjiSampel()
    {
        $dt = Input::get('sr');
        $dt['tgl_pemeriksaan_sampel_campak'] = (empty($dt['tgl_pemeriksaan_sampel_campak']) ? '' : $this->changeDate($dt['tgl_pemeriksaan_sampel_campak']));
        $dt['tgl_hasil_tersedia_campak'] = (empty($dt['tgl_hasil_tersedia_campak']) ? '' : $this->changeDate($dt['tgl_hasil_tersedia_campak']));
        $dt['tgl_hasil_dilaporkan_campak'] = (empty($dt['tgl_hasil_dilaporkan_campak']) ? '' : $this->changeDate($dt['tgl_hasil_dilaporkan_campak']));
        $dt['tgl_pemeriksaan_sampel_rubella'] = (empty($dt['tgl_pemeriksaan_sampel_rubella']) ? '' : $this->changeDate($dt['tgl_pemeriksaan_sampel_rubella']));
        $dt['tgl_hasil_tersedia_rubella'] = (empty($dt['tgl_hasil_tersedia_rubella']) ? '' : $this->changeDate($dt['tgl_hasil_tersedia_rubella']));
        $dt['tgl_hasil_dilaporkan_rubella'] = (empty($dt['tgl_hasil_dilaporkan_rubella']) ? '' : $this->changeDate($dt['tgl_hasil_dilaporkan_rubella']));
        $dt['tgl_kirim_hasil_rubella'] = (empty($dt['tgl_kirim_hasil_rubella']) ? '' : $this->changeDate($dt['tgl_kirim_hasil_rubella']));
        $dt['tgl_pengambilan_spesimen'] = (empty($dt['tgl_pengambilan_spesimen']) ? '' : $this->changeDate($dt['tgl_pengambilan_spesimen']));
        $dt['tgl_pengiriman_spesimen'] = (empty($dt['tgl_pengiriman_spesimen']) ? '' : $this->changeDate($dt['tgl_pengiriman_spesimen']));
        $dt['tgl_penerimaan_spesimen'] = (empty($dt['tgl_penerimaan_spesimen']) ? '' : $this->changeDate($dt['tgl_penerimaan_spesimen']));
        $dt['tgl_pemeriksaan_spesimen'] = (empty($dt['tgl_pemeriksaan_spesimen']) ? '' : $this->changeDate($dt['tgl_pemeriksaan_spesimen']));
        $dt['tgl_hasil_tersedia'] = (empty($dt['tgl_hasil_tersedia']) ? '' : $this->changeDate($dt['tgl_hasil_tersedia']));
        $dt['tgl_hasil_dilaporkan'] = (empty($dt['tgl_hasil_dilaporkan']) ? '' : $this->changeDate($dt['tgl_hasil_dilaporkan']));
        $dt['tgl_spesimen_dikirim_ke_rrl'] = (empty($dt['tgl_spesimen_dikirim_ke_rrl']) ? '' : $this->changeDate($dt['tgl_spesimen_dikirim_ke_rrl']));
        $dt['tgl_penerimaan_hasil_rrl'] = (empty($dt['tgl_penerimaan_hasil_rrl']) ? '' : $this->changeDate($dt['tgl_penerimaan_hasil_rrl']));
        $dt['tgl_kirim_ke_gsl'] = (empty($dt['tgl_kirim_ke_gsl']) ? '' : $this->changeDate($dt['tgl_kirim_ke_gsl']));
        $dt['tgl_hasil_sequencing'] = (empty($dt['tgl_hasil_sequencing']) ? '' : $this->changeDate($dt['tgl_hasil_sequencing']));
        $dt['ambil_sampel'] = (empty($dt['ambil_sampel']) ? '' : $this->changeDate($dt['ambil_sampel']));
        $dt['kirim_sampel'] = (empty($dt['kirim_sampel']) ? '' : $this->changeDate($dt['kirim_sampel']));
        $dt['terima_sampel'] = (empty($dt['terima_sampel']) ? '' : $this->changeDate($dt['terima_sampel']));
        $dt['tgl_pemeriksaan_kultur'] = (empty($dt['tgl_pemeriksaan_kultur']) ? '' : $this->changeDate($dt['tgl_pemeriksaan_kultur']));
        $dt['tgl_hasil_kultur_dilaporkan'] = (empty($dt['tgl_hasil_kultur_dilaporkan']) ? '' : $this->changeDate($dt['tgl_hasil_kultur_dilaporkan']));
        $dt['tgl_pemeriksaan_pcr'] = (empty($dt['tgl_pemeriksaan_pcr']) ? '' : $this->changeDate($dt['tgl_pemeriksaan_pcr']));
        $dt['tgl_keluar_hasil_pcr'] = (empty($dt['tgl_keluar_hasil_pcr']) ? '' : $this->changeDate($dt['tgl_keluar_hasil_pcr']));
        $dt['tgl_spesimen_dirujuk'] = (empty($dt['tgl_spesimen_dirujuk']) ? '' : $this->changeDate($dt['tgl_spesimen_dirujuk']));
        $dt['tgl_periksa_sequencing'] = (empty($dt['tgl_periksa_sequencing']) ? '' : $this->changeDate($dt['tgl_periksa_sequencing']));
        $dt['tgl_terima_hasil_sequencing'] = (empty($dt['tgl_terima_hasil_sequencing']) ? '' : $this->changeDate($dt['tgl_terima_hasil_sequencing']));
        $dt['created_at'] = date('Y-m-d H:i:s');
        $dt['tgl_pemeriksaan'] = date('Y-m-d');
        $hasil_igm_campak = (empty($dt['hasil_igm_campak']) ? '' : 'Hasil IgM Campak : '.$dt['hasil_igm_campak']);
        $hasil_igm_rubella = (empty($dt['hasil_igm_rubella']) ? '' : 'Hasil IgM Rubella : '.$dt['hasil_igm_rubella']);
        $hasil_pemeriksaan_kultur = (empty($dt['hasil_pemeriksaan_kultur']) ? '' : 'Hasil pemeriksaan kultur : '.$dt['hasil_pemeriksaan_kultur']);
        $hasil_periksa_pcr = (empty($dt['hasil_periksa_pcr']) ? '' : 'Hasil pemeriksaan PCR : '.$dt['hasil_periksa_pcr']);
        $hasil_sequencing_virologi = (empty($dt['hasil_sequencing_virologi']) ? '' : 'Hasil Sequencing : '.$dt['hasil_sequencing_virologi']);
        $dt['hasil_uji_sampel'] = $hasil_igm_campak.';'.$hasil_igm_rubella.';'.$hasil_pemeriksaan_kultur.';'.$hasil_periksa_pcr.';'.$hasil_sequencing_virologi;
        $id_sampel = DB::table('uji_sampel')->insertGetId($dt);
        //update rujukan lab
        $dh['rujukan'] = Input::get('rujukan');
        $dh['from_lab_rujukan'] = (Input::get('rujukan') == 'Ya') ? Sentry::getUser()->id_user : '';
        $dh['to_lab_rujukan'] = (Input::get('to_lab_rujukan')) ? Input::get('to_lab_rujukan') : '';
        $dh['tgl_lab_rujukan'] = (Input::get('tgl_lab_rujukan')) ? $this->changeDate(Input::get('tgl_lab_rujukan')) : '';
        DB::table('hasil_uji_lab_campak')
            ->where('id_campak', Input::get('id_campak'))
            ->update($dh);

        $sent = [
            'id_sampel' => $id_sampel,
            'hasil_igm_campak' => $hasil_igm_campak,
            'hasil_igm_rubella' => $hasil_igm_rubella,
            'hasil_pemeriksaan_kultur' => $hasil_pemeriksaan_kultur,
            'hasil_periksa_pcr' => $hasil_periksa_pcr,
            'hasil_sequencing_virologi' => $hasil_sequencing_virologi,
            'msg' => 'Sampel berhasil di simpan.',
        ];

        return Response::json(compact('sent'));
    }

    //simpan hasil lab,
    //fungsi ini akan menyimpan data berupa hasil lab campak dan rubella
    //dari jenis pemeriksaan serologi maupun verologi
    //dengan parameter masing-masing jenis pemeriksaan

    public function posthasillab()
    {
        $jenis_pemeriksaan = Input::get('jenis_pemeriksaan');
        $jenis_sampel = Input::get('jenis_sampel');
        $hasil_igm_campak = Input::get('hasil_igm_campak');
        $hasil_igm_rubella = Input::get('hasil_igm_rubella');
        $nama_penyakit = Input::get('nama_penyakit');
        $id_campak = Input::get('id');

        $cek = DB::table('uji_spesimen')->where('jenis_pemeriksaan', $jenis_pemeriksaan)->where('id_campak', $id_campak)->where('jenis_sampel', $jenis_sampel)->get();
        if (empty($cek)) {
            //tambahkan ke table
            DB::table('uji_spesimen')
                ->insert(
                    array(
                        'id_campak' => $id_campak,
                        'jenis_pemeriksaan' => $jenis_pemeriksaan,
                        'jenis_sampel' => $jenis_sampel,
                        'hasil_igm_campak' => $hasil_igm_campak,
                        'hasil_igm_rubella' => $hasil_igm_rubella,
                        'nama_penyakit' => $nama_penyakit,
                        'is_periksa' => '1',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        ));
            $feedback = 'Data berhasil di simpan';
        } else {
            if (empty($nama_penyakit) || $nama_penyakit == '') {
                DB::table('uji_spesimen')
                    ->where('id_campak', $id_campak)
                    ->where('jenis_pemeriksaan', $jenis_pemeriksaan)
                    ->where('jenis_sampel', $jenis_sampel)
                    ->update(
                    array(
                        'hasil_igm_campak' => $hasil_igm_campak,
                        'hasil_igm_rubella' => $hasil_igm_rubella,
                        'is_periksa' => '1',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        )
                    );
                $feedback = 'Data berhasil di simpan';
            } elseif (isset($nama_penyakit) || $nama_penyakit != '') {
                DB::table('uji_spesimen')
                    ->where('id_campak', $id_campak)
                    ->where('jenis_pemeriksaan', $jenis_pemeriksaan)
                    ->where('jenis_sampel', $jenis_sampel)
                    ->update(
                    array(
                        'hasil_igm_campak' => $hasil_igm_campak,
                        'hasil_igm_rubella' => $hasil_igm_rubella,
                        'nama_penyakit' => $nama_penyakit,
                        'is_periksa' => '1',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        )
                    );
                $feedback = 'Data berhasil di simpan';
            }
        }

        return Response::json(compact('feedback'));
    }

    public function deletesampel($id)
    {
        DB::table('uji_sampel')->where('id_sampel', '=', $id)->delete();
    }

    public function postStoreLabCrs()
    {
        $idCrs = Input::get('id_crs');
        $crs = DB::table('crs')->WHERE('id_crs', $idCrs)->get();
        //pelapor
        $dp = Input::get('dp');
        $dp['tanggal_laporan'] = (empty($dp['tanggal_laporan']) ? '' : $this->changeDate($dp['tanggal_laporan']));
        $dp['tanggal_investigasi'] = (empty($dp['tanggal_investigasi']) ? '' : $this->changeDate($dp['tanggal_investigasi']));
        if (empty($crs[0]->id_pelapor)) {
            $dp['created_by'] = Sentry::getUser()->id;
            $dp['created_at'] = date('Y-m-d H:i:s');
            $id_pelapor = DB::table('pelapor')->insertGetId($dp);
        } else {
            $dp['updated_by'] = Sentry::getUser()->id;
            $dp['updated_at'] = date('Y-m-d H:i:s');
            DB::table('pelapor')->WHERE('id_pelapor', $crs[0]->id_pelapor)->update($dp);
            $id_pelapor = $crs[0]->id_pelapor;
        }
        //pasien
        $dpa = Input::get('dpa');
        $dpa['tanggal_lahir'] = (empty($dpa['tanggal_lahir']) ? '' : $this->changeDate($dpa['tanggal_lahir']));
        if (empty($crs[0]->id_pasien)) {
            $dpa['created_by'] = Sentry::getUser()->id;
            $dpa['created_at'] = date('Y-m-d H:i:s');
            $id_pasien = DB::table('pasien')->insertGetId($dpa);
        } else {
            $dpa['updated_by'] = Sentry::getUser()->id;
            $dpa['updated_at'] = date('Y-m-d H:i:s');
            DB::table('pasien')->WHERE('id_pasien', $crs[0]->id_pasien)->update($dpa);
            $id_pasien = $crs[0]->id_pasien;
        }
        //hasil lab
        $dlab = Input::get('dlab');
        $dlab['id_laboratorium'] = DB::table('laboratorium')->WHERE('lab_code', Session::get('kd_faskes'))->pluck('id_laboratorium');
        $dlab['lab_tgl_ambil_serum1'] = (empty($dlab['lab_tgl_ambil_serum1']) ? '' : $this->changeDate($dlab['lab_tgl_ambil_serum1']));
        $dlab['lab_tgl_kirim_serum1'] = (empty($dlab['lab_tgl_kirim_serum1']) ? '' : $this->changeDate($dlab['lab_tgl_kirim_serum1']));
        $dlab['lab_tgl_tiba_serum1'] = (empty($dlab['lab_tgl_tiba_serum1']) ? '' : $this->changeDate($dlab['lab_tgl_tiba_serum1']));
        $dlab['lab_tgl_ambil_serum2'] = (empty($dlab['lab_tgl_ambil_serum2']) ? '' : $this->changeDate($dlab['lab_tgl_ambil_serum2']));
        $dlab['lab_tgl_kirim_serum2'] = (empty($dlab['lab_tgl_kirim_serum2']) ? '' : $this->changeDate($dlab['lab_tgl_kirim_serum2']));
        $dlab['lab_tgl_tiba_serum2'] = (empty($dlab['lab_tgl_tiba_serum2']) ? '' : $this->changeDate($dlab['lab_tgl_tiba_serum2']));
        $dlab['lab_tgl_ambil_throat_swab'] = (empty($dlab['lab_tgl_ambil_throat_swab']) ? '' : $this->changeDate($dlab['lab_tgl_ambil_throat_swab']));
        $dlab['lab_tgl_kirim_throat_swab'] = (empty($dlab['lab_tgl_kirim_throat_swab']) ? '' : $this->changeDate($dlab['lab_tgl_kirim_throat_swab']));
        $dlab['lab_tgl_tiba_throat_swab'] = (empty($dlab['lab_tgl_tiba_throat_swab']) ? '' : $this->changeDate($dlab['lab_tgl_tiba_throat_swab']));
        $dlab['lab_tgl_ambil_urine'] = (empty($dlab['lab_tgl_ambil_urine']) ? '' : $this->changeDate($dlab['lab_tgl_ambil_urine']));
        $dlab['lab_tgl_kirim_urine'] = (empty($dlab['lab_tgl_kirim_urine']) ? '' : $this->changeDate($dlab['lab_tgl_kirim_urine']));
        $dlab['lab_tgl_tiba_urine'] = (empty($dlab['lab_tgl_tiba_urine']) ? '' : $this->changeDate($dlab['lab_tgl_tiba_urine']));
        $dlab['lab_tgl_igm_serum1'] = (empty($dlab['lab_tgl_igm_serum1']) ? '' : $this->changeDate($dlab['lab_tgl_igm_serum1']));
        $dlab['lab_tgl_igm_serum2'] = (empty($dlab['lab_tgl_igm_serum2']) ? '' : $this->changeDate($dlab['lab_tgl_igm_serum2']));
        $dlab['lab_tgl_igg_serum1'] = (empty($dlab['lab_tgl_igg_serum1']) ? '' : $this->changeDate($dlab['lab_tgl_igg_serum1']));
        $dlab['lab_tgl_igg_serum2'] = (empty($dlab['lab_tgl_igg_serum2']) ? '' : $this->changeDate($dlab['lab_tgl_igg_serum2']));
        $dlab['lab_tgl_isolasi'] = (empty($dlab['lab_tgl_isolasi']) ? '' : $this->changeDate($dlab['lab_tgl_isolasi']));
        $dlab['validity_lab'] = date('Y-m-d');
        if (empty($crs[0]->id_hasil_uji_lab_crs)) {
            $dlab['created_by'] = Sentry::getUser()->id;
            $dlab['created_at'] = date('Y-m-d H:i:s');
            $id_lab = DB::table('hasil_uji_lab_crs')->insertGetId($dlab);
        } else {
            $dlab['updated_by'] = Sentry::getUser()->id;
            $dlab['updated_at'] = date('Y-m-d H:i:s');
            DB::table('hasil_uji_lab_crs')->WHERE('id_hasil_uji_lab_crs', $crs[0]->id_hasil_uji_lab_crs)->update($dlab);
            $id_lab = $crs[0]->id_hasil_uji_lab_crs;
        }

        $dk = Input::get('dk');
        $dk['id_tempat_periksa'] = Session::get('kd_faskes');
        $dk['id_pelapor'] = $id_pelapor;
        $dk['id_pasien'] = $id_pasien;
        $dk['id_hasil_uji_lab_crs'] = $id_lab;
        if (empty($crs[0]->id_crs)) {
            $dk['created_by'] = Sentry::getUser()->id;
            $dk['created_at'] = date('Y-m-d H:i:s');
            $id_crs = DB::table('crs')->insertGetId($dk);
        } else {
            $dk['updated_by'] = Sentry::getUser()->id;
            $dk['updated_at'] = date('Y-m-d H:i:s');
            DB::table('crs')->WHERE('id_crs', $crs[0]->id_crs)->update($dk);
            $id_crs = $crs[0]->id_crs;
        }

        return Redirect::to('labs/crs#daftar');
    }

    public function postStoreLab()
    {
        $id_campak = Input::get('id_campak');
        $campak = DB::table('getdetailcampak')->where('campak_id_campak', $id_campak)->get();
        $dp = Input::get('dp');
        $dp['tanggal_lahir'] = (empty($dp['tanggal_lahir']) ? '' : $this->changeDate($dp['tanggal_lahir']));
        if (empty($campak[0]->pasien_id_pasien)) {
            $dp['created_by'] = Sentry::getUser()->id;
            $dp['created_at'] = date('Y-m-d H:i:s');
            $id_pasien = DB::table('pasien')->insertGetId($dp);
        } else {
            $dp['updated_by'] = Sentry::getUser()->id;
            $dp['updated_at'] = date('Y-m-d H:i:s');
            DB::table('pasien')->WHERE('id_pasien', $campak[0]->pasien_id_pasien)->update($dp);
            $id_pasien = $campak[0]->pasien_id_pasien;
        }
        $dc = Input::get('dc');
        $dc['tanggal_imunisasi_terakhir'] = (empty($dc['tanggal_imunisasi_terakhir']) ? '' : $this->changeDate($dc['tanggal_imunisasi_terakhir']));
        $dc['tanggal_timbul_demam'] = (empty($dc['tanggal_timbul_demam']) ? '' : $this->changeDate($dc['tanggal_timbul_demam']));
        $dc['tanggal_timbul_rash'] = (empty($dc['tanggal_timbul_rash']) ? '' : $this->changeDate($dc['tanggal_timbul_rash']));
        $id_rs_outbreak = (empty(Input::get('do')['id_rs_outbreak']) ? '' : Input::get('do')['id_rs_outbreak']);
        $id_puskesmas_outbreak = (empty(Input::get('do')['id_puskesmas_outbreak']) ? '' : Input::get('do')['id_puskesmas_outbreak']);
        $dc['id_laboratorium'] = Sentry::getUser()->id_user;
        if (empty($campak[0]->campak_id_campak)) {
            $dc['id_tempat_periksa'] = (empty($id_rs_outbreak) ? $id_puskesmas_outbreak : $id_rs_outbreak);
            $dc['kode_faskes'] = 'Laboratorium';
            $dc['created_by'] = Sentry::getUser()->id;
            $dc['created_at'] = date('Y-m-d H:i:s');
            $id_campak = DB::table('campak')->insertGetId($dc);
        } else {
            $dc['updated_by'] = Sentry::getUser()->id;
            $dc['updated_at'] = date('Y-m-d H:i:s');
            DB::table('campak')->WHERE('id_campak', $campak[0]->campak_id_campak)->update($dc);
            $id_campak = $campak[0]->campak_id_campak;
        }

        DB::table('gejala_campak')->where('id_campak', $id_campak)->delete();
        $gejala_lain = Input::get('gejala_lain');
        $tanggal_gejala_lain = Input::get('tanggal_gejala_lain');
        if (isset($gejala_lain)) {
            for ($i = 0; $i < count($gejala_lain); ++$i) {
                $dg = [
                    'id_campak' => $id_campak,
                    'id_daftar_gejala_campak' => $gejala_lain[$i],
                    'tgl_mulai' => $this->changeDate($tanggal_gejala_lain[$i]),
                    'created_by' => Sentry::getUser()->id,
                    'created_at' => date('Y-m-d H:i:s'),
                ];
                DB::table('gejala_campak')->insert($dg);
            }
        }

        DB::table('komplikasi')->where('id_campak', $id_campak)->delete();
        $komplikasi = Input::get('komplikasi');
        if (isset($komplikasi)) {
            for ($i = 0; $i < count($komplikasi); ++$i) {
                $dk = [
                    'id_campak' => $id_campak,
                    'komplikasi' => $komplikasi[$i],
                    'created_by' => Sentry::getUser()->id,
                    'created_at' => date('Y-m-d H:i:s'),
                ];
                DB::table('komplikasi')->insert($dk);
            }
        }
        $dh = Input::get('do');
        $dh['id_lab'] = Sentry::getUser()->id_user;
        $dh['id_pasien'] = $id_pasien;
        $dh['id_campak'] = $id_campak;
        $dh['tgl_ambil_spesimen'] = (empty($dh['tgl_ambil_spesimen']) ? '' : $this->changeDate($dh['tgl_ambil_spesimen']));
        $dh['tgl_kirim_spesimen'] = (empty($dh['tgl_kirim_spesimen']) ? '' : $this->changeDate($dh['tgl_kirim_spesimen']));
        $dh['tgl_terima_spesimen'] = (empty($dh['tgl_terima_spesimen']) ? '' : $this->changeDate($dh['tgl_terima_spesimen']));
        $dh['status_lab'] = '1';
        if (empty($campak[0]->lab_id_hasil_uji_lab_campak)) {
            $dh['created_by'] = Sentry::getUser()->id;
            $dh['created_at'] = date('Y-m-d H:i:s');
            $id_hasil_campak = DB::table('hasil_uji_lab_campak')->insertGetId($dh);
        } else {
            $dh['updated_by'] = Sentry::getUser()->id;
            $dh['updated_at'] = date('Y-m-d H:i:s');
            DB::table('hasil_uji_lab_campak')->WHERE('id_hasil_uji_lab_campak', $campak[0]->lab_id_hasil_uji_lab_campak)->update($dh);
            $id_hasil_campak = $campak[0]->lab_id_hasil_uji_lab_campak;
        }
        $ujisampel = Input::get('sampel_lab');
        if (!empty($ujisampel)) {
            for ($i = 0; $i < count($ujisampel); ++$i) {
                $uj['id_campak'] = $id_campak;
                DB::table('uji_sampel')->WHERE('id_sampel', $ujisampel[$i])->update($uj);
            }
        }

        $sent = [
            'url' => 'URL::to("lab/campak")',
            'msg' => 'Success.',
        ];

        return Response::json(compact('sent'));
    }

    //get hasil uji spesimen
    public function getHasilUjiSpesimen($id_campak)
    {
        $data = DB::table('uji_spesimen')->where('id_campak', $id_campak)->get();
        $ambil_noepid = DB::table('campak')->where('id_campak', $id_campak)->get();
        $no_epid = $ambil_noepid[0]->no_epid;
        $html = '<tr>
			<th>Jenis pemeriksaan</th>
			<th>Jenis sampel</th>
			<th>IGM Campak</th>
			<th>IGM Rubella</th>
			<th>Nama penyakit</th>
			<th>aksi</th>
		</tr>';
        foreach ($data as $value) {
            if ($value->is_periksa == 1) {
                $btn = '<a href="'.URL::to('input_detail_pemeriksaan/'.$no_epid).'" class="btn btn-medium btn-success">input detail pemeriksaan lab</a>';
            } else {
                $btn = '<a disabled="disabled" href="'.URL::to('input_detail_pemeriksaan').'" class="btn btn-medium btn-success">input detail pemeriksaan lab</a>';
            }
            if ($value->jenis_pemeriksaan == 0) {
                $jenis_pemeriksaan = 'Serologi';
                if ($value->jenis_sampel == 0) {
                    $jenis_sampel = 'Darah';
                } elseif ($value->jenis_sampel == 1) {
                    $jenis_sampel = 'Urin';
                }

                if ($value->hasil_igm_campak == 0) {
                    $hasil_igm_campak = 'Positif';
                } elseif ($value->hasil_igm_campak == 1) {
                    $hasil_igm_campak = 'Negatif';
                } elseif ($value->hasil_igm_campak == 2) {
                    $hasil_igm_campak = 'Equivocal';
                } elseif ($value->hasil_igm_campak == 3) {
                    $hasil_igm_campak = 'Pending';
                }

                if ($value->hasil_igm_rubella == 0) {
                    $hasil_igm_rubella = 'Positif';
                } elseif ($value->hasil_igm_rubella == 1) {
                    $hasil_igm_rubella = 'Negatif';
                } elseif ($value->hasil_igm_rubella == 2) {
                    $hasil_igm_rubella = 'Equivocal';
                } elseif ($value->hasil_igm_rubella == 3) {
                    $hasil_igm_rubella = 'Pending';
                }
            } else {
                $jenis_pemeriksaan = 'Verologi';
                if ($value->jenis_sampel == 0) {
                    $jenis_sampel = 'Urin';
                } elseif ($value->jenis_sampel == 1) {
                    $jenis_sampel = 'Usap tenggorokan';
                } elseif ($value->jenis_sampel == 2) {
                    $jenis_sampel = 'Cairan mulut';
                }

                if ($value->hasil_igm_campak == 0) {
                    $hasil_igm_campak = 'Positif';
                } elseif ($value->hasil_igm_campak == 1) {
                    $hasil_igm_campak = 'Negatif';
                } elseif ($value->hasil_igm_campak == 2) {
                    $hasil_igm_campak = 'Pending';
                }

                if ($value->hasil_igm_rubella == 0) {
                    $hasil_igm_rubella = 'Positif';
                } elseif ($value->hasil_igm_rubella == 1) {
                    $hasil_igm_rubella = 'Negatif';
                } elseif ($value->hasil_igm_rubella == 2) {
                    $hasil_igm_rubella = 'Pending';
                }
            }
            $html .= '<tr>
						<td>'.$jenis_pemeriksaan.'</td>
						<td>'.$jenis_sampel.'</td>
						<td>'.$hasil_igm_campak.'</td>
						<td>'.$hasil_igm_rubella.'</td>
						<td>'.$value->nama_penyakit.'</td>
						<td><a href="javascript:void(0);" onclick="hapus('.$value->id.')">hapus</a>&nbsp;&nbsp;'.$btn.'</td>
					</tr>';
        }

        echo $html;
    }

    public function getHapusUjispesimen()
    {
        $id = Input::get('id');
        $data = DB::table('uji_spesimen')->where('id', '=', $id)->delete();
        if ($data) {
            $feedback = 'Berhasil dihapus';
        }

        return Response::json(compact('feedback'));
    }

    public function getInputDetailPemeriksaan($no_epid)
    {
        return View::make('laboratorium.inputDetailPemeriksaanLab')->with('no_epid', $no_epid);
    }
}
