<?php

class UserTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->insert(array(
			'first_name' => 'upiq',
			'last_name'  => 'keripiq',
			'password'   => Hash::make('nananina'),
			'email'      => 'contact@upiqkeripiq.com',
			'created_at' => date('Y-m-d H:i:s'),
			'activated'  => '1'
		));
	}

}