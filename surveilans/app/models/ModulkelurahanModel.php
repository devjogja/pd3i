<?php
class ModulkelurahanModel extends \Eloquent {
	
	public static function getListModulkelurahan()
	{
		///*
		$data= DB::table('kelurahan')
	            ->select(
					
'kelurahan.id_kelurahan',
'kelurahan.id_kecamatan',
'kelurahan.kelurahan'
				)
				
	->where('kelurahan.id_kelurahan','LIKE','%'.Input::get('search_name').'%')
	->orwhere('kelurahan.id_kecamatan','LIKE','%'.Input::get('search_name').'%')
	->orwhere('kelurahan.kelurahan','LIKE','%'.Input::get('search_name').'%') 
				->paginate(20);
		return $data;
	}
	
	//load data provinsi
	public static function getModulkelurahan()
	{
		$data= DB::select("SELECT * FROM kelurahan WHERE id_kelurahan='".Input::get('id')."'");
		return $data;
	}
	
	public static function DoAddData(){
		DB::table('kelurahan')->insertGetId(
			array(
				
'id_kelurahan'=>Input::get('id_kelurahan'),
'id_kecamatan'=>Input::get('id_kecamatan'),
'kelurahan'=>Input::get('kelurahan')
			)
		);
	}
	
	public static function DoUpdateData(){
		DB::update("UPDATE kelurahan SET 
			
id_kelurahan = ?,
id_kecamatan = ?,
kelurahan = ?
			where id_kelurahan = ?", 
			array(
				
Input::get('id_kelurahan'),
Input::get('id_kecamatan'),
Input::get('kelurahan'),
				Input::get('saved_id')
			)
		);
		
		return true;
	}
	
	public static function DoDeleteModulkelurahan(){
		DB::table('kelurahan')->whereIn('id_kelurahan', Input::get('delete_id'))->delete();
		return true;
	}
}
?>
