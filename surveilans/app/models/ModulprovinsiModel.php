<?php
class ModulprovinsiModel extends \Eloquent {
	
	public static function getListModulprovinsi()
	{
		///*
		$data= DB::table('provinsi')
	            ->select(
					
'provinsi.id_provinsi',
'provinsi.provinsi',
'provinsi.longitude',
'provinsi.latitude',
'provinsi.default_zoom',
'provinsi.min_zoom',
'provinsi.konfirmasi'
				)
				
	->where('provinsi.id_provinsi','LIKE','%'.Input::get('search_name').'%')
	->orwhere('provinsi.provinsi','LIKE','%'.Input::get('search_name').'%')
	->orwhere('provinsi.longitude','LIKE','%'.Input::get('search_name').'%')
	->orwhere('provinsi.latitude','LIKE','%'.Input::get('search_name').'%')
	->orwhere('provinsi.default_zoom','LIKE','%'.Input::get('search_name').'%')
	->orwhere('provinsi.min_zoom','LIKE','%'.Input::get('search_name').'%')
	->orwhere('provinsi.konfirmasi','LIKE','%'.Input::get('search_name').'%') 
				->paginate(20);
		return $data;
	}
	
	//load data provinsi
	public static function getModulprovinsi()
	{
		$data= DB::select("SELECT * FROM provinsi WHERE id_provinsi='".Input::get('id')."'");
		return $data;
	}
	
	public static function DoAddData(){
		DB::table('provinsi')->insertGetId(
			array(
				
'id_provinsi'=>Input::get('id_provinsi'),
'provinsi'=>Input::get('provinsi'),
'longitude'=>Input::get('longitude'),
'latitude'=>Input::get('latitude'),
'default_zoom'=>Input::get('default_zoom'),
'min_zoom'=>Input::get('min_zoom'),
'konfirmasi'=>Input::get('konfirmasi')
			)
		);
	}
	
	public static function DoUpdateData(){
		DB::update("UPDATE provinsi SET 
			
id_provinsi = ?,
provinsi = ?,
longitude = ?,
latitude = ?,
default_zoom = ?,
min_zoom = ?,
konfirmasi = ?
			where id_provinsi = ?", 
			array(
				
Input::get('id_provinsi'),
Input::get('provinsi'),
Input::get('longitude'),
Input::get('latitude'),
Input::get('default_zoom'),
Input::get('min_zoom'),
Input::get('konfirmasi'),
				Input::get('saved_id')
			)
		);
		
		return true;
	}
	
	public static function DoDeleteModulprovinsi(){
		DB::table('provinsi')->whereIn('id_provinsi', Input::get('delete_id'))->delete();
		return true;
	}
}
?>
