<?php

class Wilayah extends \Eloquent {
	//load data Provinsi
	public static function getListProvinsi()
	{
		$data= DB::table('provinsi')
	            ->select(
					'provinsi.id_provinsi', 
					'provinsi.provinsi')
	            ->paginate(10);
		return $data;
	}
	
	//load data Kabupaten
	public static function getListKabupaten($id_provinsi)
	{
		$data= DB::table('kabupaten')
	            ->select(
					'kabupaten.id_kabupaten', 
					'kabupaten.id_provinsi', 
					'kabupaten.kabupaten')
				->where('id_provinsi',$id_provinsi)	
	            ->paginate(10);
		return $data;
	}
	
	//load data Kecamatan
	public static function getListKecamatan($id_kabupaten)
	{
		$data= DB::table('kecamatan')
	            ->select(
					'kecamatan.id_kecamatan', 
					'kecamatan.id_kabupaten', 
					'kecamatan.kecamatan')
				->where('id_kabupaten',$id_kabupaten)	
	            ->paginate(10);
		return $data;
	}
	
	//load data Kelurahan
	public static function getListKelurahan($id_kecamatan)
	{
		$data= DB::table('kelurahan')
	            ->select(
					'kelurahan.id_kelurahan', 
					'kelurahan.id_kecamatan', 
					'kelurahan.kelurahan')
				->where('id_kecamatan',$id_kecamatan)	
	            ->paginate(10);
		return $data;
	}

	public static function getDataArea($key){
        $query = DB::select("
                        SELECT v.kelurahan AS village_name,sd.kecamatan AS subdistrict_name, d.kabupaten AS district_name, p.provinsi AS province_name,
                        v.id_kelurahan AS village_code, sd.id_kecamatan AS subdistrict_code, d.id_kabupaten AS district_code, p.id_provinsi AS province_code
                        FROM kelurahan v
                        LEFT JOIN kecamatan sd ON sd.id_kecamatan = v.id_kecamatan
                        LEFT JOIN kabupaten d ON d.id_kabupaten = sd.id_kabupaten
                        LEFT JOIN provinsi p ON p.id_provinsi = d.id_provinsi
                        WHERE sd.kecamatan LIKE '%$key%'

                        UNION

                        SELECT v.kelurahan AS village_name,sd.kecamatan AS subdistrict_name, d.kabupaten AS district_name, p.provinsi AS province_name,
                        v.id_kelurahan AS village_code, sd.id_kecamatan AS subdistrict_code, d.id_kabupaten AS district_code, p.id_provinsi AS province_code
                        FROM kelurahan v
                        LEFT JOIN kecamatan sd ON sd.id_kecamatan = v.id_kecamatan
                        LEFT JOIN kabupaten d ON d.id_kabupaten = sd.id_kabupaten
                        LEFT JOIN provinsi p ON p.id_provinsi = d.id_provinsi
                        WHERE v.kelurahan LIKE '%$key%'

                        UNION

                        SELECT v.kelurahan AS village_name,sd.kecamatan AS subdistrict_name, d.kabupaten AS district_name, p.provinsi AS province_name,
                        v.id_kelurahan AS village_code, sd.id_kecamatan AS subdistrict_code, d.id_kabupaten AS district_code, p.id_provinsi AS province_code
                        FROM kelurahan v
                        LEFT JOIN kecamatan sd ON sd.id_kecamatan = v.id_kecamatan
                        LEFT JOIN kabupaten d ON d.id_kabupaten = sd.id_kabupaten
                        LEFT JOIN provinsi p ON p.id_provinsi = d.id_provinsi
                        WHERE d.kabupaten LIKE '%$key%'

                        UNION

                        SELECT v.kelurahan AS village_name,sd.kecamatan AS subdistrict_name, d.kabupaten AS district_name, p.provinsi AS province_name,
                        v.id_kelurahan AS village_code, sd.id_kecamatan AS subdistrict_code, d.id_kabupaten AS district_code, p.id_provinsi AS province_code
                        FROM kelurahan v
                        LEFT JOIN kecamatan sd ON sd.id_kecamatan = v.id_kecamatan
                        LEFT JOIN kabupaten d ON d.id_kabupaten = sd.id_kabupaten
                        LEFT JOIN provinsi p ON p.id_provinsi = d.id_provinsi
                        WHERE p.provinsi LIKE '%$key%'
                            ");
        $r = array();
        foreach ($query as $key) {
            $r[] = array(
                'kelurahan' =>$key->village_name,
                'kecamatan' =>$key->subdistrict_name,
                'kabupaten'   =>$key->district_name,
                'provinsi'  =>$key->province_name,
                'id_kelurahan'=>$key->village_code,
                'id_kecamatan'=>$key->subdistrict_code,
                'id_kabupaten' =>$key->district_code,
                'id_provinsi' =>$key->province_code
            );
        }
        return $r;
    }
}