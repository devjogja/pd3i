<?php
class ModultipespesimenModel extends \Eloquent {
	
	public static function getListModultipespesimen()
	{
		///*
		$data= DB::table('tipe_spesimen')
	            ->select(
					
'tipe_spesimen.id',
'tipe_spesimen.id_campak',
'tipe_spesimen.tipe_spesimen',
'tipe_spesimen.jml_tipe_spesimen',
'tipe_spesimen.created_at',
'tipe_spesimen.updated_at'
				)
				
	->where('tipe_spesimen.id','LIKE','%'.Input::get('search_name').'%')
	->orwhere('tipe_spesimen.id_campak','LIKE','%'.Input::get('search_name').'%')
	->orwhere('tipe_spesimen.tipe_spesimen','LIKE','%'.Input::get('search_name').'%')
	->orwhere('tipe_spesimen.jml_tipe_spesimen','LIKE','%'.Input::get('search_name').'%')
	->orwhere('tipe_spesimen.created_at','LIKE','%'.Input::get('search_name').'%')
	->orwhere('tipe_spesimen.updated_at','LIKE','%'.Input::get('search_name').'%') 
				->paginate(20);
		return $data;
	}
	
	//load data provinsi
	public static function getModultipespesimen()
	{
		$data= DB::select("SELECT * FROM tipe_spesimen WHERE id='".Input::get('id')."'");
		return $data;
	}
	
	public static function DoAddData(){
		DB::table('tipe_spesimen')->insertGetId(
			array(
				
'id'=>Input::get('id'),
'id_campak'=>Input::get('id_campak'),
'tipe_spesimen'=>Input::get('tipe_spesimen'),
'jml_tipe_spesimen'=>Input::get('jml_tipe_spesimen'),
'created_at'=>Input::get('created_at'),
'updated_at'=>Input::get('updated_at')
			)
		);
	}
	
	public static function DoUpdateData(){
		DB::update("UPDATE tipe_spesimen SET 
			
id = ?,
id_campak = ?,
tipe_spesimen = ?,
jml_tipe_spesimen = ?,
created_at = ?,
updated_at = ?
			where id = ?", 
			array(
				
Input::get('id'),
Input::get('id_campak'),
Input::get('tipe_spesimen'),
Input::get('jml_tipe_spesimen'),
Input::get('created_at'),
Input::get('updated_at'),
				Input::get('saved_id')
			)
		);
		
		return true;
	}
	
	public static function DoDeleteModultipespesimen(){
		DB::table('tipe_spesimen')->whereIn('id', Input::get('delete_id'))->delete();
		return true;
	}
}
?>
