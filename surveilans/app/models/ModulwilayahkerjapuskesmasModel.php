<?php
class ModulwilayahkerjapuskesmasModel extends \Eloquent {
	
	public static function getListModulwilayahkerjapuskesmas()
	{
		echo $type = Session::get('type');
		$kd_faskes = Session::get('kd_faskes');
		
		/*
		JOIN kecamatan b ON b.id_kecamatan=a.id_kecamatan
		JOIN kabupaten c ON c.id_kabupaten=b.id_kabupaten
		JOIN provinsi d ON d.id_provinsi=c.id_provinsi
		*/
		$data = array();
		///*
		if($type=='puskesmas'){
		$data= DB::table('wilayah_kerja_puskesmas')
	            ->select(					
					'wilayah_kerja_puskesmas.id',
					'wilayah_kerja_puskesmas.id_kelurahan',
					'wilayah_kerja_puskesmas.puskesmas_code_faskes',
					'puskesmas.puskesmas_name',
					'kelurahan.kelurahan'
					)		
					->join('puskesmas', 'puskesmas.puskesmas_code_faskes', '=', 'wilayah_kerja_puskesmas.puskesmas_code_faskes')		
					->join('kelurahan', 'kelurahan.id_kelurahan', '=', 'wilayah_kerja_puskesmas.id_kelurahan')		
					->join('kecamatan', 'kecamatan.id_kecamatan', '=', 'kelurahan.id_kecamatan')		
					->join('kabupaten', 'kabupaten.id_kabupaten', '=', 'kecamatan.id_kabupaten')		
					->join('provinsi', 'provinsi.id_provinsi', '=', 'kabupaten.id_provinsi')		
					->where('puskesmas.puskesmas_code_faskes','=',$kd_faskes)
					->where('wilayah_kerja_puskesmas.id','LIKE','%'.Input::get('search_name').'%')
					->orwhere('wilayah_kerja_puskesmas.id_kelurahan','LIKE','%'.Input::get('search_name').'%')
					->orwhere('wilayah_kerja_puskesmas.puskesmas_code_faskes','LIKE','%'.Input::get('search_name').'%')
					->orwhere('puskesmas.puskesmas_name','LIKE','%'.Input::get('search_name').'%')
					->orwhere('kelurahan.kelurahan','LIKE','%'.Input::get('search_name').'%')			
				->paginate(20);
		} else if($type=='kabupaten'){		
		$data= DB::table('wilayah_kerja_puskesmas')
	            ->select(					
					'wilayah_kerja_puskesmas.id',
					'wilayah_kerja_puskesmas.id_kelurahan',
					'wilayah_kerja_puskesmas.puskesmas_code_faskes',
					'puskesmas.puskesmas_name',
					'kelurahan.kelurahan'
					)		
					->join('puskesmas', 'puskesmas.puskesmas_code_faskes', '=', 'wilayah_kerja_puskesmas.puskesmas_code_faskes')		
					->join('kelurahan', 'kelurahan.id_kelurahan', '=', 'wilayah_kerja_puskesmas.id_kelurahan')		
					->join('kecamatan', 'kecamatan.id_kecamatan', '=', 'kelurahan.id_kecamatan')		
					->join('kabupaten', 'kabupaten.id_kabupaten', '=', 'kecamatan.id_kabupaten')		
					->join('provinsi', 'provinsi.id_provinsi', '=', 'kabupaten.id_provinsi')		
					->where('kabupaten.id_kabupaten','=',$kd_faskes)
					->where('wilayah_kerja_puskesmas.id','LIKE','%'.Input::get('search_name').'%')
					->orwhere('wilayah_kerja_puskesmas.id_kelurahan','LIKE','%'.Input::get('search_name').'%')
					->orwhere('wilayah_kerja_puskesmas.puskesmas_code_faskes','LIKE','%'.Input::get('search_name').'%')
					->orwhere('puskesmas.puskesmas_name','LIKE','%'.Input::get('search_name').'%')
					->orwhere('kelurahan.kelurahan','LIKE','%'.Input::get('search_name').'%')			
				->paginate(20);
		}	else if($type=='provinsi'){		
		$data= DB::table('wilayah_kerja_puskesmas')
	            ->select(					
					'wilayah_kerja_puskesmas.id',
					'wilayah_kerja_puskesmas.id_kelurahan',
					'wilayah_kerja_puskesmas.puskesmas_code_faskes',
					'puskesmas.puskesmas_name',
					'kelurahan.kelurahan'
					)		
					->join('puskesmas', 'puskesmas.puskesmas_code_faskes', '=', 'wilayah_kerja_puskesmas.puskesmas_code_faskes')		
					->join('kelurahan', 'kelurahan.id_kelurahan', '=', 'wilayah_kerja_puskesmas.id_kelurahan')		
					->join('kecamatan', 'kecamatan.id_kecamatan', '=', 'kelurahan.id_kecamatan')		
					->join('kabupaten', 'kabupaten.id_kabupaten', '=', 'kecamatan.id_kabupaten')		
					->join('provinsi', 'provinsi.id_provinsi', '=', 'kabupaten.id_provinsi')		
					->where('provinsi.id_provinsi','=',$kd_faskes)
					->where('wilayah_kerja_puskesmas.id','LIKE','%'.Input::get('search_name').'%')
					->orwhere('wilayah_kerja_puskesmas.id_kelurahan','LIKE','%'.Input::get('search_name').'%')
					->orwhere('wilayah_kerja_puskesmas.puskesmas_code_faskes','LIKE','%'.Input::get('search_name').'%')
					->orwhere('puskesmas.puskesmas_name','LIKE','%'.Input::get('search_name').'%')
					->orwhere('kelurahan.kelurahan','LIKE','%'.Input::get('search_name').'%')			
				->paginate(20);
		}	else if($type=='kemenkes'){		
		$data= DB::table('wilayah_kerja_puskesmas')
	            ->select(					
					'wilayah_kerja_puskesmas.id',
					'wilayah_kerja_puskesmas.id_kelurahan',
					'wilayah_kerja_puskesmas.puskesmas_code_faskes',
					'puskesmas.puskesmas_name',
					'kelurahan.kelurahan'
					)		
					->join('puskesmas', 'puskesmas.puskesmas_code_faskes', '=', 'wilayah_kerja_puskesmas.puskesmas_code_faskes')		
					->join('kelurahan', 'kelurahan.id_kelurahan', '=', 'wilayah_kerja_puskesmas.id_kelurahan')		
					->join('kecamatan', 'kecamatan.id_kecamatan', '=', 'kelurahan.id_kecamatan')		
					->join('kabupaten', 'kabupaten.id_kabupaten', '=', 'kecamatan.id_kabupaten')		
					->join('provinsi', 'provinsi.id_provinsi', '=', 'kabupaten.id_provinsi')		
					
					->where('wilayah_kerja_puskesmas.id','LIKE','%'.Input::get('search_name').'%')
					->orwhere('wilayah_kerja_puskesmas.id_kelurahan','LIKE','%'.Input::get('search_name').'%')
					->orwhere('wilayah_kerja_puskesmas.puskesmas_code_faskes','LIKE','%'.Input::get('search_name').'%')
					->orwhere('puskesmas.puskesmas_name','LIKE','%'.Input::get('search_name').'%')
					->orwhere('kelurahan.kelurahan','LIKE','%'.Input::get('search_name').'%')			
							
				->paginate(20);
		}				
		return $data;
	}
	
	//load data provinsi
	public static function getModulwilayahkerjapuskesmas()
	{
		$data= DB::select("SELECT * FROM wilayah_kerja_puskesmas WHERE id='".Input::get('id')."'");
		return $data;
	}
	
	public static function DoAddData(){
		return DB::table('wilayah_kerja_puskesmas')->insertGetId(
			array(				
				'id'=>Input::get('id'),
				'id_kelurahan'=>Input::get('id_kelurahan'),
				'puskesmas_code_faskes'=>Input::get('puskesmas_code_faskes')
			)
		);
	}
	
	public static function DoUpdateData(){
		$data = array(
				'id_kelurahan'=>Input::get('id_kelurahan'),
				'puskesmas_code_faskes'=>Input::get('puskesmas_code_faskes'),
				'id_kelurahan'=>Input::get('id_kelurahan'),
			);
		$query = DB::table('wilayah_kerja_puskesmas');
		$query->where('id',Input::get('saved_id'));
		return $query->update($data);
	}
	
	public static function DoDeleteModulwilayahkerjapuskesmas(){
		DB::table('wilayah_kerja_puskesmas')->whereIn('id', Input::get('delete_id'))->delete();
		return true;
	}
}
?>
