<?php
class Afp extends Eloquent
{

    //indentifikasi table
    protected $table = "afp";

    public $timestamps = false;

    //indentifikasi primaryKey
    protected $primaryKey = 'id_afp';

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    // Don't forget to fill this array
    protected $fillable = [
        'id_laboratorium',
        'id_tempat_periksa',
        'jenis_tempat_periksa',
        'no_epid',
        'tanggal_mulai_lumpuh',
        'demam_sebelum_lumpuh',
        'kelumpuhan_anggota_gerak_kanan',
        'kelumpuhan_anggota_gerak_kiri',
        'gangguan_raba_anggota_gerak_kanan',
        'gangguan_raba_anggota_gerak_kiri',
        'imunisasi_polio_sebelum_sakit',
        'tanggal_pengambilan_spesimen',
        'jenis_spesimen',
        'kontak',
        'keadaan_akhir',
        'tanggal_periksa',
    ];

    /*public static function delete($id)
    {
    $dt = [
    'deleted_by'=>Sentry::getUser()->id,
    'deleted_at' =>date('Y-m-d H:i:s')
    ];
    return DB::table($this->table)->where('id_afp',$id)->update($dt);
    }*/

    public static function simpan(
        $id_laboratorium,
        $id_tempat_periksa,
        $jenis_tempat_periksa,
        $no_epid,
        $tanggal_mulai_lumpuh,
        $demam_sebelum_lumpuh,
        $kelumpuhan_anggota_gerak_kanan,
        $kelumpuhan_anggota_gerak_kiri,
        $gangguan_raba_anggota_gerak_kanan,
        $gangguan_raba_anggota_gerak_kiri,
        $imunisasi_polio_sebelum_sakit,
        $tanggal_pengambilan_spesimen,
        $jenis_spesimen,
        $kontak,
        $keadaan_akhir
    ) {
        Afp::create(compact(
            'id_laboratorium',
            'id_tempat_periksa',
            'jenis_tempat_periksa',
            'no_epid',
            'tanggal_mulai_lumpuh',
            'demam_sebelum_lumpuh',
            'kelumpuhan_anggota_gerak_kanan',
            'kelumpuhan_anggota_gerak_kiri',
            'gangguan_raba_anggota_gerak_kanan',
            'gangguan_raba_anggota_gerak_kiri',
            'imunisasi_polio_sebelum_sakit',
            'tanggal_pengambilan_spesimen',
            'jenis_spesimen',
            'kontak',
            'keadaan_akhir',
            'timestamps()'
        ));
    }

    public static function simpanHasilLab(
        $id_afp,
        $id_pasien,
        $hasil_spesimen,
        $klasifikasi_akhir,
        $tanggal_uji_laboratorium
    ) {
        DB::table('hasil_uji_lab_afp')->insert(array(
            'id_afp' => $id_afp,
            'id_pasien' => $id_pasien,
            'hasil_spesimen' => $hasil_spesimen,
            'klasifikasi_akhir' => $klasifikasi_akhir,
            'tanggal_uji_laboratorium' => $tanggal_uji_laboratorium,
        ));
    }

    public static function pasienafp($date = null, $district = null)
    {
        $type = Session::get('type');
        $kd_faskes = Session::get('kd_faskes');
        $wilayah = "";
        if ($type == "rs") {
            $wilayah = "AND rs_kode_faskes='$kd_faskes'";
        } else if ($type == "puskesmas") {
            $wilayah = "AND puskesmas_code_faskes='$kd_faskes'";
        } else if ($type == "kabupaten") {
            $wilayah = "AND (puskesmas_kode_kab='$kd_faskes' OR rs_kode_kab='$kd_faskes')";
        } else if ($type == "provinsi") {
            $wilayah = "AND (puskesmas_kode_prop='$kd_faskes' OR rs_kode_prop='$kd_faskes')";
        } else if ($type == "kemenkes") {
            $wilayah = "";
        }

        $data = DB::SELECT("
			SELECT
				afp_no_epid AS no_epid,
				afp_id_afp AS id_afp,
				CASE
					WHEN afp_kode_faskes='puskesmas' THEN puskesmas_name
					WHEN afp_kode_faskes='rs' THEN rs_nama_faskes
				END AS nama_faskes,
				pasien_umur AS umur,
				pasien_umur_bln AS umur_bln,
				pasien_umur_hr AS umur_hr,
				afp_kode_faskes AS kode_faskes,
				pe_id AS id_pe_afp, pasien_nama_anak AS nama_anak, pasien_nama_ortu AS nama_ortu,
				alamat_new AS alamat, pasien_umur AS umur, pasien_tanggal_lahir AS tanggal_lahir,
				pasien_jenis_kelamin AS jenis_kelamin, afp_keadaan_akhir AS keadaan_akhir,
				CASE
					WHEN afp_keadaan_akhir='1' THEN 'Hidup/Sehat'
					WHEN afp_keadaan_akhir='2' THEN 'Meninggal'
					ELSE 'Tidak diisi'
				END AS 'keadaan_akhir_nm',
				afp_klasifikasi_final AS klasifikasi_final,
				CASE
					WHEN afp_klasifikasi_final='1' THEN 'Polio'
					WHEN afp_klasifikasi_final='2' THEN 'Bukan Polio'
					WHEN afp_klasifikasi_final='3' THEN 'Compatible'
					ELSE 'Tidak diisi'
				END AS 'klasifikasi_final_nm',
				lab_status AS status,
				lab_id_hasil_uji_lab_afp AS id_hasil_uji_lab_afp
			FROM getdetailafp
			WHERE
			1=1
			$wilayah
			$date
			$district
		");
        return $data;
    }

    public static function count()
    {
        $data = DB::select("SELECT
								count(*) as jml
							FROM
								pasien,afp,users,laboratorium,hasil_uji_lab_afp
							WHERE
								users.id_user=laboratorium.id_laboratorium
							AND
								pasien.id_pasien=hasil_uji_lab_afp.id_pasien
							AND
								afp.id_afp=hasil_uji_lab_afp.id_afp
							AND
								afp.id_laboratorium = laboratorium.id_laboratorium
							AND
								users.email='" . Sentry::getUser()->email . "'");
        $hsl = $data[0]->jml;
        return $hsl;
    }

    //simpan data penyelidikan epidemologi afp
    public static function epid_afp($param = [])
    {
        $d = [];
        foreach ($param as $key => $val) {
            if (!empty($val)) {
                $d[$key] = $val;
            }
        }
        $d['created_by'] = Sentry::getUser()->id;
        $d['faskes_id'] = Sentry::getUser()->id_user;
        $d['created_at'] = date("Y-m-d H:i:s");
        $d['updated_at'] = date("Y-m-d H:i:s");
        $id_pe = DB::table('pe_afp')->insertGetId($d);
        return $id_pe;
    }

    public static function getPeAfp()
    {
        $type = Session::get('type');
        $kd_faskes = Session::get('kd_faskes');
        $wilayah = "";
        if ($type == "rs") {
            $wilayah = "AND rs_kode_faskes='$kd_faskes'";
        } else if ($type == "puskesmas") {
            $wilayah = "AND puskesmas_code_faskes='$kd_faskes'";
        } else if ($type == "kabupaten") {
            $wilayah = "AND (puskesmas_kode_kab='$kd_faskes' OR rs_kode_kab='$kd_faskes')";
        } else if ($type == "provinsi") {
            $wilayah = "AND (puskesmas_kode_prop='$kd_faskes' OR rs_kode_prop='$kd_faskes')";
        } else if ($type == "kemenkes") {
            $wilayah = "";
        }
        $data = DB::select("SELECT
			*
			FROM
			getdetailafp
			WHERE
			pe_id IS NOT NULL
			$wilayah
			");
        return $data;
    }

    public static function getEditPeAfp($id)
    {
        $data = DB::select("SELECT
					b.id_pasien,
					b.nik,
					b.nama_ortu,
					b.nama_anak,
					concat_ws(', ',b.alamat, c.kelurahan, c.kecamatan,c.kabupaten,c.provinsi) AS alamat,
					b.tanggal_lahir,
					b.umur,
					b.umur_bln,
					b.umur_hr,
					b.jenis_kelamin,
					b.id_kelurahan,
					c.kecamatan,
					c.kelurahan,
					c.id_provinsi,
					c.id_kabupaten,
					a.id,
					a.propinsi AS provinsi,
					a.kabupaten,
					a.ket_sumber_laporan,
					a.laporan_dari,
					a.tanggal_mulai_sakit,
					a.tanggal_mulai_lumpuh,
					a.tanggal_meninggal,
					a.berobat_unit_pelayanan,
					a.nama_unit_pelayanan,
					a.tanggal_berobat,
					a.diagnosis,
					a.no_rekam_medis,
					a.kelumpuhan,
					a.no_epid,
					a.demam_sebelum_lumpuh,
					a.lumpuh_tungkai_kanan,
					a.lumpuh_tungkai_kiri,
					a.lumpuh_lengan_kiri,
					a.lumpuh_lengan_kanan,
					a.raba_tungkai_kanan,
					a.raba_tungkai_kiri,
					a.raba_lengan_kanan,
					a.catatan_lain,
					a.raba_lengan_kiri,
					a.sebelum_sakit_berpergian,
					a.lokasi,
					a.tanggal_pergi,
					a.berkunjung,
					a.imunisasi_rutin,
					a.jumlah_dosis,
					a.sumber_informasi,
					a.bias_polio,
					a.jumlah_dosis_bias_polio,
					a.sumber_informasi_bias_polio,
					a.tanggal_imunisasi_polio,
					a.tanggal_ambil_spesimen_I,
					a.tanggal_ambil_spesimen_II,
					a.tanggal_kirim_spesimen_I,
					a.tanggal_kirim_spesimen_II,
					a.tanggal_kirim_spesimen_I_propinsi,
					a.tanggal_kirim_spesimen_II_propinsi,
					a.catatan_tidak_diambil_spesimen,
					a.nama_petugas,
					a.hasil_diagnosis,
					a.nama_DSA
					FROM
					pe_afp AS a
					JOIN pasien AS b ON a.id_pasien=b.id_pasien
					JOIN located AS c ON b.id_kelurahan=c.id_kelurahan
					WHERE a.id ='" . $id . "'");

        return $data;
    }

    /*public static function getEditPeAfp($id)
    {
    $data = DB::table('getdetailafp')->WHERE('id',$id)->get();
    return $data;
    }*/

    //cek apakah sudah di PE atau belum
    public static function checkPE($no_epid, $id_pe)
    {
        // $data = DB::select("select no_epid from pe_afp,users where pe_afp.created_by=users.id and users.email='".Sentry::getUser()->email."' and no_epid='".$no_epid."'");
        $data = DB::table('pe_afp AS a')
            ->JOIN('users AS b', 'a.created_by', '=', 'b.id')
            ->JOIN('afp AS c', 'a.id', '=', 'c.id_pe_afp')
            ->SELECT('c.no_epid')
            ->WHERE('b.email', Sentry::getUser()->email)
            ->WHERE('c.no_epid', $no_epid)
            ->WHERE('a.id', $id_pe)
            ->get();
        return $data;
    }

    public function patients()
    {
        return $this->belongsToMany('Pasien', 'hasil_uji_lab_afp', 'id_afp', 'id_pasien');
    }

}
