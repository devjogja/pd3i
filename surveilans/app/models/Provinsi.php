<?php

class Provinsi extends \Eloquent {

	/**
	 * Mendefinisikan tabel yang digunakan oleh model ini
	 *
	 * @var string
	 */
	protected $table = 'provinsi';



	/**
	 * Mendefinisikan kolom primary key
	 *
	 * @var string
	 */
	protected $primaryKey = 'id_provinsi';



	/**
	 * Rules untuk keperluan validasi
	 *
	 * @var array
	 */
	public static $rules = [

	];



	/**
	 * Mendefinisikan kolom yang bisa diisi menggunakan mass assignment
	 *
	 * @var array
	 */
	protected $fillable = [];



	//
	/**
	 * Ambil data wilayah seluruh indonesia. paginate 10
	 *
	 * @return
	 */
	public static function getWilayah()
	{
		$data = DB::table('lokasi')
					->select('id_kelurahan','kelurahan', 'kecamatan','kabupaten','provinsi')
					->paginate(10);

		return $data;
	}

	public function kabupatens()
	{
		return $this->hasMany('Kabupaten', 'id_provinsi');
	}

}