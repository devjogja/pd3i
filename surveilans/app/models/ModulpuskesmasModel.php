<?php
class ModulpuskesmasModel extends \Eloquent {
	
	public static function getListModulpuskesmas()
	{
		///*
		$data= DB::table('puskesmas')
	            ->select(
					
'puskesmas.puskesmas_id',
'puskesmas.puskesmas_name',
'puskesmas.puskesmas_code_faskes',
'puskesmas.alamat',
'puskesmas.lokasi_puskesmas',
'puskesmas.longitude',
'puskesmas.latitude',
'puskesmas.kabupaten_name',
'puskesmas.konfirmasi',
'puskesmas.kode_desa',
'puskesmas.kode_kec',
'puskesmas.kode_kab',
'puskesmas.kode_prop'
				)
				
	->where('puskesmas.puskesmas_id','LIKE','%'.Input::get('search_name').'%')
	->orwhere('puskesmas.puskesmas_name','LIKE','%'.Input::get('search_name').'%')
	->orwhere('puskesmas.puskesmas_code_faskes','LIKE','%'.Input::get('search_name').'%')
	->orwhere('puskesmas.alamat','LIKE','%'.Input::get('search_name').'%')
	->orwhere('puskesmas.lokasi_puskesmas','LIKE','%'.Input::get('search_name').'%')
	->orwhere('puskesmas.longitude','LIKE','%'.Input::get('search_name').'%')
	->orwhere('puskesmas.latitude','LIKE','%'.Input::get('search_name').'%')
	->orwhere('puskesmas.kabupaten_name','LIKE','%'.Input::get('search_name').'%')
	->orwhere('puskesmas.konfirmasi','LIKE','%'.Input::get('search_name').'%')
	->orwhere('puskesmas.kode_desa','LIKE','%'.Input::get('search_name').'%')
	->orwhere('puskesmas.kode_kec','LIKE','%'.Input::get('search_name').'%')
	->orwhere('puskesmas.kode_kab','LIKE','%'.Input::get('search_name').'%')
	->orwhere('puskesmas.kode_prop','LIKE','%'.Input::get('search_name').'%') 
				->paginate(20);
		return $data;
	}
	
	//load data provinsi
	public static function getModulpuskesmas()
	{
		$data= DB::select("SELECT * FROM puskesmas WHERE puskesmas_id='".Input::get('id')."'");
		return $data;
	}
	
	public static function DoAddData(){
		DB::table('puskesmas')->insertGetId(
			array(
				
'puskesmas_id'=>Input::get('puskesmas_id'),
'puskesmas_name'=>Input::get('puskesmas_name'),
'puskesmas_code_faskes'=>Input::get('puskesmas_code_faskes'),
'alamat'=>Input::get('alamat'),
'lokasi_puskesmas'=>Input::get('lokasi_puskesmas'),
'longitude'=>Input::get('longitude'),
'latitude'=>Input::get('latitude'),
'kabupaten_name'=>Input::get('kabupaten_name'),
'konfirmasi'=>Input::get('konfirmasi'),
'kode_desa'=>Input::get('kode_desa'),
'kode_kec'=>Input::get('kode_kec'),
'kode_kab'=>Input::get('kode_kab'),
'kode_prop'=>Input::get('kode_prop')
			)
		);
	}

	public static function DoUpdateData($data='')
	{
		if ($data) {
			unset($data['saved_id']);
			return DB::table('puskesmas')
				->where('puskesmas_id',$data['puskesmas_id'])
				->update($data);
		}
	}
	
	/*public static function DoUpdateData(){
		DB::update("UPDATE puskesmas SET 
			
puskesmas_id = ?,
puskesmas_name = ?,
puskesmas_code_faskes = ?,
alamat = ?,
lokasi_puskesmas = ?,
longitude = ?,
latitude = ?,
kabupaten_name = ?,
konfirmasi = ?,
kode_desa = ?,
kode_kec = ?,
kode_kab = ?,
kode_prop = ?
			where puskesmas_id = ?", 
			array(
				
Input::get('puskesmas_id'),
Input::get('puskesmas_name'),
Input::get('puskesmas_code_faskes'),
Input::get('alamat'),
Input::get('lokasi_puskesmas'),
Input::get('longitude'),
Input::get('latitude'),
Input::get('kabupaten_name'),
Input::get('konfirmasi'),
Input::get('kode_desa'),
Input::get('kode_kec'),
Input::get('kode_kab'),
Input::get('kode_prop'),
				Input::get('saved_id')
			)
		);
		
		return true;
	}*/
	
	public static function DoDeleteModulpuskesmas(){
		DB::table('puskesmas')->whereIn('puskesmas_id', Input::get('delete_id'))->delete();
		return true;
	}

	static function getData($where=array())
	{
		$query = DB::table('puskesmas AS a');
		foreach ($where as $key => $val) {
			$query->where($key, $val);
		}
		return $query->get();
	}
}
?>
