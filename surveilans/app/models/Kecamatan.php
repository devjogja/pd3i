<?php

class Kecamatan extends \Eloquent {

	//mendefinisikan table kecamatan
	protected $table = "kecamatan";



	//mendefinisikan primaryKey
	protected $primaryKey = 'id_kecamatan';


	public $timestamps = false;



	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];



	// Don't forget to fill this array
	protected $fillable = [];



	//ambil kecamatan sesuai id kabupaten
	public static function getKec($id)
	{
		$data = DB::table('kecamatan')
					->select('id_kecamatan','kecamatan')
					->where('id_kabupaten',$id)
					->get();

		return $data;
	}



	public function kabupaten()
	{
		return $this->belongsTo('Kabupaten', 'id_kabupaten');
	}



	public function kelurahans()
	{
		return $this->hasMany('Kelurahan', 'id_kecamatan');
	}
}