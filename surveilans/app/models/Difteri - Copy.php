<?php
class Difteri extends Eloquent {

	//indentifikasi table
	protected $table = "difteri";



	public $timestamps = false;



	//indentifikasi primaryKey
	protected $primaryKey = 'id_difteri';



	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];



	// Don't forget to fill this array
	protected $fillable = [
		'id_laboratorium','id_tempat_periksa','jenis_tempat_periksa',
		'tanggal_timbul_demam','tanggal_mulai_sakit_kerongkongan',
		'tanggal_mulai_bengkak','tanggal_mulai_sesak_nafas',
		'tanggal_diambil_spesimen_hidung',
		'tanggal_diambil_pseudomembran','tanggal_diambil_spesimen_tenggorokan',
		'gejala_lain','vaksin_DPT_sebelum_sakit','berobat',
		'tempat_berobat','dirawat','obat','kontak','keadaan_akhir',
		'tanggal_periksa'
	];



	public static function simpan(
		$id_laboratorium,
		$id_tempat_periksa,
		$jenis_tempat_periksa,
		$tanggal_timbul_demam,
		$tanggal_mulai_sakit_kerongkongan,
		$tanggal_mulai_bengkak,
		$tanggal_mulai_sesak_nafas,
		$tanggal_diambil_spesimen_hidung,
		$tanggal_diambil_pseudomembran,
		$tanggal_diambil_spesimen_tenggorokan,
		$gejala_lain,
		$vaksin_DPT_sebelum_sakit,
		$berobat,
		$tempat_berobat,
		$dirawat,
		$obat,
		$kontak,
		$keadaan_akhir
	) {
		Difteri::create(compact(
			'id_laboratorium',
			'id_tempat_periksa',
			'jenis_tempat_periksa',
			'tanggal_timbul_demam',
			'tanggal_mulai_sakit_kerongkongan',
			'tanggal_mulai_bengkak',
			'tanggal_mulai_sesak_nafas',
			'tanggal_diambil_spesimen_hidung',
			'tanggal_diambil_pseudomembran',
			'tanggal_diambil_spesimen_tenggorokan',
			'gejala_lain',
			'vaksin_DPT_sebelum_sakit',
			'berobat',
			'tempat_berobat',
			'dirawat',
			'obat',
			'kontak',
			'keadaan_akhir',
			'timestamps()'
		));
	}



	public static function simpanHasilLabDifteri(
		$id_pasien,
		$id_difteri,
		$hasil_spesimen_tenggorokan,
		$hasil_spesimen_hidung,
		$klasifikasi_akhir,
		$tanggal_uji_laboratorium
	) {
		DB::table('hasil_uji_lab_difteri')->insert(array(
			'id_pasien'                  => $id_pasien,
			'id_difteri'                 => $id_difteri,
			'hasil_spesimen_tenggorokan' => $hasil_spesimen_tenggorokan,
			'hasil_spesimen_hidung'      => $hasil_spesimen_hidung,
			'klasifikasi_akhir'          => $klasifikasi_akhir,
			'tanggal_uji_laboratorium'   => $tanggal_uji_laboratorium
		));
	}



	public static function pasiendifteri($date=null, $district=null)
	{
		$type = Session::get('type');
		$kd_faskes = Session::get('kd_faskes');
		$wilayah = "";
		if($type == "rs"){
			$wilayah = "AND rs_kode_faskes='".$kd_faskes."'";
		} else if($type == "puskesmas") {
			$wilayah = "AND puskesmas_code_faskes='".$kd_faskes."' ";
		} else if($type == "kabupaten") {
			$wilayah = "AND puskesmas_kode_kab='".$kd_faskes."' OR rs_kode_kab='".$kd_faskes."'";
		} else if($type == "provinsi") {
			$wilayah = "AND puskesmas_kode_prop='".$kd_faskes."' OR rs_kode_prop='".$kd_faskes."'";
		} else if($type == "kemenkes") {
			$wilayah = "";
		}

		$data = DB::select("
				SELECT
				difteri_no_epid AS no_epid,
				difteri_id_difteri AS id_difteri,
				IF(difteri_kode_faskes='rs',rs_nama_faskes,
				IF(difteri_kode_faskes='puskesmas',puskesmas_name,
					'Tidak Diisi')) as nama_faskes,
				pasien_nama_anak AS nama_anak,
				pasien_umur AS umur,
				pasien_umur_bln AS umur_bln,
				pasien_umur_hr AS umur_hr,
				difteri_kode_faskes AS kode_faskes,
				pasien_nama_ortu AS nama_ortu, alamat_new AS alamat,
				pasien_tanggal_lahir AS tanggal_lahir, pasien_jenis_kelamin AS jenis_kelamin, difteri_keadaan_akhir AS keadaan_akhir,
				IF(difteri_keadaan_akhir='1','Hidup/Sehat',
				IF(difteri_keadaan_akhir='2','Meninggal',
				'Tidak Diisi')) as keadaan_akhir_nm,
				difteri_klasifikasi_final,
				IF(difteri_klasifikasi_final='1','Probable',
				IF(difteri_klasifikasi_final='2','Konfirm',
				IF(difteri_klasifikasi_final='3','Negatif',
				'Tidak Diisi'))) as klasifikasi_final_nm,
				lab_status AS status,
				lab_id_hasil_uji_lab_difteri AS id_hasil_uji_lab_difteri
				FROM getdetaildifteri
				WHERE
				1=1
				$wilayah
				$date
				$district
			");
		return $data;
	}

	public static function count()
	{
		$data = DB::select("SELECT
								count(*) as jml
							FROM
								pasien,difteri,users,laboratorium,hasil_uji_lab_difteri
							WHERE
								users.id_user=laboratorium.id_laboratorium
							AND
								pasien.id_pasien=hasil_uji_lab_difteri.id_pasien
							AND
								difteri.id_difteri=hasil_uji_lab_difteri.id_difteri
							AND
								difteri.id_laboratorium = laboratorium.id_laboratorium
							AND
								users.email='".Sentry::getUser()->email."'");
		$hsl=$data[0]->jml;
		return $hsl;
	}




	//simpan data penyelidikan epidemologi difteri
	public static function epid_difteri(
		$no_epid,
		$id_pasien,
		$created_by,
		$telepon,
		$pekerjaan,
		$alamat_tempat_kerja,
		$nama_saudara_yang_bisa_dihubungi,
		$alamat_saudara_yang_bisa_dihubungi,
		$tlp_saudara_yang_bisa_dihubungi,
		$tempat_tinggal,
		$puskesmas,
		$tanggal_mulai_sakit,
		$keluhan,
		$gejala_sakit_demam,
		$tanggal_gejala_sakit_demam,
		$gejala_sakit_kerongkongan,
		$tanggal_gejala_sakit_kerongkongan,
		$gejala_leher_bengkak,
		$tanggal_gejala_leher_bengkak,
		$gejala_sesak_nafas,
		$tanggal_gejala_sesak_nafas,
		$gejala_pseudomembran,
		$tanggal_gejala_pseudomembran,
		$gejala_lain,
		$status_imunisasi_difteri,
		$jenis_spesimen,
		$tanggal_ambil_spesimen,
		$kode_spesimen,
		$tempat_berobat,
		$dirawat,
		$trakeostomi,
		$antibiotik,
		$obat_lain,
		$ads,
		$kondisi_kasus,
		$penderita_berpergian,
		$tempat_berpergian,
		$penderita_berkunjung,
		$tempat_berkunjung,
		$penderita_menerima_tamu,
		$asal_tamu,
		$nama,
		$umur,
		$hubungan_kasus,
		$status_imunisasi,
		$hasil_lab,$profilaksis
	) {
		$id_pe = DB::table('pe_difteri')->insertGetId(array(
			'no_epid'							=> $no_epid,
			'faskes_id'						   => Sentry::getUser()->id_user,
			'id_pasien'                         => $id_pasien,
			'created_by'						=> $created_by,
			'telepon'                           => $telepon,
			'pekerjaan'                         => $pekerjaan,
			'alamat_tempat_kerja' 				=> $alamat_tempat_kerja,
			'nama_saudara_yang_bisa_dihubungi' 	=> $nama_saudara_yang_bisa_dihubungi,
			'alamat_saudara_yang_bisa_dihubungi'=> $alamat_saudara_yang_bisa_dihubungi,
			'tlp_saudara_yang_bisa_dihubungi'	=> $tlp_saudara_yang_bisa_dihubungi,
			'tempat_tinggal'                    => $tempat_tinggal,
			'puskesmas'                         => $puskesmas,
			'tanggal_mulai_sakit'               => $tanggal_mulai_sakit,
			'keluhan'                           => $keluhan,
			'gejala_sakit_demam'                => $gejala_sakit_demam,
			'tanggal_gejala_sakit_demam'        => $tanggal_gejala_sakit_demam,
			'gejala_sakit_kerongkongan'         => $gejala_sakit_kerongkongan,
			'tanggal_gejala_sakit_kerongkongan' => $tanggal_gejala_sakit_kerongkongan,
			'gejala_leher_bengkak'              => $gejala_leher_bengkak,
			'tanggal_gejala_leher_bengkak'      => $tanggal_gejala_leher_bengkak,
			'gejala_sesak_nafas'                => $gejala_sesak_nafas,
			'tanggal_gejala_sesak_nafas'        => $tanggal_gejala_sesak_nafas,
			'gejala_pseudomembran'              => $gejala_pseudomembran,
			'tanggal_gejala_pseudomembran'      => $tanggal_gejala_pseudomembran,
			'gejala_lain'                       => $gejala_lain,
			'status_imunisasi_difteri'          => $status_imunisasi_difteri,
			'jenis_spesimen'                    => $jenis_spesimen[0],
			'tanggal_ambil_spesimen'            => $tanggal_ambil_spesimen,
			'kode_spesimen'                     => $kode_spesimen[0],
			'tempat_berobat'                    => $tempat_berobat[0],
			'dirawat'                           => $dirawat,
			'trakeostomi'                       => $trakeostomi,
			'antibiotik'                        => $antibiotik,
			'obat_lain'                         => $obat_lain,
			'ads'                               => $ads,
			'kondisi_kasus'                     => $kondisi_kasus,
			'penderita_berpergian'              => $penderita_berpergian,
			'tempat_berpergian'					=> $tempat_berpergian,
			'penderita_berkunjung'				=> $penderita_berkunjung,
			'tempat_berkunjung'                 => $tempat_berkunjung,
			'penderita_menerima_tamu'           => $penderita_menerima_tamu,
			'asal_tamu'                         => $asal_tamu,
			'nama'                              => $nama,
			'umur'                              => $umur,
			'hubungan_kasus'                    => $hubungan_kasus,
			'status_imunisasi'                  => $status_imunisasi,
			'hasil_lab'                         => $hasil_lab,
			'profilaksis'                       => $profilaksis,
			'created_at'                        => date("Y-m-d H:i:s"),
			'updated_at'                        => date("Y-m-d H:i:s")));

		return $id_pe;
	}



	public static function getPeDifteri()
	{
		$data = DB::table('getpedifteri')->get();
		return $data;
	}



	public static function getEditPeDifteri($id)
	{
		$data = DB::select("select
								pasien.nik,
								pasien.nama_anak,
								pasien.nama_ortu,
								pasien.alamat,
								pasien.tanggal_lahir,
								pasien.umur,
								pasien.umur_bln,
								pasien.umur_hr,
								pasien.jenis_kelamin,
								pasien.id_kelurahan,
								lokasi.provinsi,
								lokasi.kelurahan,
								lokasi.kabupaten,
								lokasi.kecamatan,
								pe_difteri.id,
								pe_difteri.telepon,
								pe_difteri.pekerjaan,
								pe_difteri.alamat_tempat_kerja,
								pe_difteri.nama_saudara_yang_bisa_dihubungi,
								pe_difteri.alamat_saudara_yang_bisa_dihubungi,
								pe_difteri.tlp_saudara_yang_bisa_dihubungi,
								pe_difteri.tempat_tinggal,
								pe_difteri.puskesmas,
								pe_difteri.tanggal_mulai_sakit,
								pe_difteri.keluhan,
								pe_difteri.gejala_sakit_demam,
								pe_difteri.tanggal_gejala_sakit_demam,
								pe_difteri.gejala_sakit_kerongkongan,
								pe_difteri.tanggal_gejala_sakit_kerongkongan,
								pe_difteri.no_epid,
								pe_difteri.gejala_leher_bengkak,
								pe_difteri.tanggal_gejala_leher_bengkak,
								pe_difteri.gejala_sesak_nafas,
								pe_difteri.tanggal_gejala_sesak_nafas,
								pe_difteri.gejala_pseudomembran,
								pe_difteri.tanggal_gejala_pseudomembran,
								pe_difteri.gejala_lain,
								pe_difteri.status_imunisasi_difteri,
								pe_difteri.jenis_spesimen,
								pe_difteri.tanggal_ambil_spesimen,
								pe_difteri.kode_spesimen,
								pe_difteri.tempat_berobat,
								pe_difteri.dirawat,
								pe_difteri.trakeostomi,
								pe_difteri.antibiotik,
								pe_difteri.obat_lain,
								pe_difteri.ads,
								pe_difteri.kondisi_kasus,
								pe_difteri.penderita_berpergian,
								pe_difteri.tempat_berpergian,
								pe_difteri.penderita_berkunjung,
								pe_difteri.tempat_berkunjung,
								pe_difteri.penderita_menerima_tamu,
								pe_difteri.asal_tamu,
								pe_difteri.nama,
								pe_difteri.umur as umur_kontak,
								pe_difteri.hubungan_kasus,
								pe_difteri.status_imunisasi,
								pe_difteri.hasil_lab,
								pe_difteri.profilaksis
							from
								pasien,
								pe_difteri,
								lokasi
							where
								pasien.id_kelurahan=lokasi.id_kelurahan
							and
								pasien.id_pasien=pe_difteri.id_pasien
							and
								pe_difteri.id='".$id."'");
		return $data;
	}

	//cek apakah sudah di PE atau belum
	public static function checkPE($no_epid, $id_difteri) {
		$data = DB::table('pe_difteri AS a')
						->SELECT('c.no_epid','c.id_difteri')
						->JOIN('users AS b','a.created_by','=','b.id')
						->JOIN('difteri AS c','a.id','=','c.id_pe_difteri')
						->WHERE('b.email',Sentry::getUser()->email)
						->WHERE('c.id_difteri',$id_difteri)
						->get();
		return $data;
	}

	public function patients()
	{
		return $this->belongsToMany('Pasien', 'hasil_uji_lab_difteri', 'id_difteri', 'id_pasien');
	}

	//get date of birth
	//fungsi untuk mencari tanggal lahir
	public static function get_tgl_lahir($thn=0, $bln=0, $hari=0,$tgl_sakit=0) {
		$m= substr($tgl_sakit,5,2);
		$d= substr($tgl_sakit,8,2);
		$y= substr($tgl_sakit,0,4);
		$tgl_lahir = @date("d-m-Y", @mktime(1, 1, 1, $m-$bln, $d-$hari, $y-$thn));
		echo $tgl_lahir;
	}

}