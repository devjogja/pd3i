<?php
class Tetanus extends Eloquent {

    //indentifikasi table
    protected $table = "tetanus";

    public $timestamps = false;

    //indentifikasi primaryKey
    protected $primaryKey = 'id_tetanus';

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    // Don't forget to fill this array
    protected $fillable = [
        'id_tempat_periksa',
        'jenis_tempat_periksa',
        'imunisasi_TT_ibu_saat_hamil',
        'status_TT_ibu_saat_hamil',
        'riwayat_persalinan',
        'tanggal_mulai_sakit',
        'bayi_lahir_menangis',
        'tanda_kehidupan_lain',
        'mulut_mecucu',
        'bayi_kejang',
        'dirawat',
        'tempat_dirawat',
        'keadaan_akhir', 'klasifikasi_akhir',
    ];

    public static function simpan(
        $id_tempat_periksa,
        $jenis_tempat_periksa,
        $imunisasi_TT_ibu_saat_hamil,
        $status_TT_ibu_saat_hamil,
        $riwayat_persalinan,
        $tanggal_mulai_sakit,
        $bayi_lahir_menangis,
        $tanda_kehidupan_lain,
        $mulut_mecucu,
        $bayi_kejang,
        $dirawat,
        $tempat_dirawat,
        $keadaan_akhir,
        $klasifikasi_akhir
    ) {
        Tetanus::create(compact(
            'id_tempat_periksa',
            'jenis_tempat_periksa',
            'imunisasi_TT_ibu_saat_hamil',
            'status_TT_ibu_saat_hamil',
            'riwayat_persalinan',
            'tanggal_mulai_sakit',
            'bayi_lahir_menangis',
            'tanda_kehidupan_lain',
            'mulut_mecucu',
            'bayi_kejang',
            'dirawat',
            'tempat_dirawat',
            'keadaan_akhir',
            'klasifikasi_akhir'
        ));
    }

    public static function pasientetanus() {
        $type      = Session::get('type');
        $kd_faskes = Session::get('kd_faskes');
        $wilayah   = "";

        if ($type == "rs") {
            $wilayah = "AND j.kode_faskes='" . $kd_faskes . "'";
        } else if ($type == "puskesmas") {
            $wilayah = "AND h.puskesmas_code_faskes='" . $kd_faskes . "' ";
        } else if ($type == "kabupaten") {
            $wilayah = "AND (h.kode_kab='" . $kd_faskes . "' OR j.kode_kab='" . $kd_faskes . "')";
        } else if ($type == "provinsi") {
            $wilayah = "AND (h.kode_prop='" . $kd_faskes . "' OR j.kode_kab='" . $kd_faskes . "')";
        } else if ($type == "kemenkes") {
            $wilayah = "";
        }

        $data = DB::select("
			SELECT
				c.id_tetanus,
				c.no_epid,
				b.nama_anak,
				c.nama_puskesmas AS nama_faskes,
				b.nama_ortu,
				CONCAT_WS(',',b.alamat,d.kelurahan,e.kecamatan,f.kabupaten,g.provinsi) as alamat,
				b.umur,
				b.umur_bln,
				b.umur_hr,
				b.tanggal_lahir,
				b.jenis_kelamin,
				a.id_pasien_tetanus,
				c.keadaan_akhir,
				IF(c.keadaan_akhir='0','Hidup/Sehat',
				IF(c.keadaan_akhir='1','Meninggal',
				'Tidak Diisi')) as keadaan_akhir_nm,
				c.klasifikasi_akhir,
				IF(c.klasifikasi_akhir='1','Konfirm TN',
				IF(c.klasifikasi_akhir='2','Tersangka TN',
				IF(c.klasifikasi_akhir='3','Bukan TN',
				'Tidak Diisi'))) as klasifikasi_akhir_nm
			FROM pasien_terserang_tetanus a
			JOIN pasien b ON b.id_pasien=a.id_pasien
			JOIN tetanus c ON c.id_tetanus=a.id_tetanus
			LEFT JOIN puskesmas h ON h.puskesmas_id=c.id_tempat_periksa
			LEFT JOIN rumahsakit2 j ON j.id=c.id_tempat_periksa
			LEFT JOIN kelurahan d ON d.id_kelurahan=b.id_kelurahan
			LEFT JOIN kecamatan e ON e.id_kecamatan=d.id_kecamatan
			LEFT JOIN kabupaten f ON f.id_kabupaten=e.id_kabupaten
			LEFT JOIN provinsi g  ON g.id_provinsi=f.id_provinsi
			WHERE
			c.deleted_at IS NULL
			$wilayah
		");
        return $data;
    }

    //simpan data penyelidikan epidemologi tetanus
    public static function epid_tetanus(
        $id_pasien,
        $id_pe,
        $no_epid,
        $sumber_laporan,
        $nama_sumber_laporan,
        $tanggal_laporan_diterima,
        $tanggal_pelacakan,
        $anak_ke,
        $nama_ayah,
        $umur_ayah,
        $th_pendidikan_ayah,
        $pekerjaan_ayah,
        $nama_ibu,
        $th_pendidikan_ibu,
        $pekerjaan_ibu,
        $nama_yang_diwawancarai,
        $hubungan_keluarga,
        $bayi_hidup,
        $tanggal_lahir,
        $tanggal_mulai_sakit,
        $bayi_meninggal,
        $tanggal_meninggal,
        $umur,
        $bayi_menangis,
        $tanda_kehidupan_lain,
        $bayi_menetek,
        $bayi_mencucu,
        $bayi_kejang,
        $dirawat,
        $tempat_berobat,
        $nama_faskes_bayi_dirawat,
        $tanggal_dirawat,
        $keadaan_setelah_dirawat,
        $info_imunisasi,
        $mendapat_imunisasi,
        $jumlah_imunisasi,
        $umur_kehamilan_pertama,
        $tanggal_imunisasi_pertama,
        $umur_kehamilan_kedua,
        $tanggal_imunisasi_kedua,
        $ibu_mendapat_imunisasi,
        $tahun_suntikan_pertama,
        $tahun_suntikan_kedua,
        $ibu_mendapat_suntikan_pengantin,
        $tahun_suntikan_pertama_pengantin,
        $tahun_suntikan_kedua_pengantin,
        $pemotong_tali_pusat,
        $alat_lain,
        $obat_penyembuh,
        $ramuan_lainnya,
        $obat_merawat_tali_pusat,
        $kesimpulan_diagnosis,
        $TT1,
        $TT2,
        $TT3,
        $TT4,
        $TT5,
        $cakupan_tenaga_kesehatan,
        $KN1,
        $KN2,
        $gpa,
        $jml_periksa_kehamilan_dengan_dokter,
        $jml_periksa_kehamilan_dengan_bidan_perawat,
        $jml_periksa_kehamilan_dengan_dukun,
        $jml_Tidak_ANC,
        $jml_tidak_jelas
    ) {
        $id_pe = DB::table('pe_tetanus')->insertGetId(array(
            'id_pasien'                                  => $id_pasien,
            'no_epid'                                    => $no_epid,
            'id_pe'                                      => $id_pe,
            'created_by'                                 => Sentry::getUser()->id,
            'sumber_laporan'                             => $sumber_laporan,
            'nama_sumber_laporan'                        => $nama_sumber_laporan,
            'tanggal_laporan_diterima'                   => $tanggal_laporan_diterima,
            'tanggal_pelacakan'                          => $tanggal_pelacakan,
            'anak_ke'                                    => $anak_ke,
            'nama_ayah'                                  => $nama_ayah,
            'umur_ayah'                                  => $umur_ayah,
            'th_pendidikan_ayah'                         => $th_pendidikan_ayah,
            'pekerjaan_ayah'                             => $pekerjaan_ayah,
            'nama_ibu'                                   => $nama_ibu,
            'th_pendidikan_ibu'                          => $th_pendidikan_ibu,
            'pekerjaan_ibu'                              => $pekerjaan_ibu,
            'nama_yang_diwawancarai'                     => $nama_yang_diwawancarai,
            'hubungan_keluarga'                          => $hubungan_keluarga,
            'bayi_hidup'                                 => $bayi_hidup,
            'tanggal_lahir'                              => $tanggal_lahir,
            'tanggal_mulai_sakit'                        => $tanggal_mulai_sakit,
            'bayi_meninggal'                             => $bayi_meninggal,
            'tanggal_meninggal'                          => $tanggal_meninggal,
            'umur'                                       => $umur,
            'bayi_menangis'                              => $bayi_menangis,
            'tanda_kehidupan_lain'                       => $tanda_kehidupan_lain,
            'bayi_menetek'                               => $bayi_menetek,
            'bayi_mencucu'                               => $bayi_mencucu,
            'bayi_kejang'                                => $bayi_kejang,
            'dirawat'                                    => $dirawat,
            'tempat_berobat'                             => $tempat_berobat,
            'nama_faskes_bayi_dirawat'                   => $nama_faskes_bayi_dirawat,
            'tanggal_dirawat'                            => $tanggal_dirawat,
            'keadaan_setelah_dirawat'                    => $keadaan_setelah_dirawat,
            'info_imunisasi'                             => $info_imunisasi,
            'mendapat_imunisasi'                         => $mendapat_imunisasi,
            'jumlah_imunisasi'                           => $jumlah_imunisasi,
            'umur_kehamilan_pertama'                     => $umur_kehamilan_pertama,
            'tanggal_imunisasi_pertama'                  => $tanggal_imunisasi_pertama,
            'umur_kehamilan_kedua'                       => $umur_kehamilan_kedua,
            'tanggal_imunisasi_kedua'                    => $tanggal_imunisasi_kedua,
            'ibu_mendapat_imunisasi'                     => $ibu_mendapat_imunisasi,
            'tahun_suntikan_pertama'                     => $tahun_suntikan_pertama,
            'tahun_suntikan_kedua'                       => $tahun_suntikan_kedua,
            'ibu_mendapat_suntikan_pengantin'            => $ibu_mendapat_suntikan_pengantin,
            'tahun_suntikan_pertama_pengantin'           => $tahun_suntikan_pertama_pengantin,
            'tahun_suntikan_kedua_pengantin'             => $tahun_suntikan_kedua_pengantin,
            'pemotong_tali_pusat'                        => $pemotong_tali_pusat,
            'alat_lain'                                  => $alat_lain,
            'obat_penyembuh'                             => $obat_penyembuh,
            'ramuan_lainnya'                             => $ramuan_lainnya,
            'obat_merawat_tali_pusat'                    => $obat_merawat_tali_pusat,
            'kesimpulan_diagnosis'                       => $kesimpulan_diagnosis,
            'TT1'                                        => $TT1,
            'TT2'                                        => $TT2,
            'TT3'                                        => $TT3,
            'TT4'                                        => $TT4,
            'TT5'                                        => $TT5,
            'cakupan_tenaga_kesehatan'                   => $cakupan_tenaga_kesehatan,
            'KN1'                                        => $KN1,
            'KN2'                                        => $KN2,
            'gpa'                                        => $gpa,
            'jml_periksa_kehamilan_dengan_dokter'        => $jml_periksa_kehamilan_dengan_dokter,
            'jml_periksa_kehamilan_dengan_bidan_perawat' => $jml_periksa_kehamilan_dengan_bidan_perawat,
            'jml_periksa_kehamilan_dengan_dukun'         => $jml_periksa_kehamilan_dengan_dukun,
            'jml_Tidak_ANC'                              => $jml_Tidak_ANC,
            'jml_tidak_jelas'                            => $jml_tidak_jelas,
            'created_at'                                 => date("Y-m-d H:i:s"),
            'updated_at'                                 => date("Y-m-d H:i:s")));

        // DB::update('update petugas_pemeriksa set id_pe = "'.$id_pe.'" where id_pasien = ?', array($id_pasien));

    }

    public static function getPeTetanus() {
        $data = DB::table('getpetetanus')->get();
        return $data;
    }

    public static function getEditPeTetanus($id) {
        $data = DB::select("select pasien.id_pasien, pasien.nik, pasien.nama_anak, pasien.nama_ortu, pasien.alamat, pasien.tanggal_lahir, pasien.umur, pasien.umur_bln, pasien.umur_hr, pasien.jenis_kelamin, lokasi.provinsi,lokasi.kabupaten,lokasi.kecamatan,lokasi.kelurahan,pasien.id_kelurahan, pe_tetanus.id,pe_tetanus.sumber_laporan,pe_tetanus.nama_sumber_laporan, pe_tetanus.tanggal_laporan_diterima, pe_tetanus.tanggal_pelacakan, pe_tetanus.anak_ke, pe_tetanus.nama_ayah, pe_tetanus.th_pendidikan_ayah, pe_tetanus.umur_ayah, pe_tetanus.pekerjaan_ayah, pe_tetanus.nama_ibu, pe_tetanus.no_epid, pe_tetanus.th_pendidikan_ibu, pe_tetanus.pekerjaan_ibu, pe_tetanus.nama_yang_diwawancarai, pe_tetanus.hubungan_keluarga, pe_tetanus.bayi_hidup, pe_tetanus.tanggal_lahir as tgl_lahir, pe_tetanus.tanggal_mulai_sakit, pe_tetanus.bayi_meninggal, pe_tetanus.tanggal_meninggal, pe_tetanus.umur, pe_tetanus.bayi_menangis, pe_tetanus.tanda_kehidupan_lain, pe_tetanus.bayi_menetek, pe_tetanus.bayi_mencucu, pe_tetanus.dirawat, pe_tetanus.bayi_kejang, pe_tetanus.tempat_berobat, pe_tetanus.tanggal_dirawat, pe_tetanus.keadaan_setelah_dirawat,pe_tetanus.info_imunisasi, pe_tetanus.mendapat_imunisasi, pe_tetanus.jumlah_imunisasi, pe_tetanus.umur_kehamilan_pertama, pe_tetanus.umur_kehamilan_kedua, pe_tetanus.tanggal_imunisasi_kedua, pe_tetanus.ibu_mendapat_imunisasi_1,pe_tetanus.ibu_mendapat_imunisasi_2, pe_tetanus.ibu_mendapat_suntikan_pengantin,pe_tetanus.obat_penyembuh, pe_tetanus.ramuan_lainnya, pe_tetanus.perawatan_tali_pusat, pe_tetanus.obat_merawat_tali_pusat, pe_tetanus.kesimpulan_diagnosis, pe_tetanus.TT1, pe_tetanus.TT2, pe_tetanus.TT3, pe_tetanus.TT4, pe_tetanus.TT5, pe_tetanus.cakupan_tenaga_kesehatan, pe_tetanus.KN1, pe_tetanus.KN2,pe_tetanus.status_ibu_saat_hamil from pasien,pe_tetanus,lokasi where pasien.id_kelurahan=lokasi.id_kelurahan and pasien.id_pasien=pe_tetanus.id_pasien and pe_tetanus.id='" . $id . "'");
        return $data;
    }

    //cek apakah sudah di PE atau belum
    public static function checkPE($no_epid) {
        $data = DB::select("select no_epid from pe_tetanus,users where pe_tetanus.created_by=users.id and users.email='" . Sentry::getUser()->email . "' and no_epid='" . $no_epid . "'");
        return $data;
    }

    public function patients() {
        return $this->belongsToMany('Pasien', 'pasien_terserang_tetanus', 'id_tetanus', 'id_pasien');
    }

}