<?php

class Puskesmas extends \Eloquent {

	//mendefinisikan table puskesmas
	protected $table = 'puskesmas';

	//mendefinisikan primarykey 
	protected $primaryKey = 'id_puskesmas';

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['nama_puskesmas','id_kelurahan','nama_petugas','telepon','email','alamat'];

	//ambil data puskesmas
	public static function getPKM()
	{
		$data= DB::table('puskesmas')
	            ->select('puskesmas_id', 'puskesmas.puskesmas_name', 'puskesmas.puskesmas_code_faskes', 'puskesmas.lokasi_puskesmas', 'puskesmas.alamat','puskesmas.kabupaten_name')
	            ->paginate(10);
		return $data;
	}

	//hitung jumlah rumahskit
	public static function getSumPKM()
	{
		$jml = DB::table('puskesmas')->count();
		return $jml;
	}

	public static function getNamePuskesmas($id)
	{
		$name_faskes = DB::table('puskesmas')->select('puskesmas_name')->where('puskesmas_id',$id)->get();
		if (empty($name_faskes)) {
			$name_faskes = DB::table('laboratorium')->select('nama_laboratorium AS puskesmas_name')->where('lab_code',$id)->get();
		}

		return $name_faskes[0]->puskesmas_name;
	}
}