<?php
class ModulkecamatanModel extends \Eloquent {
	
	public static function getListModulkecamatan()
	{
		///*
		$data= DB::table('kecamatan')
	            ->select(
					
'kecamatan.id_kecamatan',
'kecamatan.id_kabupaten',
'kecamatan.kecamatan'
				)
				
	->where('kecamatan.id_kecamatan','LIKE','%'.Input::get('search_name').'%')
	->orwhere('kecamatan.id_kabupaten','LIKE','%'.Input::get('search_name').'%')
	->orwhere('kecamatan.kecamatan','LIKE','%'.Input::get('search_name').'%') 
				->paginate(20);
		return $data;
	}
	
	//load data provinsi
	public static function getModulkecamatan()
	{
		$data= DB::select("SELECT * FROM kecamatan WHERE id_kecamatan='".Input::get('id')."'");
		return $data;
	}
	
	public static function DoAddData(){
		DB::table('kecamatan')->insertGetId(
			array(
				
'id_kecamatan'=>Input::get('id_kecamatan'),
'id_kabupaten'=>Input::get('id_kabupaten'),
'kecamatan'=>Input::get('kecamatan')
			)
		);
	}
	
	public static function DoUpdateData(){
		DB::update("UPDATE kecamatan SET 
			
id_kecamatan = ?,
id_kabupaten = ?,
kecamatan = ?
			where id_kecamatan = ?", 
			array(
				
Input::get('id_kecamatan'),
Input::get('id_kabupaten'),
Input::get('kecamatan'),
				Input::get('saved_id')
			)
		);
		
		return true;
	}
	
	public static function DoDeleteModulkecamatan(){
		DB::table('kecamatan')->whereIn('id_kecamatan', Input::get('delete_id'))->delete();
		return true;
	}
}
?>
