<?php
class ModulkabupatenModel extends \Eloquent {
	
	public static function getListModulkabupaten()
	{
		///*
		$data= DB::table('kabupaten')
	            ->select(
					
'kabupaten.id_kabupaten',
'kabupaten.id_provinsi',
'kabupaten.kabupaten',
'kabupaten.longitude',
'kabupaten.latitude',
'kabupaten.default_zoom',
'kabupaten.min_zoom',
'kabupaten.konfirmasi'
				)
				
	->where('kabupaten.id_kabupaten','LIKE','%'.Input::get('search_name').'%')
	->orwhere('kabupaten.id_provinsi','LIKE','%'.Input::get('search_name').'%')
	->orwhere('kabupaten.kabupaten','LIKE','%'.Input::get('search_name').'%')
	->orwhere('kabupaten.longitude','LIKE','%'.Input::get('search_name').'%')
	->orwhere('kabupaten.latitude','LIKE','%'.Input::get('search_name').'%')
	->orwhere('kabupaten.default_zoom','LIKE','%'.Input::get('search_name').'%')
	->orwhere('kabupaten.min_zoom','LIKE','%'.Input::get('search_name').'%')
	->orwhere('kabupaten.konfirmasi','LIKE','%'.Input::get('search_name').'%') 
				->paginate(20);
		return $data;
	}
	
	//load data provinsi
	public static function getModulkabupaten()
	{
		$data= DB::select("SELECT * FROM kabupaten WHERE id_kabupaten='".Input::get('id')."'");
		return $data;
	}
	
	public static function DoAddData(){
		DB::table('kabupaten')->insertGetId(
			array(
				
'id_kabupaten'=>Input::get('id_kabupaten'),
'id_provinsi'=>Input::get('id_provinsi'),
'kabupaten'=>Input::get('kabupaten'),
'longitude'=>Input::get('longitude'),
'latitude'=>Input::get('latitude'),
'default_zoom'=>Input::get('default_zoom'),
'min_zoom'=>Input::get('min_zoom'),
'konfirmasi'=>Input::get('konfirmasi')
			)
		);
	}
	
	public static function DoUpdateData(){
		DB::update("UPDATE kabupaten SET 
			
id_kabupaten = ?,
id_provinsi = ?,
kabupaten = ?,
longitude = ?,
latitude = ?,
default_zoom = ?,
min_zoom = ?,
konfirmasi = ?
			where id_kabupaten = ?", 
			array(
				
Input::get('id_kabupaten'),
Input::get('id_provinsi'),
Input::get('kabupaten'),
Input::get('longitude'),
Input::get('latitude'),
Input::get('default_zoom'),
Input::get('min_zoom'),
Input::get('konfirmasi'),
				Input::get('saved_id')
			)
		);
		
		return true;
	}
	
	public static function DoDeleteModulkabupaten(){
		DB::table('kabupaten')->whereIn('id_kabupaten', Input::get('delete_id'))->delete();
		return true;
	}
}
?>
