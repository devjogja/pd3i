<?php
class Crs extends Eloquent {
    protected $table      = "crs";
    public $timestamps    = false;
    protected $primaryKey = 'id_crs';

    public static function getJenisKelaminCrs() {
        $data = DB::select("SELECT
                b.id_pasien,
                b.jenis_kelamin
                FROM
                crs AS a
                JOIN pasien AS b ON a.id_pasien=b.id_pasien
            ");
        return $data;
    }

    public static function getEpid($params = '') {
        $idKelurahan = $params['id_kelurahan'];
        $date        = substr(Helper::changeDate($params['date']), 0, 4);
        $thn         = substr($date, 2, 2);

        $query = DB::table('crs AS a');
        $query->join('pasien AS b', 'a.id_pasien', '=', 'b.id_pasien');
        $query->where('a.deleted_at', null);
        $query->where('b.id_kelurahan', $idKelurahan);
        $query->where('b.tanggal_lahir', $date);
        $count = $query->count();

        $no     = str_pad($count + 1, 3, 0, STR_PAD_LEFT);
        $noepid = 'CRS' . $idKelurahan . $thn . $no;

        return $noepid;

    }

    public static function getJenisKelaminTetanus() {
        $data = DB::select("SELECT
                c.jenis_kelamin,
                c.id_pasien
                FROM
                tetanus AS a
                JOIN pasien_terserang_tetanus AS b ON a.id_tetanus=b.id_tetanus
                JOIN pasien AS c ON b.id_pasien=c.id_pasien
            ");
        return $data;
    }

    public static function getPeCrs() {
        $type      = Session::get('type');
        $kd_faskes = Session::get('kd_faskes');
        $wilayah   = "";
        if ($type == "rs") {
            $wilayah = "AND rs_kode_faskes='" . $kd_faskes . "'";
        } else if ($type == "puskesmas") {
            $wilayah = "AND puskesmas_code_faskes='" . $kd_faskes . "' ";
        } else if ($type == "kabupaten") {
            $wilayah = "AND puskesmas_kode_kab='" . $kd_faskes . "' OR rs_kode_kab='" . $kd_faskes . "'";
        } else if ($type == "provinsi") {
            $wilayah = "AND puskesmas_kode_prop='" . $kd_faskes . "' OR rs_kode_kab='" . $kd_faskes . "'";
        } else if ($type == "kemenkes") {
            $wilayah = "";
        }
        $data = DB::select("
                SELECT
                pe_id_pe_crs AS id,
                crs_no_epid AS no_epid,
                pasien_nama_anak AS nama_anak,
                pasien_nama_ortu AS nama_ortu,
                pasien_tanggal_lahir AS tanggal_lahir,
                pasien_umur AS umur,
                pasien_jenis_kelamin AS jenis_kelamin,
                crs_id_crs AS id_crs,
                alamat_new AS alamat,
                crs_id_tempat_periksa AS id_tempat_periksa
                FROM getdetailcrs
                WHERE pe_id_pe_crs IS NOT NULL AND pe_faskes_id IS NOT NULL AND pe_faskes_id!=0
                $wilayah
            ");
        return $data;
    }

    public static function pasiencrs($date = null, $district = null) {
        $type      = Session::get('type');
        $kd_faskes = Session::get('kd_faskes');
        $wilayah   = "";
        if ($type == "rs") {
            $wilayah = "AND rs_kode_faskes='" . $kd_faskes . "'";
        } else if ($type == "puskesmas") {
            $wilayah = "AND puskesmas_code_faskes='" . $kd_faskes . "' ";
        } else if ($type == "kabupaten") {
            $wilayah = "AND (puskesmas_kode_kab='" . $kd_faskes . "' OR rs_kode_kab='" . $kd_faskes . "')";
        } else if ($type == "provinsi") {
            $wilayah = "AND (puskesmas_kode_prop='" . $kd_faskes . "' OR rs_kode_prop='" . $kd_faskes . "')";
        } else if ($type == "kemenkes") {
            $wilayah = "";
        }

        $data = DB::SELECT("
                SELECT
                crs_id_crs AS id,
                crs_no_epid AS no_epid,
                IF(crs_kode_faskes='rs',rs_nama_faskes,
                IF(crs_kode_faskes='puskesmas',puskesmas_name,
                    'Tidak Diisi')) as nama_faskes,
                pe_id_pe_crs AS id_pe_crs,
                crs_id_crs AS id_crs,
                pasien_umur AS umur,
                pasien_umur_bln AS umur_bln,
                pasien_umur_hr AS umur_hr,
                pasien_nama_anak AS nama_anak,
                pasien_nama_ortu AS nama_ortu,
                pasien_tanggal_lahir AS nama_ortu,
                pasien_jenis_kelamin AS jenis_kelamin,
                alamat_new AS alamat,
                pasien_tanggal_lahir AS tanggal_lahir,
                crs_keadaan_akhir AS keadaan_akhir_nm,
                crs_klasifikasi_final AS klasifikasi_akhir_nm,
                pe_faskes_id,
                status
                FROM 
                getdetailcrs
                WHERE
                1=1
                $district
                $wilayah
                $date
                ORDER BY crs_id_crs ASC
            ");
        return $data;
    }

    public static function exportpasien($date = null, $district = null) {
        $type      = Session::get('type');
        $kd_faskes = Session::get('kd_faskes');
        $wilayah   = "";
        if ($type == "rs") {
            $wilayah = "AND rs_kode_faskes='" . $kd_faskes . "'";
        } else if ($type == "puskesmas") {
            $wilayah = "AND puskesmas_code_faskes='" . $kd_faskes . "' ";
        } else if ($type == "kabupaten") {
            $wilayah = "AND (puskesmas_kode_kab='" . $kd_faskes . "' OR rs_kode_kab='" . $kd_faskes . "')";
        } else if ($type == "provinsi") {
            $wilayah = "AND (puskesmas_kode_prop='" . $kd_faskes . "' OR rs_kode_prop='" . $kd_faskes . "')";
        } else if ($type == "kemenkes") {
            $wilayah = "";
        }
        $data = DB::SELECT("
                SELECT *
                FROM getdetailcrs
                WHERE
                crs_deleted_at IS NULL
                $district
                $wilayah
                $date
            ");

        return $data;
    }

    //cek apakah sudah di PE atau belum
    public static function checkPE($no_epid, $id_pe) {
        $data = DB::SELECT("
                SELECT crs_no_epid AS no_epid FROM getdetailcrs WHERE pe_id_pe_crs='" . $id_pe . "' AND pe_faskes_id IS NOT NULL AND pe_faskes_id != 0
            ");
        return $data;
    }

    public static function getDate($date, $message = '') {
        $sent = ($date == '' || $date == '0000-00-00' || $date == '1970-01-01') ? '-' : $message . date('d-m-Y', strtotime($date));
        return $sent;
    }

}