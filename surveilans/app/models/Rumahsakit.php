<?php

class Rumahsakit extends \Eloquent {

	//mendefinisikan table rumah_sakit
	protected $table = 'rumahsakit';

	//mendefinisikan primaryKey
	protected $primaryKey = 'id_rumah_sakit';
	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['nama_rumah_sakit','id_kelurahan','nama_petugas','telepon','email','alamat'];

	//ambil data rumah_sakit
	public static function getRumahsakit()
	{
		$data= DB::table('rumahsakit')
	            ->select('rumahsakit.id_rumah_sakit', 'rumahsakit.nama_rumah_sakit', 'rumahsakit.alamat', 'rumahsakit.jenis_rumah_sakit')
	            ->paginate(10);
		return $data;
	}

	//hitung jumlah rumahskit
	public static function getSumRM()
	{
		$jml = DB::table('rumahsakit')->count();
		return $jml;
	}
}