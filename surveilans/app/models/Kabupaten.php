<?php

class Kabupaten extends \Eloquent {

	//mendefinisikan table kabupaten
	protected $table = "kabupaten";



	//mendefinisikan primaryKey
	protected $primaryKey = 'id_kabupaten';



	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];



	// Don't forget to fill this array
	protected $fillable = [];



	//ambil data kabupaten sesuai id kabupaten
	public static function getKab($id)
	{
		$data = DB::table('kabupaten')
					->select('id_kabupaten','kabupaten')
					->where('id_provinsi',$id)
					->get();

		return $data;
	}
	
	public function kecamatans()
	{
		return $this->hasMany('Kecamatan', 'id_kabupaten');
	}



	public function provinsi()
	{
		return $this->belongsTo('Provinsi', 'id_provinsi');
	}
}