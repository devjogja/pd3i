<?php

class Pasien extends \Eloquent {

	//indentifikasi table
	protected $table = "pasien";



	public $timestamps = false;



	//indentifikasi primaryKey
	protected $primaryKey = 'id_pasien';



	// Add your validation rules here



	public static $rules = [
		// 'title' => 'required'
	];



	// Don't forget to fill this array
	protected $fillable = [
		'nik',
		'nama_anak',
		'nama_ortu',
		'alamat',
		'tanggal_lahir',
		'umur',
		'jenis_kelamin',
		'id_kelurahan'
	];



	// protected $appends = array('age', 'address');



	//cari nik
	public static function cari($id)
	{
		return self::find($id);
	}



	public static function getPasienCampak() {
		return $pasien = DB::table('pasien')->paginate(10);
	}



	//simpan ke database;
	public static function simpan(
		$nik,
		$nama_anak,
		$nama_ortu,
		$alamat,
		$tanggal_lahir,
		$umur,
		$jenis_kelamin,
		$id_kelurahan
	) {
		Pasien::create(compact(
			'nik',
			'nama_anak',
			'nama_ortu',
			'alamat',
			'tanggal_lahir',
			'umur',
			'jenis_kelamin',
			'id_kelurahan'
		));
	}



	public static function hitungUmur($tgl_lahir)
	{
		list($thn_skrg, $bln_skrg, $tgl_skrg) = explode('-', date('Y-m-d'));
		list($thn_lhr, $bln_lhr, $tgl_lhr)    = explode('-', date('Y-m-d', strtotime($tgl_lahir)));
		$umur = $thn_skrg-$thn_lhr;

		if($bln_skrg<$bln_lhr)
		$umur--;
		else if(($bln_skrg == $bln_lhr) && ($tgl_skrg < $tgl_lhr))
		$umur--;
		return $umur;
	}

	public static function get_tgl_lahir($thn=0, $bln=0, $hari=0,$tgl_sakit=0) {
		$m= substr($tgl_sakit,5,2);
		$d= substr($tgl_sakit,8,2);
		$y= substr($tgl_sakit,0,4);
		$tgl_lahir = @date("d-m-Y", @mktime(1, 1, 1, $m-$bln, $d-$hari, $y-$thn));
		echo $tgl_lahir;
	}

	public static function umur($tgl_lahir)
	{
		$tgl        = explode("-",$tgl_lahir);
		$cek_jmlhr1 = cal_days_in_month(CAL_GREGORIAN,$tgl['1'],$tgl['2']);
		$cek_jmlhr2 = cal_days_in_month(CAL_GREGORIAN,date('m'),date('Y'));
		$sshari     = $cek_jmlhr1 - $tgl['0'];
		$ssbln      = 12 - $tgl['1'] - 1;
		$hari       = 0;
		$bulan      = 0;
		$tahun      = 0;

		//hari+bulan
	    if ($sshari + date('d') >= $cek_jmlhr2) {
			$bulan = 1;
			$hari  = $sshari + date('d') - $cek_jmlhr2;
	    } else {
	        $hari = $sshari + date('d');
	    }

	    if ($ssbln + date('m') + $bulan >= 12) {
			$bulan = ($ssbln + date('m') + $bulan) - 12;
			$tahun = date('Y') - $tgl['2'];
	    }else{
	        $bulan = ($ssbln + date('m') + $bulan);
	        $tahun = (date('Y') - $tgl['2']) - 1;
	    }

	    $selisih = $tahun." Tahun ".$bulan." Bulan ".$hari." Hari";
	    return $selisih;
	}



	/**
	 * Menampilkan tanggal lahir pasien dengan lebih detail dan
	 * dalam format yang lebih rapi [upiq]
	 *
	 * @param  string | $tanggal_lahir | tanggal lahir pasien
	 *
	 * @return string | x tahun y bulan z hari
	 */
	public static function getAgeAttribute($tanggal_lahir = null)
	{
		$now = date_create();

		/*
			Tanggal lahir tidak valid apabila:
			   1) tidak terisi atau valuenya 0000-00-00
			   2) tanggal yang lebih besar dari tanggal hari ini. masa depan.
		*/
		if ($tanggal_lahir == "0000-00-00" ||
			$tanggal_lahir > date_format($now, 'Y-m-d')
		) {
			return "<b>Tanggal lahir tidak valid</b>";
		}


		if(is_null($tanggal_lahir)) {
			return "<b>Tanggal Lahir Belum Diisi</b>";

			/*
				Rencananya mau dibuat accessor dan sekaligus fungsi static...
				Tapi ternyata accessor tidak boleh static...
				Nanti dipikirkan lagi...
				Toh yang ini juga sudah jalan :-)
			*/

		} else {
			$birthday = date_create($tanggal_lahir);
		}

	    $interval = date_diff($birthday, $now);

	    $tahun = $interval->format('%y');
	    $bulan = $interval->format('%m');
	    $hari  = $interval->format('%d');

	    $age = '';

	    if ($tahun >= 1) {
	    	$age = $tahun.' Tahun';
	    }

	    if ($bulan >=1) {
	    	$age = $age.' '.$bulan.' Bulan';
	    }

	    if ($hari >= 1) {
	    	$age = $age.' '.$hari.' Hari';
	    }

	    return $age;
	}



	/**
	 * --- Belum jadi ---
	 * Menampilkan alamat pasien dalam format yang lebih rapi.
	 * Alamat disini hanya berisi nama jalan, no rumah, dan RT - RW [upiq]
	 *
	 * @param  string | $alamat | alamat pasien
	 *
	 * @return string
	 */
	public static function getAddressAttribute($alamat = null)
	{
		if (is_null($alamat)) {
			// return "<b></b>";
			// $array_alamat = preg_split("/[,]+/", $this->alamat);
		} else {
			$array_alamat = preg_split("/[,]+/", $alamat);
		}

		// kata "RT" dan "RW" tetap ditulis kapital
		$exception = array('RT', 'RW', 'KM');
		$formatted_alamat = '';

		foreach ($array_alamat as $alamat) {
			$alamat = strtolower($alamat);

			if(in_array($alamat, $exception)){
				$formatted_alamat.= " ,".$alamat;
			} else {
				$formatted_alamat.= " ,".ucwords($alamat);
			}
		}

		return ltrim($formatted_alamat, ' ,');
	}



	/**
	 * [getUmur description]
	 *
	 * @param  [type] $value [description]
	 *
	 * @return [type]        [description]
	 */
	public function getUmur($value)
    {
        return ucfirst($value);
    }



    /**
     * [tetanus description]
     *
     * @return [type] [description]
     */
	public function tetanus()
	{
		return $this->belongsToMany(
			'Tetanus',
			'pasien_terserang_tetanus',
			'id_pasien',
			'id_tetatus'
		);
	}



	/**
	 * [campak description]
	 *
	 * @return [type] [description]
	 */
	public function campak()
	{
		return $this->belongsToMany(
			'Campak',
			'hasil_uji_lab_campak',
			'id_pasien',
			'id_campak'
		);
	}
}