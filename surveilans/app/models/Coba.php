<?php

class Coba extends \Eloquent {
	
	public static function getListCoba()
	{
		///*
		$data= DB::table('tbl_testing')
	            ->select(
					'tbl_testing.id', 
					'tbl_testing.name')
				->where	('tbl_testing.name','LIKE','%'.Input::get('search_name').'%')
	            ->paginate(20);
		return $data;
	}
	
	//load data provinsi
	public static function getCoba()
	{
		$data= DB::select("SELECT * FROM tbl_testing WHERE id='".Input::get('id')."'");
		return $data;
	}
	
	public static function DoAddData(){
		DB::table('tbl_testing')->insertGetId(
			array(
				'name'=>Input::get('name')
			)
		);
	}
	
	public static function DoUpdateData(){
		DB::update("UPDATE tbl_testing SET 
			name = ? 
			where id = ?", 
			array(
				Input::get('name'),
				Input::get('saved_id')
			)
		);
		
		return true;
	}
	
	public static function DoDeleteCoba(){
		DB::table('tbl_testing')->whereIn('id', Input::get('delete_id'))->delete();
		return true;
	}
	
}