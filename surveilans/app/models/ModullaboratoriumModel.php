<?php
class ModullaboratoriumModel extends \Eloquent {
	
	public static function getListModullaboratorium()
	{
		///*
		$data= DB::table('laboratorium')
	            ->select(
					
'laboratorium.id_laboratorium',
'laboratorium.lab_code',
'laboratorium.nama_laboratorium',
'laboratorium.alamat',
'laboratorium.telepon',
'laboratorium.fax',
'laboratorium.email',
'laboratorium.konfirmasi'
				)
				
	->where('laboratorium.id_laboratorium','LIKE','%'.Input::get('search_name').'%')
	->orwhere('laboratorium.lab_code','LIKE','%'.Input::get('search_name').'%')
	->orwhere('laboratorium.nama_laboratorium','LIKE','%'.Input::get('search_name').'%')
	->orwhere('laboratorium.alamat','LIKE','%'.Input::get('search_name').'%')
	->orwhere('laboratorium.telepon','LIKE','%'.Input::get('search_name').'%')
	->orwhere('laboratorium.fax','LIKE','%'.Input::get('search_name').'%')
	->orwhere('laboratorium.email','LIKE','%'.Input::get('search_name').'%')
	->orwhere('laboratorium.konfirmasi','LIKE','%'.Input::get('search_name').'%') 
				->paginate(20);
		return $data;
	}
	
	//load data provinsi
	public static function getModullaboratorium()
	{
		$data= DB::select("SELECT * FROM laboratorium WHERE id_laboratorium='".Input::get('id')."'");
		return $data;
	}
	
	public static function DoAddData(){
		DB::table('laboratorium')->insertGetId(
			array(
				
'id_laboratorium'=>Input::get('id_laboratorium'),
'lab_code'=>Input::get('lab_code'),
'nama_laboratorium'=>Input::get('nama_laboratorium'),
'alamat'=>Input::get('alamat'),
'telepon'=>Input::get('telepon'),
'fax'=>Input::get('fax'),
'email'=>Input::get('email'),
'konfirmasi'=>Input::get('konfirmasi')
			)
		);
	}
	
	public static function DoUpdateData(){
		DB::update("UPDATE laboratorium SET 
			
id_laboratorium = ?,
lab_code = ?,
nama_laboratorium = ?,
alamat = ?,
telepon = ?,
fax = ?,
email = ?,
konfirmasi = ?
			where id_laboratorium = ?", 
			array(
				
Input::get('id_laboratorium'),
Input::get('lab_code'),
Input::get('nama_laboratorium'),
Input::get('alamat'),
Input::get('telepon'),
Input::get('fax'),
Input::get('email'),
Input::get('konfirmasi'),
				Input::get('saved_id')
			)
		);
		
		return true;
	}
	
	public static function DoDeleteModullaboratorium(){
		DB::table('laboratorium')->whereIn('id_laboratorium', Input::get('delete_id'))->delete();
		return true;
	}
}
?>
