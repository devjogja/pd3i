<?php
class ModulrumahsakitModel extends \Eloquent {

    public static function getListModulrumahsakit() {
        ///*
        $data = DB::table('rumahsakit2')
            ->select(
                'id',
                'kode_faskes',
                'nama_faskes',
                'alamat',
                'longitude',
                'latitude',
                'kepemilikan',
                'kabupaten',
                'tipe_faskes',
                'telp',
                'kode_prop',
                'kode_kab'
            )
            ->where('kode_faskes', 'LIKE', '%' . Input::get('search_name') . '%')
            ->orwhere('nama_faskes', 'LIKE', '%' . Input::get('search_name') . '%')
            ->orwhere('alamat', 'LIKE', '%' . Input::get('search_name') . '%')
            ->orwhere('longitude', 'LIKE', '%' . Input::get('search_name') . '%')
            ->orwhere('latitude', 'LIKE', '%' . Input::get('search_name') . '%')
            ->orwhere('kepemilikan', 'LIKE', '%' . Input::get('search_name') . '%')
            ->orwhere('kabupaten', 'LIKE', '%' . Input::get('search_name') . '%')
            ->orwhere('tipe_faskes', 'LIKE', '%' . Input::get('search_name') . '%')
            ->orwhere('telp', 'LIKE', '%' . Input::get('search_name') . '%')
            ->orwhere('kode_prop', 'LIKE', '%' . Input::get('search_name') . '%')
            ->orwhere('kode_kab', 'LIKE', '%' . Input::get('search_name') . '%')
            ->paginate(20);
        return $data;
    }

    //load data provinsi
    public static function getModulrumahsakit() {
        $data = DB::select("SELECT * FROM rumahsakit2 WHERE id='" . Input::get('id') . "'");
        return $data;
    }

    public static function DoAddData() {
        DB::table('rumahsakit2')->insertGetId(
            array(
                'kode_faskes' => Input::get('id_rumah_sakit'),
                'nama_faskes' => Input::get('nama_rumah_sakit'),
                'alamat'      => Input::get('alamat'),
                'longitude'   => Input::get('longitude'),
                'latitude'    => Input::get('latitude'),
                'kepemilikan' => Input::get('jenis_rumah_sakit'),
                'kabupaten'   => Input::get('kabupaten'),
                'tipe_faskes' => Input::get('kelas_rumah_sakit'),
                'telp'        => Input::get('telepon'),
                'kode_prop'   => Input::get('kode_prop'),
                'kode_kab'    => Input::get('kode_kab'),
            )
        );
    }

    public static function DoUpdateData() {
        DB::update("UPDATE rumahsakit2 SET
                kode_faskes = ?,
                nama_faskes = ?,
                alamat = ?,
                longitude = ?,
                latitude = ?,
                kepemilikan = ?,
                kabupaten = ?,
                tipe_faskes = ?,
                telp = ?,
                kode_prop = ?,
                kode_kab = ?,
            where id = ?",
            array(
                Input::get('id_rumah_sakit'),
                Input::get('nama_rumah_sakit'),
                Input::get('alamat'),
                Input::get('longitude'),
                Input::get('latitude'),
                Input::get('jenis_rumah_sakit'),
                Input::get('kabupaten'),
                Input::get('kelas_rumah_sakit'),
                Input::get('telepon'),
                Input::get('kode_prop'),
                Input::get('kode_kab'),
                Input::get('saved_id'),
            )
        );

        return true;
    }

    public static function DoDeleteModulrumahsakit() {
        DB::table('rumahsakit2')->whereIn('id', Input::get('delete_id'))->delete();
        return true;
    }
}
