 <?php

class Kelurahan extends \Eloquent {

	//mendefinisikan table kelurahan
	protected $table = "kelurahan";



	//mendefinisikan primaryKey
	protected $primaryKey = 'id_kelurahan';



	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];



	// Don't forget to fill this array
	protected $fillable = [];



	//ambil kelurahan sesuai id kecamatan
	public static function getKel($id)
	{
		$data = DB::table('kelurahan')
					->select('id_kelurahan','kelurahan')
					->where('id_kecamatan',$id)
					->get();

		return $data;
	}



	public function kecamatan()
	{
		return $this->belongsTo('Kecamatan', 'id_kecamatan');
	}

}