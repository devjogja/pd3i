<?php

class Laboratorium extends \Eloquent {

	//mendefinisikan table laboratorium
	protected $table ='laboratorium';

	//mendefinikan primary key
	protected $primaryKey ='id_laboratorium';
	
	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['nama_laboratorium','id_kelurahan','nama_petugas','telepon','email','alamat'];

	//load data laboratorium
	public static function getLab()
	{
		$data= DB::table('laboratorium')
	            ->select('laboratorium.id_laboratorium', 'laboratorium.nama_laboratorium', 'laboratorium.telepon', 'laboratorium.alamat','laboratorium.fax')
	            ->paginate(10);
		return $data;
	}
   
	//hitung jumlah lab
	public static function getSumLab()
	{
		$jml = DB::table('laboratorium')->count();
		return $jml;
	}

	public static function getLaboratorium($id) {

		$data = DB::select("select nama_laboratorium, alamat, email, telepon, fax from laboratorium where id_laboratorium='".$id."'");
		return $data;
	}

	public static function getDaftarSampel($date=null, $district=null)
	{
		$type = Session::get('type');
		$kd_faskes = Session::get('kd_faskes');
		$wilayah='';
		if($type == "laboratorium"){
			$wilayah = "AND labs_lab_code='".$kd_faskes."'";
		}

		$data = DB::SELECT("
				SELECT * FROM getdetailcampak
				WHERE
				lab_status_lab='1'
				$district
				$wilayah
				$date
			");
		return $data;
	}

	public static function getListSampel($case,$date=null,$district=null)
	{
		$type = Session::get('type');
		$kd_faskes = Session::get('kd_faskes');
		$wilayah='';
		if($type == "laboratorium"){
			$wilayah = "AND c.lab_code='".$kd_faskes."'";
		}
		$data = array();
		if ($case=='crs') {
			$data = DB::SELECT("
				SELECT b.id_crs,b.no_epid AS no_epid, d.nama_anak AS nama_pasien, d.umur, d.umur_bln, d.umur_hr, CASE WHEN d.jenis_kelamin='0' THEN 'Laki-Laki' ELSE 'Perempuan' END AS jenis_kelamin, e.kabupaten, b.klasifikasi_final
				FROM hasil_uji_lab_crs AS a
				JOIN crs AS b ON a.id_hasil_uji_lab_crs=b.id_hasil_uji_lab_crs
				JOIN laboratorium AS c ON a.id_laboratorium=c.id_laboratorium
				JOIN pasien AS d ON b.id_pasien=d.id_pasien
				JOIN located AS e ON d.id_kelurahan=e.id_kelurahan
				WHERE a.validity_lab IS NOT NULL
				$district
				$wilayah
				$date
				");
		}elseif($case=='campak'){
			$data = DB::SELECT("
				SELECT
				b.id_campak,
				a.no_spesimen AS no_lab,
				d.nama_anak AS nama_pasien,
				d.umur,
				d.umur_bln,
				d.umur_hr,
				CASE
				WHEN d.jenis_kelamin='1' THEN 'Laki-Laki'
				WHEN d.jenis_kelamin='2' THEN 'Perempuan'
				ELSE 'Tidak Jelas' END AS jenis_kelamin,
				e.kabupaten,
				CASE
					WHEN b.klasifikasi_final='1' THEN 'Campak (Lab)'
					WHEN b.klasifikasi_final='2' THEN 'Campak (Epid)'
					WHEN b.klasifikasi_final='3' THEN 'Campak (Klinis)'
					WHEN b.klasifikasi_final='4' THEN 'Rubella'
					WHEN b.klasifikasi_final='5' THEN 'Bukan campak/rubella'
					WHEN b.klasifikasi_final='6' THEN 'Pending'
					ELSE 'Tidak diisi'
				END AS klasifikasi_final,
				DATE_FORMAT(f.tgl_pemeriksaan,'%d-%m-%Y') AS tgl_pemeriksaan,
				f.id_sampel,
				f.jenis_pemeriksaan,
				f.jenis_sampel,
				f.hasil_uji_sampel
				FROM hasil_uji_lab_campak AS a
				JOIN campak AS b ON a.id_campak=b.id_campak
				JOIN laboratorium AS c ON a.id_lab=c.id_laboratorium
				JOIN pasien AS d ON a.id_pasien=d.id_pasien
				LEFT JOIN located AS e ON d.id_kelurahan=e.id_kelurahan
				LEFT JOIN uji_sampel AS f ON b.id_campak=f.id_campak
				WHERE a.status_lab='1'
				$district
				$wilayah
				$date
				");
			/*$data=array();
			$r=0;
			foreach ($dt as $val) {
				$data[$val->id_campak] = [
					'row'=>$r,
					'id_campak'=>$val->id_campak,
					'no_lab'=>$val->no_lab,
					'nama_pasien'=>$val->nama_pasien,
					'umur'=>$val->umur,
					'umur_bln'=>$val->umur_bln,
					'umur_hr'=>$val->umur_hr,
					'jenis_kelamin'=>$val->jenis_kelamin,
					'klasifikasi_final'=>$val->klasifikasi_final,
				];
				foreach ($dt as $k1 => $v1) {
					if (isset($v1->id_sampel)) {
						$data[$v1->id_campak]['dtrow'][$v1->id_sampel] = [
							'tgl_pemeriksaan'=>$v1->tgl_pemeriksaan,
							'jenis_pemeriksaan'=>$v1->jenis_pemeriksaan,
							'jenis_sampel'=>$v1->jenis_sampel,
							'hasil_uji_sampel'=>$v1->hasil_uji_sampel,
						];
						$r++;
					}
				}
			}
			unset($dt);*/
		}

		return $data;
	}

	public static function deleteHasil($id)
	{
		
	}

}