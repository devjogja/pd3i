<?php
class Helper {
	public static function getDate($date, $message=''){
		return ($date==''||$date=='0000-00-00'||$date=='1970-01-01') ? '':$message.date('d-m-Y',strtotime($date));
	}

	public static function formatDate($date, $message=''){
		return ($date==''||$date=='0000-00-00'||$date=='1970-01-01') ? '':$message.date('Y/m/d',strtotime($date));
	}

	public static function changeDate($date='')
	{
		return (empty($date))?null:date('Y-m-d',strtotime($date));
	}

	public static function defaultValue($data){
		return (empty($data))?'':$data;
	}
}?>