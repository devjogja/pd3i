<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Campak extends Eloquent
{
    //indentifikasi table
    protected $table = 'campak';
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    public $timestamps = false;

    //indentifikasi primaryKey
    protected $primaryKey = 'id_campak';

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    // Don't forget to fill this array
    protected $fillable = [
        'id_laboratorium',
        'id_tempat_periksa',
        'jenis_tempat_periksa',
        'no_epid',
        'vaksin_campak_sebelum_sakit',
        'tanggal_timbul_demam',
        'tanggal_timbul_rash',
        'tanggal_diambil_spesimen_darah',
        'tanggal_diambil_spesimen_urin',
        'vitamin_A',
        'keadaan_akhir',
    ];

    public static function simpan(
        $id_laboratorium,
        $id_tempat_periksa,
        $jenis_tempat_periksa,
        $no_epid,
        $vaksin_campak_sebelum_sakit,
        $tanggal_timbul_demam,
        $tanggal_timbul_rash,
        $tanggal_diambil_spesimen_darah,
        $tanggal_diambil_spesimen_urin,
        $vitamin_A,
        $keadaan_akhir
    ) {
        self::create(compact(
            'id_laboratorium',
            'id_tempat_periksa',
            'jenis_tempat_periksa',
            'no_epid',
            'vaksin_campak_sebelum_sakit',
            'tanggal_timbul_demam',
            'tanggal_timbul_rash',
            'tanggal_diambil_spesimen_darah',
            'tanggal_diambil_spesimen_urin',
            'vitamin_A',
            'keadaan_akhir'
        ));
    }

    public static function simpanHasilLabCampak(
        $id_hasil_uji_lab_campak,
        $id_pasien, $id_campak,
        $hasil_spesimen_darah,
        $hasil_spesimen_urin,
        $klasifikasi_final,
        $tanggal_uji_laboratorium
    ) {
        DB::table('hasil_uji_lab_campak')->insert(array(
            'id_hasil_uji_lab_campak' => $id_hasil_uji_lab_campak,
            'id_pasien' => $id_pasien,
            'id_campak' => $id_campak,
            'hasil_spesimen_darah' => $hasil_spesimen_darah,
            'hasil_spesimen_urin' => $hasil_spesimen_urin,
            'klasifikasi_final' => $klasifikasi_final,
            'tanggal_uji_laboratorium' => $tanggal_uji_laboratorium,
        ));
    }

    public static function simpanHasilLabRubella(
        $hasil_spesimen_darah,
        $hasil_spesimen_urin,
        $klasifikasi_final_rubella
    ) {
        DB::table('hasil_uji_lab_rubella')->insert(array(
            'hasil_spesimen_darah' => $hasil_spesimen_darah,
            'hasil_spesimen_urin' => $hasil_spesimen_urin,
            'klasifikasi_final_rubella' => $klasifikasi_final_rubella));
    }

    public static function pasiencampak($date = null, $district = null)
    {
        $type = Session::get('type');
        $kd_faskes = Session::get('kd_faskes');
        $wilayah = '';
        if ($type == 'rs') {
            $wilayah = "AND rs_kode_faskes='" . $kd_faskes . "'";
        } elseif ($type == 'puskesmas') {
            $wilayah = "AND puskesmas_code_faskes='" . $kd_faskes . "' ";
        } elseif ($type == 'kabupaten') {
            $wilayah = "AND (puskesmas_id_kabupaten='" . $kd_faskes . "' OR rs_id_kabupaten='" . $kd_faskes . "')";
        } elseif ($type == 'provinsi') {
            $wilayah = "AND (puskesmas_id_provinsi='" . $kd_faskes . "' OR rs_id_provinsi='" . $kd_faskes . "')";
        } elseif ($type == 'kemenkes') {
            $wilayah = '';
        }
        $data = DB::select("
			SELECT
			campak_no_epid AS no_epid,
			campak_no_epid_klb AS no_epid_klb,
			campak_jenis_kasus AS jenis_kasus,
			campak_id_campak AS id_campak,
			CASE
			WHEN campak_kode_faskes='puskesmas' THEN puskesmas_name
			ELSE rs_nama_faskes
			END AS nama_faskes,
			pasien_umur AS umur,
			pasien_umur_bln AS umur_bln,
			pasien_umur_hr AS umur_hr,
			pasien_nama_anak AS nama_anak,
			pe_id AS id_pe_campak,
			alamat_new AS alamat,
			CASE
			WHEN campak_keadaan_akhir='1' THEN 'Hidup/Sehat'
			WHEN campak_keadaan_akhir='2' THEN 'Meninggal'
			ELSE 'Tidak diisi'
			END AS 'keadaan_akhir_nm',
			CASE
			WHEN campak_klasifikasi_final='1' THEN 'Campak (Lab)'
			WHEN campak_klasifikasi_final='2' THEN 'Campak (Epid)'
			WHEN campak_klasifikasi_final='3' THEN 'Campak (Klinis)'
			WHEN campak_klasifikasi_final='4' THEN 'Rubella'
			WHEN campak_klasifikasi_final='5' THEN 'Bukan campak/rubella'
			WHEN campak_klasifikasi_final='6' THEN 'Pending'
			ELSE 'Tidak diisi'
			END AS 'klasifikasi_final_nm'
			FROM getdetailcampak
			WHERE
			1=1
			$district
			$wilayah
			$date
			");

        return $data;
    }

    //ambil data pasien sesuain laboratorium yg diakses
    public static function pasienLabcampak()
    {
        $data = DB::select("select lokasi.kabupaten,campak.no_epid,hasil_uji_lab_campak.id_uji_lab,pasien.nama_anak,pasien.umur,hasil_uji_lab_campak.tanggal_terima_spesimen_laboratorium,hasil_uji_lab_campak.tanggal_pengiriman_spesimen_laboratorium,hasil_uji_lab_campak.kondisi_spesimen_dipropinsi,hasil_uji_lab_campak.hasil_igm_campak,hasil_uji_lab_campak.hasil_igm_rubella from campak,pasien,hasil_uji_lab_campak,users,laboratorium,lokasi where lokasi.id_kelurahan=pasien.id_kelurahan and pasien.id_pasien=hasil_uji_lab_campak.id_pasien and campak.id_campak=hasil_uji_lab_campak.id_campak and users.id_user=laboratorium.id_laboratorium and users.email='" . Sentry::getUser()->email . "' and users.hak_akses=5 and hasil_uji_lab_campak.id_uji_lab=laboratorium.lab_code");

        return $data;
    }

    public static function count()
    {
        $data = DB::select("SELECT
			count(*) as jml
			FROM
			pasien,campak,users,laboratorium,hasil_uji_lab_campak
			WHERE
			users.id_user=laboratorium.id_laboratorium
			AND
			pasien.id_pasien=hasil_uji_lab_campak.id_pasien
			AND
			campak.id_campak=hasil_uji_lab_campak.id_campak
			AND
			campak.id_laboratorium = laboratorium.id_laboratorium
			AND
			users.email='" . Sentry::getUser()->email . "'");
        $hsl = $data[0]->jml;

        return $hsl;
    }

    public static function epid_campak(
        $id_pasien,
        $gejala_umum,
        $tanggal_timbul_gejala,
        $komplikasi,
        $no_epid,
        $catatan_komplikasi,
        $waktu_pengobatan,
        $tempat_pengobatan,
        $obat_yang_diberikan,
        $dirumah_orang_sakit_sama,
        $kapan_sakit_dirumah,
        $disekolah_orang_sakit_sama,
        $kapan_sakit_disekolah,
        $kekurangan_gizi,
        $imunisasi_campak_sudah_diberikan,
        $usia_terakhir_imunisasi,
        $sediaan,
        $hasil_lab,
        $tanggal_penyelidikan,
        $pelaksana
    ) {
        DB::table('pe_campak')->insert(array(
            'id_pasien' => $id_pasien,
            'faskes_id' => Sentry::getUser()->id_user,
            'created_by' => Sentry::getUser()->id,
            'gejala_umum' => $gejala_umum,
            'tanggal_timbul_gejala' => $tanggal_timbul_gejala,
            'komplikasi' => $komplikasi,
            'no_epid' => $no_epid,
            'catatan_komplikasi' => $catatan_komplikasi,
            'waktu_pengobatan' => $waktu_pengobatan,
            'tempat_pengobatan' => $tempat_pengobatan,
            'obat_yang_diberikan' => $obat_yang_diberikan,
            'dirumah_orang_sakit_sama' => $dirumah_orang_sakit_sama,
            'kapan_sakit_dirumah' => $kapan_sakit_dirumah,
            'disekolah_orang_sakit_sama' => $disekolah_orang_sakit_sama,
            'kapan_sakit_disekolah' => $kapan_sakit_disekolah,
            'kekurangan_gizi' => $kekurangan_gizi,
            'imunisasi_campak_sudah_diberikan' => $imunisasi_campak_sudah_diberikan,
            'usia_terakhir_imunisasi' => $usia_terakhir_imunisasi,
            'sediaan' => $sediaan,
            'hasil_lab' => $hasil_lab,
            'tanggal_penyelidikan' => $tanggal_penyelidikan,
            'pelaksana' => $pelaksana,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ));
    }

    public static function getPeCampak()
    {
        $type = Session::get('type');
        $kd_faskes = Session::get('kd_faskes');
        $wilayah = '';
        if ($type == 'rs') {
            $wilayah = "AND rs_kode_faskes='" . $kd_faskes . "'";
        } elseif ($type == 'puskesmas') {
            $wilayah = "AND puskesmas_code_faskes='" . $kd_faskes . "' ";
        } elseif ($type == 'kabupaten') {
            $wilayah = "AND (puskesmas_id_kabupaten='" . $kd_faskes . "' OR rs_id_kabupaten='" . $kd_faskes . "')";
        } elseif ($type == 'provinsi') {
            $wilayah = "AND (puskesmas_id_provinsi='" . $kd_faskes . "' OR rs_id_provinsi='" . $kd_faskes . "')";
        } elseif ($type == 'kemenkes') {
            $wilayah = '';
        }
        $data = DB::SELECT("
			SELECT
			*
			FROM getpecampak
			WHERE
			1=1
			$wilayah
			");

        return $data;
    }

    public static function getEpid($params = '')
    {
        $idKelurahan = $params['id_kelurahan'];
        $date = substr(Helper::changeDate($params['date']), 0, 4);
        $thn = substr($date, 2, 2);

        $query = DB::table('campak AS a');
        $query->join('hasil_uji_lab_campak AS b', 'a.id_campak', '=', 'b.id_campak');
        $query->join('pasien AS c', 'b.id_pasien', '=', 'c.id_pasien');
        $query->where('a.deleted_at', null);
        $query->where('c.id_kelurahan', $idKelurahan);
        $query->where(DB::raw('CASE
			WHEN a.tanggal_timbul_demam IS NOT NULL THEN YEAR(a.tanggal_timbul_demam)
			WHEN a.tanggal_timbul_rash IS NOT NULL THEN YEAR(a.tanggal_timbul_rash)
			END'), $date);
        $count = $query->count();

        $no = str_pad($count + 1, 3, 0, STR_PAD_LEFT);
        $noepid = 'C' . $idKelurahan . $thn . $no;

        return $noepid;
    }

    public static function getEditPeCampak($id)
    {
        $data = DB::select("select
			pe_campak.id,
			pasien.nik,
			pasien.nama_anak,
			pasien.nama_ortu,
			located.provinsi,
			located.kabupaten,
			located.kecamatan,
			located.kelurahan,
			located.id_kabupaten,
			located.id_kecamatan,
			located.id_kelurahan,
			pasien.alamat,
			pasien.tanggal_lahir,
			pasien.umur,
			pasien.umur_bln,
			pasien.umur_hr,
			pasien.jenis_kelamin,
			pasien.id_kelurahan,
			pe_campak.id,
			pe_campak.waktu_pengobatan,
			pe_campak.tempat_pengobatan,
			pe_campak.obat_yang_diberikan,
			pe_campak.no_epid,
			pe_campak.dirumah_orang_sakit_sama,
			pe_campak.kapan_sakit_dirumah,
			pe_campak.disekolah_orang_sakit_sama,
			pe_campak.kapan_sakit_disekolah,
			pe_campak.kekurangan_gizi,
			pe_campak.alamat_tinggal_sementara,
			pe_campak.tgl_pe_dialamat_sementara,
			pe_campak.jumlah_kasus_dialamat_sementara,
			pe_campak.alamat_tempat_tinggal,
			pe_campak.tgl_pe_alamat_tempat_tinggal,
			pe_campak.jumlah_kasus_dialamat_tempat_tinggal,
			pe_campak.sekolah,
			pe_campak.tgl_pe_sekolah,
			pe_campak.jumlah_kasus_disekolah,
			pe_campak.tempat_kerja,
			pe_campak.tgl_pe_tempat_kerja,
			pe_campak.jumlah_kasus_tempat_kerja,
			pe_campak.lain_lain,
			pe_campak.tgl_pe_lain_lain,
			pe_campak.jumlah_kasus_lain_lain,
			pe_campak.jumlah_total_kasus_tambahan,
			pe_campak.tanggal_penyelidikan,
			pe_campak.pelaksana,
			campak.id_campak,
			campak.jenis_kasus
			FROM
			pe_campak
			JOIN campak ON pe_campak.id = campak.id_pe_campak
			JOIN hasil_uji_lab_campak ON campak.id_campak = hasil_uji_lab_campak.id_campak
			JOIN pasien ON hasil_uji_lab_campak.id_pasien=pasien.id_pasien
			LEFT JOIN located ON pasien.id_kelurahan=located.id_kelurahan
			WHERE
			pe_campak.id='" . $id . "'");

        return $data;
    }

    public static function exportpasien($date = null, $district = null)
    {
        $type = Session::get('type');
        $kd_faskes = Session::get('kd_faskes');
        $wilayah = '';

        if ($type == 'rs' || $type == 'puskesmas') {
            $wilayah = "AND code_faskes='" . $kd_faskes . "'";
        } else if ($type == 'kabupaten') {
            $wilayah = "AND code_kabupaten_faskes='" . $kd_faskes . "'";
        } else if ($type == 'provinsi') {
            $wilayah = "AND code_provinsi_faskes='" . $kd_faskes . "'";
        } else {
            $wilayah = '';
        }

        $data = DB::SELECT("
			SELECT
			a.*,
			b.jenis_pemeriksaan,
			b.jenis_sampel,
			b.tgl_ambil_sampel,
			b.hasil_igm_campak,
			b.hasil_igm_rubella,
			b.nama_penyakit
			FROM view_case_campak AS a
			LEFT JOIN uji_spesimen AS b ON a.id_campak=b.id_campak
			WHERE
			1=1
			$district
			$wilayah
			$date
			");
        return $data;
    }

    //cek apakah sudah di PE atau belum
    public static function checkPE($id_campak)
    {
        $data = DB::SELECT("
			SELECT
			b.id
			FROM campak AS a
			JOIN pe_campak AS b ON a.id_pe_campak=b.id
			WHERE a.id_campak='$id_campak' AND b.status_at='1'
			");

        return $data;
    }

    public function patients()
    {
        return $this->belongsToMany('Pasien', 'hasil_uji_lab_campak', 'id_campak', 'id_pasien');
    }

    public static function DaftarGejala()
    {
        return DB::table('daftar_gejala_campak')->select('id', 'nama')->get();
    }

    public static function getDaftarGejala($id)
    {
        return DB::select("select id_daftar_gejala_campak,tgl_mulai from gejala_campak where id_campak='" . $id . "'");
    }

    public static function getTGLmulaigejala($id, $id_campak)
    {
        $data = DB::select("select tgl_mulai from gejala_campak where id_daftar_gejala_campak='" . $id . "' and id_campak ='" . $id_campak . "'");
        $date = Helper::getDate($data[0]->tgl_mulai);

        return $date;
    }

    public static function TipeSpesimen()
    {
        return DB::table('tipe_spesimen')->get();
    }

    public static function getAttributesImunisasi($id)
    {
        /*
        '0' => '1X',
        '1'=>'2X',
        '2'=>'3X',
        '3'=>'4X',
        '4'=>'5X',
        '5'=>'6X',
        '6'=>'Tidak',
        '7'=>'Tidak tahu'
         */

        if ($id == 1) {
            $hasil = '1X';
        } elseif ($id == 2) {
            $hasil = '2X';
        } elseif ($id == 3) {
            $hasil = '3X';
        } elseif ($id == 4) {
            $hasil = '4X';
        } elseif ($id == 5) {
            $hasil = '5X';
        } elseif ($id == 6) {
            $hasil = '6X';
        } elseif ($id == 7) {
            $hasil = 'Tidak';
        } elseif ($id == 8) {
            $hasil = 'Tidak tahu';
        }

        return $hasil;
    }
}
